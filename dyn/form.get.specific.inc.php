<?php
/**
 * Ce fichier permet de définir des variables spécifiques à passer dans la
 * méthode formulaire des objets métier
 *
 * @package openmairie_exemple
 * @version SVN : $Id: form.get.specific.inc.php 2376 2013-06-11 09:14:57Z fmichon $
 */

/**
 * Exemple : un ecran specifique me permet de passer la date de naissance de
 * l'utilisateur au formulaire uniquement lorsque l'objet est "agenda".
 *
 * if ($obj == "agenda") {
 *     $datenaissance = "";
 *     if (isset($_GET['datenaissance'])) {
 *         $datenaissance = $_GET['datenaissance'];
 *     }
 *     $extra_parameters["datenaissance"] = $datenaissance;
 * }
 *
 * Ainsi dans la methode formulaire de l'objet en question la valeur de la date
 * de naissance sera accessible
 */

// Permet de passer le numéro du dossier d'instruction au formulaire de demande
if ($obj == "visite") {
    $date_visite = "";
    if (isset($_GET['date_visite'])) {
        $date_visite = $_GET['date_visite'];
    }
    $extra_parameters["date_visite"] = $date_visite;
    //
    $heure_debut = "";
    if (isset($_GET['heure_debut'])) {
        $heure_debut = $_GET['heure_debut'];
    }
    $extra_parameters["heure_debut"] = $heure_debut;
    //
    $heure_fin = "";
    if (isset($_GET['heure_fin'])) {
        $heure_fin = $_GET['heure_fin'];
    }
    $extra_parameters["heure_fin"] = $heure_fin;
    //
    $id_technicien = "";
    if (isset($_GET['id_technicien'])) {
        $id_technicien = $_GET['id_technicien'];
    }
    $extra_parameters["id_technicien"] = $id_technicien;
    //
    $id_programmation = "";
    if (isset($_GET['id_programmation'])) {
        $id_programmation = $_GET['id_programmation'];
    }
    $extra_parameters["id_programmation"] = $id_programmation;
    //
    $id_di = "";
    if (isset($_GET['id_di'])) {
        $id_di = $_GET['id_di'];
    }
    $extra_parameters["id_di"] = $id_di;
}

?>
