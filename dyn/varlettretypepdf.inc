<?php
/**
 * Ce script est utilisé pour remplacer la variable &contraintes_obj dans les éditions PDF
 * où obj peut prendre la valeur de 'dc' ou 'etab'.
 *
 * Le fait que le champ de fusion des contraintes soit paramétrable oblige à utiliser les
 * variables de remplacement. Les éditions concernées faisant appel aux requêtes objet
 * on se sert du fonctionnement de ces dernières pour tester et récupérer l'ID des objets.
 * 
 * &contraintes_obj
 * Liste de toutes les contraintes du DC/établissement
 * 
 * &contraintes_obj(liste_groupe=g1,g2...;liste_ssgroupe=sg1,sg2...)
 * Les options liste_groupe et liste_ssgroupe sont optionnelles et peuvent contenir une
 * valeur unique ou plusieurs valeurs separées par une virgule, sans espace.
 *
 * &contraintes_obj(affichage_sans_arborescence=t)
 * L'option affichage_sans_arborescence permet d'afficher une liste de contraintes sans
 * leurs groupes et sous-groupes, et sans puces. Il peut prendre t (Oui) ou f (Non) comme
 * valeur.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

// Cas DC
if (isset($merge_fields_values['dossier_coordination.dossier_coordination']) === true
    && empty($merge_fields_values['dossier_coordination.dossier_coordination']) === false) {
    $this->f->replace_contrainte('dc', $merge_fields_values['dossier_coordination.dossier_coordination'], $titre, $corps);
}

// Cas établissement
if (isset($merge_fields_values['etablissement.etablissement']) === true
    && empty($merge_fields_values['etablissement.etablissement']) === false) {
    $this->f->replace_contrainte('etab', $merge_fields_values['etablissement.etablissement'], $titre, $corps);
}

// Purge des éventuelles variables de remplacement restantes
$this->f->empty_contrainte('dc', $titre, $corps);
$this->f->empty_contrainte('etab', $titre, $corps);

?>
