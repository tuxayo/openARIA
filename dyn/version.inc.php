<?php
/**
 * Ce fichier permet de stocker la version de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: version.inc.php 2198 2013-03-28 17:08:33Z fmichon $
 */

/**
 * Le numero de version est affiche en permanence dans le footer
 */
$version = "1.2.2.dev0";

?>
