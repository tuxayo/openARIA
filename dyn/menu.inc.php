<?php
/**
 * Ce fichier permet de configurer quelles actions vont etre disponibles
 * dans le menu.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: menu.inc.php 2447 2013-08-27 16:46:03Z fmichon $
 */

/**
 * $menu est le tableau associatif qui contient tout le menu de
 * l'application, il contient lui meme un tableau par rubrique, puis chaque
 * rubrique contient un tableau par lien
 *
 * Caracteristiques :
 * --- tableau rubrik
 *     - title [obligatoire]
 *     - description (texte qui s'affiche au survol de la rubrique)
 *     - href (contenu du lien href)
 *     - class (classe css qui s'affiche sur la rubrique)
 *     - right [optionnel] (droit que l'utilisateur doit avoir pour visionner
 *                          cette rubrique, si aucun droit n'est mentionne alors
 *                          si aucun lien n'est present dans cette rubrique, on
 *                          ne l'affiche pas)
 *     - links [obligatoire]
 *     - open [optionnel] permet de définir les critères permettant
 *           de conserver la rubrique de menu ouverte.
 *           La définition est une liste de criteres, de type array, contenant des chaines
 *           de type "script|obj" ou "script|" ou "|obj".
 *           S'il y a un unique critere on peut ne pas mettre de array
 *           Si un critere correspond avec l'URL, la rubrique est ouverte.
 *           
 *
 * --- tableau links
 *     - title [obligatoire]
 *     - href [obligatoire] (contenu du lien href)
 *     - class (classe css qui s'affiche sur l'element)
 *     - right (droit que l'utilisateur doit avoir pour visionner cet element)
 *     - target (pour ouvrir le lien dans une nouvelle fenetre)
 *     - open [optionnel] idem à ci-dessus. Les "open" de links sont utilises pour la rubrik :
 *           pas besoin de definir le critere dans rubrik si il est defini dans links
 *           la correspondance rend le lien actif et la rubrique est ouverte
 *           exemples :
 *               open => array("tab.php|users", "form.php|users"),
 *               open => "|users"
 *               open => "script.php|"
 */
//
$menu = array();

// {{{ Rubrique ETABLISSEMENTS
//
$rubrik = array(
    "title" => _("etablissements"),
    "class" => "etablissements",
    "right" => "menu_etablissement",
);
//
$links = array();
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_referentiel_erp",
    "class" => "etablissement_referentiel_erp",
    "title" => _("Référentiel ERP"),
    "right" => array("etablissement_referentiel_erp", "etablissement_referentiel_erp_tab", ),
    "open" => array("tab.php|etablissement_referentiel_erp", "form.php|etablissement_referentiel_erp", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_tous",
    "class" => "etablissement_tous",
    "title" => _("Tous les établissements"),
    "right" => array("etablissement_tous", "etablissement_tous_tab", ),
    "open" => array("tab.php|etablissement_tous", "form.php|etablissement_tous", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "etablissement_referentiel_erp", "etablissement_referentiel_erp_tab", "etablissement_tous", "etablissement_tous_tab",
        "etablissement_unite", "etablissement_unite_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_unite",
    "class" => "etablissement_unite",
    "title" => _("unités d'accessibilité"),
    "right" => array("etablissement_unite", "etablissement_unite_tab", ),
    "open" => array("tab.php|etablissement_unite", "form.php|etablissement_unite", ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique DOSSIER
// 
$rubrik = array(
    "title" => _("dossiers"),
    "class" => "dossiers",
    "right" => "menu_dossier_instruction",
);
//
$links = array();
//
$links[] = array(
    "class" => "category",
    "title" => _("DC (coordination)"),
    "right" => array(
        "dossier_coordination_nouveau_ajouter",
        "dossier_coordination_a_qualifier", "dossier_coordination_a_qualifier_tab",
        "dossier_coordination_a_cloturer", "dossier_coordination_a_cloturer_tab",
        "dossier_coordination", "dossier_coordination_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=dossier_coordination_nouveau&amp;action=0&amp;advs_id=&amp;tricol=&amp;valide=&amp;retour=tab",
    "class" => "dossier_coordination_nouveau",
    "title" => _("Nouveau dossier"),
    "right" => array(
        "dossier_coordination_nouveau_ajouter", 
    ),
    "open" => array("form.php|dossier_coordination_nouveau",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_coordination_nouveau_ajouter", 
        "dossier_coordination_a_qualifier", "dossier_coordination_a_qualifier_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination_a_qualifier",
    "class" => "dossier_coordination_a_qualifier",
    "title" => _("Dossiers à qualifier"),
    "right" => array(
        "dossier_coordination_a_qualifier", "dossier_coordination_a_qualifier_tab",
    ),
    "open" => array("tab.php|dossier_coordination_a_qualifier", "form.php|dossier_coordination_a_qualifier",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_coordination_a_qualifier", "dossier_coordination_a_qualifier_tab",
        "dossier_coordination_a_cloturer", "dossier_coordination_a_cloturer_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination_a_cloturer",
    "class" => "dossier_coordination_a_cloturer",
    "title" => _("Dossiers à cloturer"),
    "right" => array(
        "dossier_coordination_a_cloturer", "dossier_coordination_a_cloturer_tab",
    ),
    "open" => array("tab.php|dossier_coordination_a_cloturer", "form.php|dossier_coordination_a_cloturer",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_coordination_a_cloturer", "dossier_coordination_a_cloturer",
        "dossier_coordination", "dossier_coordination_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination",
    "class" => "dossier_coordination",
    "title" => _("Tous les dossiers"),
    "right" => array(
        "dossier_coordination", "dossier_coordination_tab",
    ),
    "open" => array("tab.php|dossier_coordination", "form.php|dossier_coordination",),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("DI (instruction)"),
    "right" => array(
        "menu_dossier_instruction",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_a_affecter",
    "class" => "dossier_instruction_a_affecter",
    "title" => _("Dossiers à affecter"),
    "right" => array(
        "dossier_instruction_a_affecter", "dossier_instruction_a_affecter_tab",
    ),
    "open" => array("tab.php|dossier_instruction_a_affecter", "form.php|dossier_instruction_a_affecter",),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_a_qualifier",
    "class" => "dossier_instruction_a_qualifier",
    "title" => _("Dossiers à qualifier"),
    "right" => array(
        "dossier_instruction_a_qualifier", "dossier_instruction_a_qualifier_tab",
    ),
    "open" => array("tab.php|dossier_instruction_a_qualifier", "form.php|dossier_instruction_a_qualifier",),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction_mes_plans", "dossier_instruction_mes_plans_tab", 
        "dossier_instruction_tous_plans", "dossier_instruction_tous_plans_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_mes_plans",
    "class" => "dossier_instruction_mes_plans",
    "title" => _("Mes plans"),
    "right" => array(
        "dossier_instruction_mes_plans", "dossier_instruction_mes_plans_tab", 
    ),
    "open" => array("tab.php|dossier_instruction_mes_plans", "form.php|dossier_instruction_mes_plans", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_tous_plans",
    "class" => "dossier_instruction_tous_plans",
    "title" => _("Tous les plans"),
    "right" => array(
        "dossier_instruction_tous_plans", "dossier_instruction_tous_plans_tab",
    ),
    "open" => array("tab.php|dossier_instruction_tous_plans", "form.php|dossier_instruction_tous_plans", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction_mes_visites", "dossier_instruction_mes_visites_tab", 
        "dossier_instruction_tous_visites", "dossier_instruction_tous_visites_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_mes_visites",
    "class" => "dossier_instruction_mes_visites",
    "title" => _("Mes visites"),
    "right" => array(
        "dossier_instruction_mes_visites", "dossier_instruction_mes_visites_tab",
    ),
    "open" => array("tab.php|dossier_instruction_mes_visites", "form.php|dossier_instruction_mes_visites", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction_tous_visites",
    "class" => "dossier_instruction_tous_visites",
    "title" => _("Toutes les visites"),
    "right" => array(
        "dossier_instruction_tous_visites", "dossier_instruction_tous_visites_tab", 
    ),
    "open" => array("tab.php|dossier_instruction_tous_visites", "form.php|dossier_instruction_tous_visites", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "dossier_instruction", "dossier_instruction_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_instruction",
    "class" => "dossier_instruction",
    "title" => _("Tous les dossiers"),
    "right" => array(
        "dossier_instruction", "dossier_instruction_tab",
    ),
    "open" => array("tab.php|dossier_instruction", "form.php|dossier_instruction",),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Visites"),
    "right" => array(
        "visite_mes_visites_a_realiser", "visite_mes_visites_a_realiser_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=visite_mes_visites_a_realiser",
    "class" => "visite_mes_visites_a_realiser",
    "title" => _("Mes visites à réaliser"),
    "right" => array(
        "visite_mes_visites_a_realiser", "visite_mes_visites_a_realiser_tab",
    ),
    "open" => array("tab.php|visite_mes_visites_a_realiser", "form.php|visite_mes_visites_a_realiser", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Messages"),
    "right" => array(
        "dossier_coordination_message_mes_non_lu", "dossier_coordination_message_mes_non_lu_tab",
        "dossier_coordination_message_tous", "dossier_coordination_message_tous_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination_message_mes_non_lu",
    "class" => "dossier_coordination_message_mes_non_lu",
    "title" => _("Mes non lus"),
    "right" => array(
        "dossier_coordination_message_mes_non_lu", "dossier_coordination_message_mes_non_lu_tab",
    ),
    "open" => array("tab.php|dossier_coordination_message_mes_non_lu", "form.php|dossier_coordination_message_mes_non_lu", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination_message_tous",
    "class" => "dossier_coordination_message_tous",
    "title" => _("Tous les messages"),
    "right" => array(
        "dossier_coordination_message_tous", "dossier_coordination_message_tous_tab",
    ),
    "open" => array("tab.php|dossier_coordination_message_tous", "form.php|dossier_coordination_message_tous", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Documents entrants"),
    "right" => array(
        "piece_non_lu", "piece_non_lu_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=piece_non_lu",
    "class" => "piece_non_lu",
    "title" => _("mes non lus"),
    "right" => array(
        "piece_non_lu", "piece_non_lu_tab", 
    ),
    "open" => array("tab.php|piece_non_lu", "form.php|piece_non_lu", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("Autorite de police"),
    "right" => array(
        "autorite_police_non_notifiee_executee", "autorite_police_non_notifiee_executee_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=autorite_police_non_notifiee_executee",
    "class" => "autorite_police_non_notifiee_executee",
    "title" => _("AP non notif./exec."),
    "right" => array(
        "autorite_police_non_notifiee_executee", "autorite_police_non_notifiee_executee_tab",
    ),
    "open" => array("tab.php|autorite_police_non_notifiee_executee", "form.php|autorite_police_non_notifiee_executee", ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique REUNIONS
// 
$rubrik = array(
    "title" => _("suivi"),
    "class" => "suivi",
    "right" => array("menu_suivi" ),
);
//
$links = array();
//
$links[] = array(
    "class" => "category",
    "title" => _("documents entrants"),
    "right" => array(
        "piece_bannette", "piece_bannette_tab", 
        "piece_suivi", "piece_suivi_tab", 
        "piece_a_valider", "piece_a_valider_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=piece_bannette",
    "class" => "piece_bannette",
    "title" => _("bannette"),
    "right" => array(
        "piece_bannette", "piece_bannette_tab", 
    ),
    "open" => array("tab.php|piece_bannette", "form.php|piece_bannette", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "piece_bannette", "piece_bannette_tab", 
        "piece_suivi", "piece_suivi_tab", 
        "piece_a_valider", "piece_a_valider_tab",
        "piece_non_lu", "piece_non_lu_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=piece_suivi",
    "class" => "piece_suivi",
    "title" => _("suivi"),
    "right" => array(
        "piece_suivi", "piece_suivi_tab", 
    ),
    "open" => array("tab.php|piece_suivi", "form.php|piece_suivi", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=piece_a_valider",
    "class" => "piece_a_valider",
    "title" => _("a valider"),
    "right" => array(
        "piece_a_valider", "piece_a_valider_tab", 
    ),
    "open" => array("tab.php|piece_a_valider", "form.php|piece_a_valider", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("documents generes"),
    "right" => array(
        "courrier", "courrier_tab", 
        "courrier_suivi_form_suivi",
        "courrier_rar_form_rar",
        "contact_institutionnel", "contact_institutionnel_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier",
    "class" => "courrier",
    "title" => _("gestion"),
    "right" => array(
        "courrier", "courrier_tab", 
    ),
    "open" => array("tab.php|courrier", "form.php|courrier", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_a_editer",
    "class" => "courrier_a_editer",
    "title" => _("à éditer"),
    "right" => array(
        "courrier_a_editer", "courrier_a_editer_tab", 
    ),
    "open" => array("tab.php|courrier_a_editer", "form.php|courrier_a_editer", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_attente_signature",
    "class" => "courrier_attente_signature",
    "title" => _("attente signature"),
    "right" => array(
        "courrier_attente_signature", "courrier_attente_signature_tab", 
    ),
    "open" => array("tab.php|courrier_attente_signature", "form.php|courrier_attente_signature", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_attente_retour_ar",
    "class" => "courrier_attente_retour_ar",
    "title" => _("attente retour AR"),
    "right" => array(
        "courrier_attente_retour_ar", "courrier_attente_retour_ar_tab", 
    ),
    "open" => array("tab.php|courrier_attente_retour_ar", "form.php|courrier_attente_retour_ar", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "courrier", "courrier_tab", 
        "courrier_suivi_form_suivi",
        "courrier_rar_form_rar",
        "contact_institutionnel", "contact_institutionnel_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=courrier_suivi&amp;action=7&amp;idx=0",
    "class" => "courrier_suivi",
    "title" => _("Suivi par code barres"),
    "right" => array(
        "courrier_suivi_form_suivi",
    ),
    "open" => array("form.php|courrier_suivi", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=courrier_rar&amp;action=8&amp;idx=0",
    "class" => "courrier_rar",
    "title" => _("edition RAR"),
    "right" => array(
        "courrier_rar_form_rar",
    ),
    "open" => array("form.php|courrier_rar", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=contact_institutionnel",
    "class" => "contact",
    "title" => _("contact_institutionnel"),
    "right" => array(
        "contact_institutionnel", "contact_institutionnel_tab", 
    ),
    "open" => array("tab.php|contact_institutionnel", "form.php|contact_institutionnel", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("programmations"),
    "right" => array("programmation", "programmation_tab", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=programmation",
    "class" => "programmation",
    "title" => _("gestion"),
    "right" => array("programmation", "programmation_tab", ),
    "open" => array("tab.php|programmation", "form.php|programmation", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("reunions"),
    "right" => array("reunion", "reunion_tab", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reunion",
    "class" => "reunion",
    "title" => _("gestion"),
    "right" => array("reunion", "reunion_tab", ),
    "open" => array("tab.php|reunion", "form.php|reunion", ),
);
//
$links[] = array(
    "class" => "category",
    "title" => _("pilotage"),
    "right" => array(
        "statistiques", "statistiques_tab", 
        "reqmo", 
    ),
);
//
$links[] = array(
    "href" => "../app/pilotage.php",
    "class" => "statistiques",
    "title" => _("statistiques"),
    "right" => array("statistiques", "statistiques_tab", ),
    "open" => array("pilotage.php|", ),
);
//
$links[] = array(
    "href" => "../scr/reqmo.php",
    "class" => "reqmo",
    "title" => _("requêtes mémorisées"),
    "right" => "reqmo",
    "open" => array("reqmo.php|", "requeteur.php|", ),
);
//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}





// {{{ Rubrique PARAMETRAGE
//
$rubrik = array(
    "title" => _("parametrage"),
    "class" => "parametrage",
    "right" => "menu_parametrage",
);
//
$links = array();



//
$links[] = array(
    "class" => "category",
    "title" => _("etablissements"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_type",
    "class" => "etablissement_type",
    "title" => _("types"),
    "description" => _("Typologie d'un établissement représentant son activité."),
    "right" => array("etablissement_type", "etablissement_type_tab", ),
    "open" => array("tab.php|etablissement_type", "form.php|etablissement_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_categorie",
    "class" => "etablissement_categorie",
    "title" => _("categories"),
    "description" => _("Liste des catégories d'un établissement représentant sa capacité."),
    "right" => array("etablissement_categorie", "etablissement_categorie_tab", ),
    "open" => array("tab.php|etablissement_categorie", "form.php|etablissement_categorie", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_nature",
    "class" => "etablissement_nature",
    "title" => _("natures"),
    "description" => _("Liste des natures d'un établissement (ERP Référentiel, ERP non référentiel, Bâtiment non ERP, ...)."),
    "right" => array("etablissement_nature", "etablissement_nature_tab", ),
    "open" => array("tab.php|etablissement_nature", "form.php|etablissement_nature", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_etat",
    "class" => "etablissement_etat",
    "title" => _("etats"),
    "description" => _("Liste des etats d'un établissement (Ouvert, Fermé, Non suivi, ...)."),
    "right" => array("etablissement_etat", "etablissement_etat_tab", ),
    "open" => array("tab.php|etablissement_etat", "form.php|etablissement_etat", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_statut_juridique",
    "class" => "etablissement_statut_juridique",
    "title" => _("statuts juridiques"),
    "description" => _("Liste des statuts juridiques d'un établissement (ville, public, privé, ...)."),
    "right" => array("etablissement_statut_juridique", "etablissement_statut_juridique_tab", ),
    "open" => array("tab.php|etablissement_statut_juridique", "form.php|etablissement_statut_juridique", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=etablissement_tutelle_adm",
    "class" => "etablissement_tutelle_adm",
    "title" => _("tutelles administratives"),
    "description" => _("Liste des tutelles administratives d'un établissement lorsque son statut juridique est 'public'."),
    "right" => array("etablissement_tutelle_adm", "etablissement_tutelle_adm_tab", ),
    "open" => array("tab.php|etablissement_tutelle_adm", "form.php|etablissement_tutelle_adm", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=periodicite_visites",
    "class" => "periodicite_visites",
    "title" => _("periodicites de visites"),
    "description" => _("Paramétrage de la périodicité des visites obligatoires à réaliser sur les établissements de nature 'ERP référentiel'."),
    "right" => array("periodicite_visites", "periodicite_visites_tab", ),
    "open" => array("tab.php|periodicite_visites", "form.php|periodicite_visites", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("adresses"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=voie",
    "class" => "voie",
    "title" => _("voies"),
    "description" => _("Liste des voies auxquelles sont rattachées les établissements."),
    "right" => array("voie", "voie_tab", ),
    "open" => array("tab.php|voie", "form.php|voie", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=arrondissement",
    "class" => "arrondissement",
    "title" => _("arrondissements"),
    "description" => _("Liste des arrondissements auxquels sont rattachés les établissements."),
    "right" => array("arrondissement", "arrondissement_tab", ),
    "open" => array("tab.php|arrondissement", "form.php|arrondissement", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("contacts"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=contact_type",
    "class" => "contact_type",
    "title" => _("types"),
    "description" => _("Typologie d'un contact."),
    "right" => array("contact_type", "contact_type_tab", ),
    "open" => array("tab.php|contact_type", "form.php|contact_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=contact_civilite",
    "class" => "contact_civilite",
    "title" => _("civilites"),
    "description" => _("Liste des civilités d'un contact."),
    "right" => array("contact_civilite", "contact_civilite_tab", ),
    "open" => array("tab.php|contact_civilite", "form.php|contact_civilite", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("contraintes"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=contrainte",
    "class" => "contrainte",
    "title" => _("listing"),
    "description" => _("Liste des contraintes paramétrées et applicables aux établissements / dossiers de coordination."),
    "right" => array("contrainte", "contrainte_tab", ),
    "open" => array("tab.php|contrainte", "form.php|contrainte", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=contrainte_synchronisation&action=4&idx=0",
    "class" => "contrainte_synchronisation",
    "title" => _("synchronisation"),
    "description" => _("Synchroniser les contraintes paramétrées liées à un référentiel avec celles provenant du SIG."),
    "right" => array("contrainte_synchronisation", "contrainte_synchronisation_synchroniser", ),
    "open" => array("form.php|contrainte_synchronisation", ),
    "parameters" => array("option_sig" => "sig_externe", ),
);

//
$links[] = array(
    "class" => "category",
    "title" => _("metiers"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=service",
    "class" => "service",
    "title" => _("services"),
    "description" => _("Liste des services."),
    "right" => array("service", "service_tab", ),
    "open" => array("tab.php|service", "form.php|service", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=acteur",
    "class" => "acteur",
    "title" => _("acteurs"),
    "description" => _("Liste des acteurs de l'application représentant les cadres, techniciens et secrétaires affectés à un service. Un acteur peut être rattaché à un utilisateur ou non."),
    "right" => array("acteur", "acteur_tab", ),
    "open" => array("tab.php|acteur", "form.php|acteur", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reunion_avis",
    "class" => "reunion_avis",
    "title" => _("avis"),
    "description" => _("Liste des avis possibles sur un dossier que ce soit en réunion, suite à une visite ou dans une analyse."),
    "right" => array("reunion_avis", "reunion_avis_tab", ),
    "open" => array("tab.php|reunion_avis", "form.php|reunion_avis", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reunion_instance",
    "class" => "reunion_instance",
    "title" => _("instances"),
    "description" => _("Paramétrage des instances convoquées lors des réunions ou lors des visites ainsi que de leurs membres."),
    "right" => array("reunion_instance", "reunion_instance_tab", ),
    "open" => array("tab.php|reunion_instance", "form.php|reunion_instance", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=autorite_competente",
    "class" => "autorite_competente",
    "title" => _("autorites competentes"),
    "description" => _("Liste des autorités compétentes d'un dossier d'instruction."),
    "right" => array("autorite_competente", "autorite_competente_tab", ),
    "open" => array("tab.php|autorite_competente", "form.php|autorite_competente", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=derogation_scda",
    "class" => "derogation_scda",
    "title" => _("derogations scda"),
    "description" => _("Liste des dérogations SCDA disponibles depuis les données techniques des établissements."),
    "right" => array("derogation_scda", "derogation_scda_tab", ),
    "open" => array("tab.php|derogation_scda", "form.php|derogation_scda", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("reunions"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reunion_type",
    "class" => "reunion_type",
    "title" => _("types"),
    "description" => _("Typologie et paramétrage de toutes les informations communes à chaque réunion et qui caractérisent un type de réunion."),
    "right" => array("reunion_type", "reunion_type_tab", ),
    "open" => array("tab.php|reunion_type", "form.php|reunion_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reunion_categorie",
    "class" => "reunion_categorie",
    "title" => _("categories"),
    "description" => _("Liste des catégories de dossiers traitées en réunion."),
    "right" => array("reunion_categorie", "reunion_categorie_tab", ),
    "open" => array("tab.php|reunion_categorie", "form.php|reunion_categorie", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("autorites de police"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=autorite_police_decision",
    "class" => "autorite_police_decision",
    "title" => _("types"),
    "description" => _("Typologie et paramétrage d'une décision d'autorité de police notamment les délais."),
    "right" => array("autorite_police_decision", "autorite_police_decision_tab", ),
    "open" => array("tab.php|autorite_police_decision", "form.php|autorite_police_decision", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=autorite_police_motif",
    "class" => "autorite_police_motif",
    "title" => _("motifs"),
    "description" => _("Liste des motifs d'une décision d'autorité de police."),
    "right" => array("autorite_police_motif", "autorite_police_motif_tab", ),
    "open" => array("tab.php|autorite_police_motif", "form.php|autorite_police_motif", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("analyses"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=analyses_type",
    "class" => "analyses_type",
    "title" => _("types"),
    "description" => _("Typologie et paramétrage des analyses notamment les modèles d'éditions associés."),
    "right" => array("analyses_type", "analyses_type_tab", ),
    "open" => array("tab.php|analyses_type", "form.php|analyses_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=essai_realise",
    "class" => "essai_realise",
    "title" => _("essais realises"),
    "description" => _("Textes types disponibles à l'insertion depuis le formulaire de saisie des essais réalisés lors de l'analyse des dossiers d'instruction."),
    "right" => array("essai_realise", "essai_realise_tab", ),
    "open" => array("tab.php|essai_realise", "form.php|essai_realise", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=document_presente",
    "class" => "document_presente",
    "title" => _("documents presentes"),
    "description" => _("Textes types disponibles à l'insertion depuis le formulaire de saisie des documents présentés lors de l'analyse des dossiers d'instruction."),
    "right" => array("document_presente", "document_presente_tab", ),
    "open" => array("tab.php|document_presente", "form.php|document_presente", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=reglementation_applicable",
    "class" => "reglementation_applicable",
    "title" => _("reglementations applicables"),
    "description" => _("Textes types disponibles à l'insertion depuis le formulaire de saisie des réglementation applicables lors de l'analyse des dossiers d'instruction."),
    "right" => array("reglementation_applicable", "reglementation_applicable_tab", ),
    "open" => array("tab.php|reglementation_applicable", "form.php|reglementation_applicable", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=prescription_reglementaire",
    "class" => "prescription_reglementaire",
    "title" => _("prescriptions"),
    "description" => _("Paramétrage des prescriptions réglementaires et spécifiques utilisées dans les analyses des dossiers d'instruction."),
    "right" => array("prescription_reglementaire", "prescription_reglementaire_tab", ),
    "open" => array("tab.php|prescription_reglementaire", "form.php|prescription_reglementaire", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("documents entrants"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=piece_type",
    "class" => "piece_type",
    "title" => _("types"),
    "description" => _("Typologie d'un document entrant."),
    "right" => array("piece_type", "piece_type_tab", ),
    "open" => array("tab.php|piece_type", "form.php|piece_type", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("documents generes"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_texte_type",
    "class" => "courrier_texte_type",
    "title" => _("complements"),
    "description" => _("Textes types disponibles à l'insertion depuis le formulaire de saisie d'un document généré dans les champs compléments."),
    "right" => array("courrier_texte_type", "courrier_texte_type_tab", ),
    "open" => array("tab.php|courrier_texte_type", "form.php|courrier_texte_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=signataire_qualite",
    "class" => "signataire_qualite",
    "title" => _("qualites de signataire"),
    "description" => _("Liste des qualités d'un signataire."),
    "right" => array(
        "signataire_qualite", "signataire_qualite_tab", 
    ),
    "open" => array("tab.php|signataire_qualite", "form.php|signataire_qualite", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=signataire",
    "class" => "signataire",
    "title" => _("signataires"),
    "description" => _("Paramétrage des signataires disponibles depuis un document généré ou un PV."),
    "right" => array("signataire", "signataire_tab", ),
    "open" => array("tab.php|signataire", "form.php|signataire", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("visites"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=visite_duree",
    "class" => "visite_duree",
    "title" => _("durees"),
    "description" => _("Liste des durées de visite."),
    "right" => array("visite_duree", "visite_duree_tab", ),
    "open" => array("tab.php|visite_duree", "form.php|visite_duree", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=visite_motif_annulation",
    "class" => "visite_motif_annulation",
    "title" => _("motifs d'annulation"),
    "description" => _("Liste des motifs d'annulation d'une visite."),
    "right" => array("visite_motif_annulation", "visite_motif_annulation_tab", ),
    "open" => array("tab.php|visite_motif_annulation", "form.php|visite_motif_annulation", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("dossiers"),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_type",
    "class" => "dossier_type",
    "title" => _("types"),
    "description" => _("Typologie des types de dossiers de coordination (Visites, Plans, ...)."),
    "right" => array("dossier_type", "dossier_type_tab", ),
    "open" => array("tab.php|dossier_type", "form.php|dossier_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=dossier_coordination_type",
    "class" => "dossier_coordination_type",
    "title" => _("types de DC"),
    "description" => _("Typologie et paramétrage des dossiers de coordination (AT, PC, Visite périodque, ...)."),
    "right" => array("dossier_coordination_type", "dossier_coordination_type_tab", ),
    "open" => array("tab.php|dossier_coordination_type", "form.php|dossier_coordination_type", ),
);


//
$links[] = array(
    "class" => "category",
    "title" => _("editions"),
    //"description" => _("Configuration des éditions PDF disponibles dans l'application."),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "courrier_type", "courrier_type_tab",
        "courrier_type_categorie", "courrier_type_categorie_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_type",
    "class" => "courrier_type",
    "title" => _("types"),
    "description" => _("Typologie et paramétrage d'un modèle d'édition qui permet de filtrer les modèles d'édition disponibles en fonction du contexte des interfaces."),
    "right" => array(
        "courrier_type", "courrier_type_tab", 
    ),
    "open" => array("tab.php|courrier_type", "form.php|courrier_type", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=courrier_type_categorie",
    "class" => "courrier_type_categorie",
    "title" => _("categories"),
    "description" => _("Paramétrage des catégories de types de modèles d'édition. Cette catégorisation permet de définir le contexte dans lequel les types de modèles d'édtion rattachés à cette catégorie vont être disponibles."),
    "right" => array(
        "courrier_type_categorie", "courrier_type_categorie_tab", 
    ),
    "open" => array("tab.php|courrier_type_categorie", "form.php|courrier_type_categorie", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "modele_edition", "modele_edition_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=modele_edition",
    "class" => "modele_edition",
    "title" => _("modeles d'edition"),
    "description" => _("Paramétrage des modèles d'édition par la sélection de leur type et de la lettre type utilisée."),
    "right" => array(
        "modele_edition", "modele_edition_tab", 
    ),
    "open" => array("tab.php|modele_edition", "form.php|modele_edition", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_lettretype", "om_lettretype_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_lettretype",
    "class" => "om_lettretype",
    "title" => _("lettres types"),
    "description" => _("Composition des lettres types."),
    "right" => array("om_lettretype", "om_lettretype_tab"),
    "open" => array("tab.php|om_lettretype", "form.php|om_lettretype", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_logo", "om_logo_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_logo",
    "class" => "om_logo",
    "title" => _("logos"),
    "description" => _("Paramétrage des logos disponibles depuis l'écran de composition des lettres types."),
    "right" => array("om_logo", "om_logo_tab", ),
    "open" => array("tab.php|om_logo", "form.php|om_logo", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_sousetat", "om_sousetat_tab",
        "om_requete", "om_requete_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sousetat",
    "class" => "om_sousetat",
    "title" => _("sous états"),
    "description" => _("Paramétrage des tableaux (appelés sous-états) disponibles à l'insertion depuis l'écran de composition des lettres types."),
    "right" => array("om_sousetat", "om_sousetat_tab", ),
    "open" => array("tab.php|om_sousetat", "form.php|om_sousetat", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_requete",
    "class" => "om_requete",
    "title" => _("requêtes"),
    "description" => _("Paramétrage des configurations de champs de fusion disponibles depuis l'écran de composition des lettres types."),
    "right" => array("om_requete", "om_requete_tab", ),
    "open" => array("tab.php|om_requete", "form.php|om_requete", ),
);



//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

// {{{ Rubrique ADMINISTRATION
//
$rubrik = array(
    "title" => _("administration"),
    "class" => "administration",
    "right" => "menu_administration",
);
//
$links = array();



//
$links[] = array(
    "class" => "category",
    "title" => _("général"),
    "right" => array(
        "om_collectivite", "om_collectivite_tab", 
        "om_parametre", "om_parametre_tab", 
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_collectivite",
    "class" => "collectivite",
    "title" => _("collectivites"),
    "description" => _("Paramétrage des collectivités."),
    "right" => array(
        "om_collectivite", "om_collectivite_tab", 
    ),
    "open" => array("tab.php|om_collectivite", "form.php|om_collectivite", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_parametre",
    "class" => "parametre",
    "title" => _("parametres"),
    "description" => _("Divers paramètres de l'application : champs de fusion généraux disponibles pour les éditions pdf, activation/désactivation de modules complémentaires, paramétrages fonctionnels, ..."),
    "right" => array(
        "om_parametre", "om_parametre_tab", 
    ),
    "open" => array("tab.php|om_parametre", "form.php|om_parametre", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("gestion des utilisateurs"),
    "right" => array(
        "om_utilisateur", "om_utilisateur_tab", "om_profil", "om_profil_tab",
        "om_droit", "om_droit_tab", "directory",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_utilisateur", "om_utilisateur_tab", "om_profil", "om_profil_tab",
        "om_droit", "om_droit_tab",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_profil",
    "class" => "profil",
    "title" => _("profils"),
    "description" => _("Paramétrage des profils utilisateurs et de toutes les permissions qui y sont associées."),
    "right" => array("om_profil", "om_profil_tab", ),
    "open" => array("tab.php|om_profil", "form.php|om_profil", ),
);
//
// $links[] = array(
//     "href" => "../scr/tab.php?obj=om_droit",
//     "class" => "droit",
//     "title" => _("om_droit"),
//     "description" => _("Matrice des permissions affectées à chaque profil."),
//     "right" => array("om_droit", "om_droit_tab", ),
//     "open" => array("tab.php|om_droit", "form.php|om_droit", ),
// );
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_utilisateur",
    "class" => "utilisateur",
    "title" => _("utilisateurs"),
    "description" => _("Paramétrage des utilisateurs autorisés à se connecter à l'application."),
    "right" => array("om_utilisateur", "om_utilisateur_tab", ),
    "open" => array(
        "tab.php|om_utilisateur",
        "form.php|om_utilisateur[action=0]",
        "form.php|om_utilisateur[action=1]",
        "form.php|om_utilisateur[action=2]",
        "form.php|om_utilisateur[action=3]",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("om_utilisateur", "om_utilisateur_synchroniser", ),
    "parameters" => array("isDirectoryOptionEnabled" => true, ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=om_utilisateur&idx=0&action=11",
    "class" => "annuaire",
    "title" => _("annuaire"),
    "description" => _("Interface de synchronisation des utilisateurs avec un annuaire."),
    "right" => array("om_utilisateur", "om_utilisateur_synchroniser", ),
    "open" => array("form.php|om_utilisateur[action=11]", ),
    "parameters" => array("isDirectoryOptionEnabled" => true, ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("tableaux de bord"),
    "right" => array(
        "om_widget", "om_widget_tab", "om_dashboard",
    ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_widget", "om_widget_tab", "om_dashboard",
    ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_widget",
    "class" => "om_widget",
    "title" => _("widgets"),
    "description" => _("Paramétrage des blocs d'informations affichables sur le tableau de bord."),
    "right" => array("om_widget", "om_widget_tab", ),
    "open" => array("tab.php|om_widget", "form.php|om_widget", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=om_dashboard&amp;idx=0&amp;action=4",
    "class" => "om_dashboard",
    "title" => _("composition"),
    "description" => _("Composition des tableaux de bord par profil."),
    "right" => array("om_dashboard", ),
    "open" => array("tab.php|om_dashboard", "form.php|om_dashboard", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("sig"),
    "right" => array(
        "om_sig_map", "om_sig_map_tab", "om_sig_flux", "om_sig_flux_tab", "om_sig_extent", "om_sig_extent_tab",
    ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array(
        "om_sig_map", "om_sig_map_tab", "om_sig_flux", "om_sig_flux_tab", "om_sig_extent", "om_sig_extent_tab",
    ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_extent",
    "class" => "om_sig_extent",
    "title" => _("om_sig_extent"),
    "description" => _("XXX."),
    "right" => array("om_sig_extent", "om_sig_extent_tab", ),
    "open" => array("tab.php|om_sig_extent", "form.php|om_sig_extent", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_map",
    "class" => "om_sig_map",
    "title" => _("om_sig_map"),
    "description" => _("XXX."),
    "right" => array("om_sig_map", "om_sig_map_tab", ),
    "open" => array("tab.php|om_sig_map", "form.php|om_sig_map", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);
//
$links[] = array(
    "href" => "../scr/tab.php?obj=om_sig_flux",
    "class" => "om_sig_flux",
    "title" => _("om_sig_flux"),
    "description" => _("XXX."),
    "right" => array("om_sig_flux", "om_sig_flux_tab", ),
    "open" => array("tab.php|om_sig_flux", "form.php|om_sig_flux", ),
    "parameters" => array("option_localisation" => "sig_interne", ),
);



//
$links[] = array(
    "class" => "category",
    "title" => _("options avancées"),
    "right" => array("import", "gen", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("dossier_coordination_geocoder_tous", "dossier_coordination_geocoder_tous_geocoder", ),
    "parameters" => array("option_sig" => "sig_externe", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=dossier_coordination_geocoder_tous&action=260&idx=0",
    "class" => "dossier_coordination_geocoder_tous",
    "title" => _("géolocalisation"),
    "description" => _("Géolocaliser tous les dossiers de coordination et établissements."),
    "right" => array("dossier_coordination_geocoder_tous", "dossier_coordination_geocoder_tous_geocoder", ),
    "open" => array("form.php|dossier_coordination_geocoder_tous", ),
    "parameters" => array("option_sig" => "sig_externe", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("import", ),
);
//
$links[] = array(
    "href" => "../scr/import.php",
    "class" => "import",
    "title" => _("import"),
    "description" => _("Ce module permet l'intégration de données dans l'application depuis des fichiers CSV."),
    "right" => array("import", ),
    "open" => array("import.php|", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("settings", ),
);
//
$links[] = array(
    "href" => "../app/settings.php?controlpanel=interface_avec_le_referentiel_ads",
    "class" => "interface_avec_le_referentiel_ads",
    "title" => _("interface avec le réferentiel ADS"),
    "description" => _("Gère les échanges entre les applications openARIA et openADS."),
    "right" => array("settings", ),
    "open" => array("settings.php|", ),
);
//
$links[] = array(
    "href" => "../scr/form.php?obj=dossier_coordination_message&action=99&idx=0",
    "class" => "controlpanel_messages",
    "title" => _("messages"),
    "description" => _("Ce module permet d'accéder à des statistiques sur les messages et de lancer des traitements."),
    "right" => array("dossier_coordination_message_controlpanel", ),
    "open" => array("settings.php|", ),
);
//
$links[] = array(
    "title" => "<hr/>",
    "right" => array("gen", ),
);
//
$links[] = array(
    "title" => _("generateur"),
    "href" => "../scr/gen.php",
    "class" => "generator",
    "description" => _("Ce module permet la génération d'éléments à partir du modèle de données."),
    "right" => array("gen", ),
    "open" => array(
        "gen.php|","genauto.php|", "gensup.php|", "genfull.php|",
        "genetat.php|", "gensousetat.php|", "genlettretype.php|",
        "genimport.php|",
    ),
);



//
$rubrik['links'] = $links;
//
$menu[] = $rubrik;
// }}}

?>
