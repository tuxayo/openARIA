<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_table pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_table.class.php 2051 2013-01-24 14:44:09Z jbastide $
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_table.class.php";

/**
 *
 */
class om_table extends table {

    function process_global_action_validity() {
        //
        if ($this->_om_validite == true) { // si c'est un établissement

            if ($this->params['obj'] == 'etablissement_tous'
                || $this->params['obj'] == 'etablissement_referentiel_erp') {

                if ($this->getParam('valide') == 'false') {
                    $om_validite_message = _("Masquer les elements archives");
                    $params['valide'] = 'true';
                } else {
                    $om_validite_message = _("Afficher les elements archives");
                    $params['valide'] = 'false';
                }
                //
                $action = array(
                    "type" => "om_validite",
                    "class" => "om_validite",
                    "link" => $this->composeURL($params),
                    "title" => $om_validite_message,
                );
                //
                $this->add_action_to_global_actions($action);
            } else { // sinon c'est une table de référence
                //
                if ($this->getParam('valide') == 'false') {
                    $om_validite_message = _("Masquer les elements expires");
                    $params['valide'] = 'true';
                } else {
                    $om_validite_message = _("Afficher les elements expires");
                    $params['valide'] = 'false';
                }
                //
                $action_id = "action-";
                if ($this->getParam('onglet') === false) {
                    $action_id .= "tab-";
                } else {
                    $action_id .= "soustab-";
                }
                $action_id .= $this->getParam("obj")."-global-om_validite-".$params['valide'];
                //
                $action = array(
                    "id" => $action_id,
                    "type" => "om_validite",
                    "class" => "om_validite",
                    "link" => $this->composeURL($params),
                    "title" => $om_validite_message,
                );
                //
                $this->add_action_to_global_actions($action);
            }            
        }
    }


}

?>
