<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../core/obj/om_profil.class.php";

class om_profil extends om_profil_core {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setType($form, $maj) {

        parent::setType($form, $maj);

        //
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            // Liaison NaN
            $form->setType("permissions", "checkbox_multiple");
        }
        //
        if ($this->getParameter("maj") == 2 || $this->getParameter("maj") == 3) {
            // Liaison NaN
            $form->setType("permissions", "select_multiple_static");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $this->getParameter("maj"), $dnu1, $dnu2);

        // Liaison NaN - reunion_type/reunion_categorie
        $sql_permissions = "
        SELECT
            om_permission.libelle,
            om_permission.libelle as lib
        FROM ".DB_PREFIXE."om_permission
        ORDER BY lib";
        $sql_permissions_by_id = "
        SELECT
            om_permission.libelle,
            om_permission.libelle as lib
        FROM ".DB_PREFIXE."om_permission
        WHERE om_permission.libelle IN (<idx>)
        ORDER BY lib";

        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "permissions", $sql_permissions, $sql_permissions_by_id, false, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["permissions"][0]);
            array_shift($form->select["permissions"][1]);
        }

    }


    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "om_droit" => array(
            "table_l" => "om_droit",
            "table_f" => "om_permission",
            "field" => "permissions",
        ),
    );

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(_("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(_("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }
    }

    /**
     *
     * @return bool
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            // XXX $table_f a été remplacé par libelle, il faut modifier
            // le tableau de définition liaison_nan pour prendre en compte le 
            // détail table/field
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                "libelle" => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            require_once "../obj/".$table_l.".class.php";
            $obj_l = new $table_l("]");
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }


}

?>
