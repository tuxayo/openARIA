<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui ne sont
 * pas liés à un etab/DC/DI.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/piece.class.php");

class piece_bannette extends piece {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }
    /**
     * Mutateur.
     *
     * Permet d'effectuer des appels aux mutateurs spécifiques sur le formulaire
     * de manière fonctionnelle et non en fonction du mutateur. Exemple : au lieu 
     * de gérer le champ service dans les méthodes setType, setSelect, le setLib, 
     * ... Nous allons les gérer dans cette méthode et appeler tous les mutateurs
     * à la suite.
     * 
     * @param resource $form Instance formulaire.
     * @param integer  $maj  Clé de l'action.
     *
     * @return void
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        //  Si utilisateur sans service et mode modifier
        if (is_null($_SESSION['service']) 
            && $this->getParameter("maj") == 1) {
            // On peut rechanger de service
            $form->setType("service", "select");
            // service
        	$this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
        }
    }
}// fin classe

?>
