<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/prescription.class.php";

class prescription extends prescription_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Retourne un tableau de prescriptions pour une analyse donnée
     * ou une chaîne 'Aucune' si aucun résultat.
     * 
     * @param  [integer] $idx Valeur de la clé primaire de l'analyse
     * @return [string]       Code HTML du tableau
     */
    function get_prescription_from_analyse($idx) {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line = '
        <tr>
            <td rowspan="2" style="width:5%%;text-align:center;%1$s">%2$s</td>
            <td style="width:95%%;%1$s">%3$s</td>
        </tr>
        <tr>
            <td style="width:95%%;%1$s">%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "border: 0.5px solid #000;";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "background-color: #F9F9F9;";
        $css_bg_line_even = "";

        /**
         * Récupération des prescriptions
         */
        // Requête
        $sql = "
        SELECT
            ordre as ordre,
            CASE WHEN prescription_reglementaire IS NULL
                THEN
                    ''
                ELSE
                    pr_description_om_html
            END as prescription_reglementaire,
            ps_description_om_html as prescription_specifique
        FROM 
            ".DB_PREFIXE."prescription
        WHERE
            analyses = ".intval($idx)."
        ORDER BY 
            ordre ASC
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Traitement des erreurs de base de données
        if ($this->f->isDatabaseError($res, true)) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if ($res->numRows() == 0) {
            return _("Aucune");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                $row["ordre"],
                $row["prescription_reglementaire"],
                $row["prescription_specifique"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }

    /**
     * Retourne une liste de prescriptions défavorables pour une analyse donnée
     * ou une chaîne 'Aucune' si aucun résultat.
     * 
     * @param  [integer] $idx Valeur de la clé primaire de l'analyse
     * @return [string]       Code HTML du tableau
     */
    function get_prescription_defavorable_from_analyse($idx) {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line = '
        <tr>
            <td rowspan="2" style="width:5%%;text-align:center;%1$s">%2$s</td>
            <td style="width:95%%;%1$s">%3$s</td>
        </tr>
        <tr>
            <td style="width:95%%;%1$s">%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "";
        $css_bg_line_even = "";

        /**
         * Récupération des prescriptions
         */
        // Requête
        $sql = "
        SELECT
            ordre as ordre,
            CASE WHEN prescription_reglementaire IS NULL
                THEN
                    ''
                ELSE
                    pr_description_om_html
            END as prescription_reglementaire,
            ps_description_om_html as prescription_specifique
        FROM 
            ".DB_PREFIXE."prescription
        WHERE
            analyses = ".intval($idx)."
            AND prescription.pr_defavorable IS TRUE
        ORDER BY 
            ordre ASC
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Traitement des erreurs de base de données
        if ($this->f->isDatabaseError($res, true)) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if ($res->numRows() == 0) {
            return _("Aucune");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                "-",
                $row["prescription_reglementaire"],
                $row["prescription_specifique"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }
}

?>
