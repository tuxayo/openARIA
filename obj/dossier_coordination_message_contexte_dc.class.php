<?php
/**
 * Surcharge de la classe dossier_coordination_message.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/dossier_coordination_message.class.php";

class dossier_coordination_message_contexte_dc extends dossier_coordination_message {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode
        // d'initialisation de la classe parente
        parent::init_class_actions();
        // ACTION - 021 - redirect_context_dc
        // Redirection vers la vue consulter du message dans le contexte de son
        // dossier de coordination
        $this->class_actions[21] = array(
            "identifier" => "redirect_context_dc",
            "view" => "view_redirect_context_dc",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * VIEW - view_redirect_context_dc.
     *
     * @return void
     */
    function view_redirect_context_dc() {
        //
        $this->checkAccessibility();
        // Si il n'y a pas de DC lié alors on renvoi vers la vue dans le
        // contexte du message
        $inst_dc = $this->get_inst_dossier_coordination();
        if ($inst_dc->getVal($inst_dc->clePrimaire) === "") {
            $redirect =  sprintf(
                "../scr/form.php?obj=dossier_coordination_message_tous&idx=%s&action=3",
                $this->getVal($this->clePrimaire)
            );
            header("Location: ".$redirect);
            die();
        }
        // Si il y a un DC lié alors on renvoi vers la vue dans le contexte du
        // DC
        $redirect = sprintf(
            "../spg/direct_link.php?obj=dossier_coordination&idx=%s&action=3&direct_form=dossier_coordination_message_contexte_dc&direct_idx=%s&direct_action=3",
            $inst_dc->getVal($inst_dc->clePrimaire),
            $this->getVal($this->clePrimaire)
        );
        header("Location: ".$redirect);
        die();
    }

}

?>
