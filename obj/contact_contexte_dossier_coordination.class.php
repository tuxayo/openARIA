<?php
/**
 * Surcharge de la classe contact pour gérer les spécificités du contexte
 * dossier de coordination.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../obj/contact.class.php";

class contact_contexte_dossier_coordination extends contact {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

    function setType(&$form,$maj) {
        parent::setType($form,$maj);
                $form->setType('reception_convocation', 'hidden');
    }

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "lien_dossier_coordination_contact" => array(
            "table_l" => "lien_dossier_coordination_contact",
            "table_f" => "dossier_coordination",
            "field" => "dossier_coordination",
        ),
    );

    /**
     * Ajouter une liaison NàN entre deux tables.
     *
     * @param string $table_l Table de liaison
     * @param string $table_f Table cible
     * @param string $field   Champ de la table à liée
     *
     * @return integer Nombre de lien crée
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field, $values=null) {
        //
        $multiple_values = array();
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } elseif ($values != null) {
            //
            $multiple_values = $values;
            // Si ce n'est pas un tableau
            if (!is_array($values)) {
                //
                $multiple_values = explode(";", $multiple_values);
            }
        }

        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            require_once "../obj/".$table_l.".class.php";
            $obj_l = new $table_l("]");
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le 
     * générateur en devant surcharger la méthode cleSecondaire afin de supprimer
     * les éléments liés dans les tables NaN.
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaison NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $this->getParameter("idxformulaire")
            );
        }
        //
        return true;
    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }
        //
        return true;
    }

}

?>
