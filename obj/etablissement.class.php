<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once ("../gen/obj/etablissement.class.php");

class etablissement extends etablissement_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    var $inst_voie = null;
    var $inst_exploitant = null;
    var $tot_ua_valid = null;
    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "etablissement",
        "geom_point",
        "geom_emprise",
    );

    /**
     *
     */
    function get_inst_exploitant($exploitant = null) {
        //
        if (!is_null($exploitant)) {
            //
            require_once "../obj/contact.class.php";
            return new contact($exploitant);
        }
        //
        if (is_null($this->inst_exploitant)) {
            //
            $exploitant = $this->recuperer_ID_exploitant($this->getVal($this->clePrimaire));
            // Si l'établissement n'a pas d'exploitant
            if ($exploitant == "]") {
                $exploitant = 0;
            }
            //
            require_once "../obj/contact.class.php";
            $this->inst_exploitant = new contact($exploitant);
        }
        //
        return $this->inst_exploitant;
    }

    /**
     *
     */
    function get_inst_voie($voie = null) {
        //
        if (is_null($this->inst_voie)) {
            //
            if (is_null($voie)) {
                $voie = $this->getVal("adresse_voie");
            }
            //
            require_once "../obj/voie.class.php";
            $this->inst_voie = new voie($voie);
        }
        //
        return $this->inst_voie;
    }

    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $ua = "";
        require_once "../obj/etablissement_unite.class.php";
        $inst_ua = new etablissement_unite(0);
        $ua = $inst_ua->get_display_for_merge_field(array(
            "obj" => "etablissement", 
            "idx" => $this->getVal("etablissement"),
        ));
        $values["etablissement.ua_validees"] = $ua;
        //
        $inst_voie = $this->get_inst_voie();
        $values["etablissement.adresse_voie"] = $inst_voie->getVal("libelle");
        //
        $values["etablissement.si_derniere_visite_avis"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_derniere_visite_avis",
            $this->getVal("si_derniere_visite_avis")
        );
        //
        $values["etablissement.si_dernier_plan_avis"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_dernier_plan_avis",
            $this->getVal("si_dernier_plan_avis")
        );
        //
        $values["etablissement.si_autorite_competente_visite"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_autorite_competente_visite",
            $this->getVal("si_autorite_competente_visite")
        );
        //
        $values["etablissement.si_autorite_competente_plan"] = $this->f->get_label_of_foreign_key(
            "etablissement",
            "si_autorite_competente_plan",
            $this->getVal("si_autorite_competente_plan")
        );
        // Ajout du champ "type(s) secondaire(s)"
        $field_type_sec = "etablissement.etablissement_type_secondaire";
        $id_etablissement = $this->getVal($this->clePrimaire);
        $values[$field_type_sec] = "";
        // si un établissement est instancié
        if (!empty($id_etablissement)) {
            $sql_type_sec = "SELECT
                array_to_string(
                    array_agg(
                        lien.etablissement_type
                        ORDER BY type.libelle
                    ), ';'
                ) as etablissement_type_secondaire
                FROM ".DB_PREFIXE."lien_etablissement_e_type lien
                LEFT JOIN ".DB_PREFIXE."etablissement_type type
                    ON lien.etablissement_type = type.etablissement_type
                WHERE lien.etablissement = ".$id_etablissement;
            $this->f->addToLog("get_libelle() db->getOne(\"".$sql_type_sec."\");",
                VERBOSE_MODE);
            $values[$field_type_sec] = $this->f->db->getOne($sql_type_sec);
            $this->f->isDatabaseError($values[$field_type_sec]);
            if (!empty($values[$field_type_sec])) {
                // Transformation du string en array
                $array = array();
                $array = explode(';', $values[$field_type_sec]);
                // Remplacement de l'ID du type par son code
                foreach ($array as $key => $id) {
                    $array[$key] = strtoupper($this->f->get_label_of_foreign_key("etablissement","etablissement_type",$id));
                }
                // Remplacement de la valeur du champ de fusion
                $values[$field_type_sec] = implode(',', $array);
            }
        }
        // Gestion de l'exploitant
        $inst_expl = $this->get_inst_exploitant();
        $contact_values = $inst_expl->get_values_merge_fields();
        foreach ($contact_values as $key => $value) {
            $new_key = str_replace("contact.", "exploitant.", $key);
            $exploitant_values[$new_key] = $value;
        }
        $values = array_merge(
            $values,
            $exploitant_values
        );
        // Si clé primaire existante on la transmet pour la gestion spécifique des contraintes
        if ($this->getVal($this->clePrimaire) != '') {
            $values['etablissement.etablissement'] = $this->getVal($this->clePrimaire);
        }
        //
        return $values;
    }


    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        // Ajout types secondaires
        $labels[_("etablissement")]["etablissement.etablissement_type_secondaire"] = _("type(s) secondaire(s)");
        $labels[_("etablissement")]["etablissement.ua_validees"] = _("liste des UA validées");
        // Gestion de l'exploitant
        $inst_expl = $this->get_inst_exploitant();
        $contact_labels = $inst_expl->get_labels_merge_fields();
        foreach ($contact_labels[_("contact")] as $key => $value) {
            $new_key = str_replace("contact.", "exploitant.", $key);
            $exploitant_labels[$new_key] = $value;
        }
        $labels[_("etablissement")] = array_merge(
            $labels[_("etablissement")],
            $exploitant_labels
        );
        //
        return $labels;
    }

    /**
     * Surcharge du fil d'ariane.
     *
     * @param string $ent Chaîne initiale
     *
     * @return chaîne de sortie
     */
    function getFormTitle($ent) {
        if ($this->getParameter("maj") == 0) {
            return $ent." -> +";
        } else {
            return $ent." -> ".$this->getVal("code")." - ".$this->getVal("libelle");
        }
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 004 - contrainte
        //
        $this->class_actions[4] = array(
            "identifier" => "contrainte",
            "view" => "view_contrainte",
            "permission_suffix" => "contrainte",
        );

        // ACTION - 005 - archiver
        //
        $this->class_actions[5] = array(
            "identifier" => "archiver",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("archiver"),
                "order" => 40,
                "class" => "zip-16",
            ),
            "view" => "formulaire",
            "method" => "archiver",
            "button" => "archiver",
            "permission_suffix" => "archiver",
            "condition" => "is_archivable",
        );

        // ACTION - 006 - desarchiver
        //
        $this->class_actions[6] = array(
            "identifier" => "desarchiver",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("desarchiver"),
                "order" => 50,
                "class" => "unzip-16",
            ),
            "view" => "formulaire",
            "method" => "desarchiver",
            "button" => "desarchiver",
            "permission_suffix" => "desarchiver",
            "condition" => "is_desarchivable",
        );

        // ACTION - 007 - masquer_si_periodicite_visites
        //
        $this->class_actions[7] = array(
            "identifier" => "masquer_si_periodicite_visites",
            "view" => "masquer_si_periodicite_visites",
            "permission_suffix" => "json",
        );
        
        // ACTION - 009 - view_data_etablissement
        //
        $this->class_actions[9] = array(
            "identifier" => "data_etablissement",
            "view" => "view_data_etablissement",
            "permission_suffix" => "json",
        );

        // ACTION - 015 - view_synthesis
        //
        $this->class_actions[15] = array(
            "identifier" => "synthesis",
            "view" => "view_synthesis",
            "permission_suffix" => "consulter",
        );

        // ACTION - 015 - view_lien_referentiel_patrimoine
        //
        $this->class_actions[16] = array(
            "identifier" => "lien_referentiel_patrimoine",
            "view" => "view_lien_referentiel_patrimoine",
            "permission_suffix" => "consulter",
        );

        // ACTION - 020 - view_ua_tab
        //
        $this->class_actions[20] = array(
            "identifier" => "ua-tab",
            "view" => "view_ua_tab",
            "permission_suffix" => "consulter",
        );

        // ACTION - 030 - Redirection vers le SIG
        $this->class_actions[30] = array(
            "identifier" => "localiser",
            "view" => "view_localiser",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 032 - geocoder
        //
        $this->class_actions[32] = array(
            "identifier" => "geocoder",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("géolocaliser"),
                "order" => 110,
                "class" => "sig-16",
            ),
            "view" => "formulaire",
            "method" => "geocoder",
            "permission_suffix" => "consulter",
            "condition" => array(
                "is_not_geolocalised",
                "is_option_sig_enabled",
            ),
        );

        // ACTION - 033 - plot_owner
        //
        $this->class_actions[33] = array(
            "identifier" => "plot_owner",
            "view" => "view_plot_owner",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

    }

    /**
     * VIEW - view_ua_tab.
     *
     * @return void
     */
    function view_ua_tab() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $this->f->displaySubTitle("-> "._("unités d'accessibilité"));
        //
        printf('
<div id="ua-tabs">
<ul>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_valide" href="../scr/soustab.php?obj=etablissement_unite__contexte_etab__ua_valide&retourformulaire=etablissement&idxformulaire=%1$s">Validées</a>
</li>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_enprojet" href="../scr/soustab.php?obj=etablissement_unite__contexte_etab__ua_enprojet&retourformulaire=etablissement&idxformulaire=%1$s">En projet</a>
</li>
<li>
    <a id="onglet-etablissement_unite__contexte_etab__ua_archive" href="../scr/soustab.php?obj=etablissement_unite__contexte_etab__ua_archive&retourformulaire=etablissement&idxformulaire=%1$s">Archivées</a>
</li>
</ul>
</div>
',
            $this->getVal("etablissement")
        );
    }

    /**
     * VIEW - view_lien_referentiel_patrimoine.
     *
     * @return void
     */
    function view_lien_referentiel_patrimoine() {
        // Données
        $references_cadastrale = (isset($_GET['ref_cad']) ? $_GET['ref_cad'] : "" );

        //Si l'identifiant a bien été fourni
        if( isset($references_cadastrale) && $references_cadastrale != "" ) {
            
            //Données à envoyer
            $parcelles = $this->f->parseParcelles($references_cadastrale);
            //
            $parcellesWS = "";
            //Formatage des références cadastrale pour l'envoi
            foreach ($parcelles as $parcelle) {

                $parcellesWS .= "parcelle=";
                //On ajoute les données dans le tableau que si quartier + section + parcelle
                //ont été fournis
                if ($parcelle["quartier"]!==""&&$parcelle["section"]!==""&&$parcelle["parcelle"]!=="") {

                    //On récupère le code impôts de l'arrondissement lié au quartier
                    $sql = "SELECT arrondissement.code_impots 
                            FROM ".DB_PREFIXE."arrondissement
                            LEFT JOIN ".DB_PREFIXE."quartier
                            ON quartier.arrondissement = arrondissement.arrondissement
                            WHERE quartier.code_impots = '".$this->f->db->escapeSimple($parcelle['quartier'])."'";
                    $this->f->addToLog("lien_referentiel_patrimoine.php : ".$sql." execute <br>", EXTRA_VERBOSE_MODE);

                    $res = $this->f->db->getOne($sql);
                    $error = $this->f->isDatabaseError($res, 'true');
                    if ( $error === true ) {
                        $this->f->displayMessage("error", _("Erreur de base de donnees. Contactez votre administrateur."));
                    }
                    //
                    $parcellesWS .= $res.$parcelle["quartier"]."%20".$parcelle["section"].$parcelle["parcelle"]."&";
                }
            }

            if (file_exists("../dyn/services.inc.php")) {
                include "../dyn/services.inc.php";
            }
            $link = "";
            if (isset($URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT)
                && $URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT !== "") {
                $link = $URL_REFERENTIEL_PATRIMOINE_EQUIPEMENT."?".$parcellesWS;
            }

            require_once '../services/outgoing/MessageSenderRest.class.php';
            $messageSenderRest = new MessageSenderRest($link);
            
            //Exécution de la requête
            $refs_patrimoine = $messageSenderRest->execute('GET', 'text/html','');
            
            //Affichage du résultat
            $this->f->setTitle(_("Établissement")." -> "._("Références patrimoine"));
            $this->f->displayTitle();
            //
            $description = _("Cette page vous permet de lier des références "
                    . "patrimoine à un établissement");
            //
            $this->f->displayDescription($description);
            // Ouverture du formulaire
            printf("<form name=f2>");
            //Bouton retour
            $retour = "<a "
                    . "onclick=\"$('#dialog').dialog('close');\" "
                    . "href=\"#\" "
                    . "class=\"retour\">"
                    . _('Retour')
                    ."</a>";
            //Si la récupération s'est bien passée
            if ($messageSenderRest->getResponseCode() == 200){
                //S'il n'y a pas de référence patrimoine à afficher
                if (count($refs_patrimoine)==0){
                    $this->f->displayMessage("error", _("Aucune référence patrimoine liée."));
                }
                else {
                    printf("<div class=\"formControls\">");
                    // Bouton retour
                    printf($retour);
                    printf("</div>");
                    //
                    printf("<div class=\"formEntete ui-corner-all\">");
                    //Affichage des références patrimoine
                    printf("<div>"._("Liste des références patrimoine trouvées :")."</div>");
                    //
                    $template_patrimoine_liste = '
<table class="table">
    %s
</table>';
                    //
                    $template_patrimoine_elem = '
<tr>
    <td>
        <input type="checkbox" name="ref_patrimoine[]" value="%1$s" />
    </td>
    <td>
       <i title="%6$s">%1$s</i> 
       / <span title="%7$s">%2$s</span> / <span title="%8$s"><b>%3$s</b></span>
       <br/>
       <span title="%9$s">%4$s</span> / <i title="%10$s">%5$s</i>
    </td>
</tr>
                    ';
                    //
                    $patrimoine_liste_content = "";
                    //
                    foreach ($refs_patrimoine as $ref_patrimoine) {
                        //
                        $patrimoine_liste_content .= sprintf(
                            $template_patrimoine_elem,
                            $ref_patrimoine['idEquipement'],
                            $ref_patrimoine['nature'],
                            $ref_patrimoine['nom'],
                            $ref_patrimoine['adresse'],
                            $ref_patrimoine['affectataire'],
                            _("numéro d'équipement"),
                            _("catégorie d'équipement patrimoine"),
                            _("nom de l'équipement"),
                            _("adresse de l'équipement"),
                            _("affectataire")
                        );
                    }
                    //
                    printf(
                        $template_patrimoine_liste,
                        $patrimoine_liste_content
                    );
                    //
                    printf("</div>");
                    // Bouton "Valider"
                    printf("<div class=\"formControls\">");
                    printf("<input "
                            . "id=\"button-lien_referentiel_patrimoine-valider\" "
                            . "type=\"button\" "
                            . "class=\"om-button ui-button ui-widget ui-state-default ui-corner-all\" "
                            . 'onclick="getRefPatrimoine();" '
                            . "value=\"Valider\" "
                            . "role=\"button\" "
                            . "aria-disabled=\"false\">");
                    // Bouton retour
                    printf($retour);
                    printf("</div>");
                    return;
                }
            }
            else {
                $this->addToLog(
                    __METHOD__."() : Erreur de communication avec le référentiel patrimoine (coderet:".$messageSenderRest->getResponseCode().")",
                    DEBUG_MODE
                );
                $this->f->displayMessage("error", _("Une erreur s'est produite lors de la "
                        . "récupération des références patrimoine. Contactez votre "
                        . "administrateur"));
            }
            printf("</div>");
            printf("<div class=\"formControls\">");
            // Bouton retour
            printf($retour);
            printf("</div>");
            // Fermeture du formulaire
            printf("</form>");
        }
        else {
            printf (_("Aucune référence cadastrale fournie"));
        }
    }

    /**
     * VIEW - masquer_si_periodicite_visites.
     * 
     * Retourne en JSON si on doit masquer les champs de périodicité
     * suivant la nature et le type postés.
     *
     * @return void
     */
    function masquer_si_periodicite_visites() {
        //
        $this->f->disableLog();
        //
        $postvar = $this->getParameter("postvar");
        $nature = $postvar['id_nature'];
        $type = $postvar['id_type'];
        // Traitement du type de l'établissement
        $erpr = false;
        if ($nature != 'none') {
            $code_etablissement_nature = $this->get_code_etablissement_nature($nature);
            if ($code_etablissement_nature == 'erpr') {
                $erpr = true;
            }
        }
        // Traitement de la nature de l'établissement
        $plein_air = false;
        if ($type != 'none') {
            $code_etablissement_type = $this->get_code_etablissement_type($type);
            if ($code_etablissement_type == 'pa') {
                $plein_air = true;
            }
        }
        // si nature ERP différente de référentiel ou si de type plein air
        // alors on masque les champs de périodicité des visites
        if (($erpr == false) || ($plein_air == true)) {

            echo json_encode(array(
                "resultat"=>"masquer"
                )
            );
        } else {

            echo json_encode(array(
                "resultat"=>"afficher"
                )
            );
        }
    }


    /**
     * Retourne le code de la nature de l'établissement passée en paramètre.
     * 
     * @param string $id Identifiant de la nature de l'etablissement.
     * 
     * @return string Code de la nature de l'établissement
     */
    function get_code_etablissement_nature($id) {

        $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_nature
                WHERE etablissement_nature = ".$id;
        $this->f->addToLog("get_code_etablissement_nature() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $code = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($code);
        return $code;
    }

    /**
     * Retourne le libellé du type de l'établissement passé en paramètre.
     * 
     * @param string $id Identifiant du type de l'établissement
     * 
     * @return string Libellé du type de l'établissement
     */
    function get_code_etablissement_type($id) {

        $sql = "SELECT LOWER(libelle)
                FROM ".DB_PREFIXE."etablissement_type
                WHERE etablissement_type = ".$id;
        $libelle = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($libelle);
        return $libelle;
    }

    /**
     * Permet de définir le type des champs
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        // En mode "ajout" et "modifier"
        if ($maj == 0 || $maj == 1) {
            // Champ en autocomplete
            $form->setType('adresse_voie', 'autocomplete');
            $form->setType('geolocalise', 'hidden');
        }
        if ($maj == 0) {
            $form->setType('code', 'hidden');
            $form->setType('si_type_ssi','select');
            $form->setType('si_type_alarme','select');
        }
        if ($maj == 1) {
            $form->setType('code', 'hiddenstatic');
            $form->setType('si_type_ssi','select');
            $form->setType('si_type_alarme','select');
        }

        //
        $form->setType('geom_point', 'hidden');
        $form->setType('geom_emprise', 'hidden');

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // En mode "ajouter" et "modifier"
        if ($maj == 0 || $maj == 1) {
            // Initialise les paramétres
            $params = array();
            // Surcharge visée pour l'ajout
            $params['obj'] = "voie";
            // Table de l'objet
            $params['table'] = "voie";
            // Permission d'ajouter
            $params['droit_ajout'] = false;
            // Critères de recherche
            $params['criteres'] = array(
                "voie.voie" => _("identifiant"),
                "voie.libelle" => _("libelle"),
                "voie.rivoli" => _("rivoli")
            );
            //
            $where_tmp = "";
            if ($maj == 1) {
                $adresse_voie = $this->getVal("adresse_voie");
                if (!empty($adresse_voie)) {
                    $where_tmp = " OR voie.voie = ".intval($adresse_voie)." ";
                }
            }
            $params['where'] = " 
            ((
                (
                    voie.om_validite_debut IS NULL 
                    AND (
                        voie.om_validite_fin IS NULL 
                        OR voie.om_validite_fin > CURRENT_DATE
                        )
                ) 
                OR (
                    voie.om_validite_debut <= CURRENT_DATE 
                    AND (
                        voie.om_validite_fin IS NULL 
                        OR voie.om_validite_fin > CURRENT_DATE
                        )
                    )
            )".$where_tmp.")
            ";

            // Tables liées
            $params['jointures'] = array();
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            $params['identifiant'] = "voie.voie";
            $params['libelle'] = array (
                "voie.libelle"
            );
            // Envoi des paramètres
            $form->setSelect("adresse_voie", $params);

            // Initialise le select en fonction de la valeur d'un autre champ
            $form->setSelect('adresse_arrondissement', $this->load_select_adresse_arrondissement($form, $maj, "adresse_voie"));

            /* Type SSI */
            $ssi = array();
            $ssi[0][0] = 'NC';
            $ssi[1][0] = 'NC';
            $ssi[0][1] = 'A';
            $ssi[1][1] = 'A';
            $ssi[0][2] = 'B';
            $ssi[1][2] = 'B';
            $ssi[0][3] = 'C';
            $ssi[1][3] = 'C';
            $ssi[0][4] = 'D';
            $ssi[1][4] = 'D';
            $ssi[0][5] = 'E';
            $ssi[1][5] = 'E';
            $form->setSelect("si_type_ssi", $ssi);

            /* Type SSI */
            $alarme = array();
            $alarme[0][0] = 'NC';
            $alarme[1][0] = 'NC';
            $alarme[0][1] = '1';
            $alarme[1][1] = '1';
            $alarme[0][2] = '2a';
            $alarme[1][2] = '2a';
            $alarme[0][3] = '2b';
            $alarme[1][3] = '2b';
            $alarme[0][4] = '3';
            $alarme[1][4] = '3';
            $alarme[0][5] = '4';
            $alarme[1][5] = '4';
            $form->setSelect("si_type_alarme", $alarme);
        }

        // Si formulaire en consultation et sig activé
        if ($this->getParameter('maj') === '3'
            && $this->is_option_sig_enabled() === true) {
            // Paramètre obligatoires pour l'affichage de la liste des
            // propriétaires
            $params = array();
            $params['obj'] = get_class($this);
            $params['idx'] = $this->getParameter('idx');
            $params['action'] = '33';
            $params['title'] = _('Liste des propriétaires');
            //
            $form->setSelect('references_cadastrales', $params);
        }
    }

    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        parent::triggerajouter($id, $dnu1, $val, $dnu2);
        // Récupération des id demandeurs postés
        $this->valF["code"] = $this->f->getParameter("etablissement_code_prefixe").
            $this->valF["etablissement"];
    }


    /**
     * Charge le select du champ "adresse_arrondissement" par rapport au
     * champ "adresse_voie".
     * 
     * @param  object $form  Formulaire
     * @param  int    $maj   Mode d'insertion
     * @param  string $champ Champ activant le filtre
     * @return array         Contenu du select
     */
    function load_select_adresse_arrondissement(&$form, $maj, $champ) {
        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        // Contenu du select
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = _('choisir')." "._("adresse_arrondissement");

        // Récupère l'id de la voie
        $adresse_voie = "";
        if (isset($_POST[$champ])) {
            $adresse_voie = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $adresse_voie = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $adresse_voie = $form->val[$champ];
        }

        // S'il y a une voie
        if (!empty($adresse_voie)) {
            // Requête SQL
            $sql_adresse_arrondissement_by_adresse_voie = str_replace('<idx_voie>', $adresse_voie, $sql_adresse_arrondissement_by_adresse_voie);
            $res = $this->f->db->query($sql_adresse_arrondissement_by_adresse_voie);
            // Log
            $this->addToLog(__METHOD__."() db->query(\"".$sql_adresse_arrondissement_by_adresse_voie."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            //Les résultats de la requête sont stocké dans le tableau contenu
            $k=1;
            while ($row=& $res->fetchRow()){
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            } 
        }
        //
        return $contenu;
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object  &$form Formumaire
     * @param integer $maj   Mode d'insertion
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);

        //
        $form->setOnchange('adresse_voie',
            'filterSelect(this.value,
            \'adresse_arrondissement\',
            \'adresse_voie\',
            \'etablissement\')');
    }

    /**
     *
     */
    function is_archivable() {
        //
        $om_validite_fin = $this->getVal("om_validite_fin");
        // Vérification de l'état d'archivage
        // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
        if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     */
    function is_desarchivable() {
        //
        $om_validite_fin = $this->getVal("om_validite_fin");
        // Vérification de l'état d'archivage
        // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
        if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * TREATMENT - archiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function archiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("archive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - desarchiver.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function desarchiver($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_archiving("unarchive", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_archiving($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "archive" && $mode != "unarchive") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "archive") {
            // Date pour la bdd et pour l'affichage
            $om_validite_fin_bdd = date("Y-m-d");
            $om_validite_fin_aff = date("d/m/Y");
            // Valeurs à modifier
            $valF = array(
                "om_validite_fin" => $om_validite_fin_bdd,
            );
            //
            $valid_message = _("Archivage correctement effectue.");
        } elseif ($mode == "unarchive") {
            // Valeurs à modifier
            $valF = array(
                "om_validite_fin" => null,
            );
            //
            $valid_message = _("Desarchivage correctement effectue.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * Récupère le code de la nature de l'établissement par rapport à
     * l'établissement.
     *
     * @param integer $id Identifiant de l'établissement
     *
     * @return string Code de la nature de l'établissement
     */
    function get_etablissement_nature_code_by_etablissement($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si contact_type n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "
                SELECT LOWER(etablissement_nature.code)
                FROM ".DB_PREFIXE."etablissement_nature
                    LEFT JOIN ".DB_PREFIXE."etablissement
                        ON etablissement.etablissement_nature = etablissement_nature.etablissement_nature
                WHERE etablissement.etablissement = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    /**
     * Récupère les données de l'établissement.
     *
     * @param integer $id Identifiant de l'établissement
     *
     * @return array Liste de données
     */
    function get_data($id) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Initialisation de la vatiable de résultat
        $values = array();

        // Si l'identifiant est renseigné
        if (!empty($id)) {
            // Récupère les données nécessaires au type spécifique etablissement
            $values = array(
                "code" => $this->getVal("code"),
                "libelle" => $this->getVal("libelle"),
                "adresse_numero" => $this->getVal("adresse_numero"),
                "adresse_numero2" => $this->getVal("adresse_numero2"),
                "adresse_voie" => $this->getVal("adresse_voie"),
                "adresse_complement" => $this->getVal("adresse_complement"),
                "lieu_dit" => $this->getVal("lieu_dit"),
                "boite_postale" => $this->getVal("boite_postale"),
                "adresse_cp" => $this->getVal("adresse_cp"),
                "adresse_ville" => $this->getVal("adresse_ville"),
                "adresse_arrondissement" => $this->getVal("adresse_arrondissement"),
                "cedex" => $this->getVal("cedex"),
                "etablissement_nature_libelle" => $this->getVal("etablissement_nature"),
                "etablissement_type_libelle" => $this->getVal("etablissement_type"),
                "etablissement_categorie_libelle" => $this->getVal("etablissement_categorie"),
                "si_locaux_sommeil" => ($this->getVal("si_locaux_sommeil")=='t')?_("locaux à sommeil"):"",
                "geolocalise" => $this->getVal("geolocalise"),
            );

            // Récupère le libellé de la voie
            if (!empty($values['adresse_voie'])) {
                $sql_adresse_voie_by_id = str_replace('<idx>', $values['adresse_voie'], $sql_adresse_voie_by_id);
                $res = $this->f->db->query($sql_adresse_voie_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_adresse_voie_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['adresse_voie'] = $row[1];
            }

            // Récupère le libellé de l'arrondissement
            if (!empty($values['adresse_arrondissement'])) {
                $sql_adresse_arrondissement_by_id = str_replace('<idx>', $values['adresse_arrondissement'], $sql_adresse_arrondissement_by_id);
                $res = $this->f->db->query($sql_adresse_arrondissement_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_adresse_arrondissement_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['adresse_arrondissement'] = $row[1];
            }
            
            // Récupère la nature de l'établissement
            if (!empty($values['etablissement_nature_libelle'])) {
                $sql_etablissement_nature_by_id = str_replace('<idx>', $values['etablissement_nature_libelle'], $sql_etablissement_nature_by_id);
                $res = $this->f->db->query($sql_etablissement_nature_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_nature_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_nature_libelle'] = $row[1];
            }
            
            // Récupère le libellé de le type de l'établissement
            if (!empty($values['etablissement_type_libelle'])) {
                $sql_etablissement_type_by_id = str_replace('<idx>', $values['etablissement_type_libelle'], $sql_etablissement_type_by_id);
                $res = $this->f->db->query($sql_etablissement_type_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_type_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_type_libelle'] = $row[1];
            }
            
            // Récupère le libellé de la catégorie de l'établissement
            if (!empty($values['etablissement_categorie_libelle'])) {
                $sql_etablissement_categorie_by_id = str_replace('<idx>', $values['etablissement_categorie_libelle'], $sql_etablissement_categorie_by_id);
                $res = $this->f->db->query($sql_etablissement_categorie_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_categorie_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                $row =& $res->fetchRow();
                $values['etablissement_categorie_libelle'] = $row[1];
            }
        }

        // Retourne le résultat
        return $values;
    }

    /**
     * Retourne l'ID de l'exploitant de l'établissement passé en paramètre,
     * ou "]" si n'existe pas.
     * 
     * @param string $etablissement Identifiant de l'etablissement
     */
    function recuperer_ID_exploitant($etablissement) {

        // Si l'établissement passé en argument est un enregistrement valide
        if (!empty($etablissement) && $etablissement != "]" && is_numeric($etablissement)) {

            $sql = "SELECT contact.contact
            FROM ".DB_PREFIXE."contact
            WHERE contact.contact_type='".$this->recuperer_ID_type_exploitant()."'
            AND contact.etablissement='".$etablissement."'";
            $res = $this->f->db->query($sql);
            $this->addToLog("recuperer_ID_exploitant() db->query(\"".$sql."\");",
                VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
            // Si un exploitant est attaché à l'établissement
            if ($row) {
                return $row['contact'];
            }
        }
        // Si nouvel établissement ou pas d'exploitant attaché
        return "]";
    }

    /**
     * Retourne l'ID du type de contact exploitant.
     */
    function recuperer_ID_type_exploitant() {

        $sql = "SELECT contact_type.contact_type as idtypeexploitant
        FROM ".DB_PREFIXE."contact_type
        WHERE LOWER(contact_type.code)='expl'";
        $res = $this->f->db->query($sql);
        $this->addToLog("recuperer_ID__type_exploitant() db->query(\"".$sql."\");",
            VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $row = $res->fetchrow(DB_FETCHMODE_ASSOC);
        return $row['idtypeexploitant'];            
    }

    /**
     * Met à jour le champ "autorite_police_encours"
     *
     * @param boolean $value true ou false
     *
     * @return boolean
     */
    function update_autorite_police_encours($value) {
        // Valeur à mettre àjour
        $valF = array(
            "autorite_police_encours" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);
        //
        if ($update == false) {
            return false;
        }

        //
        return true;
    }

    /**
     * Met à jour l'état de l'établissement.
     *
     * @param integer $value Nouvel état
     *
     * @return boolean
     */
    function update_etat($value) {
        // Valeur à mettre àjour
        $valF = array(
            "etablissement_etat" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la nature de l'établissement.
     *
     * @param integer $value Nouvelle nature
     *
     * @return boolean
     */
    function update_nature($value) {
        // Valeur à mettre àjour
        $valF = array(
            "etablissement_nature" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la date d'arrêté d'ouverture de l'établissement.
     *
     * @param string $value Valeur
     *
     * @return boolean
     */
    function update_date_arrete_ouverture($value) {
        // Valeur à mettre àjour
        $valF = array(
            "date_arrete_ouverture" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    function setValFAjout($val = array()) {
        parent::setValFAjout($val);

        // Définition d'une valeur au champ code (requis) elle automatiquement
        // modifiée dans la méthode triggerajouter
        $this->valF["code"] = "gonna_change";
    }

    /**
     * Méthode de vérification des données et de retour d’erreurs.
     *
     * @param array $val Tableau de valeur
     *
     * @return boolean
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {

        // On appelle la methode de la classe parent
        parent::verifier($val);

        // Si la nature a été choisi
        if ($this->valF["etablissement_nature"] != "") {

            // On récupère le libellé de la nature de l'établissement saisi
            $sql = "SELECT etablissement_nature
                    FROM ".DB_PREFIXE."etablissement_nature 
                    WHERE LOWER(code) = LOWER('".$this->f->getParameter("etablissement_nature_erpr")."')";
            $etablissement_nature = $this->f->db->getOne($sql);
            $this->addToLog("verifier(): db->getOne(".$sql.");", VERBOSE_MODE);
            $this->f->isDatabaseError($etablissement_nature);

            // Si l'établissement est un ERP, le type et la catégorie doivent être
            // saisis
            if ($etablissement_nature == $this->valF["etablissement_nature"]) {

                //
                if(is_null($this->valF["etablissement_type"])
                    || $this->valF["etablissement_type"] == ""
                    || is_null($this->valF["etablissement_categorie"])
                    || $this->valF["etablissement_categorie"] == "") {

                    //
                    $this->correct = false;
                    $this->addToMessage(_("Le <span class=\"bold\">type</span> et la ".
                        "<span class=\"bold\">catégorie</span> de l'établissement".
                        " doivent être saisis si l'établissement est un ERP"));

                    // Stop le traitement
                    return false;
                }
            }
        }

        // Continue le traitement
        return true;
    }

    /**
     * VIEW - view_data_etablissement.
     *
     * Retourne en JSON les informations de l'établissement
     * 
     * @return void
     */
    function view_data_etablissement() {
        //
        $this->f->disableLog();
        //
        $result = $this->get_data_etablissement();
        //
        echo json_encode($result);
    }

    /**
     * Récupère les informations de l'établissement.
     *
     * @return array
     */
    function get_data_etablissement() {

        // Initialisation du tableau des résultats
        $return = array();

        // Identifiant de l'enregistrement
        $id = $this->getVal($this->clePrimaire);

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT
                        etablissement.etablissement_type as etablissement_type,
                        etablissement_categorie as etablissement_categorie,
                        si_locaux_sommeil as etablissement_locaux_sommeil,
                        CASE WHEN etablissement_nature.nature = 'ERP Référentiel' 
                            THEN 't'
                            ELSE 'f'
                        END as etablissement_nature,
                        array_to_string(
                            array_agg(lien_etablissement_e_type.etablissement_type),
                        ';') as etablissement_type_secondaire
                    FROM ".DB_PREFIXE."etablissement
                    LEFT JOIN ".DB_PREFIXE."lien_etablissement_e_type
                    ON lien_etablissement_e_type.etablissement = etablissement.etablissement
                    LEFT JOIN ".DB_PREFIXE."etablissement_nature
                        ON etablissement.etablissement_nature=etablissement_nature.etablissement_nature
                    WHERE etablissement.etablissement = ".$id."
                    GROUP BY etablissement.etablissement_type, etablissement.etablissement_categorie,
                    etablissement.si_locaux_sommeil, etablissement_nature.nature";
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);

            // Ligne de résultat
            $row = &$res->fetchRow(DB_FETCHMODE_ASSOC);
            $types_sec = array();
            $types_sec = explode(';', $row["etablissement_type_secondaire"]);
            $row["etablissement_type_secondaire"] = $types_sec;

            // Retourne le résultat
            $return = $row;
        }

        // Retourne les résultat
        return $return;
    }

    /**
     * Vérifie si l'établissement à un dossier de coordination de visite
     * périodique.
     *
     * @param integer $etablissement_id Identifiant de l'établissement
     *
     * @return boolean
     */
    function has_dossier_coordination_periodique($etablissement_id=null) {

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Récupère la valeur du champ dossier_coordination_periodique
        $dossier_coordination_periodique = $etablissement->getVal('dossier_coordination_periodique');

        // Si le champ n'est pas vide
        if (!empty($dossier_coordination_periodique)) {

            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($dossier_coordination_periodique);

            // Si le dossier de coordination est périodique et pas clôturé
            if ($dossier_coordination->is_periodic() == true
                && $dossier_coordination->getVal('dossier_cloture') == 'f') {

                // Retourne vrai
                $this->addToLog(__METHOD__."() - return TRUE", EXTRA_VERBOSE_MODE);
                return true;
            }
        }

        // Retourne faux
        $this->addToLog(__METHOD__."() - return FALSE", EXTRA_VERBOSE_MODE);
        return false;

    }

    /**
     * Récupère la périodicité de visite de l'établissement par rapport à son
     * type et sa catégorie.
     *
     * @param integer $etablissement_type      Identifiant type de l'étab
     * @param integer $etablissement_categorie Identifiant catégorie de l'étab
     * @param boolean $etablissement_locauxasommeil Caractère locaux à sommeil de l'établissement.
     * @param integer $id                      Identifiant établissement
     *
     * @return boolean
     */
    function get_periodicite_visites($etablissement_type = null, $etablissement_categorie = null, $etablissement_locauxasommeil = null, $id = null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Récupère le type de l'établissement
        if ($etablissement_type == null) {
            $etablissement_type = $etablissement->getVal('etablissement_type');
        }
        // Récupère la catégorie de l'établissement
        if ($etablissement_categorie == null) {
            $etablissement_categorie = $etablissement->getVal('etablissement_categorie');
        }
        // Récupère le caractère locaux à sommeil de l'établissement
        if ($etablissement_locauxasommeil == null) {
            $etablissement_locauxasommeil = ($etablissement->getVal('si_locaux_sommeil') == 't' ? true : false);
        }

        // Instance de la classe periodicite_visites
        require_once '../obj/periodicite_visites.class.php';
        $periodicite_visites = new periodicite_visites(0);

        // Récupère l'identifiant de la, périodicité de visite
        $periodicite_visites_id = $periodicite_visites->get_periodicite_by_type_categorie_locaux_sommeil(
            $etablissement_type,
            $etablissement_categorie,
            $etablissement_locauxasommeil
        );

        // Si le champ n'est pas vide
        if (!empty($periodicite_visites_id)) {

            // Retourne vrai
            $this->addToLog(__METHOD__."() - return ".$periodicite_visites_id, EXTRA_VERBOSE_MODE);
            return $periodicite_visites_id;
        }

        // Retourne faux
        $this->addToLog(__METHOD__."() - return FALSE", EXTRA_VERBOSE_MODE);
        return false;
    }

    /**
     * Indique si l'établissement a des visites périodiques.
     *
     * @param integer $id Identifiant de l'établissement
     *
     * @return boolean
     */
    function is_erp($id = null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Récupère l'identifiant de la nature des établissements ayant des
        // visites périodique
        require_once '../obj/etablissement_nature.class.php';
        $etablissement_nature = new etablissement_nature(0);
        $etablissement_nature_periodique_code = $this->f->getParameter("etablissement_nature_periodique");
        $etablissement_nature_periodique = $etablissement_nature->get_etablissement_nature_by_code($etablissement_nature_periodique_code);

        // Récupère l'identifiant de l'état des établissements ayant des visites
        // périodique
        require_once '../obj/etablissement_etat.class.php';
        $etablissement_etat = new etablissement_etat(0);
        $etablissement_etat_periodique_code = $this->f->getParameter("etablissement_etat_periodique");
        $etablissement_etat_periodique = $etablissement_etat->get_etablissement_etat_by_code($etablissement_etat_periodique_code);

        // Récupère la nature de l'établissement
        $etablissement_nature = $etablissement->getVal('etablissement_nature');

        // Récupère l'état de l'établissement
        $etablissement_etat = $etablissement->getVal('etablissement_etat');

        // Si l'établissement a la bonne nature et le bon état pour avoir
        // des visites périodique
        if ($etablissement_nature == $etablissement_nature_periodique
            && $etablissement_etat == $etablissement_etat_periodique) {

            // Retourne VRAI
            return true;
        }

        // Retourn FAUX
        return false;
    }

    /**
     * Récupère le dossier de coordination qui devrait, d'après son type, être
     * le dossier de visite périodique de l'établissement.
     *
     * @param integer $etablissement_id Identifiant de l'établissement
     *
     * @return mixed                    Identifiant du DC ou false
     */
    function get_supposed_dossier_coordination_periodique($dossier_coordination=null, $etablissement_id=null) {

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Récupère la liste des dossiers de coordination de l'établissement
        $list_dossier_coordination_by_etablissement = $etablissement->get_list_dossier_coordination_by_etablissement($etablissement->getVal($etablissement->clePrimaire), $dossier_coordination);

        // Pour chaque dossier de coordination
        foreach ($list_dossier_coordination_by_etablissement as $dossier_coordination_id) {

            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($dossier_coordination_id);

            // Vérifie si le dossier de coordination est du type périodique
            $is_periodic = $dossier_coordination->is_periodic();

            // Si le type correspond au type des visites périodique
            if ($is_periodic == true) {

                // Si le dossier de coordination n'est pas clôturé
                if ($dossier_coordination->getVal('dossier_cloture') == "f") {

                    //
                    $supposed_dossier_coordination_periodique = $dossier_coordination->getVal($dossier_coordination->clePrimaire);

                    // Retourne l'identifiant du dossier de coordination
                    $this->addToLog(__METHOD__."() - return ".$supposed_dossier_coordination_periodique, EXTRA_VERBOSE_MODE);
                    return $supposed_dossier_coordination_periodique;
                }
            }
        }

        // Aucun dossier de coordination n'est du bon type
        $this->addToLog(__METHOD__."() - return FALSE", EXTRA_VERBOSE_MODE);
        return false;

    }

    /**
     * Récupère la liste des dossier de coordination de l'établissement.
     *
     * @param integer $etablissement Identifiant de l'établissement
     *
     * @return array
     */
    function get_list_dossier_coordination_by_etablissement($etablissement, $dossier_coordination=null) {

        // Initialisation du résultat
        $result = array();

        // S'il y a un établissement
        if (!empty($etablissement)) {

            // Requête SQL
            $sql = "SELECT dossier_coordination
                    FROM ".DB_PREFIXE."dossier_coordination
                    WHERE etablissement = ".intval($etablissement);
            // Si l'identifiant d'un dossier de coordination est passé
            if ($dossier_coordination !== null) {
                // Ne retourne pas le dossier de coordination passé en paramètre
                $sql .= " AND dossier_coordination NOT IN (".intval($dossier_coordination).") ";
            }
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Tant qu'il y a un résultat
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Ajoute ce résultat au tableau
                $result[] = $row['dossier_coordination'];
            }
        }

        // Retourne le résultat
        return $result;
    }

    /**
     * Ajoute la durée de périodicité à une date.
     *
     * @param string  $date_visite            Date
     * @param integer $periodicite_visites_id Identifiant de la périodicité
     * @param integer $etablissement_id       Identifiant de l'établissement
     *
     * @return string
     */
    function sum_date_visite_periodicite($date_visite, $periodicite_visites_id = null, $etablissement_id = null) {

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Initialise la date calculée
        $date_visite_periodicite = "";

        // Si la date passé est bien une date
        if ($this->f->check_date($date_visite) == true) {

            // Si l'identifiant de la périodicité n'est pas passé
            if ($periodicite_visites_id == null) {

                // Récupère la périodicité de visite
                $periodicite_visites_id = $etablissement->getVal("si_periodicite_visites");
            }

            // Instance de la classe periodicite_visites
            require_once '../obj/periodicite_visites.class.php';
            $periodicite_visites = new periodicite_visites($periodicite_visites_id);

            // Récupère la périodicité
            $periodicite = $periodicite_visites->getVal("periodicite");
            $periodicite_month = $periodicite*12;

            // Date calculé
            $date_periodicite = $this->f->mois_date($date_visite, $periodicite_month);
        }

        // Retourne la date calculé
        return $date_periodicite;
    }

    /**
     * Met à jour la périodicité de visite.
     *
     * @param integer $periodicite_visites_id Identifiant de la périodicité
     * @param integer $id                     Identifiant de l'établissement
     *
     * @return boolean
     */
    function update_etablissement_periodicite_visites($periodicite_visites_id, $id = null) {

        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Met à jour l'établissement
        $update_etablissement_values = array(
            "si_periodicite_visites" => $periodicite_visites_id,
        );
        $update_etablissement = $etablissement->update_autoexecute($update_etablissement_values);

        // Si la mise à jour échoue
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;

    }

    /**
     * Récupère le libellé du type de dossier de coordination paramétré
     * comme le type de visites périodiques.
     *
     * @return string
     */
    function get_dossier_coordination_type_periodique_libelle() {

        // Instance de la classe dossier_coordination_type
        require_once '../obj/dossier_coordination_type.class.php';
        $dossier_coordination_type = new dossier_coordination_type(0);

        // Récupère l'identifiant
        $id = $dossier_coordination_type->get_dossier_coordination_type_by_code($this->f->getParameter("dossier_coordination_type_periodique"));

        // Instance de la classe dossier_coordination_type
        require_once '../obj/dossier_coordination_type.class.php';
        $dossier_coordination_type = new dossier_coordination_type($id);

        // Récupère le libellé
        $libelle = $dossier_coordination_type->getVal('libelle');

        // Retourne le libellé
        return $libelle;
    }

    /**
     * Récupère la liste des identifiants de tous les établissements.
     *
     * @return array
     */
    function get_list_etablissement() {

        // Initialisation du résultat retourné
        $return = array();

        // Requête SQL
        $sql = "
            SELECT etablissement
            FROM ".DB_PREFIXE."etablissement";
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        $this->f->isDatabaseError($res);

        // Tant qu'il y a un résultat
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            // Ajoute ce résultat au tableau
            $return[] = $row['etablissement'];
        }

        // Résultat retourné
        return $return;
    }

    /**
     * Ajoute un dossier de coordination périodique.
     *
     * @param string  $date_periodicite Date anniverdaire périodicité
     * @param integer $etablissement_id    Identifiant établissement
     *
     * @return boolean
     */
    function add_dossier_coordination_periodique($date_periodicite, $etablissement_id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Récupération du code du type de dossier de coordination qui
        // correspond à la visite périodique
        $dossier_coordination_type_periodique = $this->f->getParameter("dossier_coordination_type_periodique");

        // Instance de la classe dossier_coordination_type
        require_once '../obj/dossier_coordination_type.class.php';
        $dossier_coordination_type = new dossier_coordination_type(0);
        // Récupération de l'identifiant du type de dossier de coordination
        $dossier_coordination_type_id = $dossier_coordination_type->get_dossier_coordination_type_by_code($dossier_coordination_type_periodique);

        // Intancie le type de dossier de coordination
        $dossier_coordination_type = new dossier_coordination_type($dossier_coordination_type_id);
        // Récupère la liste des paramètres par défaut du dossier de
        // coordination
        $list_dossier_coordination_params = $dossier_coordination_type->get_dossier_coordination_type_param();

        // Récupère les informations de l'établissement nécessaire au dossier de 
        // coordination
        $list_etablissement_data = $etablissement->get_data_etablissement();

        // Valeurs du dossier de coordination périodique
        $valF = array();
        $valF["dossier_coordination"] = "";
        $valF["dossier_coordination_type"] = $dossier_coordination_type_id;
        $valF["date_demande"] = $this->dateDBToForm($date_periodicite);
        $valF["date_butoir"] = $this->dateDBToForm($date_periodicite);
        $valF["etablissement"] = $etablissement->getVal($etablissement->clePrimaire);
        $valF["a_qualifier"] = $list_dossier_coordination_params['a_qualifier'];
        $valF["dossier_instruction_secu"] = $list_dossier_coordination_params['dossier_instruction_secu'];
        $valF["dossier_instruction_acc"] = $list_dossier_coordination_params['dossier_instruction_acc'];
        $valF["erp"] = $list_etablissement_data['etablissement_nature'];
        $valF["etablissement_locaux_sommeil"] = $list_etablissement_data['etablissement_locaux_sommeil'];
        $valF["etablissement_type"] = $list_etablissement_data['etablissement_type'];
        $valF["etablissement_categorie"] = $list_etablissement_data['etablissement_categorie'];

        // Ajoute le dossier de coordination
        $add_dossier_coordination = $etablissement->add_dossier_coordination($valF);
        $this->addToLog(__METHOD__."() - return ".$add_dossier_coordination, EXTRA_VERBOSE_MODE);

        // Si l'ajout échoue
        if ($add_dossier_coordination === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return $add_dossier_coordination;
    }

    /**
     * Ajoute un dossier de coordination.
     *
     * @param array $valF Données de l'enregistrement
     *
     * @return integer    Identifiant du nouvel enregistrement
     */
    function add_dossier_coordination($valF) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Instance de la classe dossier_coordination
        require_once '../obj/dossier_coordination.class.php';
        $dossier_coordination = new dossier_coordination("]");

        // Initialise à vide tous les champs de l'enregistrement
        foreach ($dossier_coordination->champs as $champ) {
            $val[$champ] = "";
        }

        // Fusionne les tableaux de données
        $data = array_merge($val, $valF);

        // Ajoute le dossier de coordination
        $add = $dossier_coordination->ajouter($data);

        // Si l'ajout échoue
        if ($add === false) {
            // Stop le traitement
            return false;
        }

        // Identifiant du nouvel enregistrement
        $id = $dossier_coordination->valF[$dossier_coordination->clePrimaire];

        // Logger
        $this->addToLog(__METHOD__."() - return ".$id, EXTRA_VERBOSE_MODE);

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement et retourne l'identifiant
        return $id;
    }

    /**
     * Met à jour le champ dossier_coordination_periodique de l'établissement.
     *
     * @param integer $dossier_coordination_periodique Identifiant du DC
     * @param integer $etablissement_id                Identifiant de l'étab
     *
     * @return boolean
     */
    function update_etablissement_dossier_coordination_periodique($dossier_coordination_periodique, $etablissement_id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Met à jour l'établissement
        $update_etablissement_values = array(
            "dossier_coordination_periodique" => $dossier_coordination_periodique,
        );
        $update_etablissement = $etablissement->update_autoexecute($update_etablissement_values);

        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);

        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour le champ si_derniere_visite_periodique_date de
     * l'établissement.
     *
     * @param string  $derniere_visite_periodique Date de mise à jour
     * @param integer $id                         Identifiant de l'étab
     *
     * @return boolean
     */
    function update_etablissement_derniere_visite_periodique($derniere_visite_periodique, $id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Met à jour l'établissement
        $update_etablissement_values = array(
            "si_derniere_visite_periodique_date" => $derniere_visite_periodique,
        );
        $update_etablissement = $etablissement->update_autoexecute($update_etablissement_values);

        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);

        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour le champ si_prochaine_visite_periodique_date_previsionnelle
     * de l'établissement.
     *
     * @param string  $prochaine_visite_periodique Date de mise à jour
     * @param integer $id                          Identifiant de l'étab
     *
     * @return boolean
     */
    function update_etablissement_prochaine_visite_periodique($prochaine_visite_periodique, $id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Met à jour l'établissement
        $update_etablissement_values = array(
            "si_prochaine_visite_periodique_date_previsionnelle" => $prochaine_visite_periodique,
        );
        $update_etablissement = $etablissement->update_autoexecute($update_etablissement_values);

        // Logger
        $this->addToLog(__METHOD__."() - return ".$update_etablissement, EXTRA_VERBOSE_MODE);

        // Si la mise à jour a échouée
        if ($update_etablissement === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Met à jour la date butoir du dossier de coordination périodique de
     * l'établissement.
     *
     * @param string  $date_butoir Nouvelle date butoir
     * @param integer $id          Identifiant de l'établissement
     *
     * @return boolean
     */
    function update_dossier_coordination_periodique_date_butoir($date_butoir, $etablissement_id=null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // Si l'identifiant n'est pas passé
        if ($etablissement_id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($etablissement_id);
        }

        // Identifiant du dossier de coordination périodique
        $dossier_coordination_periodique_id = $etablissement->getVal('dossier_coordination_periodique');

        // Instance de la classe dossier_coordination
        require_once '../obj/dossier_coordination.class.php';
        $dossier_coordination = new dossier_coordination($dossier_coordination_periodique_id);

        // Valeurs à mettre à jour
        $values = array(
            'date_butoir' => $this->dateDBToForm($date_butoir),
        );

        // Met à jour le dossier de coordination
        $update = $dossier_coordination->update_autoexecute($values);

        // Logger
        $this->addToLog(__METHOD__."() - return ".$update, EXTRA_VERBOSE_MODE);

        // Si la mise à jour a échouée
        if ($update === false) {
            // Stop le traitement
            return false;
        }

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        // Continue le traitement
        return true;
    }

    /**
     * Gestion du dossier de coordination périodique de l'établissement.
     *
     * @param string  $mode Mode d'utilisation de la fonction
     * @param array   $val  Valeurs
     * @param integer $id   Identifiant de l'établissement
     *
     * @return boolean
     */
    function handle_dossier_coordination_periodique($mode = null, $val = array(), $id = null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $etablissement = $this;
        } else {
            // Sinon instancie l'établissement
            $etablissement = new etablissement($id);
        }

        // Si le mode n'existe pas
        if ($mode != "update" && $mode != "lock" && $mode != "all") {
            //
            return false;
        }

        // Variables à contrôler
        $is_erp = $etablissement->is_erp();
        $periodicite_visites = $etablissement->get_periodicite_visites();
        $has_dossier_coordination_periodique = $etablissement->has_dossier_coordination_periodique();

        // Vérifie si l'établissement est un ERP valide
        if ($is_erp == false
            || $periodicite_visites == false) {
            // Message d'erreur
            $msg_error = sprintf(_("L'etablissement %s n'est pas un ERP valide :"), ' <span class="bold">'.$etablissement->getVal('libelle').'</span>');
            $this->addToMessage($msg_error);
            //Précision du message d'erreur
            if ($is_erp == false) {
                //
                $this->addToMessage("- "._("L'etat et/ou la nature de l'etablissement ne correspondent pas aux criteres d'un etablissement recevant des visites periodiques définit dans les parametres."));
            }
            //
            if ($periodicite_visites == false) {
                //
                $this->addToMessage("- "._("Le type et la categorie de l'etablissement n'ont aucune correspondance dans la table des periodicites de visites."));
            }
            // Stop le traitement
            return false;
        }

        // Met à jour la périodicité de visite de l'établissement
        $update = $etablissement->update_etablissement_periodicite_visites($periodicite_visites);

        // Si la mise à jour échoue
        if ($update === false) {
            // Stop le traitement
            return false;
        }

        // En mode "update"
        // Utilisé à l'ajout ou à la modification d'un dossier de coordination
        if ($mode == "update") {

            // Vérifie si l'établissement possède déjà un dossier de coordination
            // périodique
            if ($has_dossier_coordination_periodique == true) {
                // On vide le message de validation de l'update fait en amont
                $etablissement->msg = "";
                // Message d'erreur
                $msg_error = sprintf(_("L'etablissement %s possede deja un dossier de coordination de type %s."), ' <span class="bold">'.$etablissement->getVal('libelle').'</span>', ' <span class="bold">'.$etablissement->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                // Stop le traitement
                return false;
            }

            // Récupère le dossier de coordination
            $dossier_coordination_periodique = isset($val['dossier_coordination_periodique']) ? $val['dossier_coordination_periodique'] : null;

            // Si l'identifiant du dossier de coordination n'est pas passé en
            // paramètre
            if ($dossier_coordination_periodique == null) {
                // Stop le traitement
                return false;
            }

            // Recherche dans les dossiers de coordination existant
            $supposed_dossier_coordination_periodique = $etablissement->get_supposed_dossier_coordination_periodique($dossier_coordination_periodique);

            // Si un autre dossier de coordination que celui demandé à lié est
            // trouvé
            if ($supposed_dossier_coordination_periodique != false) {
                // On vide le message de validation de l'update fait en amont
                $etablissement->msg = "";
                // Message d'erreur
                $msg_error = sprintf(_("L'etablissement %s possede deja un dossier de coordination de type %s, mais celui-ci n'est pas correctement identifie."), ' <span class="bold">'.$etablissement->getVal('libelle').'</span>', ' <span class="bold">'.$etablissement->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                $this->addToMessage(_("Veuillez contacter votre administrateur."));
                // Stop le traitement
                return false;
            }

            // Met à jour le champ dossier_coordination_periodique de
            // l'établissement
            $update = $etablissement->update_etablissement_dossier_coordination_periodique($dossier_coordination_periodique);

            // Si la mise à jour a échouée
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            //
            $date_visite_periodicite = $etablissement->getVal('si_prochaine_visite_periodique_date_previsionnelle');

            // Si la date de prochaine visite périodique n'est pas renseigné
            if (empty($date_visite_periodicite)) {

                // On récupère la dernière date de visite du DC
                $dossier_coordination = new dossier_coordination($dossier_coordination_periodique);
                $date_visite = $dossier_coordination->get_last_date_visite();

                // On récupère la date de demande du dossire de coordination
                $date_visite = (!empty($date_visite)) ? $date_visite : $dossier_coordination->getVal('date_demande');

                // Récupère la date de la prochaine visite périodique
                // -> Date demande + périodicité
                $date_visite_periodicite = $etablissement->sum_date_visite_periodicite($date_visite);

                // Met à jour la date de la prochaine visite périodique
                $update = $etablissement->update_etablissement_prochaine_visite_periodique($date_visite_periodicite);

                // Si la mise à jour a échouée
                if ($update === false) {
                    // Stop le traitement
                    return false;
                }
            }

            // Met à jour la date butoir du dossier de coordination
            $update = $etablissement->update_dossier_coordination_periodique_date_butoir($date_visite_periodicite, $etablissement->getVal($etablissement->clePrimaire));

            // Si la mise à jour a échouée
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }

        // En mode "lock"
        // Utilisé à la clôture d'un dossier de coordination
        if ($mode == "lock") {

            // Récupère la date de la dernière visite périodique
            $date_visite = isset($val['date_visite']) ? $val['date_visite'] : null;

            // Si la date de visite n'est passée en paramètre
            if ($date_visite == null) {
                // Stop le traitement
                return false;
            }

            // Met à jour la date de la dernière visite périodique
            $update = $etablissement->update_etablissement_derniere_visite_periodique($date_visite);

            // Si la mise à jour a échouée
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            // Récupère la date de la prochaine visite périodique
            // -> Date de dernière visite + périodicité
            $date_visite_periodicite = $etablissement->sum_date_visite_periodicite($date_visite);

            // Met à jour la date de la prochaine visite périodique
            $update = $etablissement->update_etablissement_prochaine_visite_periodique($date_visite_periodicite);

            // Si la mise à jour a échouée
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            // Ajoute le nouveau dossier de coordination périodique
            $add = $etablissement->add_dossier_coordination_periodique($date_visite_periodicite);

            // Si l'ajout a échoué
            if ($add === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }

        // En mode "all"
        // Utilisé pour mettre à jour tous les établissements
        if ($mode == "all") {

            // Vérifie si l'établissement possède déjà un dossier de coordination
            // périodique
            if ($has_dossier_coordination_periodique == true) {
                // On vide le message de validation de l'update fait en amont
                $etablissement->msg = "";
                // Message d'erreur
                $msg_error = sprintf(_("L'etablissement %s possede deja un dossier de coordination de type %s."), ' <span class="bold">'.$etablissement->getVal('libelle').'</span>', ' <span class="bold">'.$etablissement->get_dossier_coordination_type_periodique_libelle().'</span>');
                $this->addToMessage($msg_error);
                // Stop le traitement
                return false;
            }

            // Recherche dans les dossiers de coordination existant
            $supposed_dossier_coordination_periodique = $etablissement->get_supposed_dossier_coordination_periodique();

            // Si un dossier de coordination est trouvé
            if ($supposed_dossier_coordination_periodique != false) {

                // Utilise le mode "update" de la même fonction
                $handle_dossier_coordination_periodique = $etablissement->handle_dossier_coordination_periodique("update", array("dossier_coordination_periodique" => $supposed_dossier_coordination_periodique));

                // Si l'ajout a échoué
                if ($handle_dossier_coordination_periodique === false) {
                    // Stop le traitement
                    return false;
                }

                // Continue le traitement
                return true;
            }

            // Récupère la date du jour
            $date_visite = date('Y-m-d');

            // Si l'établissement à une date de dernière visite périodique
            $etablissement_si_derniere_visite_periodique_date = $etablissement->getVal('si_derniere_visite_periodique_date');
            if (!empty($etablissement_si_derniere_visite_periodique_date)) {
                // Récupère la date de dernière visite périodique
                $date_visite = $etablissement_si_derniere_visite_periodique_date;
            }

            // Récupère la date de prochaine visite
            $date_visite_periodicite = $etablissement->sum_date_visite_periodicite($date_visite, $periodicite_visites);

            // Récupère la date de la dernière visite périodique
            $date_visite_periodicite = isset($val['date_visite_periodicite']) ? $val['date_visite_periodicite'] : $date_visite_periodicite;

            // Met à jour la date de la prochaine visite périodique
            $update = $etablissement->update_etablissement_prochaine_visite_periodique($date_visite_periodicite);

            // Si la mise à jour a échouée
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            // Ajoute le nouveau dossier de coordination périodique
            $add = $etablissement->add_dossier_coordination_periodique($date_visite_periodicite);

            // Si l'ajout a échoué
            if ($add === false) {
                // Stop le traitement
                return false;
            }

            // Continue le traitement
            return true;
        }
    }

    /**
     * VIEW - view_synthesis.
     *
     * @return void
     */
    function view_synthesis() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Désactive les logs
        $this->f->disableLog();
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Affichage de la synthèse
        $this->display_synthesis();
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * 
     *
     * @return void
     */
    function display_synthesis() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        echo $this->get_display_synthesis();
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     *
     * @return string
     */
    function get_display_synthesis() {
        //
        $output = "";

        /**
         * Données de l'établissement nécessaires
         */
        //
        $etablissement_id = $this->getVal($this->clePrimaire);
        //
        $etablissement_code = $this->getVal("code");
        $etablissement_libelle = $this->getVal("libelle");
        //
        $etablissement_etablissement_nature = $this->getVal("etablissement_nature");
        $etablissement_etablissement_type = $this->getVal("etablissement_type");
        $etablissement_etablissement_categorie = $this->getVal("etablissement_categorie");
        $etablissement_si_locaux_sommeil = $this->getVal("si_locaux_sommeil");
        //
        $etablissement_boite_postale = $this->getVal("boite_postale");
        $etablissement_cedex = $this->getVal("cedex");
        $etablissement_adresse_numero = $this->getVal("adresse_numero");
        $etablissement_adresse_numero2 = $this->getVal("adresse_numero2");
        $etablissement_adresse_voie = $this->getVal("adresse_voie");
        $etablissement_lieu_dit = $this->getVal("lieu_dit");
        $etablissement_adresse_complement = $this->getVal("adresse_complement");
        $etablissement_adresse_cp = $this->getVal("adresse_cp");
        $etablissement_adresse_ville = $this->getVal("adresse_ville");
        $etablissement_adresse_arrondissement = $this->getVal("adresse_arrondissement");

        /**
         * Formatage des données pour l'affichage
         */
        //// ETABLISSEMENT

        //// ADRESSE
        //
        $libelle_adresse_boite_postale = "";
        if (!empty($etablissement_boite_postale)) {
            $libelle_adresse_boite_postale = _("BP")." ".$etablissement_boite_postale;
        }
        //
        $libelle_adresse_cedex = "";
        if (!empty($etablissement_cedex)) {
            $libelle_adresse_cedex = _("Cedex")." ".$etablissement_cedex;
        }
        //
        $libelle_adresse_voie = $this->get_field_from_table_by_id(
            $etablissement_adresse_voie, 
            "libelle", 
            "voie"
        );
        //
        $libelle_adresse_arrondissement = $this->get_field_from_table_by_id(
            $etablissement_adresse_arrondissement, 
            "libelle", 
            "arrondissement"
        );
        // Découpage de l'adresse par ligne
        $adr = array();
        // Ligne 1
        $adr[] = array(
            $etablissement_adresse_numero,
            $etablissement_adresse_numero2,
            $libelle_adresse_voie,
        );
        // Ligne 2
        $adr[] = array(
            $etablissement_adresse_complement,
        );
        // Ligne 3
        $adr[] = array(
            $etablissement_lieu_dit,
            $libelle_adresse_boite_postale,
        );
        // Ligne 4
        $adr[] = array(
            $etablissement_adresse_cp,
            $etablissement_adresse_ville,
            $etablissement_adresse_arrondissement,
            $libelle_adresse_cedex,
        );
        //// NATURE
        //
        $libelle_nature = $this->get_field_from_table_by_id(
            $etablissement_etablissement_nature, 
            "nature", 
            "etablissement_nature"
        );
        //// TYPE, CATEGORIE ET LOCAUX A SOMMEIL
        //
        $libelle_type = "";
        if ($etablissement_etablissement_type != "") {
            $libelle_type = $this->get_field_from_table_by_id(
                $etablissement_etablissement_type, 
                "libelle", 
                "etablissement_type"
            );
        }
        //
        $libelle_categorie = "";
        if ($etablissement_etablissement_categorie != "") {
            $libelle_categorie = $this->get_field_from_table_by_id(
                $etablissement_etablissement_categorie, 
                "libelle", 
                "etablissement_categorie"
            );
        }
        //
        $libelle_locaux_sommeil = "";
        if ($etablissement_si_locaux_sommeil == "t") {
            $libelle_locaux_sommeil = _("locaux à sommeil");
        }



        //
        $adresse = "";
        // Compteur de ligne
        $i = 1;
        // Pour chaque ligne
        foreach ($adr as $ligne) {
            // Concat des données
            $value = $this->f->concat_text($ligne, " ");
            // Si la ligne n'est pas vide
            if (!empty($value)) {
                // Texte d'une ligne à afficher
                $text = '<span id ="etablissement_adresse_ligne'.$i.'" class="field_value">';
                $text .= $value;
                $text .= "</span>";
                // Affichage de la ligne
                $adresse .= $text."<br/>";
                // Incrémente le compteur
                $i++;
            }
        }

        /**
         *
         */
        $output = sprintf('
<div class="etablissement-synthesis" id="etablissement-synthesis-%1$s">
    <div class="legend_synthesis_etablissement">
        <a 
            id="link_etablissement" 
            class="lienFormulaire" 
            href="../scr/form.php?obj=etablissement_tous&amp;action=3&amp;idx=%1$s"
        >
            %2$s - %3$s
        </a>
    </div>
    %4$s
    <span id="etablissement-synthesis-%1$s-nature" class="etablissement-synthesis-nature">%5$s</span><br/>
    <span id="etablissement-synthesis-%1$s-type" class="etablissement-synthesis-type">%6$s %7$s</span>
    <span id="etablissement-synthesis-%1$s-categorie" class="etablissement-synthesis-categorie">%8$s %9$s</span>
    <span id="etablissement-synthesis-%1$s-locaux_sommeil" class="etablissement-synthesis-locaux_sommeil">%10$s</span>
</div>
',
            $etablissement_id,
            $etablissement_code, 
            $etablissement_libelle,
            $adresse,
            $libelle_nature,
            ("type"),
            $libelle_type,
            _("categorie"),
            $libelle_categorie,
            $libelle_locaux_sommeil
        );
        //
        return $output;
    }

    /**
     * Met à jour la date et le type de la prochaine visite SI.
     *
     * @param string  $date Date
     * @param integer $type Type
     *
     * @return boolean
     */
    function update_si_next_visit($date, $type) {
        // Valeurs de mise à jour
        $valF = array();
        $valF['si_prochaine_visite_date'] = $date;
        $valF['si_prochaine_visite_type'] = $type;

        // Mise à jour de l'étalissement
        $update = $this->update_autoexecute($valF);

        //
        return $update;
    }

    /**
     * Met à jour les dates de suivi des visites.
     *
     * @return boolean
     */
    function update_tracking_visit_dates() {

        // Récupère la prochaine visite SI
        $si_next_visit = $this->get_si_next_visit();
        // Récupère la dernière visite SI
        $si_last_visit = $this->get_si_last_visit();
        // Récupère la dernière visite ACC
        $acc_last_visit = $this->get_acc_last_visit();

        // Initalisation du tableau des données
        $valF = array();

        // Si une visite est récupérée
        if ($si_next_visit != null) {

            // Si la date de prochaine visite SI est différente
            if ($si_next_visit->getVal('date_visite') != $this->getVal('si_derniere_visite_date')) {

                // Valeurs de mise à jour
                $valF['si_derniere_visite_date'] = $si_next_visit->getVal('date_visite');
                $valF['si_derniere_visite_avis'] = $si_next_visit->get_dossier_instruction_reunion_avis();
                $valF['si_derniere_visite_technicien'] = $si_next_visit->getVal('acteur');
            }
        }

        // Si une visite est récupérée
        if ($si_last_visit != null) {

            // Si la date de prochaine visite SI est différente
            if ($si_last_visit->getVal('date_visite') != $this->getVal('si_prochaine_visite_date')) {

                // Valeurs de mise à jour
                $valF['si_prochaine_visite_date'] = $si_last_visit->getVal('date_visite');
                $valF['si_prochaine_visite_type'] = $si_last_visit->get_dossier_coordination_type();
            }
        }

        // Si une visite est récupérée
        if ($acc_last_visit != null) {

            // Si la date de prochaine visite SI est différente
            if ($acc_last_visit->getVal('date_visite') != $this->getVal('acc_derniere_visite_date')) {

                // Valeurs de mise à jour
                $valF['acc_derniere_visite_date'] = $acc_last_visit->getVal('date_visite');
                $valF['acc_derniere_visite_avis'] = $acc_last_visit->get_dossier_instruction_reunion_avis();
                $valF['acc_derniere_visite_technicien'] = $acc_last_visit->getVal('acteur');
            }
        }

        // Met à jour l'établissement
        $update = $this->update_autoexecute($valF);
        //
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * Récupère la prochaine visite SI de l'établissement.
     *
     * @return integer
     */
    function get_si_next_visit() {

        // Initialisation du retour
        $si_next_visit = null;

        // Requête SQL
        $sql = "SELECT visite
                FROM ".DB_PREFIXE."visite
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."service
                    ON dossier_instruction.service = service.service
                WHERE visite.date_visite > NOW()
                    AND LOWER(service.code) = LOWER('SI')
                ORDER BY visite.date_visite ASC
                LIMIT 1";
        $this->f->addToLog(__METHOD__."() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $visite = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($visite);

        // Si une visite est retournée par la requête
        if (!empty($visite)) {

            // Instanciation de la visite
            require_once '../obj/visite.class.php';
            $si_next_visit = new visite($visite);
        }

        //
        return $si_next_visit;
    }

    /**
     * Récupère la dernière visite SI de l'établissement.
     *
     * @return integer
     */
    function get_si_last_visit() {

        // Initialisation du retour
        $si_last_visit = null;

        // Requête SQL
        $sql = "SELECT visite
                FROM ".DB_PREFIXE."visite
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."service
                    ON dossier_instruction.service = service.service
                WHERE visite.date_visite < NOW()
                    AND LOWER(service.code) = LOWER('SI')
                ORDER BY visite.date_visite DESC
                LIMIT 1";
        $this->f->addToLog(__METHOD__."() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $visite = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($visite);

        // Si une visite est retournée par la requête
        if (!empty($visite)) {

            // Instanciation de la visite
            require_once '../obj/visite.class.php';
            $si_last_visit = new visite($visite);
        }

        //
        return $si_last_visit;
    }

    /**
     * Récupère la dernière visite ACC de l'établissement.
     *
     * @return integer
     */
    function get_acc_last_visit() {

        // Initialisation du retour
        $acc_last_visit = null;

        // Requête SQL
        $sql = "SELECT visite
                FROM ".DB_PREFIXE."visite
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON visite.dossier_instruction = dossier_instruction.dossier_instruction
                LEFT JOIN ".DB_PREFIXE."service
                    ON dossier_instruction.service = service.service
                WHERE visite.date_visite < NOW()
                    AND LOWER(service.code) = LOWER('ACC')
                ORDER BY visite.date_visite DESC
                LIMIT 1";
        $this->f->addToLog(__METHOD__."() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $visite = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($visite);

        // Si une visite est retournée par la requête
        if (!empty($visite)) {

            // Instanciation de la visite
            require_once '../obj/visite.class.php';
            $acc_last_visit = new visite($visite);
        }

        //
        return $acc_last_visit;
    }

    function get_tot_ua_valid($id_etablissement = null) {
        // Si déjà connue on la retourne directement
        if ($this->tot_ua_valid !== null) {
            return $this->tot_ua_valid;
        }
        // Sinon on la calcule puis on la stocke et enfin on la retourne
        require_once '../obj/etablissement_unite.class.php';
        $ua = new etablissement_unite(0);
        if ($id_etablissement == null) {
            $id_etablissement = intval($this->getVal($this->clePrimaire));
        }
        $tot_ua_valid = count($ua->get_list_for_etablissement($id_etablissement));
        $this->tot_ua_valid = $tot_ua_valid;
        return $tot_ua_valid;
    }

    /**
     *  Permet de pré-remplir les valeurs des formulaires.
     *  
     * @param [object]   $form        formulaire
     * @param [integer]  $maj         mode
     * @param [integer]  $validation  validation
     */
    function set_form_default_values(&$form, $maj, $validation) {
        // En mode supprimer et consulter
        if ($maj == 2 || $maj == 3) {
            // Nb total d'UA validées
            $tot_ua_valid = $this->get_tot_ua_valid();
            $form->setVal("tot_ua_valid", $this->get_tot_ua_valid());
            // S'il y en a on calcule ses champs
            if ($tot_ua_valid > 0) {
                require_once '../obj/etablissement_unite.class.php';
                $ua = new etablissement_unite(0);
                $champs_acc = array(
                    "acc_handicap_auditif",
                    "acc_handicap_mental",
                    "acc_handicap_physique",
                    "acc_handicap_visuel",
                );
                $id_etablissement = intval($this->getVal($this->clePrimaire));
                foreach ($champs_acc as $champ) {
                    $form->setVal($champ, $ua->get_general_value_switch_field_by_etablissement($id_etablissement, $champ));
                }
                if ($ua->is_any_ua_having_scda($id_etablissement) === true) {
                    $form->setVal('acc_derogation_scda', _('Oui'));
                }
            }
        }
    }
    

    /**
     * Vérifie si l'établissement à des unités d'accessibilité.
     *
     * @param integer $etablissement_id Identifiant de l'établissement
     *
     * @return boolean
     */
    function has_validated_unite_accessibilite($etablissement_id = null) {

        // Si le nombre d'unité d'accessibilité est différent de 0
        if ($this->get_tot_ua_valid($etablissement_id) > 0) {
            //
            return true;
        }
        //
        return false;
    }

    // {{{ BEGIN - CONDITION

    /**
     * CONDITION - is_referentiel.
     *
     * L'établissement est dit référentiel lorsque sa nature est de type
     * 'ERP Référentiel' dont le code dans la table 'etablissement_nature'
     * est 'ERPR'.
     *
     * @return boolean
     */
    function is_referentiel() {
        //
        if (count($this->val) === 0) {
            return null;
        }
        //
        $inst_etablissement_nature = $this->get_inst_etablissement_nature();
        if (strtolower($inst_etablissement_nature->getVal("code")) === "erpr") {
            return true;
        }
        //
        return false;
    }

    // }}} END - CONDITION

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_etablissement_nature($etablissement_nature = null) {
        return $this->get_inst_common(
            "etablissement_nature",
            $etablissement_nature
        );
    }

    /**
     *
     */
    function get_inst_arrondissement($arrondissement = null) {
        return $this->get_inst_common(
            "arrondissement",
            $arrondissement,
            "adresse_arrondissement"
        );
    }

    // }}} END - GET INST


    /**
     * Permet de savoir si l'établissement a déjà été géolocalisé, et existe sur
     * le SIG.
     *
     * @return boolean true si l'établissement n'est pas géolocalisé, sinon true.
     */
    function is_not_geolocalised() {

        $is_geolocalised = $this->getVal('geolocalise');
        //
        if ($is_geolocalised === 't') {
            //
            return false;
        }
        return true;
    }

    /**
     * VIEW - view_localiser
     * Redirige l'utilisateur vers le SIG externe.
     *
     * @return void
     */
    public function view_localiser() {
        $this->checkAccessibility();
        // Code de l'établissement
        $code = $this->getVal('code');
        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();
        if($geoaria === false) {
            // L'erreur geoaria est affichée dans la méthode handle_geoaria_exception
            return false;
        }

        // Si la redirection est sur la base des parcelles de l'établissement
        if ($this->f->get_submitted_get_value('option') === 'parcelles' AND $this->getVal('references_cadastrales') != '') {
            // Formattage des parcelles dans un tableau avant l'envoi au connecteur
            $tab_parcelles = $this->f->parseParcelles($this->getVal('references_cadastrales'));
            // Récupération du lien de redirection vers le SIG
            try {
                $url = $geoaria->redirection_web('parcelle', $tab_parcelles);
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        elseif ($this->is_not_geolocalised() === false){
            try {
                $url = $geoaria->redirection_web('etablissement', array($code));
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        // Sinon redirection vers le sig sans argument
        else {
            try {
                $url = $geoaria->redirection_web();
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
        }
        // Redirection
        header("Location: ".$url);

    }

    /**
     * VIEW - contrainte.
     *
     * Vue des contraintes de l'établissement
     *
     * Cette vue permet de gérer le contenu de l'onglet "Contraintes" sur un 
     * étblissement. Cette vue spécifique est nécessaire car l'ergonomie standard du
     * framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des contraintes liées à l'établissement qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_contrainte() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");

        // Objet à charger
        $obj = "lien_contrainte_etablissement";
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=$obj";
        $url .= "&idx=0";
        $url .= "&action=4";
        $url .= "&retourformulaire=$retourformulaire";
        $url .= "&idxformulaire=$idxformulaire";
        $url .= "&retour=form";
        // Affichage du container permettant le raffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * Retourne toutes les parcelles de l'établissement
     *
     * @return array
     */
    public function get_formated_parcelles() {
        // Résultat retourné
        return $this->f->parseParcelles($this->getVal('references_cadastrales'));
    }


    /**
     * Méthode de géocodage directement appellée par une action.
     * Cette méthode gère les exceptions et les messages d'erreur.
     *
     * @return boolean
     */
    function geocoder() {

        // Begin
        $this->begin_treatment(__METHOD__);

        // On considère que le traitement est toujours valide
        // afin que l'action ne soit jamais en erreur
        $this->geocoder_treatment();
        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - Méthode de géocodage de l'établissement sur le SIG.
     * 
     * Cette méthode peut seulement être appelée sur un établissement qui n'a pas encore
     * été géolocalisé. Elle ne doit pas faire obstacle à sa création.
     * 
     * Elle renvoie FAUX si l'établissement n'est pas géocodé, du fait d'une nécessité
     * d'intervention manuelle ou d'une erreur BBD/SIG.
     *
     * @throws  void
     * @return  boolean
     */
    function geocoder_treatment() {

        // Récupération des valeurs du formulaire dans le contexte de création d'un nouvel étab
        if ($this->get_action_crud() === 'create') {
            $code_etablissement = $this->valF['code'];
            $references_cadastrales = $this->valF['references_cadastrales'];
            $numero_voie = isset($this->valF['adresse_numero']) ? $this->valF['adresse_numero'] : '';
            $id_etablissement = $this->valF[$this->clePrimaire];
        }
        else {
            $code_etablissement = $this->getVal("code");
            $references_cadastrales = $this->getVal("references_cadastrales");
            $numero_voie = $this->getVal("adresse_numero");
            $id_etablissement = $this->getVal($this->clePrimaire);
        }
        $tab_parcelles = $this->f->parseParcelles($references_cadastrales);

        // Tableau associatif de paramètres passés au SIG
        $params = array (
             'parcelles' => $tab_parcelles,
             'numero' => $numero_voie,
             'ref_voie' => $this->get_id_voie_ref_by_etablissement_code($code_etablissement),
             'dossier_ads' => '',
        );
        // Interrogation du web service du SIG
        $precision_geocodage = '';
        //
        try {
            //
            $geoaria = $this->f->get_inst_geoaria();
            $precision_geocodage = $geoaria->geocoder_objet('etablissement', $code_etablissement, $params);

        } catch (geoaria_connector_4XX_exception $e) {

            // Si la géolocalisation de cet établissement à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_already_exist') {
                // Met à jour le flag de géolocalisation
                $update = $this->update_geolocalise('t', $id_etablissement);

                // Si la mise à jour échoue
                if ($update === false) {
                    // Stop le traitement
                    return false;
                }

                //
                $this->addToMessage(_("L'établissement est déjà géolocalisé."));
                //
                return false;
            }

            // Si la géolocalisation de cet établissement à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_cant_geocode') {
                // Template du lien vers le SIG
                $template_link_redirection_web_emprise = "<br/><a id='link_dessin_emprise_sig' title=\"%s\" class='lien' target='_blank' href='%s'><span class='om-icon om-icon-16 om-icon-fix sig-16'></span>%s</a>";
                //
                try {
                    //
                    $url_redirection_web_emprise = $this->redirection_web_emprise();

                } catch (geoaria_exception $e) {
                    //
                    $this->handle_geoaria_exception($e);
                    return $this->end_treatment(__METHOD__, false);
                }
                //
                $this->addToMessage(_("L'établissement n'a pas pu être créé automatiquement sur le SIG."));
                // Affichage du lien seulement s'il n'est pas vide
                if ($url_redirection_web_emprise != ''){
                    $link_redirection_web_emprise = sprintf(
                        $template_link_redirection_web_emprise,
                        _("Dessiner l'établissement sur le SIG"),
                        $url_redirection_web_emprise,
                        _("Cliquez ici pour le dessiner.")
                    );
                    $this->addToMessage(_($link_redirection_web_emprise."<br/>"));
                }
            }

            //
            return false;

        } catch (geoaria_exception $e) {
            //
            $this->addToMessage($this->handle_geoaria_exception($e));
            //
            return false;
        }

        // Traitement de la réponse du web service du SIG
        if ($precision_geocodage !== false AND $precision_geocodage !== '') {
            // Met à jour le flag de géolocalisation
            $update = $this->update_geolocalise('t', $id_etablissement);

            // Si la mise à jour échoue
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            //
            $this->addToMessage(
                sprintf(
                    _("L'établissement a été géolocalisé avec une précision de %sm."),
                    $precision_geocodage
                )
            );
            //
            return true;
        }

        //
        $this->addToMessage(_("L'établissement n'a pas pu être créé sur le SIG."));
        //
        return false;
    }


    /**
     * Met à jour le flag de géolocalisation de l'établissement.
     *
     * @param string $value Valeur du champ.
     *
     * @return boolean
     */
    protected function update_geolocalise($value, $etablissement) {

        // Valeur à mettre àjour
        $valF = array(
            "geolocalise" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF, $etablissement, false);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }


    /**
     * Surcharge de la méthode du core afin de faire appel à la fonction JS
     * getDataFieldReferenceCadastrale().
     *
     * @param integer $maj Mode de mise a jour
     * @return void
     */
    function bouton($maj) {
        if (!$this->correct
            && $this->checkActionAvailability() == true) {
            // Ancienne gestion des actions
            if ($this->is_option_class_action_activated() == false) {
                switch($maj) {
                    case 0 :
                        $bouton = _("Ajouter");
                        break;
                    case 1 :
                        $bouton = _("Modifier");
                        break;
                    case 2 :
                        $bouton = _("Supprimer");
                        break;
                    case 999 :
                        $bouton = _("Rechercher");
                        break;
                    default :
                        $bouton = _("Valider");
                        break;
                }
            }
            // Nouvelle gestions des actions
            if ($this->is_option_class_action_activated() == true) {
                // Actions SCRUD ou indéfinies
                if ($this->get_action_param($maj, "button") == null) {
                    // Récupération du mode de l'action
                    $crud = $this->get_action_crud($maj);
                    switch($crud) {
                        case 'create' :
                            $bouton = _("Ajouter");
                            break;
                        case 'update' :
                            $bouton = _("Modifier");
                            break;
                        case 'delete' :
                            $bouton = _("Supprimer");
                            break;
                        case 'search' :
                            $bouton = _("Rechercher");
                            break;
                        default :
                            $bouton = _("Valider");
                            break;
                    }
                }
                // Actions spécifiques
                else {
                    //
                    $bouton = $this->get_action_param($maj, "button");
                    
                }
            }
            //
            $bouton .= "&nbsp;"._("l'enregistrement de la table")."&nbsp;:";
            $bouton .= "&nbsp;'"._($this->table)."'";
            //
            $params = array(
                "value" => $bouton,
                "name" => "submit",
                "onclick" => "return getDataFieldReferenceCadastrale();"
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * Méthode renvoyant l'URL permettant de dessiner manuellement l'élément sur le SIG.
     *
     * @return string $url_redirection_web_emprise
     */
    private function redirection_web_emprise() {

        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();

        if ($this->get_action_crud() === 'create') {
            $etablissement_code = $this->valF['code'];
        }
        else {
            $etablissement_code = $this->getVal('code');
        }
        //
        $ref_voie = $this->get_id_voie_ref_by_etablissement_code($etablissement_code);

        //
        $data = array (
            'id' => $etablissement_code,
            'ref_voie' => $ref_voie,
        );
        // Interrogation du web service du SIG
        try {
            $url_redirection_web_emprise = $geoaria->redirection_web_emprise('etablissement', $data);
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            return '';
        }
        return $url_redirection_web_emprise;
    }


    /**
     * 
     * Retourne l'identifiant de la voie du référentiel lié à l'établissement
     * passé en paramètre.
     *
     * @param string $etablissement_code Code de l'établissement.
     *
     * @return mixed false si erreur sinon l'identifiant de la voie.
     */
    function get_id_voie_ref_by_etablissement_code($etablissement_code) {
        //
        $sql = "SELECT
                    voie.id_voie_ref
                FROM
                    ".DB_PREFIXE."etablissement
                LEFT JOIN
                    ".DB_PREFIXE."voie
                    ON etablissement.adresse_voie = voie.voie
                WHERE
                    etablissement.code = '".$etablissement_code."'";
        $id_voie_ref = $this->f->db->getOne($sql);
        if ($id_voie_ref === null) {
            $id_voie_ref = "";
        }
        $this->addToLog("get_id_voie_ref_by_etablissement_code(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($id_voie_ref);

        return $id_voie_ref;
    }

    /**
     * [construct_contraintes description]
     * @param  [type] $contraintes_params [description]
     * @return [type]                     [description]
     */
    public function construct_contraintes($contraintes_params = null) {
        // Variable à afficher à la place de "&contraintes"
        $contraintes = "";
        // Tableau associatif à 2 dimensions, qui contient l'ensemble des paramètres fournis à 
        // &contraintes explosés, une ligne du tableau contient un nom de groupe, sous-groupe
        // ou une valeur. 
        $conditions = array();

        // SELECT
        $selectContraintes = "SELECT 
            lien_contrainte_etablissement.texte_complete as lien_contrainte_etablissement_texte,
            lower(contrainte.groupe) as contrainte_groupe,
            lower(contrainte.sousgroupe) as contrainte_sousgroupe ";

        // FROM
        $fromContraintes = " FROM ".DB_PREFIXE."contrainte 
            LEFT JOIN ".DB_PREFIXE."lien_contrainte_etablissement
                ON  lien_contrainte_etablissement.contrainte = contrainte.contrainte ";

        // WHERE
        $whereContraintes = sprintf(
            " WHERE lien_contrainte_etablissement.etablissement = %s ",
            $this->getVal($this->clePrimaire)
        );

        $triContraintes = " ORDER BY contrainte_groupe DESC, 
            contrainte_sousgroupe, 
            contrainte.ordre_d_affichage, 
            contrainte.libelle ";

        // S'il y a des paramètres
        if ($contraintes_params !== null) {
            // Explose les paramètres et valeurs récupérées dans un tableau
            $conditions = $this->f->explode_condition_contrainte($contraintes_params[1]);
            // Récupération des conditions à ajouter au WHERE de la requête SQL
            $whereContraintes .= $this->f->treatment_condition_contrainte(NULL, $conditions);
        }

        // Tri différent sur les contraintes si l'affichage est sans arborescence
        if (isset($conditions['affichage_sans_arborescence']) && $conditions['affichage_sans_arborescence'] == 't') {
            $triContraintes = " ORDER BY contrainte.ordre_d_affichage, contrainte.libelle ";
        }

        // Requête
        $sqlContraintes = $selectContraintes.$fromContraintes.$whereContraintes.$triContraintes;
        $resContraintes = $this->f->db->query($sqlContraintes);
        $this->f->addToLog(__METHOD__." : db->query(\"".$sqlContraintes."\");", 
            VERBOSE_MODE);
        // Étant donné le contexte d'édition PDF,
        // si erreur BDD alors on stoppe le traitement et on affiche le message
        $this->f->isDatabaseError($resContraintes);

        // S'il y a un résultat
        if ($resContraintes->numRows() != 0) {
            
            // Sauvegarde des données pour les comparer
            $lastRowContrainte = array();
            $lastRowContrainte['contrainte_groupe'] = 'empty';
            $lastRowContrainte['contrainte_sousgroupe'] = 'empty';
            $contraintes .= "<table width=\"auto\" >";
            // Tant qu'il y a un résultat
            while ($rowContrainte =& $resContraintes->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Si l'identifiant du groupe de la contrainte présente et celui d'avant sont
                // différents, et si l'option affichage_sans_arborescence est désactivée
                if ($rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe']
                    && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                    $contraintes .= 
                        "<tr><td style=\"width:10px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_groupe'], 'UTF-8')."</td></tr>";
                    
                }

                // Si l'identifiant du sousgroupe de la contrainte présente et celui d'avant sont
                // différents, ou s'ils sont identiques mais n'appartiennent pas au même groupe
                // Et si l'option affichage_sans_arborescence est désactivée
                if (($rowContrainte['contrainte_sousgroupe'] != $lastRowContrainte['contrainte_sousgroupe']
                    || $rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe'])
                    &&  $rowContrainte['contrainte_sousgroupe'] != "" && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                        $contraintes .=
                        "<tr><td style=\"width:30px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_sousgroupe'], 'UTF-8')."</td></tr>";

                }
                // Si l'option d'affichage sans arborescence n'est pas activée, on ajoute les
                // contraintes avec alinéas avec tirets.
                // Sinon on affiche les contraintes sans tirets et alinéas.
                if (!isset($conditions['affichage_sans_arborescence']) || $conditions['affichage_sans_arborescence'] != 't') {
                    $contraintes .= 
                    "<tr><td style=\"width:50px; text-align:right;\"> - </td>
                    <td style=\"width:5px;\"></td><td>".
                    ucfirst($rowContrainte['lien_contrainte_etablissement_texte'])."</td></tr>";
                }
                else {
                    $contraintes .= 
                    "<tr><td>".ucfirst($rowContrainte['lien_contrainte_etablissement_texte'])."</td></tr>";
                }
                // sauvegarde des valeurs avant nouvelle itération
                $lastRowContrainte=$rowContrainte;
            }
            $contraintes .= "</table>";
        }
        //
        return $contraintes;
    }
}

?>
