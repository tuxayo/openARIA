<?php
//$Id$ 
//gen openMairie le 12/03/2015 17:59

require_once "../gen/obj/signataire.class.php";

class signataire extends signataire_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $signataire_nom = $this->getVal("nom");
        $signataire_prenom = $this->getVal("prenom");
        $signataire_qualite = $this->f->get_label_of_foreign_key("signataire","signataire_qualite",$this->getVal("signataire_qualite"));
        $signataire_civilite = $this->f->get_label_of_foreign_key("signataire","civilite",$this->getVal("civilite"));
        $signataire_signature = str_replace("\n", "<br/>", $this->getVal("signature"));
        //
        $values = array(
            "signataire.civilite" => $signataire_civilite,
            "signataire.nom" => $signataire_nom,
            "signataire.prenom" => $signataire_prenom,
            "signataire.qualite" => $signataire_qualite,
            "signataire.signature" => $signataire_signature,
        );
        //
        return $values;
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = array(
            _("signataire") => array(
                "signataire.nom" => _("nom"),
                "signataire.civilite" => _("civilite"),
                "signataire.prenom" => _("prenom"),
                "signataire.qualite" => _("signataire_qualite"),
                "signataire.signature" => _("signature"),
            ),
        );
        //
        return $labels;
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_signataire_qualite($signataire_qualite = null) {
        return $this->get_inst_common("signataire_qualite", $signataire_qualite);
    }

    // }}} END - GET INST

}

?>
