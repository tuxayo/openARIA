<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/acteur_conge.class.php";

class acteur_conge extends acteur_conge_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    function setType(&$form,$maj) {
        parent::setType($form,$maj);

        if ($maj == 0 || $maj == 1) {
            $form->setType('heure_debut','heure_minute');
            $form->setType('heure_fin','heure_minute');
        }
    }

}

?>
