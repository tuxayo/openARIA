<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/reunion_type.class.php";

class reunion_type extends reunion_type_gen {

    var $all_instances = null;

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array("is_from_good_service", );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array("is_from_good_service", );

        // ACTION - 101 - get_infos
        //
        $this->class_actions[101] = array(
            "identifier" => "get_infos",
            "view" => "get_infos",
            "permission_suffix" => "json",
        );
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
        // Si l'utilisateur est sur un service
        if (is_null($_SESSION['service'])) {
            // En mode AJOUTER
            if ($this->getParameter("maj") == 0) {
                // A la modification du champ service alors on met à jour les champs
                // liés pour que seules les valeurs correspondantes soient affichées.
                $form->setOnchange(
                    "service", 
                    "filterSelect(this.value, 'categories_autorisees', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'instances_autorisees', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'avis_autorises', 'service', 'reunion_type');".
                    "filterSelect(this.value, 'president', 'service', 'reunion_type');"
                );
            }
        }
    }

    /**
     * VIEW - get_infos.
     *
     * Renvoi un tableau JSON représentant toutes les données de 
     * l'enregistrement courant.
     *
     * @return void
     */
    function get_infos() {
        //
        $this->f->disableLog();
        //
        $infos = array();
        foreach ($this->champs as $key => $value) {
            $infos[$value] = $this->getVal($value);
        }
        //
        echo json_encode($infos);
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $this->getParameter("maj"));
        // Liaison NaN
        $form->setLib("avis_autorises", _("avis_autorises"));
        $form->setLib("instances_autorisees", _("instances_autorisees"));
        $form->setLib("categories_autorisees", _("categories_autorisees"));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $this->getParameter("maj"));
        //
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            // Liaison NaN
            $form->setType("avis_autorises", "select_multiple");
            $form->setType("instances_autorisees", "select_multiple");
            $form->setType("categories_autorisees", "select_multiple");
        }
        //
        if ($this->getParameter("maj") == 2 || $this->getParameter("maj") == 3) {
            // Liaison NaN
            $form->setType("avis_autorises", "select_multiple_static");
            $form->setType("instances_autorisees", "select_multiple_static");
            $form->setType("categories_autorisees", "select_multiple_static");
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // // XXX Attention : l'appel au parent est commenté
        // parent::setSelect($form, $this->getParameter("maj"));

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // modele_compte_rendu_global
        $sql_modele_compte_rendu_global = $sql_modele_edition;
        $sql_modele_compte_rendu_global = str_replace('<courrier_type_code>', 'REU-CRG', $sql_modele_compte_rendu_global);
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_global", $sql_modele_compte_rendu_global, $sql_modele_compte_rendu_global_by_id, true);
        // modele_compte_rendu_specifique
        $sql_modele_compte_rendu_specifique = $sql_modele_edition;
        $sql_modele_compte_rendu_specifique = str_replace('<courrier_type_code>', 'REU-CRA', $sql_modele_compte_rendu_specifique);
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_specifique", $sql_modele_compte_rendu_specifique, $sql_modele_compte_rendu_specifique_by_id, true);
        // modele_feuille_presence
        $sql_modele_feuille_presence = $sql_modele_edition;
        $sql_modele_feuille_presence = str_replace('<courrier_type_code>', 'REU-FP', $sql_modele_feuille_presence);
        $this->init_select($form, $this->f->db, $maj, null, "modele_feuille_presence", $sql_modele_feuille_presence, $sql_modele_feuille_presence_by_id, true);
        // modele_ordre_du_jour
        $sql_modele_ordre_du_jour = $sql_modele_edition;
        $sql_modele_ordre_du_jour = str_replace('<courrier_type_code>', 'REU-ODJ', $sql_modele_ordre_du_jour);
        $this->init_select($form, $this->f->db, $maj, null, "modele_ordre_du_jour", $sql_modele_ordre_du_jour, $sql_modele_ordre_du_jour_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);

        // Filtre par service
        $service = "";
        $champ = "service";
        if (isset($_POST[$champ])) {
            $service = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $service = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $service = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $service = $this->getVal($champ);
        }
        if ($service == "") {
            $service = -1;
        }

        // Filtre par service
        $sql_president = str_replace('<idx_service>', $service, $sql_president);
        $sql_president_by_id = str_replace('<idx_service>', $service, $sql_president_by_id);
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "president", $sql_president, $sql_president_by_id, true);

        // Filtre par service
        $sql_categories_autorisees = str_replace('<idx_service>', $service, $sql_categories_autorisees);
        $sql_categories_autorisees_by_id = str_replace('<idx_service>', $service, $sql_categories_autorisees_by_id);
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "categories_autorisees", $sql_categories_autorisees, $sql_categories_autorisees_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["categories_autorisees"][0]);
            array_shift($form->select["categories_autorisees"][1]);
        }

        // Filtre par service
        $sql_avis_autorises = str_replace('<idx_service>', $service, $sql_avis_autorises);
        $sql_avis_autorises_by_id = str_replace('<idx_service>', $service, $sql_avis_autorises_by_id);
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "avis_autorises", $sql_avis_autorises, $sql_avis_autorises_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["avis_autorises"][0]);
            array_shift($form->select["avis_autorises"][1]);
        }

        // Filtre par service
        $sql_instances_autorisees = str_replace('<idx_service>', $service, $sql_instances_autorisees);
        $sql_instances_autorisees_by_id = str_replace('<idx_service>', $service, $sql_instances_autorisees_by_id);
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "instances_autorisees", $sql_instances_autorisees, $sql_instances_autorisees_by_id, true, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["instances_autorisees"][0]);
            array_shift($form->select["instances_autorisees"][1]);
        }
    }

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "reunion_type_reunion_avis" => array(
            "table_l" => "reunion_type_reunion_avis",
            "table_f" => "reunion_avis",
            "field" => "avis_autorises",
        ),
        "reunion_type_reunion_instance" => array(
            "table_l" => "reunion_type_reunion_instance",
            "table_f" => "reunion_instance",
            "field" => "instances_autorisees",
        ),
        "reunion_type_reunion_categorie" => array(
            "table_l" => "reunion_type_reunion_categorie",
            "table_f" => "reunion_categorie",
            "field" => "categories_autorisees",
        ),
    );

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(_("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(_("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }
    }

    /**
     *
     * @return bool
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            require_once "../obj/".$table_l.".class.php";
            $obj_l = new $table_l("]");
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     *
     */
    function get_all_instances() {
        //
        if (is_null($this->all_instances)) {
            //
            $this->all_instances = array();
            //
            if ($this->getVal($this->clePrimaire) != "") {
                //
                $president_instance = -1;
                if ($this->getVal("president") != "") {
                    $president_instance = $this->getVal("president");
                }
                //
                $query = "
                SELECT
                    distinct(reunion_instance.reunion_instance),
                    reunion_instance.courriels
                FROM 
                    ".DB_PREFIXE."reunion_instance
                    LEFT JOIN  ".DB_PREFIXE."reunion_type_reunion_instance
                        ON reunion_instance.reunion_instance=reunion_type_reunion_instance.reunion_instance
                WHERE
                    reunion_type_reunion_instance.reunion_type=".$this->getVal($this->clePrimaire)."
                    OR reunion_instance.reunion_instance = ".$president_instance."";
                //
                $res = $this->f->db->query($query);
                //
                $this->f->addToLog(__METHOD__."(): db->query(\"".$query."\");", VERBOSE_MODE);
                //
                $this->f->isDatabaseError($res);
                //
                $results = array();
                while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
                    $results[] = $row;
                }
                $this->all_instances = $results;
            }
        }
        //
        return $this->all_instances;
    }

}

?>
