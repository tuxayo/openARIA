<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/courrier_texte_type.class.php";

class courrier_texte_type extends courrier_texte_type_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Récupère la liste des textes types possible sur un courrier.
     *
     * @param integer $courrier_id Identifiant du courrier
     *
     * @return array
     */
    function get_courrier_texte_type_by_courrier_type($courrier_type) {
        // Init de la variable de retour
        $list_courrier_texte_type = array();

        // Si un courrier type est sélectionné
        if (!empty($courrier_type)) {

            // Requête SQL si le courrier n'a pas de paramétrage
            $sql = "SELECT courrier_texte_type,
                           libelle,
                           contenu_om_html
                    FROM ".DB_PREFIXE."courrier_texte_type
                    WHERE courrier_type = ".intval($courrier_type)."
                    OR courrier_type IS NULL";
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_courrier_texte_type[] = $row;
            }
        }

        // Retourne le tableau de résultat
        return $list_courrier_texte_type;
    }

}

?>
