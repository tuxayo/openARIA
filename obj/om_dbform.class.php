<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_dbform pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_dbform.class.php 2045 2013-01-22 14:03:35Z jbastide $
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_dbform.class.php";

/**
 *
 */
require_once "om_formulaire.class.php";

/**
 *
 */
require_once 'treatment_exception.class.php';

/**
 *
 */
class om_dbform extends dbForm {
    
    /**
     *
     */
    var $om_formulaire = "om_formulaire";

    /**
     *
     */
    var $merge_fields_to_avoid_app = array(
        "om_validite_debut",
        "om_validite_fin",
    );

    /**
     *
     */
    function get_values_substitution_vars($om_collectivite_idx = null) {
        //
        $values = parent::get_values_substitution_vars($om_collectivite_idx);
        //
        $values["aujourdhui"] = date("d/m/Y");
        $values["aujourdhui_lettre"] = strftime("%d %B %Y"); 
        //
        return $values;
    }

    /**
     *
     */
    function get_labels_substitution_vars($om_collectivite_idx = null) {
        //
        $labels = parent::get_labels_substitution_vars($om_collectivite_idx);
        //
        $labels["divers"]["aujourdhui"] = _("Date du jour (Format : 14/01/1978)");
        $labels["divers"]["aujourdhui_lettre"] = _("Date du jour (Format : 14 janvier 1978)");
        //
        $labels["specifique"]["contraintes_dc|etab"] = _("Liste de toutes les contraintes respectivement du dossier de coordination ou de l'établissement");
        $labels["specifique"]["contraintes_dc|etab(liste_groupe=g1,g2...;liste_ssgroupe=sg1,sg2...)"] = _("Les options liste_groupe et liste_ssgroupe sont optionnelles et peuvent contenir une valeur unique ou plusieurs valeurs separees par une virgule, sans espace.");
        $labels["specifique"]["contraintes_dc|etab(affichage_sans_arborescence=t)"] = _("L'option affichage_sans_arborescence permet d'afficher une liste de contraintes sans leurs groupes et sous-groupes, et sans puces. Il peut prendre t (Oui) ou f (Non) comme valeur.");
        //
        return $labels;
    }

    /**
     * CONDITION - is_from_good_service.
     */
    function is_from_good_service() {
        // Si l'utilisateur a un service associé
        // et que ce service est différent du service de l'élément 
        // sur lequel on se trouve
        if (!is_null($_SESSION["service"]) 
            && $this->getVal("service") != $_SESSION["service"]) {
            // Alors on indique qu'il ne peut pas accéder à l'élément
            $this->addToLog(__METHOD__."(): return false;", EXTRA_VERBOSE_MODE);
            return false;
        } else {
            // Si l'utilisateur n'a pas de service associé
            // Alors on indique q'uil peut accéder à l'élément puisque
            // ce sont les permissions standards qui permettent d'indiquer
            // si il peut accéder ou non à l'élément
            $this->addToLog(__METHOD__."(): return true;", EXTRA_VERBOSE_MODE);
            return true;
        }
    }

    /**
     * Gestion du champ service
     */
    function set_form_specificity_service_common(&$form, $maj) {
        // Si l'utilisateur est sur un service
        if (!is_null($_SESSION['service'])) {
            // On cache le champ service
            $form->setType("service", "hidden");
            // En mode AJOUTER et MODIFIER
            if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
                // On positionne la valeur du champ service sur le service
                // de l'utilisateur
                $form->setVal("service", $_SESSION['service']);
            }
        } else {
             if ($this->getParameter("maj") == 1 || $this->getParameter("maj") == 2) {
                //
                $form->setType("service", "selecthiddenstatic");
            } elseif ($this->getParameter("maj") == 3) {
                //
                $form->setType("service", "selectstatic");
            }
        }
    }

    /**
     * TREATMENT - finalize.
     * 
     * Permet de finaliser un enregistrement
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function finalize($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_finalizing("finalize", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unfinalize.
     * 
     * Permet de definaliser un enregistrement
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function unfinalize($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_finalizing("unfinalize", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Effectue le traitement de finalisation et définalisation.
     *
     * @param string $mode finalize/unfinalize
     * @param array  $val  valeurs du formulaire
     *
     * @return boolean true/false
     */
    function manage_finalizing($mode = null, $val = array()) {
        // Logger
        $this->addToLog("manage_finalizing() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "finalize" && $mode != "unfinalize" &&
            isset($this->finalized_field) && !empty($this->finalized_field)) {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "finalize") {
            // Valeurs à modifier
            $valF = array(
                $this->finalized_field => true,
            );
            //
            $valid_message = _("Finalisation correctement effectuee.");
        } elseif ($mode == "unfinalize") {
            // Valeurs à modifier
            $valF = array(
                $this->finalized_field => false,
            );
            //
            $valid_message = _("Definalisation correctement effectuee.");
        }
        // Trigger avant
        if($this->trigger_finalize($id, $mode) === false) {
            $this->correct = false;
            return false;
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(
                    _("Attention vous n'avez fait aucune modification.")."<br/>"
                );
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
            // Trigger après
            if($this->trigger_finalize_after($id, $mode) === false) {
                $this->correct = false;
                return false;
            }
        }
        // Logger
        $this->addToLog("manage_finalizing() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }


    /**
     * Méthode appelée avant enregistrement en base de la finalisation.
     * 
     * @param string $id
     * @param string $mode  finalize/unfinalize
     * @param null   &$dnu1 @deprecated Ancienne ressource de base de données.
     * @param array  $val
     * @param null   $dnu2  @deprecated Ancien marqueur de débogage.
     */
    function trigger_finalize($id, $mode, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
    }

    /**
     * Méthode appelée après enregistrement en base de la finalisation.
     * 
     * @param string $id
     * @param string $mode  finalize/unfinalize
     * @param null   &$dnu1 @deprecated Ancienne ressource de base de données.
     * @param array  $val
     * @param null   $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function trigger_finalize_after($id, $mode, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
    }

    /**
     * Échappe et encode la valeur passée en paramètre dans le codage utilisé
     * par la base de données.
     * À la place de la valeur on peut passer en argument un paramètre POST :
     * on traitera dans ce cas la valeur du champ fourni en paramètre.
     * 
     * @param   string   $champ   Identifiant du champ posté ou valeur à échapper
     * @param   boolean  $posted  Vrai si valeur, faux si champ POST
     * @return  string            Valeur échappée
     */
    function encode_and_escape($champ, $posted = false) {

        // Liste des encodages disponibles
        $encodages = array("UTF-8", "ASCII", "Windows-1252", "ISO-8859-15", "ISO-8859-1");

        //
        if ($posted == false) {
            $postedValues = $this->getParameter("postvar");
            $value = $postedValues[$champ];
        } else {
            $value = $champ;
        }

        // Cette fonction a été supprimée avec PHP 5.4.0 et 
        // renvoi toujours la valeur FALSE depuis
        // @deprecated
        if (get_magic_quotes_gpc()) {
            // Supprime les antislashs de la chaîne
            $value = stripslashes($value);
        }
        // On remplace les doubles quotes par des simples quotes ?
        $value = strtr($value, chr(34), "'");
        // On encode les valeurs reçues dans l'encodage de la 
        // base de données
        $value = iconv(
            mb_detect_encoding($value, $encodages), 
            DBCHARSET, 
            $value
        );

        //
        return $value;
    }

    /**
     * Génère un document PDF.
     * 
     * @param string $mode Mode depuis lequel est généré le PDF.
     * @param  [integer]  $modele  ID du modèle d'édition ou de la lettretype ou de l'état.
     * @param  [mixed]    $idx     ID de l'objet instancié
     * @param  [array]    $params  Paramètres
     *
     * @return [string]            Document PDF
     */
    function compute_pdf_output($type, $obj, $collectivite = null, $idx = null, $params = null) {
        //
        if ($type == "modele_edition") {
            //
            require_once "../obj/modele_edition.class.php";
            //
            $modeles = explode(";", $obj);
            $obj = array();
            //
            foreach ($modeles as $key => $modele) {
                //
                $modele_edition = new modele_edition($modele);
                //
                if (is_numeric($modele_edition->getVal("om_lettretype"))) {
                    $type = "lettretype";
                    require_once "../obj/om_lettretype.class.php";
                    $edition = new om_lettretype($modele_edition->getVal("om_lettretype"));
                } elseif (is_numeric($modele_edition->getVal("om_etat"))) {
                    $type = "etat";
                    require_once "../obj/om_etat.class.php";
                    $edition = new om_etat($modele_edition->getVal("om_etat"));
                } else {
                    return array(
                        "pdf_output" => "",
                        "filename" => "",
                    );
                }
                //
                $obj[] = $edition->getVal("id");
            }
            //
            $obj = implode(";", $obj);
        }
        //
        return parent::compute_pdf_output(
            $type,
            $obj,
            $collectivite,
            $idx,
            $params
        );
    }

    /**
     * Permet de mettre à jour un enregistrement sans passer par la fonction
     * modifier.
     *
     * @param  array  $valF  Liste des valeurs à mettre à jour
     * @param  mixed  $id    Identifiant de l'enregistrement (instance si non précisé)
     *
     * @return boolean       Vrai si traitement réalisé avec succès
     */
    function update_autoexecute($valF, $id = null, $show_valid_msg = true) {
        if (empty($id)) {
            $id = $this->getVal($this->clePrimaire);
        }
        // Begin
        $this->begin_treatment(__METHOD__);
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $valid_message = _("Vos modifications ont bien ete enregistrees.");
        //
        $main_res_affected_rows = $this->db->affectedRows();
        // Log
        $this->addToLog(_("Requete executee"), VERBOSE_MODE);
        // Log
        $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
        $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
        $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
        $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
        $this->addToLog($valid_message, VERBOSE_MODE);
        // Message de validation
        if ($main_res_affected_rows == 0) {
            //
            if ($show_valid_msg === true) {
                //
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            }
        } else {
            //
            if ($show_valid_msg === true) {
                //
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Récupère la valeur d'un champ d'une table par l'identifiant de 
     * l'enregistrement.
     *
     * @param mixed  $id    Identifiant de l'enregistrement
     * @param string $field Nom du champ
     * @param string $table Nom de la table
     *
     * @return mixed Valeur du champ
     */
    function get_field_from_table_by_id($id, $field, $table) {
        // Instance de l'objet
        require_once '../obj/'.$table.'.class.php';
        $item = new $table($id);
        // Rretourne la valeur du champ
        return $item->getVal($field);
    }

    /**
     * Cette methode permet d'effacer les messages de validation.
     */
    function cleanMessage() {
        //
        $this->msg = "";
    }

    /**
     * Accesseur standard à une ressource.
     *
     * Cette méthode permet d'instancier la classe passée en paramètre selon
     * deux logiques différentes :
     *  - Cas n°1 : soit on veut instancier un objet en particulier de manière
     *    ponctuelle alors on passe le paramètre id qui correspond à
     *    l'identifiant de l'objet sur lequel on veut instancier la classe, et
     *    l'instanciation est effectuée et la ressource retournée.
     *  - Cas n°2 : soit on veut instancier un objet lié (clé étrangère) à
     *    l'objet courant et on ne passe donc pas de paramètre id, car il est
     *    récupéré directement sur l'objet courant (on peut éventuellement
     *    indiquer le nom du champ à récupérer par le paramètre field sinon
     *    c'est le nom de la classe qui est utilisé), et l'instanciation est
     *    effectuée et la ressource stockée puis retournée. Attention, si la
     *    ressource a déjà été stockée lors d'un appel précédent alors on la
     *    retourne sans réinstanciation.
     *
     * @param string $class Nom de la classe à instancier.
     * @param string|null $id Identifiant de l'objet à instancier.
     * @param string|null $field Nom du champ ou récupérer l'identifiant de
     *                    l'objet à instancier si différent du nom de la classe.
     *
     * @return resource
     */
    function get_inst_common($class, $id = null, $field = null) {
        // On inclut le script qui déclare la classe à instancier
        require_once "../obj/".$class.".class.php";

        //// Gestion du cas n°1 -> Instanciation ponctuelle
        // Si un identifiant est passé en paramètre
        if ($id !== null) {
            // Retour de l'instanciation
            return new $class(
                $id,
                $this->f->db,
                0
            );
        }

        //// Gestion du cas n°2 -> Instanciation liée à l'objet courant
        // On définit le nom de l'attribut dans lequel on va stocker la
        // ressource
        $var_name = "inst_".$class;
        // Si l'attribut n'existe pas ou est initialisé à null
        if (!isset($this->$var_name) || $this->$var_name === null) {
            // Si le paramètre field n'est pas passé en paramètre
            // alors on utilise le nom de la classe
            if ($field === null) {
                $field = $class;
            }
            // Stockage de l'instanciation dans l'attribut de l'objet courant
            $this->$var_name = new $class(
                $this->getVal($field),
                $this->f->db,
                0
            );
        }
        // Retour de l'instanciation
        return $this->$var_name;
    }

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     * Liste des métadonnées communes à l'ensemble des fichiers de l'application.
     */
    var $metadata_global = array(
        "application" => "get_md_application",
    );

    /**
     * Retourne le code produit de l'application (Producteur des fichiers).
     *
     * @return string
     */
    function get_md_application() {
        return "openARIA";
    }

    /**
     *
     */
    function get_md_origine_televerse() {
        return "téléversé";
    }

    /**
     *
     */
    function get_md_origine_genere() {
        return "généré";
    }

    /**
     *
     */
    function get_md_etablissement_code() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("code");
    }

    /**
     *
     */
    function get_md_etablissement_libelle() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("libelle");
    }

    /**
     *
     */
    function get_md_etablissement_siret() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("siret");
    }

    /**
     *
     */
    function get_md_etablissement_referentiel() {
        $inst_etab = $this->get_inst_etablissement();
        $referentiel = $inst_etab->is_referentiel();
        if ($referentiel === null) {
            return "";
        }
        if ($referentiel !== true) {
            return "false";
        }
        return "true";
    }

    /**
     *
     */
    function get_md_etablissement_exploitant() {
        $inst_etab = $this->get_inst_etablissement();
        $inst_exp = $inst_etab->get_inst_exploitant();
        return trim($inst_exp->getVal("prenom")." ".$inst_exp->getVal("nom"));
    }

    /**
     *
     */
    function get_md_etablissement_adresse_numero() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("adresse_numero");
    }

    /**
     *
     */
    function get_md_etablissement_adresse_mention() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("adresse_numero2");
    }

    /**
     *
     */
    function get_md_etablissement_adresse_voie() {
        $inst_etab = $this->get_inst_etablissement();
        $inst_voie = $inst_etab->get_inst_voie();
        return $inst_voie->getVal("libelle");
    }

    /**
     *
     */
    function get_md_etablissement_adresse_cp() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("adresse_cp");
    }

    /**
     *
     */
    function get_md_etablissement_adresse_ville() {
        $inst_etab = $this->get_inst_etablissement();
        return $inst_etab->getVal("adresse_ville");
    }

    /**
     *
     */
    function get_md_etablissement_adresse_arrondissement() {
        $inst_etab = $this->get_inst_etablissement();
        $inst_arr = $inst_etab->get_inst_arrondissement();
        return $inst_arr->getVal("libelle");
    }

    /**
     *
     */
    function get_md_etablissement_ref_patrimoine() {
        $inst_etab = $this->get_inst_etablissement();
        $ref_patrimoine = $inst_etab->getVal("ref_patrimoine");
        return str_replace(
            "\n",
            "",
            implode(";", explode("\r\n", $ref_patrimoine))
        );
    }

    /**
     *
     */
    function get_md_dossier_coordination() {
        $inst_dc = $this->get_inst_dossier_coordination();
        return $inst_dc->getVal("libelle");
    }

    /**
     *
     */
    function get_md_dossier_instruction() {
        $inst_di = $this->get_inst_dossier_instruction();
        return $inst_di->getVal("libelle");
    }

    /**
     *
     */
    function get_md_pv_erp_reference_urbanisme() {
        $inst_pv = $this->get_inst_proces_verbal();
        $inst_dc = $inst_pv->get_inst_dossier_coordination();
        return $inst_dc->getVal("dossier_autorisation_ads");
    }

    /**
     *
     */
    function get_md_pv_erp_nature_analyse() {
        $inst_pv = $this->get_inst_proces_verbal();
        $inst_di = $inst_pv->get_inst_dossier_instruction();
        $inst_analyses = $inst_di->get_inst_analyse();
        $inst_analyses_type = $inst_analyses->get_inst_analyses_type();
        return $inst_analyses_type->getVal("libelle");
    }

    /**
     *
     */
    function get_md_pv_erp_numero() { 
        $inst_pv = $this->get_inst_proces_verbal();
        return $inst_pv->get_full_number();
    }

    /**
     *
     */
    function get_md_pv_erp_avis_rendu() {
        $inst_pv = $this->get_inst_proces_verbal();
        $inst_dir = $inst_pv->get_inst_dossier_instruction_reunion();
        $inst_reu_avis = $inst_dir->get_inst_reunion_avis();
        return $inst_reu_avis->getVal("libelle");
    }

    /**
     *
     */
    function get_md_signataire() {
        $inst_signataire = $this->get_inst_signataire();
        return trim($inst_signataire->getVal("prenom")." ".$inst_signataire->getVal("nom"));
    }

    /**
     *
     */
    function get_md_signataire_qualite() {
        $inst_signataire = $this->get_inst_signataire();
        $inst_signataire_qualite = $inst_signataire->get_inst_signataire_qualite();
        return $inst_signataire_qualite->getVal("libelle");
    }

    /**
     *
     */
    function get_md_code_reunion() {
        $inst_reunion = $this->get_inst_reunion();
        return $inst_reunion->getVal("code"); 
    }

    /**
     *
     */
    function get_md_date_reunion() {
        $inst_reunion = $this->get_inst_reunion();
        return $inst_reunion->getVal("date_reunion"); 
    }

    /**
     *
     */
    function get_md_type_reunion() {
        $inst_reunion = $this->get_inst_reunion();
        $inst_rt = $inst_reunion->get_inst_reunion_type();
        return $inst_rt->getVal("libelle");
    }

    /**
     *
     */
    function get_md_commission() {
        $inst_reunion = $this->get_inst_reunion();
        $inst_rt = $inst_reunion->get_inst_reunion_type();
        if ($inst_rt->getVal("commission") == 't') {
            $commission = "true";
        } else {
            $commission = "false";
        }
        return $commission;
    }

    /**
     *
     */
    function get_md_titre_common() {
        //
        $titre = "";
        //
        $inst_etab = $this->get_inst_etablissement();
        if ($inst_etab->getVal("code") !== "") {
            $titre .= "Établissement ".$inst_etab->getVal("code")." - ";
        }
        //
        $inst_di = $this->get_inst_dossier_instruction();
        $inst_dc = $this->get_inst_dossier_coordination();
        if ($inst_di->getVal("libelle") !== "") {
            $titre .= "Dossier ".$inst_di->getVal("libelle")." - ";
        } elseif ($inst_dc->getVal("libelle") !== "") {
             $titre .= "Dossier ".$inst_dc->getVal("libelle")." - ";
        }
        //
        return $titre;
    }

    // }}} END - METADATA FILESTORAGE

    /**
     * CONDITION - is_option_referentiel_ads_enabled.
     *
     * @return boolean
     */
    public function is_option_referentiel_ads_enabled() {
        //
        if ($this->f->is_option_referentiel_ads_enabled() !== true) {
            return false;
        }
        //
        return true;
    }

    //
    // GESTION SIG
    //
    
    /**
     * CONDITION - is_option_sig_enabled.
     *
     * Condition pour toutes les IHM liées à la géolocalisation
     *
     * @return boolean
     */
    public function is_option_sig_enabled() {
        //
        if($this->f->is_option_sig_enabled() === false) {
            return false;
        }
        //
        return true;
    }

    /**
     * Log et ajout du message d'erreur retourné par l'abstracteur geoaria.
     *
     * @param geoads_exception $e exception attrapée
     *
     * @return string ou json si Ajax
     */
    protected function handle_geoaria_exception($e) {
        //
        $this->addToLog(
            "geolocalisation : Traitement webservice SIG: id = ".
            $this->getVal($this->clePrimaire)." : ".$e->getMessage(), DEBUG_MODE
        );
        //
        // if ($this->f->isAjaxRequest()) {
        //     $ret['log'] = array(
        //         "date" => date('d/m/Y H:i:s'),
        //         "etat" => false,
        //         "message" => $e->getMessage(),
        //     );
        //     echo json_encode($ret);
        //     die();
        // }
        //
        return $e->getMessage();
    }

    /**
     * Cette méthode permet d'exécuter une routine en début des méthodes dites 
     * de TREATMENT. Elle étend celle du core dans le sens où le traitement
     * se fait dans une transaction SQL. Celle du core gère la validation nativement.
     *
     * @param string $method_name Nom de la méthode appelante.
     * @param array  $extras      Paramètres supplémentaires.
     *
     * @return void
     */
    public function begin_treatment_with_transaction($method_name) {
        //
        $this->f->db->autoCommit(false);
        $this->begin_treatment($method_name);
    }

    /**
     * Cette méthode permet d'exécuter une routine en fin des méthodes dites 
     * de TREATMENT. Elle étend celle du core dans le sens où le traitement
     * se fait dans une transaction SQL. De plus elle gère la validation.
     *
     * @param string  $method_name Nom de la méthode appelante.
     * @param boolean $ret         Valeur de retour.
     * @param array   $extras      Paramètres supplémentaires.
     *
     * @return mixed
     */
    public function end_treatment_with_transaction($method_name, $ret = true) {
        //
        if ($ret === true) {
            $this->f->db->commit();
            return $this->end_treatment($method_name, true);
        }
        //
        $this->correct = false;
        $this->f->db->rollback();
        return $this->end_treatment($method_name, false);
    }

    /**
     * Affichage du message de validation
     * 
     * @return void
     */
    protected function display_msg() {
        $type = 'valid';
        if ($this->correct === false) {
            $type = 'error';
        }
        $this->f->displayMessage($type, $this->msg);
    }


    /**
     * VIEW - view_plot_owner.
     *
     * Affichage de la liste des propriétaires des parcelles pour les classes
     * etablissement et dossier_coordination.
     *
     * @return void
     */
    public function view_plot_owner() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération des propriétaires
        $list_plot_owner = $this->get_plot_owner();

        // Si une erreur se produit lors de la récupération des propriétaires
        if ($list_plot_owner === false) {
            //
            $this->f->layout->display_message('error', _("Une erreur s'est produite lors de la récupération des propriétaires.") . ' ' . _("Veuillez contacter votre administrateur."));
            //
            return false;
        }

        // Bouton d'ajout d'un contact
        $template_add_btn = '<a id="%1$s" onclick="javascript:show_hide_plot_owner_add_form_contact(\'show\', \'%3$s\')" href="#">
            <span class="om-icon om-icon-16 om-icon-fix add-16" title="%2$s"></span>
            %2$s
        </a>';
        // Lien du bouton d'ajout
        $link = "../scr/sousform.php?obj=contact&action=100&retourformulaire=".get_class($this)."&idxformulaire=".$this->getVal($this->clePrimaire)."&idx=0";
        // Affichage du bouton d'ajout
        $add_btn = '';
        // Permission sur l'ajout de contact
        if ($this->f->isAccredited(array("contact", "contact_ajouter"), "OR")) {
            //
            $add_btn = sprintf(
                $template_add_btn,
                'plot_owner_add_btn_contact',
                _('Ajouter un contact'),
                $link
            );
        }

        // Bouton de fermuture de l'overlay
        $template_close_btn = '<input id="%s" value="Fermer" onclick="$(\'%s\').remove();" class="om-button ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false" type="submit">';
        // Affichage du bouton de fermeture
        $close_btn = sprintf($template_close_btn, 'plot_owner_close_btn', '#overlay-container');

        // Template des fieldsets des propriétaires
        $template_owner_fieldset = '<fieldset id="fieldset-form-plot_owner_list" class="cadre ui-corner-all ui-widget-content">
            <legend class="ui-corner-all ui-widget-content ui-state-active">%1$s %2$s </legend>
            <div class="fieldsetContent">
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-ident1" class="libelle-ident1" for="ident1"> ident1 </label>
                    </div>
                    <div class="form-content">
                        <span id="ident1" class="field_value">%1$s </span>
                    </div>
                </div>

                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-ident2" class="libelle-ident2" for="ident2"> ident2 </label>
                    </div>
                    <div class="form-content">
                        <span id="ident2" class="field_value">%2$s </span>
                    </div>
                </div>
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-adr1" class="libelle-adr1" for="adr1"> adr1 </label>
                    </div>
                    <div class="form-content">
                        <span id="adr1" class="field_value">%3$s </span>
                    </div>
                </div>
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-adr2" class="libelle-adr2" for="adr2"> adr2 </label>
                    </div>
                    <div class="form-content">
                        <span id="adr2" class="field_value">%4$s </span>
                    </div>
                </div>
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-adr3" class="libelle-adr3" for="adr3"> adr3 </label>
                    </div>
                    <div class="form-content">
                        <span id="adr3" class="field_value">%5$s </span>
                    </div>
                </div>
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-cpville_pays" class="libelle-cpville_pays" for="cpville_pays"> cpville_pays </label>
                    </div>
                    <div class="form-content">
                        <span id="cpville_pays" class="field_value">%6$s </span>
                    </div>
                </div>
                <div class="field field-type-static">
                    <div class="form-libelle">
                        <label id="lib-code_pays" class="libelle-code_pays" for="code_pays"> code_pays </label>
                    </div>
                    <div class="form-content">
                        <span id="code_pays" class="field_value">%7$s </span>
                    </div>
                </div>
            </div>
        </fieldset>';

        // Liste des propriétaires
        $show_list = '';
        // S'il n'y a pas de propriétaire retourné
        if (is_array($list_plot_owner) && $list_plot_owner === array()) {

            //
            $show_list = _('Aucun propriétaires.');
        } else {

            // Pour chaque propriétaire récupéré
            foreach ($list_plot_owner as $plot_owner) {

                // Affiche le fieldset avec les information récupérées
                $show_list .= sprintf(
                    $template_owner_fieldset,
                    $plot_owner['ident1'],
                    $plot_owner['ident2'],
                    $plot_owner['adr1'],
                    $plot_owner['adr2'],
                    $plot_owner['adr3'],
                    $plot_owner['cpville_pays'],
                    $plot_owner['code_pays']
                );
            }
        }

        // Template du formulaire
        $template_plot_owner_list = '<div id="plot_owner" >
            %s
            <div id="plot_owner_list">
                %s
            </div>
            %s
        </div>';
        //
        printf($template_plot_owner_list, $add_btn, $show_list, $close_btn);

        // Div du sous-form d'ajout d'un contact
        // Caché par défaut en CSS
        printf('<div id="plot_owner_add_form_contact" ></div>');
    }


    /**
     * [get_plot_owner description]
     *
     * @return [type] [description]
     */
    public function get_plot_owner() {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $get_plot_owner_treatment = $this->get_plot_owner_treatment();

        //
        return $this->end_treatment(__METHOD__, $get_plot_owner_treatment);
    }


    /**
     * [get_plot_owner_treatment description]
     *
     * @return [type] [description]
     */
    public function get_plot_owner_treatment() {

        // On vérifie que le champ des références cadastrales est
        // corrrectement remplie
        if ($this->getVal('references_cadastrales') === ''
            || $this->getVal('references_cadastrales') === null) {
            //
            return false;
        }

        // Récupère les références cadastrales, ce traitment étant exclusivement
        // utilisé en consultion, on peut utiliser la méthorde getVal()
        $references_cadastrales = $this->getVal('references_cadastrales');

        // Récupère la liste des parcelle par références cadastrales
        $parcelles = $this->f->parseParcelles($references_cadastrales);

        //
        $list_plot_owner = false;

        //
        try {
            // Instance de geoaria
            $inst_geoaria = $this->f->get_inst_geoaria();
            // Récupère la liste des propriétaires par parcelles
            $list_plot_owner = $inst_geoaria->lister_proprietaires_parcelles($parcelles);

        } catch (geoaria_exception $e) {
            //
            $this->handle_geoaria_exception($e);
            //
            return false;
        }

        //
        return $list_plot_owner;
    }


}

?>
