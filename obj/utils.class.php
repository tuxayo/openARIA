<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_application pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: utils.class.php 547 2011-08-25 12:03:42Z fmichon $
 */

/**
 *
 */
require_once "../dyn/locales.inc.php";

/**
 *
 */
require_once "../dyn/include.inc.php";

/**
 *
 */
require_once "../dyn/debug.inc.php";

/**
 *
 */
(defined("PATH_OPENMAIRIE") ? "" : define("PATH_OPENMAIRIE", ""));

/**
 *
 */
require_once PATH_OPENMAIRIE."om_application.class.php";

/**
 *
 */
class utils extends application {

    /**
     * Gestion du nom de l'application.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_application_name = "openARIA";

    /**
     * Titre HTML.
     *
     * @var mixed Configuration niveau application.
     */
    protected $html_head_title = ":: openMairie :: openARIA - Analyse du Risque Incendie et de l'Accessibilité pour les ERP";

    /**
     * Gestion du nom de la session.
     *
     * @var mixed Configuration niveau application.
     */
    protected $_session_name = "1bb484de79f96a6d0b00aaf4463c18f8bf";

    /**
     * Gestion du nombre de colonnes du tableau de bord.
     *
     * @var mixed Configuration niveau application.
     */
    protected $config__dashboard_nb_column = 2;

    /**
     * Gestion du mode de gestion des permissions.
     *
     * @var mixed Configuration niveau framework.
     */
    protected $config__permission_by_hierarchical_profile = false;

    // {{{
    
    /**
     * 
     */
    function isAccredited($obj = NULL, $operator = "AND") {
        // Lorsque l'utilisateur a le login 'admin' alors il est authorisé à
        // accéder à toutes les fonctions du logiciel
        // XXX à modifier pour changer ça vers un profil et non un login utilisateur
        if ($_SESSION["login"] == "admin" 
            && $obj != "menu_administration"
            && $obj != "menu_parametrage"
            ) {
            return true;
        }
        // Fonctionnement standard
        return parent::isAccredited($obj, $operator);
    }

    /**
     * Permet de calculer la liste des parcelles à partir de la chaîne passée en paramètre
     * et la retourner sous forme d'un tableau associatif
     * 
     * @param  string $strParcelles chaîne de la parcelles 
     * @return array (array(quartier, section, parcelle), ...)
     */
    function parseParcelles($strParcelles) {
        
        // Séparation des lignes
        $references = explode(";", $strParcelles);
        $liste_parcelles = array();
        
        // On boucle sur chaque ligne pour ajouter la liste des parcelles de chaque ligne
        foreach ($references as $parcelles) {
            
            // On transforme la chaîne de la ligne de parcelles en tableau
            $ref = str_split($parcelles);
            // Les 1er caractères sont numériques
            $num = true;
            
            // Tableau des champs de la ligne de références cadastrales
            $reference_tab = array();
            $temp = "";
            foreach ($ref as $carac) {
                
                // Permet de tester si le caractère courant est de même type que le précédent
                if(is_numeric($carac) === $num) {
                    $temp .= $carac;
                } else {
                    // Bascule
                    $num = !$num;
                    // On stock le champ
                    $reference_tab[] = $temp;
                    // re-init de la valeur temporaire pour le champ suivant
                    $temp = $carac;
                }
            }
            // Stockage du dernier champ sur lequel il n'y a pas eu de bascule
            $reference_tab[] = $temp;
            // Calcul des parcelles
            $quartier = $reference_tab[0];
            $sect = $reference_tab[1];

            $ancien_ref_parc = "";
            for ($i = 2; $i < count($reference_tab); $i += 2) {
                $parc["prefixe"] = $this->get_arrondissement_code_impot($quartier);
                $parc["quartier"] = $quartier;
                // Met en majuscule si besoin
                $parc["section"] = strtoupper($sect);
                if ($ancien_ref_parc == "" OR $reference_tab[$i - 1] == "/") {
                    // 1ere parcelle ou parcelle individuelle
                    // Compléte par des "0" le début de la chaîne si besoin
                    $parc["parcelle"] = str_pad($reference_tab[$i], 4, "0", STR_PAD_LEFT);
                    // Ajout d'une parcelle à la liste
                    $liste_parcelles[] = $parc;
                } elseif ($reference_tab[$i - 1] == "A") {
                    // Interval de parcelles
                    for ($j = $ancien_ref_parc + 1; $j <= $reference_tab[$i]; $j++) {
                        // Compléte par des "0" le début de la chaîne si besoin
                        $parc["parcelle"] = str_pad($j, 4, "0", STR_PAD_LEFT);
                        // Ajout d'une parcelle à la liste
                        $liste_parcelles[] = $parc;
                    }
                }
                //Gestion des erreurs
                else{
                    
                    echo "<div class=\"message ui-widget ui-corner-all ui-state-highlight ui-state-valid\">";
                    echo "<p><span class=\"ui-icon ui-icon-info\"></span>";
                    echo "<span class=\"text\">";
                    echo _("Un mauvais formatage de la reference cadastrale a ete detecte : ");
                    echo _("le separateur doit etre 'A' ou '/' et non '").$reference_tab[$i - 1]."'.";
                    echo "</span></p></div>";
                }
                // Sauvegarde de la référence courante de parcelle
                $ancien_ref_parc = $reference_tab[$i];
            }
        }

        return $liste_parcelles;
    }


    /**
     * Récupère le code impôt par rapport au quartier.
     * 
     * @param string $quartier Numéro de quartier.
     * @return string Code impôts.
     */
    protected function get_arrondissement_code_impot($quartier) {
        // Initialisation
        $code_impots = "";
        // Si le quartier fournis est correct
        if ($quartier != "") {
            // Requête SQL
            $sql = "SELECT
                        arrondissement.code_impots
                    FROM
                        ".DB_PREFIXE."arrondissement
                    LEFT JOIN
                        ".DB_PREFIXE."quartier
                        ON
                            quartier.arrondissement = arrondissement.arrondissement 
                    WHERE
                        quartier.code_impots = '".$quartier."'";

        }
        $code_impots = $this->db->getOne($sql);
        if ($code_impots === null) {
            $code_impots = "";
        }
        $this->isDatabaseError($code_impots); 
        // Retour
        return $code_impots;
    }


    /**
     * Vérification du n° de SIRET avec la méthode de Luhn.
     * 
     * @param string $val Numéro de SIRET
     * 
     * @return bool Vrai si valide
     */
    function checkLuhn($val) {

        $len = strlen($val);
        $total = 0;
        for ($i = 1; $i <= $len; $i++) {

            $chiffre = substr($val, -$i, 1);
            if($i % 2 == 0) {
                
                $total += 2 * $chiffre;
                if((2 * $chiffre) >= 10) {
                    $total -= 9;
                }
            }
            else {

                $total += $chiffre;
            }
        }
        if ($total % 10 == 0) {

            return true;
        } else {

            return false;
        }
    }

    /**
     *
     */
    function is_user_with_role_and_service($role, $service) {
        //
        if (in_array($role, array("technicien", "cadre", "secretaire", )) !== true) {
            return false;
        }
        //
        if (in_array($service, array("si", "acc", )) !== true) {
            return false;
        }
        //
        $sql = "
        SELECT *
        FROM ".DB_PREFIXE."acteur 
            LEFT JOIN ".DB_PREFIXE."om_utilisateur
                ON  acteur.om_utilisateur=om_utilisateur.om_utilisateur
        WHERE om_utilisateur.login='".$_SESSION["login"]."'
        ";
        $res = $this->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        if ($this->isDatabaseError($res, true) !== false) {
            return false;
        }
        //
        if ($res->numrows() === 0) {
            return false;
        }
        $row = $res->fetchRow(DB_FETCHMODE_ASSOC);
        if ($role != $row["role"]) {
            return false;
        }
        if ($service != $this->get_service_code($row["service"])) {
            return false;
        }
        return true;
    }


    /**
     * Teste si un acteur a un rôle donné
     * 
     * @param string $nom_prenom Le nom concaténé au prénom, de l'acteur
     * @param string $role Le rôle de l'acteur
     * 
     * @return mixed true si l'acteur a le rôle donné,
     *               false si l'acteur n'a pas le rôle donné
     *               -1 si une erreur s'est produite
     */
    function hasActeurRole($nom_prenom, $role) {
                
        //On vérifie les données passées en paramètre
        if (is_string($nom_prenom) && $nom_prenom !== "" &&
            is_string($role) && $role !== "") {
            
            //Création de la requête
            $sql = "SELECT role "
                    . "FROM ".DB_PREFIXE."acteur "
                    . "WHERE nom_prenom = '".$nom_prenom."'";
            //Exécution de la requête
            $roleActeurBdd = $this->db->getOne($sql);
            $this->addToLog("hasActeurRole(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->isDatabaseError($roleActeurBdd);
            
            if (!is_string($roleActeurBdd)||$roleActeurBdd===""){
                $this->addToLog("hasActeurRole(): "._("Aucun rôle associé à cet acteur"), VERBOSE_MODE);
                return -1;
            }
            
            //On compare le rôle récupéré en base de données et le rôle passé en
            //paramètre de la fonction
            if (strcmp($role, $roleActeurBdd)===0){
                return true;
            }
            else {
                $this->addToLog("hasActeurRole(): ".
                    _("L'acteur n'a pas le rôle '").$role._("' mais le rôle '").
                    $roleActeurBdd."'", VERBOSE_MODE);
                return false;
            }
        }
        $this->addToLog("hasActeurRole(): ".
            _("Le format des données fournies en entrées est incorrect"), VERBOSE_MODE);
        return -1;
    }

    /**
     * Récupère l'identifiant de l'acteur de l'utilisateur courant.
     *
     * @return integer Identifiant de l'acteur
     */
    function get_acteur_by_session_login() {
        //
        $acteur = "";

        //
        if (isset($_SESSION['login']) && !empty($_SESSION['login'])) {
            //
            $sql = "SELECT acteur
                    FROM ".DB_PREFIXE."acteur
                    LEFT JOIN ".DB_PREFIXE."om_utilisateur
                        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur
                    WHERE login = '".$_SESSION['login']."'";
            $acteur = $this->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->isDatabaseError($acteur);
        }

        //
        return $acteur;
    }

    /**
     * Permet de concaténer les cellules d'un tableau en un string 
     * en supprimant les vides.
     *
     * @param array  $texts Tableau à transformer en string
     * @param string $glue  Caractère de séparation des cellules
     *
     * @return string
     */
    function concat_text($texts = array(), $glue = "") {
        //
        $texts_without_blank = array();
        //
        foreach ($texts as $key => $value) {
            if (!empty($value)) {
                $texts_without_blank[$key] = $texts[$key];
            }
        }
        //
        return implode($glue, $texts_without_blank);
    }

    // {{{

    /**
     *
     */
    function triggerAfterLogin($utilisateur = NULL) {
        //
        parent::triggerAfterLogin($utilisateur);
        //
        $sql = "select *
        from ".DB_PREFIXE."om_utilisateur 
            left join ".DB_PREFIXE."acteur 
            on om_utilisateur.om_utilisateur=acteur.om_utilisateur
        where om_utilisateur.om_utilisateur=".$utilisateur["om_utilisateur"];
        $res = $this->db->query($sql);
        // Logger
        $this->addToLog("triggerAfterLogin(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->isDatabaseError($res);
        if ($res->numRows() == 0) {
            $_SESSION["service"] = null;
        } else {
            $row =& $res->fetchRow(DB_FETCHMODE_ASSOC);
            if ($row["service"] != "") {
                $_SESSION["service"] = $row["service"];
            } else {
                $_SESSION["service"] = null;
            }
        }
        $res->free();
    }

    // }}}

    /**
     * Permet de transformer la valeur retournée de la bdd en booléen.
     *
     * @param string $value valeur du champ
     *
     * @return mixed booléen ou valeur du champ
     */
    function get_val_boolean($value) {
        //
        if ($value == 't') {
            return true;
        }
        //
        if ($value == 'f') {
            return false;
        }
        //
        return $value;
    }
    
    /**
     * Retourne le code du service
     * 
     * @param   integer $id_service L'identifiant du service
     * @return string Code du service
     */
    function get_service_code($id_service) {
        
        $code = "";
        //Si l'identifiant du service a été fourni
        if($id_service!=''){
            // Requête 
            $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."service
                WHERE service = ".intval($id_service);
            $this->addToLog("get_service_code() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $code = $this->db->getOne($sql);
            $this->isDatabaseError($code);
        }
        
        return $code;
    }

    /**
     * Récupère la valeur d'un champ d'une table par l'identifiant de 
     * l'enregistrement.
     *
     * @param mixed  $id    Identifiant de l'enregistrement
     * @param string $field Nom du champ
     * @param string $table Nom de la table
     *
     * @return mixed Valeur du champ
     */
    function get_field_from_table_by_id($id, $field, $table) {
        // Instance de l'objet
        require_once '../obj/'.$table.'.class.php';
        $item = new $table($id);
        // Rretourne la valeur du champ
        return $item->getVal($field);
    }


    /**
     * Récupère l'identifiant de l'enregistrement par la valeur d'un de ses
     * champs.
     *
     * @param mixed  $value Valeur du champ de l'enregistrement.
     * @param string $field Nom du champ.
     * @param string $table Nom de la table.
     *
     * @return mixed Identifiant de l'enregistrement ou null.
     */
    public function get_id_from_table_by_field($value, $field, $table) {
        //
        if ($value === null || $value === ''
            || $field === null || $field === ''
            || $table === null || $table === '') {
            //
            return null;
        }

        // Requête SQL
        $sql = 'SELECT %1$s
                FROM '.DB_PREFIXE.'%1$s
                WHERE %2$s = \'%3$s\'';
        $sql = sprintf($sql, $table, $field, $value);
        // log
        $this->addToLog(__METHOD__."() db->query(\"".$sql."\");",
                VERBOSE_MODE);
        // Utilise 'query' car il pourrait y avoir plusieurs résultat
        $res = $this->db->query($sql);
        //
        $this->isDatabaseError($res);

        // S'il y a plusieurs résultats
        if ($res->numRows() > 1) {
            //
            return null;
        }

        //
        $row = &$res->fetchRow();
        return $row[0];
    }


    /**
     * Retourne l'identifiant du service.
     *
     * @param string $code_service code de service
     *
     * @return mixed false si erreur sinon identifiant du service
     */
    function get_service_id($code_service) {
        
        $id_service = "";
        //Si l'identifiant du service a été fourni
        if($code_service !=' '){
            // Requête 
            $sql = "SELECT service
                FROM ".DB_PREFIXE."service
                WHERE code = '".$code_service."'";
            $this->addToLog("get_service_id() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $id_service = $this->db->getOne($sql);
            $this->isDatabaseError($id_service);
        }
        
        return $id_service;
    }

    /**
     * Permet de générer un code barres.
     *
     * @param string  $prefix Préfix du code barres
     * @param integer $id     Identifiant de l'enregistrement
     *
     * @return string         Code barres
     */
    function generate_code_barres($prefix, $id) {
        // Met l'identifiant de l'enregistrement sur 10 caractères
        $result = str_pad($id, 10, "0", STR_PAD_LEFT);
        // Retourne la concaténation du préfix avec l'identifiant
        return $prefix.$result;
    }
    
    /**
     * Retourne la semaine qui suit la dernière existante
     * 
     * @return array tableau associatif de l'année et la semaine
     */
    function define_next_week($annee_max, $semaine_max) {
        
    	// Calcul de la prochaine semaine
        // si aucune semaine récupérée on ne continue pas le traitement
        if ($annee_max == '' || $semaine_max == '') {
        	return false;
        }
        // on définit une date au lundi de la semaine récupérée
        $date_recuperee = new DateTime();
        $date_recuperee->setISODate($annee_max,$semaine_max,1);
        // on l'incrémente d'une semaine
        $date_recuperee->add(new DateInterval('P7D'));
        // on récupère les valeurs
        $annee = $date_recuperee->format('o');
        $semaine = $date_recuperee->format('W');
        $prochaine_semaine = array(
        	"annee" => $annee,
        	"semaine" => $semaine,
        );
        return $prochaine_semaine;
    }
    
    /**
     * Vérification des paramètres
     */
    function checkParams() {
        //
        parent::checkParams();
        // Dossier des numérisations
        (isset($this->config['digitalization_folder']) ? "" : $this->config['digitalization_folder'] = '../var/digitalization/');
    }

    /**
     * Calcule une date par l'ajout ou la soustraction de mois
     * @param date    $date     Date de base
     * @param integer $delay    Délais à ajouter (en mois)
     * @param string  $operator Opérateur pour le calcul ("-" ou "+")
     * 
     * @return date             Date calculée
     */
    function mois_date($date, $delay, $operator = "+") {
            
        // Si aucune date n'a été fournie ou si ce n'est pas une date correctement formatée
        if ( is_null($date) || $date == "" ||
            preg_match('/[0-9]{4}-[0-9]{2}-[0-9]{2}/', $date) == 0 ){
            return null;
        }

        // Découpage de la date
        $temp = explode("-", $date);
        $day = (int) $temp[2];
        $month = (int) $temp[1];
        $year = (int) $temp[0];

        // Si c'est une addition
        if ($operator == '+') {
            // Année à ajouter
            $year += floor($delay / 12);
            // Mois restant
            $nb_month = ($delay % 12);
            // S'il y a des mois restant
            if ($nb_month != 0) {
                // Ajout des mois restant
                $month += $nb_month;
                // Si ça dépasse le mois 12 (décembre)
                if ($month > 12) {
                    // Soustrait 12 au mois
                    $month -= 12;
                    // Ajoute 1 à l'année
                    $year += 1;
                }
            }
        }

        // Si c'est une soustraction
        if ($operator == "-") {
            // Année à soustraire
            $year -= floor($delay / 12);
            // Mois restant
            $nb_month = ($delay % 12);
            // S'il y a des mois restant
            if ($nb_month != 0) {
                // Soustrait le délais
                $month -= $nb_month;
                // Si ça dépasse le mois 1 (janvier)
                if ($month < 1) {
                    // Soustrait 12 au mois
                    $month += 12;
                    // Ajoute 1 à l'année
                    $year -= 1;
                }
            }
        }

        // Calcul du nombre de jours dans le mois sélectionné
        switch($month) {
            // Mois de février
            case "2":
                if ($year % 4 == 0 && $year % 100 != 0 || $year % 400 == 0) {
                    $day_max = 29;
                } else {
                    $day_max = 28;
                }
            break;
            // Mois d'avril, juin, septembre et novembre
            case "4":
            case "6":
            case "9":
            case "11":
                $day_max = 30;
            break;
            // Mois de janvier, mars, mai, juillet, août, octobre et décembre 
            default:
                $day_max = 31;
        }

        // Si le jour est supérieur au jour maximum du mois
        if ($day > $day_max) {
            // Le jour devient le jour maximum
            $day = $day_max;
        }

        // Compléte le mois et le jour par un 0 à gauche si c'est un chiffre
        $month = str_pad($month, 2, "0", STR_PAD_LEFT);
        $day = str_pad($day, 2, "0", STR_PAD_LEFT);

        // Retourne la date calculée
        return $year."-".$month."-".$day ;
    }

    /**
     * Vérifie la valididité d'une date.
     * 
     * @param string $pDate Date à vérifier
     * 
     * @return boolean
     */
    function check_date($pDate) {

        // Vérifie si c'est une date valide
        if (preg_match("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $pDate, $date) 
            && checkdate($date[2], $date[3], $date[1]) 
            && $date[1] >= 1900) {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * Récupère le type du courrier depuis le modèle d'édition.
     *
     * @param integer $modele_edition_id Identifiant du modèle d'édition
     *
     * @return integer
     */
    function get_courrier_type_by_modele_edition($modele_edition_id) {
        // Initialisation de la variable de résultat
        $courrier_type = "";
        //
        if (!empty($modele_edition_id)) {
            // Instance de la classe modele_edition
            require_once '../obj/modele_edition.class.php';
            $modele_edition = new modele_edition($modele_edition_id);

            // Récupère courrier_type
            $courrier_type = $modele_edition->getVal('courrier_type');
        }

        // Retourne le résultat
        return $courrier_type;
    }

    /**
     * Permet d'obtenir le code/libellé d'un select
     * 
     * @param  [string]   $class  classe principale
     * @param  [string]   $field  nom de la clé étrangère
     * @param  [integer]  $value  valeur de la clé étrangère
     * @return [string]           code ou libellé de la clé étrangère
     */
    function get_label_of_foreign_key($class, $field, $value = "") {
        if ($value != "") {
            // Inclusion du fichier de requêtes
            if (file_exists("../sql/".OM_DB_PHPTYPE."/".$class.".form.inc.php")) {
                include "../sql/".OM_DB_PHPTYPE."/".$class.".form.inc.php";
            } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$class.".form.inc")) {
                include "../sql/".OM_DB_PHPTYPE."/".$class.".form.inc";
            }
            // construction variable sql
            $var_sql = "sql_".$field."_by_id";
            // si la variable existe
            if (isset($$var_sql)) {
                // remplacement de l'id par sa valeur dans la condition
                $sql = str_replace('<idx>', $value, $$var_sql);
                // exécution requete
                $res = $this->db->query($sql);
                $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
                // Si la récupération du code/libellé  échoue
                if ($this->isDatabaseError($res, true)) {
                    $this->addToLog(__METHOD__."(): ERROR");
                    return "";
                }
                $row = &$res->fetchRow();
                // retour de la deuxième colonne sélectionnée
                // (considérée comme le code/libellé)
                return $row[1];
            }
        }
        // retour chaîne vide si pas de valeur
        return "";
    }

    /**
     *
     */
    function view_module_settings() {
        //
        require_once "../obj/settings.class.php";
        $settings = new settings();
        $settings->view_main();
    }

    // {{{ BEGIN - SWROD
    // 

    /**
     *
     */
    var $inst_swrod = null;

    /**
     *
     */
    function is_swrod_enabled() {
        //
        if ($this->getParameter("swrod") === "true") {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function is_swrod_available() {
        //
        $inst_swrod = $this->get_inst_swrod();
        //
        if ($inst_swrod !== false
            && isset($inst_swrod->swrod)
            && $inst_swrod->swrod != null) {
            return true;
        }
        //
        return false;
    }

    /**
     *
     */
    function get_inst_swrod() {
        //
        if ($this->inst_swrod !== null) {
            return $this->inst_swrod;
        }
        //
        if (!file_exists("../dyn/swrod.inc.php")) {
            $this->inst_swrod = false;
            return $this->inst_swrod;
        }
        require "../dyn/swrod.inc.php";
        if (!isset($swrod)) {
            $this->inst_swrod = false;
            return $this->inst_swrod;
        }
        //
        require_once "../obj/swrod.class.php";
        $this->inst_swrod = new swrod($swrod);
        return $this->inst_swrod;
    }

    // }}} END - SWROD

    /**
     * Vérifie que l'état de l'option du SIG
     *
     * @return boolean true si activée
     */
    public function is_option_sig_enabled() {
        //
        $option_sig = $this->getParameter('option_sig');
        if ($option_sig === 'sig_externe' || $option_sig === 'sig_interne') {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * Instance de l'abstracteur de géolocalisation.
     *
     * @var object
     */
    var $inst_geoaria = null;

    /**
     * Retourne une instance du connecteur geoaria
     * et la crée si elle n'existait pas.
     *
     * @return inst_geoaria Instance du connecteur
     */
    public function get_inst_geoaria() {
        //
        if (isset($this->inst_geoaria)) {
            return $this->inst_geoaria;
        }
        // Instanciation de l'abstracteur geoaria
        // On récupère les informations de la collectivité de l'utilisateur
        $param_coll = $this->getCollectivite();
        require_once "../obj/geoaria.class.php";
        $this->inst_geoaria = new geoaria($param_coll);
        //
        return $this->inst_geoaria;
    }

    /**
     * SURCHARGE DE LA CLASSE OM_APPLICATION.
     *
     * @see Documentation sur la méthode parent 'om_application:getCollectivite'.
     */
    function getCollectivite($om_collectivite_idx = null) {
        // On vérifie si une valeur a été passée en paramètre ou non.
        if ($om_collectivite_idx === null) {
            // Cas d'utilisation n°1 : nous sommes dans le cas où on 
            // veut récupérer les informations de la collectivité de
            // l'utilisateur et on stocke l'info dans un flag.
            $is_get_collectivite_from_user = true;
            // On initialise l'identifiant de la collectivité
            // à partir de la variable de session de l'utilisateur.
            $om_collectivite_idx = $_SESSION['collectivite'];
        } else {
            // Cas d'utilisation n°2 : nous sommes dans le cas où on
            // veut récupérer les informations de la collectivité 
            // passée en paramètre et on stocke l'info dans le flag.
            $is_get_collectivite_from_user = false;
        }
        //
        $collectivite_parameters = parent::getCollectivite($om_collectivite_idx);

        //// BEGIN - SURCHARGE OPENADS

        // Ajout du paramétrage du sig pour la collectivité
        if (file_exists("../dyn/sig.inc.php")) {
            include "../dyn/sig.inc.php";
        }
        $sig_externe = $this->get_database_extra_parameters('sig');
        if (!isset($sig_externe)) {
            $sig_externe = "sig-default";
        }

        if (isset($collectivite_parameters['om_collectivite_idx'])
            && isset($conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']])
            && isset($collectivite_parameters["option_sig"]) 
            && $collectivite_parameters["option_sig"] == "sig_externe"
            ) {

            // Cas numéro 1 : conf sig définie sur la collectivité et option sig active
            $collectivite_parameters["sig"] = $conf[$sig_externe][$collectivite_parameters['om_collectivite_idx']];
        }
        //// END - SURCHARGE OPENADS

        // Si on se trouve dans le cas d'utilisation n°1
        if ($is_get_collectivite_from_user === true) {
            // Alors on stocke dans l'attribut collectivite le tableau de 
            // paramètres pour utilisation depuis la méthode 'getParameter'.
            $this->collectivite = $collectivite_parameters;
        }
        // On retourne le tableau de paramètres.
        return $collectivite_parameters;
    }


    /**
     * Retourne le caractère 's' si nécessaire
     *
     * @param  integer $nombre
     * @return string
     */
    public function pluriel($nombre) {
        // Spécifique au français
        if (LOCALE !== 'fr_FR') {
            return '';
        }
        if (intval($nombre) > 1) {
            return 's';
        }
        return '';
    }

    /**
     * Cette fonction prend en entrée le ou les paramètres du &contrainte qui sont entre
     * parenthèses (un ensemble de paramètres séparés par des points-virgules). Elle
     * sépare les paramètres et leurs valeurs puis construit et retourne un tableau 
     * associatif qui contient pour les groupes et sous-groupes :
     * - un tableau de valeurs, avec un nom de groupe ou sous-groupe par ligne
     * pour les autres options :
     * - la valeur de l'option
     * 
     * @param  string  $contraintes_param  Chaîne contenant tous les paramètres
     * 
     * @return array   Tableau associatif avec paramètres et valeurs séparés
     */
    public function explode_condition_contrainte($contraintes_param) {

        // Initialisation des variables
        $return = array();
        $listGroupes = "";
        $listSousgroupes = "";
        $affichage_sans_arborescence = "";

        // Sépare toutes les conditions avec leurs valeurs et les met dans un tableau
        $contraintes_params = explode(";", $contraintes_param);
        
        // Pour chaque paramètre de &contraintes
        foreach ($contraintes_params as $value) {
            // Récupère le mot-clé "liste_groupe" et les valeurs du paramètre
            if (strstr($value, "liste_groupe=")) { 
                // On enlève le mots-clé "liste_groupe=", on garde les valeurs
                $listGroupes = str_replace("liste_groupe=", "", $value);
            }
            // Récupère le mot-clé "liste_ssgroupe" et les valeurs du paramètre
            if (strstr($value, "liste_ssgroupe=")) { 
                // On enlève le mots-clé "liste_ssgroupe=", on garde les valeurs
                $listSousgroupes = str_replace("liste_ssgroupe=", "", $value);
            }
            // Récupère le mot-clé "affichage_sans_arborescence" et la valeur du
            // paramètre
            if (strstr($value, "affichage_sans_arborescence=")) { 
                // On enlève le mots-clé "affichage_sans_arborescence=", on garde la valeur
                $affichage_sans_arborescence = str_replace("affichage_sans_arborescence=", "", $value);
            }
        }

        // Récupère dans des tableaux la liste des groupes et sous-groupes qui  
        // doivent être utilisés lors du traitement de la condition
        if ($listGroupes != "") {
            $listGroupes = array_map('trim', explode(",", $listGroupes));
        }
        if ($listSousgroupes != "") {
            $listSousgroupes = array_map('trim', explode(",", $listSousgroupes));
        }

        // Tableau à retourner
        $return['groupes'] = $listGroupes;
        $return['sousgroupes'] = $listSousgroupes;
        $return['affichage_sans_arborescence'] = $affichage_sans_arborescence;
        return $return;
    }

    /**
     * Méthode qui complète la clause WHERE de la requête SQL de récupération des
     * contraintes, selon les paramètres fournis. Elle permet d'ajouter une condition sur
     * les groupes, sous-groupes et les services consultés.
     * 
     * @param  string  $part  Contient tous les paramètres fournis à &contraintes séparés
     *                 par des points-virgules, tel que définis dans l'état.
     * @param  array   $conditions  Paramètre optionnel, contient les conditions déjà
     *                 explosées par la fonction explode_condition_contrainte()
     * 
     * @return string  Contient les clauses WHERE à ajouter à la requête SQL principale.
     */
    public function treatment_condition_contrainte($part, $conditions = NULL) {

        // Initialisation de la condition
        $whereContraintes = "";
        // Lorsqu'on a déjà les conditions explosées dans le paramètre $conditions, on
        // utilise ces données. Sinon, on appelle la méthode qui explose la chaîne de 
        // caractères contenant l'ensemble des paramètres.
        if (is_array($conditions) === false){
            $conditions = $this->explode_condition_contrainte($part);
        }
        // Récupère les groupes et sous-groupes pour la condition
        $groupes = $conditions['groupes'];
        $sousgroupes = $conditions['sousgroupes'];

        // Pour chaque groupe
        if ($groupes != "") {
            foreach ($groupes as $key => $groupe) {
                // Si le groupe n'est pas vide
                if (!empty($groupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.groupe)) = lower('"
                        .pg_escape_string($groupe)."')";
                }
            }
            // S'il y a des valeurs dans groupe
            if (count($groupe) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Pour chaque sous-groupe
        if ($sousgroupes != "") {
            foreach ($sousgroupes as $key => $sousgroupe) {
                // Si le sous-groupe n'est pas vide
                if (!empty($sousgroupe)) {
                    // Choisit l'opérateur logique
                    $op_logique = $key > 0 ? 'OR' : 'AND (';
                    // Ajoute la condition
                    $whereContraintes .= " ".$op_logique." lower(trim(both E'\n\r\t' from contrainte.sousgroupe)) = lower('"
                        .pg_escape_string($sousgroupe)."')";
                }
            }
            // S'il y a des valeurs dans sous-groupe
            if (count($sousgroupes) > 0) {
                // Ferme la parenthèse
                $whereContraintes .= " ) ";
            }
        }

        // Condition retournée
        return $whereContraintes;
    }

    /**
     * [replace_contrainte description]
     * @param  [type] $objet     [description]
     * @param  [type] $objet_idx [description]
     * @param  [type] &$titre    [description]
     * @param  [type] &$corps    [description]
     * @return [type]            [description]
     */
    public function replace_contrainte($objet, $objet_idx, &$titre, &$corps) {

        switch ($objet) {
            case 'dc':
                require_once PATH_OPENMAIRIE.'../obj/dossier_coordination.class.php';
                $inst = new dossier_coordination($objet_idx);
                break;
            case 'etab':
                require_once PATH_OPENMAIRIE.'../obj/etablissement.class.php';
                $inst = new etablissement($objet_idx);
                break;
            default:
                return;
        }

        // Cas 1.1 : contraintes avec paramètres dans le titre
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $titre, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $titre = str_ireplace(
                $contraintes_params[0],
                $inst->construct_contraintes($contraintes_params),
                $titre
            );
        }

        // Cas 1.2 : contraintes sans paramètre dans le titre
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $titre, $matches_contraintes_titre);
        //
        foreach ($matches_contraintes_titre[0] as $key => $value) {
            //
            $titre = str_ireplace(
                $value,
                $inst->construct_contraintes(),
                $titre
            );
        }

        // Cas 2.1 : contraintes avec paramètres dans le corps
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $corps, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $corps = str_ireplace(
                $contraintes_params[0],
                $inst->construct_contraintes($contraintes_params),
                $corps
            );
        }

        // Cas 2.2 : contraintes sans paramètre dans le corps
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $corps, $matches_contraintes_corps);
        //
        foreach ($matches_contraintes_corps[0] as $key => $value) {
            //
            $corps = str_ireplace(
                $value,
                $inst->construct_contraintes(),
                $corps
            );
        }
    }

    public function empty_contrainte($objet, &$titre, &$corps) {
        // Cas 1.1 : contraintes avec paramètres dans le titre
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $titre, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $titre = str_ireplace(
                $contraintes_params[0],
                '',
                $titre
            );
        }

        // Cas 1.2 : contraintes sans paramètre dans le titre
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $titre, $matches_contraintes_titre);
        //
        foreach ($matches_contraintes_titre[0] as $key => $value) {
            //
            $titre = str_ireplace(
                $value,
                '',
                $titre
            );
        }

        // Cas 2.1 : contraintes avec paramètres dans le corps
        preg_match_all("/&contraintes_".$objet."\((.*)\)/", $corps, $matches_contraintes_params);
        //
        foreach ($matches_contraintes_params[0] as $key => $value) {
            //
            $contraintes_params[0] = $value;
            $contraintes_params[1] = $matches_contraintes_params[1][$key];
            //
            $corps = str_ireplace(
                $contraintes_params[0],
                '',
                $corps
            );
        }

        // Cas 2.2 : contraintes sans paramètre dans le corps
        preg_match_all("/&contraintes_".$objet."(?!\(.*\))/", $corps, $matches_contraintes_corps);
        //
        foreach ($matches_contraintes_corps[0] as $key => $value) {
            //
            $corps = str_ireplace(
                $value,
                '',
                $corps
            );
        }
    }

    // {{{

    /**
     *
     */
    function is_option_referentiel_ads_enabled() {
        //
        if ($this->getParameter('option_referentiel_ads') !== 'true') {
            return false;
        }
        //
        return true;
    }

    /**
     *
     */
    function get_inst_referentiel_ads() {
        require_once "../obj/interface_referentiel_ads.class.php";
        return new interface_referentiel_ads();
    }

    /**
     * Interface avec le référentiel ADS.
     */
    function execute_action_to_referentiel_ads($code, $infos) {
        $interface_referentiel_ads = $this->get_inst_referentiel_ads();
        return $interface_referentiel_ads->execute_action($code, $infos);
    }

    // }}}

}

?>
