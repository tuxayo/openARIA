<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à qualifier.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/dossier_coordination.class.php");

class dossier_coordination_a_qualifier extends dossier_coordination {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
