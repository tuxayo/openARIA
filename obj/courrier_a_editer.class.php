<?php
/**
 * Surcharge de la classe courrier pour afficher seulement les pièces qui sont
 * à editer.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/courrier.class.php");

class courrier_a_editer extends courrier {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
