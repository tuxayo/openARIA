<?php
/**
 * Ce fichier est destine a permettre la surcharge de certaines methodes de
 * la classe om_formulaire pour des besoins specifiques de l'application
 *
 * @package openmairie_exemple
 * @version SVN : $Id: om_formulaire.class.php 2382 2013-06-11 10:56:25Z fmichon $
 */

/**
 *
 */
require_once PATH_OPENMAIRIE."om_formulaire.class.php";

/**
 *
 */
class om_formulaire extends formulaire {

    function selecthidden($champ, $validation, $DEBUG = false) {
        $this->select($champ, $validation, $DEBUG);
    }

    /**
     * Widget - Saisie des références cadastrales.
     *
     * Ce type permet d'afficher un widget de formualire avec une aide à la 
     * saisie pour la saisie de références cadastrales.
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Etat de la validation du formulaire.
     * @param boolean $DEBUG      Parametre inutilise.
     *
     * @return void
     */
    function referencescadastrales($champ, $validation, $DEBUG = false) {
        //
        $this->textarea($champ, $validation, $DEBUG);
    }


    /**
     * La valeur du champ est passe par le controle hidden
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referencescadastralesstatic($champ, $validation, $DEBUG = false) {

        //
        if ($this->val[$champ] != '') {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le bouton pour la récupération des propriétaires des
        // parcelles
        if ($this->val[$champ] != '') {
            //
            $this->plot_owner($champ, $validation, $DEBUG);
        }
        //
        foreach (explode(';', $this->val[$champ]) as $key => $ref) {

            echo "<span class=\"reference-cadastrale-".$key."\">";
            echo $ref;
            echo "</span>&nbsp";
            
        }
    }

    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function textareahidden($champ, $validation, $DEBUG = false) {
        $this->textarea($champ, $validation, $DEBUG);
    }
    
    /**
     * Affiche un textarea avec un bouton
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoine($champ, $validation, $DEBUG = false){
        //
        echo "<textarea";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " cols=\"".$this->taille[$champ]."\"";
        echo " rows=\"".$this->max[$champ]."\"";
        if(!isset($this->select[$champ]['class'])) {
            $this->select[$champ]['class'] = "";
        }
        echo " class=\"champFormulaire noautoresize ".$this->select[$champ]['class']."\"";
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                echo " onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                echo " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                echo " onclick=\"".$this->onclick[$champ]."\"";
            }
        } else {
            echo " disabled=\"disabled\"";
        }
        echo ">\n";
        echo $this->val[$champ];
        echo "</textarea>\n";
        
        //Ajout du bouton si le paramètre option_referentiel_patrimoine est à true
        if ($this->select[$champ]['option_referentiel_patrimoine']=='true'){
            
            echo "<input";
            echo " id=\"lien_referentiel_patrimoine\"";
            echo " class=\"champFormulaire ui-button ui-widget ui-state-default ui-corner-all \"";
            echo " type=\"button\"";
            echo " onclick=\"getReferencesPatrimoine(this);\"";
            echo " value=\""._("Récupérer le(s) référence(s) patrimoine")."\"";
            echo " />\n";
        }
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinehidden($champ, $validation, $DEBUG = false){
        $this->referentielpatrimoine($champ, $validation, $DEBUG);
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinehiddenstatic($champ, $validation, $DEBUG = false){
        //
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        echo $this->val[$champ]."\n";
    }
    
    /**
     * La balise textarea et son libellé sont cachés grâce au CSS, cela permet
     * un traitement en javascript pour afficher/masquer le champ
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function referentielpatrimoinestatic($champ, $validation, $DEBUG = false){
        //
        echo "<span id=\"".$champ."\" class=\"field_value\">";
        echo $this->val[$champ];
        echo "</span>";
    }

    /**
     * Champ lien vers un enregistrement lié.
     *
     * Liste des paramètres à envoyer par la fonction setSelect :
     * array(
     *     "obj" => Formulaire à ouvrir
     *     "libelle" => Libellé de la valeur du champ
     *     "idx" => Identifiant de l'enregistrement, peut être remplacé par la
     *         valeur du champ
     * )
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function link($champ, $validation, $DEBUG = false) {

        // Récupère l'objet du champ
        $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : "";
        // Récupère la valeur du champ
        $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : "";
        if ($idx == "") {
            $idx = (isset($this->val[$champ])) ? $this->val[$champ] : "";
        }
        // Récupère le libelle de la valeur du champ
        $libelle = $idx;
        if (!empty($this->select[$champ]['libelle'])) {
            $libelle = $this->select[$champ]['libelle'];
        }
        // Récupère le libelle à envoyer dans l'URL pour alimenter le fil
        // d'Ariane
        $idz = "";
        if ($libelle != $idx) {
            $idz = $libelle;
        }

        // Liste des paramètres du pattern :
        // l'objet, l'identifiant de l'enregistrement, le libellé de 
        // l'enregistrement et la valeur du champ affiché
        $link = '<a id="link_'.$champ.'" class="lienFormulaire" href="form.php?';
        // Objet de l'enregistrement
        $link .= 'obj=%s';
        $link .= '&amp;action=3';
        // Identifiant de l'objet
        $link .= '&amp;idx=%s';
        // Libellé de l'enregistrement, est affiché dans le fil d'Ariane
        $link .= '&amp;idz=%s';
        $link .= '&amp;premier=0';
        $link .= '&amp;advs_id=';
        $link .= '&amp;recherche=';
        $link .= '&amp;tricol=';
        $link .= '&amp;selectioncol=';
        $link .= '&amp;valide=';
        $link .= '&amp;retour=tab">';
        // Valeur affichée
        $link .= '%s';
        $link .= '</a>';

        // Affiche la value dans un champ hidden pour être postée par le form
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
        // Affichage du champ
        printf($link, $obj, $idx, $idz, $libelle);
    }

    /**
     * Regroupe toutes les informations d'une adresse.
     *
     * @param string  $champ          Nom du champ
     * @param integer $validation     Validation du formulaire
     * @param boolean $DEBUG          Paramètre inutilisé
     * @param boolean $arrondissement Adresse composée d'un arrondissement
     *
     * @return void
     */
    function adresse($champ, $validation, $DEBUG = false, $arrondissement = false) {

        // Si des valeurs sont envoyées
        if (!empty($this->select[$champ]["values"])) {

            // Ajoute "BP" devant la boite postale si celle-ci est renseignée
            if (!empty($this->select[$champ]["values"]["boite_postale"])) {
                $this->select[$champ]["values"]["boite_postale"] = _("BP")." ".$this->select[$champ]["values"]["boite_postale"];
            }

            // Ajoute "Cedex" devant le cedex si celui-ci est renseigné
            if (!empty($this->select[$champ]["values"]["cedex"])) {
                $this->select[$champ]["values"]["cedex"] = _("Cedex")." ".$this->select[$champ]["values"]["cedex"];
            }

            // Découpage de l'adresse par ligne
            $adr = array();
            // Ligne 1
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_numero"],
                $this->select[$champ]["values"]["adresse_numero2"],
                $this->select[$champ]["values"]["adresse_voie"],
            );
            // Ligne 2
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_complement"],
            );
            // Ligne 3
            $adr[] = array(
                $this->select[$champ]["values"]["lieu_dit"],
                $this->select[$champ]["values"]["boite_postale"],
            );
            // Ligne 4
            // Vérifie si l'adresse est composée d'un arrondissement
            $adresse_arrondissement = "";
            if ($arrondissement == true) {
                $adresse_arrondissement = $this->select[$champ]["values"]["adresse_arrondissement"];
            }
            $adr[] = array(
                $this->select[$champ]["values"]["adresse_cp"],
                $this->select[$champ]["values"]["adresse_ville"],
                $adresse_arrondissement,
                $this->select[$champ]["values"]["cedex"],
            );

            // Compteur de ligne
            $i = 1;
            // Pour chaque ligne
            foreach ($adr as $ligne) {

                // Concat des données
                $value = $this->f->concat_text($ligne, " ");
                // Si la ligne n'est pas vide
                if (!empty($value)) {

                    // Texte d'une ligne à afficher
                    $text = '<span id ="'.$champ.'_adresse_ligne'.$i.'" class="field_value">';
                    $text .= $value;
                    $text .= "</span>";

                    // Affichage de la ligne
                    printf("<br>".$text);

                    // Incrémente le compteur
                    $i++;
                }
            }
        }
    }

    /**
     * Champ spécifique à la table etablissement, permet en mode consulter 
     * d'afficher en valeur de champ, toute les informations utiles de 
     * l'établissement.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function etablissement($champ, $validation, $DEBUG = false) {

        // Affiche le lien de redirection vers le SIG si l'établissement lié est géolocalisé
        if (isset($this->select[$champ]["values"]["geolocalise"]) && $this->select[$champ]["values"]["geolocalise"] !== "f") {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le libellé avec le type link
        $this->link($champ, $validation, $DEBUG);

        // Affiche l'adresse
        $this->adresse($champ, $validation, $DEBUG, true);

        // Si des valeurs sont envoyées
        if (!empty($this->select[$champ]["values"])) {
            //
            if($this->select[$champ]["values"]["etablissement_nature_libelle"]!=""){
                echo "<br/>";
                $this->champstatic("etablissement_nature_libelle", $this->select[$champ]["values"]["etablissement_nature_libelle"], $DEBUG);
            }
            //
            if ($this->select[$champ]["values"]["etablissement_type_libelle"]!=""||$this->select[$champ]["values"]["etablissement_categorie_libelle"]!=""){
                echo "<br/>";
                if ($this->select[$champ]["values"]["etablissement_type_libelle"]!=""){
                    $this->champstatic("etablissement_type_libelle", _("type")."&nbsp;".$this->select[$champ]["values"]["etablissement_type_libelle"], $DEBUG);
                }
                
                if ($this->select[$champ]["values"]["etablissement_categorie_libelle"]!=""){
                    echo "&nbsp;";
                    $this->champstatic("etablissement_categorie_libelle", _("categorie")."&nbsp;".$this->select[$champ]["values"]["etablissement_categorie_libelle"], $DEBUG);
                }
            }
            //
            if($this->select[$champ]["values"]["si_locaux_sommeil"]!=""){
                echo "<br/>";
                $this->champstatic("si_locaux_sommeil", $this->select[$champ]["values"]["si_locaux_sommeil"], $DEBUG);
            }
        }
    }

    /**
     * [contact_exploitant description]
     *
     * @param [type]  $champ      [description]
     * @param [type]  $validation [description]
     * @param boolean $DEBUG      [description]
     *
     * @return [type]  [description]
     */
    function contact_exploitant($champ, $validation, $DEBUG = false) {

        //
        if ($this->select[$champ]["values"] != "") {
            //
            $identite = array(
                $this->select[$champ]["values"]["civilite"],
                $this->select[$champ]["values"]["nom"],
                $this->select[$champ]["values"]["prenom"],
            );
            //
            $value = $this->f->concat_text($identite, " ");
            $text = '<span id ="'.$champ.'_identite" class="field_value">';
            $text .= $value;
            $text .= "</span>";
            //
            printf($text);
        }

        //
        $this->adresse($champ, $validation, $DEBUG);
    }

    /**
     * Affichage des données
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function champstatic($champ, $value, $DEBUG = false){
        //
        printf('<span id="%s"/>%s</span>',
            $champ,
            $value
        );
    }
    
    /**
     * Affichage des données concernant le dossier de coordination
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function dossier_coordination_data($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        $this->champstatic($champ, $this->select[$champ]["value"], $DEBUG);
    }
    
    /**
     * Affichage des données concernant l'établissement
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function etablissement_data($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        $this->champstatic($champ, $this->select[$champ]["libelle"], $DEBUG);
    }
    
    /**
     * Champ spécifique à la table dossier_coordination, permet  
     * d'afficher un lien vers le dossier de coordination lié
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function dossier_coordination_link($champ, $validation, $DEBUG = false) {
        
        // Affiche les données
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->select[$champ]['idx']."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        // Affiche le lien de redirection vers le SIG si le dossier de coordination lié est géolocalisé
        if (isset($this->select[$champ]["geolocalise"]) && $this->select[$champ]["geolocalise"] != "f") {
            $this->link_geolocalisation($champ, $validation, $DEBUG);
        }
        // Affiche le libellé avec le type link
        $this->link($champ, $validation, $DEBUG);
    }

    /**
     * Choix de l'heure et de la minute dans des listes à choix.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du form
     * @param boolean $DEBUG      Parametre inutilise
     */
    function heure_minute($champ, $validation, $DEBUG = false) {
        //
        $text_onchange = "";
        if (isset($this->onchange) and $this->onchange[$champ] != "") {
            $text_onchange = $this->onchange[$champ];
        }

        // Découpe la valeur du champ
        $explode_value = explode(":", $this->val[$champ]);
        // Récupère la valeur du select des heures
        $value_heure = (isset($explode_value[0])) ? $explode_value[0] : "";
        // Récupère la valeur du select des minutes
        $value_minute = (isset($explode_value[1])) ? $explode_value[1] : "";

        // Initialise le select des heures
        $this->setVal($champ."_heure", $value_heure);
        $contenu = array();
        $contenu[0][0] = "";
        $contenu[1][0] = _('choisir');
        for ($i=0; $i < 24; $i++) {
            $j = str_pad($i, 2, "0", STR_PAD_LEFT);
            $contenu[0][$i+1] = $j;
            $contenu[1][$i+1] = $j;
        }
        $this->setSelect($champ."_heure", $contenu);

        // Initialise le onChange du select des heures
        $this->setOnchange($champ."_heure", "heure_minute('".$champ."', 'heure');".$text_onchange);
        // Affiche le champ select des heures
        $this->select($champ."_heure", $validation, $DEBUG);

        // Affiche ":" entre les deux select
        printf("<span> : </span>");

        // Initialise le select des minutes
        $this->setVal($champ."_minute", $value_minute);
        $contenu = array();
        $contenu[0][0] = "";
        $contenu[0][1] = "00";
        $contenu[0][2] = "15";
        $contenu[0][3] = "30";
        $contenu[0][4] = "45";
        $contenu[1][0] = _('choisir');
        $contenu[1][1] = "00";
        $contenu[1][2] = "15";
        $contenu[1][3] = "30";
        $contenu[1][4] = "45";
        $this->setSelect($champ."_minute", $contenu);
        // Initialise le onChange du select des minutes
        $this->setOnchange($champ."_minute", "heure_minute('".$champ."', 'minute');".$text_onchange);
        // Affiche le champ select des minutes
        $this->select($champ."_minute", $validation, $DEBUG);

        // Valeur mise à jour par javascript
        // Affiche la value dans un champ hidden pour être postée par le form
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }

    /**
     * Création d'un input pour choisir une couleur
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function color($champ, $validation, $DEBUG = false) {

        // Inclusion de JSColor
        $this->f->addHTMLHeadJs("../app/lib/jscolor/jscolor.js");
        // Affiche l'input
        echo "<input";
        echo " type=\"text\"";
        echo " id=\"".$champ."\"";
        echo " size=\"6\"";
        echo " maxlength=\"6\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire color\"";
        echo " />\n";  
    }

    /**
     * Création d'un tableau pour choisir un ou plusieurs essais réalisés
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function essai_realise($champ, $validation, $DEBUG = false) {
        // Récupération de tous les essais réalisés
        // avec des informations supplémentaires pour ceux liés à l'analyse
        $essais = $this->val[$champ];
        // Ouverture du tableau
        $tableau = sprintf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">
                </th>
                <th class="title col-1 marge_essai_droite">
                </th>
                <th class="title col-2  center_th_essai_realise marge_essai_droite">
                    <span>%s</span>
                </th>
                <th class="title col-3 center_th_essai_realise">
                    <span>%s</span>
                </th>
                <th class="title col-4  center_th_essai_realise lastcol marge_essai_gauche">
                    <span>%s</span>
                </th>
            </tr></thead>';
        $tableau .= sprintf(
            $template_tab_header,
            _('essai'),
            _("concluant ?"),
            _('complement')
        );
        // Corps du tableau
        $tableau .= sprintf('<tbody>');
        $i = 0;
        // Affiche chacun des essais
        foreach ($essais as $essai => $valeurs) {
            $i++;
            ($i % 2 == 0) ? $odd = "" : $odd = "odd";
            // initialisation des attributs checked et disabled
            $disabled = '';
            $essai_checked = 'checked="checked"';
            $concluant_checked = 'checked="checked"';
            // récupération des attributs checked et disabled
            if ($valeurs["lien"] == 'f') {
                $essai_checked = "";
                $disabled = 'disabled="disabled"';
            }
            if ($valeurs["conc"] == 'f') {
                $concluant_checked = "";
            }
            // action JS à la (dé)sélection d'un essai réalisé
            $disable = ".removeAttr('disabled')";
            $enable = ".attr('disabled', 'disabled')";
            $select = sprintf("if (this.checked) {
                    this.value='t';
                    $('#conc_%s')%s;
                    $('#comp_%s')%s;
                } else {
                    this.value='f';
                    $('#conc_%s')%s;
                    $('#comp_%s')%s;
                }",
                $valeurs["id"],
                $disable,
                $valeurs["id"],
                $disable,
                $valeurs["id"],
                $enable,
                $valeurs["id"],
                $enable
            );
            // action JS lorsque l'on (dé)coche qu'un essai a été concluant
            $coche = "if (this.checked) this.value='t'; else this.value='f';";
            // ouverture de la ligne
            $tableau .= sprintf('<tr class="tab-data %s">',$odd);
            // checkbox de sélection
            $tableau .= sprintf('<td class="col-0 icons marge_essai_droite">
                <input id="essai_%s" type="checkbox" %s value="%s"
                    onclick="%s" name="essai[%s]" /></td>',
                $valeurs["id"],
                $essai_checked,
                $valeurs["lien"],
                $select,
                $valeurs["id"]
            );
            // Infobulle de l'essai s'il y a une description
            if ($valeurs["desc"] != '') {
                $tableau .= sprintf('<td class="col-1 icons marge_essai_droite">
                    <span title="%s" class="info-16"></span></td>',
                    $valeurs["desc"]
                );
            } // Sinon colonne vide
            else {
                $tableau .= sprintf('<td class="col-1 icons marge_essai_droite"></td>');
            }
            // Libellé de l'essai réalisé
            $tableau .= sprintf('<td class="col-2 marge_essai_droite">%s</td>',
                $valeurs['lib']
            );
            // Checkbox d'essai concluant ou non
            $tableau .= sprintf('<td class="col-3 icons">
                <input id="conc_%s" type="checkbox" %s value="%s"
                    onclick="%s" name="conc[%s]" /></td>',
                $valeurs["id"],
                $disabled.$concluant_checked,
                $valeurs["conc"],
                $coche,
                $valeurs["id"]
            );
            // Eventuel complément
            $tableau .= sprintf('<td class="col-4 lastcol marge_essai_gauche">
                <input id="comp_%s" type="text" %s size="75" name="comp[%s]"
                    value="%s"/></td>',
                $valeurs["id"],
                $disabled,
                $valeurs["id"],
                $valeurs["comp"]
            );
            // fermeture de la ligne
            $tableau .= sprintf('</tr>');
        }
        // fermeture du tableau
        $tableau .= sprintf('</tbody></table>');
        if ($i == 0) {
            printf('%s<br/>%s',
                _("Aucun essai realise disponible."),
                _("Veuillez configurer le parametrage.")
            );
        } else {
            printf($tableau);
        }
    }

    /**
     * Création d'un tableau pour choisir une ou plusieurs prescriptions
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean  $DEBUG      Paramètre inutilisé
     */
    function prescription($champ, $validation, $DEBUG = false) {
        // Récupération de toutes les prescriptions
        $essais = $this->val[$champ];
        // Création du bouton d'ajout d'une prescription
        $btn_add = sprintf('<span title="%s"
            class="boutons_prescription ui-icon ui-icon-plusthick"></span>',
            _("Ajouter une prescription")
        );
        // Ouverture du tableau
        printf('<table class="tab-tab">');
        // Entête de tableau
        $template_tab_header = '
            <thead><tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0 marge_essai_droite">%s</th>
                <th class="title col-1 marge_essai_droite">
                    <span class="name">%s</span>
                </th>
                <th class="title col-2 lastcol">
                    <span class="name">%s</span>
                </th>
            </tr></thead>';
        printf(
            $template_tab_header,
            $btn_add,
            _('Prescription reglementaire'),
            _('Prescriptions specifiques')
        );
        // Corps du tableau
        printf('<tbody>');
        $i = 0;
        // Affiche chacun des essais
        foreach ($essais as $essai => $valeurs) {
            $i++;
            ($i % 2 == 0) ? $odd = "" : $odd = "odd";
            // action JS lors de la demande de suppression de la prescription
            $js_delete = sprintf("delete_prescription(this);");
            // action JS lors de la demande de modification de la prescription
            $js_edit = sprintf("",
                $valeurs["id"]
            );
            // action JS lors de la demande de déplacement vers le bas
            $js_bas = sprintf("",
                $valeurs["ord"]
            );
            // action JS lors de la demande de déplacement vers le haut
            $js_haut = sprintf("",
                $valeurs["ord"]
            );
            // ouverture de la ligne
            printf('<tr id="prescription_%s" class="tab-data %s une_prescription">',
                $valeurs["ord"],
                $odd
            );
            // boutons d'actions JS
            printf('<td class="col-0 icons marge_essai_droite">
                <span id="delete_prescription_%s" title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-closethick"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-pencil"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-arrowthick-1-s"></span>
                <span title="%s" onclick="%s"
                class="boutons_prescription ui-icon ui-icon-arrowthick-1-n"></span>
                </td>',
                $valeurs["ord"],
                _("Supprimer cette prescription"),
                $js_delete,
                _("Modifier cette prescription"),
                $js_edit,
                _("Deplacer cette prescription vers le bas"),
                $js_bas,
                _("Deplacer cette prescription vers le haut"),
                $js_haut
            );
            // Description de la prescription réglementaire
            printf('<td class="col-1 marge_essai_droite">%s</td>',
                ($valeurs["pr_id"] == '') ? "-" : $valeurs["pr_desc"]
            );
            // Description de la prescription spécifique
            printf('<td class="col-2 lastcol">%s</td>',
                $valeurs['ps_desc']
            );
            // fermeture de la ligne
            printf('</tr>');
        }
        // fermeture du tableau
        printf('</tbody></table>');
    }

    /**
     * httpclick - lien http en formulaire - passage d argument sur une
     * application tierce
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function httpclick($champ, $validation, $DEBUG = false) {

        //
        if (isset($this->select[$champ][0])) {
            $aff = $this->select[$champ][0];
        } else {
            $aff = $champ;
        }
        //
        echo "<a id='".$champ."' href='#' onclick=\"".$this->val[$champ]."; return false;\" class='insert' >";
        echo $aff;
        echo "</a>\n";

    }

    /**
     * Affiche une liste de courrier.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean $DEBUG      [description]
     *
     * @return void
     */
    function listecourrierstatic($champ, $validation, $DEBUG = false) {
        // Affiche les données
        echo "<div id=\"list_courrier_".$champ."\">";
        echo "<ul>";
        foreach($this->select[$champ] as $id => $infos) {
            echo "<li>".$infos["contact"]."</li>";
        }
        echo "</ul>";
        echo "</div>";
    }

    /**
     * Affiche une liste de courrier avec lien de téléchargement.
     *
     * @param string   $champ      Nom du champ
     * @param integer  $validation 
     * @param boolean $DEBUG      [description]
     *
     * @return void
     */
    function listecourrier($champ, $validation, $DEBUG = false) {
        if(empty($this->select[$champ])) {
            return;
        }
        if ($this->f->storage == null) {
            // Message d'erreur
            echo _("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        // Affiche les données
        echo "<div id=\"list_courrier_".$champ."\" class=\"listecourrierFormulaire\">";
        echo "<ul>";
        foreach ($this->select[$champ] as $id => $infos) {
            //
            if ($infos["om_fichier_finalise_courrier"] != "") {
                echo "<li>";
                //
                $filename = $this->f->storage->getFilename($infos["om_fichier_finalise_courrier"]);
                //
                if ($filename != ""
                    && $filename != 'OP_FAILURE') {
                    //
                    echo "<span class=\"om-prev-icon reqmo-16\" title=\""._("Enregistrer le fichier")."\">";
                    echo "<a href=\"../spg/file.php?obj=".$infos["obj"]."&amp;champ=".$infos["champ"].
                            "&amp;id=".$id."\" target=\"_blank\" class=\"lienFormulaire\">";
                    //
                    echo $filename;
                    echo " - "._("Destinataire")." : ";
                    echo $infos["contact"];
                    echo "</a>";
                    echo "</span>";
                } else {
                    //
                    echo _("Le fichier n'existe pas ou n'est pas accessible.");
                }
                echo "</li>";
            }
        }
        echo "</ul>";
        echo "</div>";
    }

    /**
     * Permet d'afficher la liste des valeurs d'un select_multiple avec les
     * valeurs passaient dans le formulaire.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du formulaire
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function hidden_static_select_multiple($champ, $validation, $DEBUG = false) {
        //
        $this->select_multiple_static($champ, $validation, $DEBUG);
        //
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }


    /**
     * Crée un input hidden d'un contact
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function hidden_contact($champ, $validation, $DEBUG = false) {
        echo "<input";
        echo " type=\"hidden\"";
        echo " name=\"contact[]\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$this->val[$champ]."\"";
        echo " />\n";
    }

    /**
     * La valeur du champ est passe par le controle hidden
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     */
    function hiddenstatic($champ, $validation, $DEBUG = false) {

        //
        echo "<input";
        echo " type=\"hidden\"";
        echo " id=\"".$champ."\"";
        echo " name=\"".$champ."\"";
        echo " value=\"".$this->val[$champ]."\"";
        echo " class=\"champFormulaire\"";
        echo " />\n";
        echo "<span id=\"".$champ."_value\" class=\"field_value\">";
        echo $this->val[$champ]."\n";
        echo "</span>";

    }

    /**
     * WIDGET_FORM - select_multiple_static.
     *
     * Ce widget permet d'afficher une liste statique (html) des valeurs 
     * d'un champ. Cette liste de valeurs provient de la combinaison entre les 
     * valeurs et libellés disponibles dans le paramétrage select de ce champ
     * et entre les valeurs du champ représentées de manière linéaire. 
     * 
     * Deux contraintes sont présentes ici : 
     *  - $this->val[$champ] correspond aux valeurs sélectionnées. Le format 
     *    attendu ici dans la valeur du champ est une chaine de caractère 
     *    représentant la liste des valeurs sélectionnées séparées par des ; 
     *    (points virgules). 
     *    Exemple : $this->val[$champ] = string(5) "4;2;3";
     *  - $this->select[$champ] correspond aux libellés de toutes les valeurs 
     *    disponibles dans cette liste lors de la modification de l'élément.
     *    Exemple : $this->select[$champ] = array(2) {
     *         [0] => array(3) {
     *           [0] => string(1) "2"
     *           [1] => string(1) "3"
     *           [2] => string(1) "4"
     *         }
     *         [1] => array(3) {
     *           [0] => string(5) "Plans"
     *           [1] => string(7) "Visites"
     *           [2] => string(18) "Dossiers à enjeux"
     *         }
     *       }
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function select_multiple_static($champ, $validation, $DEBUG = false) {
        // Si aucune valeur n'est sélectionnée alors on affiche rien
        if ($this->val[$champ] == "") {
            return;
        }
        // On transforme la chaine de caractère en tableau grâce au
        // séparateur ;
        $selected_values = explode(";", $this->val[$champ]);
        // On affiche la liste
        echo "<ul id='".$champ."'>";
        // On boucle sur la liste de valeurs sélectionnées
        foreach ($selected_values as $value) {
            //
            echo "<li>";
            // On affiche le libellé correspondant à la valeur
            echo $this->select[$champ][1][array_search($value, $this->select[$champ][0])];
            //
            echo "</li>";
        }
        //
        echo "</ul>";
    }

    /**
     * WIDGET_FORM - file.
     *
     * Surcharge pour utiliser un champ différent de celui de base.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function file($champ, $validation, $DEBUG = false) {
        // Récupération du paramétrage si renseigné
        $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : $this->getParameter("obj");
        $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : $this->getParameter("idx");
        $val = (isset($this->select[$champ]['val'])) ? $this->select[$champ]['val'] : $this->val[$champ];
        $field = (isset($this->select[$champ]['champ'])) ? $this->select[$champ]['champ'] : $champ;
        // Si le storage n'est pas configuré, alors on affiche un message
        // d'erreur clair pour l'utilisateur
        echo "<div id=\"".$champ."\">";
        if ($this->f->storage == null) {
            // Message d'erreur
            echo _("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        //
        if ($val != "") {
            //
            $filename = $this->f->storage->getFilename($val);
            //
            if ($filename != ""
                && $filename != 'OP_FAILURE') {
                //
                echo $filename;
                //
                $link = "../spg/voir.php?obj=".$obj."&amp;champ=".$field."&amp;id=".$idx;
                //
                echo "<span class=\"om-prev-icon consult-16\" title=\""._("Ouvrir le fichier")."\">";
                echo "<a href=\"javascript:load_form_in_modal('".$link."');\" >";
                echo _("Visualiser");
                echo "</a>";
                echo "</span>";
                //
                echo "<span class=\"om-prev-icon reqmo-16\" title=\""._("Enregistrer le fichier")."\">";
                echo "<a href=\"../spg/file.php?obj=".$obj."&amp;champ=".$field.
                        "&amp;id=".$idx."\" target=\"_blank\">";
                echo _("Telecharger");
                echo "</a>";
                echo "</span>";
            } else {
                echo _("Le fichier n'existe pas ou n'est pas accessible.");
            }
        }
        echo "</div>";
    }

    /**
     * WIDGET_FORM - filehiddenstatic.
     *
     * Affichage du nom du fichier ou d'une erreur si le fichier est
     * inaccessible avec la valeur passée dans le formulaire.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation du formulaire
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function filehiddenstatic($champ, $validation, $DEBUG=false) {
        //
        $this->filestatic($champ, $validation, $DEBUG);
        //
        $this->setType($champ, "hidden");
        $this->hidden($champ, $validation, $DEBUG);
    }

    /**
     * WIDGET_FORM - filestatic.
     *
     * Surcharge pour utiliser un champ différent de celui de base.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function filestatic($champ, $validation, $DEBUG = false) {
        // Récupération du paramétrage si renseigné
        $val = (isset($this->select[$champ]['val'])) ? $this->select[$champ]['val'] : $this->val[$champ];
        // Si le storage n'est pas configuré, alors on affiche un message
        // d'erreur clair pour l'utilisateur
        echo "<div id=\"".$champ."\">";
        if ($this->f->storage == null) {
            // Message d'erreur
            echo _("Le syteme de stockage n'est pas accessible. Erreur de ".
                   "parametrage. Contactez votre administrateur.");
            echo "</div>";
            // On sort de la méthode
            return -1;
        }
        if ($val != "") {
            $filename = $this->f->storage->getFilename($val);
            if ($filename != ""
                && $filename != "OP_FAILURE") {
                //
                echo $filename;
            } else {
                echo _("Le fichier n'existe pas ou n'est pas accessible.");
            }
        }
        echo "</div>";
    }

    /**
     *
     * Créée un lien de redirection vers l'objet dans le référentiel ADS.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function link_dossier_autorisation_ads($champ, $validation, $DEBUG = false) {
        //
        $this->hiddenstatic($champ, $validation, $DEBUG);
        //
        if (isset($this->select[$champ]["link"]) && $this->val[$champ] != "") {
            printf(
                '
<a id="%s_link_openads" target="blank" href="%s">
<span class="om-icon om-icon-16 om-icon-fix external-link-16" title="%s">
%s
</span>
</a>
',
                $champ,
                $this->select[$champ]["link"],
                _('Voir dans openADS'),
                _('Voir dans openADS')
            );
        }
    }

    /**
     *
     * Créée un lien de redirection vers l'objet dans le SIG.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function link_geolocalisation($champ, $validation, $DEBUG = false) {
        //
        if ($this->f->is_option_sig_enabled() === false) {
            return;
        }
        // Si le lien est affiché dans un champ de références cadastrales ou le champ
        // geolocalise, on utilise l'objet et l'idx courant
        if ($champ === 'references_cadastrales' OR $champ === 'geolocalise') {
            $obj = $this->getParameter('obj');
            $idx = $this->getParameter('idx');
        }
        else {
            // Récupère l'objet du champ
            $obj = (isset($this->select[$champ]['obj'])) ? $this->select[$champ]['obj'] : '';
            $idx = (isset($this->select[$champ]['idx'])) ? $this->select[$champ]['idx'] : '';
            if ($idx == '') {
                $idx = (isset($this->val[$champ])) ? $this->val[$champ] : '';
            }
        }

        if ($obj != '' AND $idx != '') {
            echo "<a id=\"".$champ."_localiser_sig_externe\" target=\"blank\"".
                 " href=\"form.php?obj=".$obj."&amp;action=30&amp;idx=".$idx;
            if ($champ === 'references_cadastrales') {
                echo "&amp;option=parcelles";
            }
            echo "\">";
            echo "<span class=\"om-icon om-icon-16 om-icon-fix sig-16\" title=\"Localiser\">"._('Localiser')."</span>";
            echo "</a>";
        }
    }


    /**
     * WIDGET_FORM - enjeu_ads
     *
     * Cette méthode permet d'ajouter un élément dans le widget de enjeu ADS.
     * Elle affiche un triangle jaune si le champ est a 1 't' ou 'Oui'.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function enjeu_ads($champ, $validation, $DEBUG = false){
        if ($this->val[$champ] == 1 || $this->val[$champ] == "t"
            || $this->val[$champ] == "Oui") {
            $value = "Oui";
        } else {
            $value = "Non";
        }
        printf(
            '<span id="%s" class="om-icon om-icon-16 om-icon-fix enjeu-ads-16 field_value">%s</span>',
            $champ,
            $value
        );
    }


    /**
     * WIDGET_FORM - enjeu_erp
     *
     * Cette méthode permet d'ajouter un élément dans le widget de enjeu ERP.
     * Elle affiche un triangle rose si le champ est a 1 't' ou 'Oui'.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function enjeu_erp($champ, $validation, $DEBUG = false){
        if ($this->val[$champ] == 1 || $this->val[$champ] == "t"
            || $this->val[$champ] == "Oui") {
            $value = "Oui";
        } else {
            $value = "Non";
        }
        printf(
            '<span id="%s" class="om-icon om-icon-16 om-icon-fix enjeu-erp-16 field_value">%s</span>',
            $champ,
            $value
        );
    }

    /**
     * WIDGET_FORM - autocomplete_link_selection
     *
     * Cette méthode permet d'ajouter un élément dans le widget de formulaire autocomplete.
     * Elle est à surcharger dans chaque application selon les besoins.
     *
     * @param string  $champ      Nom du champ
     * @param integer $validation Validation
     * @param boolean $DEBUG      Parametre inutilise
     *
     * @return void
     */
    function autocomplete_link_selection($champ, $validation, $DEBUG = false) {
        //
        if ($this->f->is_option_sig_enabled() === false
            OR isset($this->select[$champ]['obj']) === false
            OR ($this->select[$champ]['obj'] != 'etablissement_tous')) {
            return;
        }
        $obj = $this->select[$champ]['obj'];
        echo "<a id=\"autocomplete-".$obj."-link-selection\"  class=\"autocomplete autocomplete-empty ui-state-default ui-corner-all\"".
             " onclick=\"overlay_etablissements_proches_qualification()\">";
        echo "<span title=\""._("Liste d'établissements proches")."\" class=\"om-icon om-icon-16 localiser-16\">";
        echo _("Liste d'établissements proches");
        echo "</span>";
        echo "</a>\n";
    }

    /**
     * WIDGET_FORM - button
     *
     * Cette méthode permet de créer un input type button
     * 
     * @param   string   $champ       Nom du champ
     * @param   integer  $validation  Validation
     * @param   boolean  $DEBUG       Parametre inutilise
     * @return  void
     */
    function button($champ, $validation, $DEBUG = false) {
        // PARAMS
        $name = '';
        $text_onchange="";
        $text_onkeyup="";
        $text_onclick="";
        if (isset($this->select[$champ]) === true
            && isset($this->select[$champ]['name']) === true) {
            $name = $this->select[$champ]['name'];
        }
        if (!$this->correct) {
            if (isset($this->onchange) and $this->onchange[$champ] != "") {
                $text_onchange=" onchange=\"".$this->onchange[$champ]."\"";
            }
            if (isset($this->onkeyup) and $this->onkeyup[$champ] != "") {
                $text_onkeyup= " onkeyup=\"".$this->onkeyup[$champ]."\"";
            }
            if (isset($this->onclick) and $this->onclick[$champ] != "") {
                $text_onclick= " onclick=\"".$this->onclick[$champ]."\"";
            }
        } 
        // Affichage bouton
        echo "<input";
        echo " type=\"button\"";
        echo " name=\"".$champ."\"";
        echo " id=\"".$champ."\" ";
        echo " value=\"".$name."\"";
        if ($text_onchange !== '') {
            echo $text_onchange;
        }
        if ( $text_onkeyup !== '') {
            echo $text_onkeyup;
        }
        if ($text_onclick !== '') {
            echo  $text_onclick;
        }
       //
        echo " />\n";
       //
    }

    /**
     * WIDGET_FORM - plot_owner
     *
     * Bouton de récupération des propriétaires de parcelles sur un formulaire
     * en consultation. Utilisable seulement dans le widget de formulaire
     * 'referencescadastralesstatic'.
     * L'objet, l'identifiant, l'action et le titre de l'overlay doivent être
     * passés par le setSelect de l'objet
     *
     * @param string  $champ      Nom du champ.
     * @param integer $validation Niveau de validation.
     * @param boolean $DEBUG      Parametre inutilisé.
     *
     * @return void
     */
    public function plot_owner($champ, $validation, $DEBUG = false) {

        // Vérifie l'option SIG
        if ($this->f->is_option_sig_enabled() === false) {
            return;
        }

        // Récupère les paramètres obligatoire passés par le setSelect
        if (isset($this->select[$champ]['obj']) === false) {
            return;
        }
        if (isset($this->select[$champ]['idx']) === false) {
            return;
        }
        if (isset($this->select[$champ]['action']) === false) {
            return;
        }
        if (isset($this->select[$champ]['title']) === false) {
            return;
        }
        $obj = $this->select[$champ]['obj'];
        $idx = $this->select[$champ]['idx'];
        $action = $this->select[$champ]['action'];
        $title = $this->select[$champ]['title'];

        // Construit l'url pour le traitement ajax
        $link = '../scr/form.php?obj='.$obj.'&action='.$action.'&idx='.$idx;

        // Message à afficher en cas d'erreur du traitement ajax
        $msg_error = _("Une erreur s'est produite lors de la récupération des propriétaires.") . ' ' . _("Veuillez contacter votre administrateur.");
        // Si le message d'erreur est définit dans le setSelect de l'objet
        // alors on affiche celui-ci
        if (isset($this->select[$champ]['msg_error']) === true) {
            //
            $msg_error = $this->select[$champ]['msg_error'];
        }
        //
        $msg_error = addslashes($msg_error);

        // Affichage du bouton
        if ($obj !== '' && $obj !== null
            && $idx !== '' && $idx !== null
            && $action !== '' && $action !== null
            && $title !== '' && $title !== null
            && $msg_error !== '' && $msg_error !== null) {
            //
            $text = _('Récupération des propriétaires des parcelles');
            //
            $template = '<a id="%1$s_get_plot_owner" href="javascript:overlay_load_form(\'%2$s\', \'%3$s\', \'%4$s\', \'%5$s\', \'%6$s\', \'%7$s\', \'%8$s\')">
                <span class="om-icon om-icon-16 om-icon-fix contact-16" title="%9$s">
                    %9$s
                </span>
            </a>';

            //
            printf(
                $template,
                $champ,
                $title,
                $link,
                $msg_error,
                'auto',
                'auto',
                'left top',
                false,
                $text
            );
        }
    }

    /**
     * WIDGET_FORM - datetimehiddenstatic.
     *
     * Affiche les champs au format datetime 'Y-m-d H:i:s' au format
     * 'd/m/Y H:i:s' en mode hiddenstatic.
     *
     * @param string $champ Nom du champ
     * @param integer $validation
     * @param boolean $DEBUG Parametre inutilise
     *
     * @return void
     */
    function datetimehiddenstatic($champ, $validation, $DEBUG = false) {
        //
        $datetime_aff = "";
        if ($this->val[$champ] != "") {
            if (DateTime::createFromFormat('Y-m-d H:i:s', $this->val[$champ]) !== false) {
                $dateFormat = new DateTime($this->val[$champ]);
                $datetime_aff = $dateFormat->format('d/m/Y H:i:s');
            }
        }
        //
        if (!$this->correct) {
            echo "<input type='hidden' ";
            echo "name='".$champ."' ";
            echo "id=\"".$champ."\" ";
            echo "value=\"".$this->val[$champ]."\" ";
            echo "class='champFormulaire' />\n";
        }
        echo $datetime_aff."";
    }

}

?>
