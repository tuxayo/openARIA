<?php
/**
 * Surcharge de la classe autorite_police pour afficher
 * seulement les autorités de police non notifiées ou executées.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/autorite_police.class.php");

class autorite_police_non_notifiee_executee extends autorite_police {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
