<?php
/**
 * Surcharge de la classe etablissement_unite.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/etablissement_unite.class.php";

class etablissement_unite__contexte_di_analyse__ua_en_analyse extends etablissement_unite {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        /**
         *
         */
        //
        $this->class_actions = array();
        foreach (array(0, 1, 3, ) as $key) {
            if (isset($this->class_actions_available[$key])) {
                $this->class_actions[$key] = $this->class_actions_available[$key];
            }
        }
    }

    /**
     *
     */
    function setValFAjout($val = array()) {
        //
        parent::setvalFAjout($val);
        //
        $this->valF['etat'] = "enprojet";
        //
        $this->valF['dossier_instruction'] = $this->getParameter("idxformulaire");
        //
        $inst_di = $this->get_inst_dossier_instruction($this->getParameter("idxformulaire"));
        $inst_etab = $inst_di->get_inst_etablissement();
        $this->valF['etablissement'] = $inst_etab->getVal("etablissement");
    }

}

?>
