<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/autorite_police_decision.class.php";

class autorite_police_decision extends autorite_police_decision_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Initialisation des actions supplémentaires de la classe.
     */
    function init_class_actions() {
        parent::init_class_actions();
        // ACTION - 004 - view_json_nomenclature_actes
        // Tableau json représentant la nomenclature actes.
        $this->class_actions[4] = array(
            "identifier" => "view_json_nomenclature_actes",
            "view" => "view_json_nomenclature_actes",
            "permission_suffix" => "consulter",
        );
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //type
        if ($maj==0){ //ajout
            $form->setType('avis', 'select');
            $form->setType('type', 'select');
            $form->setType('nomenclature_actes_nature', 'select');
            $form->setType('nomenclature_actes_matiere_niv1', 'select');
            $form->setType('nomenclature_actes_matiere_niv2', 'select');
            $form->setType('arrete_reglementaire', 'select');
            $form->setType('arrete_notification', 'select');
            $form->setType('arrete_publication', 'select');
            $form->setType('arrete_temporaire', 'select');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('avis', 'select');
            $form->setType('type', 'select');
            $form->setType('nomenclature_actes_nature', 'select');
            $form->setType('nomenclature_actes_matiere_niv1', 'select');
            $form->setType('nomenclature_actes_matiere_niv2', 'select');
            $form->setType('arrete_reglementaire', 'select');
            $form->setType('arrete_notification', 'select');
            $form->setType('arrete_publication', 'select');
            $form->setType('arrete_temporaire', 'select');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('avis', 'selectstatic');
            $form->setType('arrete_reglementaire', 'selectstatic');
            $form->setType('arrete_notification', 'selectstatic');
            $form->setType('arrete_publication', 'selectstatic');
            $form->setType('arrete_temporaire', 'selectstatic');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('avis', 'selectstatic');
            $form->setType('type', 'selectstatic');
            $form->setType('nomenclature_actes_nature', 'selectstatic');
            $form->setType('nomenclature_actes_matiere_niv1', 'selectstatic');
            $form->setType('nomenclature_actes_matiere_niv2', 'selectstatic');
            $form->setType('arrete_reglementaire', 'selectstatic');
            $form->setType('arrete_notification', 'selectstatic');
            $form->setType('arrete_publication', 'selectstatic');
            $form->setType('arrete_temporaire', 'selectstatic');
        }//fin consulter
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        //
        $form->setSelect("avis", $avis);
        // Select du champ "nomenclature_actes_nature"
        $nomenclature_actes_nature = array(
            array("", "arretes_reglementaires", "arretes_individuels", ),
            array(_("Choisir")." "._("nature"), _("Arrêtés réglementaires"), _("Arrêtés individuels"), ),
        );
        $form->setSelect('nomenclature_actes_nature', $nomenclature_actes_nature);

        /**
         * Nomenclature Actes
         */
        //
        $nomenclature_actes = $this->get_nomenclature_actes();
        //// * NIVEAU 1
        //
        $nomenclature_actes_matiere_niv1_values = array_keys($nomenclature_actes);
        $nomenclature_actes_matiere_niv1_labels = $nomenclature_actes_matiere_niv1_values;
        //
        array_unshift($nomenclature_actes_matiere_niv1_values, "");
        array_unshift($nomenclature_actes_matiere_niv1_labels, _("Choisir")." "._("niveau 1"));
        //
        $nomenclature_actes_matiere_niv1 = array(
            $nomenclature_actes_matiere_niv1_values,
            $nomenclature_actes_matiere_niv1_labels,
        );
        $form->setSelect('nomenclature_actes_matiere_niv1', $nomenclature_actes_matiere_niv1);
        //// * NIVEAU 2
        //
        $nomenclature_actes_matiere_niv2_values = array("", );
        $nomenclature_actes_matiere_niv2_labels = array(_("Choisir")." "._("niveau 2"), );
        //
        $nomenclature_actes_matiere_niv2 = array();
        foreach ($nomenclature_actes as $key => $value) {
            $nomenclature_actes_matiere_niv2 = array_merge(
                $nomenclature_actes_matiere_niv2,
                array_values($value)
            );
        }
        //
        $nomenclature_actes_matiere_niv2_values = array_merge(
            $nomenclature_actes_matiere_niv2_values,
            $nomenclature_actes_matiere_niv2
        );
        $nomenclature_actes_matiere_niv2_labels = array_merge(
            $nomenclature_actes_matiere_niv2_labels,
            $nomenclature_actes_matiere_niv2
        );
        //
        $nomenclature_actes_matiere_niv2 = array(
            $nomenclature_actes_matiere_niv2_values,
            $nomenclature_actes_matiere_niv2_labels,
        );
        $form->setSelect('nomenclature_actes_matiere_niv2', $nomenclature_actes_matiere_niv2);

        /**
         *
         */
        // Booléens (pour la gestion du null)
        $booleens = array(
            array("null", "t", "f"),
            array(_("Choisir..."), _("Oui"), _("Non")),
        );
        $form->setSelect('arrete_reglementaire', $booleens);
        $form->setSelect('arrete_notification', $booleens);
        $form->setSelect('arrete_publication', $booleens);
        $form->setSelect('arrete_temporaire', $booleens);
    }

    /**
     *
     */
    function setvalF($val = array()) {
        //
        parent::setvalF($val);
        
        // Surcharge des booléens transformés en select pour accepter le null
        $boolean_three_values_fields = array(
            "arrete_reglementaire", 
            "arrete_notification",
            "arrete_publication",
            "arrete_temporaire",
        );
        foreach ($boolean_three_values_fields as $field_id) {
            //
            switch ($val[$field_id]) {
                //
                case 1:
                case "t":
                case "Oui":
                    $this->valF[$field_id] = true;
                    break;
                //
                case "null":
                case "NULL":
                    $this->valF[$field_id] = null;
                    break;
                //
                default:
                    $this->valF[$field_id] = false;
                    break;
            }
        }
    }

    /**
     *
     */
    function setLayout(&$form, $maj) {
        //
        $form->setBloc($this->clePrimaire, 'D', "", "form_autorite_police_decision_content_bloc");
        //
        $form->setBloc($this->clePrimaire, 'D', "", "main");
        $form->setBloc("type_arrete", 'F', "", "");
        //
        $form->setBloc("arrete_reglementaire", 'D', "", "arrete_parameters");
        $form->setBloc("nomenclature_actes_matiere_niv2", 'F', "", "");
        //
        $form->setBloc("nomenclature_actes_matiere_niv2", 'F', "", "");
    }

    /**
     *
     */
    function setOnchange(&$form, $maj) {
        //
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange("type_arrete", "handle_ap_decision_type_arrete();");
        $form->setOnchange("nomenclature_actes_matiere_niv1", "handle_filter_select_nomenclature_actes();");
    }


    /**
     * Vocabulaire 'nomenclature_actes' destiné aux champs :
     * - nomenclature_actes_matiere_niv1
     * - nomenclature_actes_matiere_niv2
     */
    function get_nomenclature_actes() {
        //
        $nomenclature_actes = array(
            "1 Commande Publique" => array(
                "1.1 Marchés publics",
                "1.2 Délégation de service public",
                "1.3 Conventions de Mandat",
                "1.4 Autres types de contrats",
                "1.5 Transactions /protocole d accord transactionnel",
                "1.6 Actes relatifs à la maîtrise d'oeuvre",
                "1.7 Actes speciaux et divers",
            ),
            "2 Urbanisme" => array(
                "2.1 Documents d urbanisme",
                "2.2 Actes relatifs au droit d occupation ou d utilisation des sols",
                "2.3 Droit de preemption urbain",
            ),
            "3 Domaine et patrimoine" => array(
                "3.1 Acquisitions",
                "3.2 Alienations",
                "3.3 Locations",
                "3.4 Limites territoriales",
                "3.5 Autres actes de gestion du domaine public",
                "3.6 Autres actes de gestion du domaine prive",
            ),
            "4 Fonction publique" => array(
                "4.1 Personnel titulaires et stagiaires de la F.P.T.",
                "4.2 Personnel contractuel",
                "4.3 Fonction publique hospitaliere",
                "4.4 Autres categories de personnels",
                "4.5 Regime indemnitaire",
            ),
            "5 Institutions et vie politique" => array(
                "5.1 Election executif",
                "5.2 Fonctionnement des assemblees",
                "5.3 Designation de representants",
                "5.4 Delegation de fonctions",
                "5.5 Delegation de signature",
                "5.6 Exercice des mandats locaux",
                "5.7 Intercommunalite",
                "5.8 Decision d ester en justice",
            ),
            "6 Libertés publiques et pouvoirs de police" => array(
                "6.1 Police municipale",
                "6.2 Pouvoir du president du conseil general",
                "6.3 Pouvoir du president du conseil regional",
                "6.4 Autres actes reglementaires",
                "6.5 Actes pris au nom de l Etat et soumis au controle hierarchique",
            ),
            "7 Finances locales" => array(
                "7.1 Decisions budgetaires",
                "7.2 Fiscalité",
                "7.3 Emprunts",
                "7.4 Interventions economiques",
                "7.5 Subventions",
                "7.6 Contributions budgetaires",
                "7.7 Avances",
                "7.8 Fonds de concours",
                "7.9 Prise de participation (SEM, etc...)",
                "7.10 Divers",
            ),
            "8 Domaines de competences par themes" => array(
                "8.1 Enseignement",
                "8.2 Aide sociale",
                "8.3 Voirie",
                "8.4 Amenagement du territoire",
                "8.5 Politique de la ville-habitat-logement",
                "8.6 Emploi-formation professionnelle",
                "8.7 Transports",
                "8.8 Environnement",
                "8.9 Culture",
            ),
            "9 Autres domaines de competences" => array(
                "9.1 Autres domaines de competences des communes",
                "9.2 Autres domaines de competences des departements",
                "9.3 Autres domaines de competences des regions",
                "9.4 Voeux et motions",
            ),
        );
        //
        return $nomenclature_actes;
    }

    /**
     *
     */
    function view_json_nomenclature_actes() {
        //
        $this->checkAccessibility();
        //
        echo json_encode($this->get_nomenclature_actes());
    }
}

?>
