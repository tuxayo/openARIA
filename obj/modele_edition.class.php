<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/modele_edition.class.php";

class modele_edition extends modele_edition_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Permet de définir le type des champs.
     * 
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);

        // En mode consuultation
        $form->setType("om_etat", "hidden");

        if ($maj == 3) {
            // Le cahmp om_lettretype n'est pas obligatoire alors on vérifie 
            // qu'il est saisi avant de paramétrer le lien
            if ($this->getVal("om_lettretype") != "") {
                //
                $form->setType("om_lettretype", "link");
            }
        }
    }

    /**
     * [get_om_lettretype_id description]
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    function get_om_lettretype_id($id) {
        //
        $om_lettretype_id = "";
        //
        if (!empty($id)) {
            // Instance de la classe om_lettretype
            require_once '../obj/om_lettretype.class.php';
            $om_lettretype = new om_lettretype($id);

            //
            $om_lettretype_id = $om_lettretype->getVal('id');
        }

        //
        return $om_lettretype_id;
    }

    /**
     * [get_modele_edition_by_code description]
     *
     * @param [type] $code [description]
     *
     * @return [type] [description]
     */
    function get_modele_edition_by_code($code) {
        //
        $modele_edition_id = "";
        //
        if (!empty($code)) {
            //
            $sql = "SELECT modele_edition
                    FROM ".DB_PREFIXE."modele_edition
                    WHERE LOWER(code) = LOWER('".$this->f->db->escapeSimple($code)."')";
            $modele_edition_id = $this->f->db->getOne($sql);
            $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($modele_edition_id);
        }

        //
        return $modele_edition_id;
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // En mode consuultation
        if ($maj == 3) {
            // Le cahmp om_lettretype n'est pas obligatoire alors on vérifie 
            // qu'il est saisi avant de paramétrer le lien
            if ($this->getVal("om_lettretype") != "") {
                // Paramétres envoyés au type link om_lettretype
                $params = array();
                $params['obj'] = "om_lettretype";
                $params['libelle'] = $form->select['om_lettretype'][1][0];
                $params['idx'] = $this->getVal("om_lettretype");
                $form->setSelect("om_lettretype", $params);
            }
        }


    }

}

?>
