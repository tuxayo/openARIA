<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once ("../gen/obj/periodicite_visites.class.php");

class periodicite_visites extends periodicite_visites_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 010 - update_all_dossier_coordination_periodique
        // Cette action permet de mettre à jour tous les établissements
        $this->class_actions[10] = array(
            "identifier" => "update_all_dossier_coordination_periodique",
            "view" => "formulaire",
            "method" => "update_all_dossier_coordination_periodique",
            "button" => "update_all_dossier_coordination_periodique",
            "permission_suffix" => "treatment_periodicity",
        );

        // ACTION - 099 - controlpanel
        $this->class_actions[99] = array(
            "identifier" => "controlpanel",
            "view" => "view_controlpanel",
            "permission_suffix" => "controlpanel",
        );
    }

    /**
     * Récupère l'identifiant de la periodicité de visite grâce au type et à la
     * catégorie de l'établisement ainsi qu'à son caractère de locaux à sommeil.
     *
     * @param integer $etablissement_type           Type de l'établissement.
     * @param integer $etablissement_categorie      Catégorie de l'établissement.
     * @param boolean $etablissement_locaux_sommeil Caractère locaux à sommeil de l'établissement.
     *
     * @return integer
     */
    function get_periodicite_by_type_categorie_locaux_sommeil($etablissement_type, $etablissement_categorie, $etablissement_locaux_sommeil) {

        // Initialisation du résultat
        $id = "";

        // Gestion du caractère locaux à sommeil
        if ($etablissement_locaux_sommeil === true) {
            $field_locaux_sommeil = "avec_locaux_sommeil";
        } elseif ($etablissement_locaux_sommeil === false) {
            $field_locaux_sommeil = "sans_locaux_sommeil";
        } else {
            return $id;
        }

        // Si le type et la catégorie de l'établissement sont renseignés
        if (!empty($etablissement_type)
            && !empty($etablissement_categorie)) {

            // Requête SQL
            $sql = sprintf(
                'SELECT 
                    periodicite_visites
                FROM 
                    %1$speriodicite_visites
                WHERE 
                    etablissement_type = %2$s
                    AND etablissement_categorie = %3$s
                    AND %4$s IS TRUE',
                DB_PREFIXE,
                intval($etablissement_type),
                intval($etablissement_categorie),
                $field_locaux_sommeil
            );
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $id = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($id);
        }

        // Retourne l'identifiant
        return $id;
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        if ($maj == 10) {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
        }
    }

    /**
     * TREATMENT - update_all_dossier_coordination_periodique.
     *
     * Traite tous les établissements concernant le dossier de coordination
     * de visites périodiques obligatoire sur les ERP Référentiel.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function update_all_dossier_coordination_periodique($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // Récupère la liste de tous les établissements
        require_once "../obj/etablissement.class.php";
        $etablissement = new etablissement(0);
        $list_etablissement = $etablissement->get_list_etablissement();
        // Compteur établissement mise à jour
        $cpt_updated = 0;
        // Compteur établissement non mise à jour
        $cpt_not_updated = 0;

        // Pour chaque établissement
        foreach ($list_etablissement as $etablissement_id) {

            // Instance de l'établissement
            require_once "../obj/etablissement.class.php";
            $etablissement = new etablissement($etablissement_id);

            // Traitement des dossiers de coordination périodique
            $handle_dossier_coordination_periodique = $etablissement->handle_dossier_coordination_periodique("all");

            // Si le traitement est effectué sur l'établissement
            if ($handle_dossier_coordination_periodique == true) {
                // Ajoute au compteur des établissements mises à jour
                $cpt_updated++;
            } else {
                // Ajoute au compteur des établissements non mises à jour
                $cpt_not_updated++;
            }
        }

        // Affiche les messages à l'utilisateur
        $this->addToMessage(sprintf(_("Nombre d'etablissement total : %s"), "<span class='bold'>".count($list_etablissement)."</span>"));
        $this->addToMessage(sprintf(_("Nombre d'etablissement mise a jour : %s"), "<span class='bold'>".$cpt_updated."</span>"));
        $this->addToMessage(sprintf(_("Nombre d'etablissement ignore : %s"), "<span class='bold'>".$cpt_not_updated."</span>"));

        // Return
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     *
     */
    function getFormTitle($ent) {
        if ($this->getParameter("maj") == 99) {
            return _("administration_parametrage")." -> "._("Gestion de la périodicité");
        } else {
            return $ent;
        }
    }

    /**
     * VIEW - view_controlpanel.
     *
     * @return void
     */
    function view_controlpanel() {
        //
        $this->checkAccessibility();
        //
        $this->retour();
        /**
         * Traitements
         */
        //
        echo "<br/>";
        //
        echo "<br/>";
        $this->f->displaySubTitle("Traitement");
        printf(
            '
            <ul class="portlet-list">
            <li>
            <a 
            href="#"
            id="action-controlpanel-periodicite_visites-update_all_dossier_coordination_periodique"
            class="action action-direct action-with-confirmation"
            title="Ce traitement permet de mettre à jour la périodicité des visites pour tous les établissements et dossiers de coordination concernés"
            data-href="../scr/form.php?obj=periodicite_visites&amp;action=10&amp;idx=0">
            <span class="om-prev-icon om-icon-16">Cliquer ici pour déclencher le traitement de périodicité</span>
            </a>
            </li>
            </ul>
            <br/>'
        );
        //
        echo "<br/>";
        $this->f->displaySubTitle("Configuration");
        //
        echo "<u>paramètres :</u>";
        $parametres = array(
            "etablissement_nature_periodique",
            "etablissement_etat_periodique",
            "dossier_coordination_type_periodique",
        );
        $out = '';
        foreach ($parametres as $parametre) {
            $out .= sprintf(
                "- %s : <b>%s</b>\n",
                $parametre,
                var_export($this->f->getParameter($parametre), true)
            );
        }
        printf(
            '<pre>%s</pre>',
            $out
        );
        /**
         *
         */
        echo "<br/>";
        $this->retour();
    }

}

?>
