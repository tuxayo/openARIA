<?php
//$Id$ 
//gen openMairie le 10/08/2016 17:34

require_once "../gen/obj/contrainte.class.php";

class contrainte extends contrainte_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        // Champs cachés
        $form->setType('contrainte', 'hidden');
        $form->setType('id_referentiel', 'hidden');

        // En mode ajouter et modifier
        if ($maj == 0 || $maj == 1) {
            $form->setType('nature', 'select');
            $form->setType('lie_a_un_referentiel', 'hidden');
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        }
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");
        }

        // nature
        $form->setSelect("nature", $nature);
    }

    /**
     * Méthode de mise en page.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {

        //
        $form->setFieldset("libelle", "D", _("Contrainte"));
        $form->setFieldset("ordre_d_affichage", "F");
        //
        $form->setFieldset("groupe", "D", _("Categorie"));
        $form->setFieldset("sousgroupe", "F");
        //
        $form->setFieldset("texte", "D", _("Detail"));
        $form->setFieldset("om_validite_fin", "F");
        
    }

    /**
     * Récupère le texte de la contrainte paramétrée selon un ordre de préférence.
     *
     * Cette méthode est appelée par les contraintes appliquées afin de définir le texte complété.
     * 
     * @return string texte préférentiel
     */
    public function get_texte_preferentiel() {
        // préférence au texte surchargé si défini
        if ($this->getVal('texte_surcharge') !== '') {
            return $this->getVal('texte_surcharge');
        }
        // sinon au texte si défini
        if ($this->getVal('texte') !== '') {
            return $this->getVal('texte');
        }
        // sinon au libellé qui lui est forcément défini
        return $this->getVal('libelle');
    }

    /**
     * Permet de savoir si une contrainte SIG fait partie des
     * contraintes paramétrées actives, et si oui de récupérer
     * son identifiant.
     *
     * @param  string  $id_ref ID référentiel de la contrainte SIG
     * @throws treatment_exception
     * @return integer clé primaire de la contrainte paramétrée
     */
    public function get_id_by_referentiel($id_referentiel) {
        //
        $sql = "SELECT contrainte
            FROM ".DB_PREFIXE."contrainte
            WHERE id_referentiel='$id_referentiel'
            AND lie_a_un_referentiel = 't'
            AND (om_validite_fin IS NULL OR om_validite_fin > CURRENT_DATE)";
        $this->f->addToLog(__METHOD__.": db->getOne(\"".$sql."\");", VERBOSE_MODE);
        $id_contrainte = $this->f->db->getOne($sql);
        //
        if (database::isError($id_contrainte, true)) {
            throw new treatment_exception(
                _("Erreur de base de donnees. Contactez votre administrateur."),
                __METHOD__.": ".$id_contrainte->getMessage()
            );
        }
        //
        return $id_contrainte;
    }
}

?>
