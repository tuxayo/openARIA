<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once ("../gen/obj/etablissement_statut_juridique.class.php");

class etablissement_statut_juridique extends etablissement_statut_juridique_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 090 - get_code_statut_juridique
        //
        $this->class_actions[90] = array(
            "identifier" => "get_code_statut_juridique",
            "view" => "get_code_statut_juridique",
            "permission_suffix" => "json",
        );

    }

    /**
     * VIEW - get_code_statut_juridique.
     * 
     * Retourne en JSON le code du statut juridique posté.
     *
     * @return void
     */
    function get_code_statut_juridique() {
        /*
        $enteteTab, $validation, $maj, &$db, $postVar, $aff,
        $DEBUG = false, $idx, $premier = 0, $recherche = "",
        $tricol = "", $idz = "", $selectioncol = "",
        $advs_id = "", $valide = "", $retour = "", $actions = array(),
        $extra_parameters = array()) {
        */
        $this->f->disableLog();

        $postvar = $this->getParameter("postvar");
        $id_statut = $postvar['id_statut'];
        
        $sql = "SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_statut_juridique
                WHERE etablissement_statut_juridique = ".$id_statut;
        $this->f->addToLog("get_code_statut_juridique() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $code = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($code);

        // Traitement du retour :
        echo json_encode(array(
            "resultat"=>$code
            )
        );
    }

}// fin classe

?>
