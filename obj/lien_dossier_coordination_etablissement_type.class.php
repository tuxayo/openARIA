<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/lien_dossier_coordination_etablissement_type.class.php";

class lien_dossier_coordination_etablissement_type extends lien_dossier_coordination_etablissement_type_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Supprime tous les enregistrements liés à un dossier de coordination donné.
     *
     * @param integer $dc Identifiant du DC
     *
     * @return boolean
     */
    function delete_by_dossier_coordination($dc) {
        // Récupère la liste des enregistrements
        $liens = $this->get_liens_by_dossier_coordination($dc);
        // S'il y a des liens
        if (is_array($liens) && count($liens) > 0) {
            // Pour chaque enregistrement
            foreach ($liens as $lien) {
                // Instancie l'enregistrment
                $obj = new lien_dossier_coordination_etablissement_type(
                    $lien['lien_dossier_coordination_etablissement_type']
                );
                // initialisation de la clé primaire
                $val= array(
                    'lien_dossier_coordination_etablissement_type' => $lien['lien_dossier_coordination_etablissement_type'],
                );
                // Supprime l'enregistrement
                if ($obj->supprimer($val) == false) {
                    return false;
                }
            }
        }
        //
        return true;
    }

    /**
     * Récupère la liste des liens par l'identifiant du dossier de coordination.
     *
     * @param integer $dc Identifiant du DC
     *
     * @return boolean
     */
    function get_liens_by_dossier_coordination($dc) {
        // Initialisation de la variable de résultat
        $result = array();
        // Si l'autorité de police est renseigné
        if (!empty($dc)) {
            // Requête SQL
            $sql = sprintf(
                'SELECT *
                FROM %slien_dossier_coordination_etablissement_type
                WHERE dossier_coordination = %s',
                DB_PREFIXE,
                intval($dc)
            );
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $result[] = $row;
            }
        }
        // Retourne le résultat
        return $result;
    }

}

?>
