<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

require_once "../core/obj/om_logo.class.php";

class om_logo extends om_logo_core {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        "uid" => array(
            "titre" => "get_md_titre",
            "description" => "get_md_description",
        ),
    );

    function get_md_titre() { return "Logo ".$this->getVal("libelle"); }
    function get_md_description() { return "logo"; }

    // }}} END - METADATA FILESTORAGE

}

?>
