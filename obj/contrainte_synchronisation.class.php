<?php
//$Id$ 

require_once '../obj/contrainte.class.php';

class contrainte_synchronisation extends contrainte {

    // Abstracteur SIG
    var $geoaria = null;

    //
    // VIEWS
    //

    /**
     * VIEW - view_synchroniser.
     *
     * Permet de synchroniser les contraintes du SIG depuis une interface.
     *
     * @return void
     */
    function view_synchroniser() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Traitement et affichage message si formulaire validé
        if ($this->f->get_submitted_post_value('valider_synchronisation') === 'oui') {
            //
            $this->begin_treatment_with_transaction(__METHOD__);
            //
            try {
                $this->synchroniser();
                $this->end_treatment_with_transaction(__METHOD__);
            } catch (treatment_exception $e) {
                $this->addToMessage($e->getMessage());
                $this->end_treatment_with_transaction(__METHOD__, false);
            }
            //
            $this->display_msg();
        }

        // Formulaire de validation
        printf("<form method=\"POST\" action=\"\" name=\"f1\">");
        printf('<div class="formEntete ui-corner-all">');
        $this->f->displayDescription(_("Cette page permet de synchroniser les contraintes paramétrées et liées à un référentiel avec celles provenant du SIG."));
        printf('<input type="hidden" name="valider_synchronisation" id="valider_synchronisation" value="oui" />');
        //
        printf("</div><div class=\"formControls\">");
        $btn = array(
            "value" => _("Synchroniser"),
            "name" => "btn_synchroniser"
        );
        $this->f->layout->display_form_button($btn);
        printf("</div></form>");
    }

    //
    // CONDITIONS
    //

    /**
     * CONDITION - can_synchroniser.
     * 
     * Méthode permettant de tester la condition nécessaire
     * à la synchronisation des contraintes.
     * 
     * @return boolean
     */
    function can_synchroniser() {
        // L'option SIG doit être activée
        if ($this->is_option_sig_enabled() === true) {
            return true;
        }
        return false;
    }

    //
    // TREATMENTS
    //

    /**
     * TREATMENT - synchroniser.
     *
     * Synchronise les contraintes
     *
     * @param  array $val Valeurs soumises par le formulaire
     * @throws treatment_exception
     * @return void
     */
    function synchroniser($val = array()) {
        // Récupération de toutes les contraintes du référentiel SIG
        $c_SIG_list = $this->get_contraintes_sig();

        // Récupération des ID référentiels de toutes les contraintes paramétrées
        // actives et liées à un référentiel.
        $c_PARAM_list = $this->get_id_ref_from_contraintes_parametrees();

        // Init compteurs
        $tot_del = 0; $tot_add = 0; $tot_up = 0;
        // Pour chaque contrainte paramétrée active et liée à un référentiel,
        // on l'archive si son ID référentiel ne fait pas partie des contraintes SIG récupérées
        foreach ($c_PARAM_list as $id_referentiel) {
            if (array_key_exists($id_referentiel, $c_SIG_list) === false) {
                $this->disable_contrainte_parametree($id_referentiel);
                $tot_del++;
            }
        }
        // Pour chaque contrainte SIG récupérée :
        // - on ajoute une contrainte paramétrée si aucune n'a son ID référentiel
        // - sinon on met à jour la correspondante
        foreach ($c_SIG_list as $c_SIG) {
            if (in_array($c_SIG['id_referentiel'], $c_PARAM_list) === false) {
                $this->add_contrainte_parametree($c_SIG);
                $tot_add++;
                continue;
            }
            $this->update_contrainte_parametree($c_SIG);
            $tot_up++;
        }

        //
        $this->addToMessage($this->contruct_msg_val($tot_add, $tot_up, $tot_del));
    }

    /**
     * Archive une contrainte à la date du jour
     *
     * @param  string $id_referentiel identifiant unique dans le SIG
     * @throws treatment_exception
     * @return void
     */
    private function disable_contrainte_parametree($id_referentiel) {
        //
        $err_mess = _("Une erreur s'est produite lors de l'archivage des contraintes.")
            ." "._("Contactez votre administrateur.");
        //
        $id = $this->get_id_by_referentiel($id_referentiel);
        //
        $valF = array(
            "om_validite_fin" =>  date('Y-m-d'),
        );
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        //
        if (database::isError($res, true)) {
            throw new treatment_exception(
                $err_mess,
                __METHOD__.": ".$res->getMessage()
            );
        }
    }

    /**
     * Crée une nouvelle contrainte paramétrée active correspondant
     * à la contrainte SIG fournie.
     * 
     * @param  array $c_SIG
     * @throws treatment_exception
     * @return void
     */
    private function add_contrainte_parametree($c_SIG) {
        //
        $err_mess = _("Une erreur s'est produite lors de l'ajout des contraintes.")
            ." "._("Contactez votre administrateur.");
        //
        $contrainte = new contrainte(']');
        //
        $val = array(
            'contrainte' => ']',
            'id_referentiel' => $c_SIG['id_referentiel'],
            'nature' => 'PLU',
            'groupe' => $c_SIG['groupe'],
            'sousgroupe' => (isset($c_SIG['sousgroupe']))?$c_SIG['sousgroupe']:"",
            'libelle' => $c_SIG['libelle'],
            'lie_a_un_referentiel' => true,
            'texte' => $c_SIG['texte'],
            'texte_surcharge' => null,
            'ordre_d_affichage' => null,
            'om_validite_debut' => date('d/m/Y'),
            'om_validite_fin' => null,
        );
        //
        if ($contrainte->ajouter($val, $this->f->db, DEBUG) === false) {
            throw new treatment_exception(
                $err_mess
            );
        }
    }

    /**
     * Modifie la contrainte paramétrée active correspondant
     * à la contrainte SIG fournie.
     * 
     * @param  array $c_SIG
     * @throws treatment_exception
     * @return bool
     */
    private function update_contrainte_parametree($c_SIG) {
        //
        $err_mess = _("Une erreur s'est produite lors de la mise à jour des contraintes.")
            ." "._("Contactez votre administrateur.");
        //
        $id = $this->get_id_by_referentiel($c_SIG['id_referentiel']);
        //
        $contrainte = new contrainte($id);
        //
        $val = array();
        foreach ($contrainte->champs as $key => $champ) {
            $val[$champ] = $contrainte->val[$key];
        }
        $val['groupe'] = $c_SIG['groupe'];
        $val['sousgroupe'] = (isset($c_SIG['sousgroupe']))?$c_SIG['sousgroupe']:"";
        $val['libelle'] = $c_SIG['libelle'];
        $val['texte'] = $c_SIG['texte'];
        //
        if ($contrainte->modifier($val, $this->f->db, DEBUG) === false) {
            throw new treatment_exception(
                $err_mess
            );
        }
    }

    //
    // REQUÊTES SQL
    //

    /**
     * Récupération des ID référentiels de toutes les contraintes paramétrées
     * actives et liées à un référentiel.
     *
     * @throws treatment_exception
     * @return array
     */
    private function get_id_ref_from_contraintes_parametrees() {

        // Initialisation résultat
        $c_PARAM_list = array();

        // Requête SQL
        $sql = "SELECT id_referentiel
            FROM ".DB_PREFIXE."contrainte
            WHERE lie_a_un_referentiel = 't'
            AND (om_validite_fin IS NULL OR om_validite_fin > CURRENT_DATE)";
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
        $res = $this->f->db->query($sql);
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                _("Erreur de base de donnees. Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }

        // Tableau des résultats
        while ($row = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $c_PARAM_list[] = $row['id_referentiel'];
        }

        //
        return  $c_PARAM_list;
    }

    //
    // REQUÊTES WS
    //

    /**
     * Récupération de toutes les contraintes du référentiel SIG
     *
     * @throws treatment_exception
     * @return array
     */
    private function get_contraintes_sig() {
        $insee = null;
        if (isset($this->f->collectivite['insee']) === true) {
            $insee = $this->f->collectivite['insee'];
        }
        try {
            // WS OUT
            $geoaria = $this->f->get_inst_geoaria();
            return $geoaria->synchro_contraintes($insee);
        } catch (geoaria_exception $e) {
            throw new treatment_exception(
                $this->handle_geoaria_exception($e)
            );
        }
    }

    //
    // HELPERS
    //

    /**
     * Construit le message de validation de la synchronisation
     * 
     * @param  integer $tot_add nombre de contraintes ajoutées
     * @param  integer $tot_up  nombre de contraintes mises à jour
     * @param  integer $tot_del nombre de contraintes archivées
     * @return string
     */
    function contruct_msg_val($tot_add, $tot_up, $tot_del) {
        //
        $tot = $tot_add+$tot_up+$tot_del;
        $puce = '&nbsp;&bull;&nbsp;';
        $lf = '<br/>';
        //
        $msg_temp = _('%1$s contrainte%2$s paramétrée%2$s synchronisée%2$s dont :').$lf
            .$puce._('%3$s archivée%4$s').$lf
            .$puce._('%5$s ajoutée%6$s').$lf
            .$puce._('%7$s mise%8$s à jour');
        return sprintf($msg_temp,
            $tot, $this->f->pluriel($tot),
            $tot_del, $this->f->pluriel($tot_del),
            $tot_add, $this->f->pluriel($tot_add),
            $tot_up, $this->f->pluriel($tot_up)
        );
    }

    //
    // OVERRIDE
    //

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 001 - modifier
        // Désactivation de l'action modifier
        $this->class_actions[1] = null;

        // ACTION - 002 - supprimer
        // Désactivation de l'action supprimer
        $this->class_actions[2] = null;

        // ACTION - 003 - consulter
        // Désactivation de l'action consulter
        $this->class_actions[3] = null;

        // ACTION - 004 - synchroniser
        // Synchronisation des contraintes
        $this->class_actions[4] = array(
            "identifier" => "synchroniser",
            "view" => "view_synchroniser",
            "permission_suffix" => "synchroniser",
            "condition" => "can_synchroniser",
        );
    }
}

?>
