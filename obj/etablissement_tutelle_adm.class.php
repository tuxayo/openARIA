<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once ("../gen/obj/etablissement_tutelle_adm.class.php");

class etablissement_tutelle_adm extends etablissement_tutelle_adm_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);
        //
		if ($validation==0) {
			if ($maj == 0){
				$form->setVal("om_validite_debut", date('Y-m-d'));
			}
		}
	}

}// fin classe
?>
