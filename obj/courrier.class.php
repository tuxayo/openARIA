<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/courrier.class.php";

class courrier extends courrier_gen {

    /**
     * On définit le type global de champs spécifiques.
     */
    var $abstract_type = array(
        "om_fichier_finalise_courrier" => "file",
        "om_fichier_signe_courrier" => "file",
    );

    /**
     * Définit le nom du champ pour la finalisation.
     *
     * @var string
     */
    var $finalized_field = "finalise";

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        //
        "lien_courrier_contact" => array(
            "table_l" => "lien_courrier_contact",
            "table_f" => "contact",
            "field" => "contacts_lies",
        ),
        //
        "lien_autorite_police_courrier" => array(
            "table_l" => "lien_autorite_police_courrier",
            "table_f" => "autorite_police",
            "field" => "autorites_police_liees",
        ),
    );

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 002 - supprimer
        // Supprime l'enregistrement
        $this->class_actions[2]["condition"] = "is_deletable";

        // ACTION - 004 - finalise
        // Finalise le courrier, si c'est un mailing crée un courrier enfant
        // par destinataire
        $this->class_actions[4] = array(
            "identifier" => "finalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("finaliser"),
                "order" => 100,
                "class" => "finalize-16",
            ),
            "view" => "formulaire",
            "method" => "finalize",
            "button" => "finalise",
            "permission_suffix" => "finalise",
            "condition" => "is_finalizable",
        );

        // ACTION - 005 - definalise
        // Définalise le courrier, si c'est un mailing supprime tous les
        // courriers enfants
        $this->class_actions[5] = array(
            "identifier" => "definalise",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("definaliser"),
                "order" => 110,
                "class" => "unfinalize-16",
            ),
            "view" => "formulaire",
            "method" => "unfinalize",
            "button" => "definalise",
            "permission_suffix" => "definalise",
            "condition" => "is_unfinalizable",
        );

        // ACTION - 006 - overlay_courrier_texte_type
        // Propositions d'ajout de textes-types
        $this->class_actions[6] = array(
            "identifier" => "overlay_courrier_texte_type",
            "view" => "view_overlay_courrier_texte_type",
            "permission_suffix" => "modifier",
        );

        // ACTION - 007 - form_suivi
        // Formulaire de suivi du document généré par code barres
        $this->class_actions[7] = array(
            "identifier" => "form_suivi",
            "view" => "view_form_suivi",
            "permission_suffix" => "form_suivi",
        );

        // ACTION - 008 - form_rar
        // Formulaire pour l'impression des étiquettes rar
        $this->class_actions[8] = array(
            "identifier" => "form_rar",
            "view" => "view_form_rar",
            "permission_suffix" => "form_rar",
        );

        // ACTION - 010 - pdf_rar
        // Affichage du pdf rar
        $this->class_actions[10] = array(
            "identifier" => "pdf_rar",
            "view" => "view_pdf_rar",
            "permission_suffix" => "form_rar",
        );

        // ACTION - 009 - 
        // Affiche/cache le multi-select des autorités de police
        $this->class_actions[9] = array(
            "identifier" => "display_autorites_police_liees",
            "view" => "view_display_autorites_police_liees",
            "permission_suffix" => "modifier",
        );

        // ACTION - 011 - previsualiser
        //
        $this->class_actions[11] = array(
            "identifier" => "previsualiser",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => _("previsualiser"),
                "order" => 120,
                "class" => "pdf-16",
            ),
            "view" => "view_edition",
            "permission_suffix" => "previsualiser",
            "condition" => "is_previewable",
        );

        // ACTION - 051 - send_pv_to_referentiel_ads
        //
        $this->class_actions[51] = array(
            "identifier" => "send_pv_to_referentiel_ads",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Transmettre au référentiel ADS"),
                "order" => 120,
                "class" => "send-pv-to-referentiel-ads-16",
            ),
            "method" => "send_pv_to_referentiel_ads",
            "button" => "send_pv_to_referentiel_ads",
            "permission_suffix" => "send_pv_to_referentiel_ads",
            "condition" => array(
                "is_option_referentiel_ads_enabled",
                "is_type_proces_verbal",
                "is_dc_connected_to_referentiel_ads",
                "is_pv_sendable_to_referentiel_ads",
            ),
        );

    }


    /**
     *
     */
    var $arrete_number = null;

    /**
     *
     */
    function set_arrete_number($arrete_number) {
        $this->arrete_number = $arrete_number;
    }

    /**
     *
     */
    function get_arrete_number() {
        if ($this->arrete_number !== null) {
            return $this->arrete_number;
        }
        return $this->getVal("arrete_numero");
    }

    /**
     *
     * @param array $val
     * @param null &$dnu1 @deprecated Ancienne ressource de base de données.
     * @param null $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        //
        parent::verifier($val, $dnu1, $dnu2);
        // Un document généré de type ARRÊTÉ ne peut pas être un mailing (avoir
        // plusieurs contacts liés) sinon on ne saurait pas à quel document rattaché
        // le numéro d'arrêté.
        if ($this->is_type_arrete() === true
            || $this->get_courrier_type_code($val["courrier_type"]) == "dec") {
            //
            $contacts_lies = explode(";", $val["contacts_lies"]);
            if (count($contacts_lies) > 1) {
                //
                $this->addToMessage("Impossible d'avoir plusieurs contacts liés sur un Arrêté.");
                $this->correct = false;
                return false;
            }
        }
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $maj);
        //
        $form->setLib("contacts_lies", _("contacts_lies"));
        $form->setLib("autorites_police_liees", _("autorites_police_liees"));
        $form->setLib("courrier_texte_type_complement1", "");
        $form->setLib("courrier_texte_type_complement2", "");
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        $form->setType("etablissement", "hidden");
        $form->setType("proces_verbal", "hidden");
        $form->setType("visite", "hidden");
        //
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            $form->setType("dossier_instruction", "link");
            $form->setType("dossier_coordination", "link");
            $form->setType("courrier_parent", "link");
            if (isset($form->val["etablissement"])
                && !empty($form->val["etablissement"])) {
                $form->setType("etablissement", "link");
            }
            if (isset($form->val["proces_verbal"])
                && !empty($form->val["proces_verbal"])) {
                $form->setType("proces_verbal", "selecthiddenstatic");
            }
            if (isset($form->val["visite"])
                && !empty($form->val["visite"])) {
                $form->setType("visite", "selecthiddenstatic");
            }
        }

        //
        if ($maj == 2 || $maj == 3) {
            $form->setType("courrier_joint", "link");
        }

        // En mode ajout
        if ($maj == 0) {
            //
            $form->setType('contacts_lies', 'select_multiple');
            $form->setType('autorites_police_liees', 'select_multiple');
            // Champs à cacher
            $form->setType('om_date_creation', 'hidden');
            $form->setType('date_finalisation', 'hidden');
            $form->setType('date_envoi_signature', 'hidden');
            $form->setType('date_retour_signature', 'hidden');
            $form->setType('date_envoi_controle_legalite', 'hidden');
            $form->setType('date_retour_controle_legalite', 'hidden');
            $form->setType('date_envoi_rar', 'hidden');
            $form->setType('date_retour_rar', 'hidden');
            $form->setType("dossier_instruction", "hidden");
            $form->setType("dossier_coordination", "hidden");
            $form->setType("courrier_parent", "hidden");
            $form->setType("mailing", "hidden");
            $form->setType("finalise", "hidden");
            $form->setType("code_barres", "hidden");
            $form->setType("courrier_joint", "autocomplete");
            $form->setType('om_fichier_finalise_courrier', 'hidden');
            $form->setType('date_envoi_mail_om_fichier_finalise_courrier', 'hidden');
            $form->setType('om_fichier_signe_courrier', 'hidden');
            $form->setType('date_envoi_mail_om_fichier_signe_courrier', 'hidden');
            $form->setType('signataire', 'select');
            $form->setType('courrier_texte_type_complement1', 'httpclick');
            $form->setType('courrier_texte_type_complement2', 'httpclick');
        }// fin ajout

        // En mode modifier
        if ($maj == 1) {
            // Le type de courrier n'est pas modifiable
            // En fonction du type du courrier, il y a des comportements annexes qui
            // sont impossibles à tous contrôler si ce type de courrier est modifiable.
            // Par exemple : Arrêté (DEC) ou Notification d'autorité de police (NOTIFAP)
            $form->setType('courrier_type', 'selecthiddenstatic');
            // date de création non modifiable
            $form->setType('om_date_creation', 'hiddenstaticdate');
            $form->setType('date_finalisation', 'hiddenstaticdate');
            $form->setType('contacts_lies', 'select_multiple');
            $form->setType('autorites_police_liees', 'select_multiple');
            $form->setType("code_barres", "hiddenstatic");
            $form->setType("mailing", "checkboxhiddenstatic");
            $form->setType("finalise", "checkboxhiddenstatic");
            $form->setType('om_fichier_finalise_courrier', 'filestaticedit');
            $form->setType("courrier_joint", "autocomplete");
            $form->setType('courrier_texte_type_complement1', 'httpclick');
            $form->setType('courrier_texte_type_complement2', 'httpclick');
            //
            if ($this->retourformulaire == "") {
                $form->setType('om_fichier_signe_courrier', 'upload');
            } else {
                $form->setType('om_fichier_signe_courrier', 'upload2');
            }
            //
            if ($this->getVal('mailing') == 't') {
                $form->setType("courrier_parent", "hidden");
                $form->setType('date_envoi_signature', 'hidden');
                $form->setType('date_retour_signature', 'hidden');
                $form->setType('date_envoi_controle_legalite', 'hidden');
                $form->setType('date_retour_controle_legalite', 'hidden');
                $form->setType('date_envoi_rar', 'hidden');
                $form->setType('date_retour_rar', 'hidden');
                $form->setType('om_fichier_finalise_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_finalise_courrier', 'hidden');
                $form->setType('om_fichier_signe_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_signe_courrier', 'hidden');
                $form->setType('signataire', 'hidden');
            }
            //
            if ($this->getVal('finalise') == 't') {
                $form->setType('modele_edition', 'selecthiddenstatic');
                $form->setType('courrier_joint', 'link');
                $form->setType('complement1_om_html', 'htmlstatic');
                $form->setType('complement2_om_html', 'htmlstatic');
                $form->setType('courrier_texte_type_complement1', 'hidden');
                $form->setType('courrier_texte_type_complement2', 'hidden');
                $form->setType('contacts_lies', 'hidden_static_select_multiple');
                $form->setType('autorites_police_liees', 'hidden_static_select_multiple');
                $form->setType('signataire', 'selecthiddenstatic');
            }
            //
            if ($this->get_courrier_type_code($this->getVal("courrier_type")) != "notifap") {
                $form->setType('autorites_police_liees', 'hidden');
            }
        }// fin modifier

        // En mode supprimer
        if ($maj == 2) {
            $form->setType('contacts_lies', 'hidden_static_select_multiple');
            $form->setType('autorites_police_liees', 'hidden_static_select_multiple');
            //
            if ($this->get_courrier_type_code($this->getVal("courrier_type")) != "notifap") {
                $form->setType('autorites_police_liees', 'hidden');
            }
            $form->setType('om_fichier_finalise_courrier', 'filestatic');
            $form->setType('om_fichier_signe_courrier', 'filestatic');
            $form->setType('courrier_texte_type_complement1', 'hidden');
            $form->setType('courrier_texte_type_complement2', 'hidden');
            //
            if ($this->getVal('mailing') == 't') {
                $form->setType("courrier_parent", "hidden");
                $form->setType('date_envoi_signature', 'hidden');
                $form->setType('date_retour_signature', 'hidden');
                $form->setType('date_envoi_controle_legalite', 'hidden');
                $form->setType('date_retour_controle_legalite', 'hidden');
                $form->setType('date_envoi_rar', 'hidden');
                $form->setType('date_retour_rar', 'hidden');
                $form->setType('om_fichier_finalise_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_finalise_courrier', 'hidden');
                $form->setType('om_fichier_signe_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_signe_courrier', 'hidden');
                $form->setType('signataire', 'hidden');
            }
        }//fin supprimer

        // En mode consulter
        if ($maj == 3) {
            $form->setType('contacts_lies', 'select_multiple_static');
            $form->setType('autorites_police_liees', 'select_multiple_static');
            //
            if ($this->get_courrier_type_code($this->getVal("courrier_type")) != "notifap") {
                $form->setType('autorites_police_liees', 'hidden');
            }
            $form->setType('om_fichier_finalise_courrier', 'file');
            $form->setType('om_fichier_signe_courrier', 'file');
            $form->setType('courrier_texte_type_complement1', 'hidden');
            $form->setType('courrier_texte_type_complement2', 'hidden');
            //
            if ($this->getVal('mailing') == 't') {
                $form->setType("courrier_parent", "hidden");
                $form->setType('date_envoi_signature', 'hidden');
                $form->setType('date_retour_signature', 'hidden');
                $form->setType('date_envoi_controle_legalite', 'hidden');
                $form->setType('date_retour_controle_legalite', 'hidden');
                $form->setType('date_envoi_rar', 'hidden');
                $form->setType('date_retour_rar', 'hidden');
                $form->setType('om_fichier_finalise_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_finalise_courrier', 'hidden');
                $form->setType('om_fichier_signe_courrier', 'hidden');
                $form->setType('date_envoi_mail_om_fichier_signe_courrier', 'hidden');
                $form->setType('signataire', 'hidden');
            }
        }//fin consulter

        // Supprime les insertions de texte-type sur le formulaire de validation
        if ($this->getParameter('validation') > 0) {
            $form->setType('courrier_texte_type_complement1', 'hidden');
            $form->setType('courrier_texte_type_complement2', 'hidden');
        }

        // Si le type de courrier n'est pas un Arrêté (DEC) alors on cache le champs
        // arrete_numero
        $form->setType('arrete_numero', 'hiddenstatic');
        if ($this->is_type_arrete() !== true) {
            $form->setType('arrete_numero', 'hidden');
        }

        //
        $form->setTaille('contacts_lies', 10);

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Filtre par établissement
        $etablissement = "";
        $champ = "etablissement";
        if (isset($_POST[$champ])) {
            $etablissement = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $etablissement = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $etablissement = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $etablissement = $this->getVal($champ);
        }
        if ($etablissement == "") {
            $etablissement = -1;
        }
        // Filtre par dossier de coordination
        $dossier_coordination = "";
        $champ = "dossier_coordination";
        if (isset($_POST[$champ])) {
            $dossier_coordination = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $dossier_coordination = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $dossier_coordination = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $dossier_coordination = $this->getVal($champ);
        }
        if ($dossier_coordination == "") {
            $dossier_coordination = -1;
        }

        //
        $sql_contacts_lies = str_replace('<idx_etab>', $etablissement, $sql_contacts_lies);
        $sql_contacts_lies = str_replace('<idx_dc>', $dossier_coordination, $sql_contacts_lies);
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "contacts_lies", $sql_contacts_lies, $sql_contacts_lies_by_id, false, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["contacts_lies"][0]);
            array_shift($form->select["contacts_lies"][1]);
        }

        //
        $sql_autorites_police_liees = str_replace('<idx_dc>', $dossier_coordination, $sql_autorites_police_liees);
        // Liaison NaN
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "autorites_police_liees", $sql_autorites_police_liees, $sql_autorites_police_liees_by_id, false, true);
        // Suppression du choix vide
        if ($this->getParameter("maj") == 0 || $this->getParameter("maj") == 1) {
            array_shift($form->select["autorites_police_liees"][0]);
            array_shift($form->select["autorites_police_liees"][1]);
        }

        // Iniatilisation de la condition de la requête du champs courrier joint
        // en autocomplete
        $courrier_joint_autocomplete_where = " mailing IS FALSE ";

        // Initialisation des données pour la composition des requêtes de filtre
        // du champ courrier_joint
        $etablissement_value = $this->getVal("etablissement");
        $dossier_coordination_value = $this->getVal("dossier_coordination");
        $dossier_instruction_value = $this->getVal("dossier_instruction");
        $courrier_id = $this->getVal($this->clePrimaire);
        // En mode "ajout"
        if ($maj == 0) {
            // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
            // le filterSelect sur le champ model_edition
            if (isset($form->val["etablissement"])) {
                $etablissement_value = $form->val["etablissement"];
            }
            // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
            // le filterSelect sur le champ model_edition
            if (isset($form->val["dossier_coordination"])) {
                $dossier_coordination_value = $form->val["dossier_coordination"];
            }
            // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
            // le filterSelect sur le champ model_edition
            if (isset($form->val["dossier_instruction"])) {
                $dossier_instruction_value = $form->val["dossier_instruction"];
            }
            $courrier_id = 0;
        }

        // Si la valeur de l'établissement n'est pas vide
        if (!empty($etablissement_value)) {

            // WHERE de la requête de récupération des courriers joints par
            // l'établissement
            $courrier_joint_autocomplete_where = $sql_courrier_joint_by_etablissement_where;
            $courrier_joint_autocomplete_where = str_replace('<idx_etab>', $etablissement_value, $courrier_joint_autocomplete_where);
            $courrier_joint_autocomplete_where = str_replace('<idx>', $courrier_id, $courrier_joint_autocomplete_where);
        }
        // Si la valeur du DC n'est pas vide
        if (!empty($dossier_coordination_value)) {

            // WHERE de la requête de récupération des courriers joints
            // par le DC
            $courrier_joint_autocomplete_where = $sql_courrier_joint_by_dossier_coordination_where;
           $courrier_joint_autocomplete_where = str_replace('<idx_dc>', $dossier_coordination_value, $courrier_joint_autocomplete_where);
           $courrier_joint_autocomplete_where = str_replace('<idx>', $courrier_id, $courrier_joint_autocomplete_where);
        }
        // Si la valeur du DI n'est pas vide
        if (!empty($dossier_instruction_value)) {

            // WHERE de la requête de récupération des courriers joints
            // par le DI
            $courrier_joint_autocomplete_where = $sql_courrier_joint_by_dossier_instruction_where;
            $courrier_joint_autocomplete_where = str_replace('<idx_di>', $dossier_instruction_value, $courrier_joint_autocomplete_where);
            $courrier_joint_autocomplete_where = str_replace('<idx>', $courrier_id, $courrier_joint_autocomplete_where);
        }

        // Champs de type link
        $params = array();
        $params['obj'] = "etablissement_tous";
        $params['idx'] = "";
        // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
        // le filterSelect sur le champ model_edition
        if (isset($form->val["etablissement"])) {
            $params['idx'] = $form->val['etablissement'];
        }
        // Instance de la classe etablissement
        require_once '../obj/etablissement.class.php';
        $etablissement = new etablissement($params['idx']);
        $params['libelle'] = $etablissement->getVal("libelle");
        $form->setSelect("etablissement", $params);

        // Champs de type link
        $params = array();
        $params['obj'] = "dossier_coordination";
        $params['idx'] = "";
        // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
        // le filterSelect sur le champ model_edition
        if (isset($form->val["dossier_coordination"])) {
            $params['idx'] = $form->val['dossier_coordination'];
        }
        // Instance de la classe dossier_coordination
        require_once '../obj/dossier_coordination.class.php';
        $dossier_coordination = new dossier_coordination($params['idx']);
        $params['libelle'] = $dossier_coordination->getVal("libelle");
        $form->setSelect("dossier_coordination", $params);

        // Champs de type link
        $params = array();
        $params['idx'] = "";
        // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
        // le filterSelect sur le champ model_edition
        if (isset($form->val["dossier_instruction"])) {
            $params['idx'] = $form->val['dossier_instruction'];
        }
        // Instance de la classe dossier_instruction
        require_once '../obj/dossier_instruction.class.php';
        $dossier_instruction = new dossier_instruction($params['idx']);
        // Réupère le type du dossier d'instruction
        $dossier_instruction_type = "";
        // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
        // le filterSelect sur le champ model_edition
        if (isset($form->val["dossier_coordination"])) {
            $dossier_coordination->get_dossier_type_code_by_dossier_coordination($form->val['dossier_coordination']);
        }
        // Objet visé
        $obj = "dossier_instruction_tous_visites";
        if ($dossier_instruction_type == "plan") {
            $obj = "dossier_instruction_tous_plans";
        }
        $params['obj'] = $obj;
        $params['libelle'] = $dossier_instruction->getVal("libelle");
        $form->setSelect("dossier_instruction", $params);

        // Champs de type link
        $params = array();
        $params['obj'] = "courrier";
        $params['idx'] = "";
        // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
        // le filterSelect sur le champ model_edition
        if (isset($form->val["courrier_parent"])) {
            $params['idx'] = $form->val['courrier_parent'];
        }
        // Instance de la classe courrier
        require_once '../obj/courrier.class.php';
        // Libellé du courrier parent
        $courrier = new courrier($params['idx']);
        $libelle = $courrier->getVal($courrier->clePrimaire);
        if (!empty($libelle)) {
            //
            $libelle = sprintf(_("Courrier mailing n°%s"), $libelle);
        }
        $params['libelle'] = $libelle;
        $form->setSelect("courrier_parent", $params);

        // En supression et en consultation
        // Ou en modification d'un courrier finalisé
        if ($maj == 2 || $maj == 3
            || $maj == 1 && $this->getVal('finalise') == 't') {
            // Champs de type link
            $params = array();
            $params['obj'] = "courrier";
            $params['idx'] = "";
            // Ajout d'une condition pour qu'il n'y est pas d'erreur dans
            // le filterSelect sur le champ model_edition
            if (isset($form->val["courrier_joint"])) {
                $params['idx'] = $form->val['courrier_joint'];;
            }
            // Instance de la classe courrier
            require_once '../obj/courrier.class.php';
            $courrier = new courrier($params['idx']);
            $params['libelle'] = $courrier->get_libelle_explicit($courrier->getVal($courrier->clePrimaire));
            $form->setSelect("courrier_joint", $params);
        }

        // En mode "ajout" et "modifier" d'un courrier finalisé
        if ($maj == 0 || ($maj == 1 && $this->getVal('finalise') == 'f')) {
            //
            $params = array();
            // Surcharge visée pour l'ajout
            $params['obj'] = "courrier";
            // Table de l'objet
            $params['table'] = "courrier";
            // Permission d'ajouter
            $params['droit_ajout'] = false;
            // Critères de recherche
            $params['criteres'] = array(
                "courrier.courrier" => _("Identifiant du courrier"),
                "courrier.code_barres" => _("Code barres du courrier"),
                "contact.nom" => _("nom du destinataire"),
                "contact.prenom" => _("prenom du destinataire"),
                "etablissement.libelle" => _("libelle de l'etablissement"),
                "dossier_coordination.libelle" => _("libelle du DC"),
                "dossier_instruction.libelle" => _("libelle du DI"),
                "modele_edition.libelle" => _("libelle du modèle d'edition")
            );
            // Tables liées
            $params['jointures'] = $list_joint_libelle;
            // Colonnes ID et libellé du champ
            // (si plusieurs pour le libellé alors une concaténation est faite)
            $params['identifiant'] = "courrier.courrier";
            $params['libelle'] = $list_fields_libelle;
            //
            if (!empty($courrier_joint_autocomplete_where)) {
                $params['where'] = $courrier_joint_autocomplete_where;
            }
            //
            $params['group_by'] = array(
                "courrier.courrier",
                "dossier_coordination.libelle",
                "dossier_instruction.libelle",
                "etablissement.libelle",
                "contact.nom",
                "contact.prenom",
                "modele_edition.code",
                "courrier_type.code",
                "contact_type.libelle",
                "contact_civilite.libelle",
            );
            // Envoi des paramètres
            $form->setSelect("courrier_joint", $params);

            // Texte-type pour les champs complément1 et complément2
            $contenu = array(_("Inserer un texte-type"));
            $form->setSelect("courrier_texte_type_complement1", $contenu);
            $form->setSelect("courrier_texte_type_complement2", $contenu);

            /**
             * SELECT du type de courrier 'courrier_type'
             * 
             * Le type de courrier a deux utilités dans ce contexte : 
             * - permettre de simplifier les listes de modèles d'édition
             *   présentés à l'utilisateur,
             * - permettre de modifier les affichages et la gestion des
             *   courriers créés si c'est un courrier 'AUTORITE DE POLICE
             *   (NOTIFAP)', un courrier 'ARRETE (DEC)', ou autre.
             */
            // Si c'est un sous-formulaire alors on popule le SELECT
            // En effet, le type de courrier est sélectionnable uniquement dans
            // les contextes des objets 'etablissement', 'dossier_coordination',
            // 'dossier_instruction'.
            // XXX Il peut être nécessaire d'être plus restrictif sur cette
            //     condition en spécifiant les contextes dans lesquels on
            //     doit populer le SELECT.
            $retourformulaire = $this->getParameter("retourformulaire");
            if (!empty($retourformulaire)) {
                // En fonction du contexte dans lequel on se trouve, on
                // instancie la classe en question pour récupérer le nom de la
                // table soit l'objet absolu ainsi que la liste des champs de
                // la table pour y vérifier la présence ou l'absence du champ
                // service.
                require_once '../obj/'.$retourformulaire.'.class.php';
                $object = new $retourformulaire($this->getParameter("idxformulaire"));
                // Première composante du filtre : le contexte
                // On remplace dans la portion de requête la chaîne <obj> par 
                // l'objet absolu. Cela permet de filtrer uniquement sur les 
                // types de courrier dont la catégorie correspond au contexte
                // actuel.
                $sql_courrier_type_where = str_replace('<obj>', $object->table, $sql_courrier_type_where);
                // Seconde composante du filtre : le service
                // On veut avoir dans notre liste les types de courriers :
                // A) TOUS si l'utilisateur n'a pas de service et que le
                //    contexte n'a pas de champ service
                // B) PAS TOUS
                //    - ceux qui ne sont pas rattachés à un service
                //    - ceux qui sont rattachés soit au service du contexte
                //      (si le contexte en a un) soit si le contexte n'en a pas
                //      au service de l'utilisateur
                // On initialise un marqueur de service pour vérifier ces conditions
                $courrier_type_service = null;
                // Si l'objet du contexte actuel possède le champ service
                // OU Si l'utilisateur a un service
                if (in_array("service", $object->champs) == true 
                    && $object->getVal('service') != "") {
                    // On écrase le marqueur de service
                    $courrier_type_service = $object->getVal('service');
                } elseif (!is_null($_SESSION["service"])) {
                    // On écrase le marqueur de service
                    $courrier_type_service = $_SESSION["service"];
                }
                // Si le marqueur de service a été modifié alors on ajoute
                // le filtre sur le service
                $sql_courrier_type_where_service = "";
                if ($courrier_type_service != null) {
                    $sql_courrier_type_where_service = sprintf(
                        " AND (courrier_type.service = %s OR courrier_type.service IS NULL) ",
                        $courrier_type_service
                    );
                }
                // Concaténation de la requête suite aux précédentes
                // compositions de filtres
                $sql_courrier_type = $sql_courrier_type_select.$sql_courrier_type_from.$sql_courrier_type_where.$sql_courrier_type_where_service.$sql_courrier_type_order;
                // Application de la requête sur le SELECT 'courrier_type'
                $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "courrier_type", $sql_courrier_type, $sql_courrier_type_by_id, true);
            }

            // Initialise le select en fonction de la valeur d'un autre champ
            $form->setSelect('modele_edition', $this->loadSelect_modele_edition($form, $maj, "courrier_type"));
        }

    }

    /**
     * Charge le select du champ "modele_edition" par rapport au
     * champ "courrier_type".
     * 
     * @param  object $form  Formulaire
     * @param  int    $maj   Mode d'insertion
     * @param  string $champ Champ activant le filtre
     * @return array         Contenu du select
     */
    function loadSelect_modele_edition(&$form, $maj, $champ) {
        //
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc");

        // Contenu du select
        $contenu = array();
        $contenu[0][0] = '';
        $contenu[1][0] = _('choisir')." "._("modele_edition");

        // Récupère l'id de l'établissement
        $courrier_type = "";
        if (isset($_POST[$champ])) {
            $courrier_type = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $courrier_type = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $courrier_type = $form->val[$champ];
        }

        // S'il y a un établissement
        if (!empty($courrier_type)) {
            //
            $sql_modele_edition_by_courrier_type = str_replace('<idx_courrier_type>', $courrier_type, $sql_modele_edition_by_courrier_type);
            $res = $this->f->db->query($sql_modele_edition_by_courrier_type);
            $this->addToLog(__METHOD__."() db->query(\"".$sql_modele_edition_by_courrier_type."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            //Les résultats de la requête sont stocké dans le tableau contenu
            $k=1;
            while ($row=& $res->fetchRow()){
                $contenu[0][$k]=$row[0];
                $contenu[1][$k]=$row[1];
                $k++;
            } 
        }
        //
        return $contenu;
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object  $form Formumaire
     * @param integer $maj  Mode d'insertion
     *
     * @return void
     */
    function setOnChange(&$form, $maj) {
        parent::setOnChange($form, $maj);
        // Filtre les contacts à liés par rapport à l'établissement
        $form->setOnchange("etablissement", "filterSelect(this.value, 'contacts_lies', 'etablissement', 'courrier');");
        // Change les textes-types utilisable en fonction du type du courrier
        $form->setOnchange("courrier_type", "change_filter_courrier_texte_type(this.value); filterSelect(this.value, 'modele_edition', 'courrier_type', 'courrier'); display_autorites_police_liees();");
    }

    /**
     * Construit et retourne le numéro d'arrêté 2016_012345_ERP.
     *
     * Retourne le prochain numéro d'arrêté. Attention la
     * séquence est incrémentée lors de l'appel à la méthode
     * 'get_next_arrete_number'.
     *
     * @return string
     */
    function compute_arrete_number() {
        $this->addToLog(__METHOD__ . "(): begin", EXTRA_VERBOSE_MODE);
        $year = date("Y");
        $arrete_number = sprintf(
            '%s_%s_ERP',
            $year,
            str_pad($this->get_next_arrete_number($year), 5, "0", STR_PAD_LEFT)
        );
        $this->addToLog(__METHOD__ . "(): ".$arrete_number, EXTRA_VERBOSE_MODE);
        $this->addToLog(__METHOD__ . "(): end", EXTRA_VERBOSE_MODE);
        return $arrete_number;
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);

        //
        if ($validation == 0 && $maj == 0) {

            //
            if (!empty($form->val['dossier_coordination'])) {
                //
                $form->setVal('etablissement', $this->f->get_field_from_table_by_id($form->val['dossier_coordination'], 'etablissement', 'dossier_coordination'));
            }

            //
            if (!empty($form->val['dossier_instruction'])) {
                //
                $form->setVal('dossier_coordination', $this->f->get_field_from_table_by_id($form->val['dossier_instruction'], 'dossier_coordination', 'dossier_instruction'));
                //
                $form->setVal('etablissement', $this->f->get_field_from_table_by_id($form->val['dossier_coordination'], 'etablissement', 'dossier_coordination'));
            }
        }

        //
        $this->setVal($form, $maj, $validation);
    }

    /**
     * Permet de définir les valeurs des champs.
     *
     * @param [type] $form       [description]
     * @param [type] $maj        [description]
     * @param [type] $validation [description]
     * @param [type] $dnu1       [description]
     * @param [type] $dnu2       [description]
     *
     * @return void
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);

        //
        if ($validation == 0) {
            //
            if ($maj == 0) {
                //
                $this->set_val_httpclick($form, $form->val['courrier_type']);
            }
            // En modification et si le document n'est pas finalisé
            if ($maj == 1 && $this->getVal('finalise') == 'f') {
                //
                $this->set_val_httpclick($form, $this->getVal('courrier_type'));
            }
        }
    }

    /**
     * Permet de définir les actions des champs httpclick.
     *
     * @param object  $form          Instance du formulaire
     * @param integer $courrier_type Type du courrier
     */
    function set_val_httpclick($form, $courrier_type) {
        //
        $form->setVal("courrier_texte_type_complement1", "view_overlay_courrier_texte_type('../scr/form.php?obj=courrier&action=6&idx=0', 'complement1_om_html', '".$courrier_type."')");
        //
        $form->setVal("courrier_texte_type_complement2", "view_overlay_courrier_texte_type('../scr/form.php?obj=courrier&action=6&idx=0', 'complement2_om_html', '".$courrier_type."')");
    }

    /**
     * Méthode de mise en page.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {

        // Container global nécessaire pour le positionnement correct du premier 
        // fieldset par rapport au portlet d'actions
        $form->setBloc('courrier', 'D', "", "form-document_genere-action-".$maj);

        // Fieldset "Informations générales"
        $form->setBloc('courrier', 'D', "", "col_12");
            $form->setBloc('courrier', 'D', "", "col_12");
                $form->setFieldset('courrier', 'D', _('Informations generales'), "");
                $form->setFieldset('courrier_texte_type_complement2', 'F', '');
            $form->setBloc('courrier_texte_type_complement2', 'F');
        $form->setBloc('courrier_texte_type_complement2', 'F');

        // Fieldset "Document"
        $form->setBloc('om_fichier_finalise_courrier', 'D', "", "col_12");
            $form->setBloc('om_fichier_finalise_courrier', 'D', "", "col_12");
                $form->setFieldset('om_fichier_finalise_courrier', 'D', _('Document'), "");
                $form->setFieldset('signataire', 'F', '');
            $form->setBloc('signataire', 'F');
        $form->setBloc('signataire', 'F');

        // Fieldset "Suivi"
        $form->setBloc('om_date_creation', 'D', "", "col_9");
            $form->setBloc('om_date_creation', 'D', "", "col_12");
                $form->setFieldset('om_date_creation', 'D', _('Suivi'), "");
                $form->setFieldset('date_retour_rar', 'F', '');
            $form->setBloc('date_retour_rar', 'F');
        $form->setBloc('date_retour_rar', 'F');

        // Fieldset "Statut"
        $form->setBloc('finalise', 'D', "", "col_3");
            $form->setBloc('finalise', 'D', "", "col_12");
                $form->setFieldset('finalise', 'D', _('Statut'), "");
                $form->setFieldset('mailing', 'F', '');
            $form->setBloc('mailing', 'F');
        $form->setBloc('mailing', 'F');

        // Fieldset "Lien"
        $form->setBloc('etablissement', 'D', "", "col_12");
            $form->setBloc('etablissement', 'D', "", "col_12");
                $form->setFieldset('etablissement', 'D', _('Lien'), "");
                $form->setFieldset('autorites_police_liees', 'F', '');
            $form->setBloc('autorites_police_liees', 'F');
        $form->setBloc('autorites_police_liees', 'F');

        // Fermeture du container global
        $form->setBloc('autorites_police_liees', 'F');
    }

    /**
     * CONDITION - is_type_arrete.
     *
     * Un document généré est considéré de type ARRÊTÉ lorsque son type de courrier
     * est une décision (code 'DEC').
     *
     * @return boolean
     */
    function is_type_arrete() {
        if ($this->get_courrier_type_code($this->getVal("courrier_type")) == "dec") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_type_proces_verbal.
     *
     * Un document généré est considéré de type PROCÈS VERBAL lorsqu'il est lié
     * à un enregistrement de la table 'proces_verbal'.
     *
     * @return boolean
     */
    function is_type_proces_verbal() {
        if ($this->getVal("proces_verbal") !== "") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_type_notification_ap.
     *
     * Un document généré est considéré de type NOTIFICATION D'AUTORITÉ DE
     * POLICE lorsque son type de courrier est une de ce type (code 'NOTIFAP').
     *
     * @return boolean
     */
    function is_type_notification_ap() {
        if ($this->get_courrier_type_code($this->getVal("courrier_type")) == "notifap") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_deletable.
     *
     * Condition pour afficher le bouton supprimer
     *
     * @return boolean
     */
    function is_deletable() {
        // Récupère le PV
        $pv = $this->getVal('proces_verbal');

        // Si le courrier est lié à un pv
        if ($pv != null || $pv != "") {

            //
            return false;
        }

        // Récupère la valeur du champ finalise
        $finalise = $this->getVal("finalise");
        // Récupère la valeur du champ mailing
        $mailing = $this->getVal("mailing");
        // Récupère la valeur du champ courrier_parent
        $courrier_parent = $this->getVal('courrier_parent');
        
        //
        if ($finalise == 'f'
            && ($mailing == 't' || empty($courrier_parent))) {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_finalizable.
     *
     * Condition pour afficher le bouton de finalisation.
     *
     * @return boolean
     */
    function is_finalizable() {
        // Récupère la valeur du champ finalise
        $finalise = $this->getVal('finalise');
        // Récupère la valeur du champ modele_edition
        $modele_edition = $this->getVal('modele_edition');
        // Récupère la valeur du champ courrier_joint
        $courrier_joint = $this->getVal('courrier_joint');


        // Si finalise est à true
        if ($finalise != 'f') {

            // Stop le traitement
            return false;
        }

        // Si courrier_joint n'est pas vide
        if (!empty($courrier_joint)) {

            // Instancie le courrier joint
            $courrier_joint_inst = new courrier($courrier_joint);
            // Récupère la valeur du champ finalise
            $courrier_joint_finalise = $courrier_joint_inst->getVal('finalise');

            // Si le courrier joint n'est pas finalisé
            if ($courrier_joint_finalise != 't') {

                // Stop le traitement
                return false;
            }
        }

        // Instancie la classe modele_edition
        require_once "../obj/modele_edition.class.php";
        $modele_edition_inst = new modele_edition($modele_edition);

        // Récupère la lettre type et l'état
        $om_lettretype = $modele_edition_inst->getVal('om_lettretype');
        $om_etat = $modele_edition_inst->getVal('om_etat');

        // Si le modèle d'édition selectionné ne possède ni une lettre type,
        // ni un état
        if (empty($om_lettretype) && empty($om_etat)) {

            // Stop le traitement
            return false;
        }

        //
        return true;
    }

    /**
     * CONDITION - is_unfinalizable.
     *
     * Condition pour afficher le bouton de définalisation.
     *
     * @return boolean
     */
    function is_unfinalizable() {
        // Récupère le PV
        $pv = $this->getVal('proces_verbal');

        // Si le courrier est lié à un pv
        if ($pv != null || $pv != "") {

            //
            return false;
        }

        // Récupère la valeur du champ finalise
        $finalise = $this->getVal('finalise');
        // Récupère la valeur du champ mailing
        $mailing = $this->getVal("mailing");
        // Récupère la valeur du champ courrier_parent
        $courrier_parent = $this->getVal('courrier_parent');

        // Si finalise est à false
        if ($finalise == 't'
            && ($mailing == 't' || empty($courrier_parent))) {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_previewable.
     *
     * Condition pour afficher le bouton de prévisualisation.
     *
     * @return boolean
     */
    function is_previewable() {
        // Récupère la valeur du champ finalise
        $finalise = $this->getVal('finalise');
        // Récupère la valeur du champ modele_edition
        $modele_edition = $this->getVal('modele_edition');

        // Si finalise est à false
        if ($finalise == 'f' && !empty($modele_edition)) {

            // Instancie la classe modele_edition
            require_once "../obj/modele_edition.class.php";
            $modele_edition_inst = new modele_edition($modele_edition);

            // Récupère la lettre type et l'état
            $om_lettretype = $modele_edition_inst->getVal('om_lettretype');
            $om_etat = $modele_edition_inst->getVal('om_etat');

            // Si le modèle d'édition selectionné ne possède pas une lettre type
            // ou un état
            if (empty($om_lettretype) && empty($om_etat)) {

                // Stop le traitement
                return false;
            }

            //
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_dc_connected_to_referentiel_ads.
     *
     * @return boolean
     */
    function is_dc_connected_to_referentiel_ads() {
        //
        if ($this->get_inst_dossier_coordination()->is_connected_to_referentiel_ads() !== true) {
            return false;
        }
        //
        return true;
    }

    /**
     * Effectue le traitement de finalisation et définalisation.
     *
     * @param string $mode finalize/unfinalize
     * @param array  $val  valeurs du formulaire
     *
     * @return boolean true/false
     */
    function manage_finalizing($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "finalize" && $mode != "unfinalize" &&
            isset($this->finalized_field) && !empty($this->finalized_field)) {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "finalize") {
            // Valeurs à modifier
            $valF = array(
                $this->finalized_field => true,
                "date_finalisation" => date("d/m/Y"),
            );
            //
            $valid_message = _("Finalisation correctement effectuee.");
        } elseif ($mode == "unfinalize") {
            // Valeurs à modifier
            $valF = array(
                $this->finalized_field => false,
                "date_finalisation" => null,
            );
            //
            $valid_message = _("Definalisation correctement effectuee.");
        }
        // Trigger avant
        if ($this->trigger_finalize($id, $mode) === false) {
            $this->addToLog(
                __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la finalisation",
                DEBUG_MODE
            );
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage(_("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(
                    _("Attention vous n'avez fait aucune modification.")."<br/>"
                );
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
            if ($this->trigger_finalize_after($id, $mode) === false) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la finalisation",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Méthode appelée après enregistrement en base de la finalisation.
     * 
     * @param string $id    Identifiant de l'enregistrement
     * @param string $mode  finalize/unfinalize
     * @param null   &$dnu1 @deprecated Ancienne ressource de base de données.
     * @param array  $val   Valeurs du formulaire
     * @param null   $dnu2  @deprecated Ancien marqueur de débogage.
     */
    function trigger_finalize_after($id, $mode, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Si le mode n'existe pas on retourne false
        if ($mode != "finalize" && $mode != "unfinalize" &&
            isset($this->finalized_field) && !empty($this->finalized_field)) {
            return false;
        }

        //
        // Trois cas :
        // - 1 - finalisation d'un courrier unique (mailing : false)
        // - 2 - finalisation d'un courrier mailing (mailing : true)
        // - 3 - définalisation d'un courrier mailing (mailing : true)
        //
        if ($this->f->get_val_boolean($this->getVal("mailing")) == false
            && $mode == "finalize") {
            //
            // Cas n°1 : Lors de la finalisation d'un courrier unique (pas un
            // mailing), si il est de type arrêté alors on lui génère un numéro
            // puis on effectue l'action de finalisation.
            //
            // SI le document généré est de type ARRÊTÉ et que le fichier n'a
            // jamais été généré.
            if ($this->is_type_arrete() === true
                && ($this->getVal("om_fichier_finalise_courrier") === ""
                    || $this->getVal("om_fichier_finalise_courrier") === null)
            ) {
                $arrete_number = $this->compute_arrete_number();
                $ret = $this->update_autoexecute(
                    array(
                        "arrete_numero" => $arrete_number,
                    ),
                    null,
                    false
                );
                if ($ret !== true) {
                    return false;
                }
                $this->set_arrete_number($arrete_number);
            }
            // On appelle la méthode de finalisation d'un courrier unique
            $ret = $this->generate_courrier_pdf(array(
                "courrier" => $id,
                "modele_edition" => $this->getVal("modele_edition"),
                "om_fichier_finalise_courrier" => $this->getVal("om_fichier_finalise_courrier"),
            ));
            if ($ret !== true) {
                return false;
            }
        } elseif ($this->f->get_val_boolean($this->getVal("mailing")) == true
            && $mode == "finalize") {
            //
            // Cas n°2 : Lors de la finilisation d'un courrier mailing,
            // on crée un courrier enfant par destinataire du mailing.
            //
            // Récupère les données du courrier mailing
            foreach ($this->champs as $champ) {
                $data[$champ] = $this->getVal($champ);
            }
            // Modifie les valeurs de certains champs
            $data["finalise"] = "t";
            $data["mailing"] = "f";
            $data["contacts_lies"] = "";
            $data['om_date_creation'] = $this->f->formatDate($data['om_date_creation'], true);
            $data['date_finalisation'] = date("d/m/Y");

            // Récupère la liste des contacts
            $list_contact = $this->get_list_contact($id);
            // Compte le nombre de destinataire
            $nb_contact = count($list_contact);

            // S'il y a plusieurs destinataire
            if ($nb_contact > 1) {

                // Modifie les valeurs de certains champs
                $data["courrier_parent"] = $data["courrier"];
                $data["courrier"] = "";

                // Pour chaque destinataire
                foreach ($list_contact as $contact) {
                    // 
                    $data["contacts_lies"] = array($contact);
                    //
                    $add = $this->add_courrier_child($data);

                    //
                    if ($add == false) {
                        //
                        return false;
                    }
                }

                // Récupère la liste de courriers enfants d'un courrier
                $list_courrier_child = $this->get_list_courrier_child($id);
                // Génère le PDF de chaque courrier
                $tmpfile_uid = $this->generate_all_courriers_pdf($list_courrier_child, "temporary");
                if ($tmpfile_uid == false) {
                    $this->addToLog(
                        __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la génération du fichier pdf temporaire concaténé du mailing",
                        DEBUG_MODE
                    );
                    $this->correct = false;
                    $this->cleanMessage();
                    $this->addToMessage(_("Une erreur s'est produite lors de la finalisation. Contactez votre administrateur."));
                    return false;
                }

                // Affiche le message de validation avec le lien des PDF 
                // concaténés
                $this->addToMessage(_("Telechargez la liste des documents generes").
                    " <a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../spg/file.php?mode=temporary&amp;uid=".$tmpfile_uid."\" >".
                    _("ici")."</a><br/>");
            }

            // S'il y a qu'un seul destinataire ou aucun
            if ($nb_contact <= 1) {
                // => XXX
                // Modifie les valeurs de certains champs
                $data["contacts_lies"] = $list_contact;

                // Modifie le courrier
                $update = $this->modifier($data);

                // Si la modification a échouée
                if ($update === false) {
                    //
                    return false;
                }

                // Génère le pdf du courrier
                $generate_courrier_pdf = $this->generate_courrier_pdf($valF);

                // Si la génération à échouée
                if ($generate_courrier_pdf == false) {
                    //
                    return false;
                }
            }
        } elseif ($this->f->get_val_boolean($this->getVal("mailing")) == true
            && $mode == "unfinalize") {
            //
            // Cas n°3 : Lors de la définilisation d'un courrier mailing,
            // on supprime chaque courrier enfant qui a été créé lors de
            // finalisation.
            //
            // Récupère la liste de courriers enfants d'un courrier
            $list_courrier_child = $this->get_list_courrier_child($id);
            // Compte le nombre de courrier enfant
            $nb_courrier_child = count($list_courrier_child);

            // S'il y a plusieurs destinataire
            if ($nb_courrier_child > 1) {

                // Pour chaque courrier
                foreach ($list_courrier_child as $courrier_child) {
                    // Supprime le courrier
                    $delete = $this->delete_courrier_child($courrier_child);

                    //
                    if ($delete == false) {
                        //
                        return false;
                    }
                }
            } else {
                // => XXX
                $data = array(
                    "om_fichier_finalise_courrier" => '',
                    "om_fichier_signe_courrier" => '',
                    "mailing" => 't',
                );

                // Met à jour le courrier
                $update = $this->update_autoexecute($data, $id);

                // Si la modification a échouée
                if ($update === false) {
                    //
                    return false;
                }
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Récupère la liste des contacts du courrier.
     *
     * @param integer $id Identifiant du courrier
     *
     * @return array Liste des contacts
     */
    function get_list_contact($id) {

        // Initialisation de la variable de résultat
        $list_contact = array();

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT contact
                    FROM ".DB_PREFIXE."lien_courrier_contact
                    WHERE courrier = ".intval($id);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_contact[] = $row['contact'];
            }
        }

        // Retourne le résultat
        return $list_contact;
    }

    /**
     * Récupère la liste des courriers enfants ou renvoi l'id si le courrier est
     * finalisé mais sans enfant.
     *
     * @param integer $id Identifiant du courrier parent
     *
     * @return array Liste des courriers enfants
     */
    function get_list_courrier_child($id) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Logger
        $this->addToLog(__METHOD__."(): \$id = ".$id, EXTRA_VERBOSE_MODE);
        // Initialisation de la variable de résultat
        $list_courrier = array();

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT courrier
                    FROM ".DB_PREFIXE."courrier
                    WHERE courrier_parent = ".intval($id);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_courrier[] = $row['courrier'];
            }

            // Si il n'y a pas de résultat
            if (empty($list_courrier)) {
                // Instancie le courrier
                $courrier = new courrier($id);

                // Si le courrier est finalisé
                if ($courrier->getVal('finalise') == 't') {
                    //
                    $list_courrier[] = $id;
                }
            }
        }

        // Retourne le résultat
        return $list_courrier;
    }

    /**
     * Récupère la liste des AP liées.
     *
     * @param integer $id Identifiant du courrier
     *
     * @return array
     */
    function get_list_autorite_police($id) {

        // Initialisation de la variable de résultat
        $list_autorite_police = array();

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT autorite_police
                    FROM ".DB_PREFIXE."lien_autorite_police_courrier
                    WHERE courrier = ".intval($id);
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_autorite_police[] = $row['autorite_police'];
            }
        }

        // Retourne le résultat
        return $list_autorite_police;
    }

    /**
     * Récupère les informations des courriers enfants.
     *
     * @param integer $id identifiant du courrier parent.
     *
     * @return array tableau courrier=>(om_fichier_finalise_courrier, contact)
     */
    function get_info_courrier_child($id) {

        // Initialisation de la variable de résultat
        $list_courrier = array();

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT courrier.courrier, om_fichier_finalise_courrier,
                    concat(contact.nom, ' ', contact.prenom) as contact
                    FROM ".DB_PREFIXE."courrier
                    LEFT JOIN ".DB_PREFIXE."lien_courrier_contact
                        ON lien_courrier_contact.courrier = courrier.courrier
                        AND courrier.mailing IS FALSE
                    LEFT JOIN ".DB_PREFIXE."contact
                        ON lien_courrier_contact.contact = contact.contact
                    WHERE courrier_parent = ".intval($id)." AND om_fichier_finalise_courrier IS NOT NULL";
            $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", VERBOSE_MODE);
            $res = $this->f->db->query($sql);
            $this->f->isDatabaseError($res);

            // Récupère les résultats dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                //
                $list_courrier[$row['courrier']] = array(
                    "om_fichier_finalise_courrier" => $row['om_fichier_finalise_courrier'],
                    "contact" => $row['contact'],
                    );
            }
        }

        // Retourne le résultat
        return $list_courrier;
    }


    /**
     * Permet d'ajouter un courrier.
     *
     * @param array $val Valeur à l'ajout
     *
     * @return boolean
     */
    function add_courrier_child($val) {

        // Instance de la classe courrier
        require_once '../obj/courrier.class.php';
        $courrier = new courrier("]");

        // Ajoute l'enregistrement
        $add = $courrier->ajouter($val);

        //
        return $add;
    }

    /**
     * Permet de supprimer un courrier.
     *
     * @param integer $id Identifiant du courrier à supprimer.
     *
     * @return boolean
     */
    function delete_courrier_child($id) {

        // Instance de la classe courrier
        require_once '../obj/courrier.class.php';
        $courrier = new courrier($id);

        //
        $val = array();
        foreach($courrier->champs as $id => $champ) {
            $val[$champ] = $courrier->val[$id];
        }
        // Supprime l'enregistrement
        $delete = $courrier->supprimer($val);
        //
        return $delete;
    }

    /**
     * 
     * 
     * Cette méthode génère le fichier pdf du document généré, calcule les
     * métadonnées, stocke le fichier et met à jour le champ fichier avec l'uid
     * du fichier stocké.
     *
     * @param array $val
     *
     * @return boolean
     */
    function generate_courrier_pdf($val) {
        /**
         * Génération de l'édition pdf
         */
        $generate_edition = $this->generate_edition(false, $val['courrier']);

        /**
         * Composition des métadonnées
         */
        // Métadonnées obligatoires du document
        $metadata = array(
            'filename' => $generate_edition['filename'],
            'mimetype' => 'application/pdf',
            'size' => strlen($generate_edition['pdf_output'])
        );
        // METADATA FILESTORAGE
        // En fonction du type de document généré, on calcule l'ensemble de
        // métadonnées associé : 
        // - Procès Verbal : om_fichier_finalise_proces_verbal
        // - Arrêté : om_fichier_finalise_arrete
        // - Document généré standard : om_fichier_finalise_courrier
        if ($this->is_type_proces_verbal() === true) {
            $spe_metadata = $this->getMetadata("om_fichier_finalise_proces_verbal");
        } elseif ($this->is_type_arrete() === true) {
            $spe_metadata = $this->getMetadata("om_fichier_finalise_arrete");
        } else {
            $spe_metadata = $this->getMetadata("om_fichier_finalise_courrier");
        }
        $metadata = array_merge($metadata, $spe_metadata);

        /**
         * Stockage du document
         */
        // Si le document a déjà été finalisé,
        // met à jour le document mais pas son uid
        if (!empty($val["om_fichier_finalise_courrier"])) {
            $uid = $this->f->storage->update(
                $val["om_fichier_finalise_courrier"],
                $generate_edition['pdf_output'],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la mise à jour du document sur le système de stockage des fichiers [courrier.om_fichier_finalise_courrier]",
                    DEBUG_MODE
                );
                return $this->end_treatment(__METHOD__, false);
            }
        } else { // Sinon, ajoute le document et récupère son uid
            $uid = $this->f->storage->create(
                $generate_edition['pdf_output'],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la création du document sur le système de stockage des fichiers [courrier.om_fichier_finalise_courrier]",
                    DEBUG_MODE
                );
                return $this->end_treatment(__METHOD__, false);
            }
        }

        /**
         * Mise à jour de l'enregistrement en base de données
         */
        $ret = $this->update_autoexecute(
            array(
                "om_fichier_finalise_courrier" => $uid,
            ),
            $val['courrier']
        );
        if ($ret !== true) {
            $this->addToLog(
                __METHOD__ . "(): Erreur lors de la mise à jour de l'enregistrement en base de données",
                DEBUG_MODE
            );
            return $this->end_treatment(__METHOD__, false);
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Génération de tous les documents pdf des courriers passés en paramètre en un seul document.
     *
     * @param array $list_courrier_child
     * @param mixed $storage_mode
     *
     * @return mixed 
     */
    function generate_all_courriers_pdf($list_courrier_child, $storage_mode = null) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        $this->addToLog(__METHOD__."(): \$list_courrier_child = ".print_r($list_courrier_child, true), EXTRA_VERBOSE_MODE);

        //
        $liste_modele_edition = array();
        //
        $liste_idx = array();
        //
        foreach ($list_courrier_child as $courrier_child_id) {
            // Instance de la classe courrier
            require_once '../obj/courrier.class.php';
            $courrier_child = new courrier($courrier_child_id);
            $liste_modele_edition[] = $courrier_child->getVal('modele_edition');
            $liste_idx[] = $courrier_child->getVal($courrier_child->clePrimaire);

            $courrier_joint_id = $courrier_child->getVal('courrier_joint');
            // Si le courrier enfant a un courrier joint
            if (!empty($courrier_joint_id)) {

                // Instancie le courrier joint
                $courrier_child_courrier_joint = new courrier($courrier_joint_id);
                $liste_modele_edition[] = $courrier_child_courrier_joint->getVal('modele_edition');
                $liste_idx[] = $courrier_child_courrier_joint->getVal($courrier_child_courrier_joint->clePrimaire);
            }
        }

        //
        $liste_modele_edition = implode(";", $liste_modele_edition);
        $liste_idx = implode(";", $liste_idx);
        //
        $pdfedition = $this->compute_pdf_output("modele_edition", $liste_modele_edition, null, $liste_idx);

        // Nom du fichier
        $filename = _("liste_documents_generes")."_".$courrier_child->getVal('courrier_parent').'.pdf';

        //Métadonnées du document
        $metadata = array(
            'filename' => $filename,
            'mimetype' => 'application/pdf',
            'size' => strlen($pdfedition["pdf_output"])
        );

        //Stockage du PDF en stockage temporaire
        if ($storage_mode === "temporary") {
            $uid = $this->f->storage->create_temporary(
                $pdfedition["pdf_output"],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la création du document sur le système de stockage des fichiers temporaires",
                    DEBUG_MODE
                );
                return $this->end_treatment(__METHOD__, false);
            }
        } else {
            $uid = $this->f->storage->create(
                $pdfedition["pdf_output"],
                $metadata
            );
            if ($uid == OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la création du document sur le système de stockage des fichiers",
                    DEBUG_MODE
                );
                return $this->end_treatment(__METHOD__, false);
            }
        }

        //
        return $uid;
    }

    /**
     * [get_om_lettretype_id_by_modele_edition description]
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    function get_om_lettretype_id_by_modele_edition($id) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        $om_lettretype_id = "";
        //
        if (!empty($id)) {
            // Instance de la classe modele_edition
            require_once '../obj/modele_edition.class.php';
            $modele_edition = new modele_edition($id);
            //
            $om_lettretype_id = $modele_edition->get_om_lettretype_id($modele_edition->getVal('om_lettretype'));
        }
        // Logger
        $this->addToLog(__METHOD__."() - return \$om_lettretype_id = ".$om_lettretype_id, EXTRA_VERBOSE_MODE);
        //
        return $om_lettretype_id;
    }

    /**
     * TRIGGER - triggerajouter.
     *
     * - setValF.
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * setValF.
         */
        //
        $this->valF['om_date_creation'] = date("d/m/Y");
        //
        if (!empty($this->valF['dossier_coordination'])) {
            //
            if (empty($this->valF['etablissement'])) {
                //
                $this->valF['etablissement'] = $this->f->get_field_from_table_by_id($this->valF['dossier_coordination'], 'etablissement', 'dossier_coordination');
                // Au lieu de modifier la valeur de retour de get_field_from_table_by_id()
                // on vérifie ici que celle-ci n'est pas vide. On évite ainsi
                // une erreur de base de données car la colonne est de type integer.
                if ($this->valF['etablissement'] == '') {
                    $this->valF['etablissement'] = null;
                }
            }
        }
        //
        if (!empty($this->valF['dossier_instruction'])) {
            //
            if (empty($this->valF['dossier_coordination'])) {
                //
                $this->valF['dossier_coordination'] = $this->f->get_field_from_table_by_id($this->valF['dossier_instruction'], 'dossier_coordination', 'dossier_instruction');
                // Au lieu de modifier la valeur de retour de get_field_from_table_by_id()
                // on vérifie ici que celle-ci n'est pas vide. On évite ainsi
                // une erreur de base de données car la colonne est de type integer.
                if ($this->valF['dossier_coordination'] == '') {
                    $this->valF['dossier_coordination'] = null;
                }
            }

            //
            if (empty($this->valF['etablissement'])) {
                //
                $this->valF['etablissement'] = $this->f->get_field_from_table_by_id($this->valF['dossier_coordination'], 'etablissement', 'dossier_coordination');
                // Au lieu de modifier la valeur de retour de get_field_from_table_by_id()
                // on vérifie ici que celle-ci n'est pas vide. On évite ainsi
                // une erreur de base de données car la colonne est de type integer.
                if ($this->valF['etablissement'] == '') {
                    $this->valF['etablissement'] = null;
                }
            }
        }
        // o vérifie ici que la valeur du PV n'est pas vide. On évite ainsi
        // une erreur de base de données car la colonne est de type integer.
        if ($this->valF['proces_verbal'] == '') {
            $this->valF['proces_verbal'] = null;
        }
        // Définit la valeur du champ mailing en fonction du nombre de contact
        // lié
        $this->valF['mailing'] = $this->is_mailing($val['contacts_lies']);
        // Génération du code barres
        $this->valF["code_barres"] = $this->f->generate_code_barres("10", $this->valF[$this->clePrimaire]);

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggerajouterapres.
     *
     * - Gestion des liaisons NàN.
     * - Génération du document PDF.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * Gestion des liaisons NàN
         */
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $val[$liaison_nan["field"]]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                if ($nb_liens == 1 ){
                    $this->addToMessage(sprintf(_("Creation d'une nouvelle liaison realisee avec succes.")));
                } else {
                    $this->addToMessage(sprintf(_("Creation de %s nouvelles liaisons realisee avec succes."), $nb_liens));
                }
            }
        }

        /**
         * Génération du document PDF.
         */
        // Récupère l'identifiant de l'enregistrement
        $val['courrier'] = $id;
        // Si le courrier passé n'est pas un mailing mais qu'il est finalisé
        if ($val['mailing'] == 'f' && $val['finalise'] == 't') {
            $ret = $this->generate_courrier_pdf($val);
            if ($ret === false) {
                $this->addToLog(
                    __METHOD__ . "(): Erreur lors de la finalisation du fichier pdf",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifier.
     *
     * - setValF.
     *
     * @return boolean
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * setValF.
         */
        // Définit la valeur du champ mailing en fonction du nombre de contact
        // lié
        $this->valF['mailing'] = $this->is_mailing($val['contacts_lies']);
        // L'ajout du document généré signé numérisé se fait via le formulaire
        // de modification du document généré. On se positionne ici dans le
        // triggermodifier (avant le traitement) pour modifier le jeu de
        // métadonnées qui doit être utilisé lors du stockage du fichier en
        // question en fonction de certains critères :
        // - soit c'est un procès verbal (le champ concerné n'est pas vide)
        // - soit c'est un arrêté
        // - sinon on utilise les métadonnées standards
        //
        if ($this->getVal("proces_verbal") !== ""
            && $this->getVal("proces_verbal") !== null) {
            $this->metadata["om_fichier_signe_courrier.bak"] = $this->metadata["om_fichier_signe_courrier"];
            $this->metadata["om_fichier_signe_courrier"] = $this->metadata["om_fichier_signe_proces_verbal"];
        } elseif ($this->getVal("arrete_numero") !== "") {
            $this->metadata["om_fichier_signe_courrier.bak"] = $this->metadata["om_fichier_signe_courrier"];
            $this->metadata["om_fichier_signe_courrier"] = $this->metadata["om_fichier_signe_arrete"];
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - Gestion des liaisons NàN.
     * - Mise à jour des dates de notifications sur les autorités de police liées.
     * - Interface avec le référentiel ADS [201].
     * - Interface avec le référentiel ADS [202].
     * - Interface avec le référentiel ADS [208].
     * - Mise à jour des métadonnées des fichiers stockés.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * Gestion des liaisons NàN
         */
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"],
                $val[$liaison_nan["field"]]
            );
            // Message de confirmation
            if ($nb_liens > 0) {
                $this->addToMessage(_("Mise a jour des liaisons realisee avec succes."));
            }
        }

        /**
         * Mise à jour des dates de notifications sur les autorités de police liées.
         */
        // Si la date de retour AR à été modifiée et qu'elle n'est pas vide
        if ($this->getVal("date_retour_rar") != $this->valF['date_retour_rar']
            && !empty($this->valF['date_retour_rar'])) {
            // Valeurs de mise à jour
            $values = array(
                "date_retour_rar" => $this->valF['date_retour_rar'],
            );
            // Met à jour la date de notification des autorités de police liées
            $ret = $this->update_autorites_police_liees($id, $values);
            if ($ret === false) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour des AP liées",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[201] Mise à jour du numéro de l’établissement dans le référentiel ADS
         * Déclencheur :
         * - L’option ADS est activée
         * - Le DC lié est connecté au référentiel ADS
         * - Un document généré signé de type « Arrêté » est ajouté
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->get_inst_dossier_coordination()->is_connected_to_referentiel_ads() === true
            && $this->is_type_arrete() === true
            && $this->getVal('om_fichier_signe_courrier') === ""
            && $this->valF["om_fichier_signe_courrier"] !== null
            && $this->valF["om_fichier_signe_courrier"] !== "") {
            //
            $infos = array(
                "dossier_coordination" => $this->get_inst_dossier_coordination()->getVal("dossier_coordination"),
                "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                "dossier_autorisation" => $this->get_inst_dossier_coordination()->getVal("dossier_autorisation_ads"),
                "numero_erp" => $this->get_inst_etablissement()->getVal('etablissement'),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(201, $infos);
            if ($ret !== true) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la notification (201) du référentiel ADS",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (201) du référentiel ADS OK."));
        }

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[202] Mise à jour du statut ouvert de l’établissement dans le référentiel ADS
         * Déclencheur :
         * - L’option ADS est activée
         * - Le DC lié est connecté au référentiel ADS
         * - Type d'Arrêté Ouverture
         */
        // On récupère l'identifiant correspond à l'état OUVERT
        // (le paramètre etablissement_etat_periodique)
        require_once "../obj/etablissement_etat.class.php";
        $inst_util__etat = new etablissement_etat(0, $this->f->db, 0);
        $etat_ouvert_id = $inst_util__etat->get_etablissement_etat_by_code(
            $this->f->getParameter("etablissement_etat_periodique")
        );
        //
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->get_inst_dossier_coordination()->is_connected_to_referentiel_ads() === true
            && $this->is_type_arrete() === true
            && $this->get_inst_ap_type_arrete_info("etablissement_etat") == $etat_ouvert_id
            && $this->get_inst_ap_type_arrete_info("type_arrete") == 't'
            && $this->valF["date_retour_rar"] !== ""
            && $this->valF["date_retour_rar"] !== null
            && $this->valF["date_retour_rar"] !== $this->getVal("date_retour_rar")) {
            //
            $infos = array(
                "dossier_coordination" => $this->get_inst_dossier_coordination()->getVal("dossier_coordination"),
                "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                "dossier_autorisation" => $this->get_inst_dossier_coordination()->getVal("dossier_autorisation_ads"),
                "erp_ouvert" => "Oui",
                "date_arrete" => $this->f->formatDate($this->valF["date_retour_rar"], true),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(202, $infos);
            if ($ret !== true) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la notification (202) du référentiel ADS",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (202) du référentiel ADS OK."));
        }

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[208] Dossier AT Mise à jour des informations arrêtées dans le référentiel ADS
         * Déclencheur :
         * - L’option ADS est activée
         * - Le DC lié est connecté au référentiel ADS
         * - Le DC est de type AT
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->get_inst_dossier_coordination()->is_connected_to_referentiel_ads() === true
            && substr($this->get_inst_dossier_coordination()->getVal("libelle"), 0, 2) === 'AT'
            && $this->is_type_arrete() === true
            && $this->valF["date_retour_rar"] !== ""
            && $this->valF["date_retour_rar"] !== null
            && $this->valF["date_retour_rar"] !== $this->getVal("date_retour_rar")) {
            //
            $infos = array(
                "dossier_coordination" => $this->get_inst_dossier_coordination()->getVal("dossier_coordination"),
                "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                "dossier_autorisation" => $this->get_inst_dossier_coordination()->getVal("dossier_autorisation_ads"),
                "arrete_effectue" => "Oui",
                "date_arrete" => $this->f->formatDate($this->valF["date_retour_rar"], true),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(208, $infos);
            if ($ret !== true) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la notification (208) du référentiel ADS",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (208) du référentiel ADS OK."));
        }

        // Mise à jour des métadonnées des fichiers stockés. Lorsqu'un enregistrement
        // est modifié (mise à jour des dates), le fichier stocké n'est pas
        // modifié mais les métadonnées associées nécessitent d'être mises à jour.
        // La condition nécessaire est qu'il existe un fichier dans les donnnées
        // précédemment envoyées à la base de données.
        if (isset($this->valF['om_fichier_finalise_courrier'])
            && $this->valF['om_fichier_finalise_courrier'] != null
            && $this->valF['om_fichier_finalise_courrier'] != ''
            && $this->valF['finalise'] === true) {
            if ($this->is_type_proces_verbal() === true) {
                $metadata = $this->getMetadata("om_fichier_finalise_proces_verbal");
            } elseif ($this->is_type_arrete() === true) {
                $metadata = $this->getMetadata("om_fichier_finalise_arrete");
            } else {
                $metadata = $this->getMetadata("om_fichier_finalise_courrier");
            }
            $ret = $this->f->storage->update_metadata(
                $this->valF['om_fichier_finalise_courrier'],
                $metadata
            );
            if ($ret === OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour des métadonnées sur le système de stockage de fichiers [courrier.om_fichier_finalise_courrier]",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }
        if (isset($this->valF['om_fichier_signe_courrier'])
            && $this->valF['om_fichier_signe_courrier'] != null
            && $this->valF['om_fichier_signe_courrier'] != ''
            && array_key_exists('om_fichier_signe_courrier', $val) === true
            && $val['om_fichier_signe_courrier'] == $this->valF['om_fichier_signe_courrier']
            && $this->getVal('om_fichier_signe_courrier') != '') {
            //
            if ($this->is_type_proces_verbal() === true) {
                $metadata = $this->getMetadata("om_fichier_signe_proces_verbal");
            } elseif ($this->is_type_arrete() === true) {
                $metadata = $this->getMetadata("om_fichier_signe_arrete");
            } else {
                $metadata = $this->getMetadata("om_fichier_signe_courrier");
            }
            $ret = $this->f->storage->update_metadata(
                $this->valF['om_fichier_signe_courrier'],
                $metadata
            );
            if ($ret === OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour des métadonnées sur le système de stockage de fichiers [courrier.om_fichier_signe_courrier]",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggersupprimer.
     *
     * - Gestion des liaisons NàN.
     *
     * @return boolean
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * Gestion des liaisons NàN.
         */
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Définit si le courrier doit être un mailing.
     *
     * @param  mixed  $contacts_lies Liste des contacts liés
     *
     * @return boolean
     */
    function is_mailing($contacts_lies) {
        // Si ce n'est pas un tableau
        if (!is_array($contacts_lies)) {
            // Créée un tableau
            $contacts_lies = explode(";", $contacts_lies);
        }
        // Compte le nombre de contact
        $nb_contacts_lies = count($contacts_lies);

        // Si il y a un ou aucun contact
        if ($nb_contacts_lies <= 1) {
            //
            return false;
        }

        //
        return true;
    }

    /**
     * Vérifie que la date de retour AR n'est pas vide.
     *
     * @return boolean
     */
    function has_date_retour_rar_not_empty() {
        // Récupère la valeur du champ date_retour_rar
        $date_retour_rar = $this->getVal('date_retour_rar');

        // Si la date de retour est vide
        if (empty($date_retour_rar)) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     * [ajouter_liaisons_table_nan description]
     *
     * @param [type] $table_l [description]
     * @param [type] $table_f [description]
     * @param [type] $field   [description]
     *
     * @return [type] [description]
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field, $values=null) {
        //
        $multiple_values = array();
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } elseif ($values != null) {
            //
            $multiple_values = $values;
            // Si ce n'est pas un tableau
            if (!is_array($values)) {
                //
                $multiple_values = explode(";", $multiple_values);
            }
        }

        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => "",
            );
            // On ajoute l'enregistrement
            require_once "../obj/".$table_l.".class.php";
            $obj_l = new $table_l("]");
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     * [supprimer_liaisons_table_nan description]
     *
     * @param [type] $table [description]
     *
     * @return [type] [description]
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le 
     * générateur en devant surcharger la méthode cleSecondaire afin de supprimer
     * les éléments liés dans les tables NaN.
     *
     * @param [type] $dnu1      [description]
     * @param [type] $table     [description]
     * @param [type] $field     [description]
     * @param [type] $id        [description]
     * @param [type] $dnu2      [description]
     * @param string $selection [description]
     *
     * @return [type] [description]
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }

    /**
     * VIEW - view_overlay_courrier_texte_type.
     *
     * Formulaire de sélection de textes-types. Permet d'afficher le formulaire
     * d'insertion de textes types dans les compléments de documents générés.
     *
     * @return void
     */
    function view_overlay_courrier_texte_type() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        /**
         *
         */
        //
        $template_title = '
        <div class="overlay_title ui-state-active ui-corner-all">
            <h2>%s</h2>
        </div>
        ';
        //
        $template_table = '
        <table class="tab-tab">
            <thead>
        %s
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_head_line = '
        <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
            <th class="icons actions-max-1">
            </th>
            <th class="title col-0 firstcol">
                <span class="name">%s</span>
            </th>
            <th class="title col-1">
                <span class="name">%s</span>
            </th>
            <th class="title col-2 lastcol">
                <span class="name">%s</span>
            </th>
        </tr>
        ';
        //
        $template_body_line = '
        <tr class="tab-data %1$s">
            <td class="icons">
                <input id="checkbox_%2$s" type="checkbox" name="checkbox[%2$s]" />
            </td>
            <td class="col-0 firstcol">
                <span id="id_%2$s">%2$s</span>
            </td>
            <td class="col-1">
                <span id="libelle_%2$s">%3$s</span>
            </td>
            <td class="col-2 lastcol">
                <span id="description_%2$s">%4$s</span>
            </td>
        </tr>
        ';

        /**
         *
         */
        // Récupération de l'ID du courrier
        $courrier_type_id = $_GET['courrier_type'];
        // Déclaration de l'ID CSS du conteneur dialog
        $id_css_conteneur = "form-courrier_texte_type-overlay";
        // Récupération de tous les textes-types
        require_once '../obj/courrier_texte_type.class.php';
        $courrier_texte_type = new courrier_texte_type(0);
        $list_courrier_texte_type = $courrier_texte_type->
            get_courrier_texte_type_by_courrier_type($courrier_type_id);

        /**
         *
         */
        // titre de l'overlay
        printf(
            $template_title,
            _("liste des textes-types")
        );

        /**
         *
         */
        //
        if (empty($list_courrier_texte_type)) {
            //
            $message_class = "error";
            $message = _("Aucun texte-type trouve pour ce type de courrier.");
            $this->f->displayMessage($message_class, $message);
            //
            printf('
                <div class="formControls">
            ');
            // Bouton fermer
            $this->f->layout->display_form_button(array(
                "value" => _("Fermer"),
                "onclick" => "$('#".$id_css_conteneur."').remove();",
            ));
            // Fermeture de form controls
            printf('
                </div>
            ');
            //
            return "";
        }

        /**
         *
         */
        // Ouverture formulaire
        printf('
            <form name="f2" action="" method="post">
        ');
        //
        $table_head_content = sprintf(
            $template_head_line,
            _('courrier_texte_type'),
            _('libelle'),
            _('contenu_om_html')
        );
        //
        $table_body_content = "";
        foreach ($list_courrier_texte_type as $key => $courrier_texte_type) {
            $class = (!isset($class) || $class == "even" ? "odd" : "even");
            $table_body_content .= sprintf(
                $template_body_line,
                $class,
                $courrier_texte_type["courrier_texte_type"],
                $courrier_texte_type["libelle"],
                $courrier_texte_type["contenu_om_html"]
            );
        }
        //
        printf(
            $template_table,
            $table_head_content,
            $table_body_content
        );
        //
        printf('
            <div class="formControls btn_ajouter_overlay">
        ');
        // Bouton ajouter
        $this->f->layout->display_form_button(array(
            "value" => _("Ajouter"),
            "name" => "add",
        ));
        // Bouton annuler
        $this->f->layout->display_form_button(array(
            "value" => _("Annuler"),
            "name" => "cancel",
        ));
        // Fermeture de form controls
        printf('
            </div>
        ');
        // Fermeture du formulaire
        printf('
            </form>
        ');
    }

    /**
     * Permet de récupérer un libellé complet du courrier.
     *
     * @param integer $id Indentifiant de l'enregistrement
     *
     * @return string Libellé explicite
     */
    function get_libelle_explicit($id) {

        // Initialisation de la variable de résultat
        $libelle_explicit = "";

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Inclusion du fichier de requêtes
            if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
                include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
            } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
                include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
            }

            // Requête SQL
            $sql = $sql_courrier_select.$sql_courrier_from;
            $sql .= " WHERE courrier.courrier = ".intval($id);
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $libelle_explicit = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($libelle_explicit);
        }

        // Retourne le résultat
        return $libelle_explicit;
    }

    function get_courrier_from_code_barres($code_barres) {
        if (!is_numeric($code_barres)) {
            return null;
        }
        //
        $sql = "
        SELECT courrier.courrier 
        FROM ".DB_PREFIXE."courrier 
        WHERE courrier.code_barres='".$code_barres."'";
        //
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        //
        $list_courrier = array();
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $list_courrier[] = $row["courrier"];
        }
        $nb_courrier = count($list_courrier);
        if ($nb_courrier != 1) {
            return null;
        } else {
            return $list_courrier[0];
        }
    }

    /**
     * Met à jour les AP liés.
     *
     * @param  integer $courrier Identifiant du courrier
     * @param  array   $values   Valeurs de mise à jour
     *
     * @return boolean
     */
    function update_autorites_police_liees($courrier, $values) {
        // Récupère la liste des autorités de police liées
        $list_autorite_police = $this->get_list_autorite_police($courrier);
        // Nombre d'autorite de police
        $nb_autorite_police = count($list_autorite_police);

        // Si le courrier est lié à une ou plusieurs AP
        if (!empty($list_autorite_police)) {

            // Pour chaque autorité de police
            foreach ($list_autorite_police as $autorite_police_id) {

                // Instance de la classe autorite_police
                require_once '../obj/autorite_police.class.php';
                $autorite_police = new autorite_police($autorite_police_id);

                // Met à jour l'autorité de police
                $update = $autorite_police->update_track_date($autorite_police_id, $values["date_retour_rar"]);

                // Si la mise à jour a échouée
                if ($update === false) {
                    // Message affiché à l'utilisateur
                    $message = _("La mise a jour du suivi des autorites de police a echouee.");
                    if ($nb_autorite_police == 1) {
                        $message = _("La mise a jour de l'autorite de police a echouee.");
                    }
                    $this->addToMessage($message);
                    // Stop le traitement
                    return false;
                }

                // Vérifie que tous les courriers liés sont notifiés
                $check_courriers_notified = $autorite_police->has_all_courriers_notified($autorite_police_id);

                // Si tous les courriers liés sont notifiés
                if ($check_courriers_notified == true) {

                    // Met à jour l'établissement
                    $update_etab = $autorite_police->update_etablissement($autorite_police->getVal("etablissement"), $autorite_police->getVal("autorite_police_decision"));

                    // Si la mise à jour a échouée
                    if ($update_etab === false) {
                        // Stop le traitement
                        return false;
                    }

                    // Instance de la classe etablissement
                    require_once '../obj/etablissement.class.php';
                    $etablissement = new etablissement($autorite_police->getVal("etablissement"));

                    // Message affiché à l'utilisateur
                    $message = sprintf(_("L'etablissement %s a ete mis a jour."), "<b>".$etablissement->getVal("libelle")."</b>");
                    $this->addToMessage($message);
                }
            }

            // Message de validation pour plusieurs AP
            $message = _("Les autorites de police liees ont ete notifiees.");
            // S'il y a qu'une AP
            if ($nb_autorite_police == 1) {
                // Message de validation pour une AP
                $message = _("L'autorite de police liee a ete notifiee.");
            }

            // Message affiché à l'utilisateur
            $this->addToMessage($message);
        }

        // Continue le traitement
        return true;
    }


    /**
     * Affichage du formulaire de suivi du document généré par code barres.
     *
     * @return void
     */
    function view_form_suivi() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // TRAITEMENT
        $postvar = $this->getParameter("postvar");
        if ($postvar != null) {
            // Initialisation de la variable error
            $error = true;
            // Récupère la valeur du champ
            $code_barres = trim($postvar['code_barres']);
            // Si le champ n'est pas vide
            if (!empty($code_barres)) {
                //
                $id_courrier = $this->get_courrier_from_code_barres($code_barres);
                // Si le code barres est valide
                if (!is_null($id_courrier)) {
                    $error = false;
                }
            }
            //
            if($error === false) {
                header("Location:../scr/form.php?obj=courrier&action=3&idx=".$id_courrier."");
                die();
            } else {
                $this->addToMessage(_("Veuillez saisir un code barres valide."));
            }
        }

        // Affichage du message avant d'afficher le formulaire
        $this->message();
        // Inclusion de la classe de gestion des formulaires
        require_once "../obj/om_formulaire.class.php";
        // Ouverture du formulaire
        echo "\t<form";
        echo " method=\"post\"";
        echo " id=\"courrier_suivi_par_code_barres_form\"";
        echo " action=\"".$this->getDataSubmit()."\"";
        echo ">\n";
        // Liste des champs
        $champs = array("code_barres");
        $form = new formulaire(null, 0, 0, $champs);
        $form->setType('code_barres', 'text');
        $form->setLib('code_barres', _('code_barres')." *");
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        echo "\t<div class=\"formControls\">\n";
        $this->f->layout->display_form_button(array("value" => _("Valider"), "name" => "validation"));
        echo "\t</div>\n";
        // Fermeture du formulaire
        echo "\t</form>\n";
    }

    /**
     * Affichage du formulaire permettant l'impression des étiquettes rar de
     * plusieurs documents générés.
     *
     * @return void
     */
    function view_form_rar() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        if (isset($_POST['date'])) {
            $date = $_POST['date'];
        } else {
            $date = "";
        }
        //
        if (isset($_POST['liste_code_barres_instruction'])) {
            $liste_code_barres_instruction = $_POST['liste_code_barres_instruction'];
        } else {
            $liste_code_barres_instruction = "";
        }
        // Compteur du nombre de page générées
        $nbLettres = 0;
        // Liste d'id des instructions
        $id4Gen = array();
        //
        $error = "";
        /**
         * Validation du formulaire
         */
        // Si le formulaire a été validé
        if (isset($_POST['validation'])) {
            //
            if (empty($date) || empty($liste_code_barres_instruction)) {
                //
                $message_class = "error";
                $message = _("Tous les champs doivent etre remplis.");
            } else {
                // Création d'un tableau d'instruction
                $liste = explode("\r\n", $_POST['liste_code_barres_instruction']);
                //
                foreach ($liste as $code_barres) {
                    // On enlève les éventuels espaces saisis
                    $code_barres = trim($code_barres);
                    // Vérification de l'existence de l'instruction
                    if ($code_barres != "") {
                        // Si la valeur transmise est numérique
                        if (is_numeric($code_barres)) {
                            // 
                            $sql = "SELECT courrier FROM ".DB_PREFIXE."courrier WHERE code_barres='".$code_barres."'";
                            $res = $this->f->db->query($sql);
                            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
                            $this->f->isDatabaseError($res);
                            $list_courrier = array();
                            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                                $list_courrier[] = $row["courrier"];
                            }
                            $nbInstr = count($list_courrier);
                            // 
                            if ($nbInstr == "1") {
                                //
                                $courrier_id = $list_courrier[0];
                                //
                                require_once '../obj/courrier.class.php';
                                $instr = new courrier($courrier_id);

                                if ($instr->getVal("mailing") === "f") {


                                    // Si pas de date ou correspond à la date du formulaire on 
                                    // effectue le traitement
                                    if ($instr->getVal("date_envoi_rar") == ""
                                        || $instr->getVal("date_envoi_rar") == $this->f->formatDate($date, false)) {
                                        // Vérification de la finalisation du document
                                        // correspondant au code barres
                                        if ($instr->getVal("finalise") === "t") {
                                            //
                                            $valF = array();
                                            $valF['date_envoi_rar'] = $date;
                                            $instr->update_autoexecute($valF, $courrier_id);
                                            //
                                            $id4Gen[] = $code_barres;
                                            $nbLettres ++;
                                        } else {
                                            //
                                            if ($error != "") {
                                                $error .= "<br/>";
                                            }
                                            $error .= sprintf(_("Le document correspondant au 
                                                code barres %s n'est pas finalise, 
                                                le bordereau ne sera pas genere."),
                                                $code_barres);
                                        }
                                        
                                    } else {
                                        //
                                        if ($error != "") {
                                            $error .= "<br/>";
                                        }
                                        $error .= _("Un document correspondant au code barres")." ".$code_barres." "._("a deja ete envoyee, le bordereau ne sera pas genere.");
                                    }
                                } else {
                                        //
                                        if ($error != "") {
                                            $error .= "<br/>";
                                        }
                                        $error .= _("Un document mailing ne peut pas avoir de code barres")." ".$code_barres." "._(", le bordereau ne sera pas genere.");

                                }
                            } else {
                                //
                                if ($error != "") {
                                    $error .= "<br/>";
                                }
                                $error .= _("Le numero")." ".$code_barres." "._("ne correspond a aucun code barres existant.");
                            }
                        } else {
                            //
                            if ($error != "") {
                                $error .= "<br/>";
                            }
                            $error .= _("Le code barres")." ".$code_barres." "._("n'est pas valide.");
                        }
                    }
                }
            }
        }


        // Affichage du message de validation ou d'erreur
        if (isset($message) && isset($message_class) && $message != "") {
            $this->f->displayMessage($message_class, $message);
        }
        // Affichage du message d'erreur
        if(!empty($error)) {
            $this->f->displayMessage("error", $error);
        }
        // Affichage du message de validation de la saisie
        if ($nbLettres > 0) {
            //
            $message_class = "valid";
            $message = _("Cliquez sur le lien ci-dessous pour telecharger votre document");
            $message .= " : <br/><br/>";
            $message .= "<a class='om-prev-icon pdf-16'";
            $message .= " title=\""._("imprimer les RAR")."\"";
            $message .= " href=\"../scr/form.php?obj=".get_called_class()."&action=10&idx=0&liste=".implode(",",$id4Gen)."\"";
            $message .= " target='_blank'>";
            $message .= _("Telecharger le document pour")." ".$nbLettres." "._("RAR");
            $message .= "</a>";
            $this->f->displayMessage($message_class, $message);
        }
        // Inclusion de la classe de gestion des formulaires
        require_once "../obj/om_formulaire.class.php";
        // Ouverture du formulaire
        echo "\t<form";
        echo " method=\"post\"";
        echo " id=\"suivi_envoi_lettre_rar_form\"";
        echo " action=\"".$this->getDataSubmit()."\"";
        echo ">\n";
        // Paramétrage des champs du formulaire
        $champs = array("date", "liste_code_barres_instruction");
        // Création d'un nouvel objet de type formulaire
        $form = new formulaire(NULL, 0, 0, $champs);
        // Paramétrage du champ date du formulaire
        $form->setLib("date", _("Date")." *");
        $form->setType("date", "date");
        $form->setOnchange("date", "fdate(this)");
        $form->setVal("date", ($date == "" ? date("d/m/Y") : $date));
        $form->setTaille("date", 10);
        $form->setMax("date", 10);
        // Paramétrage du champ liste_code_barres_instruction du formulaire
        $form->setLib("liste_code_barres_instruction", _("Liste des codes barres des documents générés")." *");
        $form->setType("liste_code_barres_instruction", "textarea");
        $form->setVal("liste_code_barres_instruction", $liste_code_barres_instruction);
        $form->setTaille("liste_code_barres_instruction", 20);
        $form->setMax("liste_code_barres_instruction", 20);
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        echo "\t<div class=\"formControls\">\n";
        $this->f->layout->display_form_button(array("value" => _("Valider"), "name" => "validation"));
        echo "\t</div>\n";
        // Fermeture du formulaire
        echo "\t</form>\n";
    }

    /**
     * VIEW - view_pdf_rar.
     * 
     * Génére une édition pdf à imprimer sur bordereaux de lettres RAR.
     *
     * @return void
     */
    function view_pdf_rar() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        if(isset($_GET['liste'])) {
            //
            $listeCodeBarres = explode(',',$_GET['liste']);
            // Classe permettant la mise en page de l'édition pdf
            require_once "../obj/pdf_lettre_rar.class.php";
            $pdf_lettre_rar = new pdf_lettre_rar('P', 'mm', 'A4');
            // Initialisation de la mise en page
            $pdf_lettre_rar->init();
            //
            foreach ($listeCodeBarres as $code_barres) {

                // Recuperation de l'adresse de destination
                $sqlAdresse = "
                SELECT 
                    
                    trim(concat(etablissement.code, ' - ',etablissement.libelle)) as dossier_libelle,
                    
                    trim(concat(' ',contact.nom,' ',contact.prenom,
                        contact.denomination)) as ligne1,
                    
                    trim(concat(contact.adresse_numero,' ',contact.adresse_voie)) as ligne2,
                    
                    CASE contact.adresse_complement
                        WHEN null THEN ''
                        ELSE trim(contact.adresse_complement)
                    END as ligne3,
                    
                    CASE contact.lieu_dit
                        WHEN null THEN ''
                        ELSE trim(contact.lieu_dit)
                    END as ligne4,
                    
                    trim(concat(contact.adresse_cp,' ',contact.adresse_ville,' ',contact.boite_postale,' ',contact.cedex)) as ligne5,
                    
                    code_barres as code_barres

                FROM 
                    ".DB_PREFIXE."courrier
                        LEFT JOIN ".DB_PREFIXE."lien_courrier_contact 
                            ON courrier.courrier = lien_courrier_contact.courrier
                        LEFT JOIN ".DB_PREFIXE."contact 
                            ON lien_courrier_contact.contact = contact.contact
                        LEFT JOIN ".DB_PREFIXE."etablissement 
                            ON contact.etablissement = etablissement.etablissement
                WHERE 
                    courrier.code_barres ='".$code_barres."'
                ";

                //
                $resAdresse = $this->f->db->query($sqlAdresse);
                $this->f->addToLog(
                    __METHOD__."(): db->query(\"".$sqlAdresse."\")", 
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($resAdresse);
                $adresse_dest = $resAdresse->fetchrow(DB_FETCHMODE_ASSOC);


                // Création adresse destinataire sans ligne vide
                $adresse_destinataire = array();
                $adresse_destinataire[] = $adresse_dest['ligne1'];
                $adresse_destinataire[] = $adresse_dest['ligne2'];
                if (!empty($adresse_dest['ligne3'])) {
                    $adresse_destinataire[] = $adresse_dest['ligne3'];
                }
                if (!empty($adresse_dest['ligne4'])) {
                    $adresse_destinataire[] = $adresse_dest['ligne4'];
                }        
                $adresse_destinataire[] = $adresse_dest['ligne5'];

                // Création du champ specifique
                $specifique_content = array();
                $specifique_content[] = $adresse_dest['ligne1'];
                $specifique_content[] = $adresse_dest['dossier_libelle'];
                $specifique_content[] = "|||||".$adresse_dest['code_barres']."|||||";
                unset($adresse_dest['code_barres']);

                // Ajout d'une page aux pdf
                $pdf_lettre_rar->addLetter($adresse_destinataire, $specifique_content);

            }
            //
            if ($this->f->getParameter("edition_output") == "download") {
                $output = "D";
            } else {
                $output = "I"; // Valeur par defaut
            }
            //
            $pdf_lettre_rar->Output("lettre_rar".date("dmYHis").".pdf", $output);
            //
            die();
        }
    }

    /**
     * VIEW - view_display_autorites_police_liees
     *
     * Affiche ou cache le champs des autorités de police liées suivant le type
     * du courrier.
     *
     * @return void
     */
    function view_display_autorites_police_liees() {
        // Désactive les logs
        $this->f->disableLog();

        // Récupère la valeur du champ courrier_type
        $postvar = $this->getParameter("postvar");
        $courrier_type = $postvar['courrier_type'];

        // Initialisation de la variable de résultat
        $display = false;

        // Si cela concerne un courrier d'autorité de police
        if ($this->get_courrier_type_code($courrier_type) == "notifap") {
            // Affiche le champ
            $display = true;
        }

        //
        echo json_encode(
            array(
                "display" => $display,
            )
        );
    }

    /**
     * Retourne le PDF du courrier et de son courrier joint.
     *
     * @param boolean  $preview Activé la prévisualisation
     * @param integer  $id      Identifiant du courrier
     *
     * @return array
     */
    function generate_edition($preview = false, $id = null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $courrier = $this;
        } else {
            // Sinon instancie l'établissement
            $courrier = new courrier($id);
        }

        // Initialisation du tableau des modèles d'éditions
        $liste_modele_edition = array();
        // Initialisation du tableau des idx
        $liste_idx = array();

        // Instancie modele_edition
        $modele_edition_id = $courrier->getVal('modele_edition');
        require_once "../obj/modele_edition.class.php";
        $modele_edition = new modele_edition($modele_edition_id);

        // Récupère la lettre type et l'état
        $om_lettretype_id = $modele_edition->getVal('om_lettretype');
        $om_etat_id = $modele_edition->getVal('om_etat');

        // Si c'est un état
        if (!empty($om_etat_id)) {

            // Mode pour générer le PDF
            $mode = "om_etat";
        }
        // Sinon c'est une lettre type
        else {

            // Mode pour générer le PDF
            $mode = "om_lettretype";
        }

        // Ajoute le modèle d'édition au tableau
        $liste_modele_edition[] = $modele_edition_id;
        // Ajoute l'identifiant au tableau
        $liste_idx[] = $courrier->getVal($courrier->clePrimaire);

        // Si le courrier a un courrier joint
        $courrier_joint_id = $courrier->getVal('courrier_joint');
        if (!empty($courrier_joint_id)) {

            // Instancie le courrier joint
            $courrier_joint = new courrier($courrier_joint_id);

            // Récupère le modèle d'édition du courrier joint
            $courrier_joint_modele_edition = $courrier_joint->getVal('modele_edition');

            // Ajoute le modèle d'édition au tableau
            $liste_modele_edition[] = $courrier_joint_modele_edition;
            // Ajoute l'identifiant au tableau
            $liste_idx[] = $courrier_joint->getVal($courrier_joint->clePrimaire);
        }

        // Nom du fichier
        $filename = _("document_genere")."_".$courrier->getVal($courrier->clePrimaire).'.pdf';

        $params = array();
        // Si l'affichage en mode preview est activé
        if ($preview == true) {

            // Nom du fichier
            $filename = _("previsualisation")."_".$filename;

            // Paramétre du PDF
            $params = array(
                "watermark" => true, 
                "specific" => array(
                    "mode" => "previsualisation",
                ),
            );
        }

        //
        $liste_modele_edition = implode(";", $liste_modele_edition);
        $liste_idx = implode(";", $liste_idx);
        //
        $pdfedition = $this->compute_pdf_output("modele_edition", $liste_modele_edition, null, $liste_idx, $params);
        //
        $return = array(
            "pdf_output" => $pdfedition["pdf_output"],
            "filename" => $filename,
        );

        //
        return $return;
    }

    /**
     * VIEW - view_edition
     *
     * @return void
     */
    function view_edition() {
        //
        $this->checkAccessibility();

        // Génération du PDF
        $generate_edition = $this->generate_edition(true);

        // Affichage du PDF
        $this->expose_pdf_output(
            $generate_edition['pdf_output'], 
            $generate_edition['filename']
        );
    }


    /**
     *
     */
    function is_pv_sendable_to_referentiel_ads() {
        //
        $inst_pv = $this->get_inst_proces_verbal();
        $inst_di = $inst_pv->get_inst_dossier_instruction();
        $inst_dc = $inst_pv->get_inst_dossier_coordination();
        $messages = $inst_dc->get_all_messages();
        //
        $ads_liste_services = array();
        //
        if ($inst_di->get_inst_service()->getVal("code") == 'SI') {
            //
            $ads_liste_services = explode(
                ";", 
                strtolower($this->f->getParameter('ads__liste_services__si'))
            );
        } else {
            $ads_liste_services = explode(
                ";",
                strtolower($this->f->getParameter('ads__liste_services__acc'))
            );
        }
        //
        $consultations = array();
        foreach ($messages as $key => $value) {
            //
            if ($value["type"] == "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS"
                || $value["type"] == "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE") {
                //
                $json = json_decode($value["contenu_json"], true);
                if (isset($json["contenu"]["service_abrege"]) 
                    && in_array(strtolower($json["contenu"]["service_abrege"]), $ads_liste_services)
                    && isset($json["contenu"]["consultation"])
                    && !isset($json["response"])) {
                    $consultations[] = $value;
                }
            }
        }
        //
        if (count($consultations) == 0) {
            return false;
        }
        // Si un PV signé n'est pas présent l'envoi n'est pas possible
        if ($this->getVal('om_fichier_signe_courrier') === "") {
            return false;
        }
        return true;
    }


    /**
     * TREATMENT - send_pv_to_referentiel_ads
     *
     * @return boolean
     */
    function send_pv_to_referentiel_ads($val = array()) {
        //
        $this->begin_treatment(__METHOD__);
        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[209] Dossier PC/ERP Retour de consultation
         * Déclencheur :
         * - L’option ADS est activée
         * - Le DC lié est connecté au référentiel ADS
         * - Procès Verbal généré
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->get_inst_dossier_coordination()->is_connected_to_referentiel_ads() === true
            && $this->is_type_proces_verbal() === true) {
            //
            $inst_pv = $this->get_inst_proces_verbal();
            $inst_di = $inst_pv->get_inst_dossier_instruction();
            $inst_dc = $inst_pv->get_inst_dossier_coordination();
            $messages = $inst_dc->get_all_messages();
            //
            $inst_pv = $this->get_inst_proces_verbal();
            $inst_dir = $inst_pv->get_inst_dossier_instruction_reunion();
            //
            if ($inst_dir->getVal("avis") == "") {
                $this->cleanMessage();
                $this->addToMessage(_("Un avis rendu sur le passage en réunion est obligatoire."));
                return $this->end_treatment(__METHOD__, false);
            }
            // AVIS
            $avis = $inst_dir->get_inst_reunion_avis()->getVal("categorie");
            if ($avis == "") {
                $this->cleanMessage();
                $this->addToMessage(_("Aucun paramétrage pour cet avis. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            // MOTIVATION
            $motivation = $inst_dir->getVal("avis_motivation");
            //
            $ads_liste_services = array();
            if ($inst_di->get_inst_service()->getVal("code") == 'SI') {
                $ads_liste_services = explode(";", strtolower($this->f->getParameter('ads__liste_services__si')));
            } else {
                $ads_liste_services = explode(";", strtolower($this->f->getParameter('ads__liste_services__acc')));
            }
            //
            $consultations = array();
            foreach ($messages as $key => $value) {
                //
                if ($value["type"] == "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS"
                    || $value["type"] == "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE") {
                    //
                    $json = json_decode($value["contenu_json"], true);
                    if (isset($json["contenu"]["service_abrege"]) 
                        && in_array(strtolower($json["contenu"]["service_abrege"]), $ads_liste_services)
                        && isset($json["contenu"]["consultation"])
                        && !isset($json["response"])) {
                        $consultations[] = $value;
                    }
                }
            }
            //
            $old_date = "1970-01-01";
            $consultation = null;
            foreach ($consultations as $key => $value) {
                if ($value["date_emission"] > $old_date) {
                    $consultation = $value;
                    $old_date = $value["date_emission"];
                }
            }
            //
            if ($consultation === null) {
                $this->cleanMessage();
                $this->addToMessage(_("Aucune consultation à laquelle répondre. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $file = $this->f->storage->get($this->getVal("om_fichier_signe_courrier"));
            $nom_fichier = $file["metadata"]["filename"];
            $fichier_base64 = base64_encode($file["file_content"]);
            //
            $json = json_decode($consultation["contenu_json"], true);
            $infos = array(
                "dossier_coordination" => $this->get_inst_dossier_coordination()->getVal("dossier_coordination"),
                "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                "dossier_autorisation" => $this->get_inst_dossier_coordination()->getVal("dossier_autorisation_ads"),
                "date_retour" => $this->f->formatDate($json["contenu"]["date_limite"], true),
                "avis" => $avis,
                "motivation" => $motivation,
                "consultation" => $json["contenu"]["consultation"],
                "nom_fichier" => $nom_fichier,
                "fichier_base64" => $fichier_base64,
            );
            $ret = $this->f->execute_action_to_referentiel_ads(209, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            //
            $json["response"] = array(
                "date" => date("Y-m-d H:i:s"),
            );
            $inst_dcm = $this->get_inst_common("dossier_coordination_message", $consultation["dossier_coordination_message"]);
            $ret = $inst_dcm->update_autoexecute(
                array(
                    "contenu_json" => json_encode($json),
                ),
                null,
                false
            );
            if ($ret !== true) {
                $this->addToLog(__METHOD__."(): Erreur lors de la mise à jour du message.", DEBUG_MODE);
            }
            //
            $this->addToMessage(_("Notification (209) du référentiel ADS OK."));
            return $this->end_treatment(__METHOD__, true);
        }
        //
        return $this->end_treatment(__METHOD__, false);
    }


    /**
     * Récupère le code du type de courrier.
     *
     * @param integer $courrier_type_id Identifiant du type de courrier
     *
     * @return string Code du type de courrier
     */
    function get_courrier_type_code($courrier_type_id) {
        // Initialisation de la variable de résultat
        $code = "";

        // Si l'identifiant n'est pas vide
        if (!empty($courrier_type_id)) {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."courrier_type
                WHERE courrier_type = ".intval($courrier_type_id);
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);
        }

        // Retourne le résultat
        return $code;
    }

    /**
     * Ouverture de la balise form du formulaire d'édition
     * 
     * @return void
     */
    function open_form_tag() {
        $datasubmit = $this->getDataSubmit();
        $datasubmit .= "&contentonly=true";
        printf('<!-- ########## START DBFORM ########## -->
            <form id="edit_date" method="post" name="f1" action="">',
            $this->table,
            $datasubmit
        );
    }

    /**
     *
     * @return string
     */
    function get_display_type_flag() {
        $type_flag = "";
        if ($this->is_type_proces_verbal() === true) {
            $type_flag = _("PROCÈS VERBAL");
        } elseif ($this->is_type_notification_ap() === true) {
            $type_flag = _("NOTIFICATION AUTORITÉ DE POLICE");
        } elseif ($this->is_type_arrete() === true) {
            $type_flag = _("ARRÊTÉ");
        } else {
            return "";
        }
        return '<span class="label label-default">'.$type_flag.'</span>';
    }

    /**
     *
     */
    function getFormTitle($ent) {
        //
        if ($this->getParameter('maj') != "7" && $this->getParameter('maj') != "8") {
            $ent .= " -> ".$this->getVal("code_barres");
            $display_type_flag = $this->get_display_type_flag();
            if ($display_type_flag !== "") {
                $ent .= " ".$display_type_flag;
            }
        }
        return $ent;
    }

    /**
     *
     */
    function getSubFormTitle($ent) {
        //
        $ent = _("document généré");
        $ent .= " -> ".$this->getVal("code_barres");
        $display_type_flag = $this->get_display_type_flag();
        if ($display_type_flag !== "") {
            $ent .= " ".$display_type_flag;
        }
        return $ent;
    }

    /**
     * Contenu spécifique sur le formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function formSpecificContent($maj) {
        //
        $this->fieldset_courriers_enfants($maj);
    }

    /**
     * Contenu spécifique sur le sous-formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function sousFormSpecificContent($maj) {
        //
        $this->fieldset_courriers_enfants($maj);
    }

    /**
     * Fieldset des documents générés liés au courrier.
     * 
     * @param integer $maj Mode de formulaire
     * 
     * @return void
     */
    function fieldset_courriers_enfants($maj) {
        // Liste des courriers enfans
        $list_courrier_child = $this->get_list_courrier_child($this->getVal($this->clePrimaire));
        // 
        if (($maj == 1 || $maj == 2 || $maj == 3)
            && $this->getVal('mailing') == 't'
            && !empty($list_courrier_child)) {
            // Fieldset
            printf("<div id=\"liste_courriers_enfants\" class=\"courriers_enfants_hidden_bloc col_12\">");
            printf("<fieldset class=\"cadre ui-corner-all ui-widget-content\">");
            printf("  <legend class=\"ui-corner-all ui-widget-content ui-state-active\">"
                    ._("Documents generes lies")."</legend>");

            // Ouverture liste
            print("<ul>");
            // Pour chaque courrier enfant
            foreach ($list_courrier_child as $courrier_child_id) {
                //
                $courrier_child = new courrier($courrier_child_id);
                $libelle_explicit = $courrier_child->get_libelle_explicit($courrier_child_id);

                // Pattern lien
                $link = '<a id="link_courrier'.$courrier_child_id.'" class="lienFormulaire" href="form.php?';
                // Objet de l'enregistrement
                $link .= 'obj=%s';
                $link .= '&amp;action=3';
                // Identifiant de l'objet
                $link .= '&amp;idx=%s';
                // Libellé de l'enregistrement, est affiché dans le fil d'Ariane
                $link .= '&amp;idz=%s';
                $link .= '&amp;premier=0';
                $link .= '&amp;advs_id=';
                $link .= '&amp;recherche=';
                $link .= '&amp;tricol=';
                $link .= '&amp;selectioncol=';
                $link .= '&amp;valide=';
                $link .= '&amp;retour=tab">';
                // Valeur affichée
                $link .= '%s';
                $link .= '</a>';

                //
                printf("<li>");
                //
                printf($link, $courrier_child->table, $courrier_child_id, "", $libelle_explicit);
                //
                printf("</li>");
            }
            //
            printf("</ul>");

            // Fermeture du fieldset
            printf("</div>");
            printf("</fieldset>");
            printf("</div>");
        }
    }

    /**
     * Methode de verification des contraintes not null,
     * affiche une erreur si nul.
     *
     * @return void
     */
    function checkRequired() {
        // Récupère la clé du tableau du champ
        $key = array_search('code_barres', $this->required_field);
        // Supprime le champ des obligatoires
        unset($this->required_field[$key]);

        //
        parent::checkRequired();
    }

    var $inst_contact = null;

    /**
     *
     */
    function get_inst_contact($contact = null) {
        //
        if (is_null($this->inst_contact)) {
            //
            if (is_null($contact)) {
                // On recupère la liste des contacts du courrier
                // sur lequel on se trouve
                $list_contact = $this->get_list_contact(
                    $this->getVal($this->clePrimaire)
                );
                // Si il y a un ou plusieurs contacts liés
                if (count($list_contact) != 0) {
                    // Alors on prend le premier
                    $contact = $list_contact[0];
                } else {
                    // Sinon on positionne la valeur à null
                    $contact = null;
                }
            }
            //
            require_once "../obj/contact.class.php";
            $this->inst_contact = new contact($contact);
        }
        //
        return $this->inst_contact;
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "courrier",
        "courrier_texte_type_complement1",
        "courrier_texte_type_complement2",
        "etablissement",
        "dossier_coordination",
        "dossier_instruction",
        "signataire",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        // Récupération des libellés de courrier
        $labels = parent::get_labels_merge_fields();
        // Récupération des libellés de signataire
        require_once "../obj/signataire.class.php";
        $inst_signataire = new signataire('0');
        $labels = array_merge(
            array(_('courrier') => array_merge(
                $labels['courrier'],
                $inst_signataire->get_labels_merge_fields()[_('signataire')]
            ))
        );
        // Récupération des libellés de contact
        $inst_contact = $this->get_inst_contact();
        $labels = array_merge(
            $labels,
            $inst_contact->get_labels_merge_fields()
        );
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        // Récupération des valeurs du courrier
        $values = parent::get_values_merge_fields();
        // Récupération des valeurs du signataire
        $signataire = $this->getVal("signataire");
        if ($signataire == "") {
            $signataire = '0';
        }
        require_once "../obj/signataire.class.php";
        $inst_signataire = new signataire($signataire);
        $values_signataire = $inst_signataire->get_values_merge_fields();
        // Récupération des valeurs du contact
        $inst_contact = $this->get_inst_contact();
        $values_contact= $inst_contact->get_values_merge_fields();
        // Retour de toutes les valeurs
        return array_merge(
            $values,
            $values_signataire,
            $values_contact
        );
    }


    /**
     *
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "dossier_instruction",
            "dossier_coordination",
            "etablissement",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = array();
                //
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem_method = "get_inst_".$value;
                    $elem = $this->$elem_method();
                    if ($elem != null) {
                        $elem_values = $elem->get_merge_fields($type);
                        $values = array_merge($values, $elem_values);
                    }
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = array();
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem = new $value(0);
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    var $inst_dossier_instruction = null;
    var $inst_dossier_coordination = null;
    var $inst_etablissement = null;

    /**
     * 
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        // Récupération des éventuels paramètres spécifiques
        $edition_params_specific = $this->getParameter("edition_params_specific");
        // Si l'on est en contexte de prévisualisation c'est que l'on vient
        // d'une analyse. Donc on ne fusionne pas les champs du PV
        // mais on récupère l'identifiant de l'analyse.
        if (isset($edition_params_specific["mode"]) 
            && $edition_params_specific["mode"] == "previsualisation" 
            && isset($edition_params_specific["analyses"])) {
            // On ne fusionne aucune valeur du PV, ce qui laissera
            // les champs de fusion en l'état dans l'édition
            require_once "../obj/analyses.class.php";
            $analyse = new analyses($edition_params_specific["analyses"]);
            $dossier_instruction = $analyse->getVal("dossier_instruction");
        }

        //
        if (!is_null($dossier_instruction)) {
            require_once "../obj/dossier_instruction.class.php";
            return new dossier_instruction($dossier_instruction);
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            require_once "../obj/dossier_instruction.class.php";
            $this->inst_dossier_instruction = new dossier_instruction($dossier_instruction);
        }
        //
        return $this->inst_dossier_instruction;
    }

    /**
     * 
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if (!is_null($dossier_coordination)) {
            require_once "../obj/dossier_coordination.class.php";
            return new dossier_coordination($dossier_coordination);
        }
        //
        if (is_null($this->inst_dossier_coordination)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            //
            if (count($inst_dossier_instruction->val) == 0) {
                require_once "../obj/dossier_coordination.class.php";
                $this->inst_dossier_coordination = new dossier_coordination($this->getVal("dossier_coordination"));
            } else {
                $this->inst_dossier_coordination = $inst_dossier_instruction->get_inst_dossier_coordination();
            }
        }
        //
        return $this->inst_dossier_coordination;
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (!is_null($etablissement)) {
            require_once "../obj/etablissement.class.php";
            return new etablissement($etablissement);
        }
        //
        if (is_null($this->inst_etablissement)) {
            //
            $inst_dossier_coordination = $this->get_inst_dossier_coordination();
            //
            if (count($inst_dossier_coordination->val) == 0) {
                require_once "../obj/etablissement.class.php";
                $this->inst_etablissement = new etablissement($this->getVal("etablissement"));
            } else {
                $this->inst_etablissement = $inst_dossier_coordination->get_inst_etablissement();
            }
            
        }
        //
        return $this->inst_etablissement;
    }

    /**
     * 
     */
    function get_inst_proces_verbal() {
        //
        $proces_verbal = $this->getVal("proces_verbal");
        if ($proces_verbal == "") {
            return null;
        }
        //
        require_once "../obj/proces_verbal.class.php";
        $proces_verbal = new proces_verbal($proces_verbal);
        //
        return $proces_verbal;
    }

    /**
     * 
     */
    function get_inst_visite() {
        //
        $visite = $this->getVal("visite");
        if ($visite == "") {
            return null;
        }
        //
        require_once "../obj/visite.class.php";
        $visite = new visite($visite);
        //
        return $visite;
    }


    /**
     *
     */
    function get_all_merge_fields_for_proces_verbal($type) {
        // Si récupération des libellés
        if ($type == 'labels') {
            // Instance d'un PV qui n'existe pas
            require_once "../obj/proces_verbal.class.php";
            $proces_verbal = new proces_verbal("0");
            // Retour de ses libellés + tous ceux de courrier et ses liaisons
            return array_merge(
                $proces_verbal->get_merge_fields($type),
                $this->get_all_merge_fields($type)
            );
        }
        // Récupération de l'éventuel PV lié
        $proces_verbal = $this->get_inst_proces_verbal();
        // S'il existe
        if ($proces_verbal !== null) {
            // Retour de ses valeurs + toutes celles de courrier et ses liaisons
            return array_merge(
                $proces_verbal->get_merge_fields($type),
                $this->get_all_merge_fields($type)
            );
        }
        // Sinon retour uniquement des valeurs de courrier et ses liaisons
        return $this->get_all_merge_fields($type);
    }

    /**
     *
     */
    function get_all_merge_fields_for_visite($type) {
        // Si récupération des libellés
        if ($type == 'labels') {
            // Instance d'une visite qui n'existe pas
            require_once "../obj/visite.class.php";
            $visite = new visite("0");
            // Retour de ses libellés + tous ceux de courrier et ses liaisons
            return array_merge(
                $visite->get_merge_fields($type),
                $this->get_all_merge_fields($type)
            );
        }
        // Récupération de l'éventuelle visite liée
        $visite = $this->get_inst_visite();
        // S'il existe
        if ($visite !== null) {
            // Retour de ses valeurs + toutes celles de courrier et ses liaisons
            return array_merge(
                $visite->get_merge_fields($type),
                $this->get_all_merge_fields($type)
            );
        }
        // Sinon retour uniquement des valeurs de courrier et ses liaisons
        return $this->get_all_merge_fields($type);
    }

    /**
     * Récupère le prochain identifiant de la séquence permettant de numéroter
     * les arrêtés.
     *
     * @param string $year Année.
     *
     * @return integer
     */
    function get_next_arrete_number($year) {

        // Initialise la variable à 0
        $next_id = null;

        /**
         * On compose les identifiants qui composent la séquence.
         */
        // Clé unique.
        // Exemple : 2016
        $unique_key = sprintf('%s', $year);
        // Nom de la table représentant la séquence pour appel via la méthode
        // database::nextId() qui prend un nom de séquence sans son suffixe
        // '_seq'.
        // Exemple : openaria.arrete_2016
        $table_name = sprintf('%sarrete_%s', DB_PREFIXE, $unique_key);
        // Nom de la séquence avec son suffixe 'seq'.
        // Exemple : openaria.arrete_2016_seq
        $sequence_name = sprintf('%s_seq', $table_name);

        /**
         * On interroge la base de données pour vérifier si la séquence existe
         * ou non. Si il y a un retour à l'exécution de la requête alors la
         * séquence existe et si il n'y en a pas alors la séquence n'existe
         * pas.
         *
         * Cette requête particulière (car sur la table pg_class) nécessite
         * d'être exécutée sur le schéma public pour fonctionner correctement.
         * En effet, par défaut postgresql positionne search_path avec la
         * valeur '"$user", public' ce qui peut causer des mauvais effets de
         * bord si l'utilisateur et le schéma sont identique.
         * On force donc le schéma public sur le search_path pour être sûr que
         * la requête suivante s'exécute correctement.
         */
        $res_search_path = $this->f->db->query("set search_path=public;");
        $this->f->isDatabaseError($res_search_path);
        $query_sequence_exists = sprintf(
            'SELECT 
                * 
            FROM 
                pg_class 
            WHERE
                relkind = \'S\' 
                AND oid::regclass::text = \'%s\'
            ;',
            $sequence_name
        );
        $res_sequence_exists = $this->f->db->getone($query_sequence_exists);
        $this->addToLog(
            __METHOD__.'(): db->getone("'.$query_sequence_exists.'");',
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_sequence_exists);

        /**
         * Si la séquence n'existe pas, alors on la cré.
         */
        if ($res_sequence_exists === null) {

            // Création de la sequence si elle n'existe pas
            $res = $this->f->db->createSequence($table_name);
            $this->f->addToLog(
                __METHOD__.'(): db->createSequence("'.$table_name.'");',
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
        }

        /**
         * On récupère le prochain numéro de la séquence fraichement créée ou
         * créée de longue date.
         */
        $next_id = $this->f->db->nextId($table_name, false);
        $this->addToLog(
            __METHOD__.'(): db->nextId("'.$table_name.'", false);',
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($next_id);

        /**
         * On retourne le prochain identifiant.
         */
        return $next_id;
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_modele_edition($modele_edition = null) {
        //
        return $this->get_inst_common(
            "modele_edition",
            $modele_edition
        );
    }

    /**
     *
     */
    function get_inst_signataire($signataire = null) {
        //
        return $this->get_inst_common(
            "signataire",
            $signataire
        );
    }

    /**
     *
     */
    function get_inst_reunion($reunion = null) {
        //
        if ($reunion === null) {
            $inst_pv = $this->get_inst_proces_verbal();
            return $inst_pv->get_inst_reunion();
        }
        //
        return $this->get_inst_common(
            "reunion",
            $reunion
        );
    }

    // }}} END - GET INST

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        //
        "om_fichier_finalise_courrier" => array(
            //
            "titre" => "get_md_titre_finalise_courrier",
            "description" => "get_md_description_finalise_courrier",
            //
            "origine" => "get_md_origine_genere",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
        ),
        //
        "om_fichier_finalise_proces_verbal" => array(
            //
            "titre" => "get_md_titre_finalise_proces_verbal",
            "description" => "get_md_description_finalise_courrier",
            //
            "origine" => "get_md_origine_genere",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "pv_erp_numero" => "get_md_pv_erp_numero",
            "pv_erp_nature_analyse" => "get_md_pv_erp_nature_analyse",
            "pv_erp_reference_urbanisme" => "get_md_pv_erp_reference_urbanisme",
            "pv_erp_avis_rendu" => "get_md_pv_erp_avis_rendu",
            //
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
        ),
        //
        "om_fichier_signe_courrier" => array(
            //
            "titre" => "get_md_titre_signe_courrier",
            "description" => "get_md_description_signe_courrier",
            //
            "origine" => "get_md_origine_televerse",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
        ),
        //
        "om_fichier_signe_proces_verbal" => array(
            //
            "titre" => "get_md_titre_signe_proces_verbal",
            "description" => "get_md_description_signe_courrier",
            //
            "origine" => "get_md_origine_televerse",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "pv_erp_numero" => "get_md_pv_erp_numero",
            "pv_erp_nature_analyse" => "get_md_pv_erp_nature_analyse",
            "pv_erp_reference_urbanisme" => "get_md_pv_erp_reference_urbanisme",
            "pv_erp_avis_rendu" => "get_md_pv_erp_avis_rendu",
            //
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
        ),
        //
        "om_fichier_finalise_arrete" => array(
            //
            "titre" => "get_md_titre_finalise_courrier",
            "description" => "get_md_description_finalise_courrier",
            //
            "origine" => "get_md_origine_genere",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
            //
            "arrete_numero" => "get_md_arrete_numero",
            "arrete_reglementaire" => "get_md_arrete_reglementaire",
            "arrete_notification" => "get_md_arrete_notification",
            "arrete_date_notification" => "get_md_arrete_date_notification",
            "arrete_publication" => "get_md_arrete_publication",
            "arrete_date_publication" => "get_md_arrete_date_publication",
            "arrete_temporaire" => "get_md_arrete_temporaire",
            "arrete_expiration" => "get_md_arrete_expiration",
            "arrete_date_controle_legalite" => "get_md_arrete_date_controle_legalite",
            "arrete_nature_acte" => "get_md_arrete_nature_acte",
            "arrete_nature_acte_niv1" => "get_md_arrete_nature_acte_niv1",
            "arrete_nature_acte_niv2" => "get_md_arrete_nature_acte_niv2",
        ),
        //
        "om_fichier_signe_arrete" => array(
            //
            "titre" => "get_md_titre_signe_courrier",
            "description" => "get_md_description_signe_courrier",
            //
            "origine" => "get_md_origine_televerse",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
            //
            "arrete_numero" => "get_md_arrete_numero",
            "arrete_reglementaire" => "get_md_arrete_reglementaire",
            "arrete_notification" => "get_md_arrete_notification",
            "arrete_date_notification" => "get_md_arrete_date_notification",
            "arrete_publication" => "get_md_arrete_publication",
            "arrete_date_publication" => "get_md_arrete_date_publication",
            "arrete_temporaire" => "get_md_arrete_temporaire",
            "arrete_expiration" => "get_md_arrete_expiration",
            "arrete_date_controle_legalite" => "get_md_arrete_date_controle_legalite",
            "arrete_nature_acte" => "get_md_arrete_nature_acte",
            "arrete_nature_acte_niv1" => "get_md_arrete_nature_acte_niv1",
            "arrete_nature_acte_niv2" => "get_md_arrete_nature_acte_niv2",
        ),
    );

    //
    function get_md_titre_finalise_courrier() {
        $titre = $this->get_md_titre_common();
        $inst_modele_edition = $this->get_inst_modele_edition();
        if ($inst_modele_edition->getVal("libelle") !== "") {
            $titre .= $inst_modele_edition->getVal("libelle");
        } else {
            $titre .= "document généré";
        }
        return $titre;
    }
    function get_md_titre_finalise_proces_verbal() {
        $titre = $this->get_md_titre_common();
        $titre .= "procès verbal";
        return $titre;
    }
    function get_md_description_finalise_courrier() { return "document généré finalisé"; }
    function get_md_titre_signe_courrier() {
        $titre = $this->get_md_titre_finalise_courrier();
        $titre .= " (signé)";
        return $titre;
    }
    function get_md_titre_signe_proces_verbal() {
        $titre = $this->get_md_titre_finalise_proces_verbal();
        $titre .= " (signé)";
        return $titre;
    }
    function get_md_description_signe_courrier() { return "document généré numérisé signé"; }
    //
    function get_md_date_signature() {
        if (array_key_exists("date_retour_signature", $this->valF)) {
            return $this->valF["date_retour_signature"];
        }
        return $this->getVal("date_retour_signature");
    }
    //
    function get_md_arrete_numero() { return $this->get_arrete_number(); }
    function get_md_arrete_reglementaire() {
        $value = $this->get_inst_ap_type_arrete_info("arrete_reglementaire");
        if ($value == "t") {
            return "true";
        } elseif ($value === "f") {
            return "false";
        }
        return "";
    }
    function get_md_arrete_notification() {
        $value = $this->get_inst_ap_type_arrete_info("arrete_notification");
        if ($value == "t") {
            return "true";
        } elseif ($value === "f") {
            return "false";
        }
        return "";
    }
    function get_md_arrete_date_notification() {
        if (array_key_exists("date_retour_rar", $this->valF)) {
            return $this->valF["date_retour_rar"];
        }
        return $this->getVal("date_retour_rar");
    }
    function get_md_arrete_publication() {
        $value = $this->get_inst_ap_type_arrete_info("arrete_publication");
        if ($value == "t") {
            return "true";
        } elseif ($value === "f") {
            return "false";
        }
        return "";
    }
    function get_md_arrete_date_publication() { return ""; }
    function get_md_arrete_temporaire() {
        $value = $this->get_inst_ap_type_arrete_info("arrete_temporaire");
        if ($value == "t") {
            return "true";
        } elseif ($value === "f") {
            return "false";
        }
        return "";
    }
    function get_md_arrete_expiration() {
        return ""; // XXX
    }
    function get_md_arrete_date_controle_legalite() {
        if (array_key_exists("date_retour_controle_legalite", $this->valF)) {
            return $this->valF["date_retour_controle_legalite"];
        }
        return $this->getVal("date_retour_controle_legalite");
    }
    function get_md_arrete_nature_acte() {
        $nomenclature_actes_nature = $this->get_inst_ap_type_arrete_info("nomenclature_actes_nature");
        if ($nomenclature_actes_nature == "arretes_reglementaires") {
            return _("Arrêtés réglementaires");
        } elseif ($nomenclature_actes_nature == "arretes_individuels") {
            return _("Arrêtés individuels");
        }
        return "";
    }
    function get_md_arrete_nature_acte_niv1() { return $this->get_inst_ap_type_arrete_info("nomenclature_actes_matiere_niv1"); }
    function get_md_arrete_nature_acte_niv2() { return $this->get_inst_ap_type_arrete_info("nomenclature_actes_matiere_niv2"); }

    // }}} END - METADATA FILESTORAGE

    function get_inst_ap_type_arrete_info($info) {
        $list_ap = $this->get_list_autorite_police($this->getVal($this->clePrimaire));
        if (count($list_ap) !== 1) {
            return "";
        }
        $inst_ap = $this->get_inst_common("autorite_police", $list_ap[0]);
        $inst_ap_type = $this->get_inst_common(
            "autorite_police_decision",
            $inst_ap->getVal("autorite_police_decision")
        );
        return $inst_ap_type->getVal($info);
    }


}

?>
