<?php
/**
 * Surcharge de la classe etablissement_unite.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/etablissement_unite.class.php";

class etablissement_unite__contexte_etab__ua_archive extends etablissement_unite {

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        /**
         *
         */
        //
        $this->class_actions = array();
        foreach (array(3, 6, ) as $key) {
            if (isset($this->class_actions_available[$key])) {
                $this->class_actions[$key] = $this->class_actions_available[$key];
            }
        }
    }

}

?>
