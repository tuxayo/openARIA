<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/proces_verbal.class.php";

/**
 * Selon qu'il s'agisse d'un PV (re)généré ou ajouté les comportents diffèrent,
 * notamment l'utilisation de la table courrier dans le cas du (re)généré.
 */
class proces_verbal extends proces_verbal_gen {

    /**
     * On définit le type global de champs spécifiques.
     */
    var $abstract_type = array(
        "om_fichier_signe" => "file",
    );

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);

        // Si l'on est dans le contexte de l'action ajouter on remplit
        // le $this->val pour éviter des effets secondaires spécifiques
        // à cette action.
        // XXX - L'id peut valoir 0 dans d'autres contextes.
        if ($id == 0) {
            $i = 0;
            foreach ($this->champs as $champ) {
                $this->val[$i++] = "";
            }
        }
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 000 - ajouter
        // Génération d'un nouveau PV
        $this->class_actions[0]["condition"] = array(
            "is_generable",
            "is_di_from_good_service",
        );

        // ACTION - 001 - modifier
        // Génération d'un nouveau PV
        $this->class_actions[1]["condition"] = array(
            "is_added",
            "is_di_from_good_service",
        );
        
        // ACTION - 005 - view_onglet
        // Interface spécifique de l'onglet
        $this->class_actions[5] = array(
            "identifier" => "view_onglet",
            "view" => "view_onglet",
            "permission_suffix" => "tab",
        );

        // ACTION - 006 - view_ajouter_pv
        // Ajout d'un PV au format PDF sans génération
        $this->class_actions[6] = array(
            "identifier" => "view_ajouter_pv",
            "method" => "ajouter_pv",
            "button" => _("Ajouter")." "._("le")." "._("proces_verbal"),
            "permission_suffix" => "ajouter",
            "condition" => array(
                "is_di_from_good_service",
            ),
        );

        // ACTION - 007 - view_regenerer_pv
        // Regénération du dernier PV
        $this->class_actions[7] = array(
            "identifier" => "view_regenerer_pv",
            "method" => "regenerer_pv",
            "button" => _("Regenerer")." "._("le")." "._("proces_verbal"),
            "permission_suffix" => "modifier",
            "condition" => array(
                "is_regenerable",
                "is_di_from_good_service",
            ),
        );

        // ACTION - 008 - add_signed_file
        // Pour ajouter un PV signé à un PV généré
        $this->class_actions[8] = array(
            "identifier" => "add_signed_file",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("ajouter le PV signe"),
                "order" => 80,
                "class" => "add-16",
            ),
            "view" => "view_pv_signed_file",
            "permission_suffix" => "modifier",
            "condition" => array(
                "is_generated",
                "hasnt_signed_file",
                "is_di_from_good_service",
            ),
        );

        // ACTION - 009 - edit_signed_file
        // Pour ajouter un PV signé à un PV généré
        $this->class_actions[9] = array(
            "identifier" => "edit_signed_file",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("remplacer le PV signe"),
                "order" => 80,
                "class" => "edit-16",
            ),
            "view" => "view_pv_signed_file",
            "permission_suffix" => "modifier",
            "condition" => array(
                "is_generated",
                "has_signed_file",
                "is_di_from_good_service",
            ),
        );

        // ACTION - 051 - send_pv_to_referentiel_ads
        //
        $this->class_actions[51] = array(
            "identifier" => "send_pv_to_referentiel_ads",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("Transmettre au référentiel ADS"),
                "order" => 120,
                "class" => "send-pv-to-referentiel-ads-16",
            ),
            "method" => "send_pv_to_referentiel_ads",
            "button" => "send_pv_to_referentiel_ads",
            "permission_suffix" => "send_pv_to_referentiel_ads",
            "condition" => array(
                "is_option_referentiel_ads_enabled",
                "is_dc_connected_to_referentiel_ads",
                "is_pv_sendable_to_referentiel_ads",
            ),
        );

    }

    /**
     *
     */
    function get_inst_document_genere($document_genere = null) {
        return $this->get_inst_common(
            "courrier",
            $document_genere,
            "courrier_genere"
        );
    }

    /**
     *
     */
    function send_pv_to_referentiel_ads($val = array()) {
        $inst_dg = $this->get_inst_document_genere();
        $ret = $inst_dg->send_pv_to_referentiel_ads($val);
        if ($ret === true) {
            $this->correct = true;
        } else {
            $this->correct = false;
        }
        $this->addToMessage(
            $inst_dg->msg
        );
        return $ret;
    }

    /**
     *
     */
    function is_option_referentiel_ads_enabled() {
        $inst_dg = $this->get_inst_document_genere();
        return $inst_dg->is_option_referentiel_ads_enabled();
    }

    /**
     *
     */
    function is_dc_connected_to_referentiel_ads() {
        $inst_dg = $this->get_inst_document_genere();
        return $inst_dg->is_dc_connected_to_referentiel_ads();
    }

    /**
     *
     */
    function is_pv_sendable_to_referentiel_ads() {
        $inst_dg = $this->get_inst_document_genere();
        return $inst_dg->is_pv_sendable_to_referentiel_ads();
    }

    /**
     * VIEW - view_onglet
     * 
     * Sous-tab spécifique des PV dans le DI.
     *
     * @return void
     */
    function view_onglet() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupération de l'ID du dossier d'instruction
        (isset($_GET["idxformulaire"]) and !empty($_GET["idxformulaire"])) ?
            $id_di = $_GET["idxformulaire"] : $id_di = "";
        // Récupération de l'ID du dernier PV s'il existe
        $id_dernier_pv = "0";
        if ($id_di != "") {
            // On instancie le dossier d'instruction
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($id_di);
            $id_analyse = $di->get_id_analyse();
            // Si la récupération de l'ID de l'analyse n'échoue pas
            if ($id_analyse != false) {
                // on instancie cette dernière
                require_once "../obj/analyses.class.php";
                $analyse = new analyses($id_analyse);
                $id_dernier_pv = $analyse->getVal("dernier_pv");
            }
        }
        // Définition de l'objet
        $obj = "proces_verbal";
        // Définition de l'URL du sous-form des PV générés
        $url_form_gen = sprintf("../scr/sousform.php?obj=%s&action=0&tri=&objsf=%s&premiersf=0&retourformulaire=dossier_instruction&idxformulaire=%s&trisf=&retour=tab",
            $obj,
            $obj,
            $id_di
        );
        // Définition de l'URL du sous-form des PV regénérés
        $url_form_regen = sprintf("../scr/sousform.php?obj=%s&action=7&idx=%s&tri=&objsf=%s&premiersf=0&retourformulaire=dossier_instruction&idxformulaire=%s&trisf=&retour=tab",
            $obj,
            $id_dernier_pv,
            $obj,
            $id_di
        );
        // Définition de l'URL du sous-form des PV ajoutés
        $url_form_add = sprintf("../scr/sousform.php?obj=%s&action=6&idx=0&tri=&objsf=%s&premiersf=0&retourformulaire=dossier_instruction&idxformulaire=%s&trisf=&retour=tab",
            $obj,
            $obj,
            $id_di
        );
        // Définition de l'URL du sous-tab des PV
        $url_tab = sprintf("../scr/soustab.php?obj=%s&retourformulaire=dossier_instruction&idxformulaire=%s",
            $obj,
            $id_di
        );
        // Ouverture du conteneur des boutons
        printf("\t<div class=\"formControls\">\n");
        //
        // Si utilisateur possède le droit de générer
        $action_generer_key = $this->get_action_key_for_identifier("ajouter");
        if ($this->is_action_available($action_generer_key)) {
            // Bouton générer
            $js_action_gen = sprintf("ajaxIt('%s', '%s');",
                $obj,
                $url_form_gen
            );
            printf('<input class="om-button ui-button ui-widget ui-state-default
                ui-corner-all" type="button" name="%s" id="%s" value="%s"
                role="button" aria-disabled="false" onclick="%s">',
                "generer_pv",
                "generer_pv",
                _("Generer un nouveau PV"),
                $js_action_gen
            );
        }
        // Si utilisateur possède le droit de regénérer
        $action_regenerer_key = $this->get_action_key_for_identifier("view_regenerer_pv");
        if ($this->is_action_available($action_regenerer_key)) {
            // Bouton regénérer
            $js_action_regen = sprintf("ajaxIt('%s', '%s');",
                $obj,
                $url_form_regen
            );
            printf('<input class="om-button ui-button ui-widget ui-state-default
                ui-corner-all" type="button" name="%s" id="%s" value="%s"
                role="button" aria-disabled="false" onclick="%s">',
                "regenerer_pv",
                "regenerer_pv",
                _("Regenerer le dernier PV"),
                $js_action_regen
            );
        }
        // Si utilisateur possède le droit d'ajouter un pv tiers
        $action_ajouter_key = $this->get_action_key_for_identifier("view_ajouter_pv");
        if ($this->is_action_available($action_ajouter_key)) {
            // Bouton ajouter
            $js_action_add = sprintf("ajaxIt('%s', '%s');",
                $obj,
                $url_form_add
            );
            printf('<input class="om-button ui-button ui-widget ui-state-default
                ui-corner-all" type="button" name="%s" id="%s" value="%s"
                role="button" aria-disabled="false" onclick="%s">',
                "ajouter_pv",
                "ajouter_pv",
                _("Ajouter un PV"),
                $js_action_add
            );
        }
        // Fermeture du conteneur des boutons
        printf('</div>');
        // Sous-tab des PV si DI récupéré
        if ($id_di != "") {
            printf('<div id="soustab-proces_verbal"></div>');
            printf('
                <script>
                ajaxTab(\'%s\', \'%s\');
                </script>',
                $obj,
                $url_tab
            );
        }
        // Fin de la vue
        printf("<div class=\"visualClear\"></div>");
    }

    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un sous-formulaire.
     * 
     * @param string    $subEnt Fil d'Ariane récupéréré 
     * @return                  Fil d'Ariane
     */
    function getSubFormTitle($subEnt) {
        // Pas de fil d'ariane en vue soustab spécifique
        if ($this->getParameter("maj") == 5) {
            return "";
        }
        return $subEnt;
    }

    /**
     * Définition du type des champs suivant le contexte
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);

        // Pour tous les formulaires
        $form->setType('proces_verbal','hidden');
        $form->setType('courrier_genere','selecthidden');

        // Selon le formulaire
        switch ($maj) {
            // MODE AJOUTER (GENERER)
            case 0:
                $form->setType('dossier_instruction','selecthidden');
                $form->setType('numero','hidden');
                $form->setType('genere','hidden');
                $form->setType('om_fichier_finalise','hidden');
                $form->setType('om_fichier_signe','hidden');
                break;
            // MODE MODIFIER
            case 1:
                $form->setType('dossier_instruction','selecthidden');
                $form->setType('numero','hidden');
                $form->setType('genere','hidden');
                $form->setType('om_fichier_finalise','hidden');
                if ($this->retourformulaire == "") {
                    $form->setType('om_fichier_signe', 'upload');
                } else {
                    $form->setType('om_fichier_signe', 'upload2');
                }
                $form->setType('modele_edition', 'selecthidden');
                $form->setType('signataire','selecthidden');
                // si PV généré
                if ($this->getVal("genere") == 't') {
                    $form->setType('date_redaction','datestatic');
                    $form->setType('signataire','select');
                    $form->setType('om_fichier_signe', 'hidden');
                }
                break;
            // MODE SUPPRIMER
            case 2:
                $form->setType('genere','hidden');
                $form->setType('om_fichier_finalise','hidden');
                $form->setType('om_fichier_signe','hidden');
                $form->setType('modele_edition', 'selecthidden');
                break;
            // MODE CONSULTER
            case 3:
                $form->setType('dossier_instruction','selecthidden');
                $form->setType('genere','hidden');
                // si PV ajouté
                $form->setType('modele_edition', 'selecthidden');
                $form->setType('om_fichier_finalise','hidden');
                $form->setType('signataire','selecthidden');
                $form->setType('numero','hidden');
                // si PV généré
                if ($this->getVal("genere") == 't') {
                    $form->setType('modele_edition', 'selectstatic');
                    $form->setType('om_fichier_finalise','file');
                    $form->setType('signataire','selectstatic');
                    $form->setType('numero','static');
                }
                $form->setType('om_fichier_signe','file');
                break;
            // ACTION AJOUTER
            case 6:
                $form->setType('dossier_instruction','selecthidden');
                $form->setType('dossier_instruction_reunion','select');
                $form->setType('signataire','selecthidden');
                $form->setType('modele_edition','selecthidden');
                $form->setType('numero','hidden');
                $form->setType('genere','hidden');
                $form->setType('date_redaction','date');
                $form->setType('om_fichier_finalise','hidden');
                if ($this->retourformulaire == "") {
                    $form->setType('om_fichier_signe', 'upload');
                } else {
                    $form->setType('om_fichier_signe', 'upload2');
                }
                break;
            // ACTION REGÉNÉRER
            case 7:
                $form->setType('dossier_instruction','selecthidden');
                $form->setType('dossier_instruction_reunion','select');
                $form->setType('signataire','select');
                $form->setType('modele_edition','select');
                $form->setType('date_redaction','date');
                $form->setType('numero','hiddenstatic');
                $form->setType('genere','hidden');
                $form->setType('om_fichier_finalise','hidden');
                $form->setType('om_fichier_signe','hidden');
                break;
        }
        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * Pré-saisie des valeurs du sous-formulaire lors de son ouverture
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire);

        // Instanciation du DI
        require_once "../obj/dossier_instruction.class.php";
        $di = new dossier_instruction($idxformulaire);
        // Instanciation de l'analyse
        require_once "../obj/analyses.class.php";
        $analyse = new analyses($di->get_id_analyse());
        // Récupération du modèle d'édition paramétré dans l'analyse
        $id_modele_edition = $analyse->getVal("modele_edition_proces_verbal");

        // MODE AJOUTER (GENERER)
        if ($maj == 0) {
            // Si formulaire pas validé (= première instance)
            if ($validation == 0) {
                // Date du jour
                $form->setVal("date_redaction", date('d/m/Y'));
                // Modèle d'édition paramétré dans l'analyse
                $form->setVal("modele_edition", $id_modele_edition);
            }
            // PV généré
            $form->setVal("genere", "t");
        }
        // ACTION AJOUTER
        if ($maj == 6) {
            // Si formulaire pas validé (= première instance)
            if ($validation == 0) {
                // Date du jour
                $form->setVal("date_redaction", date('d/m/Y'));
                // Modèle d'édition paramétré dans l'analyse
                $form->setVal("modele_edition", $id_modele_edition);
                // PV ajouté
                $form->setVal("genere", "f");
                // Fichier
                $form->setVal("om_fichier_signe", "");
            }
        }
    }

    /**
     * Modification des valeurs du formulaire d'ajout après sa validation
     */
    function setValFAjout($val = array()) {
        //
        parent::setValFAjout($val);

        // Si c'est un pv généré qui est ajouté
        if ($this->valF["genere"] == 't') {
            //
            $this->update_number();
        }
    }

    /**
     * Surcharge de la méthode setValF générée.
     *
     * Objet de la surcharge : 
     * - Le champ 'om_fichier_finalise' n'existe pas en base de données.
     *   Il a été ajouté simplement pour afficher dans la fiche de 
     *   visualisation du procès verbal un lien vers le fichier finalisé
     *   qui est en réalité stocké dans la table courrier (document généré).
     *   On supprime donc l'entrée correspondante dans le tableau de valeurs
     *   de mise à jour de base de données pour ne pas obtenir d'erreurs.
     *
     * @param array $val Tableau de valeurs à convertir pour usage BDD.
     *
     * @return void
     */
    function setvalF($val = array()) {
        // Cette surcharge a besoin de l'intégralité de la méthode parent.
        parent::setValF($val);
        // Supprime l'entrée 'om_fichier_finalise' qui n'existe pas en base
        // de données.
        unset($this->valF['om_fichier_finalise']);
    }

    /**
     * METHOD - update_number.
     *
     * Met à jour le numéro de PV temporaire par le définitif.
     *
     * @param  [integer]  $id  Identifiant du PV généré
     * @return [string]        Numéro définitif
     */
    function update_number() {

        // on construit le numéro définitif
        $service = "";
        // S'il y a un dossier d'instruction lié (ce qui doit être le cas)
        // on récupère son service
        if ($this->valF["dossier_instruction"] != "") {
            // On l'instancie pour récupérer son service
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($this->valF["dossier_instruction"]);
            $service = $di->getVal("service");
        }
        // Découpe la date
        $date = date_parse($this->valF["date_redaction"]);
        // Récupère le prochain id
        $next_id = $this->get_next_number($date['year'], $service);
        //
        if (!is_numeric($next_id)) {
            //
            $this->correct = false;
            return false;
        }
        //
        $numero = $date['year']."/".str_pad($next_id, 5, "0", STR_PAD_LEFT);
        $this->valF['numero'] = $numero;
        // Affiche le numéro du pv
        $this->addToMessage(_("Nouveau numero du PV").' : <span id="new_pv_number">'.$numero.'</span><br/>');
    }

    /**
     * Récupère le prochain identifiant de la séquence permettant de numéroter
     * le pv.
     *
     * @param string  $year    Année
     * @param integer $service Identifiant du service
     *
     * @return integer
     */
    function get_next_number($year, $service) {

        // Initialise la variable à 0
        $next_id = null;

        /**
         * On compose les identifiants qui composent la séquence.
         */
        // Clé unique.
        // Exemple : 2016_1
        $unique_key = sprintf('%s_%s', $year, $service);
        // Nom de la table représentant la séquence pour appel via la méthode
        // database::nextId() qui prend un nom de séquence sans son suffixe
        // '_seq'.
        // Exemple : openaria.proces_verbal_genere_2016_1
        $table_name = sprintf('%sproces_verbal_genere_%s', DB_PREFIXE, $unique_key);
        // Nom de la séquence avec son suffixe 'seq'.
        // Exemple : openaria.proces_verbal_genere_2016_1_seq
        $sequence_name = sprintf('%s_seq', $table_name);

        /**
         * On interroge la base de données pour vérifier si la séquence existe
         * ou non. Si il y a un retour à l'exécution de la requête alors la
         * séquence existe et si il n'y en a pas alors la séquence n'existe
         * pas.
         *
         * Cette requête particulière (car sur la table pg_class) nécessite
         * d'être exécutée sur le schéma public pour fonctionner correctement.
         * En effet, par défaut postgresql positionne search_path avec la
         * valeur '"$user", public' ce qui peut causer des mauvais effets de
         * bord si l'utilisateur et le schéma sont identique.
         * On force donc le schéma public sur le search_path pour être sûr que
         * la requête suivante s'exécute correctement.
         */
        $res_search_path = $this->f->db->query("set search_path=public;");
        $this->f->isDatabaseError($res_search_path);
        $query_sequence_exists = sprintf(
            'SELECT 
                * 
            FROM 
                pg_class 
            WHERE
                relkind = \'S\' 
                AND oid::regclass::text = \'%s\'
            ;',
            $sequence_name
        );
        $res_sequence_exists = $this->f->db->getone($query_sequence_exists);
        $this->addToLog(
            __METHOD__.'(): db->getone("'.$query_sequence_exists.'");',
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res_sequence_exists);

        /**
         * Si la séquence n'existe pas, alors on la cré. Puis si des DA
         * existent déjà avec cette clé unique alors on initialise cette
         * séquence avec le numéro du dernier DA correcspondant.
         */
        if ($res_sequence_exists === null) {

            // Création de la sequence si elle n'existe pas
            $res = $this->f->db->createSequence($table_name);
            $this->f->addToLog(
                __METHOD__.'(): db->createSequence("'.$table_name.'");',
                VERBOSE_MODE
            );
            $this->f->isDatabaseError($res);
        }

        /**
         * On récupère le prochain numéro de la séquence fraichement créée ou
         * créée de longue date.
         */
        $next_id = $this->f->db->nextId($table_name, false);
        $this->addToLog(
            __METHOD__.'(): db->nextId("'.$table_name.'", false);',
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($next_id);

       /**
         * On retourne le prochain identifiant.
         */
        return $next_id;
    }

    /**
     *
     */
    function get_full_number() {
        //
        if ($this->getVal("numero") !== "" && $this->getVal("numero") !== null) {
            $inst_di = $this->get_inst_dossier_instruction();
            $inst_service = $inst_di->get_inst_common("service");
            return strtoupper($inst_service->getVal("code"))."-".$this->getVal("numero");
        }
        //
        return "";
    }

    /**
     * TREATMENT - ajouter_pv.
     * 
     * Action submit lors de l'ajout d'un PV provenant de la SCDA
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function ajouter_pv($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // on ajoute le PV
        if ($this->ajouter_pv_bdd($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // trigger ajouter après spécifique à cette action
        if ($this->trigger_ajouter_pv_apres() == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - regenerer_pv.
     * 
     * Action submit lors de la regénération du dernier PV
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function regenerer_pv($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // On modifie le PV
        if ($this->regenerer_pv_bdd($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Trigger regénérer après spécifique à cette action
        if ($this->trigger_regenerer_pv_apres() == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    function trigger_ajouter_pv_apres() {
        // Mise à jour du couple avis/complément de la demande de passage en réunion
        if ($this->update_proposition_avis($this->valF) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Mise à jour des données techniques SI
        if ($this->update_donnees_techniques($this->valF) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return true;
    }

    function trigger_regenerer_pv_apres() {
        // Mise à jour du couple avis/complément de la demande de passage en réunion
        if ($this->update_proposition_avis($this->valF) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Mise à jour de l'état et du marqueur PV de l'analyse
        if ($this->acter_analyse($this->valF[$this->clePrimaire], $this->valF) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Génèration du PDF
        if ($this->generate_pv_pdf($this->valF[$this->clePrimaire]) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        // Mise à jour des données techniques SI
        if ($this->update_donnees_techniques($this->valF) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return true;
    }

    function ajouter_pv_bdd($val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Mutateur de valF
        $this->setValF($val);
        // Mutateur de valF specifique a l'ajout
        $this->setValFAjout($val);
        // Mode ajout pour la vérif
        $this->setParameter("maj", 0);
        // Verification de la validite des donnees
        $this->verifier($val, $this->f->db, null);
        // Verification specifique au MODE 'insert' de la validite des donnees
        $this->verifierAjout($val, $this->f->db);
        // Retour au mode spécifique après la vérif
        $this->setParameter("maj", 6);
        // Verification du verrou
        $this->testverrou();
        // Si les verifications precedentes sont correctes, on procede a
        // l'ajout, sinon on ne fait rien et on affiche un message d'echec
        if ($this->correct) {
            // Définition de la clé primaire
            $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
            //Traitement des fichiers uploadé
            $retTraitementFichier = $this->traitementFichierUploadAjoutModification();
            if($retTraitementFichier !== true) {
                $this->correct = false;
                $this->addToErrors("", $retTraitementFichier, $retTraitementFichier);
                return false;
            }
            // Execution de la requete d'insertion des donnees de l'attribut
            // valF de l'objet dans l'attribut table de l'objet
            $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $this->valF, DB_AUTOQUERY_INSERT);
            // Logger
            $this->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($this->valF, true).", DB_AUTOQUERY_INSERT);", VERBOSE_MODE);
            // Si une erreur survient
            if (database::isError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                $this->correct = false;
                return false;
            } else {
                //
                $main_res_affected_rows = $this->f->db->affectedRows();
                // Log
                $this->addToLog(__METHOD__."(): "._("Requete executee"), VERBOSE_MODE);
                // Mise en place du verrou pour ne pas valider plusieurs fois
                // le meme formulaire
                $this->verrouille();
                $message = _("Enregistrement")."&nbsp;".$this->valF[$this->clePrimaire]."&nbsp;";
                $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
                $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
                $message .= _("enregistrement(s) ajoute(s)")."&nbsp;]";
                $this->addToLog(__METHOD__."(): ".$message, VERBOSE_MODE);
                // Message de validation
                $this->addToMessage(_("Vos modifications ont bien ete enregistrees.")."<br/>");
            }
        } else {
            // Message d'echec (saut d'une ligne supplementaire avant le
            // message pour qu'il soit mis en evidence)
            $this->addToMessage("<br/>"._("SAISIE NON ENREGISTREE")."<br/>");
            return false;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        return true;
    }

    function regenerer_pv_bdd($val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Recuperation de la valeur de la cle primaire de l'objet
        if(isset($val[$this->clePrimaire])) {// ***
            $id = $val[$this->clePrimaire];
        } elseif(isset($this->valF[$this->clePrimaire])) {// ***
            $id = $this->valF[$this->clePrimaire];
        } else {
            $id=$this->id;
        }
        // Appel au mutateur de l'attribut valF de l'objet
        $this->setValF($val);
        // Mode modifier pour la vérif
        $this->setParameter("maj", 1);
        // Verification de la validite des donnees
        $this->verifier($val, $this->f->db, null);
        // Retour au mode spécifique après la vérif
        $this->setParameter("maj", 7);
        // Verification du verrou
        $this->testverrou();
        // Si les verifications precedentes sont correctes, on procede a
        // la modification, sinon on ne fait rien et on affiche un message
        // d'echec
        if ($this->correct) {
            //Traitement des fichiers uploadé
            $retTraitementFichier = $this->traitementFichierUploadAjoutModification();
            if($retTraitementFichier !== true) {
                $this->correct = false;
                $this->addToErrors("", $retTraitementFichier, $retTraitementFichier);
                return false;
            }
            // Execution de la requête de modification des donnees de l'attribut
            // valF de l'objet dans l'attribut table de l'objet
            $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $this->valF, DB_AUTOQUERY_UPDATE, $this->getCle($id));
            // Logger
            $this->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($this->valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")", VERBOSE_MODE);
            // Si une erreur survient
            if (database::isError($res, true)) {
                // Appel de la methode de recuperation des erreurs
                $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
                $this->correct = false;
                return false;
            } else {
                //
                $main_res_affected_rows = $this->f->db->affectedRows();
                // Mise en place du verrou pour ne pas valider plusieurs fois
                // le meme formulaire
                $this->verrouille();
                $retTraitementFichier = $this->traitementFichierUploadSuppression();
                if($retTraitementFichier !== true) {
                    $this->correct = false;
                    $this->addToErrors("", $retTraitementFichier, $retTraitementFichier);
                    return false;
                }
                // Log
                $this->addToLog(__METHOD__."(): "._("Requete executee"), VERBOSE_MODE);
                
                // Log
                $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
                $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
                $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
                $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
                $this->addToLog(__METHOD__."(): ".$message, VERBOSE_MODE);
                // Message de validation
                if ($main_res_affected_rows == 0) {
                    $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
                } else {
                    $this->addToMessage(_("Vos modifications ont bien ete enregistrees.")."<br/>");
                }
            }
        } else {
            // Message d'echec (saut d'une ligne supplementaire avant le
            // message pour qu'il soit mis en evidence)
            $this->addToMessage("<br/>"._("SAISIE NON ENREGISTREE")."<br/>");
            return false;
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);

        return true;
    }

    /**
     * Permet d’effectuer des actions après l'ajout des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'objet
     * @param object  &$db   Instance de la bdd
     * @param array   $val   Liste des valeurs
     * @param mixed   $DEBUG Debug
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        //
        parent::triggerajouterapres($id, $db, $val, $DEBUG);

        // Mise à jour du couple avis/complément de la demande de passage en réunion
        if ($this->update_proposition_avis($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // Mise à jour de l'état et du marqueur PV de l'analyse
        if ($this->acter_analyse($id, $val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // Génèration du PDF
        if ($this->generate_pv_pdf($id) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // Mise à jour des données techniques SI
        if ($this->update_donnees_techniques($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        //
        return true;
    }

    /**
     * Permet d’effectuer des actions après la modification des données dans la
     * base.
     *
     * @param integer $id    Identifiant de l'objet
     * @param object  &$db   Instance de la bdd
     * @param array   $val   Liste des valeurs
     * @param mixed   $DEBUG Debug
     *
     * @return boolean
     */
    function triggerModifierApres($id, &$db = null, $val = array(), $DEBUG = null) {
        parent::triggerModifierApres($id, $db, $val, $DEBUG);

        // Mise à jour du couple avis/complément
        if ($this->update_proposition_avis($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }

        // Mise à jour des données techniques SI
        if ($this->update_donnees_techniques($val) == false) {
            $this->correct = false;
            $this->cleanMessage();
            $this->addToMessage("Une erreur s'est produite. Contactez votre administrateur.");
            return $this->end_treatment(__METHOD__, false);
        }
        //
        return true;
    }

    function update_donnees_techniques($val) {
        // Si un fichier signé a été uploadé
        if ($val["om_fichier_signe"] != "") {
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($val["dossier_instruction"]);
            require_once "../obj/analyses.class.php";
            $analyse = new analyses($di->get_id_analyse());
            $code_service = $this->f->get_service_code($analyse->getVal("service"));
            // Si service SI
            if ($code_service == "si") {
                // Instanciation du dossier de coordination
                require_once "../obj/dossier_coordination.class.php";
                $dc = new dossier_coordination($di->getVal("dossier_coordination"));
                // Récupération de l'ID de l'établissement
                $id_etablissement = $dc->getVal("etablissement");
                // Initialisation de l'établissement s'il existe
                if ($id_etablissement != "") {
                    require_once '../obj/etablissement_tous.class.php';
                    $etablissement = new etablissement_tous($id_etablissement);
                    // Récupération des valeurs existantes
                    $values = array();
                    foreach($etablissement->champs as $key => $champ) {
                        $values[$champ] = $etablissement->val[$key];
                        // Si champ de type date
                        if ($etablissement->type[$key] == 'date') {
                            // Conversion format EN en FR
                            $values[$champ] = $etablissement->dateDBToForm($etablissement->val[$key]);
                        }
                    }
                    // Écrasement des données techniques SI
                    $values = $analyse->set_donnees_techniques_si($values, $analyse);
                    // Écrasement de la classification
                    $values["etablissement_type"] = $dc->getVal("etablissement_type");
                    $values["etablissement_categorie"] = $dc->getVal("etablissement_categorie");
                    // Écrasement de la description
                    $values["si_descriptif_om_html"]
                        = $analyse->getVal("descriptif_etablissement_om_html");
                    // Mise à jour établissement
                    if ($etablissement->modifier($values) === false) {
                        $this->correct = false;
                        $this->addToMessage(_("Erreur lors de la mise a jour de la fiche etablissement :"));
                        $this->addToMessage(str_replace(_("SAISIE NON ENREGISTREE"), "",$etablissement->msg));
                        // Suppression des interlignes superflues
                        $this->msg = str_replace("<br />", "<br/>",$this->msg);
                        $this->msg = preg_replace('#<br/>(\s*<br/>)+#', '', $this->msg);
                        return false;
                    }
                    // Mise à jour liaison types secondaires
                    if ($this->update_types_secondaires($id_etablissement, $dc->getVal("etablissement_type_secondaire")) === false) {
                        $this->correct = false;
                        $this->addToMessage(
                            _("Erreur lors de la mise a jour des types secondaires de l'etablissement.")
                        );
                        return false;
                    }
                    
                }
            }
        }
        return true;
    }

    /**
     * METHOD - update_types_secondaires.
     * 
     * Remplace les types secondaires d'établissement par ceux définis dans le DC
     * 
     * @param  [integer]  $id    établissement
     * @param  [string]   $types types secondaires
     * @return [boolean]         vrai si table mise à jour avec succès
     */
    function update_types_secondaires($id, $types) {
        // Définition de la table de liaison mise à jour
        $table = "lien_etablissement_e_type";

        // Suppression des liens existants        
        // Suppression de toutes les prescriptions de l'analyse
        $sql_delete = "DELETE FROM ".DB_PREFIXE.$table."
            WHERE etablissement = ".$id;
        // Si la suppression échoue
        $res_delete = $this->f->db->query($sql_delete);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_delete."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res_delete, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res_delete->getDebugInfo(), $res_delete->getMessage(), '');
            return false;
        }

        // Ajout des nouveaux liens s'il y en a
        if ($types != '') {
            // Création d'un tableau de liens
            $types = explode(';', $types);
            // Ajout de chaque lien
            require_once "../obj/".$table.".class.php";
            foreach ($types as $type) {
                $lien = new $table($type);
                $values = array();
                $values[$table] = "";
                $values["etablissement"] = intval($id);
                $values["etablissement_type"] = intval($type);
                if ($lien->ajouter($values) == false) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * METHOD - update_proposition_avis
     * 
     * Remplace le couple avis/complément de la demande de passage en réunion
     * du DI par celui de l'analyse
     * 
     * @param  [array]   $val tableau associatif des valeurs du PV
     * @return [boolean]      vrai si table mise à jour avec succès
     */
    function update_proposition_avis($val) {
        // On instancie la demande
        require_once "../obj/dossier_instruction_reunion.class.php";
        $demande = new dossier_instruction_reunion($val["dossier_instruction_reunion"]);
        // On instancie le DI
        require_once "../obj/dossier_instruction.class.php";
        $di = new dossier_instruction($demande->getVal("dossier_instruction"));
        // On instancie l'analyse
        require_once "../obj/analyses.class.php";
        $analyse = new analyses($di->get_id_analyse());
        // On ré-affecte à la demande ses valeurs existantes avant de la modifier
        $demande->setValFFromVal();
        // Reformatage des dates EN en FR
        foreach ($demande->champs as $key => $field) {
            if ($demande->type[$key] == 'date') {
                $demande->valF[$field] = $demande->dateDBToForm($demande->valF[$field]);
            }
        }
        // On écrase le couple proposition avis/complément
        $demande->valF["proposition_avis"] = $analyse->getVal("reunion_avis");
        $demande->valF["proposition_avis_complement"] = $analyse->getVal("avis_complement");
        // On modifie la demande
        $res = $demande->modifier($demande->valF);
        if ($res == false) {
            $this->correct = false;
            // clean les messages valides
            $this->cleanMessage();
            $this->addToMessage(
                _("Erreur lors de la mise a jour de la proposition d'avis de la demande de passage en reunion du dossier d'instruction.")." "._("Veuillez contacter votre administrateur.")
            );
            return false;
        }
        return true;
    }

    /**
     * METHOD - acter_analyse
     * 
     * Acte l'analyse.
     *
     * @param  [integer]  $id   identifiant du PV généré
     * @param  [array]    $val  tableau associatif des valeurs du PV
     * @return [boolean]        vrai si analyse mise à jour avec succès
     */
    function acter_analyse($id, $val) {
        // On instancie le DI
        require_once "../obj/dossier_instruction.class.php";
        $di = new dossier_instruction($val["dossier_instruction"]);
        // On instancie l'analyse
        require_once "../obj/analyses.class.php";
        $analyse = new analyses($di->get_id_analyse());
        // On acte l'analyse
        if ($analyse->acter($id) == false) {
            $this->correct = false;
            $this->addToMessage(
                _("Erreur lors de la validation de l'analyse.")." "._("Veuillez contacter votre administrateur.")
            );
            return false;
        }
        return true;
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion des fichiers de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        if (file_exists("../sql/".OM_DB_PHPTYPE."/analyses.form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/analyses.form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/analyses.form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/analyses.form.inc";
        }

        // Filtre des demandes de passage en réunion par le dossier d'instruction
        $dossier_instruction = "";
        $champ = "dossier_instruction";
        if (isset($_POST[$champ])) {
            $dossier_instruction = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $dossier_instruction = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $dossier_instruction = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $dossier_instruction = $this->getVal($champ);
        }
        if ($dossier_instruction == "") {
            $dossier_instruction = -1;
        }
        $sql_demande = str_replace('<idx_di>', $dossier_instruction, $sql_demande);

        // Filtre des modèles d'édition par la catégorie du courrier-type
        require_once "../obj/dossier_instruction.class.php";
        $di = new dossier_instruction($dossier_instruction);
        // récupération du code du service
        $code_service = $this->f->get_service_code($di->getVal("service"));
        $sql_modele_edition_pv = str_replace('<where_modele>', '', $sql_modele_edition);
        $sql_modele_edition_pv = str_replace('<courrier_type_code>', 'ANL-'.$code_service.'-PV', $sql_modele_edition_pv);

        // modes ajout (génerer) et modification
        if ($maj == 0 || $maj == 1) {
            $this->init_select($form, $dnu1, $maj, $dnu2, "dossier_instruction_reunion", $sql_demande, $sql_demande_by_id, false, true);
            $this->init_select($form, $dnu1, $maj, $dnu2, "modele_edition", $sql_modele_edition_pv, $sql_modele_edition_by_id, false, true);
            $this->init_select($form, $dnu1, $maj, $dnu2, "signataire", $sql_signataire, $sql_signataire_by_id, false, true);
        }

        // mode consulter
        if ($maj == 3) {
            // si PV généré
            if ($this->getVal("genere") == 't') {
                // Récupération du courrier généré
                require_once "../obj/courrier.class.php";
                $id_courrier = $this->getVal('courrier_genere');
                $courrier = new courrier($id_courrier);
                $params = array(
                    'obj' => 'courrier',
                    'idx' => $id_courrier,
                    'val' => $courrier->getVal('om_fichier_finalise_courrier'),
                    'champ' => 'om_fichier_finalise_courrier'
                );
                $form->setSelect("om_fichier_finalise", $params);
                // Récupération du courrier signé
                require_once "../obj/courrier.class.php";
                $id_courrier = $this->getVal('courrier_genere');
                $courrier = new courrier($id_courrier);
                $params = array(
                    'obj' => 'courrier',
                    'idx' => $id_courrier,
                    'val' => $courrier->getVal('om_fichier_signe_courrier'),
                    'champ' => 'om_fichier_signe_courrier'
                );
                $form->setSelect("om_fichier_signe", $params);
            }
        }

        // action ajouter
        if ($maj == 6) {
            $this->init_select($form, $dnu1, 0, $dnu2, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false, true);
            $this->init_select($form, $dnu1, 0, $dnu2, "dossier_instruction_reunion", $sql_demande, $sql_demande_by_id, false, true);
            $this->init_select($form, $dnu1, 0, $dnu2, "modele_edition", $sql_modele_edition_pv, $sql_modele_edition_by_id, false, true);
            $this->init_select($form, $dnu1, 0, $dnu2, "signataire", $sql_signataire, $sql_signataire_by_id, false, true);
        }
        // action regénérer
        if ($maj == 7) {
            $this->init_select($form, $dnu1, 1, $dnu2, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false, true);
            $this->init_select($form, $dnu1, 1, $dnu2, "dossier_instruction_reunion", $sql_demande, $sql_demande_by_id, false, true);
            $this->init_select($form, $dnu1, 1, $dnu2, "modele_edition", $sql_modele_edition_pv, $sql_modele_edition_by_id, false, true);
            $this->init_select($form, $dnu1, 1, $dnu2, "signataire", $sql_signataire, $sql_signataire_by_id, false, true);
        }
        // modes suppression et consultation
        if ($maj == 2 || $maj == 3) {
            $this->init_select($form, $dnu1, $maj, $dnu2, "dossier_instruction_reunion", $sql_demande, $sql_demande_by_id, false, true);
        }
    }
    /**
     * Cette méthode permet de composer le lien retour et de l'afficher.
     *
     * Utilisé par la VIEW sousformulaire.
     *
     * @return void
     */
    function retoursousformulaire($dnu1 = null, $dnu2 = null, $dnu3 = null, $dnu4 = null, $dnu5 = null, $dnu6 = null, $dnu7 = null, $dnu8 = null, $dnu9 = null, $dnu10 = null) {
        // En sous-formulaire d'un DI
        // et hors vue supprimer et vues ajout/remplacement PV signé accessible depuis un PV (re)généré
        // -> on revient sur la vue spécifique de l'onglet
        if ($this->getParameter('retourformulaire') == 'dossier_instruction'
            && $this->getParameter('maj') != 8 && $this->getParameter('maj') != 9 && $this->getParameter('maj') != 2) {
            // Objet à charger
            $obj = "proces_verbal";
            // Construction de l'url de sousformulaire à appeler
            $url = "../scr/sousform.php?obj=".$obj;
            $url .= "&idx=0&action=5";
            $url .= "&retourformulaire=dossier_instruction";
            $url .= "&idxformulaire=".$this->getParameter('idxformulaire');
            //
            printf('
                <a href="#" class="retour" onclick="ajaxIt(\'%s\', \'%s\');">
                %s
                </a>',
                $obj,
                $url,
                _("Retour")
            );
        }
        // Si sous-formulaire DI et si vue supprimer ou ajout/remplacement PV signé
        // -> on revient sur la consultation
        elseif ($this->getParameter('retourformulaire') == 'dossier_instruction'
            && ($this->getParameter('maj') == 2 || $this->getParameter('maj') == 8 || $this->getParameter('maj') == 9)) {
            // Objet à charger
            $obj = "proces_verbal";
            // Construction de l'url de sousformulaire à appeler
            $url = "../scr/sousform.php?obj=".$obj;
            $url .= "&idx=".$this->getParameter('idx');
            $url .= "&action=3";
            $url .= "&retourformulaire=dossier_instruction";
            $url .= "&idxformulaire=".$this->getParameter('idxformulaire');
            //
            printf('
                <a href="#" class="retour" onclick="ajaxIt(\'%s\', \'%s\');">
                %s
                </a>',
                $obj,
                $url,
                _("Retour")
            );
        }
        // si sous-formulaire d'un autre objet que DI
        // -> comportement standard
        else {
            parent::retoursousformulaire();
        }
    }

    function boutonsousformulaire($datasubmit, $maj, $val=null) {
        if (!$this->correct
            && $this->checkActionAvailability() == true) {
            //
            switch($maj) {
                case 0 :
                    $bouton = _("Generer le")." "._("proces_verbal");
                    break;
                case 1 :
                    $bouton = _("Modifier le")." "._("proces_verbal");
                    break;
                case 2 :
                    $bouton = _("Supprimer le")." "._("proces_verbal");
                    break;
                default :
                    // Actions specifiques
                    if ($this->get_action_param($maj, "button") != null) {
                        //
                        $bouton = $this->get_action_param($maj, "button");
                    } else {
                        //
                        $bouton = _("Valider");
                    }
                    break;
            }
            //
            $params = array(
                "value" => $bouton,
                "name" => "submit",
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib("om_fichier_finalise", _("PV genere"));
        $form->setLib("om_fichier_signe", _("PV signe"));
        $form->setLib("dossier_instruction_reunion", _("demande de passage en reunion"));
        // mode ajouter (= générer)
        if ($maj == 0) {
            $form->setLib("modele_edition", _("modele_edition")." "._("a utiliser lors de la generation"));
        }
        // mode consulter
        if ($maj == 3) {
            $form->setLib("modele_edition", _("modele_edition")." "._("utilise lors de la generation"));
        }
        // action ajouter
        if ($maj == 6) {
            // seulement les champs obligatoires affichés
            $form->required_field = array(
                "date_redaction",
                "dossier_instruction_reunion",
                "om_fichier_signe",
            );
            // tous les champs obligatoires
            $this->required_field = array(
                "date_redaction",
                "dossier_instruction_reunion",
                "om_fichier_signe",
                "dossier_instruction",
                "proces_verbal",
                "modele_edition",
            );
        }
        // action regénérer
        if ($maj == 7) {
            $form->setLib("modele_edition", _("modele_edition")." "._("a utiliser lors de la regeneration"));
            $form->setLib("numero", _("numero")." "._("du PV a regenerer"));
            // seulement les champs obligatoires affichés
            $form->required_field = array(
                "date_redaction",
                "dossier_instruction_reunion",
                "modele_edition",
            );
            // tous les champs obligatoires
            $this->required_field = array(
                "date_redaction",
                "dossier_instruction_reunion",
                "dossier_instruction",
                "proces_verbal",
                "modele_edition",
            );
        }
    }

    function setRequired(&$form, $maj) {
        parent::setRequired($form, $maj);

        // action ajouter
        if ($maj == 6) {
            $form->setRequired("date_redaction");
            $form->setRequired("dossier_instruction_reunion");
            $form->setRequired("om_fichier_signe");
            $form->setRequired("dossier_instruction");
            $form->setRequired("proces_verbal");
            $form->setRequired("modele_edition");
        }

        // action regénérer
        if ($maj == 7) {
            $form->setRequired("date_redaction");
            $form->setRequired("dossier_instruction_reunion");
            $form->setRequired("dossier_instruction");
            $form->setRequired("proces_verbal");
            $form->setRequired("modele_edition");
        }
    }

    /**
     * Méthode de mise en page.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {

        $col_first_fieldset = "col_10";
        //
        if ($maj == 0 || $maj == 1 || $maj == 2) {
            $col_first_fieldset = "col_12";
        }

        // Fieldset "Informations générales"
        $form->setBloc('proces_verbal', 'D', "", $col_first_fieldset);
            $form->setBloc('proces_verbal', 'D', "", "col_12");
                $form->setFieldset('proces_verbal', 'D', _('Informations generales'), "");
                $form->setFieldset('genere', 'F', '');
            $form->setBloc('genere', 'F');
        $form->setBloc('genere', 'F');

        // Fieldset "Document"
        $form->setBloc('signataire', 'D', "", "col_12");
            $form->setBloc('signataire', 'D', "", "col_12");
                $form->setFieldset('signataire', 'D', _('Document'), "");
                $form->setFieldset('om_fichier_signe', 'F', '');
            $form->setBloc('om_fichier_signe', 'F');
        $form->setBloc('om_fichier_signe', 'F');
    }

    /**
     * METHOD - generate_pv_pdf.
     * 
     * Génère le PV au format PDF et stocke le fichier dans un nouvel
     * enregistrement de la table courrier. Il se peut que ce soit une
     * regénération, auquel cas on écrase le courrier existant.
     * 
     * @param  [array]    $id  Identifiant du PV
     * @return [boolean]       Vrai si traitement réalisé avec succès
     */
    function generate_pv_pdf($id) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);

        // instanciation du PV
        $pv = new proces_verbal($id);
        // on récupère les valeurs
        foreach ($pv->champs as $value) {
            $val = $pv->getVal($value);
            (empty($val)) ? $val_pv[$value] = null : $val_pv[$value] = $val;
        }
        // instanciation du courrier existant ou d'un nouveau le cas échéant
        require_once "../obj/courrier.class.php";
        if (empty($val_pv['courrier_genere'])) {
            $courrier = new courrier("]");
            foreach ($courrier->champs as $value) {
                $val_courrier[$value] = null;
            }
            // on renseigne les champs du courrier de sorte qu'il génère le PDF
            $val_courrier['dossier_instruction'] = $val_pv['dossier_instruction'];
            if (!empty($val_pv['signataire'])) {
                $val_courrier['signataire'] = $val_pv['signataire'];
            }
            $val_courrier['proces_verbal'] = $id;
            $val_courrier['modele_edition'] = $val_pv['modele_edition'];
            $val_courrier['courrier_type'] = $this->f->get_courrier_type_by_modele_edition($val_pv['modele_edition']);
            $val_courrier['contacts_lies'] = array();
            // on crée le courrier
            if ($courrier->ajouter($val_courrier, $this->f->db) === false) {
                $this->addToLog(__METHOD__.'() - $courrier->ajouter() : false', EXTRA_VERBOSE_MODE);
                return false;
            }
            // on le lie au procès-verbal
            $courrier_id = $courrier->valF[$courrier->clePrimaire];
            $val_pv["courrier_genere"]= $courrier_id;
            $pv->correct = true;
            $pv->setParameter("maj", 1);
            $ret = $pv->modifier($val_pv);
            if ($pv->modifier($val_pv) === false) {
                $this->addToLog(__METHOD__.'() - $pv->modifier() : false', EXTRA_VERBOSE_MODE);
                return false;
            }
            // on le finalise
            $courrier_finalise = new courrier($courrier_id);
            $ret = $courrier_finalise->finalize();
            $this->addToLog(__METHOD__.'() - $courrier->finalize() : '.$ret, EXTRA_VERBOSE_MODE);
            return $ret;
        } else {
            $courrier = new courrier($val_pv['courrier_genere']);
            foreach ($courrier->champs as $value) {
                $val = $courrier->getVal($value);
                (empty($val)) ? $val_courrier[$value] = null : $val_courrier[$value] = $val;
            }
            // Le signataire et le modèle ont pu changer
            $val_courrier['signataire'] = $val_pv['signataire'];
            $val_courrier['modele_edition'] = $val_pv['modele_edition'];
            $val_courrier['courrier_type'] = $this->f->get_courrier_type_by_modele_edition($val_pv['modele_edition']);
            if ($courrier->modifier($val_courrier) === false) {
                $this->addToLog(__METHOD__.'() - $courrier->modifier() : false', EXTRA_VERBOSE_MODE);
                return false;
            }
            // Regénération PDF
            unset($courrier);
            $courrier = new courrier($val_pv['courrier_genere']);
            $ret = $courrier->finalize();
            $this->addToLog(__METHOD__.'() - $courrier->finalize() : '.$ret, EXTRA_VERBOSE_MODE);
            return $ret;
        }
    }

    /**
     * Retourne l'identifiant de la lettre-type pour un moèle d'édition donné
     * 
     * @param  [integer]  $id  ID du modèle d'édition
     * @return [string]        ID de la lettre_type
     */
    function get_om_lettretype_id_by_modele_edition($id) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        $om_lettretype_id = "";
        //
        if (!empty($id)) {
            // Instance de la classe modele_edition
            require_once '../obj/modele_edition.class.php';
            $modele_edition = new modele_edition($id);
            //
            $om_lettretype_id = $modele_edition->get_om_lettretype_id($modele_edition->getVal('om_lettretype'));
        }
        // Logger
        $this->addToLog(__METHOD__."() - return \$om_lettretype_id = ".$om_lettretype_id, EXTRA_VERBOSE_MODE);
        //
        return $om_lettretype_id;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $pv_values = array(
            "proces_verbal.numero" => $this->getVal("numero"),
            "proces_verbal.date_redaction" => $this->dateDBToForm($this->getVal("date_redaction")),
        );
        // Valeurs de la demande de passage en réunion
        require_once "../obj/dossier_instruction_reunion.class.php";
        $reunion = new dossier_instruction_reunion($this->getVal("dossier_instruction_reunion"));
        // On récupère toutes ses valeurs
        $reunion_values = $reunion->get_values_merge_fields();
        $pv_values = array_merge($pv_values, $reunion_values);
        //
        return $pv_values;
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        // Libellés du PV
        $pv_labels = array(
            _("proces_verbal") => array(
                "proces_verbal.numero" => _("numero"),
                "proces_verbal.date_redaction" => _("date_redaction"),
            ),
        );
        // Libellés de la demande de passage en réunion liée au PV
        require_once "../obj/dossier_instruction_reunion.class.php";
        $reunion = new dossier_instruction_reunion('0');
        // Retour de tous les libellés
        return array_merge($pv_labels, $reunion->get_labels_merge_fields());
    }

    /**
     * CONDITION - is_generated
     * PV généré ?
     *
     * @return boolean true si PV généré false sinon
     */
    function is_generated() {
        if ($this->getVal("genere") == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_added
     * PV ajouté ?
     *
     * @return boolean true si PV ajouté false sinon
     */
    function is_added() {
        return !$this->is_generated();
    }


    /**
     * CONDITION - has_signed_file
     * PV avec fichier signé ?
     *
     * @return boolean true si avec
     */
    function has_signed_file() {
        if ($this->getVal("courrier_genere") != "") {
            require_once '../obj/courrier.class.php';
            $courrier = new courrier($this->getVal("courrier_genere"));
            if ($courrier->getVal("om_fichier_signe_courrier") != "") {
                return true;
            }
        }
        return false;
    }

    /**
     * CONDITION - hasnt_signed_file
     * PV sans fichier signé ?
     *
     * @return boolean true si sans
     */
    function hasnt_signed_file() {
        return !$this->has_signed_file();
    }

    /**
     * VIEW - view_pv_signed_file.
     *
     * Vue d'ajout de PV signé dans la table courrier pour les PV générés
     * 
     * @return void
     */
    function view_pv_signed_file() {
        // vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // mode
        $case_msg = _("ajoute");
        if ($this->getParameter("maj") == 9) {
            $case_msg = _("remplace");
        }
        // initialisation de l'état de validation
        $validation = "";
        // traitement POST et définition de l'état de validation
        if ($this->getParameter("postvar") != null) {
            require_once '../obj/courrier.class.php';
            $courrier = new courrier($this->getVal("courrier_genere"));
            // si fichier présent et différent
            if ($this->getParameter("postvar")["om_fichier_signe_courrier"] != ""
                && $this->getParameter("postvar")["om_fichier_signe_courrier_upload"] != ""
                && $this->getParameter("postvar")["om_fichier_signe_courrier"]
                != $courrier->getVal("om_fichier_signe_courrier")) {
                foreach ($courrier->champs as $value) {
                    $val = $courrier->getVal($value);
                    (empty($val)) ? $values[$value] = null : $values[$value] = $val;
                }
                $values["om_fichier_signe_courrier"] = $this->getParameter("postvar")["om_fichier_signe_courrier"];
                if ($courrier->modifier($values)) {
                    $validation = "valid";
                } else {
                    $validation = "db_error";
                }
            }
            // si fichier présent et identique
            elseif ($this->getParameter("postvar")["om_fichier_signe_courrier"] != ""
                && $this->getParameter("postvar")["om_fichier_signe_courrier_upload"] != ""
                && $this->getParameter("postvar")["om_fichier_signe_courrier"]
                == $courrier->getVal("om_fichier_signe_courrier")) {
                $validation = "file_same";
            }
            // si aucun fichier
            else {
                $validation = "file_error";
            }
        }
        // affichage du bouton retour
        $this->display_return_from_add_signed_file();
        // affichage du message de validation et/ou du formulaire selon contexte
        switch ($validation) {
            // erreur lors du modifier
            case 'db_error':
                // message
                $this->f->layout->display_message("error", _("Erreur de base de donnees. Contactez votre administrateur."));
                // bouton retour
                $this->display_return_from_add_signed_file();
                break;
            // aucun fichier uploadé
            case 'file_error':
                // message
                $this->f->layout->display_message("error", _("Veuillez deposer un fichier."));
                // formulaire
                $this->display_form_add_signed_file();
                break;
            // même fichier uploadé
            case 'file_same':
                // message
                $this->f->layout->display_message("error", _("Vous n'avez pas uploade de nouveau fichier."));
                // formulaire
                $this->display_form_add_signed_file();
                break;
            // modification OK
            case 'valid':
                // message
                $this->f->layout->display_message("valid", _("Le PV signe a ete correctement")." ".$case_msg.".");
                // bouton retour
                $this->display_return_from_add_signed_file();
                break;
            // formulaire de première instance
            default:
                // formulaire
                $this->display_form_add_signed_file();
                break;
        }
    }

    /**
     * Affiche le formulaire d'ajout de PV signé.
     * 
     * @return void
     */
    function display_form_add_signed_file() {
        $case_button = _("Ajouter");
        $file = "";
        $case_title = _("Ajout");
        if ($this->getParameter("maj") == 9) {
            $case_button = _("Remplacer");
            $case_title = _("Remplacement");
            require_once '../obj/courrier.class.php';
            $courrier = new courrier($this->getVal("courrier_genere"));
            $file = $courrier->getVal("om_fichier_signe_courrier");
        }
        $case_title = $case_title." "._("du PV signe");
        // champs
        $fichier = "om_fichier_signe_courrier";
        $numero = "numero";
        $liste_champs = array(
            $numero,
            $fichier,
        );
        // création du formulaire
        $form = new om_formulaire(NULL, 0, 0, $liste_champs);
        // paramétrage champs
        $form->setVal($numero, $this->getVal("numero"));
        $form->setLib($numero, _("numero"));
        $form->setType($numero, "static");
        //
        $form->setVal($fichier, $file);
        $form->setLib($fichier, _("fichier"));
        $form->setType($fichier, "upload2");
        // lien
        $link = "../scr/sousform.php?obj=proces_verbal&action=".$this->getParameter("maj");
        $link .= "&idx=".$this->getVal($this->clePrimaire);
        $link .= "&idxformulaire=".$this->getParameter("idxformulaire");
        $link .= "&retourformulaire=".$this->getParameter("retourformulaire");
        // ouverture du formulaire
        echo "\t<form";
        echo " method=\"post\" action=\"\"";
        echo " name=\"f2\"";
        echo " onsubmit=\"affichersform('proces_verbal','".$link."',this);return false;\"";
        echo ">\n";
        // Affichage du formulaire
        $form->entete();
        $form->setBloc($numero, "D", "", "col_12");
        $form->setFieldset($numero, "D", $case_title);
        $form->setFieldset($fichier, "F");
        $form->setBloc($fichier, "F");
        $form->afficher($liste_champs, 0, false, false);
        $form->enpied();
        // Affichage du bouton
        echo "\t<div class=\"formControls\">\n";
        $this->f->layout->display_form_button(array("value" => $case_button, "name" => "validation"));
        $this->display_return_from_add_signed_file();
        echo "\t</div>\n";
        // Fermeture du formulaire
        echo "\t</form>\n";
    }

    function display_return_from_add_signed_file(){
        $this->retoursousformulaire();
    }

    /**
     * CONDITION - is_di_from_good_service.
     */
    function is_di_from_good_service() {
        // Récupération de l'ID du DI
        $id_di = $this->getParameter("idxformulaire");
        // Si valeurs définies et contexte DI
        if (!empty($id_di)) {
            // Instanciation du DI ou de sa surcharge
            require_once '../obj/dossier_instruction.class.php';
            $di = new dossier_instruction($id_di);
            // Appel et retour de la méthode de vérification du service du DI
            return $di->is_from_good_service();
        }
        return false;
    }

    /**
     * CONDITION - is_generable.
     */
    function is_generable() {
        // Récupération de l'ID du DI
        $id_di = $this->getParameter("idxformulaire");
        // Si valeurs définies et contexte DI
        if (!empty($id_di)) {
            // On instancie le dossier d'instruction
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($id_di);
            $id_analyse = $di->get_id_analyse();
            // Si la récupération de l'ID de l'analyse n'échoue pas
            if ($id_analyse != false) {
                // on instancie cette dernière
                require_once "../obj/analyses.class.php";
                $analyse = new analyses($id_analyse);
                // si analyse validée
                if ( $analyse->getVal("analyses_etat") == 'valide') {
                    return true;
                }

            }
        }
        return false;
    }

    /**
     * CONDITION - is_regenerable.
     */
    function is_regenerable() {
        // Récupération de l'ID du DI
        $id_di = $this->getParameter("idxformulaire");
        // Si valeurs définies et contexte DI
        if (!empty($id_di)) {
            // On instancie le dossier d'instruction
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($id_di);
            $id_analyse = $di->get_id_analyse();
            // Si la récupération de l'ID de l'analyse n'échoue pas
            if ($id_analyse != false) {
                // on instancie cette dernière
                require_once "../obj/analyses.class.php";
                $analyse = new analyses($id_analyse);
                // si analyse validée après modification et si elle a un PV
                // déjà généré
                if ( $analyse->getVal("analyses_etat") == 'valide'
                    && $analyse->getVal("modifiee_sans_gen") == 't'
                    && $analyse->getVal("dernier_pv") != "") {
                    return true;
                }

            }
        }
        return false;
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        // Il n'y a pas de champ 'etablissement' dans la table
        // 'proces_verbal' donc pour récupérer l'instance de l'établissement
        // lié au procès verbal courant on passe par l'instance du dossier
        // d'instruction lié au procès verbal courant.
        if ($etablissement == null) {
            $inst_di = $this->get_inst_dossier_instruction();
            return $inst_di->get_inst_etablissement();
        }
        //
        return $this->get_inst_common(
            "etablissement",
            $etablissement
        );
    }

    /**
     *
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        // Il n'y a pas de champ 'dossier_coordination' dans la table
        // 'proces_verbal' donc pour récupérer l'instance du dossier de
        // coordination lié au procès verbal courant on passe par l'instance du
        // dossier d'instruction lié au procès verbal courant.
        if ($dossier_coordination == null) {
            $inst_di = $this->get_inst_dossier_instruction();
            return $inst_di->get_inst_dossier_coordination();
        }
        //
        return $this->get_inst_common(
            "dossier_coordination",
            $dossier_coordination
        );
    }

    /**
     *
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if ($dossier_instruction == null
            && isset($this->valF["dossier_instruction"])
            && $this->valF["dossier_instruction"] != "") {
            $dossier_instruction = $this->valF["dossier_instruction"];
        }
        //
        return $this->get_inst_common(
            "dossier_instruction",
            $dossier_instruction
        );
    }

    /**
     *
     */
    function get_inst_dossier_instruction_reunion($dossier_instruction_reunion = null) {
        //
        if ($dossier_instruction_reunion == null
            && isset($this->valF["dossier_instruction_reunion"])
            && $this->valF["dossier_instruction_reunion"] != "") {
            $dossier_instruction_reunion = $this->valF["dossier_instruction_reunion"];
        }
        //
        return $this->get_inst_common(
            "dossier_instruction_reunion",
            $dossier_instruction_reunion
        );
    }

    /**
     *
     */
    function get_inst_signataire($signataire = null) {
        //
        if ($signataire == null
            && isset($this->valF["signataire"])
            && $this->valF["signataire"] != "") {
            $signataire = $this->valF["signataire"];
        }
        //
        return $this->get_inst_common(
            "signataire",
            $signataire
        );
    }

    /**
     *
     */
    function get_inst_reunion($reunion = null) {
        //
        if ($reunion === null) {
            $inst_dir = $this->get_inst_dossier_instruction_reunion();
            return $inst_dir->get_inst_reunion();
        }
        //
        return $this->get_inst_common(
            "reunion",
            $reunion
        );
    }

    /**
     *
     */
    function get_inst_proces_verbal($proces_verbal = null) {
        //
        if ($proces_verbal === null) {
            return $this;
        }
        //
        return $this->get_inst_common("proces_verbal", $proces_verbal);
    }

    // }}} END - GET INST

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        //
        "om_fichier_signe" => array(
            //
            "titre" => "get_md_titre",
            "description" => "get_md_description",
            //
            "origine" => "get_md_origine_televerse",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
            //
            "pv_erp_numero" => "get_md_pv_erp_numero",
            "pv_erp_nature_analyse" => "get_md_pv_erp_nature_analyse",
            "pv_erp_reference_urbanisme" => "get_md_pv_erp_reference_urbanisme",
            "pv_erp_avis_rendu" => "get_md_pv_erp_avis_rendu",
            //
            "code_reunion" => "get_md_code_reunion",
            "date_reunion" => "get_md_date_reunion",
            "type_reunion" => "get_md_type_reunion",
            "commission" => "get_md_commission",
            //
            "signataire" => "get_md_signataire",
            "signataire_qualite" => "get_md_signataire_qualite",
            "date_signature" => "get_md_date_signature",
        ),
    );

    //
    function get_md_titre() {
        $titre = $this->get_md_titre_common();
        $titre .= "procès verbal";
        return $titre;
    }
    function get_md_description() { return "procès verbal numérisé ajouté"; }
    //
    function get_md_date_signature() { return ""; }

    // }}} END - METADATA FILESTORAGE

}

?>
