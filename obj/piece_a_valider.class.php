<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont à
 * valider, c'est-à-dire les pièces dont le statut est "qualifié".
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/piece.class.php");

class piece_a_valider extends piece {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
