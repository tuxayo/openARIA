<?php
//$Id$ 
//gen openMairie le 12/03/2015 17:57

require_once "../gen/obj/contact_civilite.class.php";

class contact_civilite extends contact_civilite_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     *
     */
    function get_id_from_libelle($libelle) {
        $sql = sprintf(
            "SELECT contact_civilite FROM %scontact_civilite WHERE libelle='%s'",
            DB_PREFIXE,
            $this->f->db->escapeSimple($libelle)
        );
        $id = $this->f->db->getone($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($id, true) === true) {
            return null;
        }
        return $id;
    }
}

?>
