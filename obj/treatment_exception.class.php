<?php
//$Id$ 

/**
 * Classe gérant les erreurs rencontrées lors des traitements avec transaction.
 */
class treatment_exception extends Exception {

    /**
     * Construit l'exception
     *
     * @param string  $message  Le message de l'exception à lancer.
     * @param string  $log      Le message à logguer.
     */
    public function __construct($message, $log = null) {
        parent::__construct($message);
        if ($log !== null) {
            $this->log($log);
        }
    }

    /**
     * Cette fonction ajoute dans le message fourni dans le fichier de log.
     * 
     * @param string $log  Le message a logger.
     */
    private function log($log) {
        require_once "../core/om_logger.class.php";
        logger::instance()->log($log);
    }
}

?>
