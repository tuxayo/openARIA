<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à clore.
 *
 * @package openaria
 * @version SVN : $Id: dossier_coordination_a_cloturer.class.php 433 2014-10-15 15:20:40Z fmichon $
 */

require_once ("../obj/dossier_coordination.class.php");

class dossier_coordination_a_cloturer extends dossier_coordination {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
