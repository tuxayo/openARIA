<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/dossier_instruction_reunion.class.php";

class dossier_instruction_reunion extends dossier_instruction_reunion_gen {

    var $inst_reunion = null;
    var $inst_reunion_type = null;
    var $inst_dossier_instruction = null;
    var $inst_dossier_coordination = null;
    var $inst_etablissement = null;

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 001 - modifier
        // On ajoute la vérification du service de l'utilisateur
        $this->class_actions[1]["condition"] = array(
            "is_from_good_service",
            "is_reunion_not_closed",
        );

        // ACTION - 002 - supprimer
        // On ajoute la vérification du service de l'utilisateur
        $this->class_actions[2]["condition"] = array(
            "is_from_good_service", 
            "is_reunion_not_closed",
        );

        // ACTION - 00? - ???
        // 
        $this->class_actions[11] = array(
            "identifier" => "modifier_demande_de_passage",
            "view" => "formulaire",
            "permission_suffix" => "modifier_demande_de_passage",
            "method" => "modifier",
            "condition" => array(
                "is_from_good_service",
                "is_not_planned",
            ),
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("modifier"),
                "order" => 40,
                "class" => "zip-16",
            ),
        );

        // ACTION - 203 - edition-compte_rendu_specifique
        // Cette action permet d'imprimer un compte-rendu
        // spécifique sur la demande de passage.
        $this->class_actions[203] = array(
            "identifier" => "edition-compte_rendu_specifique",
            "view" => "view_edition",
            "portlet" => array(
                "type" => "action-blank",
                "libelle" => _("CR spécifique"),
                "order" => 83,
                "class" => "pdf-16",
            ),
            "permission_suffix" => "edition",
            "condition" => array(
                "is_planned",
                "is_numbered",
                "is_available_edition_compte_rendu_specifique",
            ),
        );

        // ACTION - 100 - display_synthesis
        // Affiche une synthése de l'enregistrement
        $this->class_actions[100] = array(
            "identifier" => "synthesis",
            "view" => "view_synthesis",
            "permission_suffix" => "consulter_resume",
        );
    }

    /**
     * 
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if (!is_null($dossier_instruction)) {
            require_once "../obj/dossier_instruction.class.php";
            return new dossier_instruction($dossier_instruction);
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            require_once "../obj/dossier_instruction.class.php";
            $this->inst_dossier_instruction = new dossier_instruction($dossier_instruction);
        }
        //
        return $this->inst_dossier_instruction;
    }

    /**
     * 
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if (!is_null($dossier_coordination)) {
            require_once "../obj/dossier_coordination.class.php";
            return new dossier_coordination($dossier_coordination);
        }
        //
        if (is_null($this->inst_dossier_coordination)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_dossier_coordination = $inst_dossier_instruction->get_inst_dossier_coordination();
        }
        //
        return $this->inst_dossier_coordination;
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (!is_null($etablissement)) {
            require_once "../obj/etablissement.class.php";
            return new etablissement($etablissement);
        }
        //
        if (is_null($this->inst_etablissement)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_etablissement = $inst_dossier_instruction->get_inst_etablissement();
        }
        //
        return $this->inst_etablissement;
    }

    /**
     *
     */
    function get_inst_reunion($reunion = null) {
        //
        if (!is_null($reunion)) {
            //
            require_once "../obj/reunion.class.php";
            return new reunion($reunion);
        }
        //
        if (is_null($this->inst_reunion)) {
            //
            $reunion = $this->getVal("reunion");
            require_once "../obj/reunion.class.php";
            $this->inst_reunion = new reunion($reunion);
        }
        //
        return $this->inst_reunion;
    }

    /**
     *
     */
    function get_inst_reunion_type($reunion_type = null) {
        //
        if (!is_null($reunion_type)) {
            //
            require_once "../obj/reunion_type.class.php";
            return new reunion_type($reunion_type);
        }
        //
        if (is_null($this->inst_reunion_type)) {
            //
            $inst_reunion = $this->get_inst_reunion();
            $this->inst_reunion_type = $inst_reunion->get_inst_reunion_type();
        }
        //
        return $this->inst_reunion_type;
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        
        if ($maj == 0) { //ajout
            $form->setType('reunion', 'hidden');
            $form->setType('avis', 'hidden');
            $form->setType('avis_complement', 'hidden');
            $form->setType('avis_motivation', 'hidden');
            $form->setType('ordre', 'hidden');
        }

        if ($maj==1){ //modifier
            $form->setType('dossier_instruction', 'selecthiddenstatic');
            $form->setType('reunion', 'selecthiddenstatic');
            $form->setType('reunion_type', 'selecthiddenstatic');
            $form->setType('reunion_type_categorie', 'selecthiddenstatic');
        }// fin modifier
        if ($maj==2){ //supprimer
            //$form->setType('avis', 'selectstatic');
        }//fin supprimer
        if ($maj==3) { //consulter
            //$form->setType('avis', 'selectstatic');
        }//fin consulter

        if ($maj == 11) {
            $form->setType('dossier_instruction_reunion', 'hiddenstatic');
            $form->setType('dossier_instruction', 'selecthiddenstatic');
            $form->setType('reunion_type_categorie', 'select');
            $form->setType('reunion_type', 'select');
            $form->setType('date_souhaitee', 'date');
            $form->setType('motivation', 'textarea');
            $form->setType('reunion', 'hidden');
            $form->setType('proposition_avis', 'select');
            $form->setType('proposition_avis_complement', 'text');
            $form->setType('avis', 'hidden');
            $form->setType('avis_complement', 'hidden');
            $form->setType('avis_motivation', 'hidden');
            $form->setType('ordre', 'hidden');
        }
        if ($maj == 12) {
            $form->setType('dossier_instruction_reunion', 'hidden');
            $form->setType('dossier_instruction', 'hidden');
            $form->setType('reunion_type_categorie', 'selecthiddenstatic');
            $form->setType('reunion_type', 'hidden');
            $form->setType('date_souhaitee', 'datestatic');
            $form->setType('motivation', 'textareastatic');
            $form->setType('reunion', 'hidden');
            $form->setType('proposition_avis', 'selecthiddenstatic');
            $form->setType('proposition_avis_complement', 'hiddenstatic');
            $form->setType('avis', 'select');
            $form->setType('avis_complement', 'text');
            $form->setType('avis_motivation', 'textarea');
            $form->setType('ordre', 'hidden');
        }
    }

    /**
     * Permet de définir l'attribut onchange des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        //
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange(
            "reunion_type", 
            "filterSelect(this.value, 'reunion_type_categorie', 'reunion_type', 'dossier_instruction_reunion');".
            "filterSelect(this.value, 'proposition_avis', 'reunion_type', 'dossier_instruction_reunion');"
        );
    }

    function setLayout(&$form, $maj) {
        $form->setFieldset("dossier_instruction_reunion", "D", _("demande de passage"));
        $form->setFieldset("proposition_avis_complement", "F");
        $form->setFieldset("reunion", "D", _("reunion"));
        $form->setFieldset("ordre", "F");
        $form->setFieldset("avis", "D", _("avis"));
        $form->setFieldset("avis_motivation", "F");
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);

        // reunion
        $this->init_select($form, $this->f->db, $maj, null, "reunion", $sql_reunion, $sql_reunion_by_id, false);


        // Nous avons besoin de connaître le service du dossier_instruction sur
        // lequel la demande de passage (dossier_instruction_reunion) porte.
        // Plusieurs possibilités :
        // - en ajout :
        //   * en form -> sur le service du di sélectionné dans le form
        //   * en sousform -> sur le service du di en retourformulaire
        // - en modification ou autre
        //   * sur le service du di en base
        $di = null;
        if ($maj != 0) {
            $di = $this->getVal("dossier_instruction");
        } elseif ($this->is_in_context_of_foreign_key('dossier_instruction', $this->getParameter('retourformulaire'))) {
            $di = $this->getParameter("idxformulaire");
        }
        //
        $service = -1;
        if (!is_null($di)) {
            //
            require_once "../obj/dossier_instruction.class.php";
            $di = new dossier_instruction($di);
            $service = $di->getVal("service");
        } else {
            //
            $service = null;
        }


        //
        if ($maj == 11 || $maj == 12) {
            $old_maj = $maj;
            $maj = 0;
        }

        //
        if (!is_null($service) && $service != "") {
            // Filtre par service
            $sql_reunion_type_by_service = str_replace('<idx_service>', $service, $sql_reunion_type_by_service);
            $sql_reunion_type_by_service_by_id = str_replace('<idx_service>', $service, $sql_reunion_type_by_service_by_id);
            // Select / reunion_type
            $this->init_select($form, $this->f->db, $maj, null, "reunion_type", $sql_reunion_type_by_service, $sql_reunion_type_by_service_by_id, true);
        } else {
            // Select / reunion_type
            $this->init_select($form, $this->f->db, $maj, null, "reunion_type", $sql_reunion_type, $sql_reunion_type_by_id, true);
        }

        // Filtre par reunion_type
        $reunion_type = "";
        $champ = "reunion_type";
        if (isset($_POST[$champ])) {
            $reunion_type = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $reunion_type = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $reunion_type = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $reunion_type = $this->getVal($champ);
        }
        if ($reunion_type == "") {
            $reunion_type = -1;
        }

        // Field "reunion_type_categorie" / filtré par reunion_type
        $sql_reunion_type_categorie = str_replace('<reunion_type>', intval($reunion_type), $sql_reunion_type_categorie);
        $this->init_select($form, $this->f->db, $maj, null, "reunion_type_categorie", $sql_reunion_type_categorie, $sql_reunion_type_categorie_by_id, true);
        if ($maj == 0 || $maj == 1) { // Suppression du choix vide
            array_shift($form->select["reunion_type_categorie"][0]);
            array_shift($form->select["reunion_type_categorie"][1]);
        }



        // Field "avis" / filtré par reunion_type
        $sql_avis = str_replace('<reunion_type>', intval($reunion_type), $sql_avis);
        $this->init_select($form, $this->f->db, $maj, null, "avis", $sql_avis, $sql_avis_by_id, true);

        //
        if (isset($old_maj)) {
            $maj = $old_maj; 
            unset($old_maj);
        }

        //
        if ($maj == 11) {
            $old_maj = $maj;
            $maj = 0;
        }

        // Field "proposition_avis" / filtré par reunion_type
        $sql_proposition_avis = str_replace('<reunion_type>', intval($reunion_type), $sql_proposition_avis);
        $this->init_select($form, $this->f->db, $maj, null, "proposition_avis", $sql_proposition_avis, $sql_proposition_avis_by_id, true);

        //
        if (isset($old_maj)) {
            $maj = $old_maj; 
            unset($old_maj);
        }
    }

    /**
     * TREATMENT - rendre_l_avis.
     * 
     * Cette methode permet ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function rendre_l_avis($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // Recuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        //
        $valF = array();
        if (!is_numeric($val['avis'])) {
            $valF['avis'] = NULL;
        } else {
            $valF['avis'] = $val['avis'];
        }
        if ($val['avis_complement'] == "") {
            $valF['avis_complement'] = NULL;
        } else {
            $valF['avis_complement'] = $val['avis_complement'];
        }
        $valF['avis_motivation'] = $val['avis_motivation'];
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(DB_PREFIXE.$this->table, $valF, DB_AUTOQUERY_UPDATE, $this->getCle($id));
        // Logger
        $this->addToLog(__METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($this->valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($id)."\")", VERBOSE_MODE);
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            // Return
            return $this->end_treatment(__METHOD__, false);
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog(__METHOD__."(): ".$message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage(_("Vos modifications ont bien ete enregistrees.")."<br/>");
            }
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * Permet d'ajouter un nouvel élément depuis l'action d'une réunion.
     * 
     * C'est-à-dire sans utiliser le formulaire d'ajout standard.
     *
     * @param mixed $val Tableau de paramètres. Les paramètres nécessaires sont
     *                   les valeurs nécessaires à la création d'un élément.
     *
     * @return boolean
     */
    function ajouter_depuis_une_reunion($val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        //
        $val[$this->clePrimaire] = null;
        //
        if (!isset($val["motivation"])) {
            $val["motivation"] = "";
        }
        $val["ordre"] = null;
        $val["proposition_avis"] = null;
        $val["proposition_avis_complement"] = "";
        $val["avis"] = null;
        $val["avis_complement"] = "";
        $val["avis_motivation"] = "";
        //
        $this->f->db->autoCommit(false); // Désactivation du commit automatique
        //
        $ret = $this->ajouter($val);
        //
        if ($ret == true) {
            $this->f->db->commit(); // Validation des transactions
        } else {
            $this->undoValidation(); // Annulation des transactions
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return $ret;
    }

    /**
     *
     * @return
     */
    function planifier_pour_la_reunion($reunion_id) {
        //
        return $this->manage_planning("plan", array("reunion" => $reunion_id));
    }

    /**
     *
     * @return
     */
    function deplanifier_de_la_reunion($reunion_id) {
        //
        return $this->manage_planning("unplan", array("reunion" => $reunion_id));
    }

    /**
     *
     * @return
     */
    function manage_planning($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "plan" && $mode != "unplan") {
            return false;
        }
        //
        $this->correct = true;
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // On vérifie que la demande de passage en réunion existe bien
        if ($id == null) {
            $this->correct = false;
            $this->addToMessage("=> Cette demande n'existe pas.");
            return false;
        }
        if ($mode == "plan") {
            // On vérifie que la demande de passage en réunion n'est pas déjà 
            // planifiée pour une réunion
            if ($this->getVal("reunion") != null) {
                $this->correct = false;
                $this->addToMessage("=> Cette demande est déjà prévue pour une réunion.");
                return false;
            }
            //
            $valF = array(
                "reunion" => $val["reunion"],
            );
            //
            $valid_message = _("Demande de passage correctement planifiee.");
        } elseif ($mode == "unplan") {
            // On vérifie que la demande de passage en réunion est effectivement 
            // planifiée pour une réunion
            if ($this->getVal("reunion") == null) {
                $this->correct = false;
                $this->addToMessage("=> Cette demande n'est pas prévue pour une réunion.");
                return false;
            }
            //
            $valF = array(
                "reunion" => null,
                "ordre" => null,
            );
            //
            $valid_message = _("Demande de passage correctement deplanifiee.");
        }
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification."));
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * VIEW - view_edition.
     *
     * @return void
     */
    function view_edition() {
        //
        $this->checkAccessibility();
        //
        if ($this->get_action_key_for_identifier("edition-compte_rendu_specifique") == $this->getParameter("maj")) {
            //
            $reunion_type = $this->get_inst_reunion_type();
            $modele = $reunion_type->getVal("modele_compte_rendu_specifique");
            //
            $pdfedition = $this->compute_pdf_output("modele_edition", $modele);
            $this->expose_pdf_output(
                $pdfedition["pdf_output"],
                $this->getVal('code')."_compte-rendu-d-avis_".date('YmdHis').".pdf"
            );
        } else {
            die();
        }
    }

    /**
     * Récupère la date de réunion depuis la réunion
     *
     * @return string date de la réunion
     */
    function get_reunion_date_reunion() {
        // Initialisation de la variable de retour
        $return = "";
        //
        $id = $this->getVal('reunion');
        //
        if (!empty($id)) {
            // Instance de la classe reunion
            require_once '../obj/reunion.class.php';
            $reunion = new reunion($id);
            // Récupère la valeur du champ
            $return = $reunion->getVal('date_reunion');
        }
        //
        return $return;
    }

    /**
     * Récupère le dossier de coordination depuis le dossier d'instruction.
     *
     * @return integer Identifiant du dossier de coordination
     */
    function get_dossier_instruction_dossier_coordination() {
        // Initialisation de la variable de retour
        $return = "";
        //
        $id = $this->getVal('dossier_instruction');
        //
        if (!empty($id)) {
            // Instance de la classe reunion
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($id);
            // Récupère la valeur du champ
            $return = $dossier_instruction->getVal('dossier_coordination');
        }
        //
        return $return;
    }

    /**
     * Récupère l'établissement depuis le dossier de coordination du
     * dossier d'instruction.
     *
     * @return integer Identifiant de l'établissement
     */
    function get_dossier_instruction_dossier_coordination_etablissement() {
        // Initialisation de la variable de retour
        $return = "";
        //
        $id = $this->getVal('dossier_instruction');
        //
        if (!empty($id)) {
            // Instance de la classe reunion
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($id);
            // Récupère la valeur du champ
            $return = $dossier_instruction->get_dossier_coordination_etablissement('dossier_coordination');
        }
        //
        return $return;
    }

    /**
     * Récupère le service du dossier d'instruction.
     *
     * @return integrer
     */
    function get_dossier_instruction_service($dossier_instruction) {
        // Initialisation de la variable de retour
        $return = "";
        //
        if (!empty($dossier_instruction)) {
            // Instance de la classe reunion
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_instruction);
            // Récupère la valeur du champ
            $return = $dossier_instruction->getVal('service');
        }
        //
        return $return;
    }

    /**
     * Contenu spécifique sur le formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function formSpecificContent($maj) {
        //
        $id = $this->getVal($this->clePrimaire);
        // Affiche l'identifiant de l'enregistrement dans le DOM
        if(isset($this->valF[$this->clePrimaire]) AND !empty($this->valF[$this->clePrimaire])) {
            echo "<input id=\"id_retour\" name=\"id_retour\" type=\"hidden\" value=\"".
                    $this->valF[$this->clePrimaire]."\" />";
        } elseif(isset($id) AND !empty($id) AND $maj == 1) {
            echo "<input id=\"id_retour\" name=\"id_retour\" type=\"hidden\" value=\"".
                    $this->getVal($this->clePrimaire)."\" />";
        }
    }

    /**
     * Contenu spécifique sur le sous-formulaire.
     *
     * @param integer $maj Mode du formulaire
     *
     * @return void
     */
    function sousformSpecificContent($maj) {
        //
        $this->formSpecificContent($maj);
    }

    /**
     * Cette méthode permet d'afficher des informations spécifiques après le
     * formulaire de l'objet.
     *
     * @return void
     */
    function afterFormSpecificContent() {
        // Affichage du contenu seulement en consultation et dans le contexte d'une reunion
        if ($this->getParameter('maj') == 3 
            && $this->is_in_context_of_foreign_key('reunion', $this->getParameter('retourformulaire'))
            && $this->is_numbered()) {
            //
            $this->fieldset_autorite_police();
            //
            $this->fieldset_dossier_instruction_reunion();
        }
    }

    /**
     * Cette méthode permet d'afficher des informations spécifiques après le
     * sous-formulaire de l'objet.
     *
     * @return void
     */
    function afterSousFormSpecificContent() {
        //
        $this->afterFormSpecificContent();
    }

    /**
     * Récupère la liste des autorités de police liées à l'enregistrement.
     *
     * @return array Tableau des AP
     */
    function list_autorite_police() {
        // Initialisation du tableau de résultat
        $return = array();
        // Identifiant de l'enregistrement
        $id = ($this->getParameter('maj') == 0) ? 0 : $this->getVal($this->clePrimaire);
        //
        if (!empty($id)) {
            // Requête SQL
            $sql = "SELECT autorite_police
                    FROM ".DB_PREFIXE."autorite_police
                    WHERE dossier_instruction_reunion = ".intval($id)."
                    OR dossier_instruction_reunion_prochain = ".intval($id);
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $return[] = $row['autorite_police'];
            }
        }
        // Retourne le tableau
        return $return;
    }

    /**
     * Fieldset des autorités de police à afficher dans le contenu spécifique.
     *
     * @return void
     */
    function fieldset_autorite_police() {
        //
        if (!$this->f->isAccredited(array("autorite_police_consulter_resume", "autorite_police",), "OR")) {
            return;
        }

        // Conteneurs de la liste des autorités de police
        printf("<div id=\"formSpecificContent_autorite_police\" class=\"autorite_police_hidden_bloc col_12\">");
        printf("<fieldset class=\"cadre ui-corner-all ui-widget-content startClosed\">");
        printf("<legend class=\"ui-corner-all ui-widget-content ui-state-active\">"._("Autorites de police")."</legend>");
        printf("<div id=\"liste_autorite_police\" class=\"liste_autorite_police col_12\">");

        // Récupère la liste des autorités de police
        $list_autorite_police = $this->list_autorite_police();
        // Pour chaque résultat affiche sa synthese
        foreach ($list_autorite_police as $value) {
            //
            require_once "../obj/autorite_police.class.php";
            $autorite_police = new autorite_police($value);
            $autorite_police->display_synthesis();
            $autorite_police->__destruct();
        }

        if ($this->f->isAccredited("autorite_police_ajouter")
            && $this->is_reunion_not_closed() == true) {
            // Bouton d'ajout
            $add_button = '<div class="autorite_police col_3" id="add_autorite_police">';
            $add_button .= '<span class="om-form-button add-16">';
            $add_button .= _("Ajouter une autorite de police");
            $add_button .= '</span>';
            $add_button .= '</div>';
            // Affiche le bouton d'ajout
            printf($add_button);
        } else {
            if (count($list_autorite_police) == 0) {
                print _("Aucune autorité de police");
            }
        }
        // Ferme les conteneurs
        printf("</div>");
        printf("</fieldset>");
        printf("</div>");
        printf("<div class=\"visualClear\"><!-- --></div>");

    }

    /**
     * Récupère la liste des demande de passage en réunion du DI.
     *
     * @param integer $dossier_instruction Identifiant du DI
     *
     * @return array
     */
    function get_list_dossier_instruction_reunion($dossier_instruction, $dossier_instruction_reunion) {
        // Initialisation du tableau de résultat
        $return = array();
        //
        if (!empty($dossier_instruction)) {
            // Requête SQL
            $sql = "SELECT dossier_instruction_reunion
                    FROM ".DB_PREFIXE."dossier_instruction_reunion
                    WHERE dossier_instruction_reunion NOT IN (".intval($dossier_instruction_reunion).")
                    AND dossier_instruction =".intval($dossier_instruction);
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__.": db->query(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $return[] = $row['dossier_instruction_reunion'];
            }
        }
        // Retourne le tableau
        return $return;
    }

    /**
     * Fieldset des autorités de police à afficher dans le contenu spécifique.
     *
     * @return void
     */
    function fieldset_dossier_instruction_reunion() {
        //
        if (!$this->f->isAccredited(array("dossier_instruction_reunion_consulter_resume", "dossier_instruction_reunion",), "OR")) {
            return;
        }

        // Conteneur de la liste des demande de passage en reunion
        printf("<div id=\"formSpecificContent_dossier_instruction_reunion\" class=\"dossier_instruction_reunion_hidden_bloc col_12\">");
        printf("<fieldset class=\"cadre ui-corner-all ui-widget-content startClosed\">");
        printf("<legend class=\"ui-corner-all ui-widget-content ui-state-active\">"._("demande de passage en reunion")."</legend>");
        printf("<div id=\"liste_dossier_instruction_reunion col_12\">");

        // Récupère la liste des autorités de police
        $list_dossier_instruction_reunion = $this->get_list_dossier_instruction_reunion($this->getVal('dossier_instruction'), $this->getVal($this->clePrimaire));
        // Pour chaque résultat affiche sa synthese
        foreach ($list_dossier_instruction_reunion as $dossier_instruction_reunion_id) {
            //
            require_once "../obj/dossier_instruction_reunion.class.php";
            $dossier_instruction_reunion = new dossier_instruction_reunion($dossier_instruction_reunion_id);
            $dossier_instruction_reunion->display_synthesis();
            $dossier_instruction_reunion->__destruct();
        }

        if ($this->f->isAccredited("dossier_instruction_reunion_ajouter")
            && $this->is_reunion_not_closed() == true) {
            // Bouton d'ajout
            $add_button = '<div class="dossier_instruction_reunion col_3" id="add_dossier_instruction_reunion">';
            $add_button .= '<span class="om-form-button add-16">';
            $add_button .= _("Ajouter une demande de passage en reunion");
            $add_button .= '</span>';
            $add_button .= '</div>';
            // Affiche le bouton d'ajout
            printf($add_button);
        }

        // Ferme les conteneurs
        printf("</div>");
        printf("</fieldset>");
        printf("</div>");

        printf("<div class=\"visualClear\"><!-- --></div>");
    }

    /**
     * VIEW - view_synthesis.
     *
     * @return void
     */
    function view_synthesis() {
      // Logger
      $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
      // Désactive les logs
      $this->f->disableLog();
      // Vérification de l'accessibilité sur l'élément
      $this->checkAccessibility();
      // Affichage de la synthèse
      $this->display_synthesis();
      // Logger
      $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * Synthèse de la demande de passage en réunion.
     * Affichage résumé de l'enregistrement.
     *
     * @return void
     */
    function display_synthesis() {
        // Identifiant de l'enregistrement
        $id = $this->getVal($this->clePrimaire);

        // Conteneur de la demande de passage
        printf("<div class=\"dossier_instruction_reunion col_3\" id=\"dossier_instruction_reunion_".$id."\">\n");

        // Légende
        printf("<div class=\"legend_synthesis_dossier_instruction_reunion\">\n");

        //
        printf(_("Demande n°%s"), $id);

        // Ferme la légende
        printf("</div>\n");

        //
        printf("<div class=\"synthese_dossier_instruction_reunion\">\n");

        // Structure pour les champs à afficher
        $field = '<div class="field">';
        $field .= '<div class="form-libelle">';
        $field .= '<label id="lib-%1$s" class="libelle-%1$s" for="%1$s">%2$s</label>';
        $field .= '</div>';
        $field .= '<div class="form-content">';
        $field .= '<span id="%1$s" class="field_value">%3$s</span>';
        $field .= '</div>';
        $field .= '</div>';

        //
        printf($field, "vs-date_souhaitee", _("date_souhaitee"), $this->dateDBToForm($this->getVal("date_souhaitee")));
        //
        printf($field, "vs-reunion_type", _("reunion_type"), $this->f->get_field_from_table_by_id($this->getVal("reunion_type"), "libelle", "reunion_type"));
        //
        printf($field, "vs-reunion_type_categorie", _("reunion_type_categorie"), $this->getVal("reunion_type_categorie"));
        //
        printf($field, "vs-avis_motivation", _("avis_motivation"), $this->getVal("avis_motivation"));

        // Valeur de formulaire à retourner
        printf("<input type=\"hidden\" class=\"dossier_instruction_reunion_id\" value=\"".$id."\" />\n");
        //
        printf("</div>\n");

        // Ferme le conteneur de l'autorité de police
        printf("</div>\n");
    }


    /**
     * CONDITION - is_not_planned.
     *
     * @return bool
     */
    function is_not_planned() {
        //
        if (is_null($this->getVal("reunion"))
            || $this->getVal("reunion") == "") {
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_planned.
     *
     * @return bool
     */
    function is_planned() {
        //
        if (!is_null($this->getVal("reunion"))
            && $this->getVal("reunion") != "") {
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_numbered.
     *
     * @return bool
     */
    function is_numbered() {
        //
        if (!is_null($this->getVal("ordre"))
            && $this->getVal("ordre") != ""
            && is_numeric($this->getVal("ordre"))
            && $this->getVal("ordre") != "0") {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_available_edition_compte_rendu_specifique.
     * 
     * @return bool
     */
    function is_available_edition_compte_rendu_specifique() {
        //
        $reunion = $this->get_inst_reunion();
        return $reunion->is_available_edition_compte_rendu_specifique();
    }

    /**
     * CONDITION - is_reunion_not_closed.
     * 
     * @return bool
     */
    function is_reunion_not_closed() {
        //
        if ($this->is_not_planned()) {
            return true;
        }
        //
        $reunion = $this->get_inst_reunion();
        if (!is_null($reunion->getVal("reunion_cloture"))
            && $reunion->getVal("reunion_cloture") != ""
            && $reunion->getVal("reunion_cloture") == "t") {
            //
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_from_good_service.
     * 
     * @return bool
     */
    function is_from_good_service() {
        //
        $dossier_instruction = $this->get_inst_dossier_instruction();
        //
        return $dossier_instruction->is_from_good_service();
    }

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = array(
            _("dossier_instruction_reunion") => array(
                "dpr.proposition_avis" => _("proposition_avis"),
                "dpr.proposition_avis_comp" => _("proposition_avis_complement"),
                "dpr.ordre" => _("ordre de passage"),
                "dpr.date_souhaitee" => _("date_souhaitee"),
                "dpr.avis_rendu" => _("avis rendu"),
                "dpr.avis_rendu_comp" => _("complement de l'avis rendu"),
                "dpr.avis_rendu_motiv" => _("motivation de l'avis rendu"),
                "dpr.reunion_type_categorie" => _("catégorie de passage"),
                "dpr.liste_ap" => _("décision autorité de police"),
            ),
        );
        // Libellés de la réunion liée à la demande de passage
        require_once "../obj/reunion.class.php";
        $rn = new reunion('0');
        $rn_labels = $rn->get_labels_merge_fields();
        // Retour de tous les libellés
        return array_merge($labels, $rn_labels);
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $inst_reu_categorie = $this->get_inst_reunion_categorie();
        $reu_categorie_libelle = $inst_reu_categorie->getVal("libelle");
        //
        $values = array(
            "dpr.ordre" => $this->getVal("ordre"),
            "dpr.proposition_avis" => $this->f->get_label_of_foreign_key("dossier_instruction_reunion","avis",$this->getVal("proposition_avis")),
            "dpr.proposition_avis_comp" => $this->getVal("proposition_avis_complement"),
            "dpr.date_souhaitee" => $this->dateDBToForm($this->getVal("date_souhaitee")),
            "dpr.avis_rendu" => $this->f->get_label_of_foreign_key("dossier_instruction_reunion","avis",$this->getVal("avis")),
            "dpr.avis_rendu_comp" => $this->getVal("avis_complement"),
            "dpr.avis_rendu_motiv" => $this->getVal("avis_motivation"),
            "dpr.reunion_type_categorie" => $reu_categorie_libelle,
            "dpr.liste_ap" => $this->get_decision_autorite_police_list(),
        );
        // On récupère les valeurs de la réunion
        require_once "../obj/reunion.class.php";
        $rn_id = $this->getVal("reunion");
        if ($rn_id == '') {
            $rn_id = '0';
        }
        $rn = new reunion($rn_id);
        $rn_values = $rn->get_values_merge_fields();
        // retour
        return array_merge($values, $rn_values);
    }

    /**
     *
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "dossier_instruction",
            "dossier_coordination",
            "etablissement",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = array();
                //
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem_method = "get_inst_".$value;
                    $elem = $this->$elem_method();
                    if ($elem != null) {
                        $elem_values = $elem->get_merge_fields($type);
                        $values = array_merge($values, $elem_values);
                    }
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = array();
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem = new $value(0);
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_reunion_avis($reunion_avis = null) {
        return $this->get_inst_common("reunion_avis", $reunion_avis, "avis");
    }

    /**
     *
     */
    function get_inst_reunion_categorie($reunion_categorie = null) {
        return $this->get_inst_common("reunion_categorie", $reunion_categorie, "reunion_type_categorie");
    }
    
    
    /**
     * Retourne une liste de décision des autorités de police.
     * 
     * @return [string]       Code HTML du tableau
     */
    function get_decision_autorite_police_list () {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_body_line = '
        <tr>
            <td rowspan="2" style="width:5%%;text-align:center;%1$s">%2$s</td>
            <td style="width:95%%;%1$s">%3$s</td>
        </tr>
        <tr>
            <td style="width:95%%;%1$s">%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "";
        $css_bg_line_even = "";

        /**
         * Récupération des prescriptions
         */
        // Requête
        $sql = "
        SELECT
            concat(
                autorite_police_decision.libelle,
                ' - ',
                autorite_police.delai,
                ' (jours)'
            ) as \"decision\",
            autorite_police_motif.libelle as \"motif\"
        FROM ".DB_PREFIXE."autorite_police
        JOIN ".DB_PREFIXE."dossier_instruction_reunion
            ON dossier_instruction_reunion.dossier_instruction_reunion=autorite_police.dossier_instruction_reunion
            AND dossier_instruction_reunion.dossier_instruction_reunion=".intval($this->getVal($this->clePrimaire))."
        LEFT JOIN ".DB_PREFIXE."autorite_police_motif
            ON autorite_police.autorite_police_motif=autorite_police_motif.autorite_police_motif
        LEFT JOIN ".DB_PREFIXE."autorite_police_decision
            ON autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision
        ORDER BY 
            autorite_police ASC
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Traitement des erreurs de base de données
        if ($this->f->isDatabaseError($res, true)) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if ($res->numRows() == 0) {
            return _("Aucun");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $i = 0;
        $table_body_content = "";
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                "-",
                $row["decision"],
                $row["motif"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_body_content
        );
    }


    // }}} END - GET INST

}

?>
