<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/technicien_arrondissement.class.php";

class technicien_arrondissement extends technicien_arrondissement_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * XXX La gestion du service pour cet objet est à revoir
     *     Il doit être sélectionné par rapport à l'acteur choisi
     *     et non par rapport au service de l'utilisateur
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // MODE AJOUTER
        if ($maj == 0) {
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)
                && $form->val["technicien"] == $this->getParameter("idxformulaire")) {
                $form->setType("service", "selecthiddenstatic");
            }
        }
        // MDOE MODIFIER
        if ($maj == 1) {
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)
                && $form->val["technicien"] == $this->getParameter("idxformulaire")) {
                $form->setType("service", "selecthiddenstatic");
            }
        }
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        //
        if ($validation == 0) {
            //
            if ($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire)) {
                //
                require_once "../obj/acteur.class.php";
                $acteur = new acteur($idxformulaire);
                $service = $acteur->getVal("service");
                $form->setVal('service', $service);
            }
        }
    }

}

?>
