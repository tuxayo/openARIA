<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once ("../gen/obj/piece.class.php");

class piece extends piece_gen {

    /**
     * On définit le type global de champs spécifiques.
     */
    var $abstract_type = array(
        "uid" => "file",
    );

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 004 - lu
        // Marquer comme lu
        $this->class_actions[4] = array(
            "identifier" => "lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("lu"),
                "order" => 100,
                "class" => "read-16",
            ),
            "view" => "formulaire",
            "method" => "read",
            "button" => "lu",
            "permission_suffix" => "marquer_comme_lu",
            "condition" => "is_readable",
        );

        // ACTION - 005 - non_lu
        // Marquer comme non lu
        $this->class_actions[5] = array(
            "identifier" => "non_lu",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("non lu"),
                "order" => 110,
                "class" => "unread-16",
            ),
            "view" => "formulaire",
            "method" => "unread",
            "button" => "non_lu",
            "permission_suffix" => "marquer_comme_non_lu",
            "condition" => "is_unreadable",
        );

        // ACTION - 006 - suivi
        // Active le suivi
        $this->class_actions[6] = array(
            "identifier" => "suivi",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("suivi"),
                "order" => 120,
                "class" => "watch-16",
            ),
            "view" => "formulaire",
            "method" => "watch",
            "button" => "suivi",
            "permission_suffix" => "suivi",
            "condition" => "is_watchable",
        );

        // ACTION - 007 - non_suivi
        // Désactive le suivi
        $this->class_actions[7] = array(
            "identifier" => "non_suivi",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("non suivi"),
                "order" => 130,
                "class" => "unwatch-16",
            ),
            "view" => "formulaire",
            "method" => "unwatch",
            "button" => "non_suivi",
            "permission_suffix" => "non_suivi",
            "condition" => "is_unwatchable",
        );

        // ACTION - 008 - valide
        // Valide le document
        $this->class_actions[8] = array(
            "identifier" => "valide",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("valide"),
                "order" => 140,
                "class" => "valid-16",
            ),
            "view" => "formulaire",
            "method" => "valid",
            "button" => "valide",
            "permission_suffix" => "valide",
            "condition" => "is_validable",
        );

        // ACTION - 009 - annule
        // Annule la validation du document
        $this->class_actions[9] = array(
            "identifier" => "annule",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("annule"),
                "order" => 140,
                "class" => "cancel-16",
            ),
            "view" => "formulaire",
            "method" => "cancel",
            "button" => "annule",
            "permission_suffix" => "annule",
            "condition" => "is_cancelable",
        );

        // ACTION - 010 - set_piece_service
        // Affiche/cache le multi-select des autorités de police
        $this->class_actions[10] = array(
            "identifier" => "set_piece_service",
            "view" => "view_set_piece_service",
        );

        // ACTION - 022 - view_list_document_entrant_guichet_unique
        //
        $this->class_actions[22] = array(
            "identifier" => "view_list_document_entrant_guichet_unique",
            "view" => "view_list_document_entrant_guichet_unique",
            "permission_suffix" => "consulter",
            "condition" => array("is_swrod_enabled"),
        );

        // ACTION - 023 - view_download_document_entrant_guichet_unique
        //
        $this->class_actions[23] = array(
            "identifier" => "view_download_document_entrant_guichet_unique",
            "view" => "view_download_document_entrant_guichet_unique",
            "permission_suffix" => "consulter",
            "condition" => array("is_swrod_enabled"),
        );
    }

    /**
     * CONDITION - is_swrod_enabled.
     *
     * @return bool
     */
    function is_swrod_enabled() {
        return $this->f->is_swrod_enabled();
    }

    /**
     * VIEW - view_download_document_entrant_guichet_unique.
     *
     * @return void
     */
    function view_download_document_entrant_guichet_unique() {

        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        //
        ($this->f->get_submitted_get_value('id') !== null ? $id = $this->f->get_submitted_get_value('id') : $id = "");

        //
        if ($this->f->is_swrod_available() !== true) {
            $this->f->displayMessage("error", _("Problème de configuration. Contactez votre administrateur."));
            return;
        }
        $swrod = $this->f->get_inst_swrod();

        //
        $ret = $swrod->get_file($id);
        if (!isset($ret['file_content'])) {
            $this->f->displayMessage("error", _("Erreur pendant l'exécution. Contactez votre administrateur."));
            return;
        }

        // Headers
        header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
        header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date dans le passé
        header("Content-Type: ".$ret['metadata']['mimetype']);
        header("Accept-Ranges: bytes");

        // Vérification pour la valeur de $dl
        if ($this->f->getParameter("edition_output") == "download") {
            $dl="download";
        } else {
            $dl="inline";
        }

        // Vérification si on affiche simplement l'image, sinon envoi un dialogue de sauvegarde
        if ($dl=="download") {
            header("Content-Disposition: attachment; filename=\"".$ret['metadata']['filename']."\";" );
         } else {
            header("Content-Disposition: inline; filename=\"".$ret['metadata']['filename']."\";" );

         }

        // Rendu du fichier
        echo $ret['file_content'];
        // Cette vue expose un fichier PDF au téléchargement, il est nécessaire 
        // d'arrêter le script pour ne pas afficher le code HTML de la page
        // standard de l'application.
        die();
    }

    /**
     * VIEW - view_list_document_entrant_guichet_unique.
     *
     * @return void
     */
    function view_list_document_entrant_guichet_unique() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // On récupère le code du dossier ADS sur lié au DC passé en paramètre
        // La règle est la suivante :
        // - un DA ADS est rempli alors on passe ce code
        // - un DA ADS n'est pas rempli mais un DI ADS est rempli alors on passe ce code
        // - aucun des deux n'est rempli alors on passe vide
        ($this->f->get_submitted_get_value('dc') !== null ? $dc = $this->f->get_submitted_get_value('dc') : $dc = "");
        $dossier = "";
        $inst_dc = $this->get_inst_dossier_coordination($dc);
        if (method_exists($inst_dc, 'getVal')) {
            if ($inst_dc->getVal('dossier_autorisation_ads') != "") {
                $dossier = $inst_dc->getVal('dossier_autorisation_ads');
            } elseif ($inst_dc->getVal("dossier_instruction_ads") != "") {
                $dossier = $inst_dc->getVal('dossier_instruction_ads');
            }
        }

        /**
         * Gestion des templates html
         */
        //
        $template_view = '
<div id="swrod">
<!-- Liste des pièces -->
%s
</div>
';
        // TABLE - HEAD
        // En-tête de tableau pour afficher la date et la catégorie des documents
        $template_header  = '
    <thead>
        <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
            <th class="title col-0 firstcol %s" colspan="4">
                <span class="name">
                    %s
                </span>
            </th>
        </tr>
    </thead>
';
        // TABLE - CELL content - NOM DU FICHIER
        // Template de visualisation du nom du fichier avec accès au
        // téléchargement de la pièce
        $template_filename = '
<a class="lienTable lienDocumentNumerise"
   href="../scr/form.php?obj=piece&action=23&idx=0&id=%s" 
   target="_blank"
   id="document_numerise_%s">
    <span class="om-prev-icon reqmo-16" title="'._("Télécharger").'">
        %s
    </span>
</a>
';

        // TABLE - BODY - TR TD - STRUCTURE 4 colonnes
        $template_line_4col = '
<tr class="tab-data %s col4">
    <td class="icons">
        %s
    </td>
    <td class="col-0 firstcol">
        %s
    </td>
    <td class="col-1">
        %s
    </td>
    <td class="col-2">
        %s
    </td>
</tr>
';

        /**
         * Récupération de la liste des pièces (document_numerise)
         *
         * En fonction du contexte dans lequel on se trouve la requête est
         * différente.
         */
        //
        if ($this->f->is_swrod_available() !== true) {
            $this->f->displayMessage("error", _("Problème de configuration. Contactez votre administrateur."));
            return;
        }
        $swrod = $this->f->get_inst_swrod();
        $list = $swrod->get_list($dossier);
        if (!isset($list) || !is_array($list)) {
            $this->f->displayMessage("error", _("Erreur pendant l'exécution. Contactez votre administrateur."));
            return;
        }

        /**
         * Gestion de l'affichage
         */
        //
        $style = 'odd';
        //
        $ct_list_dn = "";
        //
        $i = 1;
        //Résultat à $i - 1 pour tester la date et catégorie des documents
        $lastRes = array();
        $lastRes['date_creation'][0] = "";
        $lastRes['categorie'][0] = "";
        //Tant qu'il y a des résultats
        foreach ($list as $row) {
            //
            $lastRes['date_creation'][$i] = $row['date_creation'];
            $lastRes['categorie'][$i] = $row['categorie'];
            //Si la date de création est différente de celle du résultat juste avant
            if ($row['date_creation'] != $lastRes['date_creation'][$i-1]) {
                //Si ce n'est pas le premier résultat on ferme la table
                if ($i != 0) {
                    $ct_list_dn .= "</table>";
                }
                //Affiche la table
                $ct_list_dn .= "<table class='tab-tab document_numerise'>";
                //Affiche le header de la date
                $ct_list_dn .= sprintf(
                    $template_header,
                    'headerDate',
                    $this->f->formatDate($row['date_creation'])
                );
                //Affiche le header de la catégorie
                $ct_list_dn .= sprintf(
                    $template_header,
                    'headerCat',
                    $row['categorie']
                );
                //Style des lignes
                $style = 'odd';
            }
            
            //Si la date de création est identique à celle du résultat juste avant
            //et la catégorie est différente de celle du résultat juste avant
            if ($row['date_creation'] == $lastRes['date_creation'][$i-1] && $row['categorie'] != $lastRes['categorie'][$i-1]) {
                //Affiche le header de la catégorie
                $ct_list_dn .= sprintf(
                    $template_header,
                    'headerCat',
                    $row['categorie']
                );
                //Style des lignes
                $style = 'odd';
            }

            //Si toujours dans la catégorie on change le style de la ligne
            if ($row['categorie'] == $lastRes['categorie'][$i-1] && $row['date_creation'] == $lastRes['date_creation'][$i-1]) {
                $style = ($style=='even')?'odd':'even';
            }

            // Si on est dans la visualisation du DA, on affiche le numéro du dossier
            // d'instruction auquel est rataché la pièce et le nom du fichier
            $ct_list_dn .= sprintf(
                $template_line_4col,
                $style,
                "",
                $row['dossier'],
                sprintf(
                    $template_filename,
                    $row['uid'],
                    $row['uid'],
                    $row['nom_fichier']
                ),
                $row['type_document']
            );

            //
            $i++;

        }

        //On ferme la table
        $ct_list_dn .= "</table>";

        //S'il n'y a pas de résultat on affiche "Aucun enregistrement"
        if (count($list) == 0) {
            //
            $ct_list_dn = "<p class='noData'>"._("Aucun enregistrement")."</p>";
        }

        /**
         *
         */
        printf(
            $template_view,
            $ct_list_dn
        );
    }

    /**
     * VIEW - view_set_piece_service
     *
     * Modifie la valeur du service si c'est un dossier d'instruction qui est
     * sélectionné.
     *
     * @return void
     */
    function view_set_piece_service() {
        // Désactive les logs
        $this->f->disableLog();

        // Récupère la valeur du champ courrier_type
        $postvar = $this->getParameter("postvar");
        $dossier_instruction = $postvar['dossier_instruction'];

        // On récupère le service
        $service = $this->get_service_by_dossier_instruction($dossier_instruction);

        //
        echo json_encode(
            array(
                "service" => $service,
            )
        );
    }

    /**
     * Permet de définir le type des champs
     */
    function setType(&$form,$maj) {
        //
        parent::setType($form,$maj);

        // En mode ajout
        if ($maj==0) {
            if($this->retourformulaire == "") {
                $form->setType('uid','upload');
            } else {
                $form->setType('uid','upload2');
            }

            // désactivation de la date d'enregistrement
            $form->setType('om_date_creation','hidden');

            // Type select
            $form->setType('choix_lien', 'select');

            // Cache le statut
            $form->setType('piece_statut', 'selecthidden');

            // Cache le champ
            $form->setType('lu', 'hidden');

            //
            if (empty($this->retourformulaire)) {
                // Type autocomplete
                $form->setType('etablissement', 'autocomplete');
                $form->setType('dossier_coordination', 'autocomplete');
                $form->setType('dossier_instruction', 'autocomplete');
            }

            // Si c'est un sous-formulaire
            if (!empty($this->retourformulaire)) {
                // Cache le champ du choix du lien
                $form->setType('choix_lien', 'hidden');

                // Si c'est un sous-formulaire de la classe etablissement ou
                // une de ses surcharges
                if (in_array($this->retourformulaire, $this->foreign_keys_extended['etablissement'])) {
                    // Cache les autres champs de liaison
                    $form->setType('dossier_coordination', 'hidden');
                    $form->setType('dossier_instruction', 'hidden');
                }

                // Si c'est un sous-formulaire de la classe dossier_coordination
                // ou une de ses surcharges
                if (in_array($this->retourformulaire, $this->foreign_keys_extended['dossier_coordination'])) {
                    // Cache les autres champs de liaison
                    $form->setType('etablissement', 'hidden');
                    $form->setType('dossier_instruction', 'hidden');
                }

                // Si c'est un sous-formulaire de la classe dossier_instruction
                // ou une de ses surcharges
                if (in_array($this->retourformulaire, $this->foreign_keys_extended['dossier_instruction'])) {
                    // Cache les autres champs de liaison
                    $form->setType('etablissement', 'hidden');
                    $form->setType('dossier_coordination', 'hidden');
                }
            }
        }// fin ajout

        // En mode modifier
        if ($maj==1) {
            if($this->retourformulaire == "") {
                $form->setType('uid','upload');
            } else {
                $form->setType('uid','upload2');
            }
            // date de création non modifiable
            $form->setType('om_date_creation','datestatic');

            // Type select
            $form->setType('choix_lien', 'select');

            // Cache le statut
            $form->setType('piece_statut', 'selecthiddenstatic');

            // Si l'utilisateur n'a pas les permsissions
            if ($this->f->isAccredited(array(get_class($this)."_modifier_suivi", get_class($this), ), "OR") == false) {
                //
                $form->setType('suivi', 'checkboxstatic');
                $form->setType('date_butoir', 'datestatic');
                $form->setType('commentaire_suivi', 'textareastatic');
            }

            // Si l'objet est validé ou ouvert en souform
            if ($this->getVal('piece_statut') == $this->get_piece_statut_by_code('VALID') || !empty($this->retourformulaire)) {
                // Cache le champ
                $form->setType('choix_lien', 'hidden');
                // Type link
                $form->setType('etablissement', 'link');
                $form->setType('dossier_coordination', 'link');
                $form->setType('dossier_instruction', 'link');
            } else {
                // Type autocomplete
                $form->setType('etablissement', 'autocomplete');
                $form->setType('dossier_coordination', 'autocomplete');
                $form->setType('dossier_instruction', 'autocomplete');
            }
        }// fin modifier

        // En mode supprimer
        if ($maj==2) { //supprimer
            //
            $form->setType('uid','filestatic');
            // Type select
            $form->setType('choix_lien', 'selectstatic');
            // Cache le champ
            $form->setType('choix_lien', 'hidden');
            // Type link
            $form->setType('etablissement', 'link');
            $form->setType('dossier_coordination', 'link');
            $form->setType('dossier_instruction', 'link');
        }// fin supprimer

        // En mode consulter
        if ($maj==3) {
            //
            $form->setType('uid','file');
            // Cache le champ
            $form->setType('choix_lien', 'hidden');
            // Type link
            $form->setType('etablissement', 'link');
            $form->setType('dossier_coordination', 'link');
            $form->setType('dossier_instruction', 'link');
        }// fin consulter

        // Gestion fieldset suivi
        if (!$this->can_access_suivi($maj)) {
            // On masque les champs
            $form->setType('suivi', 'hidden');
            $form->setType('date_butoir', 'hidden');
            $form->setType('commentaire_suivi', 'hidden');
        }

        // Pour les actions supplémentaires qui utilisent la vue formulaire
        // il est nécessaire de cacher les champs ou plutôt de leur affecter un
        // type pour que l'affichage se fasse correctement
        if ($maj == 4 || $maj == 5 || $maj == 6 || $maj == 7 || $maj == 8 || $maj == 9) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
        }

    }

    /**
     * Mutateur.
     *
     * Permet d'effectuer des appels aux mutateurs spécifiques sur le formulaire
     * de manière fonctionnelle et non en fonction du mutateur. Exemple : au lieu 
     * de gérer le champ service dans les méthodes setType, setSelect, le setLib, 
     * ... Nous allons les gérer dans cette méthode et appeler tous les mutateurs
     * à la suite.
     * 
     * @param resource $form Instance formulaire.
     * @param integer  $maj  Clé de l'action.
     *
     * @return void
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        // Gestion du champ service
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
        // Cas particulier : pièce liée en sousformulaire d'un DI
        if (!empty($this->retourformulaire)
            && in_array($this->retourformulaire, $this->foreign_keys_extended['dossier_instruction'])) {
                // Cache le champ service
                $form->setType('service', 'hidden');
                // Si ajout et ID DI valide
            $idxformulaire = $this->getParameter("idxformulaire");
                if ($maj == 0 && !empty($idxformulaire)) {
                    $form->setVal('service', $this->get_service_by_dossier_instruction($idxformulaire));
                }
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        // Select du champ choix_lien
        $form->setSelect("choix_lien", $choix_lien);

        // Si le formulaire est en mode ajout ou en modifier avec le statut
        // différent de validé
        if ($maj == 0 || ($maj == 1 && $this->getVal('piece_statut') != $this->get_piece_statut_by_code('VALID'))) {

            // Si ce n'est un sous-formulaire d'un établissement
            if (!in_array($this->retourformulaire, $this->foreign_keys_extended['etablissement'])) {
                $params = array();
                // Surcharge visée pour l'ajout
                $params['obj'] = "etablissement_tous";
                // Table de l'objet
                $params['table'] = "etablissement";
                // Permission d'ajouter
                $params['droit_ajout'] = false;
                // Critères de recherche
                $params['criteres'] = array(
                    "etablissement.etablissement" => _("ID"),
                    "etablissement.code" => _("code"),
                    "etablissement.libelle" => _("libelle"),
                    "contact.nom" => _("nom d'un contact")
                );
                // Tables liées
                $params['jointures'] = array(
                    "contact ON etablissement.etablissement = contact.etablissement"
                );
                // Colonnes ID et libellé du champ
                // (si plusieurs pour le libellé alors une concaténation est faite)
                $params['identifiant'] = "etablissement.etablissement";
                $params['libelle'] = array (
                    "etablissement.code",
                    "etablissement.libelle"
                );
                // Envoi des paramètres
                $form->setSelect("etablissement", $params);
            }

            // Si ce n'est un sous-formulaire d'un dossier de coordination
            if (!in_array($this->retourformulaire, $this->foreign_keys_extended['dossier_coordination'])) {
                //
                $params = array();
                // Surcharge visée pour l'ajout
                $params['obj'] = "dossier_coordination";
                // Table de l'objet
                $params['table'] = "dossier_coordination";
                // Permission d'ajouter
                $params['droit_ajout'] = false;
                // Critères de recherche
                $params['criteres'] = array(
                    "dossier_coordination.dossier_coordination" => _("ID"),
                    "dossier_coordination.libelle" => _("libelle")
                );
                // Tables liées
                $params['jointures'] = array();
                // Colonnes ID et libellé du champ
                // (si plusieurs pour le libellé alors une concaténation est faite)
                $params['identifiant'] = "dossier_coordination.dossier_coordination";
                $params['libelle'] = array (
                    "dossier_coordination.libelle"
                );
                // Envoi des paramètres
                $form->setSelect("dossier_coordination", $params);
            }

            // Si ce n'est un sous-formulaire d'un dossier de coordination
            if (!in_array($this->retourformulaire, $this->foreign_keys_extended['dossier_instruction'])) {
                //
                $params = array();
                // Surcharge visée pour l'ajout
                $params['obj'] = "dossier_instruction";
                // Table de l'objet
                $params['table'] = "dossier_instruction";
                // Permission d'ajouter
                $params['droit_ajout'] = false;
                // Critères de recherche
                $params['criteres'] = array(
                    "dossier_instruction.dossier_instruction" => _("ID"),
                    "dossier_instruction.libelle" => _("libelle")
                );
                // Tables liées
                $params['jointures'] = array();
                // Colonnes ID et libellé du champ
                // (si plusieurs pour le libellé alors une concaténation est faite)
                $params['identifiant'] = "dossier_instruction.dossier_instruction";
                $params['libelle'] = array (
                    "dossier_instruction.libelle"
                );
                // Envoi des paramètres
                $form->setSelect("dossier_instruction", $params);
            }
        }

        // En mode "supprimer", "consulter" ou "modifier" avec le statut à "validé" 
        // ou en sousform
        if ($maj == 2 || $maj == 3 || ($maj == 1 && ($this->getVal('piece_statut') == $this->get_piece_statut_by_code('VALID') || !empty($this->retourformulaire)))) {

            // Paramétres envoyés au type link etablissement
            $params = array();
            $params['obj'] = "etablissement_tous";
            $params['idx'] = $this->getVal("etablissement");
            // Instance de la classe etablissement
            require_once '../obj/etablissement.class.php';
            $etablissement = new etablissement($params['idx']);
            $params['libelle'] = $etablissement->getVal("code")." - ".$etablissement->getVal("libelle");
            $form->setSelect("etablissement", $params);

            // Paramétres envoyés au type link dossier_coordination
            $params = array();
            $params['obj'] = "dossier_coordination";
            $params['idx'] = $this->getVal("dossier_coordination");
            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($params['idx']);
            $params['libelle'] = $dossier_coordination->getVal("libelle");
            $form->setSelect("dossier_coordination", $params);

            // Paramétres envoyés au type link dossier_instruction
            $params = array();
            $params['idx'] = $form->val['dossier_instruction'];
            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($params['idx']);
            // Réupère le type du dossier d'instruction
            $dossier_instruction_type = $dossier_coordination->get_dossier_type_code_by_dossier_coordination($form->val['dossier_coordination']);
            // Objet visé
            $obj = "dossier_instruction_tous_visites";
            if ($dossier_instruction_type == "plan") {
                $obj = "dossier_instruction_tous_plans";
            }
            $params['obj'] = $obj;
            $params['libelle'] = $dossier_instruction->getVal("libelle");
            $form->setSelect("dossier_instruction", $params);
        }
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        parent::setLib($form, $maj);
        //
        $form->setLib('uid', _("fichier"));
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object  &$form Formumaire
     * @param integer $maj   Mode d'insertion
     */
    function setOnchange(&$form, $maj) {
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange('choix_lien', 'show_hide_fields_by_piece_choix_lien(this.value)');
        //
        $form->setOnchange('dossier_instruction', 'set_piece_service(this.value)');
    }

    /**
     * Méthode de mise en page.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {
        //
        $col_first_fieldset = "col_10";
        //
        if ($maj == 0 || $maj == 1 || $maj == 2) {
            $col_first_fieldset = "col_12";
        }

        // Fieldset "Informations générales"
        $form->setBloc('piece', 'D', "", $col_first_fieldset);
            $form->setBloc('piece', 'D', "", "col_12");
                $form->setFieldset('piece', 'D', _('Informations generales'), "");
                $form->setFieldset('lu', 'F', '');
            $form->setBloc('lu', 'F');
        $form->setBloc('lu', 'F');

        // Fieldset "Lien"
        $form->setBloc('service', 'D', "", "col_12");
            $form->setBloc('service', 'D', "", "col_12");
                $form->setFieldset('service', 'D', _('Lien'), "");
                $form->setFieldset('dossier_instruction', 'F', '');
            $form->setBloc('dossier_instruction', 'F');
        $form->setBloc('dossier_instruction', 'F');

        // Fieldset "Suivi"
        $form->setBloc('suivi', 'D', "", "col_12");
            $form->setBloc('suivi', 'D', "", "col_12");
                $form->setFieldset('suivi', 'D', _('Suivi'), "");
                $form->setFieldset('commentaire_suivi', 'F', '');
            $form->setBloc('commentaire_suivi', 'F');
        $form->setBloc('commentaire_suivi', 'F');
    }

    /**
     * Selon le mode du formulaire et le droit utilisateur
     * affiche ou masque le fieldset "Suivi".
     * 
     * @param   integer  $maj   Mode du formulaire
     * @return  boolean         Vrai si fieldset affiché
     */
    function can_access_suivi($maj) {
        // Si consultation, ou suppression, ou :
        // ajout ou modification ET droit spécifique
        if ($maj == 2 || $maj == 3 || (
            ($maj == 0 || $maj == 1) &&
            $this->f->isAccredited(array(get_class($this)."_modifier_suivi", get_class($this), ), "OR") == true)) {
            return true;
        }
        return false;
    }

    /**
     * TRIGGER - triggerajouter.
     *
     * - setValF.
     *
     * @return boolean
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * setValF.
         */
        // Le statut de la pièce est "En cours"
        $this->valF['piece_statut'] = $this->get_piece_statut_by_code('ENCOURS');
        // Met à jour le statut de la pièce si besoin
        $this->update_piece_statut();
        // Met à jour les liens de la pièce
        $this->update_links();

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifier.
     *
     * - setValF.
     *
     * @return boolean
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * setValF.
         */
        // Met à jour le statut de la pièce si besoin
        $this->update_piece_statut();
        // Met à jour les liens de la pièce
        $this->update_links();

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - Mise à jour des métadonnées des fichiers stockés.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        /**
         * Mise à jour des métadonnées du fichier stocké.
         */
        // Lorsqu'un enregistrement est modifié (rattaché à un établissement,
        // un DC ou un DI), le fichier stocké n'est pas modifié mais les 
        // métadonnées associées nécessitent d'être mises à jour. La condition
        // nécessaire est qu'il existe un fichier dans les donnnées
        // précédemment envoyées à la base de données.
        if (isset($this->valF['uid'])
            && $this->valF['uid'] != null
            && $this->valF['uid'] != '') {
            $metadata = $this->getMetadata('uid');
            $ret = $this->f->storage->update_metadata(
                $this->valF['uid'],
                $metadata
            );
            if ($ret === OP_FAILURE) {
                $this->addToLog(
                    __METHOD__ . "()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour des métadonnées sur le système de stockage de fichiers [piece.uid]",
                    DEBUG_MODE
                );
                $this->correct = false;
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
        }

        //
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Met à jour le statut de la pièce.
     *
     * @return void
     */
    function update_piece_statut() {
        // Si la piece à la statut en cours
        if ($this->valF['piece_statut'] == $this->get_piece_statut_by_code("ENCOURS")) {
            // Si choix lien n'est pas vide ou si c'est un sous-formulaire
            if (!empty($this->valF['choix_lien'])
                || !empty($this->retourformulaire)) {

                // Si le lien choisi n'est pas vide ou si c'est un sous-formulaire
                if (!empty($this->valF[$this->valF['choix_lien']])
                    || !empty($this->retourformulaire)) {

                    // On met le statut à qualifié
                    $this->valF['piece_statut'] = $this->get_piece_statut_by_code('QUALIF');

                    // Récupère le nom et prénom de l'utilisateur courant
                    $nom_prenom = $this->get_acteur_nom_prenom_by_session_login();

                    // Si l'utilisateur courant est un acteur
                    if (!empty($nom_prenom)) {

                        // Si c'est un cadre
                        if ($this->f->hasActeurRole($nom_prenom, 'cadre')) {

                            // On met le statut à valié directement
                            $this->valF['piece_statut'] = $this->get_piece_statut_by_code('VALID');
                        }
                    }

                } else {

                    // On met le statut à en cours
                    $this->valF['piece_statut'] = $this->get_piece_statut_by_code('ENCOURS');
                }

            } else {

                // On met le statut à en cours
                $this->valF['piece_statut'] = $this->get_piece_statut_by_code('ENCOURS');
            }
        }
    }

    /**
     * Met à jour les liaisons de l'enregistrement.
     * 
     * @return void
     */
    function update_links() {
        // Si un lien est fait avec un dossier d'instruction
        if (!empty($this->valF['dossier_instruction'])) {
            // On récupère le dossier de coordination
            $this->valF['dossier_coordination'] = $this->get_dossier_coordination_by_dossier_instruction($this->valF['dossier_instruction']);
        }

        // Si un lien est fait avec un dossier de coordination
        if (!empty($this->valF['dossier_coordination'])) {
            // On récupère son éventuel établissement
            $etablissement = $this->get_etablissement_by_dossier_coordination($this->valF['dossier_coordination']);
            // S'il y en a un
            if (!empty($etablissement)) {
                // On le lie à la pièce
                $this->valF['etablissement'] = $etablissement;
            }
        }
        //
        return true;
    }

    /**
     * Récupère le dossier de coordination par le dossier d'instruction.
     *
     * @param integer $dossier_instruction_id Identifiant du DI
     *
     * @return integer                        Identifiant du DC
     */
    function get_dossier_coordination_by_dossier_instruction($dossier_instruction_id) {
        // Initialisation de la variable de résultat
        $dossier_coordination = "";

        // Si l'identifiant du dossier d'instruction n'est pas vide
        if (!empty($dossier_instruction_id)) {

            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_instruction_id);

            // Récupère le dossier de coordination
            $dossier_coordination = $dossier_instruction->getVal('dossier_coordination');
        }

        // Retourne le résultat
        return $dossier_coordination;
    }

    /**
     * Récupère le service du dossier d'instruction.
     * 
     * @param integer $dossier_instruction_id Identifiant du DI
     * 
     * @return integer                        Identifiant du service
     */
    function get_service_by_dossier_instruction($dossier_instruction_id) {
        // Initialisation de la variable de résultat
        $service = "";

        // Si l'identifiant du dossier d'instruction n'est pas vide
        if (!empty($dossier_instruction_id)) {

            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_instruction_id);

            // Récupère le service
            $service = $dossier_instruction->getVal('service');
        }

        // Retourne le résultat
        return $service;
    }

    /**
     * Récupère l'établissement par le dossier de coordination.
     *
     * @param integer $dossier_coordination_id Identifiant du DC
     *
     * @return integer                         Identifiant de l'établissement
     */
    function get_etablissement_by_dossier_coordination($dossier_coordination_id) {
        //Initialisation de la variable de résultat
        $etablissement = "";

        // Si l'identifiant du dossier de coordination n'est pas vide
        if (!empty($dossier_coordination_id)) {

            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($dossier_coordination_id);

            // Récupère l'établissement
            $etablissement = $dossier_coordination->getVal('etablissement');
        }

        // Retourne le résultat
        return $etablissement;
    }

    /**
     * CONDITION - is_readable.
     *
     * Condition pour afficher le bouton lu
     *
     * @return boolean
     */
    function is_readable() {
        // Récupère la valeur de lu
        $lu = $this->getVal("lu");

        // Si lu est à false
        if ($lu == "f") {

            // Récupère le nom et prénom de l'utilisateur courant
            $nom_prenom = $this->get_acteur_nom_prenom_by_session_login();

            // Si l'utilisateur courant est un acteur
            if (!empty($nom_prenom)) {

                // Si c'est un technicien
                if ($this->f->hasActeurRole($nom_prenom, 'technicien')) {

                    // Récupère l'id de l'acteur courrant
                    $current_acteur = $this->f->get_acteur_by_session_login();
                    // Récupère l'id de l'acteur du dossier d'instruction
                    $dossier_instruction_acteur = $this->get_acteur_by_dossier_instruction($this->getVal('dossier_instruction'));

                    // Si l'acteur en cours est celui du dossier d'instruction
                    if (!empty($current_acteur) && !empty($dossier_instruction_acteur)
                        && $current_acteur == $dossier_instruction_acteur) {

                        //
                        return true;
                    }

                    //
                    return false;
                }
            }

            // Si l'utilisateur n'est pas dans la table acteur alors c'est un 
            // administrateur
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_unreadable.
     *
     * Condition pour afficher le bouton non lu
     *
     * @return boolean
     */
    function is_unreadable() {
        // Récupère la valeur de lu
        $lu = $this->getVal("lu");

        // Si lu est à false
        if ($lu == "t") {

            // Récupère le nom et prénom de l'utilisateur courant
            $nom_prenom = $this->get_acteur_nom_prenom_by_session_login();

            // Si l'utilisateur courant est un acteur
            if (!empty($nom_prenom)) {

                // Si c'est un technicien
                if ($this->f->hasActeurRole($nom_prenom, 'technicien')) {

                    // Récupère l'id de l'acteur courrant
                    $current_acteur = $this->f->get_acteur_by_session_login();
                    // Récupère l'id de l'acteur du dossier d'instruction
                    $dossier_instruction_acteur = $this->get_acteur_by_dossier_instruction($this->getVal('dossier_instruction'));

                    // Si l'acteur en cours est celui du dossier d'instruction
                    if (!empty($current_acteur) && !empty($dossier_instruction_acteur)
                        && $current_acteur == $dossier_instruction_acteur) {

                        //
                        return true;
                    }

                    //
                    return false;
                }
            }

            // Si l'utilisateur n'est pas dans la table acteur alors c'est un 
            // administrateur
            return true;
        }

        //
        return false;
    }

    /**
     * TREATMENT - read.
     * 
     * Permet de mettre à lu l'enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function read($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_reading("read", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unread.
     * 
     * Permet de metre à non lu l'enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function unread($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_reading("unread", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_reading($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "read" && $mode != "unread") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "read") {
            $valF = array(
                "lu" => 't',
            );
            $valid_message = _("Le %s a ete marque comme lu.");
        }
        if ($mode == "unread") {
            $valF = array(
                "lu" => 'f',
            );
            $valid_message = _("Le %s a ete marque comme non lu.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, _("document entrant"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($valid_message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * CONDITION - is_watchable.
     *
     * Condition pour afficher le bouton suivi
     *
     * @return boolean
     */
    function is_watchable() {
        // Récupère la valeur de suivi
        $suivi = $this->getVal("suivi");

        // Si suivi est à false
        if ($suivi == "f") {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_unwatchable.
     *
     * Condition pour afficher le bouton non suivi
     *
     * @return boolean
     */
    function is_unwatchable() {
        // Récupère la valeur de suivi
        $suivi = $this->getVal("suivi");

        // Si suivi est à true
        if ($suivi == "t") {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * TREATMENT - watch.
     * 
     * Permet de mettre à suivi l'enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function watch($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_watching("watch", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - unwatch.
     * 
     * Permet de mettre à non suivi l'enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function unwatch($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_watching("unwatch", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_watching($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "watch" && $mode != "unwatch") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "watch") {
            $valF = array(
                "suivi" => 't',
            );
            $valid_message = _("Le %s a ete marque comme suivi.");
        }
        if ($mode == "unwatch") {
            $valF = array(
                "suivi" => 'f',
            );
            $valid_message = _("Le %s a ete marque comme non suivi.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, _("document entrant"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($valid_message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * CONDITION - is_validable.
     *
     * Condition pour afficher le bouton suivi
     *
     * @return boolean
     */
    function is_validable() {
        // Récupère la valeur de piece_statut
        $piece_statut = $this->get_piece_statut_code($this->getVal("piece_statut"));

        // Si piece_statut est à false
        if ($piece_statut == "QUALIF") {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * CONDITION - is_cancelable.
     *
     * Condition pour afficher le bouton non piece_statut
     *
     * @return boolean
     */
    function is_cancelable() {
        // Récupère la valeur de piece_statut
        $piece_statut = $this->get_piece_statut_code($this->getVal("piece_statut"));

        // Si piece_statut est à true
        if ($piece_statut == "VALID") {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * TREATMENT - valid.
     * 
     * Permet de valider l'enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function valid($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_validing("valid", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - cancel.
     * 
     * Permet d'annuler la validation d'un enregistrement.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function cancel($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_validing("cancel", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_validing($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "valid" && $mode != "cancel") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "valid") {
            $valF = array(
                "piece_statut" => $this->get_piece_statut_by_code("VALID"),
            );
            $valid_message = _("Le %s a ete valide.");
        }
        if ($mode == "cancel") {
            $valF = array(
                "piece_statut" => $this->get_piece_statut_by_code("QUALIF"),
            );
            $valid_message = _("La validition du %s a ete annulee.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, _("document entrant"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * Récupère le nom et prenom de l'utilisateur en cours.
     *
     * @return string
     */
    function get_acteur_nom_prenom_by_session_login() {
        //
        $acteur_id = $this->f->get_acteur_by_session_login();

        //
        $nom_prenom = "";
        //
        if (!empty($acteur_id)) {
            // Instance de la classe acteur
            require_once '../obj/acteur.class.php';
            $acteur = new acteur($acteur_id);

            //
            $nom_prenom = $acteur->getVal('nom_prenom');
        }

        //
        return $nom_prenom;
    }

    /**
     * Récupère l'acteur du dossier d'instruction.
     *
     * @param integer $dossier_instruction_id Identifiant du DI
     *
     * @return integer
     */
    function get_acteur_by_dossier_instruction($dossier_instruction_id) {
        //
        $acteur = "";
        //
        if (!empty($dossier_instruction_id)) {
            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_instruction_id);

            //
            $acteur = $dossier_instruction->getVal('technicien');
        }

        //
        return $acteur;
    }

    /**
     * Récupère le code du statut de la pièce.
     *
     * @param integer $piece_statut_id Identifiant de la pièce.
     *
     * @return string
     */
    function get_piece_statut_code($piece_statut_id) {
        //
        $piece_statut_code = "";

        // Si le code n'est pas vide
        if (!empty($piece_statut_id)) {
            // Requête SQL
            $sql = "SELECT code
                    FROM ".DB_PREFIXE."piece_statut
                    WHERE piece_statut=".intval($piece_statut_id)."";
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $piece_statut_code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($piece_statut_code);
        }

        //
        return $piece_statut_code;
    }

    /**
     * Récupère le statut de la pièce par son code.
     *
     * @param string $piece_statut_code Code du statut de pièce
     *
     * @return integer
     */
    function get_piece_statut_by_code($piece_statut_code) {
        //
        $piece_statut_id = "";

        // Si le code n'est pas vide
        if (!empty($piece_statut_code)) {
            // Requête SQL
            $sql = "SELECT piece_statut
                    FROM ".DB_PREFIXE."piece_statut
                    WHERE code = '".$this->f->db->escapeSimple($piece_statut_code)."'";
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $piece_statut_id = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($piece_statut_id);
        }

        //
        return $piece_statut_id;
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_document_entrant_type($document_entrant_type = null) {
        //
        if ($document_entrant_type == null
            && isset($this->valF["piece_type"])
            && $this->valF["piece_type"] != "") {
            $document_entrant_type = $this->valF["piece_type"];
        }
        //
        return $this->get_inst_common("piece_type", $document_entrant_type);
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if ($etablissement == null
            && isset($this->valF["etablissement"])
            && $this->valF["etablissement"] != "") {
            $etablissement = $this->valF["etablissement"];
        }
        //
        return $this->get_inst_common("etablissement", $etablissement);
    }

    /**
     *
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if ($dossier_coordination == null
            && isset($this->valF["dossier_coordination"])
            && $this->valF["dossier_coordination"] != "") {
            $dossier_coordination = $this->valF["dossier_coordination"];
        }
        //
        return $this->get_inst_common("dossier_coordination", $dossier_coordination);
    }

    /**
     *
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        if ($dossier_instruction == null
            && isset($this->valF["dossier_instruction"])
            && $this->valF["dossier_instruction"] != "") {
            $dossier_instruction = $this->valF["dossier_instruction"];
        }
        //
        return $this->get_inst_common("dossier_instruction", $dossier_instruction);
    }

    // }}} END - GET INST

    // {{{ BEGIN - METADATA FILESTORAGE

    /**
     *
     */
    var $metadata = array(
        "uid" => array(
            //
            "titre" => "get_md_titre",
            "description" => "get_md_description",
            //
            "origine" => "get_md_origine_televerse",
            //
            "etablissement_code" => "get_md_etablissement_code",
            "etablissement_libelle" => "get_md_etablissement_libelle",
            "etablissement_siret" => "get_md_etablissement_siret",
            "etablissement_referentiel" => "get_md_etablissement_referentiel",
            "etablissement_exploitant" => "get_md_etablissement_exploitant",
            //
            "etablissement_adresse_numero" => "get_md_etablissement_adresse_numero",
            "etablissement_adresse_mention" => "get_md_etablissement_adresse_mention",
            "etablissement_adresse_voie" => "get_md_etablissement_adresse_voie",
            "etablissement_adresse_cp" => "get_md_etablissement_adresse_cp",
            "etablissement_adresse_ville" => "get_md_etablissement_adresse_ville",
            "etablissement_adresse_arrondissement" => "get_md_etablissement_adresse_arrondissement",
            //
            "etablissement_ref_patrimoine" => "get_md_etablissement_ref_patrimoine",
            //
            "dossier_coordination" => "get_md_dossier_coordination",
            "dossier_instruction" => "get_md_dossier_instruction",
        ),
    );

    function get_md_titre() {
        $titre = $this->get_md_titre_common();
        $inst_docent_type = $this->get_inst_document_entrant_type();
        if ($inst_docent_type->getVal("libelle") !== "") {
            $titre .= $inst_docent_type->getVal("libelle");
        } else {
            $titre .= "document entrant";
        }
        return $titre;
    }
    function get_md_description() { return "document entrant numérisé"; }

    // }}} END - METADATA FILESTORAGE

}

?>
