<?php
/**
 * Surcharge de la classe etablissement pour les traitements des établissements
 * qui ne sont pas des voies publiques.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/etablissement.class.php");
require_once ("../obj/contact.class.php");

class etablissement_tous extends etablissement {

    public $exploitant;

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);

        // Création de l'exploitant suivant l'id de l'établissement
        $this->exploitant = new contact($this->recuperer_ID_exploitant($id));
    }// fin constructeur

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {
        
        parent::init_class_actions();

        // ACTION - 031 - Redirection vers le SIG pour la sélection d'établissements
        $this->class_actions[31] = array(
            "identifier" => "localiser_selection",
            "view" => "view_localiser_selection",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );
    }


    /**
     * Permet de définir le type des champs
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        //
        // Si on n'est pas en mode "ajout"
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            // On récupère l'éventuelle date d'archivage
            $om_validite_fin = $this->getVal("om_validite_fin");
            $form->setType('etablissement', 'hidden');
        }

        // En mode "ajout"
        if ($maj == 0) {

            // On cache la date d'archivage
            $form->setType('om_validite_fin', 'hiddendate');
            
            // Cache le champ "etablissement_tutelle_adm"
            $form->setType('etablissement_tutelle_adm', 'selecthidden');
            // Cache le champ "ref_patrimoine"
            $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
        }

        // En mode "modification"
        if ($maj == 1) {

            $etablissement_statut_juridique_code = 
                $this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique"));

            // On cache le champ "etablissement_tutelle_adm" si statut juridique différent de public
            if ($etablissement_statut_juridique_code != 'pub') {
                $form->setType('etablissement_tutelle_adm', 'selecthidden');
            }
            
            // On cache le champ "ref_patrimoine" si statut juridique différent de ville
            if ($etablissement_statut_juridique_code != 'vle') {
                $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
            }
            else{
                $form->setType('ref_patrimoine', 'referentielpatrimoine');
            }

            // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
            if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
                // On rend non modifiable les dates de création et d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddenstaticdate');
            }
            else{ // si désarchivé
                // On rend non modifiable la date de création et masque celle d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddendate');
            }
        }

        // En modes "ajout" et "modification"
        if ($maj == 0 || $maj == 1) {

            //
            $form->setType('references_cadastrales','referencescadastrales');

            // Transformation des champs de type booléen en select
            $form->setType("exp_civilite", 'select');
            $form->setType("si_conformite_l16", 'select');
            $form->setType("si_alimentation_remplacement", 'select');
            $form->setType("si_service_securite", 'select');

            //
            $form->setType("exp_adresse_voie", 'text');

            // On cache le message qu'il n'y a aucun exploitant
            $form->setType('exp','hidden');

            // On type la copie d'adresse en case à cocher
            $form->setType('meme_adresse','checkbox');
            // et son interligne pour l'affichage
            $form->setType('exp_blank','static');

            // On cache le champ de la durée de la périodicité des visites
            $form->setType('si_periodicite_visites', 'hidden');
            // Le suivi des dates se fait automatiquement
            $form->setType("si_derniere_visite_date", 'hiddendate');
            $form->setType("si_derniere_visite_avis", 'hidden');
            $form->setType("si_derniere_visite_technicien", 'hidden');
            $form->setType("si_prochaine_visite_date", 'hiddendate');
            $form->setType("si_prochaine_visite_type", 'hidden');
            $form->setType("acc_derniere_visite_date", 'hidden');
            $form->setType("acc_derniere_visite_avis", 'hidden');
            $form->setType("acc_derniere_visite_technicien", 'hidden');
            //
            $form->setType('dossier_coordination_periodique', 'hidden');
            //
            $form->setType('date_arrete_ouverture', 'hiddendate');
        }

        // En mode consultation
        if ($maj == 3) {
            //
            $form->setType('dossier_coordination_periodique', 'link');
        }

        // En mode "consultation" et "suppression"
        if ($maj == 2 || $maj == 3) {

            $etablissement_statut_juridique_code = 
                $this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique"));

            // On cache le champ "etablissement_tutelle_adm" si statut juridique différent de public
            if ($etablissement_statut_juridique_code != 'pub') {
              $form->setType('etablissement_tutelle_adm', 'selecthidden');
            }

            // On cache le champ "ref_patrimoine" si statut juridique différent de ville
            if ($etablissement_statut_juridique_code != 'vle') {
                $form->setType('ref_patrimoine', 'referentielpatrimoinehidden');
            }
            else{
                $form->setType('ref_patrimoine', 'referentielpatrimoinestatic');
            }

            // On cache la copie d'adresse et son interligne
            $form->setType('meme_adresse','hidden');
            $form->setType('exp_blank','hidden');

            // Transformation des champs de type booléen en select statique
            $form->setType('references_cadastrales','referencescadastralesstatic');
            $form->setType("exp_civilite", 'selectstatic');
            $form->setType("si_conformite_l16", 'selectstatic');
            $form->setType("si_alimentation_remplacement", 'selectstatic');
            $form->setType("si_service_securite", 'selectstatic');

            //
            $form->setType("exp_adresse_voie", 'textstatic');
            if ($this->getVal('geolocalise') === 't') {
                $form->setType('geolocalise', 'link_geolocalisation');
            }

            // On cache le message qu'il n'y a aucun exploitant
            // s'il y en a un
            if ($this->recuperer_ID_exploitant($this->getVal('etablissement')) != "]") {

                $form->setType('exp','hidden');
            } else { // sinon on cache les champs exploitant

                $form->setType('exp_civilite','hidden');
                $form->setType('exp_nom','hidden');
                $form->setType('exp_prenom','hidden');
                $form->setType('exp_adresse_numero','hidden');
                $form->setType('exp_adresse_numero2','hidden');
                $form->setType('exp_adresse_voie','hidden');
                $form->setType('exp_adresse_complement','hidden');
                $form->setType('exp_adresse_cp','hidden');
                $form->setType('exp_adresse_ville','hidden');
                $form->setType('exp_lieu_dit','hidden');
                $form->setType('exp_boite_postale','hidden');
                $form->setType('exp_cedex','hidden');
                $form->setType('exp_pays','hidden');
            }

            // S'il y a une date de validité de fin et qu'elle est antérieure ou égale au jour en cours c'est que l'état est archivé
            if ($om_validite_fin != NULL && $om_validite_fin <= date('Y-m-d')) {
                // On rend non modifiable les dates de création et d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddenstaticdate');
            }
            else{ // si désarchivé
                // On rend non modifiable la date de création et masque celle d'archivage
                $form->setType('om_validite_debut', 'hiddenstaticdate');
                $form->setType('om_validite_fin', 'hiddendate');
            }

        }
        // On affiche les champs calculés des UA uniquement en mode consultation
        // et si il y a des UA sur l'établissement.
        if ($maj != 3) {
            $form->setType('tot_ua_valid', 'hidden');
        }
        if ($this->get_tot_ua_valid() === 0 || $maj !=3) {
            $form->setType('acc_handicap_auditif', 'hidden');
            $form->setType('acc_handicap_mental', 'hidden');
            $form->setType('acc_handicap_physique', 'hidden');
            $form->setType('acc_handicap_visuel', 'hidden');
            $form->setType('acc_derogation_scda', 'hidden');
        }
    }

    /*
     * Ajoute l'action javascript...
     */
    function setOnchange(&$form,$maj){
        parent::setOnchange($form,$maj);
        
        // ... sur le contrôle du statut
        $form->setOnchange('etablissement_statut_juridique','verifierStatutJuridique(this.value);');

        // ... sur la copie d'adresse
        $form->setOnchange('meme_adresse','copierAdresseEtablissement(this.value);');

        // ... sur le changement de nature
        $form->setOnchange('etablissement_nature','masquerPeriodiciteVisites();');

        // ... sur le changement de type
        $form->setOnchange('etablissement_type','masquerPeriodiciteVisites();');
    }

    // Mise en page
    function setLayout(&$form, $maj) {
        //
        $form->setBloc('acc_consignes_om_html','D',"","form-etablissement-action-".$maj);

        // Organisation en blocs horizontaux
        // subdivisés blocs répartis sur 2 colonnes
        
        //
        // Détails sécurité et accessibilité
        //
        
        if ( $maj == 3) { // Si mode consulter on réduit la largeur pour le portlet
        $form->setBloc('acc_consignes_om_html','D',"","col_9");
        } else {
        $form->setBloc('acc_consignes_om_html','D',"","col_12");
        }
            // Fieldset replié Détails
            $form->setFieldset('acc_consignes_om_html','D',_('Details'), "startClosed");

                // COL 1 : Accessibilité
                $form->setBloc('acc_consignes_om_html','D',"","col_6");
                    $form->setBloc('acc_consignes_om_html','D',"","col_12");
                    $form->setFieldset('acc_consignes_om_html','D',_('Accessibilite'));
                    $form->setFieldset('acc_derogation_scda','F','');
                    $form->setBloc('acc_derogation_scda','F','');
                $form->setBloc('acc_derogation_scda','F','');

                // COL 2 : Sécurité
                $form->setBloc('si_consignes_om_html','D',"","col_6");
                    $form->setBloc('si_consignes_om_html','D',"","col_12");
                    $form->setFieldset('si_consignes_om_html','D',_('Securite'));
                    $form->setFieldset('si_personnel_nuit','F','');
                    $form->setBloc('si_personnel_nuit','F','');
                $form->setBloc('si_personnel_nuit','F','');

            $form->setFieldset('si_personnel_nuit','F','');
        $form->setBloc('si_personnel_nuit','F','');

        // Fin "Détails sécurité et accessibilité"

        //
        // Fiche établissement
        //
        
        if ($maj >= 2) {
            $form->setBloc('etablissement','D',"","col_12");
        } else {
            $form->setBloc('etablissement','D',"","col_12");
        }
        $form->setFieldset('etablissement','D',_('Fiche etablissement'),"collapsible");

            $form->setBloc('etablissement','D',"","col_6"); // COL 1

            // Fieldset établissement
            $form->setBloc('etablissement','D',"","col_12");
            $form->setFieldset('etablissement','D',_('Etablissement'),"");
                if($maj >= 2) {
                    $form->setBloc('adresse_numero','D',_('Adresse'),'');
                        $form->setBloc('adresse_numero','D','','group');
                        $form->setBloc('adresse_voie','F');
                        
                        $form->setBloc('adresse_complement','DF');
                        
                        $form->setBloc('lieu_dit','D','','group');
                        $form->setBloc('boite_postale','F');
                        $form->setBloc('adresse_cp','D','','group');
                        $form->setBloc('cedex','F');
                    $form->setBloc('cedex','F');

                }
                if ($maj >= 2) {
                    $form->setBloc('etablissement_statut_juridique','D',_('etablissement_statut_juridique'));
                        // gestion de la mauvaise gestion du groupe s'il est fermé
                        // par un champ caché, ce qui est le cas
                        // lorsque l'établissement est public
                        if ($this->get_etablissement_statut_juridique_code($this->getVal("etablissement_statut_juridique")) == 'pub') {
                            $form->setBloc('etablissement_statut_juridique','D','','group etablissement_tutelle_adm'.$this->form->val["etablissement_tutelle_adm"]);
                            $form->setBloc('etablissement_tutelle_adm','F');
                        }
                        
                    $form->setBloc('etablissement_tutelle_adm','F');
                }
            $form->setFieldset('etablissement_tutelle_adm','F','');
            $form->setBloc('etablissement_tutelle_adm','F','');
            
            // Fieldset exploitant
            $form->setBloc('exp','D',"","col_12");
            $form->setFieldset('exp','D',_('Exploitant'),'');
                if($maj >= 2) {
                    $form->setBloc('exp_civilite','D',_('Identite'));
                        $form->setBloc('exp_civilite', 'D', '', 'group');
                        $form->setBloc('exp_prenom','F');
                    $form->setBloc('exp_prenom','F');
                    $form->setBloc('exp_adresse_numero','D',_('Adresse'),'');
                        $form->setBloc('exp_adresse_numero','D','','group');
                        $form->setBloc('exp_adresse_voie','F');
                        //
                        $form->setBloc('exp_adresse_complement','D');
                        $form->setBloc('exp_adresse_complement','F');
                        //
                        $form->setBloc('exp_lieu_dit','D','','group');
                        $form->setBloc('exp_boite_postale','F');
                        //
                        $form->setBloc('exp_adresse_cp','D','','group');
                        $form->setBloc('exp_cedex','F');
                        //
                        $form->setBloc('exp_pays','D');
                        $form->setBloc('exp_pays','F');
                    $form->setBloc('exp_pays','F');
                }
            $form->setFieldset('exp_pays','F','');
            $form->setBloc('exp_pays','F');

            // Fieldset localisation
            $form->setBloc('references_cadastrales','D',"","col_12");
            $form->setFieldset('references_cadastrales','D',_('Localisation'),'');
            $form->setFieldset('geolocalise','F');
            $form->setBloc('geolocalise','F');

            $form->setBloc('geolocalise','F'); // FIN COL 1

            $form->setBloc('etablissement_type','D',"","col_6"); // COL 2

            // Fieldset statut
            $form->setBloc('etablissement_type','D',"","col_12");
            $form->setFieldset('etablissement_type','D',_('Statut'),"");
                if($maj >= 2) {
                  $form->setBloc('etablissement_type','D',_('statut réglementaire'));
                    $form->setBloc('etablissement_type','D','','group floatleft etablissement_etat'.$this->form->val["etablissement_etat"]);
                    $form->setBloc('etablissement_etat','F');
                  $form->setBloc('etablissement_etat','F');
                }
            $form->setFieldset('om_validite_fin','F','');
            $form->setBloc('om_validite_fin','F');

            // Fieldset détail technique
            $form->setBloc('si_effectif_public','D',"","col_12"); 
            $form->setFieldset('si_effectif_public','D',_('Detail technique'),"");
                if($maj >= 2) {
                  $form->setBloc('si_effectif_public','D',_('effectif'),'');
                    $form->setBloc('si_effectif_public','D','','group floatleft');
                    $form->setBloc('si_effectif_personnel','F');
                  $form->setBloc('si_effectif_personnel','F');
                }
            $form->setFieldset('si_visite_duree','F','');
            $form->setBloc('si_visite_duree','F');

            // Fieldset suivi des visites
            $form->setBloc('si_periodicite_visites','D',"","col_12"); 
            $form->setFieldset('si_periodicite_visites','D',_('Suivi des visites'),"");
                // Visites sécurité
                $form->setBloc('si_periodicite_visites','D',_("Visites securite"));
                $form->setBloc('si_prochaine_visite_type','F');
                // Visites accessibilité
                $form->setBloc('acc_derniere_visite_date','D',_("Visites accessibilite"));
                $form->setBloc('acc_derniere_visite_technicien','F');
            $form->setFieldset('acc_derniere_visite_technicien','F','');
            $form->setBloc('acc_derniere_visite_technicien','F');

            $form->setBloc('acc_derniere_visite_technicien','F'); // FIN COL 2

        $form->setFieldset('acc_derniere_visite_technicien','F');
        $form->setBloc('acc_derniere_visite_technicien','F');
        //
        $form->setBloc(end($this->champs), 'F');
    }
    
    // Libellés des champs
    function setLib(&$form,$maj) {
        
        parent::setLib($form,$maj);

        $form->setLib('exp','');
        $form->setLib('exp_blank','');
        $form->setLib('meme_adresse',_("Meme adresse que celle de l'etablissement"));
        $form->setLib('om_validite_debut', _("Date de creation"));
        $form->setLib('om_validite_fin', _("Date d'archivage"));
        $form->setLib('acc_handicap_auditif', _("acc_handicap_auditif"));
        $form->setLib('acc_handicap_mental', _("acc_handicap_mental"));
        $form->setLib('acc_handicap_physique', _("acc_handicap_physique"));
        $form->setLib('acc_handicap_visuel', _("acc_handicap_visuel"));
        $form->setLib('acc_derogation_scda', _("acc_derogation_scda"));
        $form->setLib('geolocalise', _('Géolocalisé'));
        
        // En modes "ajouter" et "modifier" ainsi que recherche avancée
        if ($maj == 999 || $maj == 0 || $maj == 1) {

            $form->setLib('exp_civilite', _('exp_civilite'));
            $form->setLib('exp_nom', _('exp_nom'));
            $form->setLib('exp_prenom', _('exp_prenom'));
            $form->setLib('exp_adresse_numero', _('exp_adresse_numero'));
            $form->setLib('exp_adresse_numero2', _('exp_adresse_numero2'));
            $form->setLib('exp_adresse_voie', _('exp_adresse_voie'));
            $form->setLib('exp_adresse_complement', _('exp_adresse_complement'));
            $form->setLib('exp_adresse_cp', _('exp_adresse_cp'));
            $form->setLib('exp_adresse_ville', _('exp_adresse_ville'));
            $form->setLib('exp_lieu_dit', _('exp_lieu_dit'));
            $form->setLib('exp_boite_postale', _('exp_boite_postale'));
            $form->setLib('exp_cedex', _('exp_cedex'));
            $form->setLib('exp_pays', _('exp_pays'));
        }

        // En modes "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {

            $form->setLib('etablissement_etat','');
            $form->setLib('etablissement_statut_juridique','');
            $form->setLib('adresse_numero','');
            $form->setLib('adresse_numero2','');
            $form->setLib('adresse_voie','');
            $form->setLib('adresse_complement','');
            $form->setLib('adresse_arrondissement','');
            $form->setLib('adresse_cp','');
            $form->setLib('adresse_ville','');
            $form->setLib('lieu_dit','');
            $form->setLib('cedex','');
            $form->setLib('boite_postale','');
            $form->setLib('exp_civilite','');
            $form->setLib('exp_nom','');
            $form->setLib('exp_prenom','');
            $form->setLib('exp_adresse_numero','');
            $form->setLib('exp_adresse_numero2','');
            $form->setLib('exp_adresse_voie','');
            $form->setLib('exp_adresse_complement','');
            $form->setLib('exp_adresse_cp','');
            $form->setLib('exp_adresse_ville','');
            $form->setLib('exp_lieu_dit','');
            $form->setLib('exp_boite_postale','');
            $form->setLib('exp_cedex','');
            $form->setLib('exp_pays','');
            $form->setLib('tot_ua_valid',_("Unites d'accessibilite"));
        }
    }

    /**
     *
     */
    function setVal(&$form, $maj, $validation, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setVal($form, $maj, $validation);
        //
        if ($validation == 0 && $maj == 0) {
                
            $form->setVal("adresse_ville", $this->f->getParameter("ville"));
            $form->setVal("exp_pays", "France");
            $form->setVal("om_validite_debut", date('Y-m-d'));
            // modifier après
            //  type exploitant
            //$this->exploitant->setVal('contact_type', 1);
            // attaché à l'établissement en cours
            //$this->exploitant->setVal('etablissement', $this->getVal('etablissement'));
        }

        if ($maj > 0) {

            // Si un exploitant est attaché à l'établissement
            if ($this->recuperer_ID_exploitant($this->getVal('etablissement')) != "]") {

                $tempBP = $this->exploitant->getVal('boite_postale');
                $tempCEDEX = $this->exploitant->getVal('cedex');

                $form->setVal("exp_civilite", $this->exploitant->getVal('civilite'));
                $form->setVal("exp_nom", $this->exploitant->getVal('nom'));
                $form->setVal("exp_prenom", $this->exploitant->getVal('prenom'));
                $form->setVal("exp_adresse_numero", $this->exploitant->getVal('adresse_numero'));
                $form->setVal("exp_adresse_numero2", $this->exploitant->getVal('adresse_numero2'));
                $form->setVal("exp_adresse_voie", $this->exploitant->getVal('adresse_voie'));
                $form->setVal("exp_adresse_complement", $this->exploitant->getVal('adresse_complement'));
                $form->setVal("exp_adresse_cp", $this->exploitant->getVal('adresse_cp'));
                $form->setVal("exp_adresse_ville", $this->exploitant->getVal('adresse_ville'));
                $form->setVal("exp_lieu_dit", $this->exploitant->getVal('lieu_dit'));
                $form->setVal("exp_pays", $this->exploitant->getVal('pays'));
                
                if ($maj >= 2) {
                    if ($tempCEDEX != '') {
                        $form->setVal('exp_cedex', _('Cedex').' '.$tempCEDEX);
                    }
                    if ($tempBP != '') {
                        $form->setVal('exp_boite_postale', _('BP').' '.$tempBP);
                    }
                }  else {
                    $form->setVal("exp_boite_postale", $tempBP);
                    $form->setVal("exp_cedex", $tempCEDEX);
                }
            }       
        }

        if ($maj >= 2) {
            if ($form->val['cedex'] != '') {
                $form->setVal('cedex', _('Cedex').' '.$form->val['cedex']);
            }
            if ($form->val['boite_postale'] != '') {
                $form->setVal('boite_postale', _('BP').' '.$form->val['boite_postale']);
            }
        }  
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Transformation des champs de type booléen en select oui/non
        // au lieu de la case à cocher
        $form->setSelect("si_conformite_l16", $booleens);
        $form->setSelect("si_alimentation_remplacement", $booleens);
        $form->setSelect("si_service_securite", $booleens);
        
        //Option du référentiel patrimoine
        $option_referentiel_patrimoine = array(
            "option_referentiel_patrimoine" => $this->f->getParameter("option_referentiel_patrimoine")
        );
        // Gestion du bouton  
        $form->setSelect("ref_patrimoine", $option_referentiel_patrimoine);

        // Exploitant
        if(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->exploitant->table.".form.inc.php"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->exploitant->table.".form.inc.php");
        elseif(file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->exploitant->table.".form.inc"))
            include ("../sql/".OM_DB_PHPTYPE."/".$this->exploitant->table.".form.inc");

        $this->init_select($form, $this->f->db, $maj, null, "exp_civilite",
                           $sql_civilite, $sql_civilite_by_id, false);

        // En mode consulter
        if ($maj == 3) {
            // Paramètres envoyés au type link du champ
            // dossier_coordination_periodique
            $params = array();
            $params['obj'] = "dossier_coordination";
            $params['idx'] = $this->getVal('dossier_coordination_periodique');
            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($params['idx']);
            $params['libelle'] = $dossier_coordination->getVal("libelle");
            $form->setSelect("dossier_coordination_periodique", $params);
        }
    }

    /**
     * Méthode de traitement des données retournées par le formulaire
     */
    function setvalF($val = array()) {

        parent::setvalF($val);

        // Ajoute la périodicité par rapport au type et à la catégorie de 
        // l'établissement
        $this->valF['si_periodicite_visites'] = $this->getEtablissementPeriodicite($this->valF['etablissement_type'], 
            $this->valF['etablissement_categorie'], $this->valF['si_locaux_sommeil']);

        // Surcharge des booléens transformés en select pour accepter le null
        switch ($val['si_conformite_l16']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_conformite_l16'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_conformite_l16'] = null;
            break;

            default:
            $this->valF['si_conformite_l16'] = false;
            break;
        }
        switch ($val['si_alimentation_remplacement']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_alimentation_remplacement'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_alimentation_remplacement'] = null;
            break;

            default:
            $this->valF['si_alimentation_remplacement'] = false;
            break;
        }
        switch ($val['si_service_securite']) {

            case 1:
            case "t":
            case "Oui":
            case "true":
            $this->valF['si_service_securite'] = true;
            break;

            case '':
            case "null":
            case "NULL":
            $this->valF['si_service_securite'] = null;
            break;

            default:
            $this->valF['si_service_securite'] = false;
            break;
        }
    }

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Ajout des parcelles dans la table etablissement_parcelle
        $this->ajouter_etablissement_parcelle($this->valF[$this->clePrimaire], 
            $val['references_cadastrales']);

        // Si au moins un champ exploitant est renseigné
        if ($this->exploitant_saisi($val) == true) {

            // On le crée
            $this->ajouter_exploitant($val);
        }

        if ($this->is_option_sig_enabled() === true) {
            // Tentative de géolocalisation de l'établissement et affichage d'un message à l'utilisateur
            // Si l'établissement n'est pas géocodé il doit quand même pouvoir être créé.
            $this->geocoder();
        }
    }

    /**
     *
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // On supprime toutes les lignes de la table etablissement_parcelle
        // qui font référence à l'établissement en cours de modification
        $this->supprimer_etablissement_parcelle($val['etablissement']);

        // Ajout des parcelles dans la table etablissement_parcelle
        $this->ajouter_etablissement_parcelle($val['etablissement'], 
            $val['references_cadastrales']);

        // S'il existe déjà un exploitant attaché à l'établissement
        if ($this->recuperer_ID_exploitant($val['etablissement']) != "]") {
            // S'il s'agit d'une modification en contexte formulaire
            if (isset($val["exp_nom"])) {
                // Si au moins un champ exploitant est renseigné
                if ($this->exploitant_saisi($val) == true) {
                    // On le modifie
                    $this->modifier_exploitant($val);
                } else {
                    // Sinon on le supprime
                    $this->supprimer_exploitant();
                }
            }
            // Sinon on ne fait rien (maj données techniques)
        } else {
            // S'il s'agit d'une modification en contexte formulaire
            if (isset($val["exp_nom"])) {
                // Si au moins un champ exploitant est renseigné
                if ($this->exploitant_saisi($val) == true) {
                    // On le crée
                    $this->ajouter_exploitant($val);
                }
            }
        }
    }

    /**
     *
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // On supprime toutes les lignes de la table etablissement_parcelle
        // qui font référence à l'établissement en cours de suppression
        $this->supprimer_etablissement_parcelle($val['etablissement']);

    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        //
        parent::verifier($val);
        // Vérification du numéro de SIRET
        $siret = $this->valF['siret'];
        // S'il y a un numéro on le vérifie
        if ($siret != '') {

            // S'il est invalide on affiche l'erreur
            if (!$this->f->checkLuhn($siret)) {

                $this->correct = false;
                $this->addToMessage(_("Le numero de SIRET est invalide."));
            }
        }
    }

    /**
     * Récupère la périodicité par rapport au type et à la catégorie de 
     * l'établissement
     * @param  integer $etablissement_type      Type de l'établissement
     * @param  integer $etablissement_categorie Catégorie de l'établissement
     * @return integer                          Périodicité
     */
    function getEtablissementPeriodicite($etablissement_type, $etablissement_categorie, $locaux_sommeil) {

      // Initialisation du résultat
      $si_periodicite_visites = null;

      // Vérifie si les paramètres ne sont pas vide
      if ($etablissement_type != "" && $etablissement_categorie != "") {

        // Requête SQL
        $sql = "SELECT periodicite_visites
                FROM ".DB_PREFIXE."periodicite_visites
                WHERE etablissement_type = $etablissement_type
                  AND etablissement_categorie = $etablissement_categorie";
        if($locaux_sommeil == 1){
          $sql .= " AND avec_locaux_sommeil IS TRUE";
        } else {
          $sql .= " AND sans_locaux_sommeil IS TRUE";
        }

        $this->f->addToLog("getEtablissementPeriodicite() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        $si_periodicite_visites = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($si_periodicite_visites);

      }

      // Retourne résultat
      return $si_periodicite_visites;
      
    }

    /**
     * Ajoute les parcelles de l'établissement passé en paramètre.
     * 
     * @param string $etablissement                Identifiant de l'établissement
     * @param string $references_cadastrales Références cadastrales
     */
    function ajouter_etablissement_parcelle($etablissement, $references_cadastrales) {

        // Parse les parcelles
        $list_parcelles = $this->f->parseParcelles($references_cadastrales);

        // Fichier requis
        require_once "../obj/etablissement_parcelle.class.php";

        // A chaque parcelle une nouvelle ligne est créée dans la table
        // etablissement_parcelle
        foreach ($list_parcelles as $parcelle) {

            // Instance de la classe etablissement_parcelle
            $etablissement_parcelle = new etablissement_parcelle("]", $this->f->db, DEBUG);

            // Valeurs à sauvegarder
            $value = array(
                'etablissement_parcelle' => '',
                'etablissement' => $etablissement,
                'ref_cadastre' => $parcelle['quartier']
                                .$parcelle['section']
                                .$parcelle['parcelle']
            );

            // Ajout de la ligne
            $etablissement_parcelle->ajouter($value, $this->f->db, DEBUG);
        }
    }

    /**
     * Supprime les parcelles de l'établissement passé en paramètre.
     * 
     * @param string $etablissement Identifiant de l'etablissement
     */
    function supprimer_etablissement_parcelle($etablissement) {

        // Suppression des parcelles de l'établissement
        $sql = "DELETE FROM ".DB_PREFIXE."etablissement_parcelle
                WHERE etablissement='".$etablissement."'";
        $res = $this->f->db->query($sql);
        $this->addToLog("supprimer_etablissement_parcelle() db->query(\"".$sql."\");",
            VERBOSE_MODE);
        database::isError($res);
    }

    /**
     * Retourne vrai si au moins un champ exploitant a été renseigné.
     */
    function exploitant_saisi($champs) {

        if ($champs["exp_civilite"] != '' ||
            $champs["exp_nom"] != '' ||
            $champs["exp_prenom"] != '' ||
            $champs["exp_adresse_numero"] != '' ||
            $champs["exp_adresse_numero2"] != '' ||
            $champs["exp_adresse_voie"] != '' ||
            $champs["exp_adresse_complement"] != '' ||
            $champs["exp_adresse_cp"] != '' ||
            $champs["exp_adresse_ville"] != '' ||
            $champs["exp_lieu_dit"] != '' ||
            $champs["exp_boite_postale"] != '' ||
            $champs["exp_cedex"] != '' ||
            $champs["exp_pays"] != '') {

            return true;
        } else {

            return false;
        }
    }

    /**
     * Crée un exploitant
     */
    function ajouter_exploitant($champs) {

        // Récupération des champs
        $valF = $this->recuperation_champs_exploitant($champs);

        // Définition de la clé primaire
        $valF['contact'] = "";

        // Définition de la qualité particulier
        $valF['qualite'] = "particulier";

        // Ajout dans la table contact
        $ajouter = $this->exploitant->ajouter($valF, $this->f->db);
        $this->f->isDatabaseError($ajouter);
    }

    /**
     * Modifie un exploitant existant
     */
    function modifier_exploitant($champs) {

        // Récupération des champs
        $valF = $this->recuperation_champs_exploitant($champs);

        // Définition de la clé primaire
        $valF['contact'] = $this->recuperer_ID_exploitant($this->valF["etablissement"]);

        // Redéfinition de la qualité particulier
        $valF['qualite'] = "particulier";

        // Modification dans la table contact
        $modifier = $this->exploitant->modifier($valF, $this->f->db);
        $this->f->isDatabaseError($modifier);
    }

    /**
     * Supprime l'exploitant de l'établissement passé en paramètre.
     */
    function supprimer_exploitant() {

        // Définition de la clé primaire
        $valF['contact'] = $this->recuperer_ID_exploitant($this->valF["etablissement"]);

        $supprimer = $this->exploitant->supprimer($valF, $this->f->db);
        $this->f->isDatabaseError($supprimer);
    }

    /**
     * Crée un exploitant
     */
    function recuperation_champs_exploitant($champs) {

        // Rattachement à l'établissement
        $valF['etablissement'] = $this->valF["etablissement"];

        // Définition du type de contact en exploitant
        $valF['contact_type'] = $this->recuperer_ID_type_exploitant();

        // Récupération de la saisie utilisateur
        $valF['civilite'] = $champs["exp_civilite"];
        $valF['nom'] = $champs["exp_nom"];
        $valF['prenom'] = $champs["exp_prenom"];
        $valF['adresse_numero'] = $champs["exp_adresse_numero"];
        $valF['adresse_numero2'] = $champs["exp_adresse_numero2"];
        $valF['adresse_voie'] = $champs["exp_adresse_voie"];
        $valF['adresse_complement'] = $champs["exp_adresse_complement"];
        $valF['adresse_cp'] = $champs["exp_adresse_cp"];
        $valF['adresse_ville'] = $champs["exp_adresse_ville"];
        $valF['lieu_dit'] = $champs["exp_lieu_dit"];
        $valF['boite_postale'] = $champs["exp_boite_postale"];
        $valF['cedex'] = $champs["exp_cedex"];
        $valF['pays'] = $champs["exp_pays"];
        //XXX Nouveaux champs
        $valF['qualite'] = '';
        $valF['denomination'] = '';
        $valF['raison_sociale'] = '';
        $valF['siret'] = '';
        $valF['categorie_juridique'] = '';
        $valF['reception_convocation'] = '';
        $valF['reception_programmation'] = '';
        $valF['reception_commission'] = '';
        $valF['service'] = '';
        
        // Champs non utilisés dans la fiche établissement
        $valF['titre'] = "";
        $valF['telephone'] = "";
        $valF['mobile'] = "";
        $valF['fax'] = "";
        $valF['courriel'] = "";
        $valF['om_validite_debut'] = "";
        $valF['om_validite_fin'] = "";

        // Retourne les champs exploitant
        return $valF;
    }

    // Récupération du code de la nature de l'établissement
    function get_etablissement_nature_code($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si contact_type n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_nature
                WHERE etablissement_nature = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    // Récupération du libellé du type de l'établissement
    function get_etablissement_type_libelle($id) {

        // Initialisation du résultat retourné
        $libelle = '';

        // Si contact_type n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(libelle)
                FROM ".DB_PREFIXE."etablissement_type
                WHERE etablissement_type = ".$id;
            $libelle = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($libelle);

        }

        // Résultat retourné
        return $libelle;
    }

    // Récupération du libellé du type de l'établissement
    function get_etablissement_statut_juridique_code($id) {

        // Initialisation du résultat retourné
        $code = '';

        // Si etablissement_statut_juridique n'est pas vide
        if ($id != '') {

            // Requête SQL
            $sql = "
                SELECT LOWER(code)
                FROM ".DB_PREFIXE."etablissement_statut_juridique
                WHERE etablissement_statut_juridique = ".$id;
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }
    
    /**
     * Mémorisation de l'ID et du libellé d'un nouvel établissement
     * lors d'un ajout dans l'autocomplete des DC
     */
    function formSpecificContent($maj) {

        // Récupération de la validation
        $validation_parameter = $this->getParameter('validation');
        $validation = (isset($validation_parameter)) ? $validation_parameter : 0;

        // Si on est en mode ajout et que le formulaire a été validé
        if ($maj == 0 && $validation > 0) {
            echo "<span id=\"id_new_etablissement_tous\">";
                echo $this->valF[$this->clePrimaire];
            echo "</span>";
            echo "<span id=\"libelle_new_etablissement_tous\">";
                echo $this->valF["libelle"];
            echo "</span>";
        }
    }

    /**
     * VIEW - view_localiser_selection
     * Redirige l'utilisateur vers le SIG externe, avec la vue centrée sur la sélection
     * d'établissements obtenue par la recherche avancée.
     * Si la recherche renvoie une liste vide, l'utilisateur est redirigé vers la couche
     * des établissements sur le SIG.
     *
     * @return void
     */
    function view_localiser_selection() {
        $this->checkAccessibility();

        (isset($_GET['obj']) ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        // Premier enregistrement a afficher
        (isset($_GET['premier']) ? $premier = $this->f->get_submitted_get_value('premier') : $premier = 0);
        // Colonne choisie pour le tri
        (isset($_GET['tricol']) ? $tricol = $this->f->get_submitted_get_value('tricol') : $tricol = "");
        // Id unique de la recherche avancee
        (isset($_GET['advs_id']) ? $advs_id = $this->f->get_submitted_get_value('advs_id') : $advs_id = "");
        // Valilite des objets a afficher
        (isset($_GET['valide']) ? $valide = $this->f->get_submitted_get_value('valide') : $valide = "");
        // Valilite des objets a afficher
        (isset($_GET['recherche']) ? $recherche = $this->f->get_submitted_get_value('recherche') : $recherche = "");
        // Valilite des objets a afficher
        (isset($_GET['selectioncol']) ? $selectioncol = $this->f->get_submitted_get_value('selectioncol') : $selectioncol = "");

        // Instance geoaria
        $geoaria = $this->f->get_inst_geoaria();
        if($geoaria === false) {
            // L'erreur geoaria est affichée dans la méthode handle_geoaria_exception
            return false;
        }
        // S'il n'y a pas eu de recherche avancée
        if ($advs_id === '') {
            try {
                $url = $geoaria->redirection_web('etablissement', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
            header("Location: ".$url);
            exit();
        }

        // Ce tableau permet a chaque application de definir des variables
        // supplementaires qui seront passees a l'objet metier dans le constructeur
        // a travers ce tableau
        // Voir le fichier dyn/form.get.specific.inc.php pour plus d'informations
        $extra_parameters = array();
        $f = $this->f;
        // surcharge globale
        if (file_exists('../dyn/tab.inc.php')) {
            require_once '../dyn/tab.inc.php';
        }
        if (file_exists('../dyn/custom.inc.php')) {
            require_once '../dyn/custom.inc.php';
        }
        // *** custom
        if(isset($custom['tab'][$obj]) and file_exists($custom['tab'][$obj])){
            require_once $custom['tab'][$obj];
        } else{
            // surcharge specifique des objets
            if (file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php")) {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
            } else {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc";
            }   
        }
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }
    
        // Redéfinition de l'ordre de tri pour avoir les codes dans un ordre alphabétique
        $tri = "ORDER BY etablissement.code ASC";

        /**
         *
         */
        // Instanciation d'om_table
        require_once "om_table.class.php";
        //
        $tb = new om_table(
            "../scr/tab.php",
            $table,
            $serie,
            $champAffiche,
            $champRecherche,
            $tri,
            $selection,
            $edition,
            $options,
            $advs_id,
            $om_validite
        );

        /**
         *
         */
        // Affectation des parametres
        $params = array(
            "obj" => $obj,
            "premier" => $premier,
            "recherche" => $recherche,
            "selectioncol" => $selectioncol,
            "tricol" => $tricol,
            "advs_id" => $advs_id,
            "valide" => $valide,
        );
        // Ajout de paramètre spécifique
        $params = array_merge($params, $extra_parameters);
        //
        $tb->setParams($params);
        // Methode permettant de definir si la recherche doit etre faite
        // sur la recherche simple ou avancée
        $tb->composeSearchTab();
        // Generation de la requete de recherche
        $tb->composeQuery();
        // Exécution de la requête
        $res = $this->db->limitquery($tb->sql, $premier, '50');
        $this->addToLog(__METHOD__."() db->limitquery(\"".$tb->sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        $nbligne = $res->numrows();
        // S'il n'y a aucun résultat
        if ($nbligne === 0) {
            try {
                $url = $geoaria->redirection_web('etablissement', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                return;
            }
            header("Location: ".$url);
            exit();
        }
        // Récupération des identifiants et libellés
        while ($row=& $res->fetchRow()){
            $etablissements_codes[] = $row[1];
        }
        $data = array();
        foreach ($etablissements_codes as $code) {
            $data[] = $code;
        }
        try {
            $url = $geoaria->redirection_web('etablissement', $data);
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            return;
        }

        //Redirection
        header("Location: ".$url);

    }


}// fin classe

?>
