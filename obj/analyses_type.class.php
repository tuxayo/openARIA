<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/analyses_type.class.php";

class analyses_type extends analyses_type_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Filtre par service
        $service = "";
        $champ = "service";
        if (isset($_POST[$champ])) {
            $service = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $service = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $service = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $service = $this->getVal($champ);
        }
        if ($service == "") {
            $service = -1;
        }
        $service_code = $this->f->get_service_code($service);

        // Modèle d'édition des rapports
        $sql_modele_edition_rapport = $sql_modele_edition;
        $sql_modele_edition_rapport = str_replace('<courrier_type_code>', 'ANL-'.$service_code.'-RPT', $sql_modele_edition_rapport);
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "modele_edition_rapport", $sql_modele_edition_rapport, $sql_modele_edition_rapport_by_id, true);

        // Modèle d'édition des comptes-rendu
        $sql_modele_edition_cr = $sql_modele_edition;
        $sql_modele_edition_cr = str_replace('<courrier_type_code>', 'ANL-'.$service_code.'-CRD', $sql_modele_edition_cr);
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "modele_edition_compte_rendu", $sql_modele_edition_cr, $sql_modele_edition_rapport_by_id, true);

        // Modèle d'édition des pv
        $sql_modele_edition_pv = $sql_modele_edition;
        $sql_modele_edition_pv = str_replace('<courrier_type_code>', 'ANL-'.$service_code.'-PV', $sql_modele_edition_pv);
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "modele_edition_proces_verbal", $sql_modele_edition_pv, $sql_modele_edition_rapport_by_id, true);
    }

    /**
     *
     */
    function set_form_specificity(&$form, $maj) {
        //
        parent::set_form_specificity($form, $this->getParameter("maj"));

        /**
         * Gestion du champ service
         */
        // 
        $this->set_form_specificity_service_common($form, $this->getParameter("maj"));
        // Si l'utilisateur est sur un service
        if (is_null($_SESSION['service'])) {
            // En mode AJOUTER
            if ($this->getParameter("maj") == 0) {
                // A la modification du champ service alors on met à jour les champs
                // liés pour que seules les valeurs correspondantes soient affichées.
                $form->setOnchange(
                    "service", 
                    "filterSelect(this.value, 'modele_edition_rapport', 'service', 'analyses_type');".
                    "filterSelect(this.value, 'modele_edition_compte_rendu', 'service', 'analyses_type');".
                    "filterSelect(this.value, 'modele_edition_proces_verbal', 'service', 'analyses_type');"
                );
            }
        }
    }

}

?>
