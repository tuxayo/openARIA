<?php
//$Id$ 
//gen openMairie le 12/03/2015 17:58

require_once "../core/obj/om_utilisateur.class.php";

class om_utilisateur extends om_utilisateur_core {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Supprimer le lien avec la table acteur
        $sql = "
            UPDATE 
                ".DB_PREFIXE."acteur
            SET
                om_utilisateur = NULL
            WHERE
                om_utilisateur=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->correct = false;
            return false;
        }
        //
        return true;
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le
     * générateur en devant surcharger la méthode cleSecondaire afin de 
     * supprimer les éléments liés dans les tables liés.
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array("acteur", ))) {
            //
            $this->addToLog(
                __METHOD__."(): On ne vérifie pas la table ".$table."",
                EXTRA_VERBOSE_MODE
            );
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }
}

?>
