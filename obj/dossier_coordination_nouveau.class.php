<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * le formulaire en mode ajouter depuis le menu.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/dossier_coordination.class.php");

class dossier_coordination_nouveau extends dossier_coordination {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

    /**
     * Surcharge du contenu du bouton retour dans le formulaire.
     * En cas d'ajout retourne sur le formulaire du nouvel enregistrement sinon
     * sur le tableau de bord.
     *
     * @param integer $dnu1
     * @param string  $dnu2
     * @param string  $dnu3
     */
    function retour($dnu1 = null, $dnu2 = null, $dnu3 = null) {

        // Récupération de la validation
        $validation_parameter = $this->getParameter('validation');
        $validation = (isset($validation_parameter)) ? $validation_parameter : 0;

        // Récupération du mode du formulaire
        $maj_parameter = $this->getParameter('maj');
        $maj = (isset($maj_parameter)) ? $maj_parameter : 0;

        // Si on est en mode ajout et que le formulaire a été validé
        if ($maj == 0 && $validation > 0 && !empty($this->valF[$this->clePrimaire])
            && $this->correct == true) {
            //
            $link = "../scr/form.php?obj=dossier_coordination&action=3&idx=".$this->valF[$this->clePrimaire];
        } else {
            //
            $link = $this->f->url_dashboard;
        }

        //
        echo "\n<a class=\"retour\" ";
        echo "href=\"".$link;
        //
        echo "\"";
        echo ">";
        //
        echo _("Retour");
        //
        echo "</a>\n";
    }

}// fin classe

?>
