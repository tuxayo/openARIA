<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once ("../gen/obj/dossier_coordination.class.php");

class dossier_coordination extends dossier_coordination_gen {

    var $valIdContact = array();
    var $postedIdContact = array();
    var $dossier_instruction_secu_id = null;
    var $dossier_instruction_acc_id = null;
    var $inst_etablissement = null;
    var $limites = array('10', '20', '30', '40', '50');
    var $limite_default = '10';

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);

        // Si ce n'est pas en mode ajout
        if ($id != "]") {
            // Recherche de l'identifiant du dossier d'instruction
            $this->dossier_instruction_secu_id =
                $this->get_dossier_instruction_by_dossier_coordination_and_service(
                    $id, 'si');

            // Recherche de l'identifiant du dossier d'instruction
            $this->dossier_instruction_acc_id =
                $this->get_dossier_instruction_by_dossier_coordination_and_service(
                    $id, 'acc');
        }
    }// fin constructeur

    function get_id_dossier_instruction($code_service) {
        
        switch ($code_service) {
            case 'acc':
                return $this->dossier_instruction_acc_id;
                break;
            case 'si' :
                return $this->dossier_instruction_secu_id;
                break;
        }
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (is_null($this->inst_etablissement)) {
            //
            if (is_null($etablissement)) {
                $etablissement = $this->getVal("etablissement");
            }
            //
            require_once "../obj/etablissement.class.php";
            $this->inst_etablissement = new etablissement($etablissement);
        }
        //
        return $this->inst_etablissement;
    }

    /**
     * Permet de définir le type des champs.
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);
        //
        $form->setType("dossier_coordination", "hidden");
        $form->setType("depot_de_piece", "hidden");
        // Champs cachés
        $form->setType('exploitant', 'hidden');

        // Si c'est un sous-formulaire
        if (!empty($this->retourformulaire)) {
            $form->setType('libelle', 'link');
        }

        // 
        $form->setType('dossier_instruction_secu_lien', 'link');
        $form->setType('dossier_instruction_acc_lien', 'link');
        $form->setType('geom_point', 'hidden');
        $form->setType('geom_emprise', 'hidden');
        // Si le dossier de coordination n'est plus à qualifier
        // ou s'il n'est pas encore lié à un dossier de coordination
        if ($form->val["a_qualifier"] == 'f' 
            || !empty($this->dossier_instruction_secu_id) 
            || !empty($this->dossier_instruction_acc_id)) {
            //
            $form->setType('dossier_coordination_type', 'selecthiddenstatic');
        }

        // En mode ajout
        if ($maj === 0) {
            $form->setType('libelle', 'hidden');
        }

        // En mode modifier
        if ($maj == 1) {
            $form->setType('libelle', 'hiddenstatic');
            $form->setType('date_demande','hiddenstaticdate');

            //
            if ($this->is_periodic()) {
                //
                $form->setType('etablissement','selecthiddenstatic');
            }
        }

        // En mode ajouter et modifier
        if ($maj === 0 || $maj == 1) {

            $form->setType('references_cadastrales','referencescadastrales');
            $form->setType("dossier_cloture", "checkboxstatic");
            $form->setType("etablissement_type_secondaire", "select_multiple");
            $form->setType('date_cloture', 'hiddenstaticdate');
            $form->setType('autorite_police_encours', 'hidden');
            $form->setType('geolocalise', 'hidden');

            // Si c'est un sous-formulaire d'un établissement
            if (in_array($this->retourformulaire, $this->foreign_keys_extended['etablissement'])) {
                $form->setType('etablissement', 'selecthiddenstatic');
            } else {
                $form->setType('etablissement', 'autocomplete');
            }
        }

        // En mode supprimer et consulter
        if ($maj == 2 || $maj == 3) {

            $form->setType('dossier_coordination_parent', 'link');
            $form->setType('references_cadastrales','referencescadastralesstatic');
            $form->setType("etablissement_type_secondaire", "select_multiple_static");

            // Type permettant de cliquer sur le champ pour ouvrir
            // le formulaire de l'objet lié et d'afficher les inforamtions
            // utiles de l'établissement
            $form->setType('etablissement', 'etablissement');
            // Type spécifique à l'exploitant, permet d'afficher ses
            // informations
            $form->setType('exploitant', 'contact_exploitant');
            if ($this->getVal('geolocalise') === 't') {
                $form->setType('geolocalise', 'link_geolocalisation');
            }
        }

        //
        $form->setType("enjeu_erp", 'hidden');
        if ($this->getVal("enjeu_erp") == "t") {
            $form->setType("enjeu_erp", "enjeu_erp");
        }

        //
        $form->setType("enjeu_ads", 'hidden');
        if ($this->getVal("enjeu_ads") == "t") {
            $form->setType("enjeu_ads", "enjeu_ads");
        }

        $form->setType("interface_referentiel_ads", 'hidden');
        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            if ($this->getVal("interface_referentiel_ads") == "t") {
                $form->setType("interface_referentiel_ads", "checkboxstatic");
            }
        }

        // Gestion des champs DA et DI ADS
        // (dossier_autorisation_ads et dossier_instruction_ads)
        if ($maj == 0) {
            $form->setType("dossier_autorisation_ads", "text");
            $form->setType("dossier_instruction_ads", "text");
        } elseif ($maj == 1) {
            if ($this->is_connected_to_referentiel_ads() !== true) {
                $form->setType("dossier_autorisation_ads", "text");
                $form->setType("dossier_instruction_ads", "text");
            } else {
                $form->setType("dossier_autorisation_ads", "link_dossier_autorisation_ads");
                $form->setType("dossier_instruction_ads", "hiddenstatic");
            }
        } else {
            $form->setType("dossier_autorisation_ads", "link_dossier_autorisation_ads");
            $form->setType("dossier_instruction_ads", "hiddenstatic");
        }

        // Pour les actions appelée en POST en Ajax, il est nécessaire de
        // qualifier le type de chaque champs (Si le champ n'est pas défini un
        // PHP Notice:  Undefined index dans core/om_formulaire.class.php est
        // levé). On sélectionne donc les actions de portlet de type
        // action-direct ou assimilé et les actions spécifiques avec le même
        // comportement.
        if ($this->get_action_param($maj, "portlet_type") == "action-direct"
            || $this->get_action_param($maj, "portlet_type") == "action-direct-with-confirmation") {
            //
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType($this->clePrimaire, "hiddenstatic");
        }
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Si c'est un sous-formulaire
        if (!empty($this->retourformulaire)) {

            // Paramètres envoyés au type link
            $params = array();
            $params['obj'] = "dossier_coordination";
            $params['libelle'] = $form->val['libelle'];
            $params['idx'] = $form->val['dossier_coordination'];
            $form->setSelect("libelle", $params);
        }

        // En mode "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {
            // Initialisation de données à vide
            $libelle = "";
            $values_etablissement = array(
                "code" => "",
                "libelle" => "",
                "adresse_numero" => "",
                "adresse_numero2" => "",
                "adresse_voie" => "",
                "adresse_complement" => "",
                "lieu_dit" => "",
                "boite_postale" => "",
                "adresse_cp" => "",
                "adresse_ville" => "",
                "adresse_arrondissement" => "",
                "cedex" => "",
                "etablissement_nature_libelle" => "",
                "etablissement_type_libelle" => "",
                "etablissement_categorie_libelle" => "",
                "si_locaux_sommeil" => "",
                "geolocalise" => "",
            );
            // Initialisation de l'objet
            $obj = "etablissement_tous";
            // Initialisation des valeurs du contact exploitan
            $values_exploitant = "";
            // Si le champ etablissement n'est pas vide
            if (!empty($form->val['etablissement'])) {

                // Récupère le libellé de l'établissement depuis la requête
                // pour le champ select
                $sql_etablissement_by_id = str_replace('<idx>', $form->val['etablissement'], $sql_etablissement_by_id);
                $res = $this->f->db->query($sql_etablissement_by_id);
                $this->addToLog("setSelect(): db->query(".$sql_etablissement_by_id.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                // Liste les résultats
                $row =& $res->fetchRow();
                // Récupère le libelle
                $libelle = $row[1];

                // Instance de la classe etablissement
                require_once '../obj/etablissement.class.php';
                $etablissement = new etablissement($form->val['etablissement']);

                // Récupère toutes les données nécessaires de l'établissement
                $values_etablissement = $etablissement->get_data($form->val['etablissement']);

                // Récupère l'exploitant de l'établissement
                $exploitant_id = $etablissement->recuperer_ID_exploitant($form->val['etablissement']);

                // Si l'établissement a un exploitant
                if ($exploitant_id != "]") {
                    // Instance de la classe contact
                    require_once '../obj/contact.class.php';
                    $exploitant = new contact($exploitant_id);

                    // Récupère toutes les données nécessaires du contact
                    $values_exploitant = $exploitant->get_data($exploitant_id);
                }
            }

            // Paramétres envoyés au type link et au type spécifique 
            // etablissement
            $params = array();
            $params['obj'] = $obj;
            $params['libelle'] = $libelle;
            $params['values'] = $values_etablissement;
            $form->setSelect("etablissement", $params);

            // Paramètres envoyés au type spécifique contact_exploitant
            $params = array();
            $params['values'] = $values_exploitant;
            $form->setSelect("exploitant", $params);

            // Paramètres envoyés au type link du champ dossier de 
            // coordination parent
            $params = array();
            $params['obj'] = "dossier_coordination";
            $params['idx'] = $form->select['dossier_coordination_parent'][0][0];
            $params['libelle'] = $form->select['dossier_coordination_parent'][1][0];
            $form->setSelect("dossier_coordination_parent", $params);
        }

        // Types secondaires
        $dossier_coordination_id = 0;
        if (!empty($form->val['dossier_coordination'])
            && $form->val['dossier_coordination'] != ''
            && $form->val['dossier_coordination'] != ']') {
            $dossier_coordination_id = $form->val['dossier_coordination'];
        }
        $sql_etablissement_type_secondaire_by_id = str_replace('<idx>', $dossier_coordination_id, $sql_etablissement_type_secondaire_by_id);
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_type_secondaire", $sql_etablissement_type_secondaire, $sql_etablissement_type_secondaire_by_id, false, true);

        // Initialisation du libellé du DI sécurité
        $dossier_instruction_secu_libelle = "";
        // Initialisation du libellé du DI accessibilité
        $dossier_instruction_acc_libelle = "";

        // Si le dossier de coordination a générer un dossier
        // d'instruction sécurité
        if (isset($form->val['dossier_instruction_secu']) && $form->val['dossier_instruction_secu'] == 't') {
            // S'il y a un dossier d'instruction de sécurité
            if (!empty($this->dossier_instruction_secu_id)) {
                // Instance de la classe dossier_instruction
                require_once '../obj/dossier_instruction.class.php';
                $dossier_instruction_secu = new dossier_instruction($this->dossier_instruction_secu_id);
                //
                $dossier_instruction_secu_libelle = $dossier_instruction_secu->getVal("libelle");
            }
        }

        // Si le dossier de coordination a générer un dossier
        // d'instruction accessibilité
        if (isset($form->val['dossier_instruction_acc']) && $form->val['dossier_instruction_acc'] == 't') {
            // S'il y a un dossier d'instruction de accessibilité
            if (!empty($this->dossier_instruction_acc_id)) {
                // Instance de la classe dossier_instruction
                require_once '../obj/dossier_instruction.class.php';
                $dossier_instruction_acc = new dossier_instruction($this->dossier_instruction_acc_id);
                //
                $dossier_instruction_acc_libelle = $dossier_instruction_acc->getVal("libelle");
            }
        }

        // Initialisation de l'objet visé
        $obj = "dossier_instruction_tous_visites";
        // Récupèration du type du dossier (PLAN ou VISIT)
        if (!empty($form->val['dossier_coordination'])) {
            // Récupère le code du type du dossier 
            $dossier_type = $this->get_dossier_type_code_by_dossier_coordination($form->val['dossier_coordination']);
            // Modifie l'objet visé
            if ($dossier_type == "plan") {
                $obj = "dossier_instruction_tous_plans";
            }
        }

        // Paramètres envoyés au type link
        $params = array();
        $params['obj'] = $obj;
        $params['idx'] = $this->dossier_instruction_secu_id;
        $params['libelle'] = $dossier_instruction_secu_libelle;
        $form->setSelect("dossier_instruction_secu_lien", $params);

        // Paramètres envoyés au type link
        $params = array();
        $params['obj'] = $obj;
        $params['idx'] = $this->dossier_instruction_acc_id;
        $params['libelle'] = $dossier_instruction_acc_libelle;
        $form->setSelect("dossier_instruction_acc_lien", $params);

        // En mode "ajouter" et "modifier"
        if ($maj == 0 || $maj == 1) {
            
            // Si ce n'est un sous-formulaire d'un établissement
            if (!in_array($this->retourformulaire, $this->foreign_keys_extended['etablissement'])) {
                $params = array();
                // Surcharge visée pour l'ajout
                $params['obj'] = "etablissement_tous";
                // Table de l'objet
                $params['table'] = "etablissement";
                // Permission d'ajouter
                $params['droit_ajout'] = true;
                // Critères de recherche
                $params['criteres'] = array(
                    "etablissement.etablissement" => _("ID"),
                    "etablissement.code" => _("code"),
                    "etablissement.libelle" => _("libelle"),
                    "contact.nom" => _("nom d'un contact")
                );
                // Tables liées
                $params['jointures'] = array(
                    "contact ON etablissement.etablissement = contact.etablissement"
                );
                // Colonnes ID et libellé du champ
                // (si plusieurs pour le libellé alors une concaténation est faite)
                $params['identifiant'] = "etablissement.etablissement";
                $params['libelle'] = array (
                    "etablissement.code",
                    "etablissement.libelle"
                );
                $params['link_selection'] = true;
                // Envoi des paramètres
                $form->setSelect("etablissement", $params);
            } 

            // Initialise le select en fonction de la valeur d'un autre champ
            $form->setSelect('dossier_coordination_parent', $this->loadSelect_dossier_coordination_parent($form, $maj, "etablissement"));
        }

        // Si formulaire en consultation et sig activé
        if ($this->getParameter('maj') === '3'
            && $this->is_option_sig_enabled() === true) {
            //
            $params = array();
            $params['obj'] = get_class($this);
            $params['idx'] = $this->getParameter('idx');
            $params['action'] = '33';
            $params['title'] = _('Liste des propriétaires');
            //
            $form->setSelect('references_cadastrales', $params);
        }

        //
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->getVal("dossier_autorisation_ads") != "") {
            //
            $inst_referentiel_ads = $this->f->get_inst_referentiel_ads();
            $url_view_da = $inst_referentiel_ads->get_url_view_da(
                $this->getVal("dossier_autorisation_ads")
            );
            if ($url_view_da !== false) {
                $form->setSelect(
                    "dossier_autorisation_ads",
                    array("link" => $url_view_da, )
                );
            }
        }
    }

    /**
     *  Permet de pré-remplir les valeurs des formulaires.
     *  
     * @param [object]   $form        formulaire
     * @param [integer]  $maj         mode
     * @param [integer]  $validation  validation
     */
    function set_form_default_values(&$form, $maj, $validation) {
        // En mode ajout
        if( $maj == 0 ) {
            // Date du jour pour date_demande
            $form->setVal("date_demande", date('d/m/Y'));
        }

        // Si le dossier de coordination a générer un dossier
        // d'instruction sécurité
        // if ($form->val['dossier_instruction_secu'] == 't') {
        //     // Recherche de l'identifiant du dossier d'instruction
        //     $this->dossier_instruction_secu_id = $this->get_dossier_instruction_by_dossier_coordination_and_service($form->val['dossier_coordination'], 'si');
            // Si l'identifiant est trouvé
        if (!empty($this->dossier_instruction_secu_id)) {
            // Affecte la valeur au cha
            $form->setVal("dossier_instruction_secu_lien", $this->dossier_instruction_secu_id);
        }
        // }

        // Si le dossier de coordination a générer un dossier
        // d'instruction accessibilité
        // if ($form->val['dossier_instruction_acc'] == 't') {
        //     // Recherche de l'identifiant du dossier d'instruction
        //     $this->dossier_instruction_acc_id = $this->get_dossier_instruction_by_dossier_coordination_and_service($form->val['dossier_coordination'], 'acc');
        //     //
        if (!empty($this->dossier_instruction_acc_id)) {
            //
            $form->setVal("dossier_instruction_acc_lien", $this->dossier_instruction_acc_id);
        }
        // }
    }

    /**
     * Génère le libelle du dossier de coordination.
     *
     * @param integer $dossier_coordination_type_id Identifiant du type du DC
     * @param integer $dossier_coordination_id      Identifiant du DC
     *
     * @return string Libellé généré
     */
    function generate_dossier_coordination_libelle($dossier_coordination_type_id, $dossier_coordination_id) {
        // Initilisation du résultat
        $libelle = "";

        // Récupération du code du type du dossier de coordination
        $dossier_coordination_type = $this->get_dossier_coordination_type($dossier_coordination_type_id);
        // Récupération du code du type du dossier
        $dossier_type = $this->get_dossier_type_by_dossier_coordination_type($dossier_coordination_type_id);
        // Mise de l'identifiant du dossier de coordination sur 6 caractères
        // numériques
        $dossier_coordination = str_pad($dossier_coordination_id, 6, "0", STR_PAD_LEFT);

        // Composition du libelle
        $libelle = $dossier_coordination_type['code'].'-'.$dossier_type['code'].'-'.$dossier_coordination;

        // Retourne le libellé
        return $libelle;

    }

    /**
     * Modifie le champ dossier_coordination de l'établissement
     *
     * @param integer $id Identifiant du dossier de coordination
     *
     * @return boolean
     */
    function update_etablissement_dossier_coordination_periodique($id = null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_coordination = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_coordination = new dossier_coordination($id);
        }

        // Instance de la classe etablissement
        require_once '../obj/etablissement.class.php';
        $etablissement = new etablissement($dossier_coordination->getVal('etablissement'));

        //
        $values = array(
            "dossier_coordination_periodique" => $dossier_coordination->getVal($dossier_coordination->clePrimaire),
        );
        $handle_dossier_coordination_periodique = $etablissement->handle_dossier_coordination_periodique("update", $values);

        //
        if ($handle_dossier_coordination_periodique == false) {
            // Message d'erreur
            $this->addToMessage($etablissement->msg);
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }

    /**
     *
     */
    function triggerajouter($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        //
        $this->valF['libelle'] = $this->generate_dossier_coordination_libelle($this->valF['dossier_coordination_type'], $this->valF['dossier_coordination']);
        return true;
    }

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Si l'établissement est renseigné
        if (!empty($this->valF["etablissement"]) && $this->is_periodic($id)) {

            // Met à jour le dossier de coordination périodique de l'établissement
            // si nécessaire
            $update_etablissement_dossier_coordination_periodique = $this->update_etablissement_dossier_coordination_periodique($id);

            // Si la mise à jour échoue
            if ($update_etablissement_dossier_coordination_periodique == false) {
                // Stop le traitement
                return false;
            }
        }

        // Si pas à qualifier lors de l'ajout d'un DC création des DI nécessaires
        if($this->valF['a_qualifier'] !== true) {
            // Si la case  dossier d'instruction sécurité est cochée
            if($this->valF['dossier_instruction_secu'] === true) {
                if(!$this->create_dossier_instruction('SI')) {
                    return false;
                }
            }
            // Si la case  dossier d'instruction accessibilité est cochée
            if($this->valF['dossier_instruction_acc'] === true) {
                if(!$this->create_dossier_instruction('ACC')) {
                    return false;
                }
            }
        }


        // Ajout ou modification des demandeurs
        $this->insertLinkDossierCoordinationContact();

        // Récupération des données du select multiple
        $types_secondaires = $this->getPostedValues('etablissement_type_secondaire');
        $type_principal = $this->valF['etablissement_type'];

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($types_secondaires) && count($types_secondaires) > 0 ) {
            // Boucle sur la liste des types sélectionnés
            foreach ($types_secondaires as $value) {
                // Pas d'ajout si valeur nulle ou type principal sélectionné
                if ($value != "" && $value != $type_principal) {
                    // 
                    $data = array(
                        'lien_dossier_coordination_etablissement_type' => "",
                        'dossier_coordination' => $this->valF[$this->clePrimaire],
                        'etablissement_type' => $value
                    );
                    // Stop le traitement si un ajout échoue
                    if ($this->add_type_secondaire($data) == false) {
                        return false;
                    }
                }
            }
        }

        if ($this->is_option_sig_enabled() === true) {
            // Tentative de géolocalisation du DC et affichage d'un message à l'utilisateur
            // Si le DC n'est pas géocodé il doit quand même pouvoir être créé.
            $this->geocoder();
          }

        // Ajout des parcelles dans la table dossier_coordination_parcelle
        $this->ajouter_dc_parcelle($this->valF[$this->clePrimaire], 
            $val['references_cadastrales']);

        return true;
    }

    /**
     *
     */
    function triggermodifier($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        if ($this->getVal("dossier_coordination_type") != $this->valF['dossier_coordination_type'] && !empty($this->valF['dossier_coordination_type'])) {
            //
            $this->valF['libelle'] = $this->generate_dossier_coordination_libelle($this->valF['dossier_coordination_type'], $this->valF['dossier_coordination']);
        }
        return true;
    }

    /**
     *
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Si l'établissement est renseigné
        if (!empty($this->valF["etablissement"]) && $this->is_periodic($id)) {

            // Met à jour le dossier de coordination périodique de l'établissement
            // si nécessaire
            $update_etablissement_dossier_coordination_periodique = $this->update_etablissement_dossier_coordination_periodique();

            // Si la mise à jour échoue
            if ($update_etablissement_dossier_coordination_periodique == false) {
                // Stop le traitement
                return false;
            }
        }

        // On supprime toutes les lignes de la table etablissement_parcelle
        // qui font référence à l'établissement en cours de modification
        $this->supprimer_dc_parcelle($val['dossier_coordination']);

        // Ajout des parcelles dans la table etablissement_parcelle
        $this->ajouter_dc_parcelle($val['dossier_coordination'], 
            $val['references_cadastrales']);

        // Si l'état a qualifié ne l'est plus
        if($this->valF['a_qualifier'] !== true) {
            // Si la case  dossier d'instruction sécurité est cochée
            if($this->valF['dossier_instruction_secu'] === true && empty($this->dossier_instruction_secu_id)) {
                if($this->create_dossier_instruction('SI') !== true) {
                    return false;
                }
            }
            // Si la case  dossier d'instruction accessibilité est cochée
            if($this->valF['dossier_instruction_acc'] === true && empty($this->dossier_instruction_acc_id)) {
                if($this->create_dossier_instruction('ACC') !== true) {
                    return false;
                }
            }
        }

        // Suppression de tous les liens de la table de liaison dc/type sec
        $this->delete_type_secondaire($this->valF[$this->clePrimaire]);

        // Récupération des données du select multiple
        $types_secondaires = $this->getPostedValues('etablissement_type_secondaire');
        $type_principal = $this->valF['etablissement_type'];

        // Ne traite les données que s'il y en a et qu'elles sont correctes
        if (is_array($types_secondaires) && count($types_secondaires) > 0 ) {
            // Boucle sur la liste des types sélectionnés
            foreach ($types_secondaires as $value) {
                // Pas d'ajout si valeur nulle ou type principal sélectionné
                if ($value != "" && $value != $type_principal) {
                    // 
                    $data = array(
                        'lien_dossier_coordination_etablissement_type' => "",
                        'dossier_coordination' => $this->valF[$this->clePrimaire],
                        'etablissement_type' => $value
                    );
                    // Stop le traitement si un ajout échoue
                    if ($this->add_type_secondaire($data) == false) {
                        return false;
                    }
                }
            }
        }

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[206] Mise à jour de la qualification ERP
         * Déclencheur :
         * - option_referentiel_ads activée
         * - le DC passe de non qualifié à qualifié
         * - le DC est de type PC
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && substr($this->getVal("libelle"), 0, 2) === 'PC'
            && $this->getVal('a_qualifier') == 't'
            && $this->valF['a_qualifier'] !== true) {
            //
            $inst_etab_type = $this->get_inst_common("etablissement_type", $this->valF["etablissement_type"]);
            $etab_type = $inst_etab_type->getVal('libelle');
            //
            $inst_etab_categ = $this->get_inst_common("etablissement_categorie", $this->valF["etablissement_categorie"]);
            $etab_categ = $inst_etab_categ->getVal('libelle');
            //
            $infos = array(
                "dossier_coordination" => $this->getVal("dossier_coordination"),
                "dossier_instruction" => $this->getVal("dossier_instruction_ads"),
                "Confirmation ERP" => ($this->valF['erp'] === true ? "Oui" : "Non"),
                "Type de dossier ERP" => ($etab_type == "" ? "NC" : $etab_type),
                "Catégorie de dossier ERP" => ($etab_categ == "" ? "NC" : $etab_categ),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(206, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (206) du référentiel ADS OK."));
        }

        return true;
    }

    /**
     * Permet de supprimer un type secondaire d'établissement lié à un DC.
     *
     * @param integer $dc Identifiant du dossier de coordination
     *
     * @return boolean
     */
    function delete_type_secondaire($dc) {
        // Instance de la classe de liaison
        require_once '../obj/lien_dossier_coordination_etablissement_type.class.php';
        $lien = new lien_dossier_coordination_etablissement_type(0);
        // Supprime l'enregistrement
        $delete = $lien->delete_by_dossier_coordination($dc);
        //
        return $delete;
    }

    /**
     * Permet de lier un type secondaire à un dossier de coordination.
     *
     * @param array $data Données de l'enregistrement
     *
     * @return boolean
     */
    function add_type_secondaire($data) {
        // Instance de la classe
        require_once '../obj/lien_dossier_coordination_etablissement_type.class.php';
        $lien = new lien_dossier_coordination_etablissement_type("]");
        // Ajoute l'enregistrement
        $add = $lien->ajouter($data);
        //
        return $add;
    }

    /**
     * Retourne le contenu de la table de liaison n à n 
     * 'lien_dossier_coordination_etablissement_type' filtré sur le DC instancié.
     *
     * @return array
     */
    function get_etablissement_type_secondaire() {
        $inst_lien__dc__etab_type = $this->get_inst_common(
            "lien_dossier_coordination_etablissement_type",
            0
        );
        return $inst_lien__dc__etab_type->get_liens_by_dossier_coordination(
            $this->getVal($this->clePrimaire)
        );
    }

    /**
     * Ajoute les parcelles du DC passé en paramètre.
     * 
     * @param string $dossier_coordination Identifiant du DC
     * @param string $references_cadastrales Références cadastrales
     */
    function ajouter_dc_parcelle($dossier_coordination, $references_cadastrales) {

        // Parse les parcelles
        $list_parcelles = $this->f->parseParcelles($references_cadastrales);

        // Fichier requis
        require_once "../obj/dossier_coordination_parcelle.class.php";

        // A chaque parcelle une nouvelle ligne est créée dans la table
        // dossier_coordination_parcelle
        foreach ($list_parcelles as $parcelle) {

            // Instance de la classe etablissement_parcelle
            $dossier_coordination_parcelle =
            new dossier_coordination_parcelle("]", $this->f->db, DEBUG);

            // Valeurs à sauvegarder
            $value = array(
                'dossier_coordination_parcelle' => '',
                'dossier_coordination' => $dossier_coordination,
                'ref_cadastre' => $parcelle['quartier']
                                .$parcelle['section']
                                .$parcelle['parcelle']
            );

            // Ajout de la ligne
            $dossier_coordination_parcelle->ajouter($value, $this->f->db, DEBUG);
        }
    }

    /**
     * Supprime les parcelles du DC passé en paramètre.
     * 
     * @param string $dossier_coordination Identifiant du DC
     */
    function supprimer_dc_parcelle($dossier_coordination) {

        // Suppression des parcelles du DC
        $sql = "DELETE FROM ".DB_PREFIXE."dossier_coordination_parcelle
                WHERE dossier_coordination='".$dossier_coordination."'";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        database::isError($res);
    }

    /**
     * [get_dossier_coordination_type description]
     * @param  [type] $dossier_coordination_type [description]
     * @return [type]                            [description]
     */
    function get_dossier_coordination_type($dossier_coordination_type) {

        // Requête SQL
        $sql = "SELECT dossier_coordination_type.libelle, dossier_coordination_type.code
                FROM ".DB_PREFIXE."dossier_coordination_type
                WHERE dossier_coordination_type = ".$dossier_coordination_type;
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Ligne de résultat
        $row = &$res->fetchRow(DB_FETCHMODE_ASSOC);

        // Retourne le résultat
        return $row;
    }

    /**
     * Cette méthode permet de récupérer le code du type de dossier d'une liste de dossiers
     * de coordination.
     * 
     * @param  array $dossier_coordination_ids Tableau d'identifiants de dossiers de coordination.
     * @return array Tableau associatif au format 'identifiant_dc' => 'code'
     */
    function get_dossiers_coordination_type_codes_by_dossiers_coordination_ids(array $dossier_coordination_ids) {

        $ids_string = implode(',', $dossier_coordination_ids);
        // Requête SQL
        $sql = "SELECT dossier_coordination.dossier_coordination, dossier_coordination_type.code
                FROM ".DB_PREFIXE."dossier_coordination_type
                LEFT OUTER JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_coordination_type.dossier_coordination_type = dossier_coordination.dossier_coordination_type
                WHERE dossier_coordination IN (".$ids_string.")";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Récupère les résultats dans un tableau
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            $result[$row['dossier_coordination']] = $row['code'];
        }
        // Retourne le résultat
        return $result;
    }

    /**
     * [get_dossier_type_by_dossier_coordination_type description]
     * @return [type] [description]
     */
    function get_dossier_type_by_dossier_coordination_type($dossier_coordination_type) {
        
        // Requête SQL
        $sql = "SELECT dossier_type.libelle, dossier_type.code
                FROM ".DB_PREFIXE."dossier_type
                LEFT OUTER JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
                WHERE dossier_coordination_type = ".$dossier_coordination_type;
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Ligne de résultat
        $row = &$res->fetchRow(DB_FETCHMODE_ASSOC);

        // Retourne le résultat
        return $row;
    }

    /**
     * Création du dossier d'instruction.
     *
     * @param string $service Code du service
     *
     * @return boolean
     */
    function create_dossier_instruction($service) {

        // Récupération de l'identifiant du service par son code
        $id_service = $this->get_service_id($service);
        // Si rien n'est retourné le traitement est arrêté
        if( $id_service === false ) {
            return false;
        }

        // Instance de la classe dossier_instruction
        require_once "../obj/dossier_instruction.class.php";
        $di = new dossier_instruction(']', $this->f->db, false);
        // Récupération des valeurs
        $values = array();
        foreach($di->champs as $key => $champ) {
            $values[$champ] = '';
        }
        // Valeurs à modifier
        $values['libelle'] = $this->valF['libelle'].'-'.$service;
        $values['dossier_coordination'] = $this->valF[$this->clePrimaire];
        $values['description'] = $this->valF['description'];
        $values['service'] = $id_service;
        $values['a_qualifier'] = true;
        $values['date_ouverture'] = date("d/m/Y");
        // Si les dossiers d'instructions sont ajoutés dès la création du
        // dossier de coordination, alors leurs dates d'ouverture est la même
        // que la date de la demande
        if ($this->getParameter('maj') == 0) {
            $values['date_ouverture'] = $this->dateDBToForm($this->valF["date_demande"]);
        }

        // Si l'ajout du dossier d'instruction échoue
        if($di->ajouter($values, $this->f->db, false) === false) {
            $this->correct = false;
            $this->addToMessage(
                sprintf(
                    _("Le dossier d'instruction %s n'a pas pu etre cree.")."<br/>".$di->msg,
                    $this->valF['libelle'].'-'.$service
                )
            );
            // Stop le traitement
            return false;
        } else {
            // Message de validation
            $this->addToMessage(
                sprintf(
                    _("Le dossier d'instruction %s a ete cree."),
                    $this->valF['libelle'].'-'.$service
                )
            );
        }

        //
        return true;
    }

    /**
     * Méthode de récupération de l'identifiant du service.
     *
     * @param string $code_service code de service
     *
     * @return mixed false si erreur sinon identifiant du service
     */
    function get_service_id($code_service) {
        $sql = "SELECT service FROM ".DB_PREFIXE."service WHERE LOWER(code)=LOWER('".$code_service."')";
        $id = $this->f->db->getOne($sql);
        $this->addToLog("get_service_id(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($id, true)) {
            $this->erreur_db($id->getDebugInfo(), $id->getMessage(), '');
            $this->correct = false;
            return false;
        }
        if( !empty($id) ) {
            return $id;
        } else {
            $this->addToMessage(_("Erreur de parametrage. Contactez votre administrateur."));
            $this->correct = false;
            return false;
        }
    }


    /**
     * Méthode de mise en page.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLayout(&$form, $maj) {
        //
        $form->setBloc($this->champs[0], 'D', "", "form-dossier_coordination-action-".$maj);
        // En mode "supprimer" et "consulter"
        if ($maj == 2 || $maj == 3) {
            // Fieldset "Informations générales"
            $form->setBloc('dossier_coordination', 'D', "", "col_12");
            $form->setBloc('dossier_coordination', 'D', "", "col_12");
            $form->setFieldset('dossier_coordination', 'D', _('Informations generales'), "");
            $form->setFieldset('dossier_coordination_parent', 'F', '');
            $form->setBloc('dossier_coordination_parent', 'F');
            $form->setBloc('dossier_coordination_parent', 'F');

            // Fieldset "Statut"
            $form->setBloc('dossier_cloture', 'D', "", "col_6");
            $form->setBloc('dossier_cloture', 'D', "", "col_12");
            $form->setFieldset('dossier_cloture', 'D', _('Statut'), "");
            $form->setFieldset('a_qualifier', 'F', '');
            $form->setBloc('a_qualifier', 'F');
            $form->setBloc('a_qualifier', 'F');

            // Fieldset "Dossiers d'instruction"
            $form->setBloc('dossier_instruction_secu', 'D', "", "col_6");
            $form->setBloc('dossier_instruction_secu', 'D', "", "col_12");
            $form->setFieldset('dossier_instruction_secu', 'D', _("Dossiers d'instruction"), "");
            //
            if ($form->val['dossier_instruction_secu'] == 't' && !empty($this->dossier_instruction_secu_id)) {
                $form->setBloc('dossier_instruction_secu','DF','','hidden');
            } else {
                $form->setBloc('dossier_instruction_secu_lien','DF','','hidden');
            }
            //
            if ($form->val['dossier_instruction_acc'] == 't' && !empty($this->dossier_instruction_acc_id)) {
                $form->setBloc('dossier_instruction_acc','DF','','hidden');
            } else {
                $form->setBloc('dossier_instruction_acc_lien','DF','','hidden');
            }
            $form->setFieldset('dossier_instruction_acc_lien', 'F', '');
            $form->setBloc('dossier_instruction_acc_lien', 'F');
            $form->setBloc('dossier_instruction_acc_lien', 'F');

            // Fieldset "Établissement"
            $form->setBloc('etablissement', 'D', "", "col_6");
            $form->setBloc('etablissement', 'D', "", "col_12");
            $form->setFieldset('etablissement', 'D', _('Etablissement'), "");
            $form->setFieldset('exploitant', 'F', '');
            $form->setBloc('exploitant', 'F');
            $form->setBloc('exploitant', 'F');

            // Fieldset "Urbanisme"
            $form->setBloc('interface_referentiel_ads', 'D', "", "col_6");
            $form->setBloc('interface_referentiel_ads', 'D', "", "col_12");
            $form->setFieldset('interface_referentiel_ads', 'D', _('Urbanisme'), "");
            $form->setFieldset('contraintes_urba_om_html', 'F', '');
            $form->setBloc('contraintes_urba_om_html', 'F');
            $form->setBloc('contraintes_urba_om_html', 'F');
        }
        
        // En mode "ajouter" et "modifier"
        if ($maj == 0 || $maj == 1) {
            // Fieldset "Informations générales"
            $form->setBloc('dossier_coordination', 'D', "", "col_12");
            $form->setBloc('dossier_coordination', 'D', "", "col_12");
            $form->setFieldset('dossier_coordination', 'D', _('Informations generales'), "");
            $form->setFieldset('dossier_cloture', 'F', '');
            $form->setBloc('dossier_cloture', 'F');
            $form->setBloc('dossier_cloture', 'F');
            
            // Fieldset "Qualification"
            $form->setBloc('a_qualifier', 'D', "", "col_12");
            $form->setBloc('a_qualifier', 'D', "", "col_12");
            $form->setFieldset('a_qualifier', 'D', _('Qualification'), "");
            //
            if ($form->val['dossier_instruction_secu'] == 't' && !empty($this->dossier_instruction_secu_id)) {
                $form->setBloc('dossier_instruction_secu','DF','','hidden');
            } else {
                $form->setBloc('dossier_instruction_secu_lien','DF','','hidden');
            }
            //
            if ($form->val['dossier_instruction_acc'] == 't' && !empty($this->dossier_instruction_acc_id)) {
                $form->setBloc('dossier_instruction_acc','DF','','hidden');
            } else {
                $form->setBloc('dossier_instruction_acc_lien','DF','','hidden');
            }
            $form->setFieldset('etablissement_categorie', 'F', '');
            $form->setBloc('etablissement_categorie', 'F');
            $form->setBloc('etablissement_categorie', 'F');
            
            // Fieldset "Urbanisme"
            $form->setBloc('interface_referentiel_ads', 'D', "", "col_12");
            $form->setBloc('interface_referentiel_ads', 'D', "", "col_12");
            $form->setFieldset('interface_referentiel_ads', 'D', _('Urbanisme'), "");
            $form->setFieldset('contraintes_urba_om_html', 'F', '');
            $form->setBloc('contraintes_urba_om_html', 'F');
            $form->setBloc('contraintes_urba_om_html', 'F');
        }
        //
        $form->setBloc(end($this->champs), 'F');
    }

    /**
     * Permet de définir le libellé des champs.
     *
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);

        //
        $form->setLib('dossier_instruction_secu_lien', _('dossier_instruction_secu'));
        $form->setLib('dossier_instruction_acc_lien', _('dossier_instruction_acc'));
        $form->setLib('etablissement_type_secondaire', _('type(s) secondaire(s)'));
        $form->setLib('geolocalise', _('Géolocalisé'));
        $form->setLib("interface_referentiel_ads", _("connecté au référentiel ADS"));
        $form->setLib("enjeu_erp", _("enjeu ERP"));
        $form->setLib("enjeu_ads", _("enjeu ADS"));

    }

    /**
     * Récupère le dossier d'instruction par rapport au dossier de coordination
     * et au service
     *
     * @param integer $dossier_coordination Identifiant du DC
     *
     * @return integer Identifiant du DI
     */
    function get_dossier_instruction_by_dossier_coordination_and_service($dossier_coordination, $service) {
        // Initialisation du résultat retourné
        $dossier_instruction = null;

        // Si dossier_coordination n'est pas vide
        if (!empty($dossier_coordination) && !empty($service)) {

            // Requête SQL
            $sql = "
                SELECT dossier_instruction
                FROM ".DB_PREFIXE."dossier_instruction
                    LEFT JOIN ".DB_PREFIXE."service
                        ON dossier_instruction.service = service.service
                WHERE LOWER(service.code) = '".$service."'
                    AND dossier_instruction.dossier_coordination = ".$dossier_coordination;
            $this->addToLog("get_dossier_instruction_by_dossier_coordination_and_service() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $dossier_instruction = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($dossier_instruction);

        }

        // Résultat retourné
        return $dossier_instruction;
    }

    /**
     * Récupère le code du type du dossier par rapport au dossier de coordination
     *
     * @param integer $dossier_coordination Identifiant du DC
     *
     * @return string Code du type du dossier
     */
    function get_dossier_type_code_by_dossier_coordination($dossier_coordination) {
        // Initialisation du résultat retourné
        $code = "";

        // Si dossier_coordination n'est pas vide
        if (!empty($dossier_coordination)) {

            // Requête SQL
            $sql = "
                SELECT LOWER(dossier_type.code)
                FROM ".DB_PREFIXE."dossier_type
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination
                        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                WHERE dossier_coordination.dossier_coordination = ".$dossier_coordination;
            $this->addToLog("get_dossier_type_code_by_dossier_coordination() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {

        // Si différent de l'ajout
        if ($this->getParameter("maj") != 0) {
            // Si le champ libelle existe
            $libelle = trim($this->getVal("libelle"));
            if (!empty($libelle)) {
                $ent .= " -> ".$libelle;
            }
            //
            $inst_etablissement = $this->get_inst_etablissement();
            $ent .= " [".$inst_etablissement->getVal("code")." - ".$inst_etablissement->getVal("libelle")."]";
        }

        // Change le fil d'Ariane
        return $ent;
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = "is_modifiable_and_supprimable";

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = "is_modifiable_and_supprimable";

        // ACTION - 004 - contrainte
        //
        $this->class_actions[4] = array(
            "identifier" => "contrainte",
            "view" => "view_contrainte",
            "permission_suffix" => "contrainte",
        );

        // ACTION - 005 - cloturer
        //
        $this->class_actions[5] = array(
            "identifier" => "cloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("cloturer"),
                "order" => 100,
                "class" => "lock-16",
            ),
            "view" => "formulaire",
            "method" => "cloturer",
            "button" => "cloturer",
            "permission_suffix" => "cloturer",
            "condition" => "is_cloturable",
        );

        // ACTION - 006 - decloturer
        //
        $this->class_actions[6] = array(
            "identifier" => "decloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("reouvrir"),
                "order" => 110,
                "class" => "unlock-16",
            ),
            "view" => "formulaire",
            "method" => "decloturer",
            "button" => "decloturer",
            "permission_suffix" => "decloturer",
            "condition" => "is_decloturable",
        );

        // ACTION - 015 - Marquer à enjeu
        //
        $this->class_actions[15] = array(
            "identifier" => "marquer_a_enjeu",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("Activer l'enjeu"),
                "order" => 100,
                "class" => "enjeu-erp-16",
            ),
            "view" => "formulaire",
            "method" => "marquer_a_enjeu",
            "button" => "marquer_a_enjeu",
            "permission_suffix" => "marquer_a_enjeu",
            "condition" => array(
                "is_modifiable_and_supprimable", "is_not_enjeu_erp",
            ),
        );

        // ACTION - 016 - Démarquer à enjeu
        //
        $this->class_actions[16] = array(
            "identifier" => "demarquer_a_enjeu",
            "portlet" => array(
                "type" => "action-direct-with-confirmation",
                "libelle" => _("Désactiver l'enjeu"),
                "order" => 100,
                "class" => "disable-enjeu-16",
            ),
            "view" => "formulaire",
            "method" => "demarquer_a_enjeu",
            "button" => "demarquer_a_enjeu",
            "permission_suffix" => "demarquer_a_enjeu",
            "condition" => array(
                "is_modifiable_and_supprimable", "is_enjeu_erp",
            ),
        );

        // ACTION - 007 - dossier_coordination_type_param
        //
        $this->class_actions[7] = array(
            "identifier" => "dossier_coordination_type_param",
            "view" => "view_dossier_coordination_type_param",
            "permission_suffix" => "json",
        );

        // ACTION - 008 - get_qualif_dc
        //
        $this->class_actions[8] = array(
            "identifier" => "get_qualif_dc",
            "view" => "get_qualif_dc",
            "permission_suffix" => "json",
        );

        // ACTION - 009 - get_data_etablissement
        //
        $this->class_actions[9] = array(
            "identifier" => "get_data_etablissement",
            "view" => "get_data_etablissement",
            "permission_suffix" => "json",
        );

        // ACTION - 021 - view_document_entrant_tab
        //
        $this->class_actions[21] = array(
            "identifier" => "document-entrant-tab",
            "view" => "view_document_entrant_tab",
            "permission_suffix" => "consulter",
        );

        // ACTION - 030 - Redirection vers le SIG
        $this->class_actions[30] = array(
            "identifier" => "localiser",
            "view" => "view_localiser",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 031 - Redirection vers le SIG pour la sélection de DC
        $this->class_actions[31] = array(
            "identifier" => "localiser_selection",
            "view" => "view_localiser_selection",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 032 - geocoder
        //
        $this->class_actions[32] = array(
            "identifier" => "geocoder",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("géolocaliser"),
                "order" => 110,
                "class" => "sig-16",
            ),
            "view" => "formulaire",
            "method" => "geocoder",
            "permission_suffix" => "consulter",
            "condition" => array(
                "is_not_geolocalised",
                "is_option_sig_enabled",
                "is_dossier_coordination_localizable"
            ),
        );

        // ACTION - 033 - plot_owner
        //
        $this->class_actions[33] = array(
            "identifier" => "plot_owner",
            "view" => "view_plot_owner",
            "permission_suffix" => "consulter",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 034 - overlay_etablissements_proches_qualification_formulaire
        //
        $this->class_actions[34] = array(
            "identifier" => "overlay_etablissements_proches_qualification_formulaire",
            "view" => "view_overlay_etablissements_proches_qualification_formulaire",
            "permission_suffix" => "modifier",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 035 - overlay_etablissements_proches_qualification_tableau
        //
        $this->class_actions[35] = array(
            "identifier" => "overlay_etablissements_proches_qualification_tableau",
            "view" => "view_overlay_etablissements_proches_qualification_tableau",
            "permission_suffix" => "modifier",
            "condition" => "is_option_sig_enabled",
        );

        // ACTION - 250 - rename_all_libelle
        // Action utilitaire à des fins d'administration
        $this->class_actions[250] = array(
            "identifier" => "rename_all_libelle",
            "view" => "view_rename_all_libelle",
        );
    }


    /**
     * VIEW - view_document_entrant_tab.
     *
     * @return void
     */
    function view_document_entrant_tab() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        if (($this->getVal("dossier_autorisation_ads") == ""
             && $this->getVal("dossier_instruction_ads") == "")
            || $this->f->is_swrod_enabled() === false) {
            header("Location:../scr/soustab.php?obj=piece&retourformulaire=dossier_coordination&idxformulaire=".$this->getVal("dossier_coordination"));
            exit();
        }
        //
        $this->f->displaySubTitle("-> "._("documents entrants"));
        //
        printf('
<div id="document-entrant-tabs">
<ul>
<li>
    <a id="onglet-document_entrant_interne__contexte_dc" href="../scr/soustab.php?obj=piece&retourformulaire=dossier_coordination&idxformulaire=%1$s">Interne</a>
</li>
<li>
    <a id="onglet-document_entrant_guichet_unique__contexte_dc" href="../scr/form.php?obj=piece&action=22&idx=0&dc=%1$s">Guichet unique</a>
</li>
</ul>
</div>

',
            $this->getVal("dossier_coordination")
        );
    }

    /**
     * VIEW - get_qualif_dc.
     *
     * Retourne en JSON la qualification du DC posté
     * 
     * @return void
     */
    function get_qualif_dc() {
        //
        $this->f->disableLog();
        //
        $postvar = $this->getParameter("postvar");
        $id_dc = $postvar['id_dc'];
        $sql = "SELECT
            etablissement_type as type,
            etablissement_categorie as categorie,
            etablissement_locaux_sommeil as locaux
        FROM ".DB_PREFIXE."dossier_coordination
        WHERE dossier_coordination = ".$id_dc;
        //
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);
        echo json_encode(array(
            "type" => $row['type'],
            "categorie" => $row['categorie'],
            "locaux" => $row['locaux'],
            )
        );
    }

    /**
     * CONDITION - is_modifiable_and_supprimable.
     *
     * Condition pour afficher les boutons modifier et supprimer
     *
     * @return boolean
     */
    function is_modifiable_and_supprimable() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        if ($dossier_cloture == "f") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_cloturable.
     * 
     * Condition pour afficher le bouton cloturer
     *
     * @return boolean
     */
    function is_cloturable() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        $a_qualifier = $this->getVal("a_qualifier");
        //
        $is_periodic = $this->is_periodic();
        //
        $last_date_visite = $this->get_last_date_visite();

        // Si le dossier de coordination est périodique
        if ($is_periodic == true) {
            // Si le dossier d'instruction n'est pas à qualifié, n'est pas
            // clôturé et qu'il possède une visite
            if (($a_qualifier == "f" || $a_qualifier == "Non")
                && ($dossier_cloture == "f" || $dossier_cloture == "Non")
                && $last_date_visite != false) {
                //
                return true;
            } else {
                //
                return false;
            }
        }

        // Si le dossier d'instruction n'est pas à qualifié
        // et n'est pas clôturé
        if (($a_qualifier == "f" || $a_qualifier == "Non")
            && ($dossier_cloture == "f" || $dossier_cloture == "Non")) {
            //
            return true;
        } else {
            //
            return false;
        }
    }

    /**
     * CONDITION - is_decloturable.
     * 
     * Condition pour afficher le bouton decloturer
     *
     * @return boolean
     */
    function is_decloturable() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        $a_qualifier = $this->getVal("a_qualifier");
        //
        $is_periodic = $this->is_periodic();

        // Si le dossier de coordination est périodique
        if ($is_periodic == true) {
            // Si le dossier de coordination n'est pas à qualifié, n'est pas
            // clôturé et que le dossier de coordination n'est pas clôturé
            if (($a_qualifier == "f" || $a_qualifier == "Non")
                && ($dossier_cloture == "t" || $dossier_cloture == "Oui")) {
                //
                return false;
            } else {
                //
                return false;
            }
        }

        // Si le dossier d'instruction n'est pas à qualifié
        // et n'est pas clôturé
        if (($a_qualifier == "f" || $a_qualifier == "Non")
            && ($dossier_cloture == "t" || $dossier_cloture == "Oui")) {
            //
            return true;
        }
    }

    /**
     * Indique si le dossier de coordination est de type périodique.
     *
     * @param integer $id Identifiant du dossier de coordination
     *
     * @return boolean
     */
    function is_periodic($id=null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_coordination = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_coordination = new dossier_coordination($id);
        }

        // Récupère le type du dossier de coordination
        $dossier_coordination_type_id = $dossier_coordination->getVal('dossier_coordination_type');

        // Instance de la classe dossier_coordination_type
        require_once '../obj/dossier_coordination_type.class.php';
        $dossier_coordination_type = new dossier_coordination_type($dossier_coordination_type_id);

        // Si le type correspond au type des visites périodique
        if ($dossier_coordination_type->is_periodic() == true) {

            //
            return true;
        }

        //
        return false;
    }

    /**
     * TREATMENT - cloturer.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function cloturer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        // Si le dossier de coordination à un dossier d'instruction sécu
        if (!empty($this->dossier_instruction_secu_id)) {
            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction_secu = new dossier_instruction($this->dossier_instruction_secu_id);
            // Si le DI n'est pas clôturé, on essaye de le clôturer.
            if ($dossier_instruction_secu->is_closed() !== true) {
                // Si le dossier d'instruction ne peut pas être clôturé
                if ($dossier_instruction_secu->is_cloturable() == false) {
                    // Stop le traitement
                    $msg = _("Le dossier d'instruction %s ne peut pas etre cloture.");
                    $this->addToMessage(sprintf($msg, "<span class='bold'>".$dossier_instruction_secu->getVal('libelle')."</span>"));
                    // Return
                    return $this->end_treatment(__METHOD__, false);
                }
                // Clôture le dossier d'instruction
                $dossier_instruction_secu_is_cloturer = $dossier_instruction_secu->cloturer($dossier_instruction_secu->val);
                // Si le dossier d'instruction ne s'est pas clôturé
                if ($dossier_instruction_secu_is_cloturer == false) {
                    // Stop le traitement
                    $msg = _("Erreur lors de la cloture du dossier d'instruction %s.");
                    $msg .= " "._("Veuillez contacter votre administrateur.");
                    $this->addToMessage(sprintf($msg, "<span class='bold'>".$dossier_instruction_secu->getVal('libelle')."</span>"));
                    // Return
                    return $this->end_treatment(__METHOD__, false);
                }
            }
        }

        // Si le dossier de coordination à un dossier d'instruction acc
        if (!empty($this->dossier_instruction_acc_id)) {
            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction_acc = new dossier_instruction($this->dossier_instruction_acc_id);
            // Si le DI n'est pas clôturé, on essaye de le clôturer.
            if ($dossier_instruction_acc->is_closed() !== true) {
                // Si le dossier d'instruction ne peut pas être clôturé
                if ($dossier_instruction_acc->is_cloturable() == false) {
                    // Stop le traitement
                    $msg = _("Le dossier d'instruction %s ne peut pas etre cloture.");
                    $this->addToMessage(sprintf($msg, "<span class='bold'>".$dossier_instruction_acc->getVal('libelle')."</span>"));
                    // Return
                    return $this->end_treatment(__METHOD__, false);
                }
                // Clôture le dossier d'instruction
                $dossier_instruction_acc_is_cloturer = $dossier_instruction_acc->cloturer($dossier_instruction_acc->val);
                // Si le dossier d'instruction ne s'est pas clôturé
                if ($dossier_instruction_acc_is_cloturer == false) {
                    // Stop le traitement
                    $msg = _("Erreur lors de la cloture du dossier d'instruction %s.");
                    $msg .= " "._("Veuillez contacter votre administrateur.");
                    $this->addToMessage(sprintf($msg, "<span class='bold'>".$dossier_instruction_acc->getVal('libelle')."</span>"));
                    // Return
                    return $this->end_treatment(__METHOD__, false);
                }
            }
        }

        // On clôture le dossier de coordination
        $dossier_coordination_is_cloturer = $this->manage_cloturing("cloture", $val);

        // Si le dossier de coordination est clôturé
        if ($dossier_coordination_is_cloturer == false) {
            // Stop le traitement
            $msg = _("Erreur lors de la cloture du dossier de coordination.");
            $msg .= " "._("Veuillez contacter votre administrateur.");
            $this->addToMessage(sprintf($msg, "<span class='bold'>".$dossier_instruction_secu->getVal('libelle')."</span>"));
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Si le dossier de coordination est périodique
        if ($this->is_periodic() == true) {

            // Instance de la classe etablissement
            require_once '../obj/etablissement.class.php';
            $etablissement = new etablissement($this->getVal("etablissement"));

            // Ajout du nouveau dossier de coordination périodique
            $values = array(
                "date_visite" => $this->get_last_date_visite(),
            );
            $handle_dossier_coordination_periodique = $etablissement->handle_dossier_coordination_periodique("lock", $values);

            // Si le traitement de clôture échoue
            if ($handle_dossier_coordination_periodique == false) {
                // Message d'erreur
                $this->addToMessage($etablissement->msg);
                // Return
                return $this->end_treatment(__METHOD__, false);
            }
        }

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[211] Clôture d'une AT
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && substr($this->getVal("libelle"), 0, 2) === 'AT') {
            //
            $infos = array(
                "dossier_coordination" => $this->getVal("dossier_coordination"),
                "dossier_instruction" => $this->getVal("dossier_instruction_ads"),
                "message" => "clos",
                "date" => date("d/m/Y"),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(211, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (211) du référentiel ADS OK."));
        }
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Récupère la dernière date de visite entre le dossier d'instruction
     * de sécurité et d'accessibilité
     *
     * @param integer $id Identifiant du dossier de coordination
     *
     * @return mixed Date ou false
     */
    function get_last_date_visite($id = null) {
        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_coordination = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_coordination = new dossier_coordination($id);
        }

        //
        $last_date_visite_secu = "";
        $last_date_visite_secu = "";

        // Si le dossier de coordination à un dossier d'instruction si
        if (!empty($dossier_coordination->dossier_instruction_secu_id)) {

            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_coordination->dossier_instruction_secu_id);

            // Récupère la dernière date de visite
            $last_date_visite_secu = $dossier_instruction->get_date_visite($dossier_instruction->get_last_visite());
        }

        // Si le dossier de coordination à un dossier d'instruction acc
        if (!empty($dossier_coordination->dossier_instruction_acc_id)) {

            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_coordination->dossier_instruction_acc_id);

            // Récupère la dernière date de visite
            $last_date_visite_acc = $dossier_instruction->get_date_visite($dossier_instruction->get_last_visite());
        }

        // Retourne seulement la date SI s'il n'y a pas d'ACC
        if (!empty($last_date_visite_secu) && empty($last_date_visite_acc)) {
            //
            return $last_date_visite_secu;
        }

        // Retourne seulement la date ACC s'il n'y a pas de SI
        if (empty($last_date_visite_secu) && !empty($last_date_visite_acc)) {
            //
            return $last_date_visite_acc;
        }

        //
        if (!empty($last_date_visite_secu) && !empty($last_date_visite_acc)) {

            // Compare les deux dates pour prendre la plus récente
            $last_date_visite_secu = new DateTime($last_date_visite_secu);
            $last_date_visite_acc = new DateTime($last_date_visite_acc);
            //
            if ($last_date_visite_secu > $last_date_visite_acc) {
                //
                return $last_date_visite_secu;
            } else {
                //
                return $last_date_visite_acc;
            }
        }

        //
        return false;
    }

    /**
     * TREATMENT - decloturer.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function decloturer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_cloturing("uncloture", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - marquer_a_enjeu.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function marquer_a_enjeu($val = array()) {
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->update_autoexecute(
            array(
                'enjeu_erp' => true,
            ),
            null,
            false
        );
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("Marqueur 'enjeu ERP' activé.");

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[207] Mise à jour du dossier d’instruction « Dossier à enjeux ERP »
         * Déclencheur :
         *  - L'option ADS est activée
         *  - Le DC est connecté au référentiel ADS
         *  - le dossier est de type PC
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && substr($this->getVal("libelle"), 0, 2) === 'PC') {
            //
            $infos = array(
                "dossier_coordination" => $this->getVal("dossier_coordination"),
                "dossier_instruction" => $this->getVal("dossier_instruction_ads"),
                "Dossier à enjeux ERP" => "oui",
            );
            $ret = $this->f->execute_action_to_referentiel_ads(207, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (207) du référentiel ADS. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (207) du référentiel ADS OK."));
        }

        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - demarquer_a_enjeu.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire.
     *
     * @return boolean
     */
    function demarquer_a_enjeu($val = array()) {
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->update_autoexecute(
            array(
                'enjeu_erp' => false,
            ),
            null,
            false
        );
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage("Marqueur 'enjeu ERP' désactivé.");

        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[207] Mise à jour du dossier d’instruction « Dossier à enjeux ERP »
         * Déclencheur :
         *  - L'option ADS est activée
         *  - Le DC est connecté au référentiel ADS
         *  - le dossier est de type PC
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && substr($this->getVal("libelle"), 0, 2) === 'PC') {
            //
            $infos = array(
                "dossier_coordination" => $this->getVal("dossier_coordination"),
                "dossier_instruction" => $this->getVal("dossier_instruction_ads"),
                "Dossier à enjeux ERP" => "non",
            );
            $ret = $this->f->execute_action_to_referentiel_ads(207, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification (207) du référentiel ADS. Contactez votre administrateur."));
                return false;
            }
            $this->addToMessage(_("Notification (207) du référentiel ADS OK."));
        }

        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_cloturing($mode = null, $val = array()) {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "cloture" && $mode != "uncloture") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "cloture") {
            $valF = array(
                "dossier_cloture" => 't',
                "date_cloture" => date("Y-m-d"),
            );
            $valid_message = _("Le %s a ete correctement cloture.");
        }
        if ($mode == "uncloture") {
            $valF = array(
                "dossier_cloture" => 'f',
                "date_cloture" => null,
            );
            $valid_message = _("Le %s a ete correctement reouvert.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, _("dossier_coordination"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($valid_message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog("manage_cloturing() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object $form Formumaire
     * @param int    $maj  Mode d'insertion
     */
    function setOnchange(&$form,$maj) {
        parent::setOnchange($form,$maj);

        $form->setOnchange('etablissement',
            'filterSelect(this.value,
            \'dossier_coordination_parent\',
            \'etablissement\',
            \'dossier_coordination\');
            updateEtablissementData(this.value, \'etablissement\');');
        $form->setOnchange('dossier_coordination_parent',
            'updateEtablissementData(this.value, \'dossier_coordination\');');
            //'recupererDCparent(this.value);');
        $form->setOnchange('dossier_coordination_type',
            'recuperer_type_dossier_coordination(this.value);');
    }

    /**
     * Compose le tableau de paramétrage du select "dossier_coordination_parent".
     *
     * Ce select liste tous les dossiers de coordination parents de
     * l'établissement présent dans le formulaire en contexte.
     * 
     * @param  object  $form  Formulaire.
     * @param  integer $maj   Mode d'insertion.
     * @param  string  $champ Champ activant le filtre.
     *
     * @return array Tableau de paramétrage du select.
     */
    function loadSelect_dossier_coordination_parent(&$form, $maj, $champ) {

        // Initialisation du tableau de paramétrage du select :
        // - la clé 0 contient le tableau des valeurs,
        // - la clé 1 contient le tableau des labels,
        // - les clés des tableaux doivent faire correspondre le couple valeur/label.
        $contenu = array(
            0 => array('', ),
            1 => array(_('choisir')." "._("dossier_coordination_parent"), ),
        );

        // Récupération de l'identifiant de l'établissement :
        // (par ordre de priorité)
        // - si une valeur est postée : c'est le cas lors du rechargement d'un
        //   formulaire et que le select doit être peuplé par rapport aux
        //   données saisies dans le formulaire,
        // - si la valeur est passée en paramètre : c'est le cas lors d'un
        //   appel via le script app/filterSelect.php qui effectue un 
        //   $object->setParameter($linkedField, $idx); lors d'un appel ajax
        //   depuis le formulaire (sélection d'un établissement via
        //   l'autocomplete qui recharge en ajax la liste des dc parents),
        // - si la valeur est dans l'enregistrement du dc sur lequel on se 
        //   trouve : c'est le cas lors de la première ouverture du formulaire
        //   en modification par exemple.
        $etablissement = "";
        if (isset($_POST[$champ])) {
            $etablissement = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $etablissement = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $etablissement = $form->val[$champ];
        }

        // Si aucun établissement ne correspond alors on retourne le select
        // vide car on ne peut pas trouver les dc parents d'aucun
        // établissement.
        if (empty($etablissement)) {
            return $contenu;
        }

        // Récupération de l'identifiant du dossier de coordination :
        // - soit nous sommes sur une instance de dc standard et il a une clé
        //   primaire,
        // - soit nous sommes en ajout ou sur une instance générale et alors
        //   on positionne 0 afin de ne pas faire échouer la requête suite au
        //   remplacement (il n'existe aucun DC qui a l'id 0 donc aucun DC ne
        //   sera exclu).
        $dossier_coordination = $this->getVal($this->clePrimaire);
        if ($maj == 0
            || $dossier_coordination === ""
            || $dossier_coordination === null) {
            $dossier_coordination = 0;
        }

        // La requête $sql_dossier_coordination_parent_by_etablissement est
        // définie dans le script inclus.
        // La requête qui compose le select contient deux variables à remplacer :
        // - <idx_etab> : l'identifiant de l'établissement duquel on recherche
        //   les dc parents,
        // - <idx_dc> : l'identifiant du dc sur lequel on se trouve si c'est le
        //   cas afin de l'exclure des résultats de la requête.
        if (file_exists ("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        }
        $sql_dossier_coordination_parent_by_etablissement = str_replace(
            '<idx_etab>',
            intval($etablissement),
            $sql_dossier_coordination_parent_by_etablissement
        );
        $sql_dossier_coordination_parent_by_etablissement = str_replace(
            '<idx_dc>',
            intval($dossier_coordination),
            $sql_dossier_coordination_parent_by_etablissement
        );

        // Exécution de la requête
        $res = $this->f->db->query($sql_dossier_coordination_parent_by_etablissement);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql_dossier_coordination_parent_by_etablissement."\");",
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);

        // Chaque résultat de la requête est ajouté au tableau de paramétrage
        // du select :
        // - la clé 0 contient le tableau des valeurs,
        // - la clé 1 contient le tableau des labels.
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $contenu[0][] = $row['dossier_coordination'];
            $contenu[1][] = $row['libelle'];
        }

        // On retourne le tableau de paramétrage
        return $contenu;
    }

    /**
     * VIEW - contrainte.
     *
     * Vue des contraintes du dossier de coordination
     *
     * Cette vue permet de gérer le contenu de l'onglet "Contraintes" sur un dossier
     * de coordination. Cette vue spécifique est nécessaire car l'ergonomie standard du
     * framework ne prend pas en charge ce cas.
     * C'est ici la vue spécifique des contraintes liées à l'établissement qui est
     * affichée directement au clic de l'onglet au lieu du soustab.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_contrainte() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Récupération des variables GET
        ($this->f->get_submitted_get_value('idxformulaire')!==null ? $idxformulaire = 
            $this->f->get_submitted_get_value('idxformulaire') : $idxformulaire = "");
        ($this->f->get_submitted_get_value('retourformulaire')!==null ? $retourformulaire = 
            $this->f->get_submitted_get_value('retourformulaire') : $retourformulaire = "");

        // Objet à charger
        $obj = "lien_contrainte_dossier_coordination";
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=$obj";
        $url .= "&idx=0";
        $url .= "&action=4";
        $url .= "&retourformulaire=$retourformulaire";
        $url .= "&idxformulaire=$idxformulaire";
        $url .= "&retour=form";
        // Affichage du container permettant le raffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * VIEW - view_dossier_coordination_type_param.
     * 
     * Permet de récupérer et d'afficher les données de paramétrage
     * du type du dossier de coordination.
     * 
     * Son instanciation est identique à la fonction formulaire.
     *
     * @return void
     */
    function view_dossier_coordination_type_param() {
        /*
                        $enteteTab, $validation, $maj, &$db, $postVar, $aff,
                        $DEBUG = false, $idx, $premier = 0, $recherche = "",
                        $tricol = "", $idz = "", $selectioncol = "",
                        $advs_id = "", $valide = "", $retour = "", $actions = array(),
                        $extra_parameters = array()) {
                            */
        // Désactive les logs
        $this->f->disableLog();
        // Initialisation de la variable de retour
        $return = "";
        //
        $postvar = $this->getParameter("postvar");
        // Si la valeur selected est envoyé
        if (!empty($postvar['selected'])) {

            // Instance de la classe dossier_coordination_type
            require_once '../obj/dossier_coordination_type.class.php';
            $dossier_coordination_type = new dossier_coordination_type($postvar['selected']);
            // Récupère les paramétres
            $return = $dossier_coordination_type->get_dossier_coordination_type_param();

            // Instance de la classe om_formulaire
            require_once "../obj/om_formulaire.class.php";
            $om_formulaire = new om_formulaire(null, 0, 0, array());

            // Récupération du tag pour afficher un champ obligatoire
            $return["required_tag"] = $om_formulaire->required_tag;
            $return["is_periodic"] = $dossier_coordination_type->is_periodic();
            $return["etablissement_already_require"] = false;
            // Si le champ est dans la liste des champs obligatoire
            if (in_array("etablissement", $this->required_field)) {
                //
                $return["etablissement_already_require"] = true;
            }
        }
        // Affiche tableau json
        echo json_encode($return);
    }

    /**
     * Méthode permettant de récupérer les id des contacts liés au DC
     *
     * @param string $id Identifiant du DC
     */
    function listeContact($id) {
        //
        $sql = "SELECT contact
            FROM ".DB_PREFIXE."lien_dossier_coordination_contact
            WHERE dossier_coordination = ".$id;
        $res = $this->f->db->query($sql);
        $this->f->addToLog("listeContact(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        // Stockage du résultat dans un tableau
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $this->valIdContact[]=$row['contact'];
        }
    }

    function get_list_contact() {
        // On récupère l'id de l'enregistrement du dossier de coordination
        $dc_idx = $this->getVal($this->clePrimaire);
        // Si le dossier de coordination instancié ne concerne pas
        // un enregistrement en base de données
        if (empty($dc_idx)) {
            // On retourne un tableau vide en tant que liste des contacts
            return array();
        }
        //
        $sql = "
        SELECT 
            *
        FROM 
            ".DB_PREFIXE."lien_dossier_coordination_contact
                LEFT JOIN ".DB_PREFIXE."contact
                    ON lien_dossier_coordination_contact.contact=contact.contact
        WHERE 
            dossier_coordination = ".$dc_idx;
        $res = $this->f->db->query($sql);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$sql."\")", 
            VERBOSE_MODE
        );
        $this->f->isDatabaseError($res);
        // Stockage du résultat dans un tableau
        $result = array();
        while ($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $result[]=$row;
        }
        return $result;
    }

    /**
     *  Formulaire au contenu spécifique disposé en bas de page
     */
    function formSpecificContent($maj) {
        // On affiche les actions pour ajouter les contacts uniquement lors
        // de l'ajout du DC. Sinon les contacts sont gérés dans l'onglet 
        // Contacts dédié.
        // ajout de la liste de contacts s'il y en a
        $values_type = "";
        $values = false;
        //
        if (isset ($this->valIdContact) AND
            !empty($this->valIdContact)) {
            $values_type = "existantes";
            $values = true;
        } elseif ( isset ($this->postedIdContact) AND
            !empty($this->postedIdContact) )  {
            $values_type = "postées";
            $values = true;
        }
        if (($maj == 0) || $values == true) {
            $this->fieldsetContact($maj, $values_type);
        }
    }

    /**
     *  Sous-formulaire au contenu spécifique disposé en bas de page
     */
    function sousFormSpecificContent($maj) {

        $this->formSpecificContent($maj);
    }

    /**
     * Affichage du fieldset de la liste des contacts
     */
    function fieldsetContact($maj, $values_type) {

        if(($maj == 0 || $maj == 1) AND !$this->correct) {
            $linkable = true;
        } else {
            $linkable = false;
        }

        // Conteneur de la listes des contacts
        echo "<div id=\"liste_contact\" class=\"contact_hidden_bloc col_12\">";
        echo "<fieldset class=\"cadre ui-corner-all ui-widget-content startClosed\">";
        echo "  <legend class=\"ui-corner-all ui-widget-content ui-state-active\">"
                ._("Contacts")."</legend>";

        require_once "../obj/contact.class.php";

        // Affichage de la synthèse
        echo "<div id=\"listeContacts col_12\">";  
        if ($values_type == "existantes") {            
            foreach ($this->valIdContact as $contact_id) {
                $contact = new contact($contact_id);
                $contact -> afficherSynthese($linkable);
                $contact -> __destruct();
            }
            
        } elseif ($values_type == "postées")  {
            foreach ($this->postedIdContact as $contact_id) {
                $contact = new contact($contact_id);
                $contact -> afficherSynthese($linkable);
                $contact -> __destruct();
            }
        }
        if ($linkable) {
            echo "<span id=\"add_contact\"
                    class=\"om-form-button add-16\">".
                    _("Ajouter un contact")
                ."</span>";
        }
        echo "</div>";
        echo "</fieldset>";
        echo "</div>";
    }

    /**
     * Fieldset de la liste des contacts
     */
    function formContact($maj, $mode) {

        $this->listeContact($this->getVal('dossier_coordination'));

        if(($maj == 0 || $maj == 1) AND !$this->correct) {
            $linkable = true;
        } else {
            $linkable = false;
        }

        // Conteneur de la listes des contacts
        echo "<div id=\"liste_contact\" class=\"contact_hidden_bloc col_12\">";
        echo "<fieldset class=\"cadre ui-corner-all ui-widget-content startClosed\">";
        echo "  <legend class=\"ui-corner-all ui-widget-content ui-state-active\">"
                ._("Contacts")."</legend>";

        require_once "../obj/contact.class.php";

        // Affichage de la synthèse
        echo "<div id=\"listeContacts col_12\">";  
        if (isset ($this->valIdContact) AND
            !empty($this->valIdContact)) {
            
            foreach ($this->valIdContact as $contact_id) {
                $contact = new contact($contact_id);
                $contact -> afficherSynthese($linkable);
                $contact -> __destruct();
            }
            
        } elseif ( isset ($this->postedIdContact) AND
            !empty($this->postedIdContact) )  {
            foreach ($this->postedIdContact as $contact_id) {
                $contact = new contact($contact_id);
                $contact -> afficherSynthese($linkable);
                $contact -> __destruct();
            }
        }
        if (($maj == 0 || $maj == 1) AND $linkable) {
            echo "<span id=\"add_contact\"
                    class=\"om-form-button add-16\">".
                    _("Ajouter un contact")
                ."</span>";
        }
        echo "</div>";
        echo "</fieldset>";
        echo "</div>";
    }

    /**
     * Fonction générique permettant de récupérer les données d'un champ postées.
     *
     * @param string $champ Nom du champ
     *
     * @return mixed Valeur posté
     */
    function getPostedValues($champ) {
        // Récupération des demandeurs dans POST
        if (isset($_POST[$champ]) ) {
            //
            return $_POST[$champ];
        }
    }

    function setvalF($val = array()) {
        parent::setvalF($val);

        // Récupération des id demandeurs postés
        $this->getPostedContacts('contact');

        // Les champs suivants ne sont jamais modifiés via le formulaire
        // mais seulement via des actions dédiées qui n'utilisent pas setValF.
        unset($this->valF["enjeu_erp"]);
        unset($this->valF["enjeu_ads"]);
        unset($this->valF["interface_referentiel_ads"]);
    }

    /**
     * Methode de recupération des valeurs postées
     */
    function getPostedContacts() {
        if (isset($_POST['contact']) AND
                !empty($_POST['contact'])) {
            $this->postedIdContact = $_POST['contact'];
        }
    }

    /**
     * Gestion des liens entre le DC et les contacts recemment ajoutés
     */
    function insertLinkDossierCoordinationContact() {

        // Suppression des anciens contacts
        $this->deleteLinkDossierCoordinationContact();

        // Ajout des nouveaux liens
        require_once "../obj/lien_dossier_coordination_contact.class.php";
        foreach ($this->postedIdContact as $contact_id) {
            $this->addLinkDossierCoordinationContact($contact_id);
        }
    }

    /**
     * Fonction permettant d'ajouter un lien
     * entre les tables dossier_coordination et contact
     */
    function addLinkDossierCoordinationContact($id) {

        $lienAjout = new lien_dossier_coordination_contact("]");
        $lien = array('lien_dossier_coordination_contact' => "",
                           'dossier_coordination' => $this->valF['dossier_coordination'],
                           'contact' => $id);
        $lienAjout->ajouter($lien);
        $lienAjout->__destruct();
    }

    /**
     * Fonction permettant de supprimer un lien
     * entre les tables dossier_coordination et contact
     */
    function deleteLinkDossierCoordinationContact() {

        // Suppression
        $sql = "DELETE FROM ".DB_PREFIXE."lien_dossier_coordination_contact ".
                "WHERE dossier_coordination='".$this->valF['dossier_coordination']."'";
        // Exécution de la requete de suppression des contacts liés au DC
        $res = $this->f->db->query($sql);
        // Logger
        $this->f->addToLog("supprimer(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Permet de renommer tous les dossiers de coordination avec le libellé correct.
     *
     * @return void
     */
    function view_rename_all_libelle() {
        // On récupère la liste des ID de DC
        $sql = "
        SELECT
            dossier_coordination.dossier_coordination,
            dossier_coordination.libelle,
            dossier_coordination.dossier_coordination_type
        FROM
            ".DB_PREFIXE."dossier_coordination
        ";
        $res = $this->f->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $dc_all = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $dc_all[] = $row;
            echo "
UPDATE dossier_coordination SET libelle='".$row["libelle"]."' WHERE dossier_coordination=".$row["dossier_coordination"].";\n
";
        }
        //
        $counter = 0;
        //
        foreach ($dc_all as $key => $value) {
            $old_libelle = $value["libelle"];
            $new_libelle = $this->generate_dossier_coordination_libelle(
                $value["dossier_coordination_type"],
                $value["dossier_coordination"]
            );
            //
            $this->addToLog(__METHOD__."(): ".$old_libelle." / ".$new_libelle, VERBOSE_MODE);
            //
            if ($old_libelle != $new_libelle) {
                $valF = array("libelle" => $new_libelle, );
                $res = $this->f->db->autoExecute(
                    DB_PREFIXE.$this->table, 
                    $valF, DB_AUTOQUERY_UPDATE, 
                    $this->getCle($value["dossier_coordination"])
                );
                $this->addToLog(
                    __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($value["dossier_coordination"])."\")", 
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                $counter++;
            }
        }
        //
        $this->addToLog(__METHOD__."(): ".$counter." / ".count($dc_all), VERBOSE_MODE);
    }

    /**
     * Met à jour le champ "autorite_police_encours".
     *
     * @param boolean $value true ou false
     *
     * @return boolean
     */
    function update_autorite_police_encours($value) {
        // Traitement à faire avant la modification
        $trigger = $this->trigger_update_autorite_police_encours($value);

        // Si le traitement s'est bien déroulé
        if ($trigger == true) {

            // Valeur à mettre à jour
            $valF = array(
                "autorite_police_encours" => $value,
            );
            // Met à jour l'enregistrement
            $update = $this->update_autoexecute($valF);

            // Si la mise à jour est ok
            if ($update == true) {
                // Traitement à faire après la modification
                $trigger_after = $this->trigger_after_update_autorite_police_encours($value);

                // Si le traitement ne s'est pas bien déroulé
                if ($trigger_after == false) {
                    // Stop le traitement
                    return false;
                }

            } else {
                // Stop le traitement
                return false;
            }

        } else {
            // Stop le traitement
            return false;
        }

        //
        return true;
    }

    /**
     * Traitement à faire avant la mise à jour du champ autorite_police_encours.
     *
     * @param boolean $value true ou false
     *
     * @return boolean
     */
    function trigger_update_autorite_police_encours($value) {
        //
        return true;
    }

    /**
     * Traitement à faire après la mise à jour du champ autorite_police_encours.
     *
     * @param boolean $value true ou false
     *
     * @return boolean
     */
    function trigger_after_update_autorite_police_encours($value) {
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);

        // Si le dossier de coordination est rattaché à un établissement
        $etablissement_id = $this->getVal('etablissement');
        if (!empty($etablissement_id)) {
            // Si la valeur à mettre à jour est true
            if ($value == true) {
                //
                $update = $this->update_etablissement_autorite_police_encours($value);
            }

            // Si la valeur à mettre à jour est false
            if ($value == false) {
                // Récupère la liste des dossiers de coordination de 
                // l'établissement
                require_once '../obj/etablissement.class.php';
                $etablissement = new etablissement($etablissement_id);
                $list_dossier_coordination = $etablissement->get_list_dossier_coordination_by_etablissement($etablissement->getVal($etablissement->clePrimaire), $id);
                // Vérifie les autres dossier de coordination de l'établissement
                // n'ont pas d'autorité de police encours
                if ($this->is_autorite_police_encours($list_dossier_coordination) == false) {
                    // Met à jour l'établissement
                    $update = $this->update_etablissement_autorite_police_encours($value);
                }
            }

            // Si la modification échoue
            if ($update == false) {
                return false;
            }
        }

        //
        return true;
    }

    /**
     * TREATMENT - mark_as_connected_to_referentiel_erp.
     *
     * Cette méthode permet de positionner le marqueur 
     * 'interface_referentiel_ads' à 'true'. Cela signifie que le dossier est
     * connecté au référentiel ADS.
     *
     * @return boolean
     */
    function mark_as_connected_to_referentiel_ads() {
        $this->begin_treatment(__METHOD__);
        //
        $ret = $this->update_autoexecute(
            array(
                "interface_referentiel_ads" => true,
            ),
            null,
            false
        );
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }
        $this->addToMessage(_("Le dossier est désormais 'connecté avec le référentiel ADS'."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Met à jour le champ "autorite_police_encours" de l'établissement.
     *
     * @param boolean $valeur true ou false
     *
     * @return boolean
     */
    function update_etablissement_autorite_police_encours($valeur) {
        // Identifiant de l'établissement
        $id = $this->getVal('etablissement');

        // Instance de la classe etablissement
        require_once '../obj/etablissement.class.php';
        $etablissement = new etablissement($id);
        // Mise à jour de l'établissement
        $update = $etablissement->update_autorite_police_encours($valeur);
        //
        return $update;
    }

    /**
     * Vérifie que les dossiers de coordination n'ont pas d'autorité de police
     * en cours.
     *
     * @param array $list_dossier_coordination Liste des DC
     *
     * @return boolean
     */
    function is_autorite_police_encours($list_dossier_coordination) {

        // Pour chaque dossier de coordination
        foreach ($list_dossier_coordination as $dossier_coordination_id) {

            // Instance de la classe dossier_coordination
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($dossier_coordination_id);

            // Si le champ "autorite_police_encours" est à true
            if ($this->f->get_val_boolean($dossier_coordination->getVal('autorite_police_encours')) == true) {
                //
                return true;
            }
        }
        //
        return false;
    }

    /**
     *
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {

        // On appelle la methode de la classe parent
        parent::verifier($val);
        
        // Si l'établissement est ERP les type et catégorie doivent être saisis
        $type_principal = $this->valF["etablissement_type"];
        $types_secondaires = $this->getPostedValues('etablissement_type_secondaire');
        $categorie = $this->valF["etablissement_categorie"];
        $correct = true;
        if ($this->valF["erp"]=='true'){
            switch ($type_principal) {
                case null:
                case "":
                    $this->addToMessage(_("Le").' <span class="bold">'._("type").'</span> '._("de l'etablissement doit etre saisi lorsqu'il s'agit d'un ERP."));
                    $correct = false;
                    break;
            }
            switch ($categorie) {
                case null:
                case "":
                    $this->addToMessage(_("La").' <span class="bold">'._("categorie").'</span> '._("de l'etablissement doit etre saisie lorsqu'il s'agit d'un ERP."));
                    $correct = false;
                    break;
            }
        }

        //
        if ($correct == false) {
            $this->correct = false;
            return false;
        }
        //
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() !== true
            && $val["dossier_autorisation_ads"] != "") {
            $ret = $this->f->execute_action_to_referentiel_ads(
                203,
                array("dossier_autorisation" => $val["dossier_autorisation_ads"], )
            );
            if ($ret === false) {
                $this->correct = false;
                $this->addToMessage("Le dossier d'autorisation saisi n'existe pas dans le référentiel ADS.");
            }
        }
        //
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() !== true
            && $val["dossier_instruction_ads"] != "") {
            $ret = $this->f->execute_action_to_referentiel_ads(
                212,
                array("dossier_instruction" => $val["dossier_instruction_ads"], )
            );
            if ($ret === false) {
                $this->correct = false;
                $this->addToMessage("Le dossier d'instruction saisi n'existe pas dans le référentiel ADS.");
            }
        }
    }
    
    /**
     * VIEW - get_data_etablissement.
     *
     * Retourne en JSON les informations de l'établissement dans le dossier de 
     * coordination
     * 
     * @return void
     */
    function get_data_etablissement() {
        //
        $this->f->disableLog();
        //
        $sql = "SELECT
            dossier_coordination.etablissement_type as etablissement_type,
            dossier_coordination.etablissement_categorie as etablissement_categorie,
            dossier_coordination.etablissement_locaux_sommeil as etablissement_locaux_sommeil,
            dossier_coordination.erp as etablissement_nature,
            array_to_string(
                array_agg(lien_dossier_coordination_etablissement_type.etablissement_type),
            ';') as etablissement_type_secondaire
            FROM ".DB_PREFIXE."dossier_coordination
            LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_etablissement_type
                    ON lien_dossier_coordination_etablissement_type.dossier_coordination = dossier_coordination.dossier_coordination
            WHERE dossier_coordination.dossier_coordination = ".$this->getVal($this->clePrimaire)."
            GROUP BY dossier_coordination.etablissement_type, dossier_coordination.etablissement_categorie,
            dossier_coordination.etablissement_locaux_sommeil, dossier_coordination.erp";
        //
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $row=& $res->fetchRow(DB_FETCHMODE_ASSOC);

        $types_sec = array();
        $types_sec = explode(';', $row["etablissement_type_secondaire"]);
        $row["etablissement_type_secondaire"] = $types_sec;

        echo json_encode(array(
            "etablissement_type" => $row['etablissement_type'],
            "etablissement_categorie" => $row['etablissement_categorie'],
            "etablissement_locaux_sommeil" => $row['etablissement_locaux_sommeil'],
            "etablissement_nature" => $row['etablissement_nature'],
            "etablissement_type_secondaire" => $row['etablissement_type_secondaire'],
            )
        );
    }

    /**
     * Retourne les informations de classification de l'établissement.
     * 
     * @return [string] code HTML des données formatées
     */
    function get_classification_etablissement() {
        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        $html = "";
        // ERP
        ($this->getVal("erp") == 't') ? $erp = _("oui") : $erp = _("non");
        $html .= sprintf("%s : %s<br/>", _("ERP"), $erp);
        // Locaux sommeil
        ($this->getVal("etablissement_locaux_sommeil") == 't') ? $locaux = _("oui") : $locaux = _("non");
        $html .= sprintf("%s : %s<br/>", _("Locaux sommeil"), $locaux);
        // Type
        $type = $this->getVal("etablissement_type");
        if ($type != "") {
            $sql = str_replace('<idx>', $type, $sql_etablissement_type_by_id);
            $res = $this->f->db->query($sql);
            $this->addToLog(__METHOD__."(): db->query(".$sql.");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Liste les résultats
            $row =& $res->fetchRow();
            // Récupère le libelle
            $type = $row[1];
        }
        $html .= sprintf("%s : %s<br/>", _("Type"), $type);
        // Types secondaires
        $secondaires = $this->getVal("etablissement_type_secondaire");
        if ($secondaires != "") {
            $secondaires = explode(';', $secondaires);
            (count($secondaires) > 1) ? $lib = _("Types secondaires") : $lib = _("Type secondaire");
            $html .= sprintf('%s :<ul class="%s">',
                $lib,
                "liste_consultation_analyse"
            );
            foreach ($secondaires as $secondaire) {
                $sql = str_replace('<idx>', $secondaire, $sql_etablissement_type_by_id);
                $res = $this->f->db->query($sql);
                $this->addToLog(__METHOD__."(): db->query(".$sql.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                // Liste les résultats
                $row =& $res->fetchRow();
                // Récupère le libelle
                $secondaire = $row[1];
                $html .= sprintf("<li>%s</li>", $secondaire);
            }
            $html .= sprintf("</ul>");
        }
        // Catégorie
        $categorie = $this->getVal("etablissement_categorie");
        if ($categorie != "") {
            $sql = str_replace('<idx>', $categorie, $sql_etablissement_categorie_by_id);
            $res = $this->f->db->query($sql);
            $this->addToLog(__METHOD__."(): db->query(".$sql.");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Liste les résultats
            $row =& $res->fetchRow();
            // Récupère le libelle
            $categorie = $row[1];
        }
        $html .= sprintf("%s : %s<br/>", _("Categorie"), $categorie);
        return $html;
    }

    /**
     * [get_date_periodicite description]
     *
     * @param [type] $id [description]
     *
     * @return [type] [description]
     */
    function get_date_periodicite($id=null) {

        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_coordination = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_coordination = new dossier_coordination($id);
        }

        //
        $date_periodicite = "";

        //
        $dossier_instruction_id = $this->dossier_instruction_secu_id;
        $etablissement_id = $dossier_coordination->getVal("etablissement");

        //
        if (!empty($dossier_instruction_id)
            && !empty($etablissement_id)) {

            // Instance de la classe dossier_instruction
            require_once '../obj/dossier_instruction.class.php';
            $dossier_instruction = new dossier_instruction($dossier_instruction_id);

            // Récupère la dernière date de visite
            $last_visite = $dossier_instruction->get_last_visite();
            $date_visite = $dossier_instruction->get_date_visite($last_visite);

            // Instance de la classe etablissement
            require_once '../obj/etablissement.class.php';
            $etablissement = new etablissement($etablissement_id);

            // Récupère la périodicité de visite
            $periodicite_visites_id = $etablissement->getVal("si_periodicite_visites");

            // Instance de la classe periodicite_visites
            require_once '../obj/periodicite_visites.class.php';
            $periodicite_visites = new periodicite_visites($periodicite_visites_id);

            // Récupère la périodicité
            $periodicite = $periodicite_visites->getVal("periodicite");
            $periodicite_month = $periodicite*12;

            // Date calculé
            $date_periodicite = $this->f->mois_date($date_visite, $periodicite_month);

        }

        //
        return $date_periodicite;
    }


    var $inst_main_contact = null;

    /**
     *
     */
    function get_inst_main_contact($contact = null) {
        //
        if (!is_null($contact)) {
            require_once "../obj/contact.class.php";
            return new contact($contact);
        }
        //
        if (is_null($this->inst_main_contact)) {
            // On recupère la liste des contacts du courrier
            // sur lequel on se trouve
            $list_contact = $this->get_list_contact();
            // Si il y a un ou plusieurs contacts liés
            if (count($list_contact) != 0) {
                // Alors on prend le premier
                $contact = $list_contact[0]["contact"];
            } else {
                // Sinon on positionne la valeur à null
                $contact = null;
            }
            //
            require_once "../obj/contact.class.php";
            $this->inst_main_contact = new contact($contact);
        }
        //
        return $this->inst_main_contact;
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "dossier_coordination",
        "etablissement",
        "geom_point",
        "geom_emprise",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        // Récupération des libellés de contact
        $inst_main_contact = $this->get_inst_main_contact();
        $labels_main_contact = $inst_main_contact->get_labels_merge_fields();
        $labels_petitionnaire = array();
        foreach ($labels_main_contact[_("contact")] as $key => $value) {
            $new_key = str_replace("contact.", "petitionnaire.", $key);
            $labels_petitionnaire[$new_key] = $value;
        }
        // types secondaires
        $labels[_("dossier_coordination")]["dossier_coordination.etablissement_type_secondaire"] = _("type(s) secondaire(s)");
        //
        $labels[_("dossier_coordination")] = array_merge(
            $labels[_("dossier_coordination")],
            $labels_petitionnaire
        );
        // Retour de tous les libellés
        return $labels;
    }

     /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        // Correction valeur du champ select multiple "type(s) secondaire(s)"
        // si au moins un type renseigné
        $field_dc_type_sec = "dossier_coordination.etablissement_type_secondaire";
        if (!empty($values[$field_dc_type_sec])) {
            // Transformation du string en array
            $array = array();
            $array = explode(';', $values[$field_dc_type_sec]);
            // Remplacement de l'ID du type par son libellé
            foreach ($array as $key => $id) {
                // Utilisation de la classe établissement
                // car le DC concatène la description au libellé, ce que l'on ne souhaite pas
                $array[$key] = strtoupper($this->f->get_label_of_foreign_key("etablissement","etablissement_type",$id));
            }
            // Remplacement de la valeur du champ de fusion
            $values[$field_dc_type_sec] = implode(',', $array);
        }
        // Récupération des valeurs du contact
        $inst_main_contact = $this->get_inst_main_contact();
        $values_main_contact = $inst_main_contact->get_values_merge_fields();
        $values_petitionnaire = array();
        foreach ($values_main_contact as $key => $value) {
            $new_key = str_replace("contact.", "petitionnaire.", $key);
            $values_petitionnaire[$new_key] = $value;
        }
        //
        $values = array_merge(
            $values,
            $values_petitionnaire
        );
        // Si clé primaire existante on la transmet pour la gestion spécifique des contraintes
        if ($this->getVal($this->clePrimaire) != '') {
            $values['dossier_coordination.dossier_coordination'] = $this->getVal($this->clePrimaire);
        }
        // Retour de tous les libellés
        return $values;
    }


    /**
     * Méthode de vérification des données et de retour d’erreurs (utilisé lors de
     * l’ajout).
     *
     * @param array $val Valeurs
     *
     * @return void
     */
    function verifierAjout($val = array(), &$db = null) {
        parent::verifierAjout($val);

        // Instance de la classe dossier_coordination_type
        require_once '../obj/dossier_coordination_type.class.php';
        $dossier_coordination_type = new dossier_coordination_type($this->valF["dossier_coordination_type"]);

        // Si le type de dossier de coordination est périodique et qu'il n'y a
        // aucun établissement sélectionné
        if ($dossier_coordination_type->is_periodic() && $this->valF['etablissement'] == '') {
            //
            $this->addToMessage( _('Le champ').' <span class="bold">'._("etablissement").'</span> '._('est obligatoire'));
            $this->correct = false;
        }
    }

    /**
     * Permet de savoir si le dossier de coordination a déjà été géolocalisé, et existe sur
     * le SIG.
     *
     * @return boolean true si le dossier n'est pas géolocalisé, sinon true.
     */
    function is_not_geolocalised() {

        $is_geolocalised = $this->getVal('geolocalise');
        //
        if ($is_geolocalised === 't') {
            //
            return false;
        }
        return true;
    }

    /**
     * CONDITION - is_connected_to_referentiel_ads.
     *
     * @return boolean
     */
    function is_connected_to_referentiel_ads() {
        //
        if ($this->getVal("interface_referentiel_ads") !== "t") {
            return false;
        }
        //
        return true;
    }

    /**
     * CONDITION - is_enjeu_erp.
     *
     * @return boolean
     */
    function is_enjeu_erp() {
        if ($this->getVal("enjeu_erp") == "t") {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_not_enjeu_erp.
     *
     * @return boolean
     */
    function is_not_enjeu_erp() {
        return ! $this->is_enjeu_erp();
    }

    /**
     * CONDITION - is_dossier_coordination_localizable.
     *
     * Le type de DC peut ne pas être localisable sur le SIG. Seuls les types de dossiers
     * ayant une couche correspondante définie dans dyn/sig.inc.php sont localisables.
     * 
     * @return boolean
     */
    function is_dossier_coordination_localizable($dossier_coordination_id = null) {
        //
        
        try {
            $inst_geoaria = $this->f->get_inst_geoaria();
            $is_localization_dc_enabled = $inst_geoaria->is_localization_dc_enabled();
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            return false;
        }
        if ($is_localization_dc_enabled !== true) {
            return false;
        }

        //
        $dossier_coordination_type_id = null;
        if ($this->get_action_crud() !== 'create') {
            //
            $dossier_coordination_id = isset($dossier_coordination_id) ? $dossier_coordination_id : $this->getVal($this->clePrimaire);
            $dossier_coordination_type_id = $this->getVal('dossier_coordination_type');
        }

        // Récupération des valeurs dans le formulaire dans le contexte de création de nouveau dossier
        $dossier_coordination_id = isset($dossier_coordination_id) ? $dossier_coordination_id : $this->valF['dossier_coordination'];
        $dossier_coordination_type_id = isset($dossier_coordination_type_id) ? $dossier_coordination_type_id : $this->valF['dossier_coordination_type'];

        // Récupère le l'identifiant et le code du type de dossier
        $dossier_coordination_type = $this->get_dossier_coordination_type($dossier_coordination_type_id);
        $dossier_coordination_type_code = $dossier_coordination_type['code'];

        $collectivite = $this->f->getCollectivite();
        // Si la configuration de SIG contient un paramétrage des couches
        if (isset($collectivite['sig']['sig_couches_dc'])) {
            // Si le paramétrage de la couche SIG n'est pas renseigné ou vide pour ce type de dossier
            if (!array_key_exists($dossier_coordination_type_code, $collectivite['sig']['sig_couches_dc'])
                OR $collectivite['sig']['sig_couches_dc'][$dossier_coordination_type_code] === '') {
                //
                return false;
            }
        }
        //
        return true;
    }

    /**
     * Retourne toutes les parcelles du DC fourni
     *
     * @return array
     */
    public function get_formated_parcelles() {
        // Résultat retourné
        return $this->f->parseParcelles($this->getVal('references_cadastrales'));
    }


    /**
     * VIEW - view_localiser
     * Redirige l'utilisateur vers le SIG externe, avec la vue centrée sur le dossier
     * de coordination.
     *
     * @return void
     */
    function view_localiser() {
        $this->checkAccessibility();
        // Libellé du dossier
        $libelle = $this->getVal('libelle');

        // Si on utilise les références cadastrales pour géolocaliser
        if ($this->f->get_submitted_get_value('option') === 'parcelles' AND $this->getVal('references_cadastrales') != '') {
            // Formattage des parcelles dans un tableau avant l'envoi au connecteur
            $tab_parcelles = $this->f->parseParcelles($this->getVal('references_cadastrales'));
            // Récupération du lien de redirection vers le SIG
            try {
                $geoaria = $this->f->get_inst_geoaria();
                $url = $geoaria->redirection_web('parcelle', $tab_parcelles);
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                $this->f->displayMessage('error', $e->getMessage());
                return;
            }
        }
        elseif ($this->is_not_geolocalised() === false AND $this->is_dossier_coordination_localizable()){
            $dossier_coordination_type_id = $this->getVal('dossier_coordination_type');
            $dossier_coordination_type = $this->get_dossier_coordination_type($dossier_coordination_type_id);
            $dossier_coordination_type_code = $dossier_coordination_type['code'];
            $data[] = array(
                    'id' => $libelle,
                    'type' => $dossier_coordination_type_code,
            );
            try {
                $geoaria = $this->f->get_inst_geoaria();
                $url = $geoaria->redirection_web('dossier_coordination', $data);
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                $this->f->displayMessage('error', $e->getMessage());
                return;
            }
        }
        // Sinon redirection vers le sig sans argument
        else {
            //
            try {
                $geoaria = $this->f->get_inst_geoaria();
                $url = $geoaria->redirection_web();
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                $this->f->displayMessage('error', $e->getMessage());
                return;
            }
        }
        // Redirection
        header("Location: ".$url);

    }


    /**
     * VIEW - view_localiser_selection
     * Redirige l'utilisateur vers le SIG externe, avec la vue centrée sur la sélection
     * de dossiers de coordination obtenue par la recherche avancée.
     * Si la recherche renvoie une liste vide, l'utilisateur est redirigé vers la couche
     * des dossiers de coordination sur le SIG.
     *
     * @return void
     */
    function view_localiser_selection() {
        $this->checkAccessibility();

        (isset($_GET['obj']) ? $obj = $this->f->get_submitted_get_value('obj') : $obj = "");
        // Premier enregistrement a afficher
        (isset($_GET['premier']) ? $premier = $this->f->get_submitted_get_value('premier') : $premier = 0);
        // Colonne choisie pour le tri
        (isset($_GET['tricol']) ? $tricol = $this->f->get_submitted_get_value('tricol') : $tricol = "");
        // Id unique de la recherche avancee
        (isset($_GET['advs_id']) ? $advs_id = $this->f->get_submitted_get_value('advs_id') : $advs_id = "");
        // Valilite des objets a afficher
        (isset($_GET['valide']) ? $valide = $this->f->get_submitted_get_value('valide') : $valide = "");
        // Valilite des objets a afficher
        (isset($_GET['recherche']) ? $recherche = $this->f->get_submitted_get_value('recherche') : $recherche = "");
        // Valilite des objets a afficher
        (isset($_GET['selectioncol']) ? $selectioncol = $this->f->get_submitted_get_value('selectioncol') : $selectioncol = "");

        // S'il n'y a pas eu de recherche avancée
        if ($advs_id === '') {
            try {
                $geoaria = $this->f->get_inst_geoaria();
                $url = $geoaria->redirection_web('dossier_coordination', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                $this->f->displayMessage('error', $e->getMessage());
                return;
            }
            header("Location: ".$url);
            exit();
        }

        // Ce tableau permet a chaque application de definir des variables
        // supplementaires qui seront passees a l'objet metier dans le constructeur
        // a travers ce tableau
        // Voir le fichier dyn/form.get.specific.inc.php pour plus d'informations
        $extra_parameters = array();
        $f = $this->f;
        // surcharge globale
        if (file_exists('../dyn/tab.inc.php')) {
            require_once '../dyn/tab.inc.php';
        }
        if (file_exists('../dyn/custom.inc.php')) {
            require_once '../dyn/custom.inc.php';
        }
        // *** custom
        if(isset($custom['tab'][$obj]) and file_exists($custom['tab'][$obj])){
            require_once $custom['tab'][$obj];
        } else{
            // surcharge specifique des objets
            if (file_exists("../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php")) {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc.php";
            } else {
                require "../sql/".OM_DB_PHPTYPE."/".$obj.".inc";
            }   
        }
        if (!isset($om_validite) or $om_validite != true) {
            $om_validite = false;
        }

        /**
         *
         */
        // Instanciation d'om_table
        require_once "om_table.class.php";
        //
        $tb = new om_table(
            "../scr/tab.php",
            $table,
            $serie,
            $champAffiche,
            $champRecherche,
            $tri,
            $selection,
            $edition,
            $options,
            $advs_id,
            $om_validite
        );

        /**
         *
         */
        // Affectation des parametres
        $params = array(
            "obj" => $obj,
            "premier" => $premier,
            "recherche" => $recherche,
            "selectioncol" => $selectioncol,
            "tricol" => $tricol,
            "advs_id" => $advs_id,
            "valide" => $valide,
        );
        // Ajout de paramètre spécifique
        $params = array_merge($params, $extra_parameters);
        //
        $tb->setParams($params);
        // Methode permettant de definir si la recherche doit etre faite
        // sur la recherche simple ou avancée
        $tb->composeSearchTab();
        // Generation de la requete de recherche
        $tb->composeQuery();
        // Exécution de la requête
        $res = $this->db->limitquery($tb->sql, $premier, '50');
        $this->addToLog(__METHOD__."() db->limitquery(\"".$tb->sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        $nbligne = $res->numrows();
        // S'il n'y a aucun résultat
        if ($nbligne === 0) {
            try {
                $geoaria = $this->f->get_inst_geoaria();
                $url = $geoaria->redirection_web('dossier_coordination', array());
            } catch (geoaria_exception $e) {
                $this->handle_geoaria_exception($e);
                $this->f->displayMessage('error', $e->getMessage());
                return;
            }
            header("Location: ".$url);
            exit();
        }
        // Récupération des identifiants et libellés
        while ($row=& $res->fetchRow()){
            $dossiers_coordination[$row[0]] = $row[1];
        }
        // 
        $dossiers_coordination_ids = array_keys($dossiers_coordination);
        $dossiers_coordination_type_codes = $this->get_dossiers_coordination_type_codes_by_dossiers_coordination_ids($dossiers_coordination_ids);
        $data = array();
        foreach ($dossiers_coordination as $key => $value) {
            $data[] = array (
                'id' => $value,
                'type' => $dossiers_coordination_type_codes[$key],
            );
        }
        try {
            $geoaria = $this->f->get_inst_geoaria();
            $url = $geoaria->redirection_web('dossier_coordination', $data);
        } catch (geoaria_exception $e) {
            $this->handle_geoaria_exception($e);
            $this->f->displayMessage('error', $e->getMessage());
            return;
        }

        // Redirection
        header("Location: ".$url);

    }

    /**
     * Méthode de géocodage directement appellée par une action.
     * Cette méthode gère les exceptions et les messages d'erreur.
     *
     * @return boolean
     */
    function geocoder() {

        // Begin
        $this->begin_treatment(__METHOD__);

        // On considère que le traitement est toujours valide
        // afin que l'action ne soit jamais en erreur
        $this->geocoder_treatment();
        //
        return $this->end_treatment(__METHOD__, true);
    }


    /**
     * Méthode de géocodage du dossier de coordination sur le SIG.
     *
     * Cette méthode peut seulement être appelée sur un DC qui n'a pas encore
     * été géolocalisé. Elle ne doit pas faire obstacle à sa création.
     *
     * Elle renvoie FAUX si l'établissement n'est pas géocodé, du fait d'une nécessité
     * d'intervention manuelle ou d'une erreur BBD/SIG.
     *
     * @throws  void
     * @return  boolean
     */
    function geocoder_treatment() {


        // Récupération des valeurs du formulaire dans le contexte d'un nouveau dossier
        if ($this->get_action_crud() == 'create') {
            $dossier_coordination_id = $this->valF[$this->clePrimaire];
            $dossier_coordination_libelle = $this->valF['libelle'];
            $references_cadastrales = $this->valF['references_cadastrales'];
            $dossier_coordination_type_id = $this->valF['dossier_coordination_type'];
            // Nécessaire sinon cette variable vaut null quand le champ est vide
            $dossier_ads = isset($this->valF['dossier_instruction_ads']) ? $this->valF['dossier_instruction_ads'] : '';
        }
        else {
            $dossier_coordination_id = $this->getVal($this->clePrimaire);
            $dossier_coordination_libelle = $this->getVal("libelle");
            $references_cadastrales = $this->getVal("references_cadastrales");
            $dossier_ads = $this->getVal("dossier_instruction_ads");
            $dossier_coordination_type_id = $this->getVal('dossier_coordination_type');
        }

        //
        if ($this->is_dossier_coordination_localizable($dossier_coordination_id) === false) {
            $this->addToMessage(_("Le dossier n'est pas géolocalisable."));
            return false;
        }

        //
        $dossier_coordination_type = $this->get_dossier_coordination_type($dossier_coordination_type_id);
        // Tableau associatif de paramètres passés au SIG
        $params = array (
             'parcelles' => $this->f->parseParcelles($references_cadastrales),
             'numero' => $this->get_numero_voie(),
             'ref_voie' => $this->get_code_referentiel_voie(),
             'dossier_ads' => $dossier_ads,
             'type' => $dossier_coordination_type['code'],
        );
        // Interrogation du web service du SIG
        $precision_geocodage = '';
        try {
            //
            $geoaria = $this->f->get_inst_geoaria();
            $precision_geocodage = $geoaria->geocoder_objet('dossier_coordination', $dossier_coordination_libelle, $params);

        } catch (geoaria_connector_4XX_exception $e) {

            // Si la géolocalisation de ce dossier à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_already_exist') {
                // Met à jour le flag de géolocalisation
                $update = $this->update_geolocalise('t', $dossier_coordination_id);

                // Si la mise à jour échoue
                if ($update === false) {
                    // Stop le traitement
                    return false;
                }

                //
                $this->addToMessage(_("Le dossier de coordination est déjà géolocalisé."));
                //
                return false;
            }

            // Si la géolocalisation de cet établissement à déjà été faite
            if ($e->get_typeException() === 'geocoder_objet_cant_geocode') {
                // Template du lien vers le SIG
                $template_link_redirection_web_emprise = "<br/><a id='link_dessin_emprise_sig' title=\"%s\" class='lien' target='_blank' href='%s'><span class='om-icon om-icon-16 om-icon-fix sig-16'></span>%s</a>";
                //
                try {
                    $url_redirection_web_emprise = $this->redirection_web_emprise($dossier_coordination_libelle, $dossier_coordination_type['code']);
                } catch (geoaria_exception $e) {
                    $this->addToMessage($this->handle_geoaria_exception($e));
                    return false;
                }
                $this->addToMessage(_("Le dossier de coordination n'a pas pu être créé automatiquement sur le SIG."));
                // Affichage du lien seulement s'il n'est pas vide
                if ($url_redirection_web_emprise != ''){
                    $link_redirection_web_emprise = sprintf(
                        $template_link_redirection_web_emprise,
                        _("Dessiner le dossier sur le SIG"),
                        $url_redirection_web_emprise,
                        _("Cliquez ici pour le dessiner.")
                    );
                    $this->addToMessage($link_redirection_web_emprise."<br/>");
                }
                //
                return false;
            }

        } catch (geoaria_exception $e) {
            //
            $this->addToMessage($this->handle_geoaria_exception($e));
            //
            return false;
        }

        // Traitement de la réponse du web service du SIG
        if ($precision_geocodage !== false AND $precision_geocodage !== '') {
            // Met à jour le flag de géolocalisation
            $update = $this->update_geolocalise('t', $dossier_coordination_id);

            // Si la mise à jour échoue
            if ($update === false) {
                // Stop le traitement
                return false;
            }

            //
            $this->addToMessage(
                sprintf(
                    _("Le dossier de coordination a été géolocalisé avec une précision de %sm."),
                    $precision_geocodage
                )
            );
            //
            return true;
        }

        //
        $this->addToMessage(_("Le dossier de coordination n'a pas pu être créé sur le SIG."));
        //
        return false;
    }


    /**
     * Met à jour le flag de géolocalisation du dossier de coordination.
     *
     * @param string $value Valeur du champ.
     *
     * @return boolean
     */
    protected function update_geolocalise($value, $dossier_coordination) {

        // Valeur à mettre àjour
        $valF = array(
            "geolocalise" => $value,
        );

        // Met à jour l'enregistrement
        $update = $this->update_autoexecute($valF, $dossier_coordination, false);

        // Si la mise à jour échoue
        if ($update == false) {
            // Stop le traitement
            return false;
        }

        // Continue le traitement
        return true;
    }


    /**
     * Méthode renvoyant l'URL permettant de dessiner manuellement l'élément sur le SIG.
     *
     * @return string $url_redirection_web_emprise
     * @throws geoaria_exception
     */
    private function redirection_web_emprise($dossier_coordination_libelle, $dossier_coordination_type_code) {

        $data = array(
            'id' => $dossier_coordination_libelle,
            'type' => $dossier_coordination_type_code,
            'ref_voie' => $this->get_code_referentiel_voie(),
        );
        // Interrogation du web service du SIG
        $geoaria = $this->f->get_inst_geoaria();
        return $geoaria->redirection_web_emprise('dossier_coordination', $data);
    }

    /**
     *
     */
    function set_geolocalise($geolocalise) {
        if ($geolocalise === true) {
            $this->valF['geolocalise'] = true;
        }
        else {
            $this->valF['geolocalise'] = false;
        }
    }

    /**
     * XXX - Le champ n'existe pas, à implémenter
     */
    function get_code_referentiel_voie() {
        return '';
    }

    /**
     * XXX - Le champ n'existe pas, à implémenter
     */
    function get_numero_voie() {
        return '';
    }

    /**
     * Surcharge de la méthode du core afin de faire appel à la fonction JS
     * getDataFieldReferenceCadastrale().
     *
     * @param integer $maj Mode de mise a jour
     * @return void
     */
    function bouton($maj) {
        if (!$this->correct
            && $this->checkActionAvailability() == true) {
            // Ancienne gestion des actions
            if ($this->is_option_class_action_activated() == false) {
                switch($maj) {
                    case 0 :
                        $bouton = _("Ajouter");
                        break;
                    case 1 :
                        $bouton = _("Modifier");
                        break;
                    case 2 :
                        $bouton = _("Supprimer");
                        break;
                    case 999 :
                        $bouton = _("Rechercher");
                        break;
                    default :
                        $bouton = _("Valider");
                        break;
                }
            }
            // Nouvelle gestions des actions
            if ($this->is_option_class_action_activated() == true) {
                // Actions SCRUD ou indéfinies
                if ($this->get_action_param($maj, "button") == null) {
                    // Récupération du mode de l'action
                    $crud = $this->get_action_crud($maj);
                    switch($crud) {
                        case 'create' :
                            $bouton = _("Ajouter");
                            break;
                        case 'update' :
                            $bouton = _("Modifier");
                            break;
                        case 'delete' :
                            $bouton = _("Supprimer");
                            break;
                        case 'search' :
                            $bouton = _("Rechercher");
                            break;
                        default :
                            $bouton = _("Valider");
                            break;
                    }
                }
                // Actions spécifiques
                else {
                    //
                    $bouton = $this->get_action_param($maj, "button");
                    
                }
            }
            //
            $bouton .= "&nbsp;"._("l'enregistrement de la table")."&nbsp;:";
            $bouton .= "&nbsp;'"._($this->table)."'";
            //
            $params = array(
                "value" => $bouton,
                "name" => "submit",
                "onclick" => "return getDataFieldReferenceCadastrale();"
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * VIEW - view_overlay_etablissements_proches_qualification_formulaire.
     *
     * Cette méthode a pour but de générer un formulaire simple contenant 2 select,
     * limite et nature.
     * Ce formulaire est ensuite affiché dans un dialog JS, qui présente les établissements
     * proches des références cadastrales saisies dans le formulaire.
     *
     * @return void
     */
    public function view_overlay_etablissements_proches_qualification_formulaire() {

        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        $this->f->setTitle(_("Liste des établissements proches des références cadastrales"));
        $this->f->displayTitle();

         // Bouton de fermuture de l'overlay
        $close_button = '<input id="close-button" value="Fermer" class="om-button ui-button ui-widget ui-state-default ui-corner-all" role="button" aria-disabled="false" type="submit">';

        // Le formulaire n'a pas été validé
        $validation = 1;
        // Le formulaire est en mode consultation
        $maj = 3;

        // Champs du formulaire
        $champs = array("limite", "nature");

        // Création d'un nouvel objet de type formulaire
        $form = new formulaire(null, $validation, $maj, $champs);
        $form->setType('limite', 'select');
        $form->setType('nature', 'select');

        // Définition des limites disponibles dans le select
        // avec setVal() sur l'option par défaut
        $options = array();
        foreach ($this->limites as $limite) {
            $options[0][] = $limite;
            $options[1][] = $limite;
        }
        $form->setSelect('limite', $options);
        $form->setVal('limite', $this->limite_default);

        $form->setVal('nature', '');
        $query_etablissement_nature = "
            SELECT etablissement_nature, nature
                FROM ".DB_PREFIXE."etablissement_nature
                ORDER BY nature";
        $this->init_select($form, $this->f->db, 1, false, "nature", $query_etablissement_nature);

        //
        $form->setFieldset("limite", "D", _("Paramètres"), "");
        $form->setBloc("limite", "D", "", "group");
        $form->setBloc("nature", "F");
        $form->setFieldset("nature", "F");

        // Début du formulaire
        printf("\n<!-- ########## START FORMULAIRE ########## -->\n");
        printf("<div class=\"formEntete ui-corner-all\">\n");

        $form->afficher($champs, $validation, false, false);

        // Fin formulaire
        printf("</div>");
        printf("<div id='tab_etablissements_proches'>");
        printf("</div>");

        // Affiche le bouton de fermeture
        printf($close_button);

        return;
    }


    /**
     * VIEW - view_overlay_etablissements_proches_qualification_tableau.
     *
     * Cette méthode compose le tableau un tableau contenant les établissements proches
     * des références cadastrales saisies, récupérés à partir du SIG configuré.
     * 
     * @return void
     */
    public function view_overlay_etablissements_proches_qualification_tableau() {

        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Message affiché
        $message_field_error_sig = '<div class="message ui-widget ui-corner-all ui-state-highlight ui-state-error" id="localiser-etablissements-proches-message">
            <p>
                <span class="ui-icon ui-icon-info"></span> 
                <span class="text"><p>%s</p></span>
            </p>
        </div>';

        /**
         * Template nécessaires à l'affichage du tableau dans l'overlay
         */
        $template_table = '
        <table class="tab-tab">
            <thead>
                <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                    <th class="title col-0 firstcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-1">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-2">
                        <span class="name">
                            %s
                        </span>
                    </th>
                    <th class="title col-3 lastcol">
                        <span class="name">
                            %s
                        </span>
                    </th>
                </tr>
            </thead>
            <tbody>
        %s
            </tbody>
        </table>
        ';
        //
        $template_line = '
        <tr class="tab-data odd">
            <td class="col-0 firstcol">
                %s
            </td>
            <td class="col-1">
                %s
            </td>
            <td class="col-2">
                %s
            </td>
            <td class="col-3 lastcol">
                %s
            </td>
        </tr>
        ';

        $template_empty_row = "
        <tr class='tab-data empty'>
            <td colspan='11'>
                %s
            </td>
        </tr>";

        // Instanciation de l'abstracteur et du connecteur SIG
        try {
            $geoaria = $this->f->get_inst_geoaria();
        } catch (geoaria_exception $e) {
            printf($message_field_error_sig, $e->getMessage());
            return;
        }

        $select_nature_etablissement = $this->f->get_submitted_get_value('nature');
        $string_parcelles = $this->f->get_submitted_get_value('parcelles');
        if ($string_parcelles == '') {
            return;
        }
        $limite = 10;
        if ($this->f->get_submitted_get_value('limite') !== '' AND $this->f->get_submitted_get_value('limite') !== null) {
            $limite = $this->f->get_submitted_get_value('limite');
        }

        $tab_parcelles = $this->f->parseParcelles($string_parcelles);
        $params = array(
            'listeparcelles' => $tab_parcelles,
            'limite' => $limite,
        );

        try {
            $tab_etablissements_proches = $geoaria->lister_etablissements_proches($params);
        } catch (geoaria_exception $e) {
            printf($message_field_error_sig, $e->getMessage());
            return;
        }

        // S'il n'y a pas d'établissements proches
        if (isset($tab_etablissements_proches) === true AND count($tab_etablissements_proches) === 0) {

            $ct_body = sprintf(
                $template_empty_row,
                _("Il n'y a aucun établissement proche des références cadastrales saisies.")
            );

            // Affichage du tableau listant les dossiers
            printf(
                $template_table,
                // Colonne 0 - Identifiant de l'établissemnt
                _('établissement'),
                // Colonne 1 - Libellé + code de l'établissement
                _('établissement'),
                // Colonne 2 - Distance de l'établissement
                _('distance'),
                // Colonne 3 - Nature de l'établissement
                _('nature'),
                // Contenu du tableau
                $ct_body
            );
            //
            return;
        }

        // On construit le contenu du tableau
        $ct_tbody = '';
        foreach ($tab_etablissements_proches as $key => $value) {
            $code[$key]  = $value['id'];
            $distance[$key] = $value['distance'];
        }
        array_multisort($distance, SORT_ASC, SORT_NUMERIC, $tab_etablissements_proches);

        $count_etablissements = 0;
        foreach ($tab_etablissements_proches as $value) {
            // On construit la ligne
            $code_etablissement = $value['id'];
            $etablissement_id = $this->get_etablissement_id_by_code($code_etablissement);
            $inst_etablissement = $this->get_inst_common('etablissement', $etablissement_id);
            $nature_etablissement = $inst_etablissement->getVal('etablissement_nature');

            if ($select_nature_etablissement !== '' AND $select_nature_etablissement !== $nature_etablissement) {
                continue;
            }
            if (isset($value['distance']) === false OR isset($value['id']) === false) {
                continue;
            }
            if ($etablissement_id === null) {
                continue;
            }
            $libelle_etablissement = $inst_etablissement->getVal('libelle');
            $inst_nature_etablissement = $this->get_inst_common('etablissement_nature', $nature_etablissement);
            $ct_tbody .= sprintf(
                $template_line,
                // Colonne 0 - Identifant de l'établissement
                $etablissement_id,
                // Colonne 1 - Code et libellé de l'établissement
                $code_etablissement . ' - ' . $libelle_etablissement,
                // Colonne 2 - Distance
                $value['distance'] . 'm',
                // Colonne 3 - Nature de l'établissement
                $inst_nature_etablissement->getVal('nature')
            );
            $count_etablissements ++;
        }

        if ($count_etablissements === 0) {
            $ct_tbody = sprintf(
                $template_empty_row,
                _("Aucun enregistrement.")
            );
        }

        // Affichage du tableau listant les dossiers
        printf(
            $template_table,
            // Colonne 0 - Identifiant de l'établissemnt
            _('établissement'),
            // Colonne 1 - Libellé + code de l'établissement
            _('établissement'),
            // Colonne 2 - Distance de l'établissement
            _('distance'),
            // Colonne 3 - Nature de l'établissement
            _('nature'),
            // Contenu du tableau
            $ct_tbody
        );

        // Fin du formulaire
        printf("</div>");

        //
        return;
    }


    /**
     * Récupère l'identifiant de l'établissement ayant le code passé en paramètre.
     *
     * @param  string $etablissement_code Code de l'établissement.
     * @return integer Identifiant de l'établissement.
     */
    function get_etablissement_id_by_code($etablissement_code) {
        // Initialisation du résultat retourné
        $code = "";

        // Si etablissement_code n'est pas vide
        if (!empty($etablissement_code)) {

            // Requête SQL
            $sql = "
                SELECT etablissement
                FROM " . DB_PREFIXE . "etablissement
                WHERE etablissement.code = '" . $etablissement_code . "'";
            $this->addToLog("get_etablissement_id_by_code() db->getOne(\"" . $sql . "\");",
                VERBOSE_MODE);
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);
        }

        // Résultat retourné
        return $code;
    }

    /**
     * [construct_contraintes description]
     * @param  [type] $contraintes_params [description]
     * @return [type]                     [description]
     */
    public function construct_contraintes($contraintes_params = null) {
        // Variable à afficher à la place de "&contraintes"
        $contraintes = "";
        // Tableau associatif à 2 dimensions, qui contient l'ensemble des paramètres fournis à 
        // &contraintes explosés, une ligne du tableau contient un nom de groupe, sous-groupe
        // ou une valeur. 
        $conditions = array();

        // SELECT
        $selectContraintes = "SELECT 
            lien_contrainte_dossier_coordination.texte_complete as lien_contrainte_dossier_coordination_texte,
            lower(contrainte.groupe) as contrainte_groupe,
            lower(contrainte.sousgroupe) as contrainte_sousgroupe ";

        // FROM
        $fromContraintes = " FROM ".DB_PREFIXE."contrainte 
            LEFT JOIN ".DB_PREFIXE."lien_contrainte_dossier_coordination
                ON  lien_contrainte_dossier_coordination.contrainte = contrainte.contrainte ";

        // WHERE
        $whereContraintes = sprintf(
            " WHERE lien_contrainte_dossier_coordination.dossier_coordination = %s ",
            $this->getVal($this->clePrimaire)
        );

        $triContraintes = " ORDER BY contrainte_groupe DESC, 
            contrainte_sousgroupe, 
            contrainte.ordre_d_affichage, 
            contrainte.libelle ";

        // S'il y a des paramètres
        if ($contraintes_params !== null) {
            // Explose les paramètres et valeurs récupérées dans un tableau
            $conditions = $this->f->explode_condition_contrainte($contraintes_params[1]);
            // Récupération des conditions à ajouter au WHERE de la requête SQL
            $whereContraintes .= $this->f->treatment_condition_contrainte(NULL, $conditions);
        }

        // Tri différent sur les contraintes si l'affichage est sans arborescence
        if (isset($conditions['affichage_sans_arborescence']) && $conditions['affichage_sans_arborescence'] == 't') {
            $triContraintes = " ORDER BY contrainte.ordre_d_affichage, contrainte.libelle ";
        }

        // Requête
        $sqlContraintes = $selectContraintes.$fromContraintes.$whereContraintes.$triContraintes;
        $resContraintes = $this->f->db->query($sqlContraintes);
        $this->f->addToLog(__METHOD__." : db->query(\"".$sqlContraintes."\");", 
            VERBOSE_MODE);
        // Étant donné le contexte d'édition PDF,
        // si erreur BDD alors on stoppe le traitement et on affiche le message
        $this->f->isDatabaseError($resContraintes);

        // S'il y a un résultat
        if ($resContraintes->numRows() != 0) {
            
            // Sauvegarde des données pour les comparer
            $lastRowContrainte = array();
            $lastRowContrainte['contrainte_groupe'] = 'empty';
            $lastRowContrainte['contrainte_sousgroupe'] = 'empty';
            $contraintes .= "<table width=\"auto\" >";
            // Tant qu'il y a un résultat
            while ($rowContrainte =& $resContraintes->fetchRow(DB_FETCHMODE_ASSOC)) {
                // Si l'identifiant du groupe de la contrainte présente et celui d'avant sont
                // différents, et si l'option affichage_sans_arborescence est désactivée
                if ($rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe']
                    && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                    $contraintes .= 
                        "<tr><td style=\"width:10px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_groupe'], 'UTF-8')."</td></tr>";
                    
                }

                // Si l'identifiant du sousgroupe de la contrainte présente et celui d'avant sont
                // différents, ou s'ils sont identiques mais n'appartiennent pas au même groupe
                // Et si l'option affichage_sans_arborescence est désactivée
                if (($rowContrainte['contrainte_sousgroupe'] != $lastRowContrainte['contrainte_sousgroupe']
                    || $rowContrainte['contrainte_groupe'] != $lastRowContrainte['contrainte_groupe'])
                    &&  $rowContrainte['contrainte_sousgroupe'] != "" && (!isset($conditions['affichage_sans_arborescence'])
                    || $conditions['affichage_sans_arborescence'] != 't')) {
                        $contraintes .=
                        "<tr><td style=\"width:30px; text-align:right;\"> - </td>
                        <td style=\"width:5px;\"></td><td>".
                        mb_strtoupper($rowContrainte['contrainte_sousgroupe'], 'UTF-8')."</td></tr>";

                }
                // Si l'option d'affichage sans arborescence n'est pas activée, on ajoute les
                // contraintes avec alinéas avec tirets.
                // Sinon on affiche les contraintes sans tirets et alinéas.
                if (!isset($conditions['affichage_sans_arborescence']) || $conditions['affichage_sans_arborescence'] != 't') {
                    $contraintes .= 
                    "<tr><td style=\"width:50px; text-align:right;\"> - </td>
                    <td style=\"width:5px;\"></td><td>".
                    ucfirst($rowContrainte['lien_contrainte_dossier_coordination_texte'])."</td></tr>";
                }
                else {
                    $contraintes .= 
                    "<tr><td>".ucfirst($rowContrainte['lien_contrainte_dossier_coordination_texte'])."</td></tr>";
                }
                // sauvegarde des valeurs avant nouvelle itération
                $lastRowContrainte=$rowContrainte;
            }
            $contraintes .= "</table>";
        }
        //
        return $contraintes;
    }


    /**
     * [get_dossier_coordination_type_by_code description]
     * @param  [type] $dossier_coordination_type_code [description]
     * @return [type]                                 [description]
     */
    function get_dossier_coordination_type_by_code($dossier_coordination_type_code) {

        // Requête SQL
        $sql = "SELECT dossier_coordination_type
                FROM ".DB_PREFIXE."dossier_coordination_type
                WHERE lower(code) = lower('".$dossier_coordination_type_code."')";
        $this->f->addToLog(
            "get_dossier_coordination_type_by_code() : db->getOne(\"".$sql."\")",
            VERBOSE_MODE);
        $dossier_coordination_type = $this->f->db->getOne($sql);
        $this->f->isDatabaseError($dossier_coordination_type);

        // Retourne le résultat
        return $dossier_coordination_type;
    }

    /**
     * 
     * @return int
     */
    function get_dossier_coordination_for_dossier_ads($dossier_ads, $dossier_coordination_type_code = null, $cible = "di") {
        //
        $dossier_coordination_type = null;
        $condition_dossier_coordination_type = "";
        if ($dossier_coordination_type_code != null) {
            if (is_array($dossier_coordination_type_code)) {
                $dossier_coordination_type = array();
                foreach ($dossier_coordination_type_code as $key => $value) {
                    $dossier_coordination_type[] = $this->get_dossier_coordination_type_by_code($value);
                }
            } else {
                $dossier_coordination_type = array($this->get_dossier_coordination_type_by_code($dossier_coordination_type_code), );
            }
            foreach ($dossier_coordination_type as $key => $value) {
                if ($value === null) {
                    unset($dossier_coordination_type[$key]);
                }
            }
            if (count($dossier_coordination_type) != 0) {
                $ids_string = implode(',', $dossier_coordination_type);
                $condition_dossier_coordination_type = sprintf(
                    " AND dossier_coordination_type IN (%s) ",
                    $ids_string
                );
            }
        }
        //
        $sql = sprintf(
            'SELECT 
                dossier_coordination
            FROM
                %sdossier_coordination
            WHERE
                %s=\'%s\'
                AND interface_referentiel_ads IS TRUE %s',
            DB_PREFIXE,
            ($cible == "da" ? "dossier_autorisation_ads" : "dossier_instruction_ads"),
            $this->f->db->escapeSimple($dossier_ads),
            $condition_dossier_coordination_type
        );
        $res = $this->f->db->query($sql);
        $this->f->addToLog(
            __METHOD__."(): db->query(\"".$sql."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        if ($res->numRows() == 0) {
            return 0;
        } elseif ($res->numRows() == 1) {
            $row = $res->fetchRow();
            return $row[0];
        } else {
            $list = array();
            while ($row =& $res->fetchrow()) {
                $list[] = $row[0];
            }
            return $list;
        }
    }

    /**
     *
     * @return mixed
     */
    function get_all_messages() {
        //
        $query = sprintf(
            'SELECT * FROM %sdossier_coordination_message WHERE dossier_coordination=%s',
            DB_PREFIXE,
            $this->getVal($this->clePrimaire)
        );
        $res = $this->f->db->query($query);
        $this->addToLog(
            __METHOD__."(): db->query(\"".$query."\");",
            VERBOSE_MODE
        );
        if ($this->f->isDatabaseError($res, true)) {
            return false;
        }
        //
        $list = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $list[] = $row;
        }
        return $list;
    }

}// fin classe

?>
