<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont non
 * lu, c'est-à-dire dont le champ lu est à false.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/piece.class.php");

class piece_non_lu extends piece {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
