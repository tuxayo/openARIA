<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/lien_autorite_police_courrier.class.php";

class lien_autorite_police_courrier extends lien_autorite_police_courrier_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Supprime tous les enregistrements de l'autorité de police.
     *
     * @param integer $autorite_police Identifiant de l'autorité de police
     *
     * @return boolean
     */
    function delete_by_autorite_police($autorite_police) {
        // Récupère la liste des enregistrements
        $list_lien_autorite_police_courrier = $this->get_lien_autorite_police_courrier_by_autorite_police($autorite_police);
        // S'il y a des liens
        if (is_array($list_lien_autorite_police_courrier) && count($list_lien_autorite_police_courrier) > 0) {
            // Pour chaque enregistrement
            foreach ($list_lien_autorite_police_courrier as $lien_autorite_police_courrier) {
                // Instancie l'enregistrment
                $lien_autorite_police_courrier_instance = new lien_autorite_police_courrier($lien_autorite_police_courrier);
                // initialisation de la clé primaire
                $val['lien_autorite_police_courrier'] = $lien_autorite_police_courrier;
                // Supprime l'enregistrement
                $delete = $lien_autorite_police_courrier_instance->supprimer($val);
                //
                if ($delete == false) {
                    return false;
                }
            }
        }
        //
        return true;
    }

    /**
     * Récupère la liste des liens par l'identifiant de l'autorité de police.
     *
     * @param integer $autorite_police Identifiant de l'autorité de police
     *
     * @return boolean
     */
    function get_lien_autorite_police_courrier_by_autorite_police($autorite_police) {
        // Initialisation de la variable de résultat
        $result = array();
        // Si l'autorité de police est renseigné
        if (!empty($autorite_police)) {
            // Requête SQL
            $sql = "SELECT lien_autorite_police_courrier
                    FROM ".DB_PREFIXE."lien_autorite_police_courrier
                    WHERE autorite_police = ".intval($autorite_police);
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            // Stockage du résultat dans un tableau
            while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
                $result[] = $row['lien_autorite_police_courrier'];
            }
        }
        // Retourne le résultat
        return $result;
    }

}

?>
