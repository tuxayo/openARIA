<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/dossier_instruction.class.php";

class dossier_instruction extends dossier_instruction_gen {

    var $inst_dossier_coordination = null;
    var $inst_etablissement = null;
    var $inst_acteur = null;

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array(
            "is_not_closed",
            "is_from_good_service",
        );

        // ACTION - 002 - supprimer
        // On ne peut pas supprimer un DI depuis l'interface
        $this->class_actions[2] = null;

        // ACTION - 005 - cloturer
        //
        $this->class_actions[5] = array(
            "identifier" => "cloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("cloturer"),
                "order" => 100,
                "class" => "lock-16",
            ),
            "view" => "formulaire",
            "method" => "cloturer",
            "button" => "cloturer",
            "permission_suffix" => "cloturer",
            "condition" => array(
                "is_cloturable",
                "is_from_good_service",
            ),
        );

        // ACTION - 006 - decloturer
        //
        $this->class_actions[6] = array(
            "identifier" => "decloturer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("reouvrir"),
                "order" => 110,
                "class" => "unlock-16",
            ),
            "view" => "formulaire",
            "method" => "decloturer",
            "button" => "decloturer",
            "permission_suffix" => "decloturer",
            "condition" => array(
                "is_decloturable",
                "is_from_good_service",
            ),
        );

        // ACTION - 007 - analyses
        //
        $this->class_actions[7] = array(
            "identifier" => "analyses",
            "view" => "view_analyses",
            "permission_suffix" => "analyses",
        );

        // ACTION - 008 - affecter_par_lot
        //
        $this->class_actions[8] = array(
            "identifier" => "affecter_par_lot",
            "view" => "view_batch_assignment",
            "permission_suffix" => "affecter_par_lot",
        );

        // ACTION - 009 - proces_verbal
        //
        $this->class_actions[9] = array(
            "identifier" => "proces_verbal",
            "view" => "view_proces_verbal",
            "permission_suffix" => "proces_verbal",
        );

        // ACTION - 250 - rename_all_libelle
        // Action utilitaire à des fins d'administration
        $this->class_actions[250] = array(
            "identifier" => "rename_all_libelle",
            "view" => "view_rename_all_libelle",
        );

        // ACTION - 010 - a_poursuivre
        //
        $this->class_actions[10] = array(
            "identifier" => "a_poursuivre",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("a poursuivre"),
                "order" => 110,
                "class" => "a_poursuivre-16",
            ),
            "method" => "a_poursuivre",
            "permission_suffix" => "a_poursuivre",
            "condition" => array(
                "can_di_status_be_a_poursuivre",
                "is_from_good_service",
            ),
        );

        // ACTION - 011 - programmer
        //
        $this->class_actions[11] = array(
            "identifier" => "programmer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("programmer"),
                "order" => 110,
                "class" => "programme-16",
            ),
            "method" => "programmer",
            "permission_suffix" => "programmer",
            "condition" => array(
                "can_di_status_be_programme",
                "is_from_good_service",
            ),
        );

        // ACTION - 012 - a_programmer
        //
        $this->class_actions[12] = array(
            "identifier" => "a_programmer",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("a programmer"),
                "order" => 110,
                "class" => "a_poursuivre-16",
            ),
            "method" => "a_programmer",
            "permission_suffix" => "a_programmer",
            "condition" => array(
                "can_di_status_be_a_programmer",
                "is_from_good_service",
            ),
        );

        // ACTION - 013 - gerer_completude
        //
        $this->class_actions[13] = array(
            "identifier" => "gerer_completude",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("gerer_completude"),
                "order" => 80,
                "class" => "edit-16",
            ),
            "view" => "formulaire",
            "method" => "gerer_completude",
            "button" => _("gerer_completude"),
            "permission_suffix" => "gerer_completude",
            "condition" => array(
                "is_not_closed",
                "is_from_good_service",
            ),
        );

        // ACTION - 021 - view_document_entrant_tab
        //
        $this->class_actions[21] = array(
            "identifier" => "document-entrant-tab",
            "view" => "view_document_entrant_tab",
            "permission_suffix" => "consulter",
        );

        // ACTION - 030 - Redirection vers le SIG
        $this->class_actions[30] = array(
            "identifier" => "localiser",
            "view" => "view_localiser",
            "permission_suffix" => "consulter",
        );

    }

    /**
     * VIEW - view_document_entrant_tab.
     *
     * @return void
     */
    function view_document_entrant_tab() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        //
        $inst_dc = $this->get_inst_dossier_coordination();
        //
        if (($inst_dc->getVal("dossier_autorisation_ads") == ""
             && $inst_dc->getVal("dossier_instruction_ads") == "")
            || $this->f->is_swrod_enabled() === false) {
            header("Location:../scr/soustab.php?obj=piece&retourformulaire=dossier_instruction&idxformulaire=".$this->getVal("dossier_instruction"));
            exit();
        }
        //
        $this->f->displaySubTitle("-> "._("documents entrants"));
        //
        printf('
<div id="document-entrant-tabs">
<ul>
<li>
    <a id="onglet-document_entrant_interne__contexte_di" href="../scr/soustab.php?obj=piece&retourformulaire=dossier_instruction&idxformulaire=%1$s">Interne</a>
</li>
<li>
    <a id="onglet-document_entrant_guichet_unique__contexte_di" href="../scr/form.php?obj=piece&action=22&idx=0&dc=%2$s">Guichet unique</a>
</li>
</ul>
</div>

',
            $this->getVal("dossier_instruction"),
            $this->getVal("dossier_coordination")
        );
    }

    /**
     *
     */
    function get_inst_acteur($acteur = null) {
        //
        if (is_null($this->inst_acteur)) {
            //
            if (is_null($acteur)) {
                $acteur = $this->getVal("technicien");
            }
            //
            require_once "../obj/acteur.class.php";
            $this->inst_acteur = new acteur($acteur);
        }
        //
        return $this->inst_acteur;
    }

    /**
     *
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if (is_null($this->inst_dossier_coordination)) {
            //
            if (is_null($dossier_coordination)) {
                $dossier_coordination = $this->getVal("dossier_coordination");
            }
            //
            require_once "../obj/dossier_coordination.class.php";
            $this->inst_dossier_coordination = new dossier_coordination($dossier_coordination);
        }
        //
        return $this->inst_dossier_coordination;
    }

    var $inst_analyse = null;

    /**
     *
     */
    function get_inst_analyse($analyse = null) {
        //
        if (!is_null($analyse)) {
            //
            require_once "../obj/analyses.class.php";
            return new analyses($analyse);
        }
        //
        if (is_null($this->inst_analyse)) {
            //
            if (!is_int(intval($this->getVal($this->clePrimaire)))) {
                $analyse = 0;
            } else {
                //
                $analyse = $this->get_id_analyse();
                if ($analyse == false) {
                    $analyse = 0;
                }
            }
            //
            require_once "../obj/analyses.class.php";
            $this->inst_analyse = new analyses($analyse);
        }
        //
        return $this->inst_analyse;
    }

    /**
     *
     */
    function get_inst_etablissement($dossier_coordination = null) {
        //
        if (is_null($this->inst_etablissement)) {
            //
            $inst_dossier_coordination = $this->get_inst_dossier_coordination($dossier_coordination);
            $this->inst_etablissement = $inst_dossier_coordination->get_inst_etablissement();
        }
        //
        return $this->inst_etablissement;
    }

    /**
     *
     */
    function get_inst_service($service = null) {
        return $this->get_inst_common("service", $service);
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param string $ent Fil d'Ariane
     * @param array  $val Valeurs de l'objet
     * @param intger $maj Mode du formulaire
     */
    function getFormTitle($ent) {

        // Si différent de l'ajout
        if ($this->getParameter("maj") != 0) {

            // Si le champ libelle existe
            $libelle = trim($this->getVal("libelle"));
            if (!empty($libelle)) {
                $ent .= " -> ".$libelle;
            }
            //
            $inst_etablissement = $this->get_inst_etablissement();
            $ent .= " [".$inst_etablissement->getVal("code")." - ".$inst_etablissement->getVal("libelle")."]";
        }

        // Change le fil d'Ariane
        return $ent;
    }

    /**
     *
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        
        $form->setType('dossier_instruction','hidden');
        $form->setType('statut','hidden');
        $form->setType('dossier_coordination','dossier_coordination_link');
        $form->setType('dossier_coordination_date_demande','dossier_coordination_data');
        $form->setType('dossier_coordination_date_butoir','dossier_coordination_data');
        $form->setType('dossier_coordination_date_cloture','dossier_coordination_data');
        $form->setType('courrier_dernier_arrete','dossier_coordination_data');
        $form->setType('dossier_coordination_autorite_police_encours','dossier_coordination_data');
        $form->setType('dossier_coordination_dossier_cloture','dossier_coordination_data');
        $form->setType('dossier_coordination_etablissement_type','dossier_coordination_data');
        $form->setType('dossier_coordination_etablissement_categorie','dossier_coordination_data');
        $form->setType('dossier_coordination_etablissement_locaux_sommeil','dossier_coordination_data');
        $form->setType('dossier_coordination_enjeu_erp', 'hidden');
        $form->setType('dossier_coordination_enjeu_ads', 'hidden');
        $form->setType('etablissement_coordonnees','etablissement');
        $form->setType('exploitant','contact_exploitant');
        $form->setType('etablissement_type','etablissement_data');
        $form->setType('etablissement_categorie','etablissement_data');
        $form->setType('etablissement_locaux_sommeil','etablissement_data');
        $form->setType('dossier_coordination_interface_referentiel_ads', 'hidden');
        $form->setType('dossier_coordination_dossier_autorisation_ads', 'hidden');
        $form->setType('dossier_coordination_dossier_instruction_ads', 'hidden');
        $form->setType('service','selecthidden');
        
        $codeDossierType = $this->getCodeDossierType($this->getVal('dossier_instruction'));
        
        // MODE AJOUTER
        if ($maj == 0) {
            $form->setType('libelle','hidden');
            $form->setType('technicien','hidden');
            $form->setType('analyses','hidden');
            if ($codeDossierType === 'PLAN'){
                $form->setType('dossier_coordination_interface_referentiel_ads', 'hiddenstatic');
                $form->setType('dossier_coordination_dossier_autorisation_ads', 'hiddenstatic');
                $form->setType('dossier_coordination_dossier_instruction_ads', 'hiddenstatic');
            }
            
            $form->setType('dossier_coordination','hidden');
            $form->setType('dossier_coordination_date_demande','hidden');
            $form->setType('dossier_coordination_date_butoir','hidden');
            $form->setType('dossier_coordination_date_cloture','hidden');
            $form->setType('courrier_dernier_arrete','hidden');
            $form->setType('dossier_coordination_autorite_police_encours','hidden');
            $form->setType('dossier_coordination_etablissement_type','hidden');
            $form->setType('dossier_coordination_etablissement_categorie','hidden');
            $form->setType('dossier_coordination_etablissement_locaux_sommeil','hidden');
            $form->setType('dossier_coordination_dossier_cloture','hidden');
            $form->setType('etablissement_coordonnees','hidden');
            $form->setType('etablissement_type','hidden');
            $form->setType('etablissement_categorie','hidden');
            $form->setType('etablissement_locaux_sommeil','hidden');
            $form->setType('exploitant','hidden');
            $form->setType('dossier_coordination_interface_referentiel_ads', 'hidden');
            $form->setType('dossier_coordination_dossier_autorisation_ads', 'hidden');
            $form->setType('dossier_coordination_dossier_instruction_ads', 'hidden');
        }

        // MODE MODIFIER
        if ($maj == 1) {
            $form->setType("dossier_cloture", "checkboxstatic");
            $form->setType('analyses','hidden');
            if ($codeDossierType === 'PLAN'){
                if ($this->getVal("dossier_coordination_interface_referentiel_ads") == "t") {
                    $form->setType("dossier_coordination_interface_referentiel_ads", "checkboxstatic");
                }
                if ($this->getVal("dossier_coordination_enjeu_erp") == "t") {
                    $form->setType("dossier_coordination_enjeu_erp", "enjeu_erp");
                }
                if ($this->getVal("dossier_coordination_enjeu_ads") == "t") {
                    $form->setType("dossier_coordination_enjeu_ads", "enjeu_ads");
                }
                $form->setType('dossier_coordination_dossier_autorisation_ads', 'link_dossier_autorisation_ads');
                $form->setType('dossier_coordination_dossier_instruction_ads', 'hiddenstatic');
            }
            
            $form->setType('libelle','hiddenstatic');
            $form->setType('technicien','select');
        }
        
        // MODE SUPPRIMER ET CONSULTER
        if ($maj > 1) {
            $form->setType('analyses','hidden');
            if ($codeDossierType === 'PLAN'){
                if ($this->getVal("dossier_coordination_interface_referentiel_ads") == "t") {
                    $form->setType("dossier_coordination_interface_referentiel_ads", "checkboxstatic");
                }
                if ($this->getVal("dossier_coordination_enjeu_erp") == "t") {
                    $form->setType("dossier_coordination_enjeu_erp", "enjeu_erp");
                }
                if ($this->getVal("dossier_coordination_enjeu_ads") == "t") {
                    $form->setType("dossier_coordination_enjeu_ads", "enjeu_ads");
                }
                $form->setType('dossier_coordination_dossier_autorisation_ads', 'link_dossier_autorisation_ads');
                $form->setType('dossier_coordination_dossier_instruction_ads', 'hiddenstatic');
            }
            
            if ( $this->getVal('incompletude')==='f'){
                $form->setType('piece_attendue','hidden');
            }
        }

        // ACTION - GERER LA COMPLETUDE
        if ($maj == 13) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
            //
            $form->setType("incompletude", "checkbox");
            $form->setType("piece_attendue", "textarea");
        }

        // Pour les actions supplémentaires qui utilisent la vue formulaire
        // il est nécessaire de cacher les champs ou plutôt de leur affecter un
        // type pour que l'affichage se fasse correctement
        if ($maj == 5 || $maj == 6) {
            //
            foreach ($this->champs as $champ) {
                $form->setType($champ, "hidden");
            }
        }

    }
    
    function setLib(&$form,$maj) {
        //
        parent::setLib($form, $maj);
        //libelle des champs
        $form->setLib('dossier_coordination_date_demande',_('dossier_coordination_date_demande'));
        $form->setLib('dossier_coordination_date_butoir',_('dossier_coordination_date_butoir'));
        $form->setLib('dossier_coordination_date_cloture',_('dossier_coordination_date_cloture'));
        $form->setLib('courrier_dernier_arrete',_('courrier_dernier_arrete'));
        $form->setLib('dossier_coordination_autorite_police_encours',_('dossier_coordination_autorite_police_encours'));
        $form->setLib('dossier_coordination_etablissement_type',_('dossier_coordination_etablissement_type'));
        $form->setLib('dossier_coordination_etablissement_categorie',_('dossier_coordination_etablissement_categorie'));
        $form->setLib('dossier_coordination_etablissement_locaux_sommeil',_('dossier_coordination_etablissement_locaux_sommeil'));
        $form->setLib('dossier_coordination_dossier_cloture',_('dossier_coordination_dossier_cloture'));
        $form->setLib('etablissement_coordonnees',_('etablissement'));
        $form->setLib('etablissement_type',_('etablissement_type'));
        $form->setLib('etablissement_categorie',_('etablissement_categorie'));
        $form->setLib('etablissement_locaux_sommeil',_('etablissement_locaux_sommeil'));
        $form->setLib('exploitant',_('exploitant'));
        $form->setLib('dossier_coordination_enjeu_erp', _("enjeu ERP"));
        $form->setLib('dossier_coordination_enjeu_ads', _("enjeu ADS"));
        $form->setLib('dossier_coordination_interface_referentiel_ads', _("connecté au référentiel ADS"));
        $form->setLib('dossier_coordination_dossier_autorisation_ads', _('dossier_coordination_dossier_autorisation_ads'));
        $form->setLib('dossier_coordination_dossier_instruction_ads', _('dossier_coordination_dossier_instruction_ads'));
    }
    
    // Mise en page
    function setLayout(&$form, $maj) {
        //
        $form->setBloc($this->champs[0], 'D', "", "form-dossier_instruction-action-".$maj);
        //
        if ($maj == 13) {
            // Layout spécifique simplifié pour ACTION - GERER LA COMPLETUDE
            $form->setFieldset('incompletude','D',_('Gestion de la complétude'), "");
            $form->setFieldset('piece_attendue','F','');
        } else {
            // Bloc d'informations générales du DI
            $form->setBloc('dossier_instruction', 'D', "", "col_12");
            $form->setBloc('dossier_instruction', 'D', "", "col_12");
            $form->setFieldset('dossier_instruction','D',_('Informations générales'), "");
            $form->setBloc('dossier_instruction','D','');
            $form->setBloc('notes','F','');
            $form->setFieldset('notes','F','');
            $form->setBloc('notes','F','');
            $form->setBloc('notes','F','');

            // Bloc d'informations concernant le dossier de coordination
            $form->setBloc('dossier_coordination', 'D', "", "col_6");
            $form->setBloc('dossier_coordination', 'D', "", "col_12");
            $form->setFieldset('dossier_coordination', 'D', _('Dossier de coordination'), "");
            $form->setFieldset('dossier_coordination_etablissement_locaux_sommeil', 'F', '');
            $form->setBloc('dossier_coordination_etablissement_locaux_sommeil', 'F');
            $form->setBloc('dossier_coordination_etablissement_locaux_sommeil', 'F');
            
            // Bloc d'informations concernant l'établissement
            $form->setBloc('etablissement_coordonnees', 'D', "", "col_6");
            $form->setBloc('etablissement_coordonnees', 'D', "", "col_12");
            $form->setFieldset('etablissement_coordonnees', 'D', _('Etablissement'), "");
            $form->setFieldset('exploitant', 'F', '');
            $form->setBloc('exploitant', 'F');
            $form->setBloc('exploitant', 'F');
            
            // Bloc urbanisme
            $form->setBloc('dossier_coordination_interface_referentiel_ads', 'D', "", "col_6");
            $form->setBloc('dossier_coordination_interface_referentiel_ads', 'D', "", "col_12");
            $form->setFieldset('dossier_coordination_interface_referentiel_ads', 'D', _('Urbanisme'), "");
            $form->setFieldset('dossier_coordination_dossier_instruction_ads', 'F', '');
            $form->setBloc('dossier_coordination_dossier_instruction_ads', 'F');
            $form->setBloc('dossier_coordination_dossier_instruction_ads', 'F');
        }
        //
        $form->setBloc(end($this->champs), 'F');
    }
    
    /*
     * Récupère le nom concaténé au prénom de l'utilisateur qui est connecté
     * 
     * @return string Le nom et prénom de l'utilisateur connecté
     */
    function getNomPrenom(){
        
        //Requête
        $sql = "SELECT nom_prenom
            FROM ".DB_PREFIXE."acteur
            LEFT JOIN ".DB_PREFIXE."om_utilisateur 
                ON acteur.om_utilisateur = om_utilisateur.om_utilisateur 
            WHERE om_utilisateur.login = '".$_SESSION['login']."'";
        $nom_prenom = $this->f->db->getOne($sql);
        $this->addToLog("getNomPrenom(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($nom_prenom);
        
        return $nom_prenom;
    }
    
    /*
     * Test si le dossier d'instruction avec l'identifant passé en paramètre est
     * un dossier de type 'visite'
     * 
     * @param integer $dossier L'identifiant du dossier d'instruction
     * @return boolean
     */
    function getCodeDossierType($dossier){
        
        $codeDossierType = '';
        
        if ($dossier!=''&&is_numeric($dossier)){
            $sql = "SELECT dossier_type.code 
                FROM ".DB_PREFIXE."dossier_type 
                LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                    ON dossier_type.dossier_type = dossier_coordination_type.dossier_type
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                WHERE dossier_instruction = ".$dossier;
            $codeDossierType = $this->f->db->getOne($sql);
            $this->addToLog("getCodeDossierType(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($codeDossierType);
        }
        
        return $codeDossierType;
    }
    
    /**
     * Méthode de traitement des données retournées par le formulaire
     */
    function setvalF($val = array()) {
        
        parent::setvalF($val);
        //
        if ($val['incompletude']=='f'||$val['incompletude']==''){
            $this->valF['piece_attendue']='';
        }
    }
    
    /**
     * Teste si le dossier de coordination du dossier d'instruction est clôturé
     * 
     * @param integer $dossier L'identifiant du dossier
     * 
     * @return boolean true si le dossier de coordination du dossier d'instruction 
     *                    est clôturé,
     *               false si le dossier de coordination du dossier d'instruction 
     *                    n'est pas clôturé
     */
    function isDossierCoordinationCloture($dossier){
        //
        if ($dossier!=''&&is_numeric($dossier)){
            //
            $sql = "SELECT dossier_coordination.dossier_cloture
                FROM ".DB_PREFIXE."dossier_coordination 
                LEFT JOIN ".DB_PREFIXE."dossier_instruction
                    ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                WHERE dossier_instruction = ".$dossier;
            $isDossierCoordinationCloture = $this->f->db->getOne($sql);
            $this->addToLog("getCodeDossierType(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($isDossierCoordinationCloture);
            //
            if ($isDossierCoordinationCloture!=''&&$isDossierCoordinationCloture=='t'){
                return true;
            }
            else {
                return false;
            }
        }
    }
    
    /**
     * Teste si le dossier de instruction est clôturé
     * 
     * @param integer $dossier L'identifiant du dossier
     * 
     * @return boolean true si le dossier de instruction du dossier d'instruction 
     *                    est clôturé,
     *               false si le dossier de instruction du dossier d'instruction 
     *                    n'est pas clôturé
     */
    function isDossierInstructionCloture($dossier){
        //
        if ($dossier!=''&&is_numeric($dossier)){
            //
            $sql = "SELECT dossier_instruction.dossier_cloture
                FROM ".DB_PREFIXE."dossier_instruction 
                WHERE dossier_instruction = ".$dossier;
            $isDossierInstructionCloture = $this->f->db->getOne($sql);
            $this->addToLog("isDossierInstructionCloture(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($isDossierInstructionCloture);
            //
            if ($isDossierInstructionCloture!=''&&$isDossierInstructionCloture=='t'){
                return true;
            }
            else {
                return false;
            }
        }
    }
    

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        //Initialisation des variables
        // Paramètres envoyés au type spécifique dossier_coordination
        $valueDateDemande = '';
        $valueDateButoir = '';
        $valueDateCloture = '';
        $valueDernierArrete = '';
        $valueAutoritePoliceEncours = '';
        $valueDossierCloture = '';
        $values_etablissement = array(
            "code" => "",
            "libelle" => "",
            "adresse_numero" => "",
            "adresse_numero2" => "",
            "adresse_voie" => "",
            "adresse_complement" => "",
            "lieu_dit" => "",
            "boite_postale" => "",
            "adresse_cp" => "",
            "adresse_ville" => "",
            "adresse_arrondissement" => "",
            "cedex" => "",
            "etablissement_nature_libelle" => "",
            "etablissement_type_libelle" => "",
            "etablissement_categorie_libelle" => "",
            "si_locaux_sommeil" => "",
            "geolocalise" => "",
        );
        $etablissement_id = '';
        $values_exploitant = '';
        $etablissementType = '';
        $etablissementCategorie = '';
        $etablissement_locaux_sommeil = '';
        $dossier_coordination['dossier_autorisation_ads'] = '';
        $dossier_coordination['dossier_instruction_ads'] = '';
        $valueObj = '';
        $valuelibelle = '';
        $valueIdx = '';
        
        // En modes "modifier", "supprimer" et "consulter"
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            
            // Données du dossier de coordination
            $dossier_coordinationId = $this->getVal('dossier_coordination');
            if ($dossier_coordinationId!=''&&  is_numeric($dossier_coordinationId)){
                require_once '../obj/dossier_coordination.class.php';
                $dossier_coordination = new dossier_coordination($dossier_coordinationId);
                
                $valueObj = "dossier_coordination";
                $valuelibelle = $dossier_coordination->getVal('libelle');
                $valueIdx = $dossier_coordination->getVal($dossier_coordination->clePrimaire);
                $valueDateDemande = $this->dateDBToForm($dossier_coordination->getVal('date_demande'));
                $valueDateButoir = $this->dateDBToForm($dossier_coordination->getVal('date_butoir'));
                $valueDateCloture = $this->dateDBToForm($dossier_coordination->getVal('date_cloture'));
                $valueDernierArrete = '';
                $valueAutoritePoliceEncours = ($dossier_coordination->getVal('autorite_police_encours')=='f')?_("Non"):_("Oui");
                $valueDossierCloture = ($dossier_coordination->getVal('dossier_cloture')=='f')?_("Non"):_("Oui");
                $valueGeolocalise = $dossier_coordination->getVal('geolocalise');
            }
            // Initialisation de l'objet
            $obj = "etablissement_tous";
            // Initialisation des variables
            $libelle = '';

            //Récupération de l'identifiant de l'établissement
            $sql = "SELECT etablissement 
                FROM ".DB_PREFIXE."dossier_coordination
                WHERE dossier_coordination.dossier_coordination = ".$this->getVal('dossier_coordination');
            $etablissementId = $this->f->db->getOne($sql);
            $this->addToLog("setSelect(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($etablissementId);
            
            if ( $etablissementId!=''&&is_numeric($etablissementId)){
                
                // Instance de la classe etablissement
                require_once '../obj/etablissement.class.php';
                $etablissement = new etablissement($etablissementId);

                // Récupère toutes les données nécessaires de l'établissement
                $values_etablissement = $etablissement->get_data($etablissementId);
                $etablissement_id=$etablissementId;
                
                $libelle = $etablissement->getVal('code')." - ".$etablissement->getVal('libelle');
                
                // Récupère l'exploitant de l'établissement
                $exploitantId = $etablissement->recuperer_ID_exploitant($etablissementId);

                // Initialisation des variables
                $values_exploitant = '';
                
                // Si l'établissement a un exploitant
                if ($exploitantId != "]") {
                    // Instance de la classe contact
                    require_once '../obj/contact.class.php';
                    $exploitant = new contact($exploitantId);

                    // Récupère toutes les données nécessaires du contact
                    $values_exploitant = $exploitant->get_data($exploitantId);
                }
                
                //Récupération du libelle du type de l'établissement
                $sql = "SELECT etablissement_type.libelle
                        FROM ".DB_PREFIXE."etablissement_type 
                        LEFT JOIN ".DB_PREFIXE."etablissement 
                            ON etablissement.etablissement_type = etablissement_type.etablissement_type
                        WHERE etablissement = ".$etablissementId;
                $etablissementType = $this->f->db->getOne($sql);
                $this->addToLog("setSelect(): db->getone(\"".$sql."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($etablissementType);
                
                //Récupération du libelle de la catégorie de l'établissement
                $sql = "SELECT etablissement_categorie.libelle
                        FROM ".DB_PREFIXE."etablissement_categorie 
                        LEFT JOIN ".DB_PREFIXE."etablissement 
                            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
                        WHERE etablissement = ".$etablissementId;
                $etablissementCategorie = $this->f->db->getOne($sql);
                $this->addToLog("setSelect(): db->getone(\"".$sql."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($etablissementCategorie);
                
                $etablissement_locaux_sommeil = ($etablissement->getVal('si_locaux_sommeil')=='f')?_("Non"):_("Oui");
            }
            
            // Récupération des données de l'établissement dans le dossier de 
            // coordination
            $sql = "SELECT etablissement_type.libelle as etablissement_type, 
                etablissement_categorie.libelle as etablissement_categorie, 
                CASE WHEN etablissement_locaux_sommeil = 't' 
                THEN '"._("Oui")."'
                ELSE '"._("Non")."'
                END as etablissement_locaux_sommeil
                FROM ".DB_PREFIXE."dossier_coordination
                LEFT JOIN ".DB_PREFIXE."etablissement_type
                    ON dossier_coordination.etablissement_type=etablissement_type.etablissement_type
                LEFT JOIN ".DB_PREFIXE."etablissement_categorie
                    ON dossier_coordination.etablissement_categorie=etablissement_categorie.etablissement_categorie
                WHERE dossier_coordination =".$this->getVal("dossier_coordination");
            $dossier_coordination_data = $this->f->db->query($sql);
            $this->addToLog("setSelect(): db->query(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($dossier_coordination_data);
            $dossier_coordination_data = $dossier_coordination_data->fetchRow(DB_FETCHMODE_ASSOC);
            // Paramètres envoyés au type spécifique dossier_coordination_data
            $params = array();
            $params['value'] = $dossier_coordination_data["etablissement_type"];
            $form->setSelect("dossier_coordination_etablissement_type", $params);
            $params = array();
            $params['value'] = $dossier_coordination_data["etablissement_categorie"];
            $form->setSelect("dossier_coordination_etablissement_categorie", $params);
            $params = array();
            $params['value'] = $dossier_coordination_data["etablissement_locaux_sommeil"];
            $form->setSelect("dossier_coordination_etablissement_locaux_sommeil", $params);
            
            // Paramétres envoyés au type link dossier_coordination
            $params = array();
            $params['obj'] = $valueObj;
            $params['libelle'] = $valuelibelle;
            $params['idx'] = $valueIdx;
            $params['geolocalise'] = $valueGeolocalise;
            $form->setSelect("dossier_coordination", $params);
            
            // Paramètres envoyés au type spécifique dossier_coordination
            $params = array();
            $params['value'] = $valueDateDemande;
            $form->setSelect("dossier_coordination_date_demande", $params);
            // Paramètres envoyés au type spécifique dossier_coordination
            $params = array();
            $params['value'] = $valueDateButoir;
            $form->setSelect("dossier_coordination_date_butoir", $params);
            // Paramètres envoyés au type spécifique dossier_coordination
            $params = array();
            $params['value'] = $valueDateCloture;
            $form->setSelect("dossier_coordination_date_cloture", $params);
            // Paramètres envoyés au type spécifique contact_exploitant
            $params = array();
            $params['value'] = $valueDernierArrete;
            $form->setSelect("courrier_dernier_arrete", $params);
            // Paramètres envoyés au type spécifique contact_exploitant
            $params = array();
            $params['value'] = $valueAutoritePoliceEncours;
            $form->setSelect("dossier_coordination_autorite_police_encours", $params);
            // Paramètres envoyés au type spécifique contact_exploitant
            $params = array();
            $params['value'] = $valueDossierCloture;
            $form->setSelect("dossier_coordination_dossier_cloture", $params);
            
            // Paramétres envoyés au type link et au type spécifique 
            // etablissement
            $params = array();
            $params['obj'] = $obj;
            $params['libelle'] = $libelle;
            $params['values'] = $values_etablissement;
            $params['idx'] = $etablissement_id;
            $form->setSelect("etablissement_coordonnees", $params);
            
            // Paramètres envoyés au type spécifique contact_exploitant
            $params = array();
            $params['values'] = $values_exploitant;
            $form->setSelect("exploitant", $params);
            
            // Paramètres envoyés au type spécifique etablissement_type
            $params = array();
            $params['libelle'] = $etablissementType;
            $form->setSelect("etablissement_type", $params);
            
            // Paramètres envoyés au type spécifique etablissement_categorie
            $params = array();
            $params['libelle'] = $etablissementCategorie;
            $form->setSelect("etablissement_categorie", $params);
            
            // Paramètres envoyés au type spécifique etablissement_locaux_sommeil
            $params = array();
            $params['libelle'] = $etablissement_locaux_sommeil;
            $form->setSelect("etablissement_locaux_sommeil", $params);

            //
            if ($this->f->is_option_referentiel_ads_enabled() === true
                && $this->getVal("dossier_coordination_dossier_autorisation_ads") != "") {
                //
                $inst_referentiel_ads = $this->f->get_inst_referentiel_ads();
                $url_view_da = $inst_referentiel_ads->get_url_view_da(
                    $this->getVal("dossier_coordination_dossier_autorisation_ads")
                );
                if ($url_view_da !== false) {
                    $form->setSelect(
                        "dossier_coordination_dossier_autorisation_ads",
                        array("link" => $url_view_da, )
                    );
                }
            }

        }

        // XXX Problème dans la recherche avancée à qualifier
        if ($maj != 999) {
            //Remplacement des variables par les identifiants des services
            // QUERY liste des acteurs qui peuvent instruire un dossier
            $sql_technicien = "
                SELECT 
                    acteur.acteur, 
                    acteur.nom_prenom 
                FROM 
                    ".DB_PREFIXE."acteur 
                WHERE 
                    (acteur.role = 'technicien' 
                     OR acteur.role = 'cadre')
                    AND acteur.service=".$this->getVal('service')."
                    AND ((acteur.om_validite_debut IS NULL 
                          AND (acteur.om_validite_fin IS NULL 
                               OR acteur.om_validite_fin > CURRENT_DATE)) 
                         OR (acteur.om_validite_debut <= CURRENT_DATE
                             AND (acteur.om_validite_fin IS NULL 
                                  OR acteur.om_validite_fin > CURRENT_DATE)))
                ORDER BY 
                    acteur.nom_prenom ASC
            ";
            $this->init_select($form, $this->f->db, $maj, null, "technicien", $sql_technicien, $sql_technicien_by_id, false);
        }


        // En mode "ajouter" et "Modifier"
        if ($maj == 0 || $maj == 1) {
            // Si le service du dossier d'instruction est renseigné
            if (!empty($form->val['service'])) {
                // Cherche les autorités compétentes du même service
                $sql_autorite_competente_by_service = str_replace('<idx>', $form->val['service'], $sql_autorite_competente_by_service);
                $res = $this->f->db->query($sql_autorite_competente_by_service);
                $this->addToLog("setSelect(): db->query(".$sql_autorite_competente_by_service.");", VERBOSE_MODE);
                $this->f->isDatabaseError($res);
                // Initialisation du select
                $contenu = array();
                $contenu[0][0] = '';
                $contenu[1][0] = _('choisir')."&nbsp;"._("autorite_competente");
                // Construit le select avec les résultats
                $k=1;
                while($row =& $res->fetchRow()){
                    $contenu[0][$k] = $row[0];
                    $contenu[1][$k] = $row[1];
                    $k++;
                }
                //
                $form->setSelect("autorite_competente", $contenu);
            }
        }

        //
        $form->setSelect("statut", $statut);
    }
    
    /**
     * Récupère le code du type du dossier par rapport au dossier de coordination
     *
     * @param integer $dossier_coordination Identifiant du DC
     *
     * @return string Code du type du dossier
     */
    function get_dossier_type_code_by_dossier_coordination($dossier_coordination) {
        // Initialisation du résultat retourné
        $code = "";

        // Si dossier_coordination n'est pas vide
        if (!empty($dossier_coordination)) {

            // Requête SQL
            $sql = "
                SELECT LOWER(dossier_type.code)
                FROM ".DB_PREFIXE."dossier_type
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
                        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination
                        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
                WHERE dossier_coordination.dossier_coordination = ".$dossier_coordination;
            $this->addToLog("get_dossier_type_code_by_dossier_coordination() db->getOne(\"".$sql."\");",
                VERBOSE_MODE);
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);

        }

        // Résultat retourné
        return $code;
    }

    /**
     * CONDITION - is_closed.
     *
     * Permet d'indiquer si le dossier d'instruction est clôturé ou non.
     *
     * @return boolean
     */
    function is_closed() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        if ($dossier_cloture == "t") {
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - is_not_closed.
     *
     * Permet d'indiquer si le dossier d'instruction est clôturé ou non.
     *
     * @return boolean
     */
    function is_not_closed() {
        //
        return !$this->is_closed();
    }

    /**
     * CONDITION - is_connected_to_referentiel_ads.
     *
     * @return boolean
     */
    function is_connected_to_referentiel_ads() {
        $inst_dc = $this->get_inst_dossier_coordination();
        return $inst_dc->is_connected_to_referentiel_ads();
    }

    /**
     * Condition pour afficher le bouton cloturer
     *
     * @return boolean
     */
    function is_cloturable() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        $a_qualifier = $this->getVal("a_qualifier");
        //
        $last_date_visite = $this->get_date_visite($this->get_last_visite());

        // Instance de la classe dossier_coordination
        require_once '../obj/dossier_coordination.class.php';
        $dossier_coordination = new dossier_coordination($this->getVal('dossier_coordination'));
        //
        $is_periodic = $dossier_coordination->is_periodic();

        // Si le dossier de coordination est périodique
        if ($is_periodic == true) {
            // Si le dossier d'instruction n'est pas à qualifié, n'est pas
            // clôturé et qu'il possède une visite
            if (($a_qualifier == "f" || $a_qualifier == "Non")
                && ($dossier_cloture == "f" || $dossier_cloture == "Non")
                && $last_date_visite != false) {
                //
                return true;
            } else {
                //
                return false;
            }
        }

        // Si le dossier d'instruction n'est pas à qualifié
        // et n'est pas clôturé
        if (($a_qualifier == "f" || $a_qualifier == "Non")
            && ($dossier_cloture == "f" || $dossier_cloture == "Non")) {
            //
            return true;
        } else {
            //
            return false;
        }
    }

    /**
     * Condition pour afficher le bouton decloturer
     *
     * @return boolean
     */
    function is_decloturable() {
        //
        $dossier_cloture = $this->getVal("dossier_cloture");
        //
        $a_qualifier = $this->getVal("a_qualifier");

        // Instance de la classe dossier_coordination
        require_once '../obj/dossier_coordination.class.php';
        $dossier_coordination = new dossier_coordination($this->getVal('dossier_coordination'));
        //
        $is_periodic = $dossier_coordination->is_periodic();
        //
        $dossier_coordination_dossier_cloture = $dossier_coordination->getVal('dossier_cloture');

        // Si le dossier de coordination est périodique
        if ($is_periodic == true) {
            // Si le dossier d'instruction n'est pas à qualifié, n'est pas
            // clôturé et que le dossier de coordination n'est pas clôturé
            if (($a_qualifier == "f" || $a_qualifier == "Non")
                && ($dossier_cloture == "t" || $dossier_cloture == "Oui")
                && $dossier_coordination_dossier_cloture = 'f') {
                //
                return true;
            } else {
                //
                return false;
            }
        }

        // Si le dossier d'instruction n'est pas à qualifié
        // et n'est pas clôturé
        if (($a_qualifier == "f" || $a_qualifier == "Non")
            && ($dossier_cloture == "t" || $dossier_cloture == "Oui")) {
            //
            return true;
        } else {
            //
            return false;
        }
    }

    /**
     * TREATMENT - cloturer.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function cloturer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_cloturing("cloture", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - decloturer.
     * 
     * Permet de ...
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function decloturer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);

        //
        $ret = $this->manage_cloturing("uncloture", $val);
        // Si le traitement ne s'est pas déroulé correctement
        if ($ret !== true) {
            // Return
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     *
     */
    function manage_cloturing($mode = null, $val = array()) {
        // Logger
        $this->addToLog("manage_cloturing() - begin", EXTRA_VERBOSE_MODE);
        // Si le mode n'existe pas on retourne false
        if ($mode != "cloture" && $mode != "uncloture") {
            return false;
        }
        // Récuperation de la valeur de la cle primaire de l'objet
        $id = $this->getVal($this->clePrimaire);
        // 
        if ($mode == "cloture") {
            $valF = array(
                "dossier_cloture" => 't',
                "a_qualifier" => 'f',
                "date_cloture" => date("Y-m-d"),
            );
            $valid_message = _("Le %s a ete correctement cloture.");
        } elseif ($mode == "uncloture") {
            $valF = array(
                "dossier_cloture" => 'f',
                "date_cloture" => null,
            );
            $valid_message = _("Le %s a ete correctement reouvert.");
        }
        //
        $this->correct = true;
        // Execution de la requête de modification des donnees de l'attribut
        // valF de l'objet dans l'attribut table de l'objet
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            $valF,
            DB_AUTOQUERY_UPDATE,
            $this->getCle($id)
        );
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        } else {
            //
            $valid_message = sprintf($valid_message, _("dossier_instruction"));
            //
            $main_res_affected_rows = $this->f->db->affectedRows();
            // Log
            $this->addToLog(_("Requete executee"), VERBOSE_MODE);
            // Log
            $message = _("Enregistrement")."&nbsp;".$id."&nbsp;";
            $message .= _("de la table")."&nbsp;\"".$this->table."\"&nbsp;";
            $message .= "[&nbsp;".$main_res_affected_rows."&nbsp;";
            $message .= _("enregistrement(s) mis a jour")."&nbsp;]";
            $this->addToLog($valid_message, VERBOSE_MODE);
            // Message de validation
            if ($main_res_affected_rows == 0) {
                $this->addToMessage(_("Attention vous n'avez fait aucune modification.")."<br/>");
            } else {
                $this->addToMessage($valid_message."<br/>");
            }
        }
        // Logger
        $this->addToLog("manage_cloturing() - end", EXTRA_VERBOSE_MODE);
        //
        return true;
    }
    
    function setValFAjout($val = array()) {
        //
        parent::setValFAjout($val);
        // Ajout automatique du technicien en fonction de l'arrondissement et 
        // si le dossier d'instruction est du service accessibilité
        // On récupère l'identifiant du service accessibilité
        $sql = "SELECT service.service FROM ".DB_PREFIXE."service WHERE code = 'ACC'";
        $service = $this->f->db->getOne($sql);
        $this->addToLog("setValFAjout(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($service);
        //
        if ($service!=''&& is_numeric($service)&&$val['service']==$service){
            
            // On récupère l'identifiant du technicien
            $sql = "SELECT technicien 
                    FROM ".DB_PREFIXE."technicien_arrondissement
                    LEFT JOIN ".DB_PREFIXE."arrondissement
                        ON technicien_arrondissement.arrondissement = arrondissement.arrondissement
                    LEFT JOIN ".DB_PREFIXE."etablissement
                        ON arrondissement.arrondissement = etablissement.adresse_arrondissement
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination
                        ON etablissement.etablissement = dossier_coordination.etablissement
                    WHERE technicien_arrondissement.service = '".$val['service']."' AND
                    dossier_coordination.dossier_coordination = '".$val['dossier_coordination']."'";
            $technicien = $this->f->db->getOne($sql);
            $this->addToLog("setValFAjout(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($technicien);
            //
            
            if ($technicien!=''&& is_numeric($technicien)){
                $this->valF['technicien'] = $technicien;
            }
        }
    }

    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        
        // Création de l'analyse
        require_once "../obj/analyses.class.php";
        $analyse = new analyses("]");
        if(!$analyse->create_analyse($this->valF[$this->clePrimaire])) {
            return false;
        }

        // Instance de l'établissement
        $inst_etablissement = $this->get_inst_etablissement($this->valF["dossier_coordination"]);

        // Si l'établissement est renseigné
        if ($inst_etablissement->getVal($inst_etablissement->clePrimaire) != null) {

            // Si l'établissement ne possède aucune unité d'accessibilité
            // validée et qu'il s'agit du service d'accessibilité
            if ($inst_etablissement->has_validated_unite_accessibilite() == false
                && $this->is_accessibility_service($this->valF[$this->clePrimaire]) == true) {
                // Instance de la classe etablissement_unite
                require_once "../obj/etablissement_unite.class.php";
                $inst_etablissement_unite = new etablissement_unite(']', $this->f->db, false);
                // Récupération des valeurs
                $values = array();
                foreach($inst_etablissement_unite->champs as $key => $champ) {
                    $values[$champ] = '';
                }
                // Valeurs à modifier
                $values['libelle'] = $inst_etablissement->getVal("libelle");
                $values['etablissement'] = $inst_etablissement->getVal($inst_etablissement->clePrimaire);
                $values['etat'] = "enprojet";
                $values['archive'] = false;
                $values['dossier_instruction'] = $this->valF[$this->clePrimaire];

                // Ajoute l'unité d'accessibilité et vérifie si cela a échoué
                $add = $inst_etablissement_unite->ajouter($values, $this->f->db, false);
                if ($add == false) {
                    // Message d'erreur
                    $this->correct = false;
                    $this->addToMessage(sprintf(_("L'unite d'accessibilite n'a pas ete ajoutee a l'etablissement %s."), $values["libelle"], $inst_etablissement->getVal('libelle')));

                    // Stop le traitement
                    return false;
                }
            }
        }

        //
        return true;
    }

    /**
     * TRIGGER - triggermodifierapres.
     *
     * - Si la modification porte sur un champ lié à la complétude et que nous 
     *   sommes dans le contexte, envoi des échanges 210, 204 et 205.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        // Si la modification porte sur un champ lié à la complétude et que nous 
        // sommes dans le contexte, envoi des échanges 210, 204 et 205.
        if ($this->getVal('incompletude') != $this->valF['incompletude']
            || $this->getVal('piece_attendue') != $this->valF['piece_attendue']) {
            //
            $valF = array();
            if ($this->valF['incompletude'] == 1 
                || $this->valF['incompletude'] == "t" 
                || $this->valF['incompletude'] == "Oui") {
                $valF['incompletude'] = true;
            } else {
                $valF['incompletude'] = false;
            }
            $valF['piece_attendue'] = $this->valF['piece_attendue'];
            //
            $ret = $this->handle_referentiel_ads_notification_completude($valF);
            if ($ret !== true) {
                return $this->end_treatment(__METHOD__, false);
            }
        }
        // Si rien ne s'est mal passé c'est que tout s'est bien passé
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Retourne l'identifiant d'un DI depuis un code de DI ou de DC.
     * 
     * Cette méthode permet de rechercher un dossier d'instruction 
     * depuis son libellé ou depuis le libellé du dossier de coordination
     * auquel il est lié et ce en fonction de l'identifiant du service
     * passé en paramètre. 
     *
     * @param mixed $val Tableau de paramètres. Les paramètres nécessaires sont : 
     *                    - "service" => (int) // Identifiant du service,
     *                    - "dossier" => (varchar) // Libellé du DI ou DC.
     *
     * @return integer|null Identifiant du DI ou 'null' si aucune correspondance.
     */
    function get_id_for_di_or_dc_code($val = array()) {
        // Vérification des paramètres
        if (!isset($val["service"]) || $val["service"] == ""
            || !isset($val["dossier"]) || $val["dossier"] == "") {
            //
            return null;
        }
        // Composition de la requête
        $sql = "
        SELECT dossier_instruction
        FROM ".DB_PREFIXE."dossier_instruction
            LEFT JOIN ".DB_PREFIXE."dossier_coordination
                ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
        WHERE 
            (dossier_instruction.libelle='".$val["dossier"]."'
            OR dossier_coordination.libelle='".$val["dossier"]."')
            AND dossier_instruction.service=".$val["service"]."
        ";
        // Exécution de la requête
        $id = $this->f->db->getOne($sql);
        // Logger
        $this->addToLog(
            "get_id_for_di_or_dc_code(): db->getone(\"".$sql."\")", 
            VERBOSE_MODE
        ); 
        // Gestion d'une éventuelle erreur de base de données
        $this->f->isDatabaseError($id);
        // Retour de l'identifiant
        return $id;
    }

    /**
     * Permet de renommer tous les dossiers d'instruction avec le libellé correct.
     *
     * @return void
     */
    function view_rename_all_libelle() {
        // On récupère la liste des ID de DI
        $sql = "
        SELECT
            dossier_instruction.dossier_instruction,
            dossier_instruction.libelle as di_lib,
            dossier_coordination.libelle as dc_lib,
            service.code as service_code
        FROM
            ".DB_PREFIXE."dossier_instruction 
                LEFT JOIN ".DB_PREFIXE."dossier_coordination
                    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
                LEFT JOIN ".DB_PREFIXE."service
                    ON dossier_instruction.service=service.service
        ";
        $res = $this->f->db->query($sql);
        $this->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($res);
        $di_all = array();
        while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
            $di_all[] = $row;
            echo "
UPDATE dossier_instruction SET libelle='".$row["di_lib"]."' WHERE dossier_instruction=".$row["dossier_instruction"].";\n
";
        }
        //
        $counter = 0;
        //
        foreach ($di_all as $key => $value) {
            $dc_libelle = $value["dc_lib"];
            $old_libelle = $value["di_lib"];
            $new_libelle = $dc_libelle."-".$value["service_code"];
            //
            $this->addToLog(__METHOD__."(): ".$old_libelle." / ".$new_libelle, VERBOSE_MODE);
            //
            if ($old_libelle != $new_libelle) {
                $valF = array("libelle" => $new_libelle, );
                $res = $this->f->db->autoExecute(
                    DB_PREFIXE.$this->table, 
                    $valF, DB_AUTOQUERY_UPDATE, 
                    $this->getCle($value["dossier_instruction"])
                );
                $this->addToLog(
                    __METHOD__."(): db->autoExecute(\"".DB_PREFIXE.$this->table."\", ".print_r($valF, true).", DB_AUTOQUERY_UPDATE, \"".$this->getCle($value["dossier_instruction"])."\")", 
                    VERBOSE_MODE
                );
                $this->f->isDatabaseError($res);
                $counter++;
            }
        }
        //
        $this->addToLog(__METHOD__."(): ".$counter." / ".count($di_all), VERBOSE_MODE);
    }

    /**
     * Récupère l'établissement depuis le dossier de coordination.
     *
     * @return integer Identifiant de l'établissement
     */
    function get_dossier_coordination_etablissement() {
        // Initialisation de la variable de retour
        $return = "";
        //
        $id = $this->getVal('dossier_coordination');
        //
        if (!empty($id)) {
            // Instance de la classe reunion
            require_once '../obj/dossier_coordination.class.php';
            $dossier_coordination = new dossier_coordination($id);
            // Récupère la valeur du champ
            $return = $dossier_coordination->getVal('etablissement');
        }
        //
        return $return;
    }

    /**
     * VIEW - analyses.
     *
     * Vue de l'analyse du dossier d'instruction.
     *
     * Cette vue permet de gérer le contenu de l'onglet "Analyse" sur un 
     * dossier d'instruction. Cette vue spécifique est nécessaire car il y 
     * a un lien 1 - 1 entre le dossier d'instruction et l'analyse et 
     * l'ergonomie standard du framework ne prend pas en charge ce cas.
     * C'est ici l'élément lié au dossier d'instruction en visualisation 
     * qui est affiché directement au clic de l'onglet sans passer par le 
     * standard listing d'éléments liés.
     * 
     * L'idée est donc de simuler l'ergonomie standard en créant un container 
     * et d'appeler la méthode javascript 'ajaxit' pour charger le contenu 
     * de la vue visualisation de l'objet lié.
     * 
     * @return void
     */
    function view_analyses() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Objet à charger
        $obj = "analyses";
        // Construction de l'url de sousformulaire à appeler
        $url = "../scr/sousform.php?obj=".$obj;
        $url .= "&idx=".$this->get_id_analyse();
        $url .= "&action=3";
        $url .= "&retourformulaire=".$this->getParameter("objsf");
        $url .= "&idxformulaire=".$this->getParameter("idx");
        $url .= "&retour=form";
        // Affichage du container permettant le reffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * VIEW - proces_verbal.
     *
     * Vue des PV du dossier d'instruction.
     *
     * Cette vue permet de lister les PV et d'y accéder. Elle propose
     * également d'en générer et d'ajouter ceux provenant de la SCD.
     * 
     * @return void
     */
    function view_proces_verbal() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Objet à charger
        $obj = "proces_verbal";
        // Construction de l'url de l'action à appeler
        $url = "../scr/sousform.php?obj=".$obj;
        $url .= "&idx=0";
        $url .= "&action=5";
        $url .= "&retourformulaire=dossier_instruction";
        $url .= "&idxformulaire=".$this->getParameter("idx");
        $url .= "&retour=form";
        // Affichage du container permettant le raffraichissement du contenu
        // dans le cas des action-direct.
        printf('
            <div id="sousform-href" data-href="%s">
            </div>',
            $url
        );
        // Affichage du container permettant de charger le retour de la requête
        // ajax récupérant le sous formulaire.
        printf('
            <div id="sousform-%s">
            </div>
            <script>
            ajaxIt(\'%s\', \'%s\');
            </script>',
            $obj,
            $obj,
            $url
        );
    }

    /**
     * VIEW - view_batch_assignment.
     *
     * Affectation par lot. Cette vue permet d'affecter un ou plusieurs DI
     * à un INSTRUCTEUR.
     *
     * @return void 
     */
    function view_batch_assignment() {
        // Logger
        $this->addToLog(__METHOD__."() - begin", EXTRA_VERBOSE_MODE);
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        //
        //Si le formulaire a été validé
        if (isset($_POST['affectation'])){
            //On vérifie qu'un technicien a été choisit
            if (isset($_POST['technicien'])&&is_numeric($_POST['technicien'])){
                //On vérifie qu'un dossier d'instruction a été choisit
                if (isset($_POST['dossier_instruction'])&&count($_POST['dossier_instruction'])>0){
                    
                    $technicien = $_POST['technicien'];
                    $dossiers_instruction = $_POST['dossier_instruction'];
                    //On met à jour les dossiers d'instruction
                    foreach ($dossiers_instruction as $dossier_instruction){
                        //
                        require_once "../obj/dossier_instruction.class.php";
                        // Création d'un nouvel objet de type dossier_instruction
                        $di = new dossier_instruction($dossier_instruction);
                        $di->setParameter("maj", 1);
                        // On n'utilise pas setValFfromVal car on obtient une erreur
                        // liée aux champs supplémentaires ajoutés dans valF
                        $values = array();
                        foreach($di->champs as $key => $champ) {
                            $values[$champ] = $di->val[$key];
                        }
                        $values['technicien'] = $technicien;
                        //
                        $this->f->db->autoCommit(false);
                        if($di->modifier($values) === true) {
                            $this->f->db->commit();
                            $this->correct = true;
                            $message = _("Affectation du technicien effectuée avec succés");
                        } else {
                            $di->undoValidation();
                            $this->correct = false;
                            $message = _("Une erreur s'est produite lors de l'affectation ".
                                    "du techicien. Veuillez contacter votre adminsitrateur");
                        }
                    }
                }
                else {
                    $this->correct = false;
                    $message = _("Veuillez choisir un dossier d'instruction");
                }
            }
            else {
                $this->correct = false;
                $message = _("Veuillez choisir un technicien");
            }
            $this->addToMessage($message);
        }

        /**
         * MESSAGE
         */
        // Affichage du message
        $this->message();
        $this->retour();

        /**
         * RECUPERATION DES INSTRUCTEURS
         */
        // On récupère les techniciens
        // QUERY liste des acteurs qui peuvent instruire un dossier
        $sql = "
            SELECT
                acteur.acteur as acteur,
                acteur.nom_prenom as nom_prenom,
                service.code as code
            FROM ".DB_PREFIXE."acteur
                LEFT JOIN ".DB_PREFIXE."service
                    ON acteur.service = service.service
            WHERE
                (acteur.role = 'technicien'
                OR acteur.role='cadre')
                AND ((acteur.om_validite_debut IS NULL 
                      AND (acteur.om_validite_fin IS NULL 
                           OR acteur.om_validite_fin > CURRENT_DATE)) 
                     OR (acteur.om_validite_debut <= CURRENT_DATE
                         AND (acteur.om_validite_fin IS NULL 
                              OR acteur.om_validite_fin > CURRENT_DATE)))
        ";
        //
        if (!is_null($_SESSION["service"])) {
            //
            $sql .= "
                AND acteur.service=".$_SESSION["service"]."
            ";
        }
        //
        $sql .= "
            ORDER BY 
                acteur.nom_prenom
        ";
        //
        $techniciens = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($techniciens);

        /**
         * RECUPERATION DES DOSSIERS D'INSTRUCTION A AFFECTER
         */
        //On récupère les dossiers d'instruction qui n'ont pas de technicien affecté
        $sql = "
            SELECT 
                dossier_instruction.dossier_instruction as dossier_instruction, 
                dossier_instruction.libelle as libelle,
                trim(concat(etablissement.code, ' - ',etablissement.libelle)) as etablissement,
                to_char(dossier_coordination.date_demande,'DD/MM/YYYY') as date_demande,
                dossier_instruction.description
            FROM 
                ".DB_PREFIXE."dossier_instruction
                    LEFT JOIN ".DB_PREFIXE."dossier_coordination
                        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
                    LEFT JOIN ".DB_PREFIXE."etablissement
                        ON dossier_coordination.etablissement = etablissement.etablissement
            WHERE 
                dossier_instruction.technicien IS NULL 
        ";
        //
        if (!is_null($_SESSION["service"])) {
            //
            $sql .= "
                AND dossier_instruction.service=".$_SESSION["service"]."
            ";
        }
        //
        $sql .= "
            ORDER BY 
                dossier_instruction.dossier_instruction 
        ";
        //
        $dossiers_instruction = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($dossiers_instruction);

        /**
         * CONSTRUCTION DU FORMULAIRE
         */
        //
        echo '<div class="row">';
        //
        echo "\n<!-- ########## START DBFORM ########## -->\n";
        echo "<form";
        echo " method=\"post\"";
        echo " id=\"affectation_lot\"";
        echo " action=\"";
        echo "../scr/form.php?obj=dossier_instruction_a_affecter&idx=0&action=8";
        echo "\"";
        echo ">\n";
        //Inclusion de la classe de gestion des formulaires
        require_once "../obj/om_formulaire.class.php";
        // Paramétrage des champs du formulaire
        $champs = array("technicien", );
        // Création d'un nouvel objet de type formulaire
        $form = new formulaire(NULL, 0, 0, $champs);
        //Paramétrage des champs
        $form->setLib("technicien", _("technicien"));
        $form->setType("technicien", "select");
        $form->setRequired("technicien");
        // Valeurs des champs
        if (isset($_POST['affectation']) 
            && isset($_POST["technicien"])
            && $this->correct != true) {
            $form->setVal("technicien", $_POST["technicien"]);   
        }
        // Données du select technicien
        $contenu = array(
            0 => array("", ),
            1 => array(_("choisir technicien")),
        );
        while ($technicien =& $techniciens->fetchRow(DB_FETCHMODE_ASSOC)) {
            $contenu[0][] = $technicien['acteur'];
            $contenu[1][] = $technicien['nom_prenom'].(is_null($_SESSION["service"]) ? ' - '.$technicien['code'] : '');
        }
        $form->setSelect("technicien", $contenu);
        // Affichage du formulaire
        $form->entete();
        $form->afficher($champs, 0, false, false);
        //Affichage du tableau
        echo '<table class="affectation-par-lot table table-condensed table-bordered table-striped table-hover">';
        // Entête de tableau
        $template_tab_header = '
        <thead>
            <tr class="ui-tabs-nav ui-accordion ui-state-default tab-title">
                <th class="title col-0"><input type="checkbox" id="dir-checkall" /></th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
                <th class="title">
                    <span class="name">%s</span>
                </th>
            </tr>
        </thead>
        ';
        //
        printf(
            $template_tab_header,
            _('di'),
            _('etablissement'),
            _('demande'),
            _('description')
        );
        //
        echo '<tbody>';
        // Affiche des données résultats
        if ($dossiers_instruction->numrows() > 0 ) {
            //
            $dossier_instruction_checked = array();
            if (isset($_POST['affectation']) && isset($_POST["dossier_instruction"])
                && is_array($_POST["dossier_instruction"])) {
                $dossier_instruction_checked = $_POST["dossier_instruction"];
            }
            //Ajout des données au tableau
            while ($value =& $dossiers_instruction->fetchRow(DB_FETCHMODE_ASSOC)){
                //
                echo '<tr class="tab-data odd">';
                // Checkbox
                echo '<td class="icons">';
                echo '<input type="checkbox" class="dir-checkbox"';
                echo ' name="dossier_instruction[]"';
                echo ' value="'.$value['dossier_instruction'].'"';
                if (in_array($value['dossier_instruction'], $dossier_instruction_checked)) {
                    //
                    echo ' checked="checked"';
                }
                echo ' />';
                echo '</td>';
                // 
                echo '<td class="col-1">';
                echo $value['libelle'];
                echo '</td>';
                // 
                echo '<td class="col-2">';
                echo $value['etablissement'];
                echo '</td>';
                // 
                echo '<td class="col-3">';
                echo $value['date_demande'];
                echo '</td>';
                // 
                echo '<td class="col-4">';
                echo $value['description'];
                echo '</td>';
                echo "</tr>";
            }
        } else {
            //
            printf(
                '<tr><td colspan="5">%s</td></tr>',
                _("Aucun dossier d'instruction à affecter.")
            );
        }
        echo '</tbody>';
        echo '</table>';
        //
        $form->enpied();
        //
        echo "\t<div class=\"formControls\">\n";
        $this->f->layout->display_form_button(array("value" => _("Affecter"), "name" => "affectation"));
        $this->retour();
        echo "\t</div>\n";
        //
        echo "</form>\n";
        echo '</div>';

        // Logger
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
    }

    /**
     * Récupère l'ID de l'analyse du DI instancié
     *
     * @return [mixed] ID de l'analyse ou false si erreur bdd
     */
    function get_id_analyse() {
        // On récupère l'id de l'enregistrement du dossier d'instruction
        $di_idx = $this->getVal($this->clePrimaire);
        // Si le dossier d'instruction instancié ne concerne pas
        // un enregistrement en base de données
        if (empty($di_idx)) {
            // On retourne 0 en tant que id de l'analyse liée
            return 0;
        }
        //
        $sql = "SELECT analyses
            FROM ".DB_PREFIXE."analyses
            WHERE dossier_instruction = ".$di_idx;
        $id = $this->f->db->getOne($sql);
        // Si la récupération de l'ID du DC de l'analyse échoue
        if ($this->f->isDatabaseError($id, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($id->getDebugInfo(), $id->getMessage(), '');
            $this->correct = false;
            return false;
        }
        return $id;
    }

    /**
     * Récupère la date de visite d'une visite.
     *
     * @param integer $visite_id Identifiant de la visite
     * @param integer $id        Identifiant du DI
     *
     * @return string
     */
    function get_date_visite($visite_id, $id=null) {
        // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_instruction = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_instruction = new dossier_instruction($id);
        }

        // Initialise la date de visite
        $date_visite = "";

        // S'il y a une visite
        if (!empty($visite_id)) {

            // Instance de la classe visite
            require_once '../obj/visite.class.php';
            $visite = new visite($visite_id);

            // Récupère la date de visite
            $date_visite = $visite->getVal('date_visite');
        }

        // Retourne la date de visite
        return $date_visite;
    }

    /**
     * Récupère l'identifiant de la dernière visite.
     *
     * @param integer $id Identifiant du DI
     *
     * @return integer
     */
    function get_last_visite($id=null) {
         // Si l'identifiant n'est pas passé
        if ($id == null) {
            // Utilise l'objet en cours
            $dossier_instruction = $this;
        } else {
            // Sinon instancie l'établissement
            $dossier_instruction = new dossier_instruction($id);
        }
        $id = $dossier_instruction->getVal($dossier_instruction->clePrimaire);

        // Requête SQL
        $sql = "SELECT MAX(visite)
                FROM ".DB_PREFIXE."visite
                WHERE dossier_instruction = ".intval($id);
        $visite = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($visite);

        // Retourne la dernière visite
        return $visite;
    }

    /**
     * Cette méthode permet de récupérer la liste de toutes les visites
     * rattachées au dossier d'instruction sur lequel on se trouve.
     *
     * @return array
     */
    function get_visites() {
        // On récupère l'id de l'enregistrement du dossier d'instruction
        $di_idx = $this->getVal($this->clePrimaire);
        // Si le dossier d'instruction instancié ne concerne pas
        // un enregistrement en base de données
        if (empty($di_idx)) {
            // On retourne un tableau vide en tant que liste des visites liées
            return array();
        }
        // Récupération de tous les documents présentés
        $sql = "
        SELECT
            visite.visite as visite__visite,
            visite.date_visite as visite__date_visite,
            visite.heure_debut as visite__heure_debut,
            visite.heure_fin as visite__heure_fin,
            visite_etat.code as visite_etat__code,
            visite.acteur as visite__acteur
        FROM 
            ".DB_PREFIXE."visite
                LEFT JOIN ".DB_PREFIXE."visite_etat
                    ON visite.visite_etat=visite_etat.visite_etat
        WHERE 
            visite.dossier_instruction = ".$di_idx."
        ORDER BY 
            date_visite ASC, heure_debut ASC
        ";
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            return array();
        }
        $visites = array();
        while($row=& $res->fetchRow(DB_FETCHMODE_ASSOC)){
            $visites[] = $row;
        }
        //
        return $visites;
    }

    /**
     * Affiche la liste des visites en texte.
     *
     * @return string
     */
    function display_list_visits() {
        // Récupère la liste des visites du dossier d'instruction
        $get_visites = $this->get_visites();
        // Formate la liste des visites
        $list_visits = array();
        foreach ($get_visites as $key => $visite) {
            //
            $date = $this->f->formatDate($visite['visite__date_visite'], true);
            $list_visits[] = $date." ".$visite["visite__heure_debut"];
        }
        $list_visits = implode(", ", $list_visits);

        //
        return $list_visits;
    }



    /**
     * Récupérer le statut du DI en fonction de l'état des visites.
     *
     * Lors de la suppression ou de l'annulation d'une visite, il faut modifier
     * le statut du DI pour le passer en "a_poursuivre" ou "a_programmer" en
     * fonction des visites existantes ou non sur ce DI :
     *  - si aucune visite n'existe : => "a_programmer"
     *  - s'il y a des visites et que celles-ci sont toutes dans l'état
     *    annulées : => "a_programmer"
     *  - s'il y a au moins une visite planifiée : => "a_poursuivre"
     *
     * Cela permet de gérer la priorité de planification du DI selon ses visites.
     * De plus on ne met pas à jour le statut en "programmé" puisque l'on considère
     * que la raison de création de la visite justifie la nécessité d'en
     * reprogrammer une autre après l'avoir annulée ou supprimée.
     * 
     * Attention cette méthode n'est pas valide pour calculer le statut du DI 
     * à tout moment mais seulement lors de la suppression ou de l'annulation 
     * d'une visite.
     * 
     * @return string
     */
    function get_status_when_visit_is_canceled_or_deleted($npai = false) {
        if ($npai == true) {
            return "annule";
        }
        //
        $visites = $this->get_visites();
        //
        $visites_ANN = 0;
        $visites_PLA = 0;
        //
        foreach ($visites as $key => $value) {
            //
            if ($value["visite_etat__code"] === "ANN") {
                //
                $visites_ANN++;
            } elseif ($value["visite_etat__code"] === "PLA") {
                //
                $visites_PLA++;
            }
        }
        //
        if (count($visites) == 0
            || count($visites) == $visites_ANN) {
            //
            $status = "a_programmer";
        } else {
            //
            $status = "a_poursuivre";
        }
        //
        return $status;
    }

    /**
     *
     *
     * @return string
     */
    function get_status_when_visit_is_planned($a_poursuivre) {
        //
        if ($a_poursuivre === false) {
            $status = "programme";
        } else {
            $status = "a_poursuivre";
        }
        //
        return $status;
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "dossier_instruction",
        "dossier_coordination_interface_referentiel_ads",
        "dossier_coordination_dossier_autorisation_ads",
        "dossier_coordination_dossier_instruction_ads",
        "dossier_coordination_enjeu_erp",
        "dossier_coordination_enjeu_ads",
        "exploitant",
        "etablissement_coordonnees",
        "dossier_coordination_etablissement_locaux_sommeil",
        "dossier_coordination_etablissement_categorie",
        "dossier_coordination_etablissement_type",
        "dossier_coordination_autorite_police_encours",
        "dossier_coordination_dossier_cloture",
        "dossier_coordination",
        "dossier_coordination_date_demande",
        "dossier_coordination_date_butoir",
        "dossier_coordination_date_cloture",
        "courrier_dernier_arrete",
        "technicien",
        "statut",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        //
        $analyse = $this->get_inst_analyse();
        $labels = array_merge(
            $labels,
            $analyse->get_merge_fields("labels")
        );
        //
        $acteur = $this->get_inst_acteur();
        $labels = array_merge(
            $labels,
            $acteur->get_merge_fields("labels")
        );
        //
        $labels[_("dossier_instruction")]["dossier_instruction.date_derniere_commission"] = _("date de dernier passage du dossier en commission");
        $labels[_("dossier_instruction")]["dossier_instruction.date_prochaine_commission"] = _("date de prochain passage du dossier en commission");
        $labels[_("dossier_instruction")]["dossier_instruction.liste_visites"] = _("liste des visites planifiées du dossier");
        // Retour de tous les libellés
        return $labels;
    }

     /**
     * Surcharge de la récupération des valeurs des champs de fusion
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $analyse = $this->get_inst_analyse();
        $values = array_merge(
            $values,
            $analyse->get_merge_fields("values")
        );
        //
        $acteur = $this->get_inst_acteur();
        $values = array_merge(
            $values,
            $acteur->get_merge_fields("values")
        );
        //
        $date_last_commission = $this->get_date_last_commission();
        if (DateTime::createFromFormat('Y-m-d', $date_last_commission) !== FALSE) {
            $dateFormat = new DateTime($date_last_commission);
            $date_last_commission = $dateFormat->format('d/m/Y');
        }
        $date_next_commission = $this->get_date_next_commission();
        if (DateTime::createFromFormat('Y-m-d', $date_next_commission) !== FALSE) {
            $dateFormat = new DateTime($date_next_commission);
            $date_next_commission = $dateFormat->format('d/m/Y');
        }
        $visites = $this->get_visites();
        $liste_visites = "";
        foreach ($visites as $key => $value) {
            if ($value["visite_etat__code"] != 'ANN') {
                $liste_visites .= $this->f->formatDate($value["visite__date_visite"])." ".$value["visite__heure_debut"];
                if ($key != count($visites) - 1) {
                    $liste_visites .= ", ";
                }
            }
        }
        $values["dossier_instruction.date_derniere_commission"] = $date_last_commission;
        $values["dossier_instruction.date_prochaine_commission"] = $date_next_commission;
        $values["dossier_instruction.liste_visites"] = $liste_visites;
        // Le champ 'piece_attendue' est un textarea donc les sauts de ligne '\n'
        // doivent être remplacés par un saut de ligne html '<br/>' pour être
        // interprêtés dans la transformation PDF
        $values["dossier_instruction.piece_attendue"] = str_replace("\n", "<br/>", $values["dossier_instruction.piece_attendue"]);
        // Retour de tous les libellés
        return $values;
    }

    function get_date_last_commission() {
        // On récupère l'id de l'enregistrement du dossier d'instruction
        $di_idx = $this->getVal($this->clePrimaire);
        // Si le dossier d'instruction instancié ne concerne pas
        // un enregistrement en base de données
        if (empty($di_idx)) {
            // On retourne une chaîne vide en tant que date de dernière
            // commission
            return "";
        }
        //
        $sql = "
        SELECT 
            reunion.date_reunion
        FROM 
            ".DB_PREFIXE."dossier_instruction_reunion 
                left join ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                left join ".DB_PREFIXE."reunion_type
                    ON reunion.reunion_type=reunion_type.reunion_type
        WHERE
            dossier_instruction_reunion.dossier_instruction=".$di_idx."
            AND reunion.date_reunion < NOW()
            AND reunion.date_reunion IS NOT NULL
            AND reunion_type.commission IS TRUE
        ORDER BY
            reunion.date_reunion DESC 
        LIMIT 1
        ";
        $date_last_commission = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($date_last_commission);
        if ($date_last_commission == null) {
            $date_last_commission = "";
        }
        return $date_last_commission;
    }

    function get_date_next_commission() {
        // On récupère l'id de l'enregistrement du dossier d'instruction
        $di_idx = $this->getVal($this->clePrimaire);
        // Si le dossier d'instruction instancié ne concerne pas
        // un enregistrement en base de données
        if (empty($di_idx)) {
            // On retourne une chaîne vide en tant que date de prochaine
            // commission
            return "";
        }
        //
        $sql = "
        SELECT 
            reunion.date_reunion
        FROM 
            ".DB_PREFIXE."dossier_instruction_reunion 
                left join ".DB_PREFIXE."reunion
                    ON dossier_instruction_reunion.reunion=reunion.reunion
                left join ".DB_PREFIXE."reunion_type
                    ON reunion.reunion_type=reunion_type.reunion_type
        WHERE
            dossier_instruction_reunion.dossier_instruction=".$di_idx."
            AND reunion.date_reunion >= NOW()
            AND reunion.date_reunion IS NOT NULL
            AND reunion_type.commission IS TRUE
        ORDER BY
            reunion.date_reunion ASC 
        LIMIT 1
        ";
        $date_next_commission = $this->f->db->getOne($sql);
        $this->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
        $this->f->isDatabaseError($date_next_commission);
        if ($date_next_commission == null) {
            $date_next_commission = "";
        }
        return $date_next_commission;
    }

    /**
     * TREATMENT - a_poursuivre.
     * 
     * Permet de modifier le statut du DI en "à poursuivre"
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function a_poursuivre($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Modification du statut du DI
        $valF["statut"] = $this->get_status_when_visit_is_planned(true);
        // Mise à jour effective du DI
        $ret = $this->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("Le statut du dossier d'instruction")." <strong>".$this->getVal('libelle')."</strong> "._('a ete modifie en "a poursuivre".'));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - can_di_status_be_a_poursuivre.
     *
     * Permet d'indiquer si on peut passer le statut du DI à "à poursuivre".
     *
     * @return boolean
     */
    function can_di_status_be_a_poursuivre() {
        //
        $status = $this->getVal("statut");
        //
        if ( $this->is_not_closed()
            && ($status == "a_programmer" || $status == "programme")
            && $this->get_status_when_visit_is_canceled_or_deleted() == "a_poursuivre") {
            return true;
        }
        //
        return false;
    }

    /**
     * TREATMENT - a_programmer.
     * 
     * Permet de modifier le statut du DI en "à poursuivre"
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function a_programmer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Modification du statut du DI
        $valF["statut"] = $this->get_status_when_visit_is_planned(true);
        // Mise à jour effective du DI
        $ret = $this->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("Le statut du dossier d'instruction")." <strong>".$this->getVal('libelle')."</strong> "._('a ete modifie en "a programmer".'));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - can_di_status_be_a_programmer.
     *
     * Permet d'indiquer si on peut passer le statut du DI à "à programmer".
     *
     * @return boolean
     */
    function can_di_status_be_a_programmer() {
        //
        $status = $this->getVal("statut");
        //
        if ( $this->is_not_closed()
            && $status == "programme"
            && $this->get_status_when_visit_is_canceled_or_deleted() == "a_programmer") {
            return true;
        }
        //
        return false;
    }

    /**
     * TREATMENT - programmer.
     * 
     * Permet de modifier le statut du DI en "programmé"
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function programmer($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Modification du statut du DI
        $valF["statut"] = $this->get_status_when_visit_is_planned(false);
        // Mise à jour effective du DI
        $ret = $this->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("Le statut du dossier d'instruction")." <strong>".$this->getVal('libelle')."</strong> "._('a ete modifie en "programme".'));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * CONDITION - can_di_status_be_programme.
     *
     * Permet d'indiquer si on peut passer le statut du DI à "programmé".
     *
     * @return boolean
     */
    function can_di_status_be_programme() {
        //
        $status = $this->getVal("statut");
        //
        if ( $this->is_not_closed()
            && ($status == "a_programmer" || $status == "a_poursuivre")
            && $this->get_status_when_visit_is_canceled_or_deleted() == "a_poursuivre") {
            return true;
        }
        //
        return false;
    }

    /**
     * TREATMENT - handle_referentiel_ads_notification_completude.
     *
     * @return boolean
     */
    function handle_referentiel_ads_notification_completude($val = array()) {
        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[210] Complétude/Incomplétude d'une AT
         * Déclencheur :
         * - L’option ADS est activée
         * - Le DC lié est connecté au référentiel ADS
         * - Le DC est de type AT
         * - Le formulaire de gestion de la complétude est validé avec le champ incomplétude à true
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && substr($this->getVal("libelle"), 0, 2) === 'AT'
            && array_key_exists('incompletude', $val) === true
            && $val['incompletude'] === true) {
            //
            $infos = array(
                "dossier_coordination" => $this->getVal("dossier_coordination"),
                "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                "message" => "complet",
                "date" => date("d/m/Y"),
            );
            $ret = $this->f->execute_action_to_referentiel_ads(210, $infos);
            if ($ret !== true) {
                $this->cleanMessage();
                $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                return $this->end_treatment(__METHOD__, false);
            }
            $this->addToMessage(_("Notification (210) du référentiel ADS OK."));
        }
        /**
         * Interface avec le référentiel ADS.
         *
         * (WS->ADS)[204] Mise à jour de complétude ERP Accessibilité
         * (WS->ADS)[205] Mise à jour de complétude ERP Sécurité
         */
        if ($this->f->is_option_referentiel_ads_enabled() === true
            && $this->is_connected_to_referentiel_ads() === true
            && array_key_exists('incompletude', $val) === true
            && array_key_exists('piece_attendue', $val) === true
            && substr($this->getVal("libelle"), 0, 2) === 'PC') {
            //
            if ($this->is_accessibility_service() === true) {
                $infos = array(
                    "dossier_coordination" => $this->getVal("dossier_coordination"),
                    "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                    "Complétude ERP ACC" => ($val['incompletude'] === true ? "non" : "oui"),
                    "Motivation Complétude ERP ACC" => ($val['piece_attendue'] === "" ? "-" : $val['piece_attendue']),
                );
                $ret = $this->f->execute_action_to_referentiel_ads(204, $infos);
                if ($ret !== true) {
                    $this->cleanMessage();
                    $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                    return $this->end_treatment(__METHOD__, false);
                }
                $this->addToMessage(_("Notification (204) du référentiel ADS OK."));
            }
            if ($this->is_accessibility_service() === false) {
                $infos = array(
                    "dossier_coordination" => $this->getVal("dossier_coordination"),
                    "dossier_instruction" => $this->get_inst_dossier_coordination()->getVal("dossier_instruction_ads"),
                    "Complétude ERP SECU" => ($val['incompletude'] === true ? "non" : "oui"),
                    "Motivation Complétude ERP SECU" => ($val['piece_attendue'] === "" ? "-" : $val['piece_attendue']),
                );
                $ret = $this->f->execute_action_to_referentiel_ads(205, $infos);
                if ($ret !== true) {
                    $this->cleanMessage();
                    $this->addToMessage(_("Une erreur s'est produite lors de la notification du référentiel ADS. Contactez votre administrateur."));
                    return $this->end_treatment(__METHOD__, false);
                }
                $this->addToMessage(_("Notification (205) du référentiel ADS OK."));
            }
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * TREATMENT - gerer_completude.
     * 
     * Permet de modifier les informations liées à la complétude sur le dossier.
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function gerer_completude($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Modification du statut du DI
        if ($val['incompletude'] == 1 
            || $val['incompletude'] == "t" 
            || $val['incompletude'] == "Oui") {
            $valF['incompletude'] = true;
        } else {
            $valF['incompletude'] = false;
        }
        $valF['piece_attendue'] = $val['piece_attendue'];
        // Mise à jour effective du DI
        $ret = $this->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return $this->end_treatment(__METHOD__, false);
        }

        //
        $ret = $this->handle_referentiel_ads_notification_completude($valF);
        if ($ret !== true) {
            return $this->end_treatment(__METHOD__, false);
        }

        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Retourne vrai si c'est un dossier d'instruction du service accessibilité.
     *
     * @param integer $dossier_instruction_id Identifiant du DI
     *
     * @return boolean
     */
    function is_accessibility_service($dossier_instruction_id = null) {
        // Si l'identifiant du dossier d'instruction n'est pas renseigné
        if ($dossier_instruction_id == null) {
            // Récupère le dossier d'instruction
            $dossier_instruction_id = $this->getVal($this->clePrimaire);
        }

        // Requête SQL
        $sql = "SELECT CASE WHEN LOWER(service.code) = LOWER('ACC')
                    THEN true
                    ELSE false
                END as res
                FROM ".DB_PREFIXE."service
                LEFT JOIN ".DB_PREFIXE."dossier_instruction ON dossier_instruction.service = service.service
                WHERE dossier_instruction.dossier_instruction = ".$dossier_instruction_id;
        // Boolean 
        $res = $this->f->db->getOne($sql);
        $this->f->addToLog(__METHOD__."() db->getOne(\"".$sql."\");",
            VERBOSE_MODE);
        $this->f->isDatabaseError($res);

        // Si c'est vrai
        if ($res === 't') {
            //
            return true;
        }

        //
        return false;
    }

    /**
     * VIEW - view_localiser
     * Redirige l'utilisateur vers le SIG externe, avec la vue centrée sur le dossier
     * de coordination lié.
     *
     * @return void
     */
    public function view_localiser() {
        // Récupération de l'instance du dossier de coordination lié
        $inst_dossier_coordination = $this->get_inst_dossier_coordination();
        // On définit la valeur du paramètre maj à 30, afin que l'action view_localiser 
        // (30) de la classe dossier_coordination soit accessible
        $inst_dossier_coordination->setParameter('maj', '30');
        $inst_dossier_coordination->view_localiser();

    }

    /**
     * CONDITION - is_user_di_instructor.
     *
     * Indique si l'utilisateur connecté est l'instructeur du dossier,
     * c'est-à-dire l'utilisateur lié à l'acteur référencé dans le champ
     * 'technicien' du dossier.
     *
     * @return boolean
     */
    function is_user_di_instructor() {
        $inst_acteur = $this->get_inst_acteur();
        $inst_om_utilisateur = $inst_acteur->get_inst_om_utilisateur();
        if ($_SESSION["login"] == $inst_om_utilisateur->getVal("login")) {
            return true;
        }
        return false;
    }

}

?>
