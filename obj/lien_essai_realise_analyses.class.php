<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/lien_essai_realise_analyses.class.php";

class lien_essai_realise_analyses extends lien_essai_realise_analyses_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Retourne un tableau d'essais réalisés pour une analyse donnée
     * ou une chaîne 'Aucun' si aucun résultat.
     * 
     * @param  [integer] $idx Valeur de la clé primaire de l'analyse
     * @return [string]       Code HTML du tableau
     */
    function get_essai_realise_from_analyse($idx) {

        /**
         * Template
         */
        //
        $template_table = '
        <table style="%s">
            <thead>
                %s
            </thead>
            <tbody>
                %s
            </tbody>
        </table>
        ';
        //
        $template_head_line = '
        <tr nobr="true">
            <th style="width:40%%;%1$s">%2$s</th>
            <th style="width:40%%;%1$s">%3$s</th>
            <th style="width:20%%;%1$s">%4$s</th>
        </tr>
        ';
        //
        $template_body_line = '
        <tr>
            <td style="width:40%%;%1$s">%2$s</td>
            <td style="width:40%%;%1$s">%3$s</td>
            <td style="width:20%%;%1$s">%4$s</td>
        </tr>
        ';
        // Style CSS
        $css_center = "text-align:center;";
        $css_border = "border: 0.5px solid #000;";
        $css_bg_head = "background-color: #D0D0D0;";
        $css_bg_line_odd = "background-color: #F9F9F9;";
        $css_bg_line_even = "";

        /**
         * Récupération des essais
         */
        // Requête
        $sql = "
        SELECT
            CASE lien.concluant
                WHEN 't'
                THEN 'Concluant'
                ELSE 'Non Concluant'
            END as resultat,
            essai_realise.description as description,
            lien.complement as complement
        FROM 
            ".DB_PREFIXE."lien_essai_realise_analyses as lien
                LEFT JOIN ".DB_PREFIXE."essai_realise
                    ON essai_realise.essai_realise = lien.essai_realise
        WHERE 
            lien.analyses = ".intval($idx)."
        ORDER BY 
            lien.concluant ASC,
            essai_realise.description ASC
        ";
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        // Traitement des erreurs de base de données
        if ($this->f->isDatabaseError($res, true)) {
            return "";
        }

        /**
         * S'il n'y a aucun résultat
         */
        //
        if ($res->numRows() == 0) {
            return _("Aucun");
        }

        /**
         * S'il y a au moins un résultat
         */
        //
        $table_head_content = sprintf(
            $template_head_line,
            $css_center.$css_border.$css_bg_head,
            _('Description'),
            _('Complement'),
            _('Résultat')
        );
        //
        $i = 0;
        $table_body_content = "";
        while ($row =& $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            //
            if ($i % 2 == 0) {
                $css_bg_line = $css_bg_line_even;
            } else {
                $css_bg_line = $css_bg_line_odd;
            }
            //
            $table_body_content .= sprintf(
                $template_body_line,
                $css_border.$css_bg_line,
                $row["description"],
                $row["complement"],
                $row["resultat"]
            );
            //
            $i++;
        }
        //
        return sprintf(
            $template_table,
            $css_border,
            $table_head_content,
            $table_body_content
        );
    }
}

?>
