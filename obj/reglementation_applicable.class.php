<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../gen/obj/reglementation_applicable.class.php";

class reglementation_applicable extends reglementation_applicable_gen {

    /**
     * Liaison NaN
     */
    var $liaisons_nan = array(
        "etablissement_categorie" => array(
            "table_l" => "lien_reglementation_applicable_etablissement_categorie",
            "table_f" => "etablissement_categorie",
            "field" => "etablissement_categorie"
        ),
        "etablissement_type" => array(
            "table_l" => "lien_reglementation_applicable_etablissement_type",
            "table_f" => "etablissement_type",
            "field" => "etablissement_type"
        )
    );

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        //
        if ($maj==0){ //ajout
            $form->setType('etablissement_categorie', 'select_multiple');
            $form->setType('etablissement_type', 'select_multiple');
        }// fin ajout
        if ($maj==1){ //modifier
            $form->setType('etablissement_categorie', 'select_multiple');
            $form->setType('etablissement_type', 'select_multiple');
        }// fin modifier
        if ($maj==2){ //supprimer
            $form->setType('etablissement_categorie', 'select_multiple_static');
            $form->setType('etablissement_type', 'select_multiple_static');
        }//fin supprimer
        if ($maj==3){ //consulter
            $form->setType('etablissement_categorie', 'select_multiple_static');
            $form->setType('etablissement_type', 'select_multiple_static');
        }//fin consulter
    }

    /**
     *
     */
    function setLib(&$form, $maj) {
        //
        parent::setLib($form, $this->getParameter("maj"));
        // Liaison NaN
        $form->setLib("etablissement_categorie", _("categorie(s) d'etablissement"));
        $form->setLib("etablissement_type", _("type(s) d'etablissement"));
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "etablissement_categorie", $sql_etablissement_categorie, $sql_etablissement_categorie_by_id, false, true);
        //
        $this->init_select($form, $dnu1, $maj, $dnu2, "etablissement_type", $sql_etablissement_type, $sql_etablissement_type_by_id, false, true);
    }

    /**
     *
     */
    function triggerajouterapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Ajout des liaisons table Nan
            $nb_liens = $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
        }
    }

    /**
     *
     * @return bool
     */
    function triggermodifierapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
            // Ajout des liaisons table Nan
            $this->ajouter_liaisons_table_nan(
                $liaison_nan["table_l"], 
                $liaison_nan["table_f"], 
                $liaison_nan["field"]
            );
        }

    }

    /**
     *
     */
    function triggersupprimer($id, &$dnu1 = null, $val = array(), $dnu2 = null) {

        // Liaisons NaN
        foreach ($this->liaisons_nan as $liaison_nan) {
            // Suppression des liaisons table NaN
            $this->supprimer_liaisons_table_nan($liaison_nan["table_l"]);
        }

    }

    /**
     *
     */
    function ajouter_liaisons_table_nan($table_l, $table_f, $field) {
        // Récupération des données du select multiple
        $postvar = $this->getParameter("postvar");
        if (isset($postvar[$field])
            && is_array($postvar[$field])) {
            $multiple_values = $postvar[$field];
        } else {
            $multiple_values = array();
        }
        // Ajout des liaisons
        $nb_liens = 0;
        // Boucle sur la liste des valeurs sélectionnées
        foreach ($multiple_values as $value) {
            // Test si la valeur par défaut est sélectionnée
            if ($value == "") {
                continue;
            }
            // On compose les données de l'enregistrement
            $donnees = array(
                $this->clePrimaire => $this->valF[$this->clePrimaire],
                $table_f => $value,
                $table_l => ""
            );
            // On ajoute l'enregistrement
            require_once "../obj/".$table_l.".class.php";
            $obj_l = new $table_l("]");
            $obj_l->ajouter($donnees);
            // On compte le nombre d'éléments ajoutés
            $nb_liens++;
        }
        //
        return $nb_liens;
    }

    /**
     *
     */
    function supprimer_liaisons_table_nan($table) {
        // Suppression de tous les enregistrements correspondants à l'id 
        // de l'objet instancié en cours dans la table NaN
        $sql = "DELETE FROM ".DB_PREFIXE.$table." WHERE ".$this->clePrimaire."=".$this->getVal($this->clePrimaire);
        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if (database::isError($res)) {
            die();
        }
    }

    /**
     * Surcharge de la méthode rechercheTable pour éviter de court-circuiter le générateur
     * en devant surcharger la méthode cleSecondaire afin de supprimer les éléments liés dans
     * les tables NaN.
     */
    function rechercheTable(&$dnu1 = null, $table, $field, $id, $dnu2 = null, $selection = "") {
        //
        if (in_array($table, array_keys($this->liaisons_nan))) {
            //
            $this->addToLog(__METHOD__."(): On ne vérifie pas la table ".$table." car liaison nan.", EXTRA_VERBOSE_MODE);
            return;
        }
        //
        parent::rechercheTable($this->f->db, $table, $field, $id, null, $selection);
    }
}

?>
