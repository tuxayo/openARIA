<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once ("../gen/obj/etablissement_etat.class.php");

class etablissement_etat extends etablissement_etat_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Récupère l'identifiant de l'état de l'établissement par rapport
     * au code.
     *
     * @param string $code Code de l'état de l'établissement
     *
     * @return integer     Identifiant de l'état de l'établissement
     */
    function get_etablissement_etat_by_code($code) {
        // Initialisation de la variable de résultat
        $etablissement_etat = "";

        // Si le code n'est pas vide
        if (!empty($code)) {

            // Requête SQL
            $sql = "SELECT etablissement_etat
                    FROM ".DB_PREFIXE."etablissement_etat
                    WHERE LOWER(code) = LOWER('".$this->db->escapesimple($code)."')";
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $etablissement_etat = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($etablissement_etat);
        }

        // Retourne le résultat
        return $etablissement_etat;
    }

    /**
     * Récupère le code de l'état d'établissement
     *
     * @param integer $id Identifiant
     *
     * @return string
     */
    function get_etablissement_etat_code($id = null) {
        // Si l'identifiant est null
        if ($id == null) {
            // Récupère celui de l'instance en cours
            $id = $this->getVal($this->clePrimaire);
        }

        // Initialisation de la variable de résultat
        $code = "";

        // Si l'identifiant n'est pas vide
        if (!empty($id)) {

            // Requête SQL
            $sql = "SELECT LOWER(code)
                    FROM ".DB_PREFIXE."etablissement_etat
                    WHERE etablissement_etat = ".$id;
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $code = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($code);
        }

        // Retourne le résultat
        return $code;
    }

}// fin classe
?>
