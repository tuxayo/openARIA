<?php
//$Id$ 
//gen openMairie le 10/08/2016 17:34

require_once '../gen/obj/lien_contrainte_etablissement.class.php';

class lien_contrainte_etablissement extends lien_contrainte_etablissement_gen {


    //
    var $inst_etablissement = null;
    // liste des contraintes paramétrées avec leurs informations
    // (destinée à l'interface spécifique d'ajout)
    var $contraintes_parametrees = array();
    // prefixe du nom d'un champ de formulaire de type contrainte
    const CONTRAINTE_PREFIXE = 'contrainte_';
    //
    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    //
    // VIEWS
    //

    /**
     * VIEW - view_tab
     * 
     * Affiche les contraintes rattachées à un établissement
     * dans un tableau organisé par groupes et sous-groupes.
     *
     * @return void
     */
    function view_tab() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Récupèration de l'établissement : classe et idx
        $retourformulaire = $this->getParameter("retourformulaire");
        $etab_idx = $this->getParameter("idxformulaire");

        // Récupération des contraintes appliquées
        try {
            $contraintes_appliquees = $this->get_contraintes_appliquees($etab_idx);
        } catch (treatment_exception $e) {
            $this->addToMessage($e->getMessage());
            $this->correct = false;
            $this->display_msg();
            printf("</div></div>");
            return;
        }

        // Entête pour le groupe
        $groupeHeader = "
        <div class='lien_contrainte_etablissement_groupe'>
            <div class='lien_contrainte_etablissement_groupe_header'>
                <span class='name'>
                    %s
                </span>
            </div>
        ";

        // Entête pour le sous-groupe
        $sousgroupeHeader = "
        <div class='lien_contrainte_etablissement_sousgroupe'>
            <div class='lien_contrainte_etablissement_sousgroupe_header'>
                <span class='name'>
                    %s
                </span>
            </div>
        ";

        // Titres des colonnes
        $tableHeader = "
        <thead><tr class='ui-tabs-nav ui-accordion ui-state-default tab-title'>
            <th class='icons actions-max-%s'></th>
            <th class='title col-0 firstcol'>
                <span class='name'>"._('libelle')."</span>
            </th>
            <th class='title col-1'>
                <span class='name'>"._('texte_complete')."</span>
            </th>
            <th class='title col-2'>
                <span class='name'>"._('recupérée')."</span>
            </th>
            <th class='title col-3 lastcol'>
                <span class='name'>"._('nature')."</span>
            </th>
        </tr></thead>";

        // Ligne de données
        $line = "
        <tr class='tab-data %s'>
            <td class='icons'>%s</td>
            <td class='col-0 firstcol'>%s</td>
            <td class='col-1'>%s</td>
            <td class='col-2'>%s</td>
            <td class='col-3 lastcol'>%s</td>
        </tr>
        ";

        // Lien des données
        $link = "
        <a class='lienTable' onclick=\"ajaxIt('lien_contrainte_etablissement','../scr/sousform.php?obj=lien_contrainte_etablissement&action=3&idx=%s&tri=&premier=0&recherche=&objsf=lien_contrainte_etablissement&premiersf=0&retourformulaire=%s&idxformulaire=%s&trisf=&retour=tab');\" href='#'>
            %s
        </a>
        ";

        // Bouton ajouter
        if ($this->f->isAccredited('lien_contrainte_etablissement')
            || $this->f->isAccredited('lien_contrainte_etablissement_ajouter')) {
           echo "
            <a id=\"ajouter_contrainte\" onclick=\"ajaxIt('lien_contrainte_etablissement','../scr/sousform.php?obj=lien_contrainte_etablissement&action=8&idx=0&tri=&objsf=lien_contrainte_etablissement&premiersf=0&retourformulaire=".$retourformulaire."&idxformulaire=".$etab_idx."&trisf=&retour=tab');\" href='#'>
                <span class=\"om-prev-icon om-icon-16 add-16\" title=\""._("Ajouter des contraintes paramétrées")."\">
                    "._("Ajouter")."
                </span>
            </a>
            ";
        }
        // Bouton récupérer
        if ($this->can_recuperer() === true
            && ($this->f->isAccredited('lien_contrainte_etablissement')
            || $this->f->isAccredited('lien_contrainte_etablissement_recuperer'))) {
            echo "
            <a id=\"recuperer_contraintes\" onclick=\"ajaxIt('lien_contrainte_etablissement','../scr/sousform.php?obj=lien_contrainte_etablissement&idx=0&action=6&tri=&objsf=lien_contrainte_etablissement&premiersf=0&retourformulaire=".$retourformulaire."&idxformulaire=".$etab_idx."&trisf=&retour=tab');\" href='#'>
                <span class=\"om-prev-icon om-icon-16 recuperer_sig-16\" title=\""._("Récupérer les contraintes depuis le référentiel selon les références cadastrales de l'établissement")."\">
                    "._("Récupérer")."
                </span>
            </a>
            ";
        }

        // Gestion aucun résulat
        if (count($contraintes_appliquees) === 0) {
            printf ("<div class='noDataForm'>"._("Aucune contrainte appliquée à l'établissement.")."</div>");
            return;
        }

        // Gestion des actions tab commune à tous les résultats
        $actions_max = 1;
        $show_btn_edit = false;
        $show_btn_delete = false;
        if ($this->f->isAccredited('lien_contrainte_etablissement')
                || $this->f->isAccredited('lien_contrainte_etablissement_modifier')) {
            $show_btn_edit = true;
            $actions_max++;
        }
        if ($this->f->isAccredited('lien_contrainte_etablissement')
                || $this->f->isAccredited('lien_contrainte_etablissement_supprimer')) {
            $show_btn_delete = true;
            $actions_max++;
        }
        // Template action tab
        $button = '
        <a onclick=\'ajaxIt("lien_contrainte_etablissement","../scr/sousform.php?obj=lien_contrainte_etablissement&amp;action=%1$s&amp;idx=%2$s&amp;tri=&amp;premier=0&amp;recherche=&amp;objsf=lien_contrainte_etablissement&amp;premiersf=0&amp;retourformulaire=%3$s&amp;idxformulaire=%4$s&amp;trisf=&amp;retour=tab");\' id="action-soustab-lien_contrainte_etablissement-corner-left-%6$s-%2$s" href="#">
            <span title="%5$s" class="om-icon om-icon-16 om-icon-fix %6$s-16">
                %5$s
            </span>
        </a>
        ';

        // Sauvegarde des données pour comparer une contrainte à la précédente
        // et ainsi gérer l'affichage des groupes/sous-groupes
        $previous = array();
        $previous['contrainte_groupe'] = 'empty';
        $previous['contrainte_sousgroupe'] = 'empty';

        // Pour chaque contrainte appliquée
        foreach ($contraintes_appliquees as $id => $actual) {
            // Gestion des sans groupe/sous-groupe
            if ($actual['contrainte_groupe'] === '') {
                $actual['contrainte_groupe'] = _('Sans groupe');
            }
            if ($actual['contrainte_sousgroupe'] === '') {
                $actual['contrainte_sousgroupe'] = _('Sans sous-groupe');
            }

            // Si l'identifiant du groupe de la contrainte présente et 
            // celle d'avant est différent
            if ($actual['contrainte_groupe'] != $previous['contrainte_groupe']) {

                // Si l'identifiant du groupe d'avant est vide
                if ($previous['contrainte_groupe'] != 'empty') {
                    // Ferme le tableau
                    printf("</table>");
                    //
                    printf("</div>");
                    //
                    printf("</div>");
                }

                // Affiche le header du groupe
                printf($groupeHeader, $actual['contrainte_groupe']);
            }

            // Si l'identifiant du sous-groupe de la contrainte présente et celle d'avant est différent
            // Ou qu'ils soient identique mais n'appartiennent pas au même groupe
            if ($actual['contrainte_sousgroupe'] != $previous['contrainte_sousgroupe']
                || ($actual['contrainte_sousgroupe'] == $previous['contrainte_sousgroupe']
                    && $actual['contrainte_groupe'] != $previous['contrainte_groupe'])) {

                //
                if($actual['contrainte_groupe'] == $previous['contrainte_groupe']) {
                    // Si l'identifiant de la sous-groupe d'avant est vide
                    if ($previous['contrainte_sousgroupe'] != 'empty') {
                        // Ferme le tableau
                        printf("</table>");
                        //
                        printf("</div>");
                    }
                }

                // Affiche le header du sous-groupe
                printf($sousgroupeHeader, $actual['contrainte_sousgroupe']);

                // Ouvre le tableau
                printf("<table id='sousgroupe_".$actual['contrainte_sousgroupe']
                    ."' class='tab-tab lien_contrainte_etablissement_view'>");
                // Affiche le header des données
                printf($tableHeader, $actions_max);
                // Définis le style des lignes
                $style = 'odd';
            }

            // Inversement du style de la ligne si même sous-tableau
            if ($actual['contrainte_groupe'] == $previous['contrainte_groupe']
                && $actual['contrainte_sousgroupe'] == $previous['contrainte_sousgroupe']) {
                $style = ($style === 'even') ? 'odd' : 'even';
            }
            
            // Gestion du "Oui" / "Non" pour le bouléen
            $contrainte_reference = _("Non");
            if ($actual['lien_contrainte_etablissement_reference'] == 1
                || $actual['lien_contrainte_etablissement_reference'] == "t"
                || $actual['lien_contrainte_etablissement_reference'] == "Oui") {
                $contrainte_reference = _("Oui");
            }

            // Affichage de la ligne de résultat
            $actions = sprintf($button, '3',
                $actual['lien_contrainte_etablissement_id'], $retourformulaire,
                $etab_idx, _('Consulter'), 'consult'
            );
            if ($show_btn_edit === true) {
                $edit = sprintf($button, '1',
                    $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, _('Modifier'), 'edit'
                );
                $actions .= $edit;
            }
            if ($show_btn_delete === true) {
                $delete = sprintf($button, '2',
                    $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, _('Supprimer'), 'delete'
                );
                $actions .= $delete;
            }
            printf($line, $style, $actions,
                sprintf($link, $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, $actual['contrainte_libelle']),
                sprintf($link, $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, nl2br($actual['lien_contrainte_etablissement_texte'])), 
                sprintf($link, $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, $contrainte_reference),
                sprintf($link, $actual['lien_contrainte_etablissement_id'], $retourformulaire, 
                    $etab_idx, $actual['contrainte_nature'])
            );

            // Sauvegarde les données
            $previous['contrainte_groupe'] = $actual['contrainte_groupe'];
            $previous['contrainte_sousgroupe'] = $actual['contrainte_sousgroupe'];
            
        }
        // Ferme le tableau, sous-groupe et groupe
        printf('</table></div></div>');
    }

    /**
     * VIEW - view_appliquer
     * 
     * Interface spécifique d'ajout des contraintes paramétrées
     *
     * @return void
     */
    function view_appliquer() {
        // récupération des contraintes paramétrées actives
        $contraintes_parametrees = $this->get_contraintes_parametrees();
        // ajout de chacune d'entre elles en champ du formulaire
        foreach ($contraintes_parametrees as $id => $values) {
            // Identifiant du champ
            $id_field = self::CONTRAINTE_PREFIXE.$id;
            // Ajoute les informations de la contrainte au tableau
            $this->contraintes_parametrees[$id_field] = $values;
            // Ajoute la contrainte en nouveau champ du formulaire
            // (opération destinés au formulaire d'ajout)
            $this->champs[] = $id_field;
        }
        // construction du form
        parent::sousformulaire();
    }

    /**
     * VIEW - view_appliquer_post_treament
     * 
     * Traite les contraintes postées et affiche
     * le résultat de ce traitement en JSON.
     *
     * @return void
     */
    function view_appliquer_post_treament() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();
        // Désactivation des logs cause AJAX
        $this->f->disableLog();
        // Récupération des POST
        $postedValue = $this->f->get_submitted_post_value();
        $decodedPost = array();
        foreach ($postedValue as $key => $value) {
            $decodedPost[$key] = utf8_decode($value);
        }

        // Identifiant de l'établissement
        $etablissement = $decodedPost['etablissement'];

        // Définition des variables pour le message retourné
        $listAddContrainte = array();
        $message = "";
        // Message pour les ajouts
        $messageAdd = _("La contrainte %s a été appliquée à l'établissement.");

        // Pour chaque champ récupéré
        foreach ($decodedPost as $key => $value) {
            // Mot-clés à rechercher
            $search_field = self::CONTRAINTE_PREFIXE;
            // Si le mot-clés est présent dans l'identifiant du champ
            if (strpos($key, $search_field) !== false) {
                // Récupération de l'identifiant de la contrainte
                $id_contrainte = str_replace(self::CONTRAINTE_PREFIXE, '', $key);
                // Si la valeur du champ est 'Oui'
                if ($value === 'Oui') {
                    // Instancie la classe lien_contrainte_etablissement
                    $lien_contrainte_etablissement = new lien_contrainte_etablissement("]");
                    // Définit les valeurs
                    $val = array(
                        'lien_contrainte_etablissement' => ']',
                        'etablissement' => $etablissement,
                        'contrainte' => $id_contrainte,
                        'texte_complete' => $this->get_texte_from_contrainte_parametree($id_contrainte),
                        'recuperee' => false
                    );
                    // Ajoute l'enregistrement
                    $ajouter = $lien_contrainte_etablissement->ajouter($val, $this->f->db, DEBUG);
                    // Si la contrainte est ajouté
                    if ($ajouter == true) {
                        // Ajoute le libellé de la contrainte au tableau
                        $listAddContrainte[] = $this->getLibelleFromContrainte($id_contrainte);
                    }
                }
            }
        }

        // Pour chaque libellé sauvegardé dans les tableaux on compose un message
        foreach ($listAddContrainte as $key => $value) {
            $message .= sprintf($messageAdd, "<strong>".$value."</strong>")."<br/>";
        }

        // Si le message à retourner est vide
        if ($message == "") {
            // Message par défaut
            $message = _("Aucune contrainte appliquée.");
        }

        // Retourne le message
        echo json_encode($message);
    }

    /**
     * VIEW - view_recuperer.
     *
     * Interface de récupération des contraintes du SIG.
     *
     * @return void
     */
    public function view_recuperer() {
        // Vérification de l'accessibilité sur l'élément
        $this->checkAccessibility();

        // Début sousform
        printf("\n<div id=\"sousform-lien_contrainte_etablissement\">\n");
        printf("\n<!-- ########## START FORMULAIRE ########## -->\n");
        $this->btn_retour();

        // Traitement et affichage message si formulaire validé
        if ($this->f->get_submitted_post_value('valider_recuperation') === 'oui') {
            $this->begin_treatment_with_transaction(__METHOD__);
            //
            try {
                $this->recuperer();
                $this->end_treatment_with_transaction(__METHOD__);
            } catch (treatment_exception $e) {
                $this->addToMessage($e->getMessage());
                $this->end_treatment_with_transaction(__METHOD__, false);
            }
            //
            $this->display_msg();
        }

        // Formulaire de validation
        $on_submit = sprintf(
            "replace_sousform_formcontrols_with_spinner('lien_contrainte_etablissement');affichersform('lien_contrainte_etablissement','%s',this); return false;",
            $this->getDataSubmitSousForm()
        );
        // 
        printf('<form method="POST" action="" name="f2" onsubmit="%s">', $on_submit);
        printf('<div class="formEntete ui-corner-all">');
        $desc = _("Cette page permet de récupérer les contraintes provenant du référentiel SIG afin de les appliquer à l'établissement.");
        $desc .= '<br/><strong>'._('Attention').'</strong> : ';
        $desc .= _("en effectuant cette action vous écraserez le texte complété des contraintes déjà récupérées.");
        $this->f->displayDescription($desc);
        printf('<input
            type="hidden"
            name="valider_recuperation"
            id="valider_recuperation"
            value="oui" />');
        printf('</div><div class="formControls">');
        $this->f->layout->display_form_button(array(
            'value' => _('Récupérer'),
            'name' => 'btn_recuperer'
        ));
        $this->btn_retour();
        printf('</div></form></div></div>');
    }

    //
    // CONDITIONS
    //

    /**
     * CONDITION - can_recuperer
     * Possibilité de récupérer les contraintes du référentiel SIG ?
     *
     * @return boolean true si possible false sinon
     */
    function can_recuperer() {
        // L'action est disponible depuis le tableau
        if ($this->can_view_tab() === false) {
            return false;
        }
        // L'option SIG doit être activée
        if ($this->is_option_sig_enabled() === false) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - can_appliquer
     * Possibilité d'appliquer les contraintes paramétrées
     *
     * @return boolean true si possible false sinon
     */
    function can_appliquer() {
        // L'action est disponible depuis le tableau
        if ($this->can_view_tab() === false) {
            return false;
        }
        return true;
    }

    /**
     * CONDITION - can_view_tab
     * Possibilité de récupérer les contraintes du référentiel SIG ?
     *
     * @return boolean true si possible false sinon
     */
    function can_view_tab() {
        // On doit être en sous-formulaire d'un établissement
        if (in_array($this->getParameter('retourformulaire'),
            $this->foreign_keys_extended['etablissement']) === true) {
            return true;
        }
        return false;
    }

    /**
     * CONDITION - is_recuperee
     * Contrainte appliquée récupérée depuis un référentiel SIG ?
     *
     * @return boolean true si c'est le cas
     */
    function is_recuperee() {
        if ($this->getVal('recuperee') === 't') {
            return true;
        }
        return false;
    }

    //
    // TREATMENTS
    //

    /**
     * TREATMENT - recuperer.
     *
     * Récupère les contraintes du SIG et les applique à l'établissement.
     *
     * @throws treatment_exception
     * @return void
     */
    public function recuperer() {
        //
        $etab_idx = $this->getParameter('idxformulaire');

        // Récupère toutes les parcelles affectées à l'établissement
        $parcelles = $this->get_parcelles_by_etablissement($etab_idx);
        // S'il y en a aucune alors récupération impossible
        if (empty($parcelles) === true) {
            throw new treatment_exception(_("L'établissement n'a aucune référence cadastrale renseignée."));
        }
        // Récupère toutes les contraintes du référentiel SIG
        // applicables aux parcelles de l'établissement
        $c_SIG_list = $this->get_contraintes_sig($parcelles);

        // Récupère toutes les contraintes appliquées à l'établissement
        // et marquées comme récupérées du SIG
        $recuperees = $this->get_contraintes_recuperees($etab_idx);

        // Init compteurs
        $tot_add = 0; $tot_up = 0;
        // Pour chaque contrainte SIG récupérée
        // - si elle n'est pas paramétrée dans l'application l'user doit synchroniser
        // - si elle est paramétrée et appliquée on met à jour l'appliquée
        // - si elle est paramétrée mais non appliquée on ajoute une appliquée
        foreach ($c_SIG_list as $key => $c_SIG) {

            // Vérification de la non-nécessité de synchronisation.
            require_once '../obj/contrainte.class.php';
            $c_PARAM = new contrainte(']');
            $c_PARAM_ID = $c_PARAM->get_id_by_referentiel($c_SIG['id_referentiel']);
            if (empty($c_PARAM_ID) === true) {
                throw new treatment_exception(_("Les contraintes doivent être synchronisées."));
            }

            // Mise à jour si contrainte appliquée existante
            if (array_key_exists($c_PARAM_ID, $recuperees) === true) {
                $C_APP_ID = $recuperees[$c_PARAM_ID]['lien_contrainte_etablissement_id'];
                $this->update_contrainte_appliquee($C_APP_ID, $c_PARAM_ID, $c_SIG['texte']);
                $tot_up++;
                continue;
            }

            // Sinon ajout
            $this->add_contrainte_appliquee($etab_idx, $c_PARAM_ID, $c_SIG['texte']);
            $tot_add++;
        }

        //
        $this->addToMessage($this->contruct_msg_val($tot_add, $tot_up));
    }

    /**
     * TREATMENT - demarquer_recuperee.
     * 
     * Permet de démarquer une contrainte appliquée récupérée
     *
     * @param array $val Valeurs soumises par le formulaire.
     * @return boolean true si succès
     */
    function demarquer_recuperee($val = array()) {
        //
        $this->begin_treatment(__METHOD__);
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            array('recuperee' => 'f'),
            DB_AUTOQUERY_UPDATE,
            $this->getCle($this->getVal($this->clePrimaire))
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            $this->addToLog(__METHOD__."() : ".$res->getMessage(), VERBOSE_MODE);
            $this->addToMessage(_("Erreur de base de données.").' '._("Contactez votre administrateur."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("La contrainte n'est plus marquée comme récupérée."));
        return $this->end_treatment(__METHOD__, true);
    }

    /**
     * Applique à l'établissement une nouvelle contrainte récupérée du SIG
     *
     * @param  integer $etab
     * @param  integer $contrainte
     * @param  string  $texte
     * @throws treatment_exception
     * @return void
     */
    private function add_contrainte_appliquee($etab, $contrainte, $texte) {
        // Le texte complété est la concaténation de celui du SIG
        // avec la surcharge du paramétrage
        require_once '../obj/contrainte.class.php';
        $c_PARAM = new contrainte($contrainte);
        $surcharge = $c_PARAM->getVal('texte_surcharge');
        $texte_complete = $texte."\n".$surcharge;
        //
        $c_APP = new lien_contrainte_etablissement(']');
        //
        $val = array(
            'lien_contrainte_etablissement' => ']',
            'etablissement' => $etab,
            'contrainte' => $contrainte,
            'texte_complete' => $texte_complete,
            'recuperee' => true,
        );
        //
        if ($c_APP->ajouter($val, $this->f->db, DEBUG) === false) {
            throw new treatment_exception(_("Erreur lors de l'application des contraintes.").' '._("Contactez votre administrateur."));
        }
    }

    /**
     * Écrase le texte complété d'une contrainte appliquée récupérée du SIG
     *
     * @param  integer $c_APP     ID de la contrainte appliquée à modifier
     * @param  integer $c_PARAM   ID de sa contrainte paramétrée liée
     * @param  string  $texte_sig texte provenant du référentiel SIG
     * @throws treatment_exception
     * @return void
     */
    private function update_contrainte_appliquee($c_APP, $c_PARAM, $texte_sig) {
        // Le texte complété est la concaténation de celui du SIG
        // avec la surcharge du paramétrage
        require_once '../obj/contrainte.class.php';
        $c_PARAM = new contrainte($c_PARAM);
        $surcharge = $c_PARAM->getVal('texte_surcharge');
        $texte_complete = $texte_sig."\n".$surcharge;
        //
        $res = $this->f->db->autoExecute(
            DB_PREFIXE.$this->table,
            array('texte_complete' => $texte_complete),
            DB_AUTOQUERY_UPDATE,
            $this->getCle($c_APP)
        );
        //
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                _("Erreur de base de données.").' '._("Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }
    }

    //
    // REQUÊTES SQL
    //

    /**
     * Récupère les contraintes du paramétrage indexées par leur clé primaire.
     *
     * @throws treatment_exception
     * @return array
     */
    function get_contraintes_parametrees() {
        // Requête SQL
        $sql = "SELECT
            contrainte.contrainte as contrainte_id,
            lower(contrainte.groupe) as contrainte_groupe, 
            lower(contrainte.sousgroupe) as contrainte_sousgroupe,
            contrainte.libelle as contrainte_lib,
            contrainte.ordre_d_affichage as contrainte_ordre
        FROM ".DB_PREFIXE."contrainte
        WHERE (contrainte.om_validite_fin IS NULL
            OR contrainte.om_validite_fin > CURRENT_DATE)
        ORDER BY
            contrainte_groupe DESC nulls last,
            contrainte_sousgroupe nulls last,
            contrainte.ordre_d_affichage,
            contrainte.libelle";
        $this->f->addToLog(__METHOD__."() : db->query(\"".$sql."\")", 
            VERBOSE_MODE);
        $res = $this->db->query($sql);
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                _("Erreur de base de données.").' '._("Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }
        $contraintes = array();
        while ($contrainte = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $contraintes[$contrainte['contrainte_id']] = $contrainte;
        }
        // Résultat retourné
        return $contraintes;
    }

    /**
     * Retourne les contraintes récupérées de l'établissement.
     * Elles sont indexées par l'ID de la contrainte paramétrée correspondante.
     *
     * @param  integer $etab_idx
     * @throws treatment_exception
     * @return array Tableau associatif
     *               C_PARAM ID => C_APP + C_PARAM VALUES
     */
    function get_contraintes_recuperees($etab_idx) {
        // Requête SQL
        $sql = 'SELECT
            lien_contrainte_etablissement.lien_contrainte_etablissement as lien_contrainte_etablissement_id,
            lien_contrainte_etablissement.texte_complete as lien_contrainte_etablissement_texte,
            lien_contrainte_etablissement.recuperee as lien_contrainte_etablissement_reference,
            contrainte.contrainte as contrainte_id,
            contrainte.libelle as contrainte_libelle,
            contrainte.nature as contrainte_nature,
            contrainte.texte as contrainte_texte,
            contrainte.lie_a_un_referentiel as contrainte_reference,
            lower(contrainte.groupe) as contrainte_groupe,
            lower(contrainte.sousgroupe) as contrainte_sousgroupe

        FROM '.DB_PREFIXE.'lien_contrainte_etablissement
        LEFT JOIN '.DB_PREFIXE.'contrainte
            ON  contrainte.contrainte = lien_contrainte_etablissement.contrainte
        WHERE
            lien_contrainte_etablissement.etablissement = '.$etab_idx.'
        AND lien_contrainte_etablissement.recuperee = \'t\'
        ORDER BY
            contrainte_groupe DESC,
            contrainte_sousgroupe,
            contrainte.ordre_d_affichage,
            contrainte.libelle';

        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                _("Erreur de base de données.").' '._("Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }
        $contraintes = array();
        while ($contrainte = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $contraintes[$contrainte['contrainte_id']] = $contrainte;
        }
        // Résultat retourné
        return $contraintes;
    }

    /**
     * Retourne toutes les contraintes appliquées à l'établissement.
     *
     * @param  integer $etab_idx
     * @throws treatment_exception
     * @return array
     */
    function get_contraintes_appliquees($etab_idx) {
        // Requête SQL
        $sql = 'SELECT
            lien_contrainte_etablissement.lien_contrainte_etablissement as lien_contrainte_etablissement_id,
            lien_contrainte_etablissement.texte_complete as lien_contrainte_etablissement_texte,
            lien_contrainte_etablissement.recuperee as lien_contrainte_etablissement_reference,
            contrainte.contrainte as contrainte_id,
            contrainte.libelle as contrainte_libelle,
            contrainte.nature as contrainte_nature,
            contrainte.texte as contrainte_texte,
            contrainte.lie_a_un_referentiel as contrainte_reference,
            lower(contrainte.groupe) as contrainte_groupe,
            lower(contrainte.sousgroupe) as contrainte_sousgroupe

        FROM '.DB_PREFIXE.'lien_contrainte_etablissement
        LEFT JOIN '.DB_PREFIXE.'contrainte
            ON  contrainte.contrainte = lien_contrainte_etablissement.contrainte
        WHERE
            lien_contrainte_etablissement.etablissement = '.$etab_idx.'
        ORDER BY
            contrainte_groupe DESC nulls last,
            contrainte_sousgroupe nulls last,
            contrainte.ordre_d_affichage,
            contrainte.libelle';

        $res = $this->f->db->query($sql);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($res, true)) {
            throw new treatment_exception(
                _("Erreur de base de données.").' '._("Contactez votre administrateur."),
                __METHOD__.": ".$res->getMessage()
            );
        }
        $contraintes = array();
        while ($contrainte = &$res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $contraintes[] = $contrainte;
        }
        // Résultat retourné
        return $contraintes;
    }

    /**
     * Recupère le libellé de la contrainte.
     *
     * @param  integer $contrainte Identifiant de la contrainte
     * @throws treatment_exception
     * @return string libellé de la contrainte
     */
    function getLibelleFromContrainte($contrainte) {

        // Initialisation résultat
        $libelle = '';

        // Si la condition n'est pas vide
        if ($contrainte !== "" 
            && $contrainte !== null
            && is_numeric($contrainte)) {

            // Requête SQL
            $sql = "SELECT libelle
                    FROM ".DB_PREFIXE."contrainte
                    WHERE contrainte = ".$contrainte;
            $this->f->addToLog(__METHOD__."(".$contrainte.") : db->getOne(\"".$sql."\")", 
                VERBOSE_MODE);
            $libelle = $this->f->db->getOne($sql);
            if ($this->f->isDatabaseError($libelle, true)) {
                throw new treatment_exception(
                    _("Erreur de base de données.").' '._("Contactez votre administrateur."),
                    __METHOD__.": ".$libelle->getMessage()
                );
            }
        }

        // Retourne résultat
        return $libelle;
    }

    /**
     * Récupère toutes les parcelles d'un établissement fourni
     * 
     * @param  integer $etab_idx
     * @throws treatment_exception
     * @return array
     */
    private function get_parcelles_by_etablissement($etab_idx) {
        require_once '../obj/etablissement.class.php';
        $etab = $this->get_inst_etablissement($etab_idx);
        try {
            return $etab->get_formated_parcelles();
        } catch (Exception $e) {
            throw new treatment_exception($e->getMessage());
        }
    }

    //
    // REQUÊTES OBJET
    //

    /**
     * Récupère le texte de la contrainte paramétrée selon un ordre de préférence
     *
     * @param  integer $id_contrainte Identifiant de la contrainte
     * @return string  Texte de la contrainte
     */
    function get_texte_from_contrainte_parametree($id_contrainte) {
        //
        require_once "../obj/contrainte.class.php";
        $contrainte_parametree = new contrainte($id_contrainte);
        return $contrainte_parametree->get_texte_preferentiel();
    }

    //
    // REQUÊTES WS
    //

    /**
     * Récupération des contraintes du référentiel SIG applicables aux parcelles fournies.
     *
     * @throws treatment_exception
     * @return array
     */
    function get_contraintes_sig($parcelles) {
        try {
            // WS OUT
            $geoaria = $this->f->get_inst_geoaria();
            return $geoaria->recup_contraintes($parcelles);
        } catch (geoaria_exception $e) {
            throw new treatment_exception($this->handle_geoaria_exception($e));
        }
    }

    //
    // GETTERS & SETTERS
    //
    
    /**
     *
     */
    function get_inst_etablissement($etab_idx = null) {
        //
        if (!is_null($etab_idx)) {
            require_once '../obj/etablissement.class.php';
            return new etablissement($etab_idx);
        }
        //
        if (is_null($this->inst_etablissement)) {
            require_once '../obj/etablissement.class.php';
            $etab_idx = $this->getVal('etablissement');
            if (empty($etab_idx) === true) {
                $etab_idx = $this->getParameter('idxformulaire');
            }
            $this->inst_etablissement = new etablissement($etab_idx);
        }
        //
        return $this->inst_etablissement;
    }

    //
    // HELPERS
    //

    /**
     * Permet de tester un champ de formulaire
     *
     * @param  string  $id_field identifiant du champ
     * @return boolean vrai si le champ est de type contrainte
     */
    private function is_field_contrainte($id_field) {
        if (strpos($id_field, self::CONTRAINTE_PREFIXE) === 0) {
            return true;
        }
        return false;
    }

    /**
     * Construit le message de validation de la récupération
     * 
     * @param  integer $tot_add nombre de contraintes ajoutées
     * @param  integer $tot_up  nombre de contraintes mises à jour
     * @return string
     */
    function contruct_msg_val($tot_add, $tot_up) {
        //
        $tot = $tot_add+$tot_up;
        $puce = '&nbsp;&bull;&nbsp;';
        $lf = '<br/>';
        //
        $msg_temp = _('%1$s contrainte%2$s récupérée%2$s dont :').$lf
            .$puce._('%3$s ajoutée%4$s').$lf
            .$puce._('%5$s mise%6$s à jour');
        return sprintf($msg_temp,
            $tot, $this->f->pluriel($tot),
            $tot_add, $this->f->pluriel($tot_add),
            $tot_up, $this->f->pluriel($tot_up)
        );
    }

    /**
     * Affiche le bouton retour dans la vue de récupérer
     * 
     * @return void
     */
    private function btn_retour() {
        $this->retoursousformulaire();
    }

    //
    // OVERRIDE
    //

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        parent::init_class_actions();

        // ACTION - 004 - tab
        // Interface spécifique de la vue de la liste des contraintes
        $this->class_actions[4] = array(
            "identifier" => "tab",
            "view" => "view_tab",
            "permission_suffix" => "tab",
            "condition" => "can_view_tab",
        );

        // ACTION - 005 - appliquer_post_treament
        // Vue du traitement d'application de contraintes paramétrées
        $this->class_actions[5] = array(
            "identifier" => "appliquer_post_treament",
            "view" => "view_appliquer_post_treament",
            "permission_suffix" => "ajouter",
        );

        // ACTION - 006 - recuperer
        // Interface spécifique de la vue de récupération de contraintes
        $this->class_actions[6] = array(
            "identifier" => "recuperer",
            "view" => "view_recuperer",
            "permission_suffix" => "recuperer",
            "condition" => "can_recuperer",
        );

        // ACTION - 007 - demarquer_recuperee
        // Pour qu'un technicien termine l'analyse
        $this->class_actions[7] = array(
            "identifier" => "demarquer_recuperee",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("démarquer comme récupérée"),
                "order" => 70,
                "class" => "derecuperer_sig-16",
            ),
            "method" => "demarquer_recuperee",
            "permission_suffix" => "demarquer_recuperee",
            "condition" => "is_recuperee",
        );

        // ACTION - 008 - appliquer
        // Interface spécification d'application de contraintes paramétrées
        $this->class_actions[8] = array(
            "identifier" => "appliquer",
            "view" => "view_appliquer",
            "permission_suffix" => "ajouter",
            "crud" => "create",
            "condition" => "can_appliquer",
        );
    }

    /**
     * Méthode de mise en page :
     * - interface spécifique du mode ajout
     * - fieldset unique pour les autres modes
     *
     * @param  object  &$form Objet du formulaire
     * @param  integer $maj   Mode du formulaire
     * @return void
     */
    function setLayout(&$form, $maj) {

        // En mode appliquer
        if ($maj == 8) {
            // Si la liste des contraintes est vide
            if (empty($this->contraintes_parametrees) === true) {
                $form->setBloc('lien_contrainte_etablissement', 'D', _("Aucune contrainte."), 
                    "noDataForm");
                return;
            }

            // Sauvegarde des données des contraintes pour les comparer
            $contrainte_before = array();
            $contrainte_before['contrainte_groupe'] = '';
            $contrainte_before['contrainte_sousgroupe'] = '';
            $contrainte_before['id_contrainte'] = '';

            // Classement des contraintes du paramétrage par groupe et sous-groupe
            foreach ($this->contraintes_parametrees as $id_contrainte => $contrainte) {
                // Gestion des sans groupe/sous-groupe
                if ($contrainte['contrainte_groupe'] === '') {
                    $contrainte['contrainte_groupe'] = _('Sans groupe');
                }
                if ($contrainte['contrainte_sousgroupe'] === '') {
                    $contrainte['contrainte_sousgroupe'] = _('Sans sous-groupe');
                }

                // Si le groupe de la contrainte diffère du précédent
                if ($contrainte['contrainte_groupe'] 
                    !== $contrainte_before['contrainte_groupe']) {

                    // On ferme l'éventuel groupe précédent
                    if ($contrainte_before['contrainte_groupe'] !== '') {
                        $form->setFieldset($contrainte_before['id_contrainte'], 'F');
                    }

                    // On ouvre éventuellement un nouveau groupe
                    if ($contrainte['contrainte_groupe'] !== '') {
                        $form->setFieldset($id_contrainte, 'D', $contrainte['contrainte_groupe'], 
                        "startClosed text_capitalize");
                    }
                }

                // Si le sous-groupe de la contrainte diffère du précédent
                if ($contrainte['contrainte_sousgroupe'] 
                    !== $contrainte_before['contrainte_sousgroupe']) {

                    // On ferme l'éventuel sous-groupe précédent
                    if ($contrainte_before['contrainte_sousgroupe'] !== '') {
                        $form->setFieldset($contrainte_before['id_contrainte'], 'F');
                    }

                    // On ouvre éventuellement un nouveau sous-groupe
                    if ($contrainte['contrainte_sousgroupe'] !== '') {
                        $form->setFieldset($id_contrainte, 'D', 
                            $contrainte['contrainte_sousgroupe'], 
                            "startClosed text_capitalize");
                    }
                }
                //
                $form->setBloc($id_contrainte, 'DF', "", "");

                // Mémorisation de la contrainte actuelle pour être utilisée
                // comme précédente lors de la prochaine boucle ou en fin de script
                $contrainte_before['contrainte_groupe'] = 
                    $contrainte['contrainte_groupe'];
                $contrainte_before['contrainte_sousgroupe'] = 
                    $contrainte['contrainte_sousgroupe'];
                $contrainte_before['id_contrainte'] = $id_contrainte;
            }
            // Ferme l'éventuel dernier sous-groupe
            if ($contrainte_before['contrainte_sousgroupe'] !== '') {
                $form->setFieldset($contrainte_before['id_contrainte'], 'F');
            }
            // Ferme l'éventuel dernier groupe
            if ($contrainte_before['contrainte_groupe'] !== '') {
                $form->setFieldset($contrainte_before['id_contrainte'], 'F');
            }
        }

        // En modes modifier, supprimer et consulter
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            //
            $form->setFieldset('texte_complete', 'DF', _("texte_complete"), "");
        }
    }

    /**
     * Permet de modifier l'affichage des boutons dans le sousformulaire.
     *
     * @param string  $datasubmit Données a transmettre
     * @param integer $maj        Mode du formulaire
     * @param array   $val        Valeur du formulaire
     */
    function boutonsousformulaire($datasubmit, $maj, $val = NULL) {

        //
        if (!$this->correct) {
            // Traitement par défaut
            $onclick =  "affichersform('".get_class($this)."', 
                '$datasubmit', this.form);return false;";
            //
            switch ($maj) {
                case 0:
                    $bouton = _("Ajouter");
                    break;
                case 1:
                    $bouton = _("Modifier");
                    break;
                case 2:
                    $bouton = _("Supprimer");
                    break;
                case 8:
                    $bouton = _("Ajouter");
                    // Traitement spécifique du mode ajouter
                    $onclick = "appliquer_contraintes(".get_class($this).", 
                        '../scr/form.php?obj=lien_contrainte_etablissement&action=5&idx=0', 
                        this.form);return false;";
                    break;
                default:
                    $bouton = _("Valider");
                    break;
            }
            //
            $params = array(
                "value" => $bouton,
                "onclick" => $onclick,
            );
            //
            $this->f->layout->display_form_button($params);
        }
    }

    /**
     * Cette méthode permet de composer le lien retour et de l'afficher.
     *
     * Utilisé par la VIEW sousformulaire.
     *
     * @return void
     */
    function retoursousformulaire($dnu1 = null, $dnu2 = null, $dnu3 = null, $dnu4 = null, $dnu5 = null, $dnu6 = null, $dnu7 = null, $dnu8 = null, $dnu9 = null, $dnu10 = null) {
        // Retour au listing spécifique si contexte favorable
        if ($this->can_view_tab() === true) {
            
            //
            echo "\n<a class=\"retour\" ";
            echo "href=\"#\" ";
            
            echo "onclick=\"ajaxIt('".$this->getParameter('objsf')."', '"; 
            echo "../scr/sousform.php"
                    ."?obj=lien_contrainte_etablissement"
                    ."&action=4"
                    ."&idx=0"
                    ."&retourformulaire=".$this->getParameter('retourformulaire').""
                    ."&idxformulaire=".$this->getParameter('idxformulaire')."');\"";
            echo  "\" ";
            echo ">";
            //
            echo _("Retour");
            //
            echo "</a>\n";
            //
            return;
        }
        //
        parent::retoursousformulaire();
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);

        // En mode appliquer

        if ($maj == 8) {
            // Ces champs ne seront pas utilisés par le post treatment
            // mais ils sont "déclarés" pour set le type en hidden
            $form->setVal('lien_contrainte_etablissement', '');
            $form->setVal('contrainte', '');
            $form->setVal('recuperee', '');
            $form->setVal('texte_complete', '');
            // Sous-formulaire d'un établissement
            $form->setVal('etablissement', $idxformulaire);
            // Tous les champs contraintes dont décochés
            foreach ($this->champs as $key => $id_field) {
                if ($this->is_field_contrainte($id_field) === true) {
                    $form->setVal($id_field, 'Non');
                }
            }
        }
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form,$maj) {
        //
        parent::setLib($form, $maj);
        // En modes modifier, supprimer et consulter
        if ($maj == 1 || $maj == 2 || $maj == 3) {
            $form->setLib('texte_complete', '');
        }
        // En mode appliquer
        if ($maj == 8) {
            // Parmi les champs, récupération du libellé de chaque contrainte
            foreach ($this->champs as $key => $id_field) {
                // 
                if ($this->is_field_contrainte($id_field) === true) {
                    $form->setLib(
                        $id_field,
                        $this->contraintes_parametrees[$id_field]['contrainte_lib']
                    );
                }
            }
        }
    }

    /**
     * Permet de définir le nombre de caractères maximum des champs.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setMax(&$form, $maj) {
        //
        parent::setMax($form, $maj);
        // En mode appliquer
        if ($maj == 8) {
            // Parmi les champs, 1 caractère max pour chaque contrainte
            foreach ($this->champs as $key => $id_field) {
                // 
                if ($this->is_field_contrainte($id_field) === true) {
                    $form->setMax($id_field, 1);
                }
            }
        }
    }

    /**
     * Permet de définir la taille des champs.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setTaille(&$form, $maj) {
        //
        parent::setTaille($form, $maj);
        // En mode appliquer
        if ($maj == 8) {
            // Parmi les champs, taille 1 pour chaque contrainte
            foreach ($this->champs as $key => $id_field) {
                // 
                if ($this->is_field_contrainte($id_field) === true) {
                    $form->setTaille($id_field, 1);
                }
            }
        }
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Objet du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setType(&$form, $maj) {
        parent::setType($form, $maj);
        
        // On cache tous les champs quel que soit le contexte
        // à l'exception du texte complété
        $form->setType('lien_contrainte_etablissement', 'hidden');
        $form->setType('etablissement', 'hidden');
        $form->setType('contrainte', 'hidden');
        $form->setType('recuperee', 'hidden');

        // En mode appliquer
        if ($maj == 8) {
            $form->setType('texte_complete', 'hidden');
            // Checkbox pour toutes les contraintes
            foreach ($this->champs as $key => $id_field) {
                if ($this->is_field_contrainte($id_field) === true) {
                    $form->setType($id_field, 'checkbox');
                }
            }
        }

        // En mode démarquer
        if ($maj == 7) {
            $form->setType('texte_complete', 'hidden');
        }
    }

    /**
     * Permet de modifier le fil d'Ariane.
     * 
     * @param  string $ent Fil d'Ariane récupéré
     * @return string      Nouveau fil d'Ariane
     */
    function getSubFormTitle($ent) {
        // Si on n'est pas en sous-formulaire d'un établissement
        // comportement standard
        if (in_array($this->getParameter("retourformulaire"), $this->foreign_keys_extended['etablissement']) === false) {
            return $ent;
        }

        // Entête
        $ent = _('etablissement').' -> '._('contraintes');

        // S'il ne s'agit pas d'une interface spécifique
        // (donc différent de l'ajout, du tableau et de la récupération)
        // ajout de l'ID de la contrainte appliquée
        if ($this->getParameter("maj") != 0
            && $this->getParameter("maj") != 4
            && $this->getParameter("maj") != 6) {
            $ent .= ' -> '.$this->getVal($this->clePrimaire);
            return $ent;
        }

        //
        return $ent;
    }

}

?>
