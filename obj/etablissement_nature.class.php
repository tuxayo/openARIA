<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once ("../gen/obj/etablissement_nature.class.php");

class etablissement_nature extends etablissement_nature_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Récupère l'identifiant de la nature de l'établissement par rapport
     * au code.
     *
     * @param string $code Code de la nature de l'établissement
     *
     * @return integer     Identifiant de la nature de l'établissement
     */
    function get_etablissement_nature_by_code($code) {
        // Initialisation de la variable de résultat
        $etablissement_nature = "";

        // Si le code n'est pas vide
        if (!empty($code)) {

            // Requête SQL
            $sql = "SELECT etablissement_nature
                    FROM ".DB_PREFIXE."etablissement_nature
                    WHERE LOWER(code) = LOWER('".$this->db->escapesimple($code)."')";
            $this->f->addToLog(__METHOD__."() : db->getOne(\"".$sql."\")", VERBOSE_MODE);
            $etablissement_nature = $this->f->db->getOne($sql);
            $this->f->isDatabaseError($etablissement_nature);
        }

        // Retourne le résultat
        return $etablissement_nature;
    }

}// fin classe
?>