<?php
/**
 * Ce script permet de déclarer la classe 'interface_referentiel_ads'.
 *
 * Interface avec le référentiel ADS.
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Définition de la classe 'interface_referentiel_ads'.
 */
class interface_referentiel_ads {

    /**
     * Instance de la classe utils
     * @var resource
     */
    var $f = null;

    /**
     * Constructeur.
     */
    public function __construct() {
        //
        $this->init_om_utils();
    }

    /**
     *
     */
    var $actions = array(
        // message "ERP Qualifie"
        101 => array(
            "code" => 101,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AT__INFORMATION_DE_QUALIFICATION_ADS",
            "label" => '(Dossier AT) Information de qualification ADS',
            "content_fields" => array(
                "competence" => null,
                "contraintes_plu" => null,
                "references_cadastrales" => null,
            ),
            "mode_lecture" => 1,
            "treatment" => array(
                "mode" => "attach",
                "dossier_coordination_type_code" => "AT",
            ),
        ),
        // message "Demande de completude de dossier PC pour un ERP"
        102 => array(
            "code" => 102,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__PRE_DEMANDE_DE_COMPLETUDE_ERP",
            "label" => '(Dossier PC/ERP) Pré-demande de complétude ERP',
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "PC",
            ),
        ),
        // message "Demande de qualification de dossier PC pour un ERP"
        103 => array(
            "code" => 103,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__PRE_DEMANDE_DE_QUALIFICATION_ERP",
            "label" => '(Dossier PC/ERP) Pré-demande de qualification ERP',
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "PC",
            ),
        ),
        // message "Demande d'instruction de dossier PC pour un ERP"
        104 => array(
            "code" => 104,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS",
            "label" => '(Dossier PC/ERP) Consultation officielle pour avis',
            "content_fields" => array(
                "consultation" => null,
                "date_envoi" => null,
                "service_abrege" => null,
                "service_libelle" => null,
                "date_limite" => null,
            ),
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "PC",
            ),
        ),
        // message "Arrete d'un dossier PC effectue"
        105 => array(
            "code" => 105,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__INFORMATION_DE_DECISION_ADS",
            "label" => '(Dossier PC/ERP) Information de décision ADS',
            "content_fields" => array(
                "decision" => null,
            ),
            "mode_lecture" => 3,
            "treatment" => array(
                "mode" => "attach",
            ),
        ),
        // message "Consultation ERP pour conformite"
        106 => array(
            "code" => 106,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE",
            "label" => '(Dossier PC/ERP) Consultation officielle pour conformité',
            "content_fields" => array(
                "consultation" => null,
                "date_envoi" => null,
                "service_abrege" => null,
                "service_libelle" => null,
                "date_limite" => null,
            ),
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "DAACT",
                "extras" => array(
                    "create_and_get_dcparent_infos_and_qualif" => array(
                        "dossier_coordination_type_code" => "PC",
                    ),
                ),
            ),
        ),
        // message "Demande d'ouverture ERP PC"
        107 => array(
            "code" => 107,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__DEMANDE_DE_VISITE_D_OUVERTURE_ERP",
            "label" => "(Dossier PC/ERP) Demande de visite d'ouverture ERP",
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "VR",
                "extras" => array(
                    "create_and_get_dcparent_infos" => array(
                        "dossier_coordination_type_code" => "PC",
                    ),
                ),
            ),
        ),
        // message "Depot de dossier DAT"
        108 => array(
            "code" => 108,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AT__DEPOT_INITIAL",
            "label" => '(Dossier AT) Dépôt initial',
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "createasync",
                "dossier_coordination_type_code" => "AT",
            ),
        ),
        // message "Annulation de la demande"
        109 => array(
            "code" => 109,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AT__RETRAIT_DU_PETITIONNAIRE",
            "label" => '(Dossier AT) Retrait du pétitionnaire',
            "mode_lecture" => 3,
            "treatment" => array(
                "mode" => "attach",
                "dossier_coordination_type_code" => "AT",
            ),
        ),
        // message "Demande d'ouverture ERP DAT"
        110 => array(
            "code" => 110,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP",
            "label" => "(Dossier AT) Demande de visite d'ouverture ERP",
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "create_or_attach",
                "dossier_coordination_type_code" => "VR",
                "extras" => array(
                    "create_and_get_dcparent_infos" => array(
                        "dossier_coordination_type_code" => "AT",
                    ),
                ),
            ),
        ),
        // message "Ajout de pièce"
        112 => array(
            "code" => 112,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AT__DEPOT_DE_PIECE_PAR_LE_PETITIONNAIRE",
            "label" => '(Dossier AT) Dépôt de pièce par le pétitionnaire',
            "content_fields" => array(
                "type_piece" => null,
            ),
            "mode_lecture" => 3,
            "treatment" => array(
                "mode" => "attach",
                "dossier_coordination_type_code" => "AT",
            ),
        ),
        // message "Nouvelle pièce"
        113 => array(
            "code" => 113,
            "categorie" => "entrant",
            "type" => "ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE",
            "label" => "Ajout d'une nouvelle pièce numérisée",
            "content_fields" => array(
                "date_creation" => null,
                "nom_fichier" => null,
                "type" => null,
                "categorie" => null,
            ),
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "attachmulti",
                "dossier_coordination_type_code" => array("AT", "PC", "DAACT", "VR", ),
            ),
        ),
        // message "Enjeu ADS"
        114 => array(
            "code" => 114,
            "categorie" => "entrant",
            "type" => "ADS_ERP__PC__ENJEU_ADS",
            "label" => "Information d'enjeu ADS",
            "content_fields" => array(
                "Dossier à enjeu ADS" => null
            ),
            "mode_lecture" => 0,
            "treatment" => array(
                "mode" => "attach",
                "dossier_coordination_type_code" => "PC",
            ),
        ),
        //
        201 => array(
            "code" => 201,
            "categorie" => "sortant",
            "type" => "ERP_ADS__MAJ_NUMERO_ETABLISSEMENT_ERP",
            "label" => 'Mise à jour du numéro de l\'établissement ERP',
            "content_fields" => array(
                "numero_erp",
            ),
            "resource" => "dossier_autorisations",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        //
        202 => array(
            "code" => 202,
            "categorie" => "sortant",
            "type" => "ERP_ADS__MAJ_STATUT_ETABLISSEMENT_ERP",
            "label" => 'Mise à jour du statut de l\'établissement ERP',
            "content_fields" => array(
                "erp_ouvert", "date_arrete",
            ),
            "resource" => "dossier_autorisations",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        // GET - dossier_autorisation
        203 => array(
            "code" => 203,
            "categorie" => "sortant",
            "type" => "ERP_ADS__RECUPERATION_INFORMATIONS_DOSSIER_AUTORISATION",
            "label" => "Récupération des informations depuis le référentiel ADS",
            "resource" => "dossier_autorisations",
            "method" => "GET",
        ),
        // message "ERP Qualifie"
        204 => array(
            "code" => 204,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__INFORMATION_COMPLETUDE_ERP_ACCESSIBILITE",
            "label" => '(Dossier PC/ERP) Information sur la complétude ERP Accessibilité',
            "content_fields" => array(
                "Complétude ERP ACC", "Motivation Complétude ERP ACC",
            ),
            "resource" => "messages",
            "method" => "POST",
            "mode_lecture" => 0,
        ),
        // message "Demande de completude de dossier PC pour un ERP"
        205 => array(
            "code" => 205,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__INFORMATION_COMPLETUDE_ERP_SECURITE",
            "label" => '(Dossier PC/ERP) Information sur la complétude ERP Sécurité',
            "content_fields" => array(
                "Complétude ERP SECU", "Motivation Complétude ERP SECU",
            ),
            "resource" => "messages",
            "method" => "POST",
            "mode_lecture" => 0,
        ),
        // message "Demande de qualification de dossier PC pour un ERP"
        206 => array(
            "code" => 206,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__INFORMATION_QUALIFICATION_ERP",
            "label" => '(Dossier PC/ERP) Information sur la qualification ERP',
            "content_fields" => array(
                "Confirmation ERP", "Type de dossier ERP", "Catégorie de dossier ERP",
            ),
            "resource" => "messages",
            "method" => "POST",
            "mode_lecture" => 0,
        ),
        // message "Demande d'instruction de dossier PC pour un ERP"
        207 => array(
            "code" => 207,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__NOTIFICATION_DOSSIER_A_ENJEUX_ERP",
            "label" => '(Dossier PC/ERP) Notification de dossier à enjeux ERP',
            "content_fields" => array(
                "Dossier à enjeux ERP",
            ),
            "resource" => "messages",
            "method" => "POST",
            "mode_lecture" => 0,
        ),
        //
        208 => array(
            "code" => 208,
            "categorie" => "sortant",
            "type" => "ERP_ADS__AT__MAJ_ARRETE_ERP_DOSSIER_AUTORISATION",
            "label" => '(Dossier AT) Mise à jour des informations sur l\'arrêté',
            "content_fields" => array(
                "arrete_effectue", "date_arrete",
            ),
            "resource" => "dossier_autorisations",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        //
        209 => array(
            "code" => 209,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__RETOUR_CONSULTATION",
            "label" => '(Dossier PC/ERP) Retour de Consultation',
            "content_fields" => array(
                "date_retour", "avis", "motivation", "nom_fichier", "fichier_base64",
            ),
            "resource" => "consultations",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        //
        210 => array(
            "code" => 210,
            "categorie" => "sortant",
            "type" => "ERP_ADS__AT__MAJ_COMPLETUDE_INCOMPLETUDE",
            "label" => '(Dossier AT) Complétude/Incomplétude',
            "content_fields" => array(
                "message", "date",
            ),
            "resource" => "dossier_instructions",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        //
        211 => array(
            "code" => 211,
            "categorie" => "sortant",
            "type" => "ERP_ADS__AT__CLOTURE",
            "label" => '(Dossier AT) Clôture',
            "content_fields" => array(
                "message", "date",
            ),
            "resource" => "dossier_instructions",
            "method" => "PUT",
            "mode_lecture" => 0,
        ),
        // GET - dossier_instruction
        212 => array(
            "code" => 212,
            "categorie" => "sortant",
            "type" => "ERP_ADS__RECUPERATION_INFORMATIONS_DOSSIER_INSTRUCTION",
            "label" => "Récupération des informations depuis le référentiel ADS",
            "resource" => "dossier_instructions",
            "method" => "GET",
        ),
        213 => array(
            "code" => 213,
            "categorie" => "sortant",
            "type" => "ERP_ADS__PC__AR_CONSULTATION_OFFICIELLE",
            "label" => "Accusé de réception de consultation d'un service ERP",
            "content_fields" => array(
                "consultation"
                ),
            "resource" => "messages",
            "method" => "POST",
        )
    );

    /**
     *
     */
    function get_config_messages_in() {
        $config = array();
        foreach ($this->actions as $key => $value) {
            if ($value["categorie"] === "entrant") {
                $config[] = $value;
            }
        }
        return $config;
    }

    function get_url_view_da($da) {
        //
        if (!file_exists("../dyn/services.inc.php")) {
            return false;
        }
        //
        include "../dyn/services.inc.php";
        if (!isset($ADS_URL_VISUALISATION_DA)) {
            return false;
        }
        $link = $ADS_URL_VISUALISATION_DA;
        $link = str_replace(
            "<ID_DA>",
            $da,
            $link
        );
        return $link;
    }

    /**
     *
     */
    function execute_action($code, $infos) {
        //
        if ($code == "203"  || $code == "212") {
            $ret = $this->get_infos_from_referentiel_ads($code, $infos);
        } else {
            $ret = $this->send_message_to_referentiel_ads($code, $infos);
        }
        return $ret;
    }

    /**
     *
     */
    function get_infos_from_referentiel_ads($code, $infos) {
        /**
         *
         */
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - %s',
                __METHOD__,
                $code,
                str_replace("\n", "", print_r($infos, true))
            ),
            VERBOSE_MODE
        );

        /**
         *
         */
        if (!isset($this->actions[$code])) {
            //
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur - Code %s invalide',
                    __METHOD__,
                    $code,
                    $code
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        if ($this->actions[$code]["categorie"] !== "sortant") {
            //
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur - Code %s invalide',
                    __METHOD__,
                    $code,
                    $code
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        //
        if (file_exists("../dyn/services.inc.php")) {
            require "../dyn/services.inc.php";
        }
        //
        switch ($this->actions[$code]["resource"]) {
            case 'dossier_instructions':
                $url = $ADS_URL_DOSSIER_INSTRUCTIONS."/".$infos["dossier_instruction"];
                break;
            case 'dossier_autorisations':
                $url = $ADS_URL_DOSSIER_AUTORISATIONS."/".$infos["dossier_autorisation"];
                break;
        }
        require_once "../services/outgoing/MessageSenderRest.class.php";
        $messageSenderRest = new MessageSenderRest($url);
        $response = $messageSenderRest->execute(
            $this->actions[$code]["method"],
            "application/json",
            null
        );
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - %s - Code Retour : %s - Response %s',
                __METHOD__,
                $code,
                $url,
                $messageSenderRest->getResponseCode(),
                str_replace("\n", "", print_r($response, true))
            ),
            VERBOSE_MODE
        );
        if ($messageSenderRest->getResponseCode() !== 200) {
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur : %s',
                    __METHOD__,
                    $code,
                    str_replace("\n", "", print_r($messageSenderRest->getResponse(), true))
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - return response;',
                __METHOD__,
                $code
            ),
            VERBOSE_MODE
        );
        //
        return $response;
    }

    /**
     *
     */
    function send_message_to_referentiel_ads($code, $infos) {
        /**
         *
         */
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - %s',
                __METHOD__,
                $code,
                str_replace("\n", "", print_r($infos, true))
            ),
            VERBOSE_MODE
        );

        /**
         *
         */
        if (!isset($this->actions[$code])) {
            //
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur - Code %s invalide',
                    __METHOD__,
                    $code,
                    $code
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        if ($this->actions[$code]["categorie"] !== "sortant") {
            //
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur - Code %s invalide',
                    __METHOD__,
                    $code,
                    $code
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        //
        $date = date('d/m/Y H:i:s');
        $emetteur = $_SESSION['login'];
        //
        $data = array(
            "type" => $this->actions[$code]["type"],
            "date" => $date,
            "emetteur" => $emetteur,
            "dossier_instruction" => $infos["dossier_instruction"],
        );
        if (isset($this->actions[$code]["content_fields"])) {
            $data["contenu"] = array();
            foreach ($this->actions[$code]["content_fields"] as $field) {
                if (!isset($infos[$field])) {
                    //
                    $this->addToLog(
                        sprintf(
                            '%s(): (WS|ERP->ADS)[%s] - champ %s obligatoire',
                            __METHOD__,
                            $code,
                            $field
                        ),
                        DEBUG_MODE
                    );
                    return false;
                }
                $data["contenu"][$field] = $infos[$field];
            }
        }

        /**
         * Ajout d'un enregistrement dans la table message_ws
         */
        // Composition des données du message
        $val = array(
            "dossier_coordination_message" => 0,
            //
            "dossier_coordination" => $infos["dossier_coordination"],
            "categorie" => $this->actions[$code]["categorie"],
            "type" => $this->actions[$code]["type"],
            "date_emission" => $date,
            "emetteur" => $emetteur,
            //
            "si_cadre_lu" => false,
            "si_technicien_lu" => false,
            "si_mode_lecture" => 'mode0',
            "acc_cadre_lu" => false,
            "acc_technicien_lu" => false,
            "acc_mode_lecture" => 'mode0',
            //
            "contenu" => json_encode($data),
            "contenu_json" => json_encode($data),

        );
        // Ajout de l'enregistrement en base de données
        require_once "../obj/dossier_coordination_message.class.php";
        $inst_message_ws = new dossier_coordination_message(0, $this->db, null);
        $ret = $inst_message_ws->ajouter($val);
        if ($ret !== true) {
            //
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur lors de la sauvegarde du message',
                    __METHOD__,
                    $code
                ),
                DEBUG_MODE
            );
            //
            return false;
        }

        /**
         *
         */
        //
        if (file_exists("../dyn/services.inc.php")) {
            require "../dyn/services.inc.php";
        }
        //
        switch ($this->actions[$code]["resource"]) {
            case 'messages':
                $url = $ADS_URL_MESSAGES;
                $json_data = json_encode($data);
                break;
            case 'dossier_instructions':
                $url = $ADS_URL_DOSSIER_INSTRUCTIONS."/".$infos["dossier_instruction"];
                $json_data = json_encode($data["contenu"]);
                break;
            case 'dossier_autorisations':
                $url = $ADS_URL_DOSSIER_AUTORISATIONS."/".$infos["dossier_autorisation"];
                $json_data = json_encode($data["contenu"]);
                break;
            case 'consultations':
                $url = $ADS_URL_CONSULTATIONS."/".$infos["consultation"];
                $json_data = json_encode($data["contenu"]);
                break;
        }
        require_once "../services/outgoing/MessageSenderRest.class.php";
        $messageSenderRest = new MessageSenderRest($url);
        $response = $messageSenderRest->execute(
            $this->actions[$code]["method"],
            "application/json",
            $json_data
        );
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - %s - Code Retour : %s - Response %s',
                __METHOD__,
                $code,
                $url,
                $messageSenderRest->getResponseCode(),
                str_replace("\n", "", print_r($response, true))
            ),
            VERBOSE_MODE
        );
        if ($messageSenderRest->getResponseCode() !== 200) {
            $this->addToLog(
                sprintf(
                    '%s(): (WS|ERP->ADS)[%s] - Erreur : %s',
                    __METHOD__,
                    $code,
                    str_replace("\n", "", print_r($messageSenderRest->getResponse(), true))
                ),
                DEBUG_MODE
            );
            return false;
        }

        /**
         *
         */
        //
        $this->addToLog(
            sprintf(
                '%s(): (WS|ERP->ADS)[%s] - return true;',
                __METHOD__,
                $code
            ),
            VERBOSE_MODE
        );
        //
        return true;
    }

    /**
     *
     */
    function view_controlpanel() {
        //
        $this->f->setRight("settings");
        $this->f->setTitle(_("administration_parametrage")." -> "._("interface avec le referentiel ads"));
        $this->f->setFlag(null);
        $this->f->display();
        $this->f->displaySubTitle("Configuration");
        //
        echo "<u>option :</u>";
        printf(
            '<pre>- option_referentiel_ads : <b>%s</b> => <b>%s</b></pre>',
            var_export($this->f->getParameter('option_referentiel_ads'), true),
            ($this->f->is_option_referentiel_ads_enabled() != true ? "DISABLED" : "ENABLED")
        );
        echo "<u>paramètres :</u>";
        $parametres = array(
            "ads__liste_services__si",
            "ads__liste_services__acc",
        );
        $out = '';
        foreach ($parametres as $parametre) {
            $out .= sprintf(
                "- %s : <b>%s</b>\n",
                $parametre,
                var_export($this->f->getParameter($parametre), true)
            );
        }
        printf(
            '<pre>%s</pre>',
            $out
        );
        //
        echo "<u>../dyn/services.inc.php :</u>";
        if (file_exists("../dyn/services.inc.php")) {
            //
            include "../dyn/services.inc.php";
            //
            $params = array(
                "MESSAGES",
                "DOSSIER_AUTORISATIONS",
                "DOSSIER_INSTRUCTIONS",
                "CONSULTATIONS",
                "VISUALISATION_DA",
            );
            echo "<pre>";
            foreach ($params as $param) {
                $name_var = sprintf('ADS_URL_%s', $param);
                if (isset(${$name_var})) {
                    echo sprintf(
                        '- $%s = "%s";',
                        $name_var,
                        $this->clean_url(${$name_var})
                    );
                } else {
                    echo sprintf('- $%s à définir', $name_var);
                }
                echo "\n";
            }
            echo "</pre>";
        } else {
            echo "- le fichier n'existe pas";
        }
    }

    // {{{ BEGIN - UTILS, LOGGER, ERROR

    /**
     * Nettoyer une URL des informations d'authentification HTTP en les 
     * remplaçant par ...
     *
     * @param string $url URL à nettoyer
     *
     * @return string URL nettoyée
     */
    function clean_url($url) {
        return preg_replace("#(?<=://)[^:@]+(:[^@]+)?(?=@)#", "...", $url);
    }

    /**
     * Initialisation de la classe utils.
     *
     * Cette méthode permet de vérifier que l'attribut f de la classe contient
     * bien la ressource utils du framework et si ce n'est pas le cas de la
     * récupérer.
     *
     * @return boolean
     */
    function init_om_utils() {
        //
        if (isset($this->f) && $this->f != null) {
            return true;
        }
        //
        if (isset($GLOBALS["f"])) {
            $this->f = $GLOBALS["f"];
            return true;
        }
        //
        return false;
    }

    /**
     * Ajout d'un message au système de logs.
     *
     * Cette méthode permet de logger un message.
     *
     * @param string  $message Message à logger.
     * @param integer $type    Niveau de log du message.
     *
     * @return void
     */
    function addToLog($message, $type = DEBUG_MODE) {
        //
        if (isset($this->f) && method_exists($this->f, "elapsedtime")) {
            logger::instance()->log(
                $this->f->elapsedtime()." : class ".get_class($this)." - ".$message,
                $type
            );
        } else {
            logger::instance()->log(
                "X.XXX : class ".get_class($this)." - ".$message,
                $type
            );
        }
    }

    // }}} END - UTILS, LOGGER, ERROR

}

?>
