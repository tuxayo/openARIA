<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once ("../gen/obj/visite.class.php");

class visite extends visite_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 001 - modifier
        //
        $this->class_actions[1]["condition"] = array(
            "is_programmation_in_working_state",
            "has_not_been_validated",
            "is_from_good_service",
        );

        // ACTION - 002 - supprimer
        //
        $this->class_actions[2]["condition"] = array(
            "is_programmation_in_working_state",
            "has_not_been_validated",
            "is_from_good_service",
        );

        // ACTION - 007 - annuler
        //
        $this->class_actions[7] = array(
            "identifier" => "annuler",
            "portlet" => array(
                "type" => "action-self",
                "libelle" => _("Annuler la programmation de la visite"),
                "order" => 60,
                "class" => "agenda-16",
            ),
            "view" => "view_annuler",
            "permission_suffix" => "annuler_plannification",
            "condition" => array(
                "is_programmation_in_working_state",
                "is_annulable",
                "has_been_validated",
                "is_from_good_service",
            )
        );

        // ACTION - 008 - a_poursuivre
        //
        $this->class_actions[8] = array(
            "identifier" => "a_poursuivre",
            "portlet" => array(
                "type" => "action-direct",
                "libelle" => _("a poursuivre"),
                "order" => 100,
                "class" => "a_poursuivre-16",
            ),
            "method" => "a_poursuivre",
            "permission_suffix" => "a_poursuivre",
            "condition" => array(
                "can_di_status_be_a_poursuivre",
                "is_from_good_service",
            ),
        );
    }

    /**
     * Méthode qui effectue les requêtes de configuration des champs.
     *
     * @param object  $form Instance du formulaire.
     * @param integer $maj  Mode du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     *
     * @return void
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        // On appelle la méthode de la classe parent
        parent::setSelect($form, $maj, $dnu1, $dnu2);
        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Si on se trouve dans un autre mode que l'ajout
        // On définit la requête des acteurs disponibles en fonction 
        // de leur rôle et du service de la programmation et 
        // des dates de validitée de l'acteur en fonction des dates
        // de la programmation
        if ($this->getParameter("maj") != 0) {
            //
            $inst_prog = $this->get_inst_programmation();
            //
            $sql_technicien = "
                SELECT 
                    acteur.acteur, 
                    acteur.nom_prenom
                FROM 
                    ".DB_PREFIXE."acteur
                WHERE 
                    (acteur.role = 'technicien'
                    OR acteur.role='cadre')
                    AND service = ".$inst_prog->getVal('service')."
                    AND ((acteur.om_validite_debut IS NULL 
                          AND (acteur.om_validite_fin IS NULL 
                               OR acteur.om_validite_fin > '".$inst_prog->get_date_firstday("Y-m-d")."'))
                         OR (acteur.om_validite_debut <= '".$inst_prog->get_date_lastday("Y-m-d")."'
                             AND (acteur.om_validite_fin IS NULL 
                                  OR acteur.om_validite_fin > '".$inst_prog->get_date_firstday("Y-m-d")."')))
                ORDER BY 
                    acteur.nom_prenom ASC
            ";
            //
            $this->init_select($form, $this->f->db, $maj, null, "acteur", $sql_technicien, $sql_acteur_by_id, true);
        }

        if ($maj == 0) {
            //
            $di = $this->getParameter("id_di");
            if ($di === null && (isset($_GET["id_di"]) || isset($_POST["dossier_instruction"]))) {
                if (isset($_GET["id_di"])) {
                    $di = $_GET["id_di"];
                }
                if (isset($_POST["dossier_instruction"])) {
                    $di = $_POST["dossier_instruction"];
                }
            }
        } else {
            //On récupère les données de base de données
            $di = $this->getVal("dossier_instruction");
        }
        $params = $this->getParamsEtablissementDossierInstruction($di);
        //
        if ($form->type['dossier_instruction']=='link'
            && isset($params['dossier_instruction'])){
            $form->setSelect("dossier_instruction", $params['dossier_instruction']);
        }
        if ($form->type['etablissement']=='etablissement'
            && isset($params['etablissement'])){
            $form->setSelect("etablissement", $params['etablissement']);
        }
        
        /* Convocation membres */
        $contenu = array();
        $contenu[0][0] = "";
        $contenu[1][0] = _('choisir')." "._('convocation_exploitants');
        $contenu[0][1] = "a_completer";
        $contenu[1][1] = _('a_completer');
        $contenu[0][2] = "a_envoyer";
        $contenu[1][2] = _('a_envoyer');
        $contenu[0][3] = "envoyees";
        $contenu[1][3] = _('envoyees');
        $contenu[0][4] = "non_envoyees";
        $contenu[1][4] = _('non_envoyees');
        $form->setSelect("convocation_exploitants", $contenu);

        // Récupération des courriers
        if($this->getVal("courrier_convocation_exploitants") != "") {
            require_once "../obj/courrier.class.php";
            $courrier = new courrier($this->getVal("courrier_convocation_exploitants"));
            $list_courrier = $courrier->get_info_courrier_child(
                                $this->getVal("courrier_convocation_exploitants")
                            );
            foreach ($list_courrier as $key => $value) {
                $list_courrier[$key]["obj"] = "courrier";
                $list_courrier[$key]["champ"] = "om_fichier_finalise_courrier";
            }
            $form->setSelect("courrier_convocation_exploitants", $list_courrier);
        } else {
            $form->setSelect("courrier_convocation_exploitants", null);
        }
    }

    /**
     * Surcharge.
     *
     * @return void
     */
    function setType(&$form, $maj) {
        // On appelle la méthode de la classe parent
        parent::setType($form, $maj);

        //
        $form->setType("programmation_version_annulation", "hidden");
        $form->setType("programmation_version_creation", "hidden");
        $form->setType("programmation_version_modification", "hidden");

         // Si contexte programmation
        if ($this->retourformulaire == 'programmation') {
            $form->setType('acteur', 'selecthiddenstatic');
        }
        $form->setType('dossier_instruction', 'link');
        $form->setType('etablissement', 'etablissement');
        $form->setType('courrier_convocation_exploitants', 'hidden');
        $form->setType('visite', 'hidden');
        $form->setType('programmation', 'hidden');
        $form->setType("visite_etat", "selecthiddenstatic");
        $form->setType("date_creation", "hiddenstaticdate");
        $form->setType("date_annulation", "hiddenstaticdate");

        $form->setType('convocation_exploitants', 'selecthiddenstatic');

        $retourform = strpos($this->retourformulaire, 'dossier_instruction');

        // En mode "ajouter"
        if ($maj == 0) {
            $form->setType('convocation_exploitants', 'hidden');
            $form->setType('heure_debut', 'heure_minute');
            $form->setType('heure_fin', 'heure_minute');
            $form->setType("visite_motif_annulation", "selecthidden");
            $form->setType("visite_etat", "selecthidden");
            $form->setType("date_creation", "hiddendate");
            $form->setType("date_annulation", "hiddendate");
            $form->setType("courrier_annulation", "selecthidden");
        }
        
        // En mode "modifier"
        if ($maj == 1) {
            $form->setType('convocation_exploitants', 'select');
            $form->setType('heure_debut', 'heure_minute');
            $form->setType('heure_fin', 'heure_minute');
            $form->setType("visite_motif_annulation", "selecthidden");
            $form->setType("courrier_convocation_exploitants", "listecourrier");
            $form->setType("convocation_exploitants", "selecthidden");
            $form->setType("courrier_annulation", "selecthidden");
            $form->setType("a_poursuivre", "hidden");
        }

        // En mode "supprimer"
        if ($maj == 2) {
            $form->setType("courrier_convocation_exploitants", "listecourrier");
            $form->setType("a_poursuivre", "hidden");
        }

        // En mode "consulter"
        if ($maj == 3) {
            $form->setType("courrier_convocation_exploitants", "listecourrier");
            $form->setType("a_poursuivre", "hidden");
        }
        
        //Si on est en sous formulaire d'un dossier d'instruction
        if ($retourform>=0&&$retourform!==false){
            //
            $form->setType('dossier_instruction', 'hidden');
        }
        //
        if ($maj == 8) {
            foreach ($this->champs as $key => $value) {
                $form->setType($value, 'hidden');
            }
            $form->setType("visite", "hiddenstatic");
        }
    }

    /**
     * Surcharge.
     *
     * @return void
     */
    function setOnchange(&$form, $maj) {
        // On appelle la méthode de la classe parent
        parent::setOnchange($form, $maj);
        //
        $form->setOnchange('heure_debut', 'check_visiting_hours(this);');
        $form->setOnchange('heure_fin', 'check_visiting_hours(this);');
    }

    /**
     * Permet de définir les valeurs des champs en sous-formualire.
     *
     * @param object  $form             Instance du formulaire.
     * @param integer $maj              Mode du formulaire.
     * @param integer $validation       Validation du form.
     * @param integer $idxformulaire    Identifiant de l'objet parent.
     * @param string  $retourformulaire Objet du formulaire.
     * @param mixed   $typeformulaire   Type du formulaire.
     * @param null    $dnu1 @deprecated Ancienne ressource de base de données.
     * @param null    $dnu2 @deprecated Ancien marqueur de débogage.
     */
    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$dnu1 = null, $dnu2 = null) {
        //
        parent::setValsousformulaire($form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, $dnu1, $dnu2);
        // Contexte programmation
        if ($retourformulaire == 'programmation') {
            // Mode ajout
            if ($maj == 0) {
                // Techncien sélectionné
                if (isset($_GET['id_technicien']) && $_GET['id_technicien'] != '') {
                    $form->setVal("acteur", $_GET['id_technicien']);
                }
                // Date de création
                $form->setVal("date_creation", date("d/m/Y"));
                // État de la visite
                $id_visite_etat = $this->get_id_visite_etat_by_code('pla');
                $form->setVal("visite_etat", $id_visite_etat);
                // Première validation
                if ($validation == 0) {
                    // Date du drop sur le calendrier
                    if (isset($_GET['date_visite']) && $_GET['date_visite'] != '') {
                        $form->setVal("date_visite", $_GET['date_visite']);
                    }
                    // Heures du drop sur le calendrier
                    if (isset($_GET['heure_debut']) && $_GET['heure_debut'] != '') {
                        $form->setVal("heure_debut", $_GET['heure_debut']);
                        $form->setVal("heure_fin", $_GET['heure_fin']);
                    }
                    // Dossier d'instruction du drag
                    if (isset($_GET['id_di']) && $_GET['id_di'] != '') {
                        $form->setVal("dossier_instruction", $_GET['id_di']);
                        $etab_params = $this->getParamsEtablissementDossierInstruction($_GET['id_di']);
                        $etablissement = $etab_params['etablissement']['idx'];
                        $form->setVal("etablissement", $etablissement);
                    }
                } elseif (isset($_POST['dossier_instruction']) && $_POST['dossier_instruction'] != '') {
                    $form->setVal("dossier_instruction", $_POST['dossier_instruction']);
                    $etab_params = $this->getParamsEtablissementDossierInstruction($_POST['dossier_instruction']);
                    $etablissement = $etab_params['etablissement']['idx'];
                    $form->setVal("etablissement", $etablissement);
                }
            }
        }
    }

    /**
     * Surcharge.
     *
     * @return void
     */
    function setValF($val = array()) {
        // On appelle la méthode de la classe parent
        parent::setValF($val);
        // Récupération du numéro courant de version de la programmation
        if ($this->getParameter("maj") != 0) {
            // Soit on récupère la programmation depuis la valeur présente en base
            $inst_prog = $this->get_inst_programmation();
        } else {
            // Soit on récupère la programmation depuis la valeur postée
            // dans le formulaire
            $inst_prog = $this->get_inst_programmation($val["programmation"]);
        }
        $this->valF["programmation_version_modification"] = $inst_prog->getVal("version");
    }

    /**
     * Surcharge.
     *
     * @return void
     */
    function setValFAjout($val = array()) {
        // On appelle la méthode de la classe parent
        parent::setValFAjout($val);
        // Définition de la date de création du courrier
        $this->valF['date_creation'] = date("Ymd");
        $this->valF['visite_etat'] = $this->get_id_etat_visite("pla");
        // Récupération du numéro courant de version de la programmation
        $inst_prog = $this->get_inst_programmation($val["programmation"]);
        $this->valF["programmation_version_creation"] = $inst_prog->getVal("version");
    }

    /**
     * Surcharge.
     *
     * @return void
     */
    function verifier($val = array(), &$dnu1 = null, $dnu2 = null) {
        // On appelle la méthode de la classe parent
        parent::verifier($val);
        
        // Vérification de la date de la visite qui doit absolument être 
        // comprise entre le premier jour et le dernier jour de la semaine de 
        // programmation
        if ($val["date_visite"] != "") {
            $inst_prog = $this->get_inst_programmation($val["programmation"]);
            $date_visite = $this->f->formatdate($val["date_visite"], false);
            if (strtotime($date_visite) < strtotime($inst_prog->get_date_firstday("Y-m-d"))
                || strtotime($date_visite) > strtotime($inst_prog->get_date_lastday("Y-m-d"))) {
                //
                $this->correct = false;
                $this->addToMessage(_("La date doit être comprise dans la semaine de programmation."));
            }
        }

        // Regex vérifiant la validité du couple heure/minute attendu
        $regex = "#^(([0-1][0-9])|([0-2][0-3])):{1}([0-5][0-9])$#";
        // Par défaut on considère les heures valides
        $correct_hours = true;
        // Si l'heure de début est incorrecte mais saisie
        if (!preg_match($regex, $val["heure_debut"])
            && $val["heure_debut"] != "") {
            $this->addToMessage(_("Le champ").' <span class="bold">'._("heure_debut").'</span> '._("est invalide."));
            $correct_hours = false;
            $this->correct = false;
        }
        // Si l'heure de fin est incorrecte mais saisie
        if (!preg_match($regex, $val["heure_fin"])
            && $val["heure_fin"] != "") {
            $this->addToMessage(_("Le champ").' <span class="bold">'._("heure_fin").'</span> '._("est invalide."));
            $correct_hours = false;
            $this->correct = false;
        }
        // Si les heures sont saisies et valides
        if ($val["heure_debut"] != ""
            && $val["heure_fin"] != ""
            && $correct_hours == true) {
            // On vérifie que l'heure de fin est supérieure à celle de début
            $begin = explode(":", $val["heure_debut"]);
            $end = explode(":", $val["heure_fin"]);
            $begin_hour = intval($begin[0]);
            $begin_minute = intval($begin[1]);
            $end_hour = intval($end[0]);
            $end_minute = intval($end[1]);
            // On vérifie heure de fin > heure de début
            if (($end_hour < $begin_hour) || 
                ($begin_hour == $end_hour && $end_minute <= $begin_minute)) {
                $compare = _("posterieure");
                if ($begin_hour == $end_hour && $end_minute == $begin_minute) {
                    $compare = _("egale");
                }
                $this->addToMessage(_("L'").' <span class="bold">'._("heure_debut").'</span> '._("est")." ".$compare." "._(" a l'").' <span class="bold">'._("heure_fin").'</span>.');
                $this->correct = false;
            }
        }
    }

    /**
     * Surcharge.
     *
     * @return boolean
     */
    function triggerajouterapres($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la méthode de la classe parent
        parent::triggerajouterapres($id, $db, $val, $DEBUG);

        //// Mise à jour d'informations sur le DI rattaché à la visite
        // On instancie le DI rattaché
        $inst_di = $this->get_inst_dossier_instruction(
            $this->valF["dossier_instruction"]
        );
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // => Mise à jour du statut du DI
        $valF["statut"] = $inst_di->get_status_when_visit_is_planned(
            $this->valF['a_poursuivre']
        );
        // => Mise à jour du technicien
        // On vérifie si le DI en question a déjà un technicien affecté ou non
        $technicien = $this->affectation_technicien(
            $this->valF["dossier_instruction"], 
            $this->valF["acteur"]
        );
        // Si le DI n'a pas de technicien affecté, on lui affecte celui de la visite
        if (!is_null($technicien)) {
            //
            $valF["technicien"] = $technicien;
        }
        // => Mise à jour effective du DI
        $ret = $inst_di->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return false;
        }

        //// Met à jour les dates de suivis de l'établissement
        $update_etablissement_tracking_dates = $this->update_etablissement_tracking_dates();
        //
        if($update_etablissement_tracking_dates === false) {
            //
            $this->correct = false;
            return false;
        }

        //
        return true;
    }

    /**
     * Surcharge.
     *
     * @return boolean
     */
    function triggermodifierapres($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la méthode de la classe parent
        parent::triggermodifierapres($id, $db, $val, $DEBUG);

        //// Mise à jour d'informations sur le DI rattaché à la visite
        // On instancie le DI rattaché
        $inst_di = $this->get_inst_dossier_instruction();
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // => Mise à jour du technicien
        // On vérifie si le DI en question a déjà un technicien affecté ou non
        $technicien = $this->affectation_technicien(
            $this->valF["dossier_instruction"], 
            $this->valF["acteur"]
        );
        // Si le DI n'a pas de technicien affecté, on lui affecte celui de la visite
        if (!is_null($technicien)) {
            //
            $valF["technicien"] = $technicien;
        }
        // => Mise à jour effective du DI
        if (!empty($valF)) {
            $ret = $inst_di->update_autoexecute($valF);
            if ($ret === false) {
                $this->correct = false;
                $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
                return false;
            }
        }

        //// Met à jour les dates de suivis de l'établissement
        $update_etablissement_tracking_dates = $this->update_etablissement_tracking_dates();
        //
        if($update_etablissement_tracking_dates === false) {
            //
            $this->correct = false;
            return false;
        }

        //
        return true;
    }

    /**
     * Surcharge.
     *
     * @return boolean
     */
    function triggersupprimerapres($id, &$dnu1 = null, $val = array(), $dnu2 = null) {
        // On appelle la méthode de la classe parent
        parent::triggersupprimerapres($id, $dnu1, $val, $dnu2);

        // Mise à jour d'informations sur le DI rattaché à la visite
        return $this->update_status_di_when_visit_is_canceled_or_deleted();
    }

    /**
     * Mise à jour du statut du DI rattaché à la visite annulée ou supprimée
     * qui appelle cette méthode.
     * 
     * @return boolean
     */
    function update_status_di_when_visit_is_canceled_or_deleted($npai = false) {
        // On instancie le DI rattaché
        $inst_di = $this->get_inst_dossier_instruction();
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Récupération et définition du nouveau statut
        $valF["statut"] = $inst_di->get_status_when_visit_is_canceled_or_deleted($npai);
        // Mise à jour effective du DI
        $ret = $inst_di->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return false;
        }
        //
        return true;
    }

    // FIN METHODES FRAMEWORK
    // METHODES OBJET
    
    /**
     * Affectation d'un technicien au dossier d'instruction.
     *
     * @param $id_dossier_instruction L'identifiant du dossier d'instruction.
     * @param $id_technicien          L'identifiant du technicien.
     * 
     * @return interger L'identifiant du technicien à affecter
     */
    function affectation_technicien($id_dossier_instruction, $id_technicien) {

        //Vérification des variables fournies
        if ($id_dossier_instruction !=''
            && is_numeric($id_dossier_instruction)
            && $id_technicien != '' && 
            is_numeric($id_technicien)) {
            
            //On récupère le technicien du dossier d'instruction
            $sql = "SELECT technicien,service
                    FROM ".DB_PREFIXE."dossier_instruction
                    WHERE dossier_instruction = ".$id_dossier_instruction;
            $res = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($res);
            
            $dossier_instruction_donnees =& $res->fetchRow(DB_FETCHMODE_ASSOC);
            
            // On vérifie que le dossier d'instruction n'a pas de technicien déjà affecté 
            if ($dossier_instruction_donnees['technicien'] == '' 
                && !is_numeric($dossier_instruction_donnees['technicien'])) {
                
                //On récupère le service du technicien passé en paramètre
                $sql = "SELECT service
                    FROM ".DB_PREFIXE."acteur
                    WHERE acteur = ".$id_technicien;
                $technicien_service = $this->f->db->getOne($sql);
                $this->f->addToLog(__METHOD__."(): db->getone(\"".$sql."\")", VERBOSE_MODE);
                $this->f->isDatabaseError($technicien_service);
                
                //On vérifie que le service du dossier d'instruction et du 
                //techncien sont les mêmes
                if ($technicien_service===$dossier_instruction_donnees['service']){
                    
                    //On retourne le technicien à affecter au dossier d'instruction
                    return $id_technicien;

                } else {

                    $this->f->addToLog(__METHOD__."(): "._("Le service du dossier d'instruction et du technicien sont différents"), VERBOSE_MODE);
                    return null;
                }

            } else {

                $this->f->addToLog(__METHOD__."(): "._("Le dossier d'instruction avait déjà un instructeur affecté"), VERBOSE_MODE);
                return null;
            }

        } else {

            $this->f->addToLog(__METHOD__."(): "._("Les variables fournies sont incorrectes"), VERBOSE_MODE);
            return null;
        }
    }

    // FIN METHODES OBJET
    // ACTIONS SPECIFIQUES

    /**
     * TREATMENT - add_mail_for_contact.
     *
     * ...
     *
     * @param boolean
     */
    function add_mail_for_contact() {
        $this->begin_treatment(__METHOD__);

        /**
         * Création du courrier
         */
        //
        $inst_courrier = $this->get_inst_courrier("]");
        //
        foreach ($inst_courrier->champs as $value) {
            $val[$value] = "";
        }
        $val["complement1_om_html"] = $this->encode_and_escape("complement1_om_html");
        $val['dossier_instruction'] = $this->getVal('dossier_instruction');
        $val['mailing'] = 't';
        $val['visite'] = $this->getVal($this->clePrimaire);
        $modele_edition = $this->get_modele_edition_by_code('convoc_expl');
        //
        if (empty($modele_edition)) {
            //
            $this->correct = false;
            $this->addToMessage(_("Le modele d'edition n'existe pas."));
            return false;
        }
        $val['modele_edition'] = $modele_edition;
        // Récupère le type du courrier depuis le modèle d'édition
        $val['courrier_type'] = $this->f->get_courrier_type_by_modele_edition($modele_edition);
        // Récupération de la liste des exploitants
        $id_exploit = $this->get_id_exploit();
        if ($id_exploit === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de l'affectation de la convocation"));
            return false;
        }
        //
        $val['contacts_lies'] = $id_exploit;
        // Ajout de la convocation à la table courrier
        if ($inst_courrier->ajouter($val, $this->f->db) === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de l'edition de la convocation"));
            return false;
        }
        //
        $courrier_id = $inst_courrier->valF[$inst_courrier->clePrimaire];

        /**
         * Finalisation du courrier précédemment créé.
         */
        $inst_courrier = $this->get_inst_courrier($courrier_id);
        $ret = $inst_courrier->finalize();
        if ($ret !== true) {
            $this->addToLog(
                __METHOD__."(): Erreur lors de la finalisation du courrier (id=".$courrier_id.")",
                DEBUG_MODE
            );
            $this->correct = false;
            $this->addToMessage($inst_courrier->msg);
            return false;
        }

        /**
         * Mise à jour de la visite sur laquelle on se trouve :
         * - on rattache l'id du courrier que l'on vient de finaliser
         * - on note l'état des convocations exploitant à 'envoyees'
         */
        $val_visite = array();
        foreach ($this->champs as $value) {
            $val_visite[$value] = $this->getVal($value);
        }
        $val_visite["date_creation"] = $this->dateDBToForm($this->getVal("date_creation"));
        $val_visite["date_annulation"] = $this->dateDBToForm($this->getVal("date_annulation"));
        $val_visite["date_visite"] = $this->dateDBToForm($this->getVal("date_visite"));
        $val_visite["courrier_convocation_exploitants"] = $courrier_id;
        $val_visite["convocation_exploitants"] = "envoyees";
        $ret = $this->modifier($val_visite);
        if ($ret !== true) {
            $this->addToLog(
                __METHOD__."(): Erreur lors de la mise à jour de la visite (id=".$this->getVal($this->clePrimaire).")",
                DEBUG_MODE
            );
            $this->addToMessage(_("Erreur lors de la mise à jour de la visite."));
            $this->correct = false;
            return false;
        }

        /**
         * Génération sur le système de stockage de fichiers temporaires du
         * courrier qui vient d'être créé (mailing : tous les courriers enfants
         * concaténés / pas mailing : le courrier unique).
         */
        $list_courrier_child = $inst_courrier->get_list_courrier_child($this->valF['courrier_convocation_exploitants']);
        $tmpfile_uid = $inst_courrier->generate_all_courriers_pdf(
            $list_courrier_child,
            "temporary"
        );
        if ($tmpfile_uid === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la génération du document pdf téléchargeable de la visite."));
            return false;
        }

        //
        $this->addToMessage(
            sprintf(
                '
%s
<a class="om-prev-icon pdf-16" target="_blank" href="../spg/file.php?mode=temporary&amp;uid=%s">
    %s
</a>
',
                _("Lettre de convocation des exploitants :"),
                $tmpfile_uid,
                _("Convocation")
            )
        );
        //
        $this->end_treatment(__METHOD__, true);
    }

    /**
     * VIEW - view_annuler.
     *
     * @return void
     */
    function view_annuler() {
        //
        $type_date_annulation= "date";
        $type_visite_motif_annulation= "select";
        $postvar = $this->getParameter("postvar");
        //Si le formulaire a été validé
        if(isset($postvar['submit'])&&$postvar['submit']!=''){
            
            $champs_manquant = array();
            $champs_mauvais_format = array();
            
            //On vérifie les données obligatoires
            if (!isset($postvar['visite']) ||$postvar['visite']=='')
                $champs_manquant[]=_("visite");
            if (!isset($postvar['date_annulation']) ||$postvar['date_annulation']=='')
                $champs_manquant[]=_("date_annulation");
            if (!isset($postvar['visite_motif_annulation']) ||$postvar['visite_motif_annulation']=='')
                $champs_manquant[]=_("visite_motif_annulation");
            
            if (!is_numeric($postvar['visite']))
                $champs_mauvais_format[]=_("visite");
            if (isset($postvar['visite_motif_annulation'])&&$postvar['visite_motif_annulation']!=''
                &&!is_numeric($postvar['visite_motif_annulation']))
                $champs_mauvais_format[]=_("visite_motif_annulation");
            
            //Si un des champs obligatoires n'est pas fournies on affiche un 
            //message d'erreur
            if(count($champs_manquant)>0||count($champs_mauvais_format)>0){
                
                $msg = "";
                foreach ($champs_manquant as $value) {
                    $msg.=_("Le champ ")."<span class=\"bold\">".$value."</span>"._(" est obligatoire")."<br/>";
                }
                foreach ($champs_mauvais_format as $value) {
                    $msg.=_("La valeur du champ ")."<span class=\"bold\">".$value."</span>"._(" doit être numérique")."<br/>";
                }
                $this->correct = false;
                $this->addToMessage($msg."<br/>"._("SAISIE NON ENREGISTRÉE"));
                $this->message();
            }
            //Les données obligatoires ont été correctement fournies
            else{
                //On récupère l'identifiant de l'état de visite "annulée"
                $sql = "SELECT visite_etat 
                        FROM ".DB_PREFIXE."visite_etat 
                        WHERE lower(code) = 'ann'";
                // Exécution de la requête
                $visite_etat = $this->f->db->getone($sql);
                $this->f->addToLog(__METHOD__."(): db->getone(\"".$sql."\");", VERBOSE_MODE);
                $this->f->isDatabaseError($visite_etat);
                
                //Il faut que l'état de visite existe
                if (is_numeric($visite_etat)){
                    
                    //On met à jour la visite
                    require_once '../obj/visite.class.php';
                    $visite = new visite($postvar['visite']);
                    //
                    $values = array();
                    foreach($visite->champs as $key => $champ) {
                        
                        $values[$champ] = $visite->getVal($champ);
                    }
                    //Formattage des dates
                    $values['date_creation']=$this->dateDBToForm($visite->getVal('date_creation'));
                    $values['date_visite']=$this->dateDBToForm($visite->getVal('date_visite'));
                    //
                    $values['visite_etat'] = $visite_etat;
                    $values['date_annulation'] = $postvar['date_annulation'];
                    $values['visite_motif_annulation'] = $postvar['visite_motif_annulation'];
                    // Récupération du numéro courant de version de la programmation
                    $inst_prog = $this->get_inst_programmation();
                    $values["programmation_version_annulation"] = $inst_prog->getVal("version");
                    //Si un courrier d'annulation a été choisi
                    if ($this->hasCourrierAnnulation()){
                        $values['convocation_exploitants'] = "a_completer";
                    }
                    //
                    $this->f->db->autoCommit(false);
                    // Un courrier d'annulation est généré seulement si 
                    // les convocations expoloitants ont été envoyées
                    if ($this->getVal("convocation_exploitants")==="envoyees"){

                        // Génération du courrier
                        $courrier_annulation = $this->create_courrier_annulation();
                        // On vérifie que la génération s'est correctement effectuée
                        if(!is_numeric($courrier_annulation)){
                            $visite->undoValidation();
                            $this->addToLog(
                                __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de l'ajout des documents générés (annulation)",
                                DEBUG_MODE
                            );
                            $this->correct = false;
                            $this->cleanMessage();
                            $this->addToMessage(_("Une erreur s'est produite. Veuillez contacter votre administrateur."));
                            $this->message();
                            return;
                        }
                        $values["courrier_annulation"] = $courrier_annulation;
                    }
                    // On annule la visite
                    if($visite->modifier($values) === true) {
                        // On met à jour le statut du DI
                        $ret = $this->update_status_di_when_visit_is_canceled_or_deleted();
                        if ($ret === false) {
                            $visite->undoValidation();
                            $this->message();
                            return;
                        }
                        // On récupère le motif de l'annulation
                        $sql = "SELECT code
                            FROM ".DB_PREFIXE."visite_motif_annulation
                            WHERE visite_motif_annulation=".$postvar['visite_motif_annulation'];
                        $visite_motif_annulation = $this->f->db->getone($sql);
                        $this->f->addToLog(__METHOD__."(): db->getone(\"".$sql."\");", VERBOSE_MODE);
                        $this->f->isDatabaseError($visite_motif_annulation);
                        // Si le motif de l'annulation est NPAI
                        if ($visite_motif_annulation == 'NPAI') {
                            // On marque l'établissement en NPAI
                            $inst_etablissement = $this->get_inst_etablissement();
                            $valF = array();
                            $valF["npai"] = true;
                            // Mise à jour effective de l'établissement
                            $ret = $inst_etablissement->update_autoexecute($valF);
                            if ($ret === false) {
                                $this->correct = false;
                                $this->addToMessage(_("Erreur lors de la mise à jour de l'etablissement."));
                                $visite->undoValidation();
                                $this->message();
                                return;
                            }
                        }
                        // Validation de la transaction
                        $this->f->db->commit();
                        $this->correct = true;
                        $this->addToMessage(_("Vos modifications ont bien ete enregistrees."));
                        $this->message();
                        //Changement du type des champs
                        $type_date_annulation= "datedisabled";
                        $type_visite_motif_annulation= "selectdisabled";
                    } else {
                        //
                        $visite->undoValidation();
                        $this->addToLog(
                            __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur lors de la mise à jour de la visite",
                            DEBUG_MODE
                        );
                        $this->correct = false;
                        $this->cleanMessage();
                        $this->addToMessage(_("Une erreur s'est produite. Veuillez contacter votre administrateur."));
                        $this->message();
                    }
                } else {
                    $this->correct = false;
                    $this->addToLog(
                        __METHOD__."()(id=".$this->getVal($this->clePrimaire)."): Erreur de paramétrage, l'état ANN n'existe pas",
                        DEBUG_MODE
                    );
                    $this->cleanMessage();
                    $this->addToMessage(_("Une erreur s'est produite. Veuillez contacter votre administrateur."));
                    $this->message();
                }
            }
        }
        
        
        /**
         * CONSTRUCTION DU FORMULAIRE
         */
        //
        printf("\n<!-- ########## START DBFORM ########## -->\n");
        printf("<form name=\"f2\" action=");
        //En sous-formulaire
        if ($this->getParameter('retour')!=''){
            printf("\"\" ");
            printf("onsubmit=\"affichersform('visite', '".$this->getDataSubmitSousForm()."&retourformulaire=".$this->getParameter('retourformulaire')."', this);return false;\" ");
        }
        else {
            printf("\"../scr/form.php?obj=visite&validation=1&action=7&idx=".$this->getParameter('idx')."&idz=".$idz."&premier=".$this->getParameter('premier')."&retour=form\" ");
        }
        printf("method=\"post\" >");
        
        printf("<div>");
        //
        if ($this->getParameter('retourformulaire') != '') {
            $this->retoursousformulaire();
        } else {
            $this->retour();
        }
        printf("</div>");
        
        printf("<div class=\"formEntete\">");
        //Liste des champs
        $champs = array("visite", "etablissement", "dossier_instruction", "date_creation",
            "date_annulation", "visite_motif_annulation");
        //Création du formulaire
        $form = new $this->om_formulaire(NULL, $this->getParameter("maj"), 0, $champs);
        
        $form->setFieldset('visite','D',_('Annulation de visite'),"");
        $form->setBloc('visite','D','');
        $form->setBloc('visite_motif_annulation','F','');
        $form->setFieldset('visite_motif_annulation','F','');
        
        //Configuration des libelle
        $form->setLib("etablissement", _("etablissement"));
        $form->setLib("dossier_instruction", _("dossier_instruction"));
        $form->setLib("date_creation", _("date_creation"));
        $form->setLib("date_annulation", _("date_annulation"));
        $form->setLib("visite_motif_annulation", _("visite_motif_annulation"));
                    
        //Configuration des types
        $form->setType("visite", "hiddenstatic");
        $form->setType("etablissement", "link");
        $form->setType("dossier_instruction", "link");
        $form->setType("date_creation", "static");
        $form->setType("date_annulation", $type_date_annulation);
        $form->setType("visite_motif_annulation", $type_visite_motif_annulation);

        $form->setRequired("visite");
        $form->setRequired("date_annulation");
        $form->setRequired("visite_motif_annulation");
        
        $form->setOnchange("date_annulation", "fdate(this)");
        
        //On récupère les données de base de données
        $params = $this->getParamsEtablissementDossierInstruction($this->getVal("dossier_instruction"));
        
        $form->setSelect("dossier_instruction", $params['dossier_instruction']);
        $form->setSelect("etablissement", $params['etablissement']);

        $sql_visite_motif_annulation="SELECT visite_motif_annulation.visite_motif_annulation, visite_motif_annulation.libelle 
            FROM ".DB_PREFIXE."visite_motif_annulation
            WHERE ((visite_motif_annulation.om_validite_debut IS NULL AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE)) OR (visite_motif_annulation.om_validite_debut <= CURRENT_DATE AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE))) 
            ORDER BY visite_motif_annulation.libelle ASC";

        // Exécution de la requête
        $visite_motif_annulation = $this->f->db->query($sql_visite_motif_annulation);
        $this->f->addToLog(__METHOD__."(): db->query(\"".$sql_visite_motif_annulation."\");", VERBOSE_MODE);
        $this->f->isDatabaseError($visite_motif_annulation);

        //Paramétrage du champ 
        $contenu = array();
        $contenu[0][] = '';
        $contenu[1][] = _("tous les motifs d'annulation");

        while ( $row =& $visite_motif_annulation->fetchRow(DB_FETCHMODE_ASSOC) ) {
            $contenu[0][] = $row['visite_motif_annulation'];
            $contenu[1][] = $row['libelle'];
        }
        $form->setSelect("visite_motif_annulation", $contenu);

        $form->setVal("visite", $this->getParameter('idx'));
        $form->setVal("etablissement", "link");
        $form->setVal("dossier_instruction", "link");
        $form->setVal("date_creation", $this->dateDBToForm($this->getVal("date_creation")));
        $form->setVal("date_annulation", isset($postvar['date_annulation'])?
            $postvar['date_annulation']:date("d/m/Y"));
        $form->setVal("visite_motif_annulation", isset($postvar['visite_motif_annulation'])?
            $postvar['visite_motif_annulation']:0);

        $form->afficher($champs, 0, false, false);
        printf("</div>");
        printf("\t<div class=\"formControls\">\n");
        
        if (!isset($postvar['submit'])||$this->correct === false){
            
            $params = array(
                "value" => _("Annuler la programmation de la visite"),
                "name" => "submit",
            );
            //
            $this->f->layout->display_form_button($params);
        }
        //
        if ($this->getParameter('retourformulaire') != '') {
            $this->retoursousformulaire();
        } else {
            $this->retour();
        }
        //
        printf("</div>");
        //
        printf("</form>\n");
        
    }
    
    // FIN ACTIONS SPECIFIQUES

    /**
     * Récupère les données de l'établissement et du dossier d'instruction pour
     * l'affichage des liens. 
     * 
     * @param type $idx_di L'identifiant du dossier d'instruction
     * 
     * @return mixed
     */
    function getParamsEtablissementDossierInstruction($idx_di){
        //
        $params = [];
        //Si c'est bien un identifiant numérique qui a été fourni
        if (is_numeric($idx_di)){
            $sql = "SELECT etablissement.etablissement as etablissement, 
                CONCAT_WS(' - ',etablissement.code, etablissement.libelle) as etablissement_libelle, 
                dossier_instruction.libelle as dossier_instruction_libelle
            FROM ".DB_PREFIXE."dossier_instruction 
            LEFT JOIN ".DB_PREFIXE."dossier_coordination 
                ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination 
            LEFT JOIN ".DB_PREFIXE."etablissement 
                ON dossier_coordination.etablissement = etablissement.etablissement
            WHERE dossier_instruction.dossier_instruction = ".$idx_di;
            // Exécution de la requête
            $data_link = $this->f->db->query($sql);
            $this->f->addToLog(__METHOD__."(): db->query(\"".$sql."\");", VERBOSE_MODE);
            $this->f->isDatabaseError($data_link);

            //Paramétrage des champs dossier d'instruction et établissement
            $row =& $data_link->fetchRow(DB_FETCHMODE_ASSOC);

            $params['dossier_instruction']['obj'] = "dossier_instruction";
            $params['dossier_instruction']['libelle'] = $row['dossier_instruction_libelle'];
            $params['dossier_instruction']['idx'] = $idx_di;
            
            // Instance de la classe etablissement
            require_once '../obj/etablissement.class.php';
            $etablissement = new etablissement($row['etablissement']);

            // Récupère toutes les données nécessaires de l'établissement
            $values_etablissement = $etablissement->get_data($etablissement->getVal($etablissement->clePrimaire));
            
            // Paramétres envoyés au type link et au type spécifique 
            // etablissement
            $params['etablissement']['idx'] = $etablissement->getVal($etablissement->clePrimaire);
            $params['etablissement']['obj'] = "etablissement_tous";
            $params['etablissement']['libelle'] = $row['etablissement_libelle'];
            $params['etablissement']['values'] = $values_etablissement;
        }
        //
        return $params;
    }
    
    /**
     * Cette fonction vérifie q'un courrier d'annulation a été choisi.
     * 
     * @todo Ajouter la logique
     * 
     * @return boolean  
     */
    function hasCourrierAnnulation(){
        return (is_integer($this->getVal("courrier_annulation")))?true:false;
    }
    
    /**
     * Ajoute un courrier ainsi que le lien vers le contact convoqué.
     *
     * @param intéger La clé primaire du courrier
     */
    function create_courrier_annulation() {
        
        // Fichier requis
        require_once "../obj/courrier.class.php";
        // instanciation du nouveau courrier parent
        $courrier = new courrier("]");
        foreach ($courrier->champs as $value) {
            $val[$value] = "";
        }
        $val["mailing"] = true;
        $val['visite'] = $this->getVal($this->clePrimaire);
        $val['dossier_instruction'] = $this->getVal('dossier_instruction');
        // Récupération de la liste des exploitants
        $id_exploit = $this->get_id_exploit();
        if($id_exploit === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de l'affectation de l'annulation de convocation")."<br/>");
            return false;
        }
        // On ajoute les contacts liés
        $val['contacts_lies'] = $id_exploit;
        // Récupération du modèle d'édition du courrier
        $val['modele_edition'] = $this->get_modele_edition_by_code("ann_convoc_expl");
        // Récupère le type du courrier depuis le modèle d'édition
        $val['courrier_type'] = $this->f->get_courrier_type_by_modele_edition($val['modele_edition']);
        // Ajout de l'a'annulation de la convocation à la table courrier
        if($courrier->ajouter($val, $this->f->db) === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de l'edition de l'annulation de convocation")."<br/>");
            return false;
        }
        //
        $courrier = new courrier($courrier->valF["courrier"]);
        // On finalise le courrier
        if($courrier->finalize()===false){
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la finalisation de l'annulation de convocation")."<br/>");
            return false;
        }
        /**
         * Génération sur le système de stockage de fichiers temporaires du
         * courrier qui vient d'être créé (mailing : tous les courriers enfants
         * concaténés / pas mailing : le courrier unique).
         */
        $list_courrier_child = $courrier->get_list_courrier_child($courrier->getVal($courrier->clePrimaire));
        $tmpfile_uid = $courrier->generate_all_courriers_pdf(
            $list_courrier_child,
            "temporary"
        );
        if ($tmpfile_uid === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la génération du document pdf téléchargeable de la visite."));
            return false;
        }
        // Affichage du lien
        $this->addToMessage(_("Lettre d'annulation à destination des contacts de l'établissement :").
                                " <a class=\"om-prev-icon pdf-16\" target=\"_blank\" href=\"../spg/file.php?mode=temporary&amp;uid=".$tmpfile_uid."\" >".
                                _("visualiser")."</a>");
        //
        return $courrier->getVal($courrier->clePrimaire);
    }

    /**
     * Récupère l'identifiant d'un état de visite à partir de son code
     * 
     * @param   string  $code   Le code de l'état de la visite
     * 
     * @return  integer  L'identifiant de l'état de la visite
     */
    function get_id_etat_visite($code){
        //
        $id_visite_etat = "";
        // Si le code a été fourni
        if (is_string($code)&&$code!=''){
            
            $sql="SELECT visite_etat 
                FROM ".DB_PREFIXE."visite_etat 
                WHERE lower(code)=lower('".$code."')";
            $id_visite_etat = $this->f->db->getOne($sql);
            $this->addToLog("get_id_etat_visite(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        }
        //
        return $id_visite_etat;
    }

    /**
     * Récupère l'avis de passage en réunion.
     *
     * @return integer
     */
    function get_dossier_instruction_reunion_avis() {
        // XXX
        return 1;
    }

    /**
     * Récupère le type du dossier d'instruction.
     *
     * @return integer
     */
    function get_analyses_type() {
        // Déclaration variable résultat
        $analyses_type = null;

        // Instanciation du dossier d'instruction
        $dossier_instruction = $this->get_inst_dossier_instruction();

        // Instanciation de l'analyse
        $analyses_id = $dossier_instruction->getVal('analyses');
        //
        if (!empty($analyse_id)) {
            //
            require_once '../obj/analyses.class.php';
            $analyses = new analyses($analyses_id);

            $analyses_type = $analyses->getVal('analyses_type');
        }

        // Retourne le type du dossier d'instruction
        return $analyses_type;
    }

    /**
     *
     */
    function get_id_visite_etat_by_code($code) {
        //
        $sql = "SELECT visite_etat FROM ".DB_PREFIXE."visite_etat WHERE LOWER(code)=LOWER('".$code."')";
        $id = $this->f->db->getOne($sql);
        $this->addToLog("get_id_visite_etat_by_code(): db->getone(\"".$sql."\");", VERBOSE_MODE);
        if ($this->f->isDatabaseError($id, true)) {
            $this->erreur_db($id->getDebugInfo(), $id->getMessage(), '');
            $this->correct = false;
            return false;
        }
        if( !empty($id) ) {
            return $id;
        } else {
            $this->addToMessage(_("Erreur de parametrage. Contactez votre administrateur."));
            $this->correct = false;
            return false;
        }
    }

    /**
     * Permet de recupérer la liste des identifiants d'exploitants de
     * l'établissement à visiter.
     *
     * @return mixed array() si pas d'erreurs false sinon
     */
    function get_id_exploit() {
        $this->addToLog(__METHOD__."() - start", EXTRA_VERBOSE_MODE);
        // Requête de récupération des exploitants
        $sql = "SELECT contact FROM ".DB_PREFIXE."contact
            JOIN ".DB_PREFIXE."etablissement ON etablissement.etablissement=contact.etablissement
            JOIN ".DB_PREFIXE."dossier_coordination ON dossier_coordination.etablissement=etablissement.etablissement
            JOIN ".DB_PREFIXE."dossier_instruction ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination
            JOIN ".DB_PREFIXE."visite ON visite.dossier_instruction = dossier_instruction.dossier_instruction
            WHERE contact.reception_convocation IS TRUE AND visite = ".$this->getVal($this->clePrimaire);
        // Exécution de la requête
        $res = $this->f->db->query($sql);
        // Si une erreur survient
        if (database::isError($res, true)) {
            // Appel de la methode de recuperation des erreurs
            $this->erreur_db($res->getDebugInfo(), $res->getMessage(), '');
            $this->correct = false;
            return false;
        }
        $id_exploit = array();
        while($contact = $res->fetchRow(DB_FETCHMODE_ASSOC)) {
            $id_exploit[] = $contact["contact"];
        }
        $this->addToLog(__METHOD__."() - end", EXTRA_VERBOSE_MODE);
        return $id_exploit;
    }

    /**
     * [get_modele_edition_by_code description]
     *
     * @param [type] $code [description]
     *
     * @return [type] [description]
     */
    function get_modele_edition_by_code($code) {
        //
        $modele_edition_id = "";
        //
        if (!empty($code)) {
            // Instance de la classe modele_edition
            require_once '../obj/modele_edition.class.php';
            $modele_edition = new modele_edition(0);

            //
            $modele_edition_id = $modele_edition->get_modele_edition_by_code($code);
        }

        //
        return $modele_edition_id;
    }

    /**
     * Récupération du code de l'état de la visite
     * 
     * @param $id_visite_etat L'identifiant du dossier d'instruction
     * @return string         Le code de l'état de la visite
     */
    function getCodeVisiteEtat($id_visite_etat){
        
        $visite_etat_code = "";
        if ($id_visite_etat!=''&&is_numeric($id_visite_etat)){
            //Requête de récupération du code du type de la visite
            $sql = "SELECT code 
                FROM ".DB_PREFIXE."visite_etat 
                WHERE visite_etat = ".$id_visite_etat;
            $visite_etat_code = $this->f->db->getOne($sql);
            $this->addToLog("getCodeVisiteEtat(): db->getone(\"".$sql."\")", VERBOSE_MODE);
            $this->f->isDatabaseError($visite_etat_code);
        }
        return $visite_etat_code;
    }

    /**
     *
     */
    var $merge_fields_to_avoid_obj = array(
        "dossier_instruction",
        "programmation",
        "etablissement",
        "acteur",
    );

    /**
     * Surcharge de la récupération des libellés des champs de fusion.
     * 
     * @return [array]  tableau associatif objet => champ de fusion => libellé
     */
    function get_labels_merge_fields() {
        //
        $labels = parent::get_labels_merge_fields();
        //
        $inst_prog = $this->get_inst_programmation();
        $labels = array_merge(
            $labels,
            $inst_prog->get_merge_fields("labels")
        );
        // Retour de tous les libellés
        return $labels;
    }

    /**
     * Surcharge de la récupération des valeurs des champs de fusion.
     * 
     * @return [array]  tableau associatif champ de fusion => valeur
     */
    function get_values_merge_fields() {
        //
        $values = parent::get_values_merge_fields();
        //
        $inst_prog = $this->get_inst_programmation();
        $values = array_merge(
            $values,
            $inst_prog->get_merge_fields("values")
        );
        // Retour de tous les libellés
        return $values;
    }

    /**
     *
     */
    var $inst_programmation = null;

    /**
     *
     */
    function get_inst_programmation($programmation = null) {
        // Si on ne passe aucun paramètre et que le résultat est déjà
        // stocké alors on retourne le résultat déjà stocké
        if ($programmation == null && $this->inst_programmation != null) {
            return $this->inst_programmation;
        }
        // On récupère l'identifiant de l'élément à instancier
        if ($programmation == null) {
            // Si aucun paramètre n'est fourni depuis l'attribut de l'objet
            $programmation_id = $this->getVal("programmation");
        } else {
            // Si un paramètre est fourni 
            $programmation_id = $programmation;
        }
        // On instancie l'élément 
        require_once "../obj/programmation.class.php";
        $inst_programmation = new programmation($programmation_id);
        // Si aucun paramètre n'est fourni alors on stocke le résultat
        if ($programmation == null) {
            $this->inst_programmation = $inst_programmation;
        }
        // On retourne le résultat
        return $inst_programmation;
    }

    /**
     *
     */
    function get_all_merge_fields($type) {
        //
        $all = array(
            "dossier_instruction",
            "dossier_coordination",
            "etablissement",
        );
        //
        switch ($type) {
            case 'values':
                //
                $values = array();
                //
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem_method = "get_inst_".$value;
                    $elem = $this->$elem_method();
                    if ($elem != null) {
                        $elem_values = $elem->get_merge_fields($type);
                        $values = array_merge($values, $elem_values);
                    }
                }
                return $values;
                break;
            case 'labels':
                //
                $labels = array();
                foreach ($all as $key => $value) {
                    require_once "../obj/".$value.".class.php";
                    $elem = new $value(0);
                    $elem_labels = $elem->get_merge_fields($type);
                    $labels = array_merge($labels, $elem_labels);
                }
                return $labels;
                break;
            default:
                return array();
                break;
        }
    }

    var $inst_dossier_instruction = null;
    var $inst_dossier_coordination = null;
    var $inst_etablissement = null;

    /**
     * 
     */
    function get_inst_dossier_instruction($dossier_instruction = null) {
        //
        if (!is_null($dossier_instruction)) {
            require_once "../obj/dossier_instruction.class.php";
            return new dossier_instruction($dossier_instruction);
        }
        //
        if (is_null($this->inst_dossier_instruction)) {
            //
            $dossier_instruction = $this->getVal("dossier_instruction");
            require_once "../obj/dossier_instruction.class.php";
            $this->inst_dossier_instruction = new dossier_instruction($dossier_instruction);
        }
        //
        return $this->inst_dossier_instruction;
    }

    /**
     * 
     */
    function get_inst_dossier_coordination($dossier_coordination = null) {
        //
        if (!is_null($dossier_coordination)) {
            require_once "../obj/dossier_coordination.class.php";
            return new dossier_coordination($dossier_coordination);
        }
        //
        if (is_null($this->inst_dossier_coordination)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_dossier_coordination = $inst_dossier_instruction->get_inst_dossier_coordination();
        }
        //
        return $this->inst_dossier_coordination;
    }

    /**
     *
     */
    function get_inst_etablissement($etablissement = null) {
        //
        if (!is_null($etablissement)) {
            require_once "../obj/etablissement.class.php";
            return new etablissement($etablissement);
        }
        //
        if (is_null($this->inst_etablissement)) {
            //
            $inst_dossier_instruction = $this->get_inst_dossier_instruction();
            $this->inst_etablissement = $inst_dossier_instruction->get_inst_etablissement();
        }
        //
        return $this->inst_etablissement;
    }


    /**
     * Permet de mettre à jour les dates de suivis de l'établissement.
     *
     * @return boolean
     */
    function update_etablissement_tracking_dates() {
        // Instance de la classe etablissement
        $etablissement = $this->get_inst_etablissement();

        // Si le champ de la classe établissement est vide ou que la date est
        // supérieure à la date de la visite en cours d'ajout
        $etablissement_si_prochaine_visite_date = $etablissement->getVal('si_prochaine_visite_date');
        if (empty($etablissement_si_prochaine_visite_date)
            || strtotime($etablissement->getVal('si_prochaine_visite_date')) > strtotime($this->valF['date_visite'])) {

            // Met à jour la date et le type de la prochaine visite SI
            $update_si_next_visit = $etablissement->update_si_next_visit($this->valF["date_visite"], $this->get_analyses_type());
            //
            if ($update_si_next_visit == false) {
                //
                $this->addToMessage(sprintf(_("La prochaine date de visite de l'etablissement %s ne s'est pas mise a jour."), "<b>".$etablissement->getVal("libelle")."</b>"));
                return false;
            }
        }

        //
        return true;
    }

    /**
     * CONDITION - has_been_validated.
     * 
     * @return boolean
     */
    function has_been_validated() {
        //
        $inst_prog = $this->get_inst_programmation();
        // Si le numéro de version de la programmation lors de création de la
        // visite est strictement supérieur au numéro de la version courant de 
        // la programmation, 
        // => c'est que la visite a été validée
        // Si le numéro de version de la programmation lors de création de la
        // visite est égal au numéro de la version courant de la programmation
        // et que la prorammation es tdans l'état validé (VAL), 
        // => c'est que la visite a été validée
        if ($this->getVal("programmation_version_creation") < $inst_prog->getVal("version")
            || ($this->getVal("programmation_version_creation") == $inst_prog->getVal("version")
                && $inst_prog->get_programmation_etat_code_by_id($inst_prog->getVal("programmation_etat")) == "VAL")) {
            //
            return true;
        }
        //
        return false;
    }

    /**
     * CONDITION - has_not_been_validated.
     * 
     * @return boolean
     */
    function has_not_been_validated() {
        //
        return !$this->has_been_validated();
    }

    /**
     * CONDITION - is_annulable.
     *
     * @return boolean
     */
    function is_annulable() {
        //
        $visite_etat_code = $this->getCodeVisiteEtat($this->getVal('visite_etat'));
        //
        if ($visite_etat_code != "ANN") {
            return true;
        } else {
            return false;
        }
    }

    /**
     * CONDITION - is_from_good_service.
     * 
     * @return boolean
     */
    function is_from_good_service() {
        //
        $inst_di = $this->get_inst_dossier_instruction();
        //
        return $inst_di->is_from_good_service();
    }

    /**
     * CONDITION - is_programmation_in_working_state.
     * 
     * @return boolean
     */
    function is_programmation_in_working_state() {
        //
        $inst_prog = $this->get_inst_programmation();
        //
        return $inst_prog->is_in_working_state();
    }

    /**
     * CONDITION - can_di_status_be_a_poursuivre.
     *
     * Permet d'indiquer si on peut passer le statut du DI à "à poursuivre".
     *
     * @return boolean
     */
    function can_di_status_be_a_poursuivre() {
        //
        $di = $this->get_inst_dossier_instruction();
        //
        return $di->can_di_status_be_a_poursuivre();
    }

    /**
     * TREATMENT - a_poursuivre.
     * 
     * Permet de modifier le statut du DI rattaché à la visite en "à poursuivre"
     *
     * @param array $val Valeurs soumises par le formulaire
     *
     * @return boolean
     */
    function a_poursuivre($val = array()) {
        // Begin
        $this->begin_treatment(__METHOD__);
        // On instancie le DI rattaché
        $inst_di = $this->get_inst_dossier_instruction();
        // Initialisation du tableau de mise à jour du DI
        $valF = array();
        // Modification du statut du DI
        $valF["statut"] = $inst_di->get_status_when_visit_is_planned(true);
        // Mise à jour effective du DI
        $ret = $inst_di->update_autoexecute($valF);
        if ($ret === false) {
            $this->correct = false;
            $this->addToMessage(_("Erreur lors de la mise à jour du dossier d'instruction."));
            return $this->end_treatment(__METHOD__, false);
        }
        //
        $this->addToMessage(_("Le statut du dossier d'instruction")." <strong>".$inst_di->getVal('libelle')."</strong> "._('a ete modifie en "a poursuivre".'));
        // Return
        return $this->end_treatment(__METHOD__, true);
    }

    // {{{ BEGIN - GET INST

    /**
     *
     */
    function get_inst_courrier($courrier = null) {
        return $this->get_inst_common("courrier", $courrier);
    }

    // }}} END - GET INST

}

?>
