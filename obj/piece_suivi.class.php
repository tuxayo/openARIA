<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont
 * suivi et dont la date butoir est dépassée.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/piece.class.php");

class piece_suivi extends piece {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
