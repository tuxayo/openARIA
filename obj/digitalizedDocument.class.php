<?php
/**
 * Ce fichier permet la déclaration de la class DigitalizedDocument
 * conçus pour gérer les fichiers numérisés
 *
 * @package openaria
 * @version SVN : $Id
 */



/**
 * Cette classe permet d'effectuer les traitements sur les documents à
 * incorporer dans l'application
 *
 * Pour importer les données depuis un dossier il faut utiliser run_import
 * dans une boucle qui parcourt le dossier des documents numérisés
 *
 * Pour purger les documents il faut utiliser la fonction run_purge
 * dans une boucle qui parcourt le dossier des documents numérisés
 */
class DigitalizedDocument {

    /**
     * @access static
     * @var string Messages utilisées pour l'écriture dans le log
     */
    var $NO_FILE_EXIST;
    var $DOC_NO_CONFORME;
    var $NO_LINK;
    var $NO_MOVE;
    var $NO_DELETE_FILE;
    var $NO_IMPORT;

    /**
     * Instance de la classe utils
     * @var utils
     */
    var $f = NULL;

    /**
     * Instance du filestorage
     * @var storage
     */
    var $filestorage = NULL;

    /**
     * Nom des fichiers qui ne se sont pas importés
     * @var array
     */
    var $filenameError = array();

    /**
     * Constructeur
     */
    public function __construct($f) {

        //Set des attributs
        $this->NO_FILE_EXIST = _("Le fichier n'existe pas.");
        $this->NO_ETAB_EXIST = _("L'établissement %s n'existe pas.");
        $this->NO_PIECE_TYPE_EXIST = _("Le type de pièce %s n'existe pas.");
        $this->BAD_DATE_FORMAT = _("Le format de date %s n'est pas valide.");
        $this->DOC_NO_CONFORME = _("Le document n'est pas conforme a la regle RG2 : ");
        $this->NO_LINK = _("Le lien entre le document et le dossier d'instruction n'a pu etre etabli.");
        $this->NO_MOVE = _("Le fichier n'a pas pu etre deplace.");
        $this->NO_DELETE_FILE = _("Le fichier n'a pas pu etre supprime : ");
        $this->NO_IMPORT = _("L'importation a été annulee.");

        //
        $this->f = $f;
        // Permet lors de l'instantiation d'objets métiers d'avoir accès à f
        $GLOBALS['f'] = $this->f;

        // initialise le msg attribute qui est utilise pour stocker les
        // messages de retour (reussite ou erreur)
        $this->msg = '';

        //Instance de filestorage
        $this->filestorage = $this->f->storage;

    }

    /**
     * Destructeur
     */
    public function __destruct() {

        //Détruit les instance de utils et filestorage
        unset($this->f);
        unset($this->filestorage);

        //Détruit la variable globale de 'f'
        unset($GLOBALS['f']);

    }

    /**
     * Ajoute une chaine de caracteres dans le log.
     * @param string $message Le message qui doit etre ajouté dans le log.
     * @param mixed $type Le mode de log.
     */
    protected function addToLog($message, $type = DEBUG_MODE) {
        //
        logger::instance()->log(
            "class ".get_class($this)." : "."method ".__METHOD__." - ".$message,
            $type
        );
    }

    /**
     * Cette fonction permet de construire les métadonnées d'un document
     * à partir des informations du nom du fichier.
     * 
     * @param string $filename Nom du fichier
     * 
     * @return array Tableau des metadonnées
     */
    public function extractMetadataFromFilename($filename) {

        // Tableau qui sera retourné en fin de traitement
        $metadata = array();
        // Par défaut le titre est le nom du fichier
        $metadata['title'] = $filename;

        // Si le nom de fichier respecte la convention de nommage
        // <Txxxxx><Type-de-doc-openARIA><JJ-MM-AAAA><.EXT>
        // où <.EXT> est obligatoirement un PDF
        if (preg_match(
                '/^T(\d{5})(.*)(\d{2})-(\d{2})-(\d{4}).pdf$/',
                $filename,
                $matches
            ) === 1) {
            // L'application de la méthode intval() supprime les
            // éventuels 0 préfixant l'ID de l'établissement dans son code.
            $etablissement_id =
                $this->get_etablissement_id('T'.intval($matches[1]));
            if ($etablissement_id === false) {
                $this->addToLog(
                    $filename.' : '.__METHOD__.' '.
                    sprintf($this->NO_ETAB_EXIST, 'T'.intval($matches[1]))
                );
                return null;
            }
            $piece_id = $this->get_piece_type_id($matches[2]);
            if ($piece_id === false) {
                $this->addToLog(
                    $filename.' : '.__METHOD__.' '.
                    sprintf($this->NO_PIECE_TYPE_EXIST, $matches[2])
                );
                return null;
            }

            if (checkdate($matches[4], $matches[3], $matches[5]) === false) {
                $this->addToLog(
                    $filename.' : '.__METHOD__.' '.
                    sprintf(
                        $this->BAD_DATE_FORMAT,
                        $matches[3].'-'.$matches[4].'-'.$matches[5]
                    )
                );
                return null;
            }
            $metadata['title'] = strstr($filename, '.', true);
            $metadata['type'] = $matches[2];
            $metadata['etablissement'] = $etablissement_id;
            $metadata['piece_type'] = $piece_id;
            $metadata['date_creation'] = $matches[5]."-".$matches[4]."-".$matches[3];

        }

        // On retourne les métadonnées
        return $metadata;
    }

    /**
     * Vérification de l'existance d'un code établissement.
     *
     * @param string $codeEtablissement Code de l'établissement à vérifier.
     *
     * @return boolean true si existe false sinon.
     */
    public function get_etablissement_id($codeEtablissement) {
        $sqlEtabId = "SELECT etablissement FROM ".
        DB_PREFIXE."etablissement WHERE code='".$codeEtablissement."'";
        $resEtabId = $this->f->db->getone($sqlEtabId);
        if ($this->f->isDatabaseError($resEtabId, true) === true) {
            $this->addToLog(__METHOD__."(): db->query(\"".$sqlEtabId."\");");
            return false;
        }
        if ($resEtabId !== null) {
            return $resEtabId;
        }
        return false;
    }

    /**
     * Vérification de l'existance d'un type de pièce.
     *
     * @param string $typePiece Type de la pièce à vérifier.
     *
     * @return boolean true si existe false sinon.
     */
    public function get_piece_type_id($typePiece) {
        $sqlPieceTypeId = "SELECT piece_type FROM ".
        DB_PREFIXE."piece_type WHERE code='".$typePiece."'";
        $resPieceTypeId = $this->f->db->getone($sqlPieceTypeId);
        if ($this->f->isDatabaseError($resPieceTypeId, true) === true) {
            $this->addToLog(__METHOD__."(): db->query(\"".$sqlPieceTypeId."\");");
            return false;
        }
        if ($resPieceTypeId !== null) {
            return $resPieceTypeId;
        }
        return false;
    }

    /**
     * Cette fonction permet de récupérer des informations sur le fichier
     * nécessaire pour le filestorage
     * @param  string $path     Chemin du dossier
     * @param  string $filename Nom du fichier
     * @return array           Tableau des métadonnées
     */
    public function extractMetadataToFilestorage($path, $filename) {

        //Test si le fichier existe
        if (!file_exists($path.'/'.$filename)) {
            //
            $this->addToLog($path.'/'.$filename.' : '.$this->NO_FILE_EXIST);
            return null;
        }

        //Tableau qui sera retourné en fin de traitement
        $metadata = array();

        //Métadonnées nécessaire au filestorage
        $metadata["filename"] = $filename;
        $metadata["size"] = filesize($path.'/'.$filename);
        $metadata["mimetype"] = mime_content_type($path.'/'.$filename);

        //Si le tableau est vide on retourne null
        if (count($metadata) === 0) {

            $this->addToLog($this->DOC_NO_CONFORME.$filename);
            return null;

        }

        //Retourne le tableau des métadonnées
        return $metadata;
    }

    /**
     * Cette fonction permet de créer un document dans le filesystem
     * @param string $file_content Contenu du fichier
     * @param array $metadata Métadonnées du fichier
     * @return string $uid identifiant du document dans le filesystem
     */
    public function createFile($file_content, $metadata) {

        //Création du fichier sur le filestorage
        $uid = $this->filestorage->create($file_content, $metadata);

        //Retourne l'identifiant unique du fichier créé
        return $uid;

    }

    /**
     * Cette fonction permet de supprimer un document dans le filesystem
     * @param string $uid L'identifiant unique du dossier
     * @param array $metadata Métadonnées du fichier
     * @return string $uid identifiant du document dans le filesystem
     */
    public function deleteFile($uid) {

        //Création du fichier sur le filestorage
        $uid = $this->filestorage->delete($uid);

        //Retourne l'identifiant unique du fichier créé
        return $uid;

    }

    /**
     * Permet de lier le document importé à l'application et de le créer dans le
     * système de stockage
     * @param  object $piece    Instance de la classe piece
     * @param  string $uid      Identifiant du fichier temporaire
     * @param  string $filename Nom du document
     * @param  string $service  Le code du service
     *
     * @return boolean          Vrai ou faux
     */
    public function createPiece($piece, $uid, $filename, $service, $metadata = array()) {

        //Maj en ajout
        $piece->setParameter("maj",0);

        // On affecte tous les champs par défaut en prévision de l'ajout
        foreach ($piece->champs as $key => $champ) {
            $values[$champ] = '';
        }
        // Données
        $values['piece']='';
        $values['uid'] = $uid;
        $values['nom'] = $filename;
        
        $values['service'] = $this->f->get_service_id($service);
        if (isset($metadata['etablissement']) === true) {
            $values['etablissement'] = $metadata['etablissement'];
        }
        if (isset($metadata['piece_type']) === true) {
            $values['piece_type'] = $metadata['piece_type'];
        }
        if (isset($metadata['date_creation']) === true) {
            $values['om_date_creation'] = $metadata['date_creation'];
        } else {
            $values['om_date_creation'] = date('d/m/Y');
        }

        // On bloque les transactions en base de données
        $piece->f->db->autoCommit(false);
        if( $piece->ajouter($values) ) {
            // Validation des transactions
            $piece->f->db->commit();
            return $piece->valF[$piece->clePrimaire];
        } else {
            // Annulation des transactions
            $piece->undoValidation();
            //Log d'erreur
            $this->addToLog($filename.' : '.$this->NO_LINK);
            $this->addToLog($piece->msg);
            return false;
        }
    }

    /**
     * Permet de déplacer les fichiers créés dans le filestorage vers le dossier
     * des fichiers traités
     * @param  string $pathSrc     Chemin du dossier source
     * @param  string $pathDes     Chemin du dossier de destination
     * @param  string $filename    Nom du fichier
     * @return boolean Retourne true si le fichier à été déplacé sinon false
     */
    public function moveDocumentNumerise($pathSrc, $pathDes, $filename) {

        //Si le dossier de destination n'existe pas, il est créé
        if (!file_exists($pathDes)) {
            mkdir($pathDes);
        }

        //Déplace le document
        $move = rename($pathSrc.'/'.$filename, $pathDes.'/'.$filename);

        //Si le déplacement à réussi
        if ($move) {

            //On retourne TRUE
            return true;
        }

        //Si le deplacement n'est pas fait on retourne false
        $this->addToLog($pathSrc.'/'.$filename.' : '.$this->NO_MOVE);
        return false;

    }

    /**
     * Cette fonction permet de vider un répertoire
     * Si la date d'import du fichier et le nombre de jour ne sont pas renseignés
     * alors les fichiers sont supprimés sans vérification sur la date
     *
     * @param string $file Fichier traité
     * @param date $dateImport Date de l'importation du fichier
     * @param int $nbDay Nombre de jour à soustraite à la date du jour
     *
     * @return boolean true si le traitement à été fait sinon false
     */
    public function purgeFiles($file, $dateImport = null, $nbDay = null) {

        //Si la date et le nombre de jour ne sont pas renseigné
        if (($nbDay == 'null' || $nbDay == null || $nbDay == '')
            || ($dateImport == 'null' || $dateImport == null || $dateImport == '')) {
            //On supprime le fichier sans faire de test
            if (unlink($file)) {
                return true;
            }
        //Si la date d'import et le nombre de jour sont renseignés
        } else {
            //Date d'import dans un format correct pour la comparaison
            $dateImport = new DateTime($dateImport);
            $dateImport = $dateImport->format('Ymd');
            //Date limite pour la suppresion des fichier (Date du jour - 60 jours)
            $dateLimit = date('d-m-Y', strtotime("- $nbDay day", strtotime(date('d-m-Y'))));
            $dateLimit = new DateTime($dateLimit);
            $dateLimit = $dateLimit->format('Ymd');
            //Si la date du fichier à dépassé la date limite
            if ($dateImport <= $dateLimit) {
                //on supprime le fichier
                if (unlink($file)) {
                    return true;
                }
            }
        }
        //Si aucun traitement n'a été fait on retourne false
        $this->addToLog($file.' : '.$this->NO_DELETE_FILE.$file);
        return false;

    }

    /**
     * Cette fonction permet de lancer toutes les fonctions utiles
     * à l'importation des documents scannés.
     * 
     * @param string $pathSrc Le chemin vers le dossier à traiter.
     * @param string $pathDes Le chemin vers le dossier après le traitement.
     * @param string $service Le code du service.
     *
     * @return boolean True si le traitement a été fait sinon false.
     */
    function run_import($pathSrc, $pathDes, $service) {
        // Récupération du nom du répertoire
        $foldername = substr(strrchr($pathSrc, "/"), 1);
        // Liste les documents contenus dans le dossier
        $listFiles = array_diff(scandir($pathSrc), array(".", ".."));
        // Si il n'y a aucun document
        if ($listFiles === null) {
            // On annule l'importation
            $this->addToLog($pathSrc.'/'.$this->NO_IMPORT);
            return false;
        }

        // Pour chaque fichier
        foreach ($listFiles as $key => $filename) {
            // Construit les métadonnées
            $metadata = $this->extractMetadataToFilestorage($pathSrc, $filename);
            
            // S'il n'y a pas de métadonnée
            if ($metadata === null) {
                $this->filenameError[] = $filename;
                continue;
            }
            // Extrait les informations du nom du document
            $metadataFromFilename = $this->extractMetadataFromFilename($filename);
            // S'il n'y a pas de métadonnée
            if ($metadataFromFilename === null) {
                $this->filenameError[] = $filename;
                continue;
            }
            if (isset($metadataFromFilename["type"]) === true) {
                $metadata["type"] = $metadataFromFilename["type"];
            }
            // Si le document n'est pas un PDF
            if (isset($metadata['mimetype']) === true and
                    $metadata['mimetype']!=='application/pdf'
                ) {
                // On annule l'importation
                $this->addToLog($pathSrc.$filename.' : '.$this->NO_IMPORT);
                $this->filenameError[] = $filename;
                continue;
            }
            // Recupère le contenu du fichier
            $file_content = file_get_contents($pathSrc.'/'.$filename);
            // Créer le fichier
            $uid = $this->createFile($file_content, $metadata);
            // Si le fichier est créé
            if ($uid !== null) {
                // Instancie la class piece
                require_once "../obj/piece.class.php";
                $piece = new piece("]");
                // Valeur retour formulaire
                $piece->retourformulaire = "dossier_instruction";
                // Créer le document sur le système de stockage et dans la
                // table piece
                $createPiece = $this->createPiece(
                    $piece,
                    $uid,
                    $filename,
                    $service,
                    $metadataFromFilename
                );

                // Si le document est crée sur le filestorage
                if ($createPiece !== false && $uid !== 'OP_FAILURE') {
                    // On déplace le document créé dans le filestorage
                    // du dossier des "à traiter" vers celui des "traités".
                    $this->moveDocumentNumerise($pathSrc, $pathDes, $filename);
                } else {
                    // On annule l'importation.
                    $this->addToLog($pathSrc.' : '.$this->NO_IMPORT);
                    $this->filenameError[] = $filename;
                    // On supprime le fichier créé.
                    if ($uid !== $this->deleteFile($uid)) {
                        $this->addToLog($pathSrc.$filename.' : '.$this->NO_DELETE_FILE);
                    }
                }
            }
        }//Fin foreach
        //Retourne true
        return true;
    }

    /**
     * Cette fonction permet de lancer toutes les fonctions utiles à la purge
     * des documents numérisés
     * @param string $path Le chemin vers le dossier
     * @param int $nbDay Nombre de jour à soustraite à la date du jour
     * @return boolean true si le traitement à été fait sinon false
     */
    function run_purge($path, $nbDay = null) {
        //Liste les documents contenus dans le dossier
        $listFiles = array_diff(scandir($path), array(".", "..") );
        // Si le dossier contient des documents
        if ($listFiles !== null) {
            //Parcours la liste des fichiers
            foreach ($listFiles as $key => $filename) {
                //Fichier
                $file = $path.$filename;
                //Si le nombre de jour est renseigné
                if ($nbDay !== null) {
                    //il faut renseigner la date d'import du fichier
                    $dateImport = date("Y-m-d", filemtime($file));
                } else {
                    //Sinon la date d'import est null
                    $dateImport = null;
                }
                //S'il n'y pas d'erreur on exécute la fonction purgeFiles
                $purgeFiles = $this->purgeFiles($file, $dateImport, $nbDay);
            }
        }
        //Si il n'y a pas d'erreur on retourne true
        return true;
    }

}

?>
