<?php
/**
 * @package openaria
 * @version SVN : $Id: acteur.inc.php 386 2014-09-26 08:14:36Z fmichon $
 */

//
require_once "../gen/obj/lien_reunion_r_instance_r_i_membre.class.php";

class lien_reunion_r_instance_r_i_membre extends lien_reunion_r_instance_r_i_membre_gen {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        $this->constructeur($id);
    }

    /**
     * Définition des actions disponibles sur la classe.
     *
     * @return void
     */
    function init_class_actions() {

        // On récupère les actions génériques définies dans la méthode 
        // d'initialisation de la classe parente
        parent::init_class_actions();

        // ACTION - 002 - supprimer
        // Suppression de la possibilité de supprimer
        $this->class_actions[2] = null;
    }

    /**
     * Permet de définir le type des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     *
     * @return void
     */
    function setType(&$form, $maj) {
        //
        parent::setType($form, $maj);
        // Si contexte réunion
        if ($this->getParameter("retourformulaire") == "reunion") {
            // tous les modes
            $form->setType('lien_reunion_r_instance_r_i_membre', 'hidden');
            $form->setType('reunion', 'selecthidden');

            // mode modifier
            if ($maj == 1) {
                $form->setType('reunion_instance', 'selecthiddenstatic');
            }// fin modifier
        }
    }

    /**
     * Permet de définir le libellé des champs.
     *
     * @param object  &$form Instance du formulaire
     * @param integer $maj   Mode du formulaire
     */
    function setLib(&$form,$maj) {
        parent::setLib($form,$maj);
        // Si contexte réunion
        if ($this->getParameter("retourformulaire") == "reunion") {
            $form->setLib('reunion_instance', _("instance"));
            $form->setLib('reunion_instance_membre', _("membre"));
        }
    }

    /**
     * Permet de définir l’attribut “onchange” sur chaque champ.
     * 
     * @param object $form Formumaire
     * @param int    $maj  Mode d'insertion
     */
    function setOnchange(&$form,$maj) {
        parent::setOnchange($form,$maj);

        // Si contexte réunion
        if ($this->getParameter("retourformulaire") == "reunion") {
            // Filtre des membres à la sélection d'une instance
            $form->setOnchange('reunion_instance',
                'filterSelect(this.value,
                \'reunion_instance_membre\',
                \'reunion_instance\',
                \'lien_reunion_r_instance_r_i_membre\');');
        }
    }

    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {
        parent::setSelect($form, $this->getParameter("maj"));

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // Récupération de l'instance
        $instance = "";
        $champ = "reunion_instance";
        if (isset($_POST[$champ])) {
            $instance = $_POST[$champ];
        } elseif($this->getParameter($champ) != "") {
            $instance = $this->getParameter($champ);
        } elseif(isset($form->val[$champ])) {
            $instance = $form->val[$champ];
        } elseif($this->getVal($champ) != null) {
            $instance = $this->getVal($champ);
        }
        if ($instance == "") {
            $instance = -1;
        }

        // Filtre des membres par instance
        $sql_membre = str_replace('<idx_instance>', $instance, $sql_membre);
        $sql_membre_by_id = str_replace('<idx_instance>', $instance, $sql_membre_by_id);
        $this->init_select($form, $this->f->db, $this->getParameter("maj"), null, "reunion_instance_membre", $sql_membre, $sql_membre_by_id, true);
    }

    /**
     * Permet de modifier le fil d'Ariane depuis l'objet pour un sous-formulaire
     * @param string    $subEnt Fil d'Ariane récupéréré 
     * @return                  Fil d'Ariane
     */
    function getSubFormTitle($subEnt) {
        // Si contexte réunion
        if ($this->getParameter("retourformulaire") == "reunion") {
            $subEnt = str_replace("application", _("reunion"), $subEnt);
            $subEnt = str_replace("lien_reunion_r_instance_r_i_membre", _("signataires"), $subEnt);
            return $subEnt;
        }
        // Sinon méthode parente
        parent::getSubFormTitle($subEnt);
    }
}

?>
