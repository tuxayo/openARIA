<?php
/**
 * Surcharge de la classe courrier pour afficher seulement les pièces qui sont
 * en attente de retour AR.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once ("../obj/courrier.class.php");

class courrier_attente_retour_ar extends courrier {

    function __construct($id, &$dnu1 = null, $dnu2 = null) {
        parent::__construct($id);
    }

}// fin classe

?>
