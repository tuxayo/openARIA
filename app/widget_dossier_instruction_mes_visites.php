<?php
/**
 * WIDGET DASHBOARD - widget_dossier_instruction_mes_visites.
 * 
 * ...
 *
 * L'objectif de ce widget est de ...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
$template_table = '
<table class="table table-condensed table-bordered table-striped table-hover">
    <thead>%s
    </thead>
    <tbody>%s
    </tbody>
</table>
';
//
$template_table_head = '
        <tr>
            <th>
                <span title="%s" class="info-16"><!-- --></span>
            </th>
            <th>%s</th>
            <th>%s</th>
            <th>%s</th>
        </tr>
';
//
$template_table_line = '
        <tr>
            <td class="icons">%s</td>
            <td>%s</td>
            <td>%s</td>
            <td>%s</td>
        </tr>
';
$template_table_line_empty = '
        <tr>
            <td colspan="4">%s</td>
        </tr>
';
// Lien vers le dossier de coordination
$template_table_link = '
<a class="lienTable" 
   href="form.php?obj=dossier_instruction_mes_visites&amp;action=3&amp;idx=%s&amp;idz=%s"
>%s</a>';
// Bouton consulter
$icon_consulter = '
<span class="om-icon om-icon-16 om-icon-fix consult-16">-></span>';
//
$template_link = '
<a href="%s">%s</a>';
//
$template_panel = '
<div class="panel panel-box">
    <div class="list-justified-container">
        <ul class="list-justified text-center">
            %s
            %s
        </ul>
    </div>
</div>
';
//
$template_panel_main_elem = '
            <li>
                <span class="size-h3 box-icon rounded bg-danger">%s</span>
                <p class="text-muted">%s</p>
            </li>';
//
$template_panel_other_elem = '
            <li>
                <p class="size-h3">%s</p>
                <p class="text-muted">%s</p>
            </li>';

/**
 *
 */
//
if ($f->isAccredited(
        array("dossier_instruction_mes_visites", "dossier_instruction_mes_visites_tab"),
        "OR"
    )) {

    /**
     * Listing des cinq plus anciens dossiers d'instruction de visite de l'utilisateur connecté
     */
    // Requête SQL
    $sql = "
    SELECT
        dossier_instruction.dossier_instruction,
        dossier_instruction.libelle as libelle,
        CONCAT(etablissement.code,' - ',etablissement.libelle) as etablissement,
        to_char(dossier_coordination.date_demande,'DD/MM/YYYY')  as date_demande
    FROM ".DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type
        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
    LEFT JOIN ".DB_PREFIXE."acteur
        ON dossier_instruction.technicien = acteur.acteur
    LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur
    LEFT JOIN ".DB_PREFIXE."etablissement
        ON dossier_coordination.etablissement = etablissement.etablissement
    WHERE
        om_utilisateur.login = '".$_SESSION["login"]."'
        AND dossier_type.code = 'VISIT'
    ORDER BY dossier_coordination.date_demande ASC
    LIMIT 5
    ";
    // Exécution de la requête
    $res = $f->db->query($sql);
    // Logger
    $f->addToLog("app/widget_dossier_instruction_mes_visites.php: db->query(\"".$sql."\");", VERBOSE_MODE);
    // Vérification d'une éventuelle erreur de base de données
    $f->isDatabaseError($res);
    // Récupération des enregistrements
    $mes_di_encours = array();
    while ($row =& $res->fetchrow(DB_FETCHMODE_ASSOC)) {
        $mes_di_encours[] = $row;
    }

    /**
     * Statistiques sur le nombre de dossiers d'instruction
     */
    // Requête SQL
    $sql = "
    SELECT
        count(dossier_instruction.dossier_instruction)
    FROM ".DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type
        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
    LEFT JOIN ".DB_PREFIXE."acteur
        ON dossier_instruction.technicien = acteur.acteur
    LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur
    LEFT JOIN ".DB_PREFIXE."etablissement
        ON dossier_coordination.etablissement = etablissement.etablissement
    WHERE
        om_utilisateur.login = '".$_SESSION["login"]."'
        AND dossier_type.code = 'VISIT' 
    ";

    // Conditions pour les DI en cours
    $sql_encours = $sql." AND dossier_instruction.dossier_cloture IS FALSE
        AND dossier_instruction.a_qualifier IS FALSE ";
    // Exécution de la requête
    $res_encours = $f->db->getone($sql_encours);
    // Logger
    $f->addToLog("app/widget_dossier_instruction_mes_visites.php: db->getone(\"".$sql_encours."\");", VERBOSE_MODE);
    // Vérification d'une éventuelle erreur de base de données
    $f->isDatabaseError($res_encours);

    // Conditions pour les DI à qualifier
    $sql_a_qualifier = $sql." AND dossier_instruction.a_qualifier IS TRUE ";
    // Exécution de la requête
    $res_a_qualifier = $f->db->getone($sql_a_qualifier);
    // Logger
    $f->addToLog("app/widget_dossier_instruction_mes_visites.php: db->getone(\"".$sql_a_qualifier."\");", VERBOSE_MODE);
    // Vérification d'une éventuelle erreur de base de données
    $f->isDatabaseError($res_a_qualifier);

    // Conditions pour les DI en cours
    $sql_cloture = $sql." AND dossier_instruction.dossier_cloture IS TRUE ";
    // Exécution de la requête
    $res_cloture = $f->db->getone($sql_cloture);
    // Logger
    $f->addToLog("app/widget_dossier_instruction_mes_visites.php: db->getone(\"".$sql_cloture."\");", VERBOSE_MODE);
    // Vérification d'une éventuelle erreur de base de données
    $f->isDatabaseError($res_cloture);

    // Les trois chiffres clés du widget
    $main_elem = array(
        "en_cours" => array(
            "count" => $res_encours,
            "libelle" => "DI "._("visites en cours"),
        ),
        "a_qualifier" => array(
            "count" => $res_a_qualifier,
            "libelle" => "DI "._("visites a qualifier"),
        ),
        "cloture" => array(
            "count" => $res_cloture,
            "libelle" => "DI "._("visites clotures"),
        ),
    );
    //
    $others_elems = array();

    /**
     *
     */
    //
    $panel_main_elem = sprintf(
        $template_panel_main_elem,
        $main_elem["en_cours"]["count"],
        $main_elem["en_cours"]["libelle"]
    );
    //
    $panel_main_elem .= sprintf(
        $template_panel_main_elem,
        $main_elem["a_qualifier"]["count"],
        $main_elem["a_qualifier"]["libelle"]
    );
    //
    $panel_main_elem .= sprintf(
        $template_panel_main_elem,
        $main_elem["cloture"]["count"],
        $main_elem["cloture"]["libelle"]
    );
    //
    $panel_others_elems = "";
    foreach ($others_elems as $key => $value) {
        $panel_others_elems .= sprintf(
            $template_panel_other_elem,
            $value["count"],
            $value["libelle"]
        );
    }
    //
    $panel = sprintf(
        $template_panel,
        $panel_main_elem,
        $panel_others_elems
    );

    //
    $table_head = sprintf(
        //
        $template_table_head,
        //
        _("Mes 5 plus anciens dossiers visites"),
        _("DI"),
        _("Etablissement"),
        _("Demande")
    );
    //
    $table_body = "";
    //
    foreach ($mes_di_encours as $key => $value) {
        //
        $table_body .= sprintf(
            //
            $template_table_line,
            //
            sprintf(
                $template_table_link, 
                $value["dossier_instruction"], 
                $value["libelle"], 
                $icon_consulter
            ),
            //
            sprintf(
                $template_table_link, 
                $value["dossier_instruction"], 
                $value["libelle"],
                $value["libelle"]
            ),
            //
            sprintf(
                $template_table_link, 
                $value["dossier_instruction"], 
                $value["libelle"],
                $value["etablissement"]
            ),
            //
            sprintf(
                $template_table_link, 
                $value["dossier_instruction"], 
                $value["libelle"],
                $value["date_demande"]
            )
        );
    }
    //
    if (count($mes_di_encours) == 0) {
        $table_body .= sprintf(
            $template_table_line_empty,
            _("Aucun dossier visite en cours.")
        );
    } else {
        $table_body .= sprintf(
            $template_table_line_empty,
            sprintf(
                $template_link,
                "../scr/tab.php?obj=dossier_instruction_mes_visites",
                $icon_consulter." "._("Voir tous mes dossiers visites")
            )
        );
    }
    //
    $table = sprintf(
        $template_table,
        $table_head,
        $table_body
    );


    /**
     *
     */
    //
    echo $panel;
    echo $table;

}

?>
