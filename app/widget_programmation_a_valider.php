<?php
/**
 * WIDGET DASHBOARD - widget_programmation_a_valider.
 * 
 * Établissements NPAI.
 *
 * L'objet de ce widget est de permettre au cadre de visualiser les 
 * programmations à valider.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("programmation", "programmation_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_programmations_a_valider();

} else {

    //
    $widget_is_empty = true;

}

?>
