<?php
/**
 * WIDGET DASHBOARD - widget_message_mes_non_lu.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
if ($f->isAccredited(array("dossier_coordination_message_mes_non_lu", "dossier_coordination_message_mes_non_lu_tab"), "OR")) {
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    $widget_is_empty = $p->view_widget_message_mes_non_lu();
} else {
    $widget_is_empty = true;
}

?>
