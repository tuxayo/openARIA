<?php
/**
* Ce script permet de recuperer les options d'un select filtré par rapport à un autre select
*
* @package openads
* @version SVN : $Id$
*/

//
require_once "../obj/utils.class.php";

// Données pour le champ visé
(isset($_GET['idx']) ? $idx = $_GET['idx'] : $idx = "");
// Table visée pour la requête
(isset($_GET['tableName']) ? $tableName = $_GET['tableName'] : $tableName = "");
// Champs visé pour le tri
(isset($_GET['linkedField']) ? $linkedField = $_GET['linkedField'] : $linkedField = "");
// Formulaire visé
(isset($_GET['formCible']) ? $formCible = $_GET['formCible'] : $formCible = "");

// Le formulaire visé doit être renseigné
if ($formCible != '') {

    $f = new utils("nohtml");
    $f->disableLog();

    require_once PATH_OPENMAIRIE."om_formulaire.class.php";
    $champ = array($linkedField);
    $form = new formulaire(NULL, 0, 0, $champ);

    // Creation d'un objet vide pour pouvoir créer facilement les champs de
    // type select 
    require_once "../obj/$formCible.class.php";
    $object = new $formCible("]", $f->db, 0);
    $object->setParameter($linkedField, $idx);
    $object->setSelect($form, 0, $f->db, false);

    //
    echo json_encode($form->select[$tableName]);

}

?>