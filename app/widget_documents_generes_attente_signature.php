<?php
/**
 * WIDGET DASHBOARD - widget_documents_generes_attente_signature.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("courrier_attente_signature", "courrier_attente_signature_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_documents_generes_attente_signature();

} else {

    //
    $widget_is_empty = true;

}

?>
