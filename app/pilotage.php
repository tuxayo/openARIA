<?php
/**
 * Ce script permet d'afficher les statistiques sous forme de widgets.
 *
 * @package openmairie_exemple
 * @version SVN : $Id: dashboard.php 2924 2014-10-10 14:49:17Z fmichon $
 */

require_once "../obj/utils.class.php";
$f = new utils("nohtml");
//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
// appel à la vue principale
$p->view_main();

?>
