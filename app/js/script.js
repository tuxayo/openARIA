/**
 * Script JS spécifique à l'applicatif, ce script est destiné à être
 * appelé en dernier dans la pile des fichiers JS.
 *
 * @package openaria
 * @version SVN : $Id:
 */

/**
 *
 */
var msg_alert_error_check_visiting_hours = "L'heure de fin doit être supérieure à l'heure de début.";

/**
 *
 */
$(function(){

    /**
     * Gère le comportement de la page settings (administration et paramétrage).
     */
    handle_settings_page();

    /**
    * Spécifique Documents Générés SUivi Des Dates pour donner le focus sur le champ
    * code barres au chargement de la page.
    */
    $('#courrier_suivi_par_code_barres_form #code_barres').focus();
    
    /**
    * Spécifique Documents Générés Edition RAR pour donner le focus sur le champ
    * liste des codes barres au chargement de la page.
    */
    $('#suivi_envoi_lettre_rar_form #liste_code_barres_instruction').focus();


    // Bind de la fonction permettant l'ajout des contacts
    $("#formulaire").on("click","#add_contact", function(event) {
        popupIt(0, 'contact',
            '../scr/form.php?obj=contact', 'auto', 'auto', afficherContact);
    });

    // Bind la fonction permettant l'ajout d'autorité de police
    $("#formulaire").on("click","#add_autorite_police", function(event) {
        // Identifiant de la demande de passage en reunion
        var dossier_instruction_reunion = $("#passage_en_reunion #dossier_instruction_reunion-id").text();
        //var dossier_instruction_reunion = $("#sformulaire #dossier_instruction_reunion").text();
        // Formulaire d'ajout en overlay
        popupIt_sf('autorite_police', '../scr/sousform.php?obj=autorite_police&action=0&retourformulaire=dossier_instruction_reunion&idxformulaire='+dossier_instruction_reunion, 'auto', 'auto', display_synthesis_autorite_police);
    });

    // Bind la fonction permettant l'ajout de demande de passage en reunion
    $("#formulaire").on("click","#add_dossier_instruction_reunion", function(event) {
        // Identifiant de la demande de passage en reunion
        var dossier_instruction = $("#passage_en_reunion #dossier_instruction-id").text();
        //var dossier_instruction = $("#formulaire #dossier_instruction").val();
        // Formulaire d'ajout en overlay
        popupIt_sf('dossier_instruction_reunion', '../scr/sousform.php?obj=dossier_instruction_reunion&action=0&retourformulaire=dossier_instruction&idxformulaire='+dossier_instruction, 'auto', 'auto', view_dossier_instruction_reunion_synthesis);
    });

    // Datatable sur le tableau du bloc de proposition
    if ($("#tableau_propositions").length) {
        // Gestion filtre établissements proches, désactivé par défaut
        if ($("div.bloc_filtre_visite_etablissement").length) {
            toggle_widgets_proches();
            $("#btn_tous").prop('disabled', true);
            $("#btn_tous").button("option", "disabled", true);
        }
        // Ajoute le champ de recherche sur chaque colonne
        $('#tableau_propositions tr th.search_tab_cell span').each( function () {
            var title = $(this).text();
            $(this).html( '<input class="search_tab_input" type="text" placeholder="'+title+'" />' );
        } );

        // Identifiant de la semaine de programmation
        var idx = $("#bloc_tableau input#programmation").val();

        // Nombre de ligne du tab
        var nb_row = 0;

        // Loading
        display_msg_programmation(msg_loading, 'info');

        // DataTable
        var table = $('#tableau_propositions').DataTable({
            autoWidth: false,
            // Récupère les données en ajax
            ajax: { 
                url : "../scr/form.php?obj=programmation&action=14&idx="+idx,
                dataSrc: function (json) {
                    if (json.status === 'OK'){
                        display_msg_programmation('', 'info');
                        return json.result;
                    } else{
                        display_msg_programmation(json.message, 'error');
                        return '';
                    }
                },
            },
            // Définit les colonnes du tableau
            columns: [
                {data: "dossier_instruction_libelle", className: "dossier_instruction_libelle"},
                {data: "etablissement_libelle", className: "etablissement_libelle"},
                {data: "adresse", className: "adresse"},
                {data: "code_postal", className: "code_postal"},
                {data: "etablissement_type_libelle", className: "etablissement_type_libelle"},
                {data: "etablissement_categorie_libelle", className: "etablissement_categorie_libelle"},
                {data: "date_demande", className: "date_demande"},
                {data: "dernier_avis", className: "dernier_avis"},
                {data: "date_dernier_avis", className: "date_dernier_avis"},
                {data: "acteur_derniere_visite", className: "acteur_derniere_visite"},
                {data: "si_prochaine_visite_periodique_date_previsionnelle", className: "si_prochaine_visite_periodique_date_previsionnelle"},
                {data: "type_visite", className: "type_visite"},
                {data: "a_poursuivre", className: "a_poursuivre"},
                {data: "locaux_a_sommeil", className: "locaux_a_sommeil"},
                {data: "retard", className: "retard"},
                {data: "prioritaire", className: "prioritaire"},
                {data: "dossier_instruction_id", className: "dossier_instruction_id"}
            ],
            // Fonctions lancé après chaque création de ligne
            rowCallback: function( row, data ) {
                $(row).attr('id', "ligne_"+nb_row++);
                $(row).draggable({
                    helper: "clone"
                });
                // Si le selecteur des acteurs est vide
                if ($("select#acteur").val() === "") {
                    $(row).draggable("disable");
                    $(row).removeClass("ui-state-disabled");
                }
            },
            // Pas de trie à l'initialisation, permet de garder le order by
            // de la requête
            order: [],
            // Supprime l'option à l'utilisateur et bloque le nombre
            // d'enregistrement à 5 par page.
            lengthChange: false,
            pageLength: 5,
            // Traductions de datatables
            language: {
                "zeroRecords": "Aucun enregistrement.",
                "info": "_START_ - _END_ enregistrement(s) sur _TOTAL_",
                "paginate": {
                    "first": "Premier",
                    "last": "Dernier",
                    "next": "Suivant",
                    "previous": "Précédent"
                },
                "infoFiltered": "(filtré(s) sur un total de _MAX_ enregistrement(s))",
                "infoEmpty": "0 - 0 enregistrement(s) sur 0",
                "aria": {
                    "sortAscending": ": activé pour trier par ordre croissant",
                    "sortDescending": ": activé pour trier par ordre décroissant"
                },
                "processing": "Veuillez patienter...",
            },
            // Déplace les élements du DOM
            dom: '<"top"ipf>rt<"bottom"><"clear">'
        });

        // Applique la recherche
        table.columns().eq( 0 ).each( function ( colIdx ) {
            $( 'input', table.column( colIdx ).footer() ).on( 'keyup change', function () {
                table
                    .column( colIdx )
                    .search( this.value )
                    .draw();
            } );
        } );

        // Retire le champ de recherche global
        $('#tableau_propositions_filter').remove();

        // Ajoute au DOM pour afficher les informations
        $('#tableau_propositions_info').wrap('<div class="tab-pagination ui-state-default ui-corner-top ui-tabs-selected ui-state-active datatable_pagination_nb"></div>');
        $('#tableau_propositions_info').wrap('<div class="pagination-nb"></div>');
        $('#tableau_propositions_info').addClass('pagination-text');
    }

    // Cette méthode permet de gérer le checkall/uncheckall
    // dans les vues de planification de dossiers en réunion
    $("#dir-checkall").click(function () {
        $(".dir-checkbox").prop('checked', $(this).prop('checked'));
    });

    // On raffraichit l'agenda au clic sur l'onglet principal de l'écran de la programmation
    // Cet appel est présent pour gérer le cas d'utilisation suivant : on affiche une programmation
    // puis on modifie une de ses visites depuis l'onglet "Visites" de cette programmation
    // Au retour sur la vue de la programmation, on veut que les modifications apportées soient 
    // visibles sur l'agenda. Le clic sur l'onglet "Programmation" est obligatoire.
    $("#main:contains('programmation')").click(function () {
        handle_programmation_technicien();
    });

});


/**
 * Fonction appelée à chaque chargement de page
 * et de formulaire AJAX
 */ 
function app_initialize_content() {

    // Programmation des visites, le lien retour lorsqu'il est sensé retourner
    // vers le listing ferme l'overlay de visualisation de la visite sinon il
    // a un comportement normal
    $("#sousform-visite.overlay a.retour-tab").off('click').on('click', function(event) {
        $('#sousform-visite.overlay').dialog('close').remove();
        return false;
    });
    // Autorité de police, le lien retour lorsqu'il est sensé retourner
    // vers le listing ferme l'overlay de visualisation de l'autorité de police
    // sinon il a un comportement normal
    $("#sousform-autorite_police.overlay a.retour-tab").off('click').on('click', function(event) {
        $('#sousform-autorite_police.overlay').dialog('close').remove();
        return false;
    });

    //Gestion de la commission
    $( "#tabs" ).tabs();

    // On formate le champ des références cadastrales uniquement si le champ 
    // est paramétré en tenant compte du conteneur
    if ($('#form-etablissement_tous-overlay .field-type-referencescadastrales').length) {
        formatFieldReferenceCadastrale('#form-etablissement_tous-overlay ');
    } else if ($('#sousform-container .field-type-referencescadastrales').length) {
        formatFieldReferenceCadastrale('#sousform-container ');
    } else if ($('#form-container .field-type-referencescadastrales').length) {
        formatFieldReferenceCadastrale('#form-container ');
    }

    // Récupération de l'URL
    url = document.location + "" ;

    // Si objet établissement
    if (url.indexOf("form.php?obj=etablissement_referentiel_erp&") != -1 ||
        url.indexOf("form.php?obj=etablissement_tous&") != -1) {

        // A la validation du formulaire
        $(document).on('submit','form',function(){
        
            // On active les champs adresse de l'exploitant
            // pour qu'ils soient postés
            activerAdresseExploitant();
        });   
    }

    // Dés que le champ du formulaire piece apparait
    if ($("select#choix_lien").length) {
        // Affiche ou cache les champs du formulaire piece
        show_hide_fields_by_piece_choix_lien($("select#choix_lien").val());
    }

    // Gestion des particuliers et personnes morales
    if ($("#qualite").length > 0) {
        verifierContactType();
    }

    // Gestion des contacts institutionnels
    if ($("select#contact_type").length || $("input#contact_type").length) {
        change_form_contact_type($("#contact_type").val());
        $("select#contact_type").change(function() {
            change_form_contact_type($(this).val());
        });
    }

    afficherBoutonAjoutContact();

    // Gestion de l'agenda des programmations
    // La classe already-fc est un marqueur qui permet d'identifier que le calendrier
    // a déjà été initialiser et qu'il est inutile de le refaire à chaque appel
    // de app_initialize_content inutilement
    if ($("#bloc_agenda .agenda_calendrier").length && !$("#bloc_agenda .already-fc").length) {
        var id = $(".agenda_calendrier").attr("id").replace("agenda_calendrier_","");
        creer_calendrier(id);
    }

    // Gestion de l'affichage des autorités de police liées sur
    // le formulaire du courrier
    if ($(".field-type-select_multiple select#autorites_police_liees").length) {
        // Affiche ou cache le champ
        display_autorites_police_liees();
    }

    // XXX
    // Problématique de l'appel dans un overlay des scripts en sousform
    // qui utilisent des appels javascript dans les attributs HTML
    // Le problème doit être réglé de manière globale dans le framework
    $('#sousform-dossier_instruction_reunion.overlay a.retour').each(function() {
        $(this).removeAttr("onclick");
    });

    // Si le select etablissement_nature est sur la page
    if ($("select#etablissement_nature").length) {
        // Affiche ou non les champs de visite prériodique
        masquerPeriodiciteVisites();
    }

    // Si le formulaire d'ajout d'un DC et son champ de sélection de type de DC
    // apparaîssent sur la page.
    if ($("#formulaire div.form-dossier_coordination-action-0 select#dossier_coordination_type").length > 0) {
        // Change le libellé de l'établissement
        etablissement_field_is_require($("select#dossier_coordination_type").val());
    }

    // Gestion des sous-onglets de la gestion des UA
    var $uatabs = $("#ua-tabs").tabs({
        select: function(event, ui) {
            // Suppression du contenu de l'onglet precedemment selectionne
            // #ui-tabs-X correspond uniquement aux ids des onglets charges
            // dynamiquement
            offsetTabIndex = parseInt($("#ua-tabs div.ui-tabs-panel:first").attr('id').replace("ui-tabs-", ""));
            selectedTabIndex = parseInt($uatabs.tabs('option', 'selected'));
            $( "#ui-tabs-"+(selectedTabIndex+offsetTabIndex)+"").empty()
            return true;
        }
    });

    // Modifie l'action des bouton de retour du sous-formulaire d'ajout de
    // contact
    $('#plot_owner_add_form_contact a.retour').attr('onclick', 'show_hide_plot_owner_add_form_contact("hide")');

    // Si le div contenant le formulaire d'ajout des contacts est vide, il est
    // caché
    if ($.trim($("div#plot_owner_add_form_contact").html()).length === 0) {
        //
        $("div#plot_owner_add_form_contact").hide();
    }

    // Gestion des sous-onglets SWROD Listing des pièces du guichet unique
    var $documententranttabs = $("#document-entrant-tabs").tabs({
        select: function(event, ui) {
            // Suppression du contenu de l'onglet precedemment selectionne
            // #ui-tabs-X correspond uniquement aux ids des onglets charges
            // dynamiquement
            offsetTabIndex = parseInt($("#document-entrant-tabs div.ui-tabs-panel:first").attr('id').replace("ui-tabs-", ""));
            selectedTabIndex = parseInt($documententranttabs.tabs('option', 'selected'));
            $( "#ui-tabs-"+(selectedTabIndex+offsetTabIndex)+"").empty()
            return true;
        }
    });

    // Aide à la saisie dans le formulaire du paramétrage
    // d'un type d'autorité de police ou décision d'autorité de police
    if ($("div.form_autorite_police_decision_content_bloc").length) {
        handle_filter_select_nomenclature_actes();
        handle_ap_decision_type_arrete();
    }

}

/**
 * Gère le comportement de la page settings (administration et paramétrage) :
 *  - autofocus sur le champ de recherche
 *  - comportement du livesearch
 */
 function handle_settings_page() {
    // Auto focus sur le champ de recherche au chargement de la page
    $('#settings-live-search #filter').focus();
    // Live search dans le contenu de la page settings
    $("#settings-live-search #filter").keyup(function(){
        var filter = $(this).val(), count = 0;
        $("div.list-group a").each(function(){
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
    });
}

/**
 * Cette fonction appelée juste avant le affichesform dans un submit de formulaire
 * permet de remplacer le bouton de soumission par un spinner.
 */
function replace_sousform_formcontrols_with_spinner(elem) {
    $('#sousform-'+elem+' div.formControls').html(msg_loading);
}

/**
 * Vérifie si le champ établissement est obligatoire.
 *
 * @param intger dossier_instruction_type Identifiant du type du DC
 *
 * @return void
 */
function etablissement_field_is_require(dossier_instruction_type) {
    // lien script php
    link = "../scr/form.php?obj=dossier_coordination&action=7&idx=0";
    //
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "selected="+dossier_instruction_type+"&",
        dataType: "json",
        success: function(json) {
            //
            display_etablissement_field_is_require(json);
        }
    });
}

/**
 * Modifie l'affichage du libellé du champ établissement.
 *
 * @param array json Tableau des valeurs
 *
 * @return void
 */
function display_etablissement_field_is_require(json) {
    // Si l'établissement n'est pas déjà obligatoire
    if (json.etablissement_already_require === false) {
        // Si c'est un type périodique
        if (json.is_periodic === true) {
            //
            $("#lib-etablissement").append(' ' + json.required_tag);
        } else {
            //
            $("#lib-etablissement span.not-null-tag").remove();
        }
    }
}

// {{{ BEGIN - FONCTIONS GENERIQUES

/**
 * Recherche avancée
 */
function toggle_advanced_search() {

    function hideclassic_showadvanced () {

        // hide classic and show advanced
        $("div#adv-search-adv-fields").show();
        $("div#adv-search-classic-fields").hide();

        // reset class input val
        $("div.adv-search-widget input[name=recherche]").val("");

        // change submit button name
        $("#adv-search-submit").attr("name", "advanced-search-submit");

        // change toggle link and legend labels
        $("#toggle-advanced-display").html("Recherche avancée");
        $("fieldset.adv-search-fieldset").children("legend").removeClass("switch_off");
        $("fieldset.adv-search-fieldset").children("legend").addClass("switch_on");
    }

    function hideadvanced_showclassic () {

        // hide advanced and show classic
        $("div#adv-search-adv-fields").hide();
        $("div#adv-search-classic-fields").show();

        // change submit button name
        $("#adv-search-submit").attr("name", "classic-search-submit");

        // change toggle link and legend labels
        $("#toggle-advanced-display").html("Recherche avancée");
        $("fieldset.adv-search-fieldset").children("legend").removeClass("switch_on");
        $("fieldset.adv-search-fieldset").children("legend").addClass("switch_off");
    }

    //
    if ($("#adv-search-submit").attr("name") == "advanced-search-submit") {
        $("div#adv-search-classic-fields").hide();
        $("#toggle-advanced-display").toggle(hideadvanced_showclassic,
                                              hideclassic_showadvanced);

        // change toggle link and legend labels
        $("#toggle-advanced-display").html("Recherche avancée");
        $("fieldset.adv-search-fieldset").children("legend").removeClass("switch_off");
        $("fieldset.adv-search-fieldset").children("legend").addClass("switch_on");

    } else {
        $("div#adv-search-adv-fields").hide();
        $("#toggle-advanced-display").toggle(hideclassic_showadvanced,
                                              hideadvanced_showclassic);

        // change toggle link and legend labels
        $("#toggle-advanced-display").html("Recherche avancée");
        $("fieldset.adv-search-fieldset").children("legend").removeClass("switch_on");
        $("fieldset.adv-search-fieldset").children("legend").addClass("switch_off");

    }
}

// Autocomplete : affichage de l'overlay d'un nouvel enregistrement
// @param obj classe du nouvel objet
function nouvel_enregistrement(obj) {

    popupIt(
        0,
        obj,
        '../scr/form.php?obj='+obj+'&action=0',
        '1000px',
        'auto',
        recuperer_nouvel_enregistrement,
        obj
    );

    return false;
}

// Autocomplete : récupère un enregistrement nouvellement créé
// @param obj classe de l'enregistrement
function recuperer_nouvel_enregistrement(obj) {

    // Déclaration des variables
    var id = "";
    var libelle = "";

    // Récupération des valeurs
    if ($("#id_new_"+obj).length > 0) {
        id = $("#id_new_"+obj).text();
    }
    if ($("#libelle_new_"+obj).length > 0) {
        libelle = $("#libelle_new_"+obj).text();
    }

    // Saisie des valeurs et modification des boutons disponibles
    if (id !== 0 && libelle !== "") {
        $("#autocomplete-"+obj+"-id").val(id);
        $("#autocomplete-"+obj+"-search").val(libelle);
        $("#autocomplete-"+obj+"-empty").show(); 
        $("#autocomplete-"+obj+"-check").show();
        $("#autocomplete-"+obj+"-link-selection").show();  
    }
    
    // 
    $("#autocomplete-"+obj+"-id").trigger("change");
    
    return false;
}

/**
 * Cette fonction permet de charger dans un dialog jqueryui un formulaire tel
 * qu'il aurait été chargé avec ajaxIt
 * 
 * @param objsf string : objet de sousformulaire
 * @param link string : lien vers un sousformulaire (../scr/sousform.php...)
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback function (optionel) : nom de la méthode à appeler
 *                                       à la fermeture du dialog
 * @param callbackParams mixed (optionel) : paramètre à traiter dans la function
 *                                          callback 
 *
 **/
function popupIt_sf(objsf, link, width, height, callback, callbackParams) {
    // 
    $('<div id=\"sousform-href\" data-href=\"'+link+'\"></div>').insertAfter('#tabs-1 .formControls');
    // Insertion du conteneur du dialog
    var dialog = $('<div id=\"sousform-'+objsf+'\" class=\"overlay\"></div>').insertAfter('#tabs-1 .formControls');
    $('<input type=\"text\" name=\"recherchedyn\" id=\"recherchedyn\" value=\"\" class=\"champFormulaire\" style=\"display:none\" />').insertAfter('#sousform-'+objsf);
    // execution de la requete passee en parametre
    // (idem ajaxIt + callback)
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            //Suppression d'un precedent dialog
            dialog.empty();
            //Ajout du contenu recupere
            dialog.append(html);
            //Initialisation du theme OM
            om_initialize_content();
            //Creation du dialog
            $(dialog).dialog({
            //OnClose suppression du contenu
            close: function(ev, ui) {
                // Si le formulaire est submit et valide on execute la méthode
                // passée en paramètre
                if (typeof(callback) === "function") {
                    callback(callbackParams);
                }
                if ($("#sousform-href").length) {
                    $("#sousform-href").remove();
                }
                $(this).remove();
            },
            resizable: true,
            modal: true,
            width: width,
            height: height,
            position: 'center',
          });
        },
        async : false
    });
    //Fermeture du dialog lors d'un clic sur le bouton retour
    $('#sousform-'+objsf).on("click",'a.retour',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}

/**
 * Cette fonction permet de charger dans un dialog jqueryui
 * un sous-formulaire comme l'aurait fait ajaxIt.
 * 
 * @param action integer : mode
 * @param obj string : objet de formulaire
 * @param link string : lien vers le formulaire (../scr/form.php...)
 * @param width integer: width en px
 * @param height integer: height en px
 * @param callback (optionnel) :
 * nom de la fonction à appeler à la fermeture du dialog
 * @param callbackParams (optionnel) :
 * paramètres de cette fonction
 */
function popupIt(action, obj, link, width, height, callback, callbackParams, idx) {

    // insertion du conteneur du dialog
    var form_id = 'form-'+obj+'-overlay';
    var dialog = $('<div id=\"'+form_id+'\"></div>').insertAfter('#tabs-1 .formControls');
    // exécution de la requete passée en paramètre
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Suppression d'un precedent dialog
            dialog.empty();
            // Ajout du contenu recupere
            dialog.append(html);
            // Initialisation du theme OM
            om_initialize_content();
            // Renommage formulaire
            $("#"+form_id+" form").attr("name","f2");
            // Creation du dialog
            $(dialog).dialog({
                // OnClose suppression du contenu
                close: function(ev, ui) {
                    if (typeof(callback) === "function") {
                        callback(callbackParams);
                    }
                    $(this).remove();
            },
            resizable: true,
            modal: true,
            width: width,
            height: height,
            position: 'center',
          });
        },
        async : false
    });
    // Fermeture du dialog lors d'un clic sur le bouton retour
    $('#'+form_id).on("click",'a.retour',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
    // Rechargement dans le dialog lors d'une validation
    $('#'+form_id+' form').on('submit',function() {

        var link = "../scr/form.php?obj="+obj+"&validation=1&action="+action+"&idx="+idx;
        post_overlay(action, obj, link, this, idx);
        return false;
    });
}

function post_overlay(action, obj, link, formulaire, idx) {
    // composition de la chaine data en fonction des elements du formulaire
    var data = "";
    if (formulaire) {
        for (i=0;i<formulaire.elements.length;i++) {
             form_val = encodeURIComponent(formulaire.elements[i].value);
             // Sauvegarde du contenu des tinymce avant serialisation
            if(tinyMCE.editors[formulaire.elements[i].name]) {
                form_val = encodeURIComponent(tinyMCE.get(formulaire.elements[i].name).getContent());
            }
            data+=formulaire.elements[i].name+"="+form_val+"&";
        }
    }
    // recuperation du terme recherche
    var recherchedyn = document.getElementById("recherchedyn"), recherche = '';
    if (recherchedyn !== null) {
        recherche = recherchedyn.value;
    }
    link += "&recherche="+recherche;
    // execution de la requete en POST
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: data,
        success: function(html){
            $("#form-"+obj+"-overlay").empty();
            $("#form-"+obj+"-overlay").append(html);
            // Renommage formulaire
            $("#form-"+obj+"-overlay form").attr("name","f2");
            om_initialize_content(true);
            // Rechargement dans le dialog lors d'une revalidation
            $("#form-"+obj+"-overlay form").on('submit',function() {

                var link = "../scr/form.php?obj="+obj+"&validation=2&action="+action+"&idx="+idx;
                post_overlay(action, obj, link, this, idx);
                return false;
            });
        }
    });
}

/**
 * Fonction de test des champs
 */
function isAlpha(str) {
    return /^[a-zA-Z\/]+$/.test(str);
}

/**
 * Valorise le champ ayant le type de formulaire "heure_minute"
 *
 * @param string $champ Nom du champ
 * @param string $type  Type du select heure ou minute
 */
function heure_minute(champ, type) {
    // Récupère la valeur du champ
    var champ_value = $("#"+champ).val();
    // Initialisation à vide de la valeur de l'heure
    var heure_value = "";
    // Initialisation à vide de la valeur de la minute
    var minute_value = "";
    // Si le champ n'est pas vide
    if (champ_value !== "") {
        // Découpe la valeur en heure et minute
        var champ_value_split = champ_value.split(":");
        // Récupère l'heure
        heure_value = champ_value_split[0];
        // Récupère la minute
        minute_value = champ_value_split[1];
    }
    //
    if (type === 'heure') {
        // Valorise l'heure
        heure_value = $("#"+champ+"_"+type).val();
    }
    //
    if (type === 'minute') {
        // Valorise la minute
        minute_value = $("#"+champ+"_"+type).val();
    }
    // Si au moins une des valeurs n'est pas vide
    if (heure_value !== "" || minute_value !== "") {
        // Remplis la valeur du champ avec les valeurs
        $("#"+champ).val(heure_value+":"+minute_value);
    }
    // Si les deux valeurs sont vides
    else {
        // On efface la valeur du couple heure/minute
        $("#"+champ).val("");
    }
}

/**
 * Appel d'un sous-tab dans un sous-form
 * 
 * @param  [sring] objsf objet sous tab
 * @param  [sring] link  url de l'action
 * @return void
 */
function ajaxTab(objsf, link) {
    // recuperation du terme recherche
    var recherche = document.getElementById("recherchedyn");
    if (recherche !== null) {
        link += "&recherche="+recherche.value;
    }else {
        link += "&recherche=";
    }
    // execution de la requete en POST
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            $("#soustab-"+objsf).empty();
            $("#soustab-"+objsf).append(html);
            if ($("#soustab-href").length) {
                $("#soustab-href").attr("data-href", link);
            }
            om_initialize_content(true);
        }
    });
}

/**
 * Cette fonction permet de compléter la valeur transmise par des zéros par la gauche
 * et de retourner la nouvelle valeur
 * @param  string  champ  Champ concerné
 * @param  integer length Taille du champ retourné
 */
function str_pad(value, length) {

    // Initialisation de la variable str
    var str = '' + value;

    // Si le champ n'est pas vide
    if (str !== '') {
        // Tant que la taille n'est pas atteint,
        // on ajoute des 0
        while (str.length < length) {
            str = '0' + str;
        }
    }
    return str;
}


/**
 * Cette fonction permet de compléter le champ par des zéro par la gauche
 * @param  string  champ  Champ concerné
 * @param  integer length Taille du champ retourné
 */
function str_pad_js(champ, length) {

    // Initialisation de la variable str
    var str = '' + champ.value;

    // Si le champ n'est pas vide
    if (str !== '') {
        // Tant que la taille n'est pas atteint,
        // on ajoute des 0
        while (str.length < length) {
            str = '0' + str;
        }
        // Modifie le champ
        champ.value = str;
    }
}

/**
 * Fonction de récupération des paramètres GET de la page
 * @return Array Tableau associatif contenant les paramètres GET
 */
function extractUrlParams(){    
    var t = location.search.substring(1).split('&');
    var f = [];
    for (var i=0; i<t.length; i++){
        var x = t[ i ].split('=');
        f[x[0]]=x[1];
    }
    return f;
}

/**
 * Cette fonction permet d'afficher les options d'un select par rapport
 * à un autre champ
 * 
 * @param  int id               Données (identifiant) du champ visé
 * @param  string tableName     Nom de la table
 * @param  string linkedField   Champ visé
 * @param  string formCible     Formulaire visé
 */
function filterSelect(id, tableName, linkedField, formCible) {

    //lien vers script php
    link = "../app/filterSelect.php?idx=" + id + "&tableName=" + tableName +
            "&linkedField=" + linkedField + "&formCible=" + formCible;

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(res){
            $('#'+tableName).empty();

            for ( j=0 ; j < res[0].length ; j++ ){

                $('#'+tableName).append(
                    '<option value="'+res[0][j]+'" >'+res[1][j]+'</option>'
                );
                
            }

        },
        async: false
    });
}

// }}} END - FONCTIONS GENERIQUES

// {{{ BEGIN - GESTION DU DOSSIER DE COORDINATION

// Sur l'événement "change" du champ "dossier_coordination_type"
function recuperer_type_dossier_coordination(selected) {
    // lien script php
    link = "../scr/form.php?obj=dossier_coordination&action=7&idx=0";
    //
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "selected="+selected+"&",
        dataType: "json",
        success: function(json){
            // Change la valeur du champ "a_qualifier"
            $("#a_qualifier").prop('checked', false);
            $("#a_qualifier").prop('value', '');
            //
            if (json.a_qualifier === 't') {
                $("#a_qualifier").prop('checked', true);
                $("#a_qualifier").prop('value', 'Oui');
            }
            // Change la valeur du champ "dossier_instruction_secu"
            $("#dossier_instruction_secu").prop('checked', false);
            $("#dossier_instruction_secu").prop('value', '');
            //
            if (json.dossier_instruction_secu === 't') {
                $("#dossier_instruction_secu").prop('checked', true);
                $("#dossier_instruction_secu").prop('value', 'Oui');
            }
            // Change la valeur du champ "dossier_instruction_acc"
            $("#dossier_instruction_acc").prop('checked', false);
            $("#dossier_instruction_acc").prop('value', '');
            //
            if (json.dossier_instruction_acc === 't') {
                $("#dossier_instruction_acc").prop('checked', true);
                $("#dossier_instruction_acc").prop('value', 'Oui');
            }
            // Change le libellé du champ "etablissement"
            display_etablissement_field_is_require(json);
        }
    });
}

/*
 * Met à jour les données de l'établissement dans le dossier de coordination
 * 
 * @param id integer L'identifiant de l'objet
 * @param obj string Le nom de l'objet
 */
function updateEtablissementData(id, obj) {
    
    var etablissement_type = $('#etablissement_type');
    var etablissement_categorie = $('#etablissement_categorie');
    var etablissement_type_secondaire = $('#etablissement_type_secondaire');
    var etablissement_locaux_sommeil = $('#etablissement_locaux_sommeil');
    var erp =$('#erp');
    // Remise à zéro des données des champs
    etablissement_type.val('');
    etablissement_categorie.val('');
    etablissement_type_secondaire.val('');
    etablissement_locaux_sommeil.prop('checked', false);
    etablissement_locaux_sommeil.val('');
    erp.prop('checked', false);
    erp.val('');
    //
    if (id !== '' && id !== 0 && typeof id !== 'undefined') {
        $.get(
            "../scr/form.php?obj="+obj+"&action=8&idx="+id,
            {"obj":obj,"action":"9", "idx":id},
            function(data){
                //
                etablissement_type.val(data.etablissement_type);
                etablissement_categorie.val(data.etablissement_categorie);
                setValMultiSelect(etablissement_type_secondaire, data.etablissement_type_secondaire);
                //
                if (data.etablissement_locaux_sommeil == 't') {
                    etablissement_locaux_sommeil.attr('checked', true);
                    etablissement_locaux_sommeil.val('Oui');
                }
                //
                if (data.etablissement_nature == 't') {
                    erp.attr('checked', true);
                    erp.val('Oui');
                }
            },
            "json"
        );
    }
    else{
        // Si le dossier de coordination est désélectionné, on met les données 
        // de l'établissement
        if (obj==='dossier_coordination'){
            updateEtablissementData($("input[name='etablissement']").val(), 'etablissement');
        }
    }
}

function setValMultiSelect(obj, values) {
    var opts = obj[0].options;
    for (var i = 0; i < opts.length; i++) {
        opts[i].selected = false;
        for (var j = 0; j < values.length; j++) {
            if (opts[i].value == values[j]) {
                opts[i].selected = true;
                break;
            }
        }
    }
}

// }}} END - GESTION DU DOSSIER DE COORDINATION

// {{{ BEGIN - GESTION DE L'ETABLISSEMENT

// Active les champs adresse de l'exploitant
function activerAdresseExploitant()  {

    $('#exp_adresse_numero').removeAttr('disabled');
    $('#exp_adresse_numero2').removeAttr('disabled');
    $('#exp_adresse_voie').removeAttr('disabled');
    $('#exp_adresse_complement').removeAttr('disabled');
    $('#exp_lieu_dit').removeAttr('disabled');
    $('#exp_boite_postale').removeAttr('disabled');
    $('#exp_adresse_cp').removeAttr('disabled');
    $('#exp_adresse_ville').removeAttr('disabled');
    $('#exp_adresse_arrondissement').removeAttr('disabled');
    $('#exp_cedex').removeAttr('disabled');
}
    

// Sur clic de la case copier l'adresse de l'établissement
// dans la fiche de cet établissement :
// efface les champs adresse de l'exploitant
// puis, si la case est cochée, effectue la copie
function copierAdresseEtablissement(checkbox) {

    // Remise à zéro des champs adresse de l'exploitant
    $('#exp_adresse_numero').val("");
    $('#exp_adresse_numero2').val("");
    $('#exp_adresse_voie').val("");
    $('#exp_adresse_complement').val("");
    $('#exp_lieu_dit').val("");
    $('#exp_boite_postale').val("");
    $('#exp_adresse_cp').val("");
    $('#exp_adresse_ville').val("");
    $('#exp_adresse_arrondissement').val("");
    $('#exp_cedex').val("");

    activerAdresseExploitant();

    // Si l'utilisateur a coché la copie d'adresse
    if (checkbox == "Oui") {

        // Récupération des champs adresse de l'établissement
        adresse_numero = $('#adresse_numero').val();
        adresse_numero2 = $('#adresse_numero2').val();
        adresse_voie = $('#autocomplete-voie-search').val();
        adresse_complement = $('#adresse_complement').val();
        lieu_dit = $('#lieu_dit').val();
        boite_postale = $('#boite_postale').val();
        adresse_cp = $('#adresse_cp').val();
        adresse_ville = $('#adresse_ville').val();
        adresse_arrondissement = $('#adresse_arrondissement').val();
        cedex = $('#cedex').val();

        // Copie vers les champs adresse de l'exploitant
        $('#exp_adresse_numero').val(adresse_numero);
        $('#exp_adresse_numero2').val(adresse_numero2);
        $('#exp_adresse_voie').val(adresse_voie);
        $('#exp_adresse_complement').val(adresse_complement);
        $('#exp_lieu_dit').val(lieu_dit);
        $('#exp_boite_postale').val(boite_postale);
        $('#exp_adresse_cp').val(adresse_cp);
        $('#exp_adresse_ville').val(adresse_ville);
        $('#exp_adresse_arrondissement').val(adresse_arrondissement);
        $('#exp_cedex').val(cedex);

        // Désactive les champs adresse de l'exploitant
        $('#exp_adresse_numero').attr('disabled', 'disabled');
        $('#exp_adresse_numero2').attr('disabled', 'disabled');
        $('#exp_adresse_voie').attr('disabled', 'disabled');
        $('#exp_adresse_complement').attr('disabled', 'disabled');
        $('#exp_lieu_dit').attr('disabled', 'disabled');
        $('#exp_boite_postale').attr('disabled', 'disabled');
        $('#exp_adresse_cp').attr('disabled', 'disabled');
        $('#exp_adresse_ville').attr('disabled', 'disabled');
        $('#exp_adresse_arrondissement').attr('disabled', 'disabled');
        $('#exp_cedex').attr('disabled', 'disabled');
    }
}

/*
 * Affiche ou masque les champs de périodicité des visites
 * suivant la nature et le type de l'établissement
 */
function masquerPeriodiciteVisites() {

    nature = $('#etablissement_nature').val();
    if (nature === '' || nature === 0 || nature == 'undefined') { nature ="none";}
    type = $('#etablissement_type').val();
    if (type === '' || type === 0 || type == 'undefined') { type ="none";}    

    var link = '../scr/form.php?obj=etablissement&action=7&idx=0';
    
    $.ajax({
        type: "POST",
        url: link,
        dataType: "json",
        data: "id_nature="+nature+"&id_type="+type,
        async: false,
        success: function(data){

            if (data.resultat == 'masquer') {

                // Date 1
                $("#si_prochaine_visite_periodique_date_previsionnelle").parent().parent().removeClass("field-type-date");
                $("#si_prochaine_visite_periodique_date_previsionnelle").parent().parent().addClass("field-type-hiddendate");
                $("#si_prochaine_visite_periodique_date_previsionnelle").prop('type', 'hidden');

                // Date 2
                $("#si_derniere_visite_periodique_date").parent().parent().removeClass("field-type-date");
                $("#si_derniere_visite_periodique_date").parent().parent().addClass("field-type-hiddendate");
                $("#si_derniere_visite_periodique_date").prop('type', 'hidden');

                // Fieldset
                $("#si_derniere_visite_periodique_date").parent().parent().parent().parent().parent().parent().hide();
            } else {

                // Date 1
                $("#si_prochaine_visite_periodique_date_previsionnelle").parent().parent().removeClass("field-type-hiddendate");
                $("#si_prochaine_visite_periodique_date_previsionnelle").parent().parent().addClass("field-type-date");
                $("#si_prochaine_visite_periodique_date_previsionnelle").prop('type', 'text');

                // Date 2
                $("#si_derniere_visite_periodique_date").parent().parent().removeClass("field-type-hiddendate");
                $("#si_derniere_visite_periodique_date").parent().parent().addClass("field-type-date");
                $("#si_derniere_visite_periodique_date").prop('type', 'text');

                // Fieldset
                $("#si_derniere_visite_periodique_date").parent().parent().parent().parent().parent().parent().show();
            }
        }
    });
}

/*
 * Récupère le statut juridique et le cas échéant affiche ou masque
 * l'établissement tutelle et les références patrimoine
 */
function verifierStatutJuridique(id_statut){
    
    if (id_statut === ''|| typeof id_statut == 'undefined') { id_statut ="none";} 
    if (id_statut != 'none') {
        var link = '../scr/form.php?obj=etablissement_statut_juridique&action=90&idx=0';
        $.ajax({
            type: "POST",
            url: link,
            dataType: "json",
            data: "id_statut="+id_statut,
            async: false,
            success: function(data){

                switch (data.resultat) {

                    // public
                    case 'pub':
                    // on affiche l'établissement tutelle
                    $("#etablissement_tutelle_adm").parent().parent().addClass("field-type-select");
                    $("#etablissement_tutelle_adm").parent().parent().removeClass("field-type-selecthidden");
                    // on cache les références patrimoine
                    $("#ref_patrimoine").parent().parent().addClass("field-type-referentielpatrimoinehidden");
                    $("#ref_patrimoine").parent().parent().removeClass("field-type-ref_patrimoine");
                    break;

                    // ville
                    case 'vle':
                    // on affiche les références patrimoine
                    $("#ref_patrimoine").parent().parent().addClass("field-type-referentielpatrimoine");
                    $("#ref_patrimoine").parent().parent().removeClass("field-type-referentielpatrimoinehidden");
                    // on cache l'établissement tutelle
                    $("#etablissement_tutelle_adm").parent().parent().addClass("field-type-selecthidden");
                    $("#etablissement_tutelle_adm").parent().parent().removeClass("field-type-select");
                    break;

                    // autre (dont statut privé et aucune sélection)
                    default:
                    // on cache l'établissement tutelle
                    $("#etablissement_tutelle_adm").parent().parent().addClass("field-type-selecthidden");
                    $("#etablissement_tutelle_adm").parent().parent().removeClass("field-type-select");
                    // on cache les références patrimoine
                    $("#ref_patrimoine").parent().parent().addClass("field-type-referentielpatrimoinehidden");
                    $("#ref_patrimoine").parent().parent().removeClass("field-type-referentielpatrimoine");
                }
            }
        });     
    }
}

/**
 * Récupération des références patrimoine en fonction des références cadastrale
 * fournies
 */
function getReferencesPatrimoine(obj){
    //Récupération de la liste des références cadastrales
    var referencesCadastrale = '';
    
    $(".reference_cadastrale_custom_field").each(
        function(){
            
            referencesCadastrale += $(this).val();
        }
    );
    if (referencesCadastrale===';'){
        referencesCadastrale='';
    }
    
    link = "../scr/form.php?obj=etablissement_tous&action=16&idx=0&ref_cad="+referencesCadastrale;
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(data){
            
            //Pas de message d'erreur
            if (referencesCadastrale!==''){
                
                //Ajout d'un DIV pour l'overlay
                $("#dialog").remove();
                $("#tabs-1").append('<div id="dialog"></div>');
                
                //Affichage du formulaire
                $("#dialog").html(data);
                
                $("#dialog").dialog({
                    //OnClose suppression du contenu
                    close: function(ev, ui) {
                        $(this).remove();
                    },
                    resizable: true,
                    modal: true,
                    width: 600,
                    height: 'auto',
                    position: 'center',
                });          
            }
            //Aucune référence cadastrale fournie
            else {
                alert(data);
            }
        },
        async: false
    });
}

/**
 * Récupération des références patrimoine choisies et affichage dans le formualire
 */
function getRefPatrimoine(){
    //
    if ($("#ref_patrimoine").val() !== "") {
        //
        $("#ref_patrimoine").val(
            $("#ref_patrimoine").val().replace(/^\s+|\s+$/g, '')+"\n"
        );
        //
        $("#ref_patrimoine").html(
            $("#ref_patrimoine").html().replace(/^\s+|\s+$/g, '')+"\n"
        );
    }
    //Pour chaque case cochée
    $("#dialog input[type='checkbox']:checked").each(function() {
        //
        $("#ref_patrimoine").val(
            $("#ref_patrimoine").val()+
            $(this).val()+"\n"
        );
        //
        $("#ref_patrimoine").html(
            $("#ref_patrimoine").html()+
            $(this).val()+"\n"
        );
    });
    //Fermeture de l'overlay
    $("#dialog").dialog("close");    
}

// }}} END - GESTION DE L'ETABLISSEMENT

// {{{ BEGIN - GESTION DE LA REUNION

/**
 * Aide à la saisie dans le formulaire de création de réunion.
 *
 * Récupère les informations du type de réunion pour pré-remplir les valeurs
 * souhaitées du formulaire de création de réunion.
 *
 * @param integer reunion_type_id Id du type de réunion sélectionné.
 */
function process_data_from_reunion_type(reunion_type_id) {

    $.ajax({
        type: "POST",
        url: "../scr/form.php?obj=reunion_type&action=101&idx="+reunion_type_id,
        dataType: "json",
        async: false,
        success: function(data){
            // Ajout des données dans les champs
            $('#libelle').val(data.libelle);
            $('#lieu_adresse_ligne1').val(data.lieu_adresse_ligne1);
            $('#lieu_adresse_ligne2').val(data.lieu_adresse_ligne2);
            $('#lieu_salle').val(data.lieu_salle);
            $('#heure_reunion').val(data.heure);
            $('#listes_de_diffusion').val(data.listes_de_diffusion);
        },
        error: function(){
            // On vide les champs
            $('#libelle').val("");
            $('#lieu_adresse_ligne1').val("");
            $('#lieu_adresse_ligne2').val("");
            $('#lieu_salle').val("");
            $('#heure_reunion').val("");
            $('#listes_de_diffusion').val("");
        }
    });
}

/**
 * Gestion du formulaire de planificiation automatique de dossiers en demandes
 * de passage en réunion.
 */
$(function(){

    // Dés que le champ du formulaire reunion planifier apparait
    if ($("select#reunion_choix_mode_planif").length) {
        // Affiche ou cache les champs du formulaire piece
        show_hide_fields_by_reunion_choix_mode_planif($("select#reunion_choix_mode_planif").val());
    }

});

/**
 * Affiche ou cache les champs par rapport à la valeur du champ choix_mode_planif
 */
function show_hide_fields_by_reunion_choix_mode_planif(value) {
    //
    if (typeof value === undefined || value === "") {
        //
        $("#reunion").parent().parent().hide();
        $("#programmation").parent().parent().hide();
        $("#dossier").parent().parent().hide();
        //
        $("#categorie").parent().parent().hide();
    } else if (value == "programmation") {
        //
        $("#reunion").parent().parent().hide();
        $("#programmation").parent().parent().show();
        $("#dossier").parent().parent().hide();
        //
        $("#categorie").parent().parent().show();
    }  else if (value == "reunion") {
        //
        $("#reunion").parent().parent().show();
        $("#programmation").parent().parent().hide();
        $("#dossier").parent().parent().hide();
        //
        $("#categorie").parent().parent().show();
    }  else if (value == "dossier") {
        //
        $("#reunion").parent().parent().hide();
        $("#programmation").parent().parent().hide();
        $("#dossier").parent().parent().show();
        //
        $("#categorie").parent().parent().show();
    }
    //
    return;
}

// }}} END - GESTION DE LA REUNION

// {{{ BEGIN - GESTION DES CONTACTS

/**
 * Actions au changement du select de la qualité du contact
 * @param qualite qualité du contact
 */
function verifierContactType(){

    // Réinitialise les champs et cache les champs inutiles
    // selon la qualité du contact

    switch ($('#qualite').val()) {

        case 'particulier':
            $('.particulier_fields span.not-null-tag').show();
            $('.personne_morale_fields input').each(
                function(){
                    $(this).val('');
                }
            );
            $('.personne_morale_fields select option[value=""]').each(
                function(){
                    $(this).attr('selected', 'selected');
                }
            );
            $('.personne_morale_fields').hide();
        break;

        case 'personne_morale':
            $('.particulier_fields span.not-null-tag').hide();
            $('.personne_morale_fields').show();
        break;
    }
}

/**
 * Fonction permettant d'afficher et cacher le bouton d'ajout de contacts
 * dans les DC
 */
function afficherBoutonAjoutContact(){

    // Si formulaire validé on cache les boutons
    if ($("form[action*=dossier_coordination].form-is-valid").size() > 0) {
        $('#add_contact').hide();
    } else {
        $('#add_contact').fadeIn(500);
    }
}

/**
 * Affichage de la synthèse d'un contact
 * ajouté via l'overlay
 */
function afficherContact() {
    var id = $('#id_retour').val();
    var link = '../scr/form.php?obj=contact&action=80&idx='+id;
    if($.isNumeric(id)) {
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            data: "linkable=true",
            success: function(html){
                $(html).insertBefore('#add_contact').fadeIn(500);

            },
            async:false
        });
    }
}

/**
 * Modification d'un contact
 */
function editContact(id) {
    var url = '../scr/form.php?obj=contact&action=1&idx='+id;
    popupIt(1, 'contact', url, 'auto', 'auto',
            replaceContact, '#contact_'+id, id);
    afficherBoutonAjoutContact();
}

/**
 * Remplacement d'un contact déjà existant
 */
function replaceContact(id_css) {
    var id_contact=$('#id_retour').val();
    var link = '../scr/form.php?obj=contact&action=80&idx='+id_contact;
    if($.isNumeric(id_contact)) {
        $.ajax({
            type: "POST",
            url: link,
            data: "linkable=true",
            cache: false,
            success: function(html){
                $(id_css).replaceWith(html);
            }
        });
    }
    om_initialize_content();
}

/**
 * Suppression d'un contact
 */
function removeContact(id) {
    $('#contact_'+id).remove();
    afficherBoutonAjoutContact();
}

// Fonction qui affiche/masque certains champs selon le type
function change_form_contact_type(field_select_type_value) {
    //
    if (field_select_type_value === undefined) {
        return;
    }
    //
    link = "../scr/form.php?obj=contact_type&action=4&idx="+field_select_type_value;
    //
    $("#reception_programmation").parent().parent().hide();
    $("#reception_commission").parent().parent().hide();
    $("#service").parent().parent().hide();
    //
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        dataType: "json",
        success: function(code){
            if (code == "inst") {
                $("#reception_programmation").parent().parent().show();
                $("#reception_commission").parent().parent().show();
                $("#service").parent().parent().show();
            }
        }
    });
}

// }}} END - GESTION DES CONTACTS

// {{{ BEGIN - GESTION DES REFERENCES REFERENCES CADASTRALES

/**
 * Formatage des champs pour les références cadastrales
 */
function formatFieldReferenceCadastrale(conteneur){

    addNewFieldReferencesCadastrales(conteneur);
    $(conteneur+'#references_cadastrales').parent().parent().hide();

    reference_cadastrale = $(conteneur+'#references_cadastrales').val();
    /*Formatage de la reference cadastrale si elle existe*/
    if (reference_cadastrale !== ''){
        
        /* Récupère la référence cadastrale non formatée */
        references_cadastrales = reference_cadastrale.split(';');
        var donnees = [];
        
        i = 0 ;
        /* Boucle sur les références, elles étaient séparées par un ; */
        for ( l = 0 ; l < references_cadastrales.length - 1 ; l ++ ){
            
            /*Récupère le code impots du qartier [CHIFFRES]*/
            k = 0;
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ ){
                
                if ( references_cadastrales[l].charAt(j) >= 0 && 
                    references_cadastrales[l].charAt(j) <= 9 ){
                        
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    i++;
                    break;
                }
            }
            
            /* Récupère la section [LETTRES] */     
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ )
                if ( isAlpha(references_cadastrales[l].charAt(j)) ){
                    
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    i++;
                    break;
                }
            
            /* Récupère la parcelle [CHIFFRES] */
            donnees[i] = '';
            for ( j = k ; j < references_cadastrales[l].length ; j++ )
                if ( references_cadastrales[l].charAt(j) >= 0 && 
                    references_cadastrales[l].charAt(j) <= 9 ){
                        
                    donnees[i] += references_cadastrales[l].charAt(j);
                    k++;
                    
                } else {
                    
                    break;
                }
            
            /* Récupère les séparateurs [ A / ] et les sections */
            m = 0 ; // Nombre de suffixe
            if ( k < references_cadastrales[l].length ){
                
                for ( j = k ; j < references_cadastrales[l].length ; j++ )
                    if ( isAlpha(references_cadastrales[l].charAt(j)) ){
                        
                        m++;
                        donnees[++i] = references_cadastrales[l].charAt(j);
                        donnees[++i] = '';
                    }
                    else {
                        
                        donnees[i] += references_cadastrales[l].charAt(j);
                    }
            }
            
            /* Créé autant de champs de que de références */
            donnees[++i] = ';';
            i++;

            if ( l > 0 ) {
                
                $(conteneur+'.reference_cadastrale_custom_fields').append( 
                    "<br/>" + fieldReferenceCadastraleBase(conteneur)
                );
            }
            
            actionFormReferenceCadastrale(conteneur);
            
            if ( m > 0 ){
                        
                for ( j = 0 ; j < m ; j++ ){
                    
                    $(conteneur+'#moreFieldReferenceCadastrale' + 
                        ( $(conteneur+'.moreFieldReferenceCadastrale').length - 1 ) ).
                    before(
                        newInputReferenceCadastrale(conteneur)
                    );
                }
            }
        }
         /* Action sur les boutons [+ ajouter d'autres champs] et [+ ajouter 
          * d'autres lignes] */
        actionLineFormReferenceCadastrale(conteneur);
        
        /* Ajoute les données dans les champs nouvellement créés */
        $(conteneur+'.reference_cadastrale_custom_field').each(
            function(index) {
                
                $(this).val(donnees[index]);
            }
        );
    }
    else{
        actionFormReferenceCadastrale(conteneur);
        actionLineFormReferenceCadastrale(conteneur);
    }
}

/**
 * Ajoute le code HTML des champs pour les références cadastrales
 */
function addNewFieldReferencesCadastrales(conteneur){

    $(conteneur+'.references_cadastrales_new_field').remove();
    $(conteneur+'.moreFieldReferenceCadastrale').remove();
    $(conteneur+'#morelineReferenceCadastrale').remove();

    var html = '<div class="field field-type-text references_cadastrales_new_field" >'+
            '<div class="form-libelle">' +
                '<label '+
                    'class="libelle-terrain_references_cadastrales" '+
                    'for="terrain_references_cadastrales">'+
                    ' Références cadastrales '+
                '</label>' +
            '</div>' +
            '<div class="form-content reference_cadastrale_custom_fields">' +
                 fieldReferenceCadastraleBase(conteneur) +
            '</div>' +
        '</div>';

    // Si formulaire pas validé
    if (!$(conteneur+"#form-content.form-is-valid").length) {

        html += '<div class="field field-type-text" id="morelineReferenceCadastrale">' +
        '<div class="form-libelle"></div>' +
        '<div class="form-content">' +
            '<span class="om-form-button add-16" title="Ajouter">ajouter d\'autres lignes</span>' +
        '</div>' +
        '</div>';
    }

    $(conteneur+'#references_cadastrales').parent().parent().before(
        html
    );
}

/**
 * Séparateur caché ; pour marquer la fin d'une ligne de référence cadastrale
 */
function hiddenSeparatorField(conteneur){

    return '<input ' +
        'class="reference_cadastrale_custom_field" ' +
        'type="hidden" ' +
        'maxlength="2" ' +
        'size="2" ' +
        'value=";" />';
}

/**
 *  Ajoute les ations spécifiques pour le formulaire personnalisé d'ajout de 
 *  références cadastrales
 */
function actionFormReferenceCadastrale(conteneur){
    
    // Si formulaire pas validé
    if (!$(conteneur+"#form-content.form-is-valid").length) {

        $(conteneur+'#moreFieldReferenceCadastrale' + 
            ($(conteneur+'.moreFieldReferenceCadastrale').length - 1 )).
        on("click", function() {
            
            $(this).before(newInputReferenceCadastrale(conteneur));
        });
    }
    
}


/*
 * Récupère les données saisies dans les champs de références cadastrales
 */
function getDataFieldReferenceCadastrale(){
    
    var reference_cadastrale = '';
    var reference_cadastrale_temp = '';
    var error = false;
    
    /*Pour chacun des champs du formulaire de saisie de référence cadastrale*/
    $('.reference_cadastrale_custom_field').each(
        function(){
            
            /*Si on est à la fin d'une ligne de champs*/
            if ( $(this).val() == ';' ){
                
                reference_cadastrale_bis = reference_cadastrale_temp ;
                
                /* Vérifie que les données sont correctement formatées avant de 
                 * les ajouter au résultat*/
                while( reference_cadastrale_bis != ''){
                    if ( /^([0-9]{1,4}[a-zA-Z]{1,3}[0-9]{1,5}([A\/]{1}[0-9]{1,4})*)*$/.test(reference_cadastrale_bis) ){
                        
                        reference_cadastrale += reference_cadastrale_bis + ";";
                        break;
                    }
                    else{
                        alert("Les références cadastrales saisies sont incorrectes. Veuillez les corriger.");
                        error = true;
                        return false;
                    }    
                }
                
                reference_cadastrale_temp = '';
            }
            
            else {
                
                /*Sinon, on récupère la valeur du champ*/
                reference_cadastrale_temp += $(this).val();
            }
        }
    );

    if(error) return false;
    // Met la valeur du résultat dans le champ généré par le framework
    $('#references_cadastrales').val(reference_cadastrale);
    return true;
}

/**
 * Action pour l'ajout de nouvelle ligne dans le formulaire d'ajout 
 * de référence cadastrale
 */
function actionLineFormReferenceCadastrale(conteneur){

    if (!$(conteneur+"#form-content.form-is-valid").length) {

        $(conteneur+'#morelineReferenceCadastrale').click( 
            function(){
                
                /*Ajout des trois champs de base*/
                $(conteneur+'.reference_cadastrale_custom_fields').
                append( "<br/>" + fieldReferenceCadastraleBase(conteneur));

                /*Ajout du bind pour l'ajout de nouveaux champs*/
                $(conteneur+'#moreFieldReferenceCadastrale'+ 
                    ($(conteneur+'.moreFieldReferenceCadastrale').length - 1 )).
                on("click", function() {
                    $(this).before(newInputReferenceCadastrale(conteneur));
                });
            }
        );
    }
}

/**
 * Ajout d'une nouvelle ligne de champ de référence cadastrale
 * Retourne le code HTML
 */
function fieldReferenceCadastraleBase(conteneur){
       
    var reference_cadastrale = '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field refquart" ' +
                'type="text" ' +
                'maxlength="3" ' +
                'size="3" '+
                'onchange="VerifNum(this);str_pad_js(this, 3);" '+
                'placeholder="Quart." ';

    // désactivation du champ de référence cadastrale
    // si formulaire validé avec succès
    if ($(conteneur+"#form-content.form-is-valid").length) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    
    reference_cadastrale += 'value="" />';

    reference_cadastrale += '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field refsect" ' +
                'type="text" ' +
                'maxlength="3" ' +
                'size="3" '+ 
                'placeholder="Sect." ';

    // désactivation du champ de référence cadastrale
    // si formulaire validé avec succès
    if ($(conteneur+"#form-content.form-is-valid").length) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    
    reference_cadastrale += 'value="" '+
                'onchange="if ( !isAlpha(this.value) && this.value != \'\' ){ alert(\'Vous ne devez saisir que des lettres dans ce champ.\'); this.value = \'\'; }; this.value=this.value.toUpperCase();"/>';
    reference_cadastrale += '<input ' +
                'class="champFormulaire reference_cadastrale_custom_field refparc" ' +
                'type="text" ' +
                'maxlength="4" ' +
                'size="4" '+ 
                'onchange="VerifNum(this);str_pad_js(this, 4);" '+
                'placeholder="Parc." ';

    // désactivation du champ de référence cadastrale
    // si formulaire validé avec succès
    if ($(conteneur+"#form-content.form-is-valid").length) {
        reference_cadastrale += 'disabled="disabled" ';
    }
        
    reference_cadastrale += 'value="" />';

    reference_cadastrale += '<span id="moreFieldReferenceCadastrale' +
    $(conteneur+'.moreFieldReferenceCadastrale').length +
    '" class="moreFieldReferenceCadastrale">' + hiddenSeparatorField(conteneur);

    // Si formulaire pas validé
    if (!$(conteneur+"#form-content.form-is-valid").length) {

        reference_cadastrale += 
            '<span class="om-form-button add-16" title="Ajouter">ajouter d\'autres champs</span>';
    }
    
    return reference_cadastrale;
}

/**
 * Action au clique sur le bouton " + ajouter d'autres champs"
 */
function newInputReferenceCadastrale(conteneur){
    
    var reference_cadastrale =
        '<input ' +
        'class="champFormulaire reference_cadastrale_custom_field" ' +
        'type="text" ' +
        'maxlength="1" ' +
        'size="2" ' +
        'value="" ' + 
        'placeholder="Sep." ';
    if ($(conteneur+"#form-content.form-is-valid").length) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    reference_cadastrale += 'onchange="testSeparator(this);"/>' +
        '<input ' +
        'class="champFormulaire reference_cadastrale_custom_field" ' +
        'type="text" ' +
        'maxlength="4" ' +
        'size="4" ' +
        'onchange="VerifNum(this);str_pad_js(this, 4);" '+
        'placeholder="Parc." ';
    if ($(conteneur+"#form-content.form-is-valid").length) {
        reference_cadastrale += 'disabled="disabled" ';
    }
    reference_cadastrale += 'value="" />';
    return reference_cadastrale;
}

function testSeparator(obj){

    if (obj.value != 'A' && obj.value != 'a' && 
         obj.value != '/' ) {
             
        alert('Saisissez uniquement un A ou un / comme séparateur');
        obj.value = '';
    } else {
        obj.value = obj.value.toUpperCase();
    }
}

// }}} END - GESTION DES REFERENCES REFERENCES CADASTRALES

// {{{ BEGIN - GESTION DE LA PROGRAMMATION DES VISITES

/**
 * Assistance dynamique à la saisie de l'année.
 * Contrôle côté PHP dans la méthode vérifier().
 * 
 * @param  [string] annee Année saisie par l'utilisateur
 */
function verifier_annee(annee) {
    //
    if (annee.value === "") {
        return;
    }
    // On teste si la valeur saisit est un nombre valide de 4 chiffres
    if ((isNaN(annee.value)) || (annee.value.length != 4)) {
        // On avertit l'utilisateur qu'il y a une erreur dans le champs
        alert("Vous n'avez pas saisi une année valide.");
        // On vide le champ
        annee.value = "";
        return;
    }
}

/**
 * Assistance dynamique à la saisie du numéro de semaine.
 * Contrôle côté PHP dans la méthode vérifier().
 * 
 * @param  [string] semaine Numéro de semaine saisi par l'utilisateur
 */
function verifier_semaine(semaine) {
    //
    if (semaine.value === "") {
        return;
    }
    // On teste si le numéro de semaine est compris entre 1 et 53 inclus
    if ((isNaN(semaine.value)) || (semaine.value < 1) || (semaine.value > 53)) {
        // On avertit l'utilisateur qu'il y a une erreur dans le champs
        alert("Vous n'avez pas saisi un numéro de semaine valide.");
        // On vide le champ
        semaine.value = "";
        return;
    }
}

/**
 * Crée un calendrier pour une semaine de programmation donnée
 * 
 * @param  [integer] id_programmation ID de la semaine de programmation
 */
function creer_calendrier(id_programmation) {

    var link_ajax = "../scr/form.php?obj=programmation&action=6&idx="+id_programmation;
    var link_events = "../scr/form.php?obj=programmation&action=7&idx="+id_programmation;

    //Requête ajax de récupération des données
    $.ajax({
        type: "POST",
        url: link_ajax,
        dataType: "json",
        async: false,
        success: function(data){
            // Affichage du calendrier
            $('#agenda_calendrier_' + id_programmation).fullCalendar({
                defaultView: 'agendaWeek',
                header: {
                    left: '',
                    center: '',
                    right:  ''
                },
                lang: 'fr',
                firstDay : 1,
                timezone: false,
                defaultDate: data.lundi,
                allDaySlot: false,
                allDayDefault: false,
                slotDuration: '00:15:00',
                axisFormat: 'HH:00',
                scrollTime: '08:00:00',
                minTime: '08:00:00',
                maxTime: '21:00:00',
                editable: false,
                droppable: true,
                defaultTimedEventDuration: '03:00:00',
                height: 'auto',
                drop: function(moment, allDay) {
                    // Récupération de l'ID du technicien
                    var id_technicien = $('select#acteur option:selected').val();
                    // Création de l'overlay d'ajout de visite si technicien sélectionné
                    if (id_technicien !== '' && id_technicien !== undefined) {
                        // Récupération du moment de l'agenda où on dépose la ligne
                        var heure_debut = moment.format("HH:mm");
                        var heure = parseInt(moment.format("HH"));
                        var minute = moment.format("mm");
                        var heure_fin = heure_debut;
                        if (heure <= 18) {
                            heure = parseInt(heure) + 3;
                            heure_fin = heure + ":" + minute;
                        }
                        var jour = moment.format("DD/MM/YYYY");
                        // Récupération de l'ID de l'établissement
                        var id_ligne = $(this).attr("id");
                        var id_di = $('#'+id_ligne+' .dossier_instruction_id').text();
                        // Formulaire d'ajout en overlay
                        popupIt_sf(
                            'visite', 
                            '../scr/sousform.php?obj=visite&action=0&retourformulaire=programmation&idxformulaire='+id_programmation+"&id_di="+id_di+"&date_visite="+jour+"&heure_debut="+heure_debut+"&heure_fin="+heure_fin+"&id_technicien="+id_technicien, 
                            'auto', 
                            'auto', 
                            handle_programmation_visite,
                            ''
                        );
                    }
                },
                events: {
                    url: link_events,
                    type: 'POST',
                },
                eventClick: function(event) {
                    if (event.url) {
                        // Récupération de l'ID du technicien
                        var id_technicien = $('select#acteur option:selected').val();
                        // Consultation de la visite
                        popupIt_sf(
                            'visite', 
                            event.url, 
                            'auto', 
                            'auto', 
                            handle_programmation_visite, 
                            ''
                        );
                        return false;
                    }
                }
            });
            // On ajoute un marqueur pour vérifier que l'initialisation du calendrier
            // a été effectuée et qu'il est inutile de la refaire
            $('#agenda_calendrier_' + id_programmation).addClass("already-fc");
        }
    });
}

/*
 * Filtre les propositions
 */
function filtre_propositions(champ){
    //
    var id_colonne = $(champ).attr('id').slice(0, -4);
    //
    if ($(champ).is(':checkbox') && $(champ).is(':checked')){
        $('#tableau_propositions').DataTable()
                .columns('.'+id_colonne)
                .search('t')
                .draw();
    }
    else if ($(champ).is('select') && $(champ).val() !== ''){
        $('#tableau_propositions').DataTable()
                .columns('.'+id_colonne)
                .search($(champ).val())
                .draw();
    }
    else{
        $('#tableau_propositions').DataTable()
            .columns('.'+id_colonne)
            .search('')
            .draw();
    }
}

/**
 * En fonction du technicien éventuellement sélectionné,
 * cette fonction permet de gérer le raffraichissement de l'agenda de la programmation
 * et le drag and drop de la liste des propositions si elle existe.
 */
function handle_programmation_technicien() {
    // Si le bloc agenda n'est pas présent, la fonction ne peut pas être exécutée
    if (!$("#bloc_agenda .agenda_calendrier").length) {
        return;
    }
    // Récupération de l'ID CSS de l'agenda
    var agenda = $("#bloc_agenda .agenda_calendrier").attr("id");
    // Récupération de l'identifiant de la programmation
    var id_programmation = agenda.replace("agenda_calendrier_","");
    // Lien vers la liste JSON de toutes les visites de la programmation
    var link_toutes_visites = "../scr/form.php?obj=programmation&action=7&idx="+id_programmation;
    // Récupération de l'id du technicien si il y en a un
    var id_technicien = "";
    if ($("#bloc_agenda select#acteur").length && $("#bloc_agenda select#acteur option:selected").val() !== "") {
        id_technicien = $("#bloc_agenda select#acteur option:selected").val();
    }
    // Lien vers la liste JSON de toutes les visites de la programmation d'un technicien
    var link_visites_technicien = "../scr/form.php?obj=programmation&action=8&idx="+id_programmation+"&idz="+id_technicien;
    // On vide les agendas
    $('#'+agenda).fullCalendar('removeEvents');
    // Si un technicien a été choisi on récupère uniquement les visites de ce dernier
    // et on active le drag and drop
    if (id_technicien !== "") {
        // 
        $('#'+agenda).fullCalendar('addEventSource', link_visites_technicien);
        //
        if ($("#tableau_propositions").length) {
            //
            $("#tableau_propositions tr").draggable("enable");
            $("#filtre_technicien_info").hide();
        }
    }
    // sinon on affiche toutes les visites et on désactive le drag and drop
    else {
        $('#'+agenda).fullCalendar('addEventSource', link_toutes_visites);
        //
        if ($("#tableau_propositions").length) {
            //
            $("#tableau_propositions tr").draggable("disable");
            $("#tableau_propositions tr").removeClass("ui-state-disabled");
            $("#filtre_technicien_info").show();
        }
    }
}

/**
 * Toute action sur une visite est susceptible de modifier :
 *  - les événements de l'agenda ;
 *  - les résultats de la liste de propositions.
 * 
 * C'est pourquoi après chaque sortie d'un formulaire de visite on
 * appelle cette méthode qui rafraîchit l'agenda et la liste.
 */
function handle_programmation_visite() {
    // Avec filtre établissements proches
    if ($("#code_etab").length
        && $("#code_etab").val() !== ''
        && document.getElementById("btn_proches").disabled === true) {
        reload_datatable_with_filter(false);
        handle_programmation_technicien();
        return;
    }
    // Sans filtre
    $('#tableau_propositions').DataTable().ajax.reload(null, true);
    handle_programmation_technicien();
}

/**
 *
 */
function check_visiting_hours(field) {
    //
    if ($("#heure_debut_minute").val() === "" ||
        $("#heure_debut_heure").val() === "" ||
        $("#heure_fin_minute").val() === "" ||
        $("#heure_fin_heure").val() === "") {
        //
        if ($("#heure_debut_minute").val() === "") {
            $("#heure_debut_minute").val("00");
        } else if ($("#heure_debut_heure").val() === "") {
            $("#heure_debut_heure").val("00");
        } else if ($("#heure_fin_minute").val() === "") {
            $("#heure_fin_minute").val("00");
        } else if ($("#heure_fin_heure").val() === "") {
            $("#heure_fin_heure").val("00");
        }
        //
        heure_minute('heure_debut', 'minute');
        heure_minute('heure_debut', 'heure');
        heure_minute('heure_fin', 'minute');
        heure_minute('heure_fin', 'heure');
    }
    //
    if ($("#heure_debut").val() >= $("#heure_fin").val()) {
        //
        alert(msg_alert_error_check_visiting_hours);
        //
        if ($("#heure_debut_minute").val() == "45") {
            //
            if ($("#heure_debut_heure").val() == "23") {
                $("#heure_fin_heure").val("23");
                $("#heure_debut_minute").val("30");
                $("#heure_fin_minute").val("45");
            } else {
                //
                $("#heure_fin_heure").val(str_pad(parseInt($("#heure_debut_heure").val())+1, 2));
                $("#heure_fin_minute").val("00");
            }
        } else {
            //
            $("#heure_fin_heure").val($("#heure_debut_heure").val());

            $("#heure_fin_minute").val(str_pad(parseInt($("#heure_debut_minute").val())+15, 2));
        }
        //
        heure_minute('heure_debut', 'minute');
        heure_minute('heure_debut', 'heure');
        heure_minute('heure_fin', 'minute');
        heure_minute('heure_fin', 'heure');
    }
    //
    return;
}

// }}} END - GESTION DE LA PROGRAMMATION DES VISITES

// {{{ BEGIN - GESTION DES AUTORITES DE POLICE

/**
 * Aide à la saisie dans le formulaire du paramétrage
 * d'un type d'autorité de police ou décision d'autorité de police
 *
 * Champs :
 * - 'autorite_police_decision.nomenclature_actes_matiere_niv1'
 * - 'autorite_police_decision.nomenclature_actes_matiere_niv2'
 */
function handle_filter_select_nomenclature_actes() {
    // Dans le cas d'un formulaire
    selected_value_niv1 = $("select#nomenclature_actes_matiere_niv1").val();
    selected_value_niv2 = $("select#nomenclature_actes_matiere_niv2").val();
    // Dans le cas d'une vue consulter
    if (selected_value_niv1 == undefined) {
        return false;
    }

    //
    if (selected_value_niv1 == "") {
        $("#nomenclature_actes_matiere_niv2").empty();
        $("#nomenclature_actes_matiere_niv2").append(
            '<option value="">Choisir niveau 2</option>'
        );
    } else {
        //
        link = "../scr/form.php?obj=autorite_police_decision&action=4&idx=0";
        //
        $.ajax({
            type: "GET",
            url: link,
            cache: false,
            dataType: "json",
            success: function(json){
                $("#nomenclature_actes_matiere_niv2").empty();
                $("#nomenclature_actes_matiere_niv2").append(
                    '<option value="">Choisir niveau 2</option>'
                );
                $.each(json[selected_value_niv1], function(i, obj) {
                    selected_attr = '';
                    if (obj == selected_value_niv2) {
                        selected_attr = ' selected="selected"';
                    }
                    $("#nomenclature_actes_matiere_niv2").append(
                        '<option value="'+obj+'"'+selected_attr+'>'+obj+'</option>'
                    );
                });
            }
        });
    }
}

/**
 * Aide à la saisie dans le formulaire du paramétrage
 * d'un type d'autorité de police ou décision d'autorité de police
 *
 * Champ :
 * - 'autorite_police_decision.type_arrete'
 */
function handle_ap_decision_type_arrete() {
    // Dans le cas d'un formulaire
    selected_value = $("input#type_arrete").val();
    // Dans le cas d'une vue consulter
    if (selected_value == undefined) {
        selected_value = $("span#type_arrete").html();
    }
    //
    if (selected_value == 'Oui') {
        $(".arrete_parameters").show();
    } else {
        $(".arrete_parameters").hide();
    }
}

/**
 * Récupère les paramétres de la décision d'autorité de police
 *
 * @param integer autorite_police_decision_selected Valeur sélectionnée
 *
 * @return [void]
 */
function get_autorite_police_decision_params(autorite_police_decision_selected) {
    //
    link = "../scr/form.php?obj=autorite_police&action=4&idx=0";

    //
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "autorite_police_decision_selected="+autorite_police_decision_selected+"&",
        dataType: "json",
        success: function(json){
            // Change la valeur du champ "delai"
            $("#delai").val(json);
        }
    });
}

/**
 * Permet d'afficher la synthèse de l'autorité de police sur le formulaire
 * dossier_instruction_reunion.
 *
 * @return [void]
 */
function display_synthesis_autorite_police() {
    // Identifiant de l'autorité de police crée récemment
    var id = $('#id_retour').val();
    // URL
    var link = '../scr/form.php?obj=autorite_police&action=5&idx='+id;
    //
    if($.isNumeric(id)) {
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            success: function(html) {
                // Affiche la synthèse de l'autorité de police avant le bouton
                // d'ajoute
                $(html).insertBefore('#add_autorite_police').fadeIn(500);
            },
            async:false
        });
    }
}

/**
 * Permet d'ouvrir en overlay le formulaire de modification de l'autorité
 * de police.
 *
 * @param integer id Identifiant de l'autorité de police
 *
 * @return [void]
 */
function edit_autorite_police(id) {
    var url = '../scr/form.php?obj=autorite_police&action=1&idx='+id;
    popupIt(1, 'autorite_police', url, 'auto', 'auto',
            replace_autorite_police, '#autorite_police_'+id, id);
}

/**
 * Permet de modifier l'affichage de la liste des autorités de police sur le
 * formulaire dossier_instruction_reunion.
 *
 * @param mixed id_css Balise
 *
 * @return [void]
 */
function replace_autorite_police(id_css) {
    var id = $('#id_retour').val();
    var link = '../scr/form.php?obj=autorite_police&action=5&idx='+id;
    if($.isNumeric(id)) {
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            success: function(html) {
                $(id_css).replaceWith(html);
            }
        });
    }
    om_initialize_content();
}

/**
 * Permet de supprimer une autorité de police depuis une demande de réunion et
 * de l'enlever de la liste.
 *
 * @param integer id          Identifiant de l'AP
 * @param integer modal_title Titre de la modale
 *
 * @return [void]
 */
function delete_autorite_police(id, modal_title) {
    // Boîte de confirmation
    $("#dialog-confirm").dialog({
        resizable: false,
        modal: true,
        title: modal_title,
        height: "auto",
        width: 250,
        buttons: {
            // Réponse "OUI"
            "Oui": function () {
                $(this).dialog('close');
                treatment_delete_autorite_police(id, true);
            },
            // Réponse "NON"
            "Non": function () {
                $(this).dialog('close');
            }
        }
    });
}

/**
 * Traitement de la suppression d'une AP.
 *
 * @param integer id            Identifiant de l'AP
 * @param boolean confirm_value Valeur de la confirmation
 *
 * @return [void]
 */
function treatment_delete_autorite_police(id, confirm_value) {
    // Si la boîte de confirmation est validé
    if (confirm_value === true) {
        //
        link = "../scr/form.php?obj=autorite_police&action=6&idx="+id;
        //
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            dataType: "json",
            success: function(json) {
                //
                if (json === true) {
                    $('#autorite_police_'+id).remove();
                }
            }
        });
    }
}

// }}} END - GESTION DES AUTORITES DE POLICE

// {{{ BEGIN - GESTION DES TEXTES TYPES

/**
 * Ajoute les descriptions des documents-types sélectionnés.
 * 
 * @param [string]  champ    ID CSS du champ html dans lequel
 *                           le résultat doit être ajouté
 * @param [string]  form     ID CSS du dialog conteneur (form overlay)
 * @param [boolean] is_html_content  vrai si on affiche le libellé
 *
 * @return [void]
 */
function add_description_type(champ, form, is_html_content) {
    is_html_content = typeof is_html_content !== 'undefined' ?  is_html_content : false;
    //
    if (tinyMCE.editors[champ]) {
        //
        res = '';
        //
        $("#"+form+" input[type=checkbox]:checked").each(function(){
            //
            var id_checkbox = this.id;
            id_checkbox = id_checkbox.replace("checkbox_", "");
            // Ajout description
            res += $('#description_'+id_checkbox).html();
            // Si le champ description ne contient pas de HTML 
            // (configuré avec un éditeur de texte riche qui a du 
            // contenu obligatoirement dans une balise <p>)
            if (is_html_content === false) {
                // Ajout on ajoute un saut de ligne
                res += '<br />';
            }
        });
        //
        if (res !== '') {
            add_html_content(champ, res);
        }
    }
    // Suppression de l'overlay de textes-types
    $('#'+form).remove();
}

/**
 *
 * @param [string]  field        ID CSS du champ html à modifer
 * @param [string]  html_content Code html à insérer dans le champ
 * @param [boolean] style_ul     Facultatif. Si défini crée un conteneur liste <ul>
 * 
 * @return [void]
 */
function add_html_content(field, html_content) {
    //
    if (tinyMCE.editors[field]) {
        //
        obj = tinyMCE.get(field);
        //
        if (html_content !== '') {
            obj.setContent(obj.getContent()+html_content);
        }
    }
}

// }}} END - GESTION DES TEXTES TYPES

// {{{ BEGIN - ANALYSE - GESTION DES DOCUMENTS PRESENTES

/**
 * Affiche un overlay de propositions d'ajout de documents-types.
 * 
 * @param [string] link  url de la vue
 * @param [string] champ ID CSS du champ qui appelle cette vue
 *
 * @return [void]
 */
function overlay_document_presente_type(link, champ) {

    // ID CSS du dialog conteneur du formulaire
    var id_dialog = "overlay_document_presente_type";

    // insertion du conteneur du dialog
    var dialog = $('<div id="'+id_dialog+'"></div>').insertAfter('#tabs-1 .formControls');

    // exécution de la requete passée en paramètre
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Suppression d'un precedent dialog
            dialog.empty();
            // Ajout du contenu recupere
            dialog.append(html);
            // Initialisation du theme OM
            om_initialize_content();
            // Creation du dialog
            $(dialog).dialog({
                // OnClose suppression du contenu
                close: function(ev, ui) {
                    $(this).remove();
                },
                resizable: true,
                modal: true,
                width: 'auto',
                height: 'auto',
                position: 'center',
            });
        },
        async : false
    });
    // Callback lors d'une validation
    $('#'+id_dialog+' form').on('submit',function() {
        add_description_type(champ, id_dialog, true);
        $(dialog).dialog('close').remove();
        return false;
    });
}

// }}} END - ANALYSE - GESTION DES DOCUMENTS PRESENTES

// {{{ BEGIN - ANALYSE - GESTION DES PRESCRIPTIONS

/**
 * Affiche l'overlay d'ajout d'une prescription.
 *
 * Cette fonction est appelée au clic sur l'icône ajouter de l'écran de gestion
 * des prescriptions. Elle gère l'affichage de l'overlay contenant le 
 * formulaire de saisie d'une prescription (couple prescription réglementaire /
 * prescription spécifique).
 * 
 * @param  [integer] id_analyse [Valeur de la clé primaire de l'analyse]
 * 
 * @return [void]
 */
function overlay_add_prescription(id_analyse) {

    // ID CSS du dialog conteneur du formulaire
    var id_dialog = "form-prescription";

    // insertion du conteneur du dialog
    var dialog = $('<div id="'+id_dialog+'"></div>').insertAfter('#sousform-analyses');

    // Récupération du prochain ordre disponible
    var ordre = get_next_order_prescription();

    // URL de la vue d'ajout
    var link = "../scr/form.php?obj=analyses&action=40&idx="+id_analyse;

    // exécution de la requete passée en paramètre
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: "ordre="+ordre+"&",
        success: function(html){
            // Suppression d'un precedent dialog
            dialog.empty();
            // Ajout du contenu recupere
            dialog.append(html);
            //
            $("#sousform-analyses").hide();
            // Initialisation du theme OM
            om_initialize_content(true);
            // Prescription réglementaire vide par défaut
            // (permet de réinitialiser les champs html
            // qui peuvent avoir déjà été instanciés)
            get_prescription_reglementaire();
        },
        async : false
    });
}

/**
 * Affiche l'overlay de modification d'une prescription.
 * 
 * Cette fonction est appelée au clic sur un des icônes modifier de l'écran de
 * gestion des prescriptions. Elle gère l'affichage de l'overlay contenant le 
 * formulaire de modification d'une prescription (couple prescription 
 * réglementaire / prescription spécifique).
 *
 * @param  [string]  id_css     [ID CSS du bouton modifier de la prescription]
 * @param  [integer] id_analyse [Valeur de la clé primaire de l'analyse]
 * 
 * @return [void]
 */
function overlay_edit_prescription(id_css, id_analyse) {

    // ID CSS du dialog conteneur du formulaire
    var id_dialog = "form-prescription";

    // insertion du conteneur du dialog
    var dialog = $('<div id="'+id_dialog+'"></div>').insertAfter('#sousform-analyses');

    // Récupération du prochain ordre disponible
    var ordre = id_css.replace("edit_prescription_","");
    ordre = parseInt(ordre);

    // URL de la vue d'ajout
    var link = "../scr/form.php?obj=analyses&action=50&idx="+id_analyse;

    // récupération des champs à modifier
    var pr_id = $("#pr_id_"+ordre).val();
    var pr_def = $("#pr_def_"+ordre).val();
    var pr_desc = $("#pr_desc_"+ordre).val();
    var ps_desc = $("#ps_desc_"+ordre).val();
    var pr_values = "ordre="+ordre+"&"+
        "pr_id="+pr_id+"&"+
        "pr_def="+pr_def+"&"+
        "pr_desc="+pr_desc+"&"+
        "ps_desc="+ps_desc+"&";

    // exécution de la requete passée en paramètre
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: pr_values,
        success: function(html){
            // Suppression d'un precedent dialog
            dialog.empty();
            // Ajout du contenu recupere
            dialog.append(html);
            //
            $("#sousform-analyses").hide();
            // Initialisation du theme OM
            om_initialize_content(true);
            // Affiche ou masque l'ajout de prescriptions spécifiques types
            link = "../scr/form.php?obj=prescription_reglementaire&action=4&idx="+pr_id;
            $.ajax({
                type: "GET",
                url: link,
                cache: false,
                dataType: "json",
                success: function(data){
                    //
                    if (data.prescriptions_specifiques === true) {
                        $("#specifique-type").show();
                    } else {
                        $("#specifique-type").hide();
                    }
                }
            });
        },
        async : false
    });
}

/**
 * Appelée à la validation du formulaire d'ajout d'une prescription,
 * cette fonction vérifie qu'une prescription réglementaire est sélectionnée.
 * Si oui elle insère cette nouvelle prescription dans le tableau
 * de la vue édition des prescriptions, sinon elle affiche un message d'erreur.
 * 
 * @return [void]
 */
function add_prescription() {
    // On vérifie qu'une prescription est renseignée
    var ps_desc = "";
    //
    var pr_id = $('select#prescription_reglementaire option:selected').val();
    //
    var ps_html = "ps_description_om_html";
    //
    if (tinyMCE.editors[ps_html]) {
        ps_desc = tinyMCE.get(ps_html).getContent();
    }
    // Si prescription réglementaire sélectionnée
    if (pr_id !== '' &&
        typeof pr_id !== 'undefined' &&
        (isNaN(pr_id) === false)) {
        
        // On récupère l'ID de l'analyse
        var id_analyse = $("#id_analyse").val();

        // On récupère le prochain ordre disponible
        var ordre = get_next_order_prescription();

        // On définit la classe odd/even en fonction du numéro d'ordre
        var css_odd = "odd";
        if (ordre % 2 === 0) {
            css_odd = "even";
        }

        // flêche haut
        var css_fleche_haut = " cache_fleche_haut_prescription";
        if (ordre > 1) {
            css_fleche_haut ="";
        }

        // Initialisation de la cellule qui contient la prescription spécifique
        var td_pr = '<td class="col-2 marge_essai_droite" id="td_pr_'+ordre+
            '"><p>Défavorable : <span style="font-weight: bold;">'+
            $("#pr_defavorable").val()+
            '<span></p>';
        //
        var pr_html = "pr_description_om_html";
        var pr_desc = $('#pr_description_om_html').html();
        td_pr += pr_desc;
        pr_input = input_prescription(ordre, "pr_desc", pr_desc);
        pr_input += input_prescription(ordre, "pr_id", pr_id);
        var pr_def = 'f';
        if ($("#pr_defavorable").val() == 'Oui' ||
            $("#pr_defavorable").val() == 'oui' ||
            $("#pr_defavorable").val() == 't') {
            pr_def = 't';
        }
        pr_input += input_prescription(ordre, "pr_def", pr_def);
        //
        td_pr += '</td>';

        // On écrit le code HTML de la nouvelle prescription
        // ouverture ligne
        var ligne = '<tr id="prescription_'+ordre+'" class="tab-data '+css_odd+' une_prescription">'+
            '<td class="col-0 center_cell_prescription actions-max-2">'+
                '<span class="cercle_ordre" id="numero_'+ordre+'">'+ordre+'</span>'+
            '</td>'+
            '<td class="col-1 btns_prescription marge_essai_droite actions-max-4">'+
                '<span id="delete_prescription_'+ordre+'"'+
                      'title="Supprimer cette prescription"'+
                      'onclick="delete_prescription(this.id);"'+
                      'class="om-form-button delete-16"></span>'+
                '<span id="edit_prescription_'+ordre+'"'+
                      'title="Modifier cette prescription"'+
                      'onclick="overlay_edit_prescription(this.id,'+id_analyse+');"'+
                      'class="om-form-button edit-16"></span>'+
                '<span id="move_down_prescription_'+ordre+'"'+
                      'title="Déplacer cette prescription vers le bas"'+
                      'onclick="move_down_prescription(this.id);"'+
                      'class="arrow-down-16 cache_fleche_bas_prescription"></span>'+
                '<span id="move_up_prescription_'+ordre+'"'+
                      'title="Déplacer cette prescription vers le haut"'+
                      'onclick="move_up_prescription(this.id);"'+
                      'class="arrow-up-16'+css_fleche_haut+'"></span>'+
            '</td>';
        // prescription réglementaire
        ligne += pr_input;
        ligne += td_pr;
        // prescription spécifique
        ligne += input_prescription(ordre, "ps_desc", ps_desc);
        if (ps_desc === '') {
            ps_desc = '<i>Aucune</i>';
        }
        ligne += '<td class="col-3 lastcol" id="td_ps_'+ordre+'">'+
            ps_desc+'</td></tr>';

        // On ajoute la ligne
        $("#tableau_prescriptions").append(ligne);

        // On gère les flêches de la précédente si elle existe
        if (ordre > 1) {
            var ordre_precedent = ordre - 1;
            var btn_bas_precedent = $('#move_down_prescription_' + ordre_precedent);
            btn_bas_precedent.show();
            if (ordre_precedent == 1) {
                var btn_haut_precedent = $('#move_up_prescription_' + ordre_precedent);
                btn_haut_precedent.hide();
            }
        }

        // On ferme le formulaire
        back_from_prescription();
    } else {
        // Sinon la prescription spécifique n'est oas
        // renseignée : on affiche un message d'erreur
        $(".erreur_prescription").show();
    }
}

/**
 * Appelée à la validation du formulaire de modification d'une prescription,
 * cette fonction vérifie qu'une prescription réglementaire est renseignée.
 * Si oui elle met à jour la prescription concernée dans le tableau de la vue
 * d'édition des prescriptions, sinon elle affiche un message d'erreur.
 * 
 * @param  [integer] ordre [Ordre de la prescription éditée]
 * 
 * @return [void]
 */
function edit_prescription(ordre) {
    // On vérifie qu'une prescription est renseignée
    var ps_desc = "";
    //
    var pr_id = $('select#prescription_reglementaire option:selected').val();
    //
    var ps_html = "ps_description_om_html";
    //
    if (tinyMCE.editors[ps_html]) {
        ps_desc = tinyMCE.get(ps_html).getContent();
    }
    // Si prescription réglementaire sélectionnée
    if (pr_id !== '' &&
        typeof pr_id !== 'undefined' &&
        (isNaN(pr_id) === false)) {

        var td_pr = "";
        var def = $("#pr_defavorable").val();
        td_pr += '<p>Défavorable : <span style="font-weight: bold;">'+def+'</span></p>';
        var pr_html = "pr_description_om_html";
        var pr_desc = $('#pr_description_om_html').html();
        td_pr += pr_desc;
        var pr_def = 'f';
        if ($("#pr_defavorable").val() == 'Oui' ||
            $("#pr_defavorable").val() == 'oui' ||
            $("#pr_defavorable").val() == 't') {
            pr_def = 't';
        }
        // inputs
        $("#pr_id_"+ordre).val(escapeHtml(pr_id));
        $("#pr_def_"+ordre).val(escapeHtml(pr_def));
        $("#pr_desc_"+ordre).val(pr_desc);
        $("#td_pr_"+ordre).empty();
        $("#td_pr_"+ordre).append(td_pr);
        // On modifie l'éventuelle prescription spécifique
        // input
        $("#ps_desc_"+ordre).val(ps_desc);
        // cellule
        $("#td_ps_"+ordre).empty();
        if (ps_desc === '') {
            ps_desc = '<i>Aucune</i>';
        }
        $("#td_ps_"+ordre).append(ps_desc);
        // On ferme e formulaire
        back_from_prescription();
    } else {
        // Sinon la prescription spécifique n'est oas
        // renseignée : on affiche un message d'erreur
        $(".erreur_prescription").show();
    }
}

/**
 * Lors du clic sur le bouton annuler du form d'ajout/modification
 * d'une prescription on revient au tableau des prescriptions.
 * 
 * @return [void]
 */
function back_from_prescription() {
    $("#form-prescription").remove();
    $("#sousform-analyses").show();
}

/**
 * Construit l'input caché d'un champ d'une prescription.
 * 
 * @param  [string] ordre  Ordre de la prescription
 * @param  [string] champ  Identifiant du champ
 * @param  [string] valeur Valeur du champ
 * 
 * @return [string]        Code HTML de l'input
 */
function input_prescription(ordre, champ, valeur) {
    valeur = escapeHtml(valeur);
    var input = '<input id="'+champ+'_'+ordre+'"';
    input += ' type="hidden"';
    input += ' value=\''+valeur+'\'';
    input += ' name="'+champ+'['+ordre+']"/>';
    return input;
}

/**
 * Échappe les caractères spéciaux d'un champ HTML.
 * 
 * @param  [string] text [texte à échapper]
 * 
 * @return [string]      [texte échappé]
 */
function escapeHtml(text) {
    if (text !== undefined) {
    return text
        .replace(/&/g,'&amp;')
        .replace(/</g,'&lt;')
        .replace(/>/g,'&gt;')
        .replace(/'/g,'&quot;')
        .replace(/"/g,'&#039;');
    }
}

/**
 * Action au changement du select de la prescription réglementaire :
 * on récupère les informations de la PR pour remplir les champs
 * si l'ID est valide, sinon on vide ces champs.
 *
 * @param [integer] id valeur de la clé primaire de la PR
 *
 * @return [void]
 */
function get_prescription_reglementaire(id) {
    //
    if (id === '' ||
        typeof id === 'undefined' ||
        (isNaN(id) === true)) {
        //
        $("#prescription_reglementaire").val("");
        $("#pr_description_om_html").html("");
        $("#pr_defavorable").prop('checked', false);
        $("#pr_defavorable").prop('value', 'Non');
        //
        $("#specifique-type").hide();
    } else {
        // lien script php
        link = "../scr/form.php?obj=prescription_reglementaire&action=4&idx="+id;
        $.ajax({
            type: "GET",
            url: link,
            cache: false,
            dataType: "json",
            success: function(data){
                //
                if (data.prescriptions_specifiques === true) {
                    $("#specifique-type").show();
                } else {
                    $("#specifique-type").hide();
                }
                //
                $("#pr_description_om_html ").html(data.desc);
                $("#pr_defavorable").prop('checked', data.check);
                $("#pr_defavorable").prop('value', data.def);
            }
        });
    }
}

/**
 * Incrémente l'ordre d'une prescription dans la liste.
 * 
 * @param  [string] id_css [ID CSS du bouton flèche haut déclencheur]
 * 
 * @return [void]
 */
function move_down_prescription(id_css) {
    var total_prescriptions = get_next_order_prescription() - 1;
    var ordre = id_css.replace("move_down_prescription_","");
    ordre = parseInt(ordre);
    var ordre_nouveau = ordre + 1;
    var ligne_deplacee = $("#prescription_"+ordre);
    var ligne_suivante = ligne_deplacee.next();
    var btn_supprimer_deplace = $('#delete_prescription_' + ordre);
    var btn_supprimer_suivant = $('#delete_prescription_' + ordre_nouveau);
    var btn_modifier_deplace = $('#edit_prescription_' + ordre);
    var btn_modifier_suivant = $('#edit_prescription_' + ordre_nouveau);
    var btn_bas_deplace = $('#move_down_prescription_' + ordre);
    var btn_bas_suivant = $('#move_down_prescription_' + ordre_nouveau);
    var btn_haut_deplace = $('#move_up_prescription_' + ordre);
    var btn_haut_suivant = $('#move_up_prescription_' + ordre_nouveau);
    var numero_deplace = $('#numero_' + ordre);
    var numero_suivant = $('#numero_' + ordre_nouveau);
    var pr_id_deplace = $('#pr_id_' + ordre);
    var pr_id_suivant = $('#pr_id_' + ordre_nouveau);
    var pr_def_deplace = $('#pr_def_' + ordre);
    var pr_def_suivant = $('#pr_def_' + ordre_nouveau);
    var pr_lib_deplace = $('#pr_lib_' + ordre);
    var pr_lib_suivant = $('#pr_lib_' + ordre_nouveau);
    var pr_tc1_deplace = $('#pr_tc1_' + ordre);
    var pr_tc1_suivant = $('#pr_tc1_' + ordre_nouveau);
    var pr_tc2_deplace = $('#pr_tc2_' + ordre);
    var pr_tc2_suivant = $('#pr_tc2_' + ordre_nouveau);
    var pr_desc_deplace = $('#pr_desc_' + ordre);
    var pr_desc_suivant = $('#pr_desc_' + ordre_nouveau);
    var ps_desc_deplace = $('#ps_desc_' + ordre);
    var ps_desc_suivant = $('#ps_desc_' + ordre_nouveau);
    var td_pr_deplace = $('#td_pr_' + ordre);
    var td_pr_suivant = $('#td_pr_' + ordre_nouveau);
    var td_ps_deplace = $('#td_ps_' + ordre);
    var td_ps_suivant = $('#td_ps_' + ordre_nouveau);
    var seconde = false;
    if (ordre_nouveau == 2) {
        seconde = true;
    }
    var derniere = false;
    if (ordre_nouveau == total_prescriptions) {
        derniere = true;
    }
    // disparition
    ligne_deplacee.fadeOut();
    ligne_suivante.fadeOut();
    // style
    ligne_deplacee.toggleClass("odd");
    ligne_deplacee.toggleClass("even");
    ligne_suivante.toggleClass("odd");
    ligne_suivante.toggleClass("even");
    // lignes de tableau
    ligne_deplacee.attr('id','prescription_' + ordre_nouveau);
    ligne_suivante.attr('id','prescription_' + ordre);
    // boutons supprimer
    btn_supprimer_deplace.attr('id','delete_prescription_' + ordre_nouveau);
    btn_supprimer_suivant.attr('id','delete_prescription_' + ordre);
    // boutons modifier
    btn_modifier_deplace.attr('id','edit_prescription_' + ordre_nouveau);
    btn_modifier_suivant.attr('id','edit_prescription_' + ordre);
    // boutons vers le bas
    if (derniere === true) {
        btn_bas_deplace.hide();
        btn_bas_suivant.show();
    }
    btn_bas_deplace.attr('id','move_down_prescription_' + ordre_nouveau);
    btn_bas_suivant.attr('id','move_down_prescription_' + ordre);
    // boutons vers le haut
    if (seconde === true) {
        btn_haut_deplace.show();
        btn_haut_suivant.hide();
    }
    btn_haut_deplace.attr('id','move_up_prescription_' + ordre_nouveau);
    btn_haut_suivant.attr('id','move_up_prescription_' + ordre);
    // numéros des prescriptions
    numero_deplace.text(ordre_nouveau);
    numero_deplace.attr('id','numero_' + ordre_nouveau);
    numero_suivant.text(ordre);
    numero_suivant.attr('id','numero_' + ordre);
    // inputs
    pr_id_deplace.attr('name','pr_id['+ordre_nouveau+']');
    pr_id_deplace.attr('id','pr_id_'+ordre_nouveau);
    pr_id_suivant.attr('name','pr_id['+ordre+']');
    pr_id_suivant.attr('id','pr_id_'+ordre);
    //
    pr_def_deplace.attr('name','pr_def['+ordre_nouveau+']');
    pr_def_deplace.attr('id','pr_def_'+ordre_nouveau);
    pr_def_suivant.attr('name','pr_def['+ordre+']');
    pr_def_suivant.attr('id','pr_def_'+ordre);
    //
    pr_lib_deplace.attr('name','pr_lib['+ordre_nouveau+']');
    pr_lib_deplace.attr('id','pr_lib_'+ordre_nouveau);
    pr_lib_suivant.attr('name','pr_lib['+ordre+']');
    pr_lib_suivant.attr('id','pr_lib_'+ordre);
    //
    pr_tc1_deplace.attr('name','pr_tc1['+ordre_nouveau+']');
    pr_tc1_deplace.attr('id','pr_tc1_'+ordre_nouveau);
    pr_tc1_suivant.attr('name','pr_tc1['+ordre+']');
    pr_tc1_suivant.attr('id','pr_tc1_'+ordre);
    //
    pr_tc2_deplace.attr('name','pr_tc2['+ordre_nouveau+']');
    pr_tc2_deplace.attr('id','pr_tc2_'+ordre_nouveau);
    pr_tc2_suivant.attr('name','pr_tc2['+ordre+']');
    pr_tc2_suivant.attr('id','pr_tc2_'+ordre);
    //
    pr_desc_deplace.attr('name','pr_desc['+ordre_nouveau+']');
    pr_desc_deplace.attr('id','pr_desc_'+ordre_nouveau);
    pr_desc_suivant.attr('name','pr_desc['+ordre+']');
    pr_desc_suivant.attr('id','pr_desc_'+ordre);
    //
    ps_desc_deplace.attr('name','ps_desc['+ordre_nouveau+']');
    ps_desc_deplace.attr('id','ps_desc_'+ordre_nouveau);
    ps_desc_suivant.attr('name','ps_desc['+ordre+']');
    ps_desc_suivant.attr('id','ps_desc_'+ordre);
    // cellules
    td_pr_deplace.attr('id','td_pr_'+ordre_nouveau);
    td_pr_suivant.attr('id','td_pr_'+ordre);
    //
    td_ps_deplace.attr('id','td_ps_'+ordre_nouveau);
    td_ps_suivant.attr('id','td_ps_'+ordre);
    // déplacement
    ligne_deplacee.insertAfter(ligne_suivante);
    // ré-apparition
    ligne_deplacee.fadeIn();
    ligne_suivante.fadeIn();
}

/**
 * Décrémente l'ordre d'une prescription dans la liste.
 * 
 * @param  [string] id_css [ID CSS du bouton flèche haut déclencheur]
 * @return [void]
 */
function move_up_prescription(id_css) {
    var total_prescriptions = get_next_order_prescription() - 1;
    var ordre = id_css.replace("move_up_prescription_","");
    ordre = parseInt(ordre);
    var ordre_nouveau = ordre - 1;
    var ligne_deplacee = $("#prescription_"+ordre);
    var ligne_precedente = ligne_deplacee.prev();
    var btn_supprimer_deplace = $('#delete_prescription_' + ordre);
    var btn_supprimer_precedent = $('#delete_prescription_' + ordre_nouveau);
    var btn_modifier_deplace = $('#edit_prescription_' + ordre);
    var btn_modifier_precedent = $('#edit_prescription_' + ordre_nouveau);
    var btn_bas_deplace = $('#move_down_prescription_' + ordre);
    var btn_bas_precedent = $('#move_down_prescription_' + ordre_nouveau);
    var btn_haut_deplace = $('#move_up_prescription_' + ordre);
    var btn_haut_precedent = $('#move_up_prescription_' + ordre_nouveau);
    var numero_deplace = $('#numero_' + ordre);
    var numero_precedent = $('#numero_' + ordre_nouveau);
    var pr_id_deplace = $('#pr_id_' + ordre);
    var pr_id_precedent = $('#pr_id_' + ordre_nouveau);
    var pr_def_deplace = $('#pr_def_' + ordre);
    var pr_def_precedent = $('#pr_def_' + ordre_nouveau);
    var pr_lib_deplace = $('#pr_lib_' + ordre);
    var pr_lib_precedent = $('#pr_lib_' + ordre_nouveau);
    var pr_tc1_deplace = $('#pr_tc1_' + ordre);
    var pr_tc1_precedent = $('#pr_tc1_' + ordre_nouveau);
    var pr_tc2_deplace = $('#pr_tc2_' + ordre);
    var pr_tc2_precedent = $('#pr_tc2_' + ordre_nouveau);
    var pr_desc_deplace = $('#pr_desc_' + ordre);
    var pr_desc_precedent = $('#pr_desc_' + ordre_nouveau);
    var ps_desc_deplace = $('#ps_desc_' + ordre);
    var ps_desc_precedent = $('#ps_desc_' + ordre_nouveau);
    var td_pr_deplace = $('#td_pr_' + ordre);
    var td_pr_precedent = $('#td_pr_' + ordre_nouveau);
    var td_ps_deplace = $('#td_ps_' + ordre);
    var td_ps_precedent = $('#td_ps_' + ordre_nouveau);
    var premiere = false;
    if (ordre_nouveau == 1) {
        premiere = true;
    }
    var avant_derniere = false;
    if (ordre_nouveau == total_prescriptions - 1) {
        avant_derniere = true;
    }
    // disparition
    ligne_deplacee.fadeOut();
    ligne_precedente.fadeOut();
    // style
    ligne_deplacee.toggleClass("odd");
    ligne_deplacee.toggleClass("even");
    ligne_precedente.toggleClass("odd");
    ligne_precedente.toggleClass("even");
    // lignes de tableau
    ligne_deplacee.attr('id','prescription_' + ordre_nouveau);
    ligne_precedente.attr('id','prescription_' + ordre);
    // boutons supprimer
    btn_supprimer_deplace.attr('id','delete_prescription_' + ordre_nouveau);
    btn_supprimer_precedent.attr('id','delete_prescription_' + ordre);
    // boutons modifier
    btn_modifier_deplace.attr('id','edit_prescription_' + ordre_nouveau);
    btn_modifier_precedent.attr('id','edit_prescription_' + ordre);
    // boutons vers le bas
    if (avant_derniere === true) {
        btn_bas_deplace.show();
        btn_bas_precedent.hide();
    }
    btn_bas_deplace.attr('id','move_down_prescription_' + ordre_nouveau);
    btn_bas_precedent.attr('id','move_down_prescription_' + ordre);
    // boutons vers le haut
    if (premiere === true) {
        btn_haut_deplace.hide();
        btn_haut_precedent.show();
    }
    btn_haut_deplace.attr('id','move_up_prescription_' + ordre_nouveau);
    btn_haut_precedent.attr('id','move_up_prescription_' + ordre);
    // numéros des prescriptions
    numero_deplace.text(ordre_nouveau);
    numero_deplace.attr('id','numero_' + ordre_nouveau);
    numero_precedent.text(ordre);
    numero_precedent.attr('id','numero_' + ordre);
    // inputs
    pr_id_deplace.attr('name','pr_id['+ordre_nouveau+']');
    pr_id_deplace.attr('id','pr_id_'+ordre_nouveau);
    pr_id_precedent.attr('name','pr_id['+ordre+']');
    pr_id_precedent.attr('id','pr_id_'+ordre);
    //
    pr_def_deplace.attr('name','pr_def['+ordre_nouveau+']');
    pr_def_deplace.attr('id','pr_def_'+ordre_nouveau);
    pr_def_precedent.attr('name','pr_def['+ordre+']');
    pr_def_precedent.attr('id','pr_def_'+ordre);
    //
    pr_lib_deplace.attr('name','pr_lib['+ordre_nouveau+']');
    pr_lib_deplace.attr('id','pr_lib_'+ordre_nouveau);
    pr_lib_precedent.attr('name','pr_lib['+ordre+']');
    pr_lib_precedent.attr('id','pr_lib_'+ordre);
    //
    pr_tc1_deplace.attr('name','pr_tc1['+ordre_nouveau+']');
    pr_tc1_deplace.attr('id','pr_tc1_'+ordre_nouveau);
    pr_tc1_precedent.attr('name','pr_tc1['+ordre+']');
    pr_tc1_precedent.attr('id','pr_tc1_'+ordre);
    //
    pr_tc2_deplace.attr('name','pr_tc2['+ordre_nouveau+']');
    pr_tc2_deplace.attr('id','pr_tc2_'+ordre_nouveau);
    pr_tc2_precedent.attr('name','pr_tc2['+ordre+']');
    pr_tc2_precedent.attr('id','pr_tc2_'+ordre);
    //
    pr_desc_deplace.attr('name','pr_desc['+ordre_nouveau+']');
    pr_desc_deplace.attr('id','pr_desc_'+ordre_nouveau);
    pr_desc_precedent.attr('name','pr_desc['+ordre+']');
    pr_desc_precedent.attr('id','pr_desc_'+ordre);
    //
    ps_desc_deplace.attr('name','ps_desc['+ordre_nouveau+']');
    ps_desc_deplace.attr('id','ps_desc_'+ordre_nouveau);
    ps_desc_precedent.attr('name','ps_desc['+ordre+']');
    ps_desc_precedent.attr('id','ps_desc_'+ordre);
    // cellules
    td_pr_deplace.attr('id','td_pr_'+ordre_nouveau);
    td_pr_precedent.attr('id','td_pr_'+ordre);
    //
    td_ps_deplace.attr('id','td_ps_'+ordre_nouveau);
    td_ps_precedent.attr('id','td_ps_'+ordre);
    // déplacement
    ligne_deplacee.insertBefore(ligne_precedente);
    // ré-apparition
    ligne_deplacee.fadeIn();
    ligne_precedente.fadeIn();
}

/**
 * Affiche un overlay de propositions d'ajout de prescriptions spécifiques-types.
 *
 * @return [void]
 */
function overlay_prescription_specifique_type() {
    var form = "overlay_prescription_specifique_type";
    var link = "../scr/form.php?obj=analyses&action=60&idx=0";
    var id_pr = $('select#prescription_reglementaire option:selected').val();
    if (id_pr !== '' && typeof id_pr !== 'undefined') {
        // insertion du conteneur du dialog
        var dialog = $('<div id="'+form+'"></div>').insertAfter('#tabs-1 .formControls');
        // exécution de la requete passée en paramètre
        $.ajax({
            type: "POST",
            url: link,
            data: "&id_pr=" + id_pr + "&",
            cache: false,
            success: function(html){
                // Suppression d'un precedent dialog
                dialog.empty();
                // Ajout du contenu recupere
                dialog.append(html);
                // Initialisation du theme OM
                om_initialize_content();
                // Creation du dialog
                $(dialog).dialog({
                    // OnClose suppression du contenu
                    close: function(ev, ui) {
                        $(this).remove();
                    },
                    resizable: true,
                    modal: true,
                    width: 'auto',
                    height: 'auto',
                    position: 'center',
                });
            },
            async : false
        });
        // Callback lors d'une validation
        $('#'+form+' form').on('submit',function() {
            add_description_type('ps_description_om_html', form, true);
            $(dialog).dialog('close').remove();
            return false;
        });
    }
}

/**
 * Supprime une prescription dans la vue d'édition uniquement.
 * 
 * @param  [string] id_css [ID CSS du bouton supprimer de la prescription]
 * 
 * @return [void]
 */
function delete_prescription(id_css) {
    var ordre = id_css.replace("delete_prescription_","");
    ordre = parseInt(ordre);
    // On récupère le nombre total de prescriptions
    var total_prescriptions = get_next_order_prescription() - 2;
    // On supprime la prescription
    $('#prescription_'+ordre).remove();
    // On décrémente les IDs des éléments des prescriptions suivantes s'il y en a
    if (total_prescriptions !== 0) {
        $('.une_prescription').each(
            function(){
                var id_prescription = this.id;
                var ordre_actuel = parseInt(id_prescription.replace('prescription_', ''));
                var ordre_nouveau = ordre_actuel;
                if (ordre_actuel > ordre) {
                    ordre_nouveau = ordre_actuel - 1;
                    // ligne de tableau
                    $('#prescription_' + ordre_actuel).attr('id','prescription_' + ordre_nouveau);
                    if (ordre_nouveau % 2 === 0) {
                        $('#prescription_' + ordre_nouveau).removeClass("odd");
                    } else {
                        $('#prescription_' + ordre_nouveau).addClass("odd");
                    }
                    // bouton modifier
                    $('#edit_prescription_' + ordre_actuel).attr('id','edit_prescription_' + ordre_nouveau);
                    // bouton supprimer
                    $('#delete_prescription_' + ordre_actuel).attr('id','delete_prescription_' + ordre_nouveau);
                    // numéro de la prescription
                    $('#numero_' + ordre_actuel).text(ordre_nouveau);
                    $('#numero_' + ordre_actuel).attr('id','numero_' + ordre_nouveau);
                    // bouton haut
                    $('#move_up_prescription_' + ordre_actuel).attr('id','move_up_prescription_' + ordre_nouveau);
                    // bouton bas
                    $('#move_down_prescription_' + ordre_actuel).attr('id','move_down_prescription_' + ordre_nouveau);
                }
                // flèche haut de la première
                if (ordre_nouveau == 1) {
                    $('#move_up_prescription_'+ordre_nouveau).hide();
                } else {
                    $('#move_up_prescription_'+ordre_nouveau).show();
                }
                // flèche bas de la dernière
                if (ordre_nouveau == total_prescriptions) {
                    $('#move_down_prescription_'+ordre_nouveau).hide();
                } else {
                    $('#move_down_prescription_'+ordre_nouveau).show();
                }
                // inputs
                $('#pr_id_'+ordre_actuel).attr('name','pr_id['+ordre_nouveau+']');
                $('#pr_id_'+ordre_actuel).attr('id','pr_id_'+ordre_nouveau);
                $('#pr_def_'+ordre_actuel).attr('name','pr_def['+ordre_nouveau+']');
                $('#pr_def_'+ordre_actuel).attr('id','pr_def_'+ordre_nouveau);
                $('#pr_lib_'+ordre_actuel).attr('name','pr_lib['+ordre_nouveau+']');
                $('#pr_lib_'+ordre_actuel).attr('id','pr_lib_'+ordre_nouveau);
                $('#pr_tc1_'+ordre_actuel).attr('name','pr_tc1['+ordre_nouveau+']');
                $('#pr_tc1_'+ordre_actuel).attr('id','pr_tc1_'+ordre_nouveau);
                $('#pr_tc2_'+ordre_actuel).attr('name','pr_tc2['+ordre_nouveau+']');
                $('#pr_tc2_'+ordre_actuel).attr('id','pr_tc2_'+ordre_nouveau);
                $('#pr_desc_'+ordre_actuel).attr('name','pr_desc['+ordre_nouveau+']');
                $('#pr_desc_'+ordre_actuel).attr('id','pr_desc_'+ordre_nouveau);
                $('#ps_desc_'+ordre_actuel).attr('name','ps_desc['+ordre_nouveau+']');
                $('#ps_desc_'+ordre_actuel).attr('id','ps_desc_'+ordre_nouveau);
                // cellules
                $('#td_pr_'+ordre_actuel).attr('id','td_pr_'+ordre_nouveau);
                $('#td_ps_'+ordre_actuel).attr('id','td_ps_'+ordre_nouveau);
            }
        );
    }
}

/**
 * Parcourt les prescriptions et retourne le prochain ordre disponible.
 * 
 * @return [integer] prochain ordre disponible
 */
function get_next_order_prescription() {
    // Variable de travail pour stocker la valeur la plus élevé du numéro 
    // d'ordre
    var next = 0;
    // On boucle sur chaque ligne de prescription du tableau qui représente 
    // une prescription
    $(".une_prescription").each(function() {
        // Le numéro d'ordre est dans l'attribut id de la balise tr avant la 
        // classe une_prescription
        var ordre = this.id;
        // Le numéro d'ordre est préfixé par la chaine "prescription_"
        ordre = ordre.replace("prescription_", "");
        // Le numéro d'ordre doit être un entier
        ordre = parseInt(ordre);
        // Si le numéro d'ordre recupéré est supérieur à la valeur actuelle
        // de la variable next alors c'est que le numéro d'ordre récupéré
        // est le plus élevé alors on l'affecte à la variable next
        if (ordre > next) {
            next = ordre;
        }
    });
    // On retourne le numéro d'ordre le plus élevé trouvé auquel on ajoute 
    // la valeur 1 pour avoir le suivant
    return next + 1;
}

// }}} END - ANALYSE - GESTION DES PRESCRIPTIONS

// {{{ BEGIN - GESTION DES DOCUMENTS GENERES

/**
 * Ouvre en overlay le formulaire permettant de sélectionner les textes-types
 * à copier.
 *
 * @param string link  Lien de l'action de l'objet
 * @param string champ Champ à remplir
 *
 * @return void
 */
function view_overlay_courrier_texte_type(link, champ, courrier_type) {
    var form = "form-courrier_texte_type-overlay";
    // insertion du conteneur du dialog
    var dialog = $('<div id="'+form+'"></div>').insertAfter('#tabs-1 .formControls');
    // exécution de la requete passée en paramètre
    $.ajax({
        type: "GET",
        url: link+"&champ="+champ+"&courrier_type="+courrier_type,
        cache: false,
        success: function(html){
            // Suppression d'un precedent dialog
            dialog.empty();
            // Ajout du contenu recupere
            dialog.append(html);
            // Initialisation du theme OM
            om_initialize_content();
            // Creation du dialog
            $(dialog).dialog({
                // OnClose suppression du contenu
                close: function(ev, ui) {
                    $(this).remove();
                },
                resizable: true,
                modal: true,
                width: 'auto',
                height: 'auto',
                position: 'center',
            });
        },
        async : false
    });
    // Callback lors d'une validation
    $('#'+form+' form').on('submit',function() {
        add_description_type(champ, form, true);
        $(dialog).dialog('close').remove();
        return false;
    });
}

/**
 * Change le filtre des textes-types.
 *
 * @param mixed selected Filtre
 *
 * @return void
 */
function change_filter_courrier_texte_type(selected) {
    //
    $("#courrier_texte_type_complement1").attr(
        'onclick', 
        "view_overlay_courrier_texte_type('../scr/form.php?obj=courrier&action=6&idx=0', 'complement1_om_html', '"+selected+"'); return false;"
    );
    //
    $("#courrier_texte_type_complement2").attr(
        'onclick', 
        "view_overlay_courrier_texte_type('../scr/form.php?obj=courrier&action=6&idx=0', 'complement2_om_html', '"+selected+"'); return false;"
    );
}

/**
 * Affiche ou cache le champ select multiple des autorités de police
 * sur le formulaire des courriers.
 *
 * @return void
 */
function display_autorites_police_liees() {

    // Valeur du select courrier_type
    var courrier_type = $('#courrier_type').val();

    // Lien de l'action
    var link = '../scr/form.php?obj=courrier&action=9&idx=0';

    // Traitement
    $.ajax({
        type: "POST",
        url: link,
        dataType: "json",
        data: "courrier_type="+courrier_type+"&",
        async: false,
        success: function(data){

            // Si le champ ne doit pas être affiché
            if (data.display === false) {

                // Supprime la valeur
                $("#autorites_police_liees").val("");
                // Cache le champ
                $("#autorites_police_liees").parent().parent().hide();

            } else {

                // Supprime la valeur
                $("#autorites_police_liees").val("");
                // Affiche le champ
                $("#autorites_police_liees").parent().parent().show();
            }
        }
    });
}

// }}} END - GESTION DES DOCUMENTS GENERES

// {{{ BEGIN - GESTION DES DOCUMENTS ENTRANTS

/**
 * Affiche ou cache les champs "liens" par rapport à la valeur
 * du champ choix_lien
 *
 * @param string value Valeur de choix_lien
 *
 * @return [void]
 */
function show_hide_fields_by_piece_choix_lien(value) {
    // Balise parent des champs
    var etablissement = $('#autocomplete-etablissement_tous').parent().parent();
    var dossier_coordination = $('#autocomplete-dossier_coordination').parent().parent();
    var dossier_instruction = $('#autocomplete-dossier_instruction').parent().parent();
    var service = $('#service').parent().parent();

    var type_input_service = $('#service').attr('type');

    // Si rien n'est sélectionné
    if (value === '') {
        // Cache/affiche les champs
        etablissement.hide();
        dossier_coordination.hide();
        dossier_instruction.hide();
        if (type_input_service !== 'hidden') {
            service.show();
        }
        // Initialise à vide les valeurs
        clear_autocomplete("autocomplete-etablissement_tous");
        clear_autocomplete("autocomplete-dossier_coordination");
        clear_autocomplete("autocomplete-dossier_instruction");
    }
    // Si etablissement est sélectionné
    if (value == 'etablissement') {
        // Cache/affiche les champs
        etablissement.show();
        dossier_coordination.hide();
        dossier_instruction.hide();
        if (type_input_service !== 'hidden') {
            service.show();
        }
        // Initialise à vide les valeurs
        clear_autocomplete("autocomplete-dossier_coordination");
        clear_autocomplete("autocomplete-dossier_instruction");
    }
    // Si dossier_coordination est sélectionné
    if (value == 'dossier_coordination') {
        // Cache/affiche les champs
        etablissement.hide();
        dossier_coordination.show();
        dossier_instruction.hide();
        if (type_input_service !== 'hidden') {
            service.show();
        }
        // Initialise à vide les valeurs
        clear_autocomplete("autocomplete-etablissement_tous");
        clear_autocomplete("autocomplete-dossier_instruction");
    }
    // Si dossier_instruction est sélectionné
    if (value == 'dossier_instruction') {
        // Cache/affiche les champs
        etablissement.hide();
        dossier_coordination.hide();
        dossier_instruction.show();
        service.hide();
        // Initialise à vide les valeurs
        clear_autocomplete("autocomplete-etablissement_tous");
        clear_autocomplete("autocomplete-dossier_coordination");
    }
}

/**
 * Mutateur du champ service dans le formulaire des pièces lors de la sélection
 * d'un dossier d'instruction.
 *
 * @return void
 */
function set_piece_service() {

    // Valeurs du dossier de d'instruction
    var dossier_instruction = $("#autocomplete-dossier_instruction-id").val();

    // Lien de l'action
    var link = '../scr/form.php?obj=piece&action=10&idx=0';

    // Traitement
    $.ajax({
        type: "POST",
        url: link,
        dataType: "json",
        data: "dossier_instruction="+dossier_instruction+"&",
        async: false,
        success: function(data){

            // Si la données retournée n'est pas vide
            if (data.service !== "") {

                // On modifie le service
                $("#service").val(data.service);
            }
        }
    });
}

// }}} END - GESTION DES DOCUMENTS ENTRANTS

/**
 * Permet d'afficher la synthèse d'une demande de passage en réunion sur le 
 * formulaire dossier_instruction_reunion.
 *
 * @return [void]
 */
function view_dossier_instruction_reunion_synthesis() {
    // Identifiant de la demande de passage crée récemment
    var id = $('#id_retour').val();
    // URL
    var link = '../scr/form.php?obj=dossier_instruction_reunion&action=100&idx='+id;
    //
    if($.isNumeric(id)) {
        $.ajax({
            type: "POST",
            url: link,
            cache: false,
            success: function(html) {
                // Affiche la synthèse de la demande de passage avant le bouton
                // d'ajout
                $(html).insertBefore('#add_dossier_instruction_reunion').fadeIn(500);
            },
            async:false
        });
    }
}

// {{{ BEGIN - GESTION DES CONTRAINTES

function appliquer_contraintes(objsf, link, formulaire) {
    // composition de la chaine data en fonction des elements du formulaire
    var data = "";
    if (formulaire) {
        for (i=0; i<formulaire.elements.length; i++) {
            data += formulaire.elements[i].name+"="+encodeURIComponent(formulaire.elements[i].value)+"&";
        }
    }
    
    // execution de la requete en POST
    $.ajax({
        type: "POST",
        url: link,
        cache: false,
        data: data,
        dataType: "json",
        success: function(html){
            // Efface le message
            $('.message').remove();
            // Affiche le message
            $('.subtitle').after(
                '<div ' +
                    'class="message ui-widget ui-corner-all ui-state-highlight ui-state-valid">' +
                    '<p>' +
                        '<span class="ui-icon ui-icon-info"></span>' +
                        '<span class="text">' +
                            html +
                        '</span>' +
                    '</p>' +
                '</div>'                    
            );
            // Décoche toutes les checkbox
            $(":checkbox").attr('checked', false);
            $(":checkbox").attr('value', '');
        }
    });
}

/**
 * Appelé dans le contexte de l'ajout ou modification d'un dossier de coordination.
 * Met à jour le champ caché de références cadastrales, puis affiche un overlay contenant
 * seulement un formulaire avec 2 select, limite et nature. Appelle ensuite
 * update_overlay_etablissements_proches_qualification() qui récupère le tableau d'étab.
 *
 * @return [void]
 */
function overlay_etablissements_proches_qualification() {

    // Met à jour le champ caché de références cadastrales avec les champs remplis
    getDataFieldReferenceCadastrale();
    references_cadastrales = $("#references_cadastrales").val();
    if (references_cadastrales == ''){
        alert('Aucune référence cadastrale fournie');
        return;
    }

    var dialog = $('<div id="overlay-etablissements-proches" role="dialog"></div>').insertAfter('#tabs-1 .formControls');

    $(dialog).dialog({
        //OnClose suppression du contenu
        close: function(ev, ui) {
            $(this).remove();
        },
        title: '',
        resizable: true,
        modal: true,
        width: 'auto',
        height: 'auto',
        position: 'top-left',
    });

    link = "../scr/form.php?obj=dossier_coordination&action=34&idx=0"

    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Ajout du contenu recupere
            dialog.html(html);
            // Initialisation du theme OM
            om_initialize_content();
        },
        async : false
    });
    // Fermeture du dialog lors d'un clic sur le bouton retour
    $('#overlay-etablissements-proches').on("click",'#close-button',function() {
        $(dialog).dialog('close').remove();
        return;
    });
    $('#overlay-etablissements-proches #nature').attr("disabled", "disabled");
    $('#overlay-etablissements-proches #limite').attr("disabled", "disabled");

    $("#overlay-etablissements-proches #limite").on("change", function() {
        update_overlay_etablissements_proches_qualification(dialog);
        return;
    });
    //
    $("#overlay-etablissements-proches #nature").on("change",function() {
        update_overlay_etablissements_proches_qualification(dialog);
        return;
    });
    update_overlay_etablissements_proches_qualification(dialog);

}

/**
 * Permet de récupérer un tableau contenant une liste d'établissements proches des
 * des références cadastrales saisies. Affiche ensuite ce tableau dans le dialog, et
 * rend chaque ligne du tableau cliquable.
 *
 * @return [void]
 */
function update_overlay_etablissements_proches_qualification(dialog) {
    //
    limite_id = $("#overlay-etablissements-proches #limite option:selected").val();
    limite_text = $("#overlay-etablissements-proches #limite option:selected").text();
    nature = $("#overlay-etablissements-proches #nature option:selected").val();

    $('#overlay-etablissements-proches #limite').attr("disabled", "disabled");
    $('#overlay-etablissements-proches #nature').attr("disabled", "disabled");
    $('#tab_etablissements_proches').html(msg_loading);

    link = "../scr/form.php?obj=dossier_coordination&action=35&idx=0"
        + "&parcelles=" + references_cadastrales
        + "&limite=" + limite_text
        + "&nature=" + nature;

    // Initialisation de la variable
    var results = true;
    var disable_fields = false;
    // Exécution de la requete passée en paramètre
    $.ajax({
        type: "GET",
        url: link,
        cache: false,
        success: function(html){
            // Ajout du contenu recupéré
            $('#tab_etablissements_proches').html(html);
            // Initialisation du theme OM
            om_initialize_content();
            // S'il n'y a pas d'établissements dans le tableau
            if (html.indexOf("Il n'y a aucun établissement proche des références cadastrales saisies.") >= 0
                || html.indexOf("Erreur SIG") >= 0) {
                disable_fields = true;
                results = false;
            }
            if (html.indexOf("Aucun enregistrement.") >= 0) {
                results = false;
            }
            // Si on n'est pas dans le cas où le SIG n'a renvoyé aucun établissement
            if (disable_fields === false) {
                $('#overlay-etablissements-proches #limite').removeAttr('disabled');
                $('#overlay-etablissements-proches #nature').removeAttr('disabled');
            }
            // Si on a des résultats à afficher dans le tableau, on rend chaque ligne cliquable
            if (results === true) {
                $("#overlay-etablissements-proches tr").on("click",function() {
                    etablissement_id = $(this).find('td:first').text().trim();
                    etablissement_label = $(this).find('td:nth-child(2)').text().trim();
                    return_etablissements_proches_qualification(dialog, etablissement_id, etablissement_label);
                    return;
                });
            }
        },
        async : false
    });

}

/**
 * Lors de la sélection d'un établissement dans l'overlay d'établissements proches, permet
 * de remplir automatiquement les champs du formulaire concernant l'établissement. 
 *
 * @return [void]
 */
function return_etablissements_proches_qualification(dialog, etablissement_id, etablissement_label){

    // on remplit le champ et la valeur
    $("#autocomplete-etablissement_tous-search").val(etablissement_label);
    $("#autocomplete-etablissement_tous-id").val(etablissement_id);
    // on exécute le onchange
    $("#autocomplete-etablissement_tous-id").trigger("change");
    // on affiche les boutons effacer et valider
    $("#autocomplete-etablissement_tous-empty").show();
    $("#autocomplete-etablissement_tous-check").show();
    //
    dialog.remove();
}


// }}} END - GESTION DES CONTRAINTES

// {{{ BEGIN - GESTION DES PROGRAMMATIONS DE VISITES

/**
 * Actions au clic des boutons du filtre établissements proches :
 *
 * - TOUS → on masque le formulaire de filtre et on recharge le tableau
 * - PROCHES → on affiche le formulaire de filtre
 * - VALIDER → on recharge le tableau filtré par les valeurs postées
 *
 * @param   action  bouton
 * @return  void
 */
function bloc_filtre_visite_etablissement(action) {
    //
    if (action !== 'tous'
        && action !== 'proches'
        && action !== 'valider') {
        return;
    }
    //
    if (action === 'tous') {
        $("#btn_proches").prop("disabled", false);
        $("#btn_proches").button("option", "disabled", false);
        $("#btn_tous").prop("disabled", true);
        $("#btn_tous").button("option", "disabled", true);
        toggle_widgets_proches();
        $('#tableau_propositions').DataTable().ajax.reload(null, true);
        return;
    }
    //
    if (action === 'proches') {
        $("#btn_proches").prop("disabled", true);
        $("#btn_proches").button("option", "disabled", true);
        $("#btn_tous").prop("disabled", false);
        $("#btn_tous").button("option", "disabled", false);
        toggle_widgets_proches();
        return;
    }
    //
    if (action === 'valider') {
        reload_datatable_with_filter(true);
        return;
    }
}

/**
 * Recharge éventuellement le tableau de propositions avec le filtre
 * établissements proches.
 *
 * Deux cas d'utilisations sont possibles. Pour chacun on récupère les
 * valeurs postées, on affiche un spinner le temps du traitement puis
 * si le filtre est effectué avec succès on recharge le tableau. La
 * différence se situe au niveau de la gestion du message de validation.
 *
 * 1. Validation du filtre par l'utilisateur (search vaut true)
 *
 * On affichage le message de validation (valide ou erreur) afin que
 * l'utilisateur soit informé du résultat de son action.
 * 
 * 2. Fermeture de l'overlay visite par l'utilisateur (search vaut false)
 *
 * On affichage un message vide afin de supprimer le spinner, vu qu'il s'agit
 * d'une réactualisation indirectement due à une action utilisateur.
 *
 * @param   bool  search  permet de spécifier le contexte
 * @return  void
 */
function reload_datatable_with_filter(search) {
    // Gestion IHM pendant traitement
    display_msg_programmation(msg_loading, 'info');
    toggle_widgets_proches();
    //
    var rayon = $('select#rayon option:selected').val();
    var code_etab = $('input#code_etab').val();
    var idx = $("#bloc_tableau input#programmation").val();
    //
    $.ajax({
        type: "POST",
        url: "../scr/form.php?obj=programmation&action=17&idx="+idx,
        cache: false,
        data: "rayon="+rayon+"&code_etab="+code_etab,
        dataType: "json",
        success: function(data) {
            toggle_widgets_proches();
            if (data.status === 'OK') {
                var table = $('#tableau_propositions').dataTable();
                table.api().clear().draw();
                if (data.result !== null) {
                    $.each(data.result, function(index, value){
                        table.api().row.add(value).draw().node();
                    });
                }
                if (search === true) {
                    display_msg_programmation(data.message, 'valid');
                    return;
                }
                display_msg_programmation('', 'info');
            }
            if (search === true) {
                display_msg_programmation(data.message, 'error');
                return;
            }
            display_msg_programmation('', 'info');
        }
    });
}

/**
 * Grise ou active les widgets de formulaire relatifs au filtre
 * des établissements proches.
 * @return  void
 */
function toggle_widgets_proches() {
    $(".bloc_filtre_visite_etablissement_proches_fields").toggle();
}

/**
 * Affiche les messages de la vue "Programmer les visites".
 * 
 * @param   string  msg   texte
 * @param   string  type  style (valid/error/info)
 * @return  void
 */
function display_msg_programmation(msg, type) {
    if (type === 'info') {
        $('#msg_view_programmation').html(msg);
        return;
    }
    $('#msg_view_programmation').html(
        '<div ' +
            'class="message ui-widget ui-corner-all ui-state-highlight ui-state-'+type+'">' +
            '<p>' +
                '<span class="ui-icon ui-icon-info"></span>' +
                '<span class="text">' + msg + '</span>' +
            '</p>' +
        '</div>'
    );
}

// }}} END - GESTION DES PROGRAMMATIONS DE VISITES


/**
 * Ouvre un overlay dont le contenu est chargé depuis le retour d'un script PHP.
 *
 * @param string  title     Titre de l'overlay.
 * @param string  link      Lien du script PHP.
 * @param string  msg_error Message d'erreur affiché à l'utilisateur en cas
 *                          d'erreur JS.
 * @param string  width     Largeur de l'overlay.
 * @param string  height    Hauteur de l'overlay.
 * @param string  position  Position de l'overlay.
 * @param boolean resizable Overlay redimensionnable.
 * @param boolean modal     Overlay modal (le reste de la page est
 *                          unitilisable).
 *
 * @return mixed
 */
function overlay_load_form(title, link, msg_error, width, height, position, resizable, modal) {
    // Valeurs par défaut
    if (typeof(width) == 'undefined') {
        width = 'auto';
    }
    //
    if (typeof(height) == 'undefined') {
        height = 'auto';
    }
    //
    if (typeof(position) == 'undefined') {
        position = 'center';
    }
    //
    if (typeof(resizable) == 'undefined') {
        resizable = true;
    }
    //
    if (typeof(modal) == 'undefined') {
        modal = true;
    }

    // Définit le dialog
    var dialog = $("<div id=\"overlay-container\" role=\"dialog\"></div>").insertAfter('#footer');

    // Par défaut un spinner est affiché dans le dialog
    dialog.empty();
    dialog.append(msg_loading);

    // Affichage du dialog
    $(dialog).dialog({
        // À la fermeture du dialog
        close: function(ev, ui) {
            // Suppression du contenu
            $(this).remove();
        },
        title: title,
        resizable: resizable,
        modal: modal,
        width: width,
        height: height,
        position: position
    });

    // Traitement ajax
    $.ajax({
        type: "GET",
        url: link,
        success: function(html) {
            // Chargement du retour de l'url dans le conteneur dialog
            dialog.empty();
            dialog.append(html);
            // initilisation du contenu
            om_initialize_content();
        },
        error: function() {
            // En cas d'erreur, affiche le message d'erreur passé en paramètre
            dialog.empty();
            dialog.append(msg_error);
        }
    });

    // Fermeture du dialog lors d'un clic sur le bouton retour
    dialog.on("click",'a.linkjsclosewindow',function() {
        $(dialog).dialog('close').remove();
        return false;
    });
}


/**
 * Permet d'afficher ou de cacher le formulaire d'ajout de contacts dans le
 * contexte de l'overlay des propriétaires récupérés par parcelles.
 *
 * @param string mode Affiche (show) ou cache (hide) le formulaire.
 * @param string link Lien script PHP pour afficher le formulaire d'ajout de
 *                    contact.
 *
 * @return void
 */
function show_hide_plot_owner_add_form_contact(mode, link) {

    // Le lien n'est pas obligatoire dans le cas où le formulaire est caché.
    if (typeof(link) == 'undefined') {
        link = null;
    }

    // Le mode doit obligatoirement être définit
    if (mode !== 'show' && mode !== 'hide') {
        return;
    }

    // Dans le cas d'un affichage, le lien doit être obligatoirement définit
    if (mode === 'show' && link === null) {
        return;
    }

    // Affiche le formulaire et désactive le bouton d'ajout de contact
    if (mode === 'show') {
        //
        $('#plot_owner_add_btn_contact').attr("disabled", "disabled");
        //
        $.ajax({
            type: "GET",
            url: link,
            success: function(html) {
                // Chargement du retour de l'url dans le conteneur dialog
                $('#plot_owner_add_form_contact').empty();
                $('#plot_owner_add_form_contact').append(html);
                // initilisation du contenu
                om_initialize_content();
            }
        });
        //
        $('#plot_owner_add_form_contact').show();

    }

    // Cache le formulaire et active le bouton d'ajout de contact
    if (mode === 'hide') {
        //
        $('#plot_owner_add_btn_contact').removeAttr("disabled");
        $('#plot_owner_add_form_contact').hide();
        $('#plot_owner_add_form_contact').empty();
    }
}

jQuery.fn.exists = function(){return this.length>0;}
