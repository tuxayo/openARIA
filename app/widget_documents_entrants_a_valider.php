<?php
/**
 * WIDGET DASHBOARD - widget_documents_entrants_a_valider.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("piece_a_valider", "piece_a_valider_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_documents_entrants_a_valider();

} else {

    //
    $widget_is_empty = true;

}

?>
