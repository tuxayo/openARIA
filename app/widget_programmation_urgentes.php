<?php
/**
 * WIDGET DASHBOARD - widget_programmation_urgentes.
 * 
 * Liste des cinq programmations urgentes.
 *
 * L'objectif de ce widget est de ...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("programmation", "programmation_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_programmations_urgentes();

} else {

    //
    $widget_is_empty = true;

}

?>
