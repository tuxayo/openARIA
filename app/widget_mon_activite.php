<?php
/**
 * WIDGET DASHBOARD - widget_mon_activite.
 *
 * Mon activité.
 *
 * L'objet de ce widget est de présenter les statistiques de l'utilisateur
 * connecté.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instanciée
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
require_once "../obj/pilotage.class.php";
$p = new pilotage();
//
$p->view_widget_mon_activite();

?>
