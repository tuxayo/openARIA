<?php
/**
 * WIDGET DASHBOARD - widget_etablissements_npai.
 * 
 * Liste des établissements NPAI.
 *
 * L'objet de ce widget est de permettre de visualiser les 
 * établissements dont l'adresse de contact est incorrecte afin d'effectuer
 * des recherches hors logiciel.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("etablissement_tous", "etablissement_tous_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_etablissements_npai();

} else {

    //
    $widget_is_empty = true;

}

?>
