<?php
/**
 * WIDGET DASHBOARD - widget_dossier_coordination_a_qualifier.
 * 
 * Liste les cinq plus anciens dossiers de coordination à qualifier.
 *
 * L'objectif de ce widget est de ...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
require_once "../obj/pilotage.class.php";
$p = new pilotage();

/**
 *
 */
//
$template_panel_main_elem = '
            <li>
                <span class="size-h3 box-icon rounded bg-danger">%s</span>
                <p class="text-muted">%s</p>
            </li>';
//
$template_panel_other_elem = '
            <li>
                <p class="size-h3">%s</p>
                <p class="text-muted">%s</p>
            </li>';

/**
 *
 */
//
if ($f->isAccredited(
        array("dossier_coordination_a_qualifier", "dossier_coordination_a_qualifier_tab"), 
        "OR"
    )) {

    //
    $redlimit = 15;
    if (isset($f) && $f->getParameter("dc_a_qualifier_redlimit")) {
        $redlimit = intval($f->getParameter("dc_a_qualifier_redlimit"));
    }
    //
    $case_ddp = "case 
    when dossier_coordination.date_demande < now() - interval '".intval($redlimit)." days'
        then 'rouge'
    when dossier_coordination.depot_de_piece is TRUE
        then 'vert'
    end";
    /**
     * Listing des cinq plus anciens dossiers de coordination à qualifier
     */
    // Requête SQL
    $sql = "
    SELECT
        dossier_coordination.dossier_coordination,
        dossier_coordination.libelle as libelle,
        CONCAT(etablissement.code,' - ',etablissement.libelle) as etablissement,
        to_char(dossier_coordination.date_demande,'DD/MM/YYYY') as date_demande,
        ".$case_ddp." as ddp

    FROM ".DB_PREFIXE."dossier_coordination 
        LEFT JOIN ".DB_PREFIXE."etablissement 
            ON dossier_coordination.etablissement=etablissement.etablissement
    WHERE
        dossier_coordination.a_qualifier = TRUE
    ORDER BY date_demande
    LIMIT 5
    ";
    // Exécution de la requête et récupération des lignes résultats de la requête
    $results = $p->get_db_query_all($sql);
    // Définition des attributs du tableau
    $results["attr"] = array(
        // On définit les entêtes de colonnes
        "head" => array(
            $p->sprint_icon_info(_("Les 5 plus anciens dossiers de coordination à qualifier")),
            _("DC"),
            _("Etablissement"),
            _("Demande")
        ),
        // On stocke le nombre de ligne résultats de la requête
        "count" => count($results),
    );
    // Les lignes résultat ne contiennent aucun lien
    // On boucle donc sur le tableau pour rajouter les liens vers
    // les éléments
    foreach ($results as $key_line => $line) {
        //
        if ($key_line === "attr") {
            continue;
        }
        // Le lien est identique pour chaque élément de la ligne
        $href = sprintf(
            'form.php?obj=dossier_coordination_a_qualifier&amp;action=3&amp;idx=%s&amp;idz=%s',
            $line["dossier_coordination"],
            $line["libelle"]
        );
        if ($line["ddp"] != "") {
            $results[$key_line]["attr"] = array(
                "class" => "ligne-".$line["ddp"],
            );
        }
        // On remplace le contenu de chaque cellule par le lien
        foreach ($line as $key_cell => $cell) {
            if ($key_cell == "ddp") {
                unset($results[$key_line][$key_cell]);
                continue;
            }
            $results[$key_line][$key_cell] = $p->sprint_link(array(
                "href" => $href,
                "libelle" =>  ($key_cell == "dossier_coordination" ? "->" : $cell),
            ));
        }
    }
    // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
    // résultat ou d'afficher un lien vers le listing complet des éléments 
    // en question
    if ($results["attr"]["count"] == 0) {
        $last_line_content = _("Aucun dossier de coordination à qualifier.");
    } else {
        $params = array(
            "href" => "../scr/tab.php?obj=dossier_coordination_a_qualifier",
            "libelle" => $p->template_icon_consulter." "._("Voir tous les dossiers de coordination a qualifier"),
        );
        $last_line_content = $p->sprint_link($params);
    }
    $results[] = array(
        "lastline" => array(
            "content" => $last_line_content,
            "colspan" => count($results["attr"]["head"]),
        ),
    );

    /**
     * Statistiques sur le nombre de dossiers de coordination à qualifier
     */
    // Requête SQL
    $sql = "
    SELECT
        dossier_type.libelle,
        count(dossier_coordination.dossier_coordination)
    FROM ".DB_PREFIXE."dossier_coordination
        LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
                ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
        LEFT JOIN ".DB_PREFIXE."dossier_type 
                ON dossier_coordination_type.dossier_type=dossier_type.dossier_type
    WHERE
        dossier_coordination.a_qualifier = TRUE
    GROUP BY dossier_type.dossier_type
    ";
    // Exécution de la requête et récupération des lignes résultats de la requête
    $stats = $p->get_db_query_all($sql);
    //
    $main_elem = array(
        "count" => 0,
        "libelle" => "DC "._("a_qualifier"),
    );
    // Récupération des enregistrements
    $others_elems = array();
    foreach ($stats as $key => $value) {
        $main_elem["count"] += $value["count"];
        $others_elems[] = array(
            "count" => $value["count"],
            "libelle" => "DC ".$value["libelle"],
        );
    }

    /**
     *
     */
    //
    $panel_main_elem = sprintf(
        $template_panel_main_elem,
        $main_elem["count"],
        $main_elem["libelle"]
    );
    //
    $panel_others_elems = "";
    foreach ($others_elems as $key => $value) {
        $panel_others_elems .= sprintf(
            $template_panel_other_elem,
            $value["count"],
            $value["libelle"]
        );
    }
    //
    $panel = sprintf(
        $p->template_panel,
        $panel_main_elem." ".$panel_others_elems
    );

    /**
     *
     */
    //
    echo $panel;
    //
    echo $p->sprint_table($results);

}

?>
