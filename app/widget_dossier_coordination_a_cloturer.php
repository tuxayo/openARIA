<?php
/**
 * WIDGET DASHBOARD - widget_dossier_coordination_a_cloturer.
 * 
 * Liste les cinq plus anciens dossiers de coordination à clore.
 *
 * L'objectif de ce widget est de signaler aux cardes les Dossier de Coordination non clos,
 *  qui ne contiennent plus aucun Dossier d'Instruction ouvert
 *
 * @package openaria
 * @version SVN : $Id: widget_dossier_coordination_a_cloturer.php 957 2015-02-12 10:33:17Z fmichon $
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
require_once "../obj/pilotage.class.php";
$p = new pilotage();

/**
 *
 */
//
$template_panel_main_elem = '
            <li>
                <span class="size-h3 box-icon rounded bg-danger">%s</span>
                <p class="text-muted">%s</p>
            </li>';
//
$template_panel_other_elem = '
            <li>
                <p class="size-h3">%s</p>
                <p class="text-muted">%s</p>
            </li>';

/**
 *
 */
//
if ($f->isAccredited(
        array("dossier_coordination_a_cloturer", "dossier_coordination_a_cloturer_tab"),
        "OR"
    )) {

    /**
     * Listing des cinq plus anciens dossiers de coordination à clore
     */
    // Requête SQL
    $sql = "
    SELECT dc.dossier_coordination, 
           dc.libelle as libelle,
           CONCAT(et.code,' - ',et.libelle) as etablissement,
           to_char(dc.date_demande,'DD/MM/YYYY') as date_demande
    FROM ".DB_PREFIXE."dossier_coordination dc 
    LEFT JOIN ".DB_PREFIXE."etablissement et
            ON dc.etablissement=et.etablissement
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type dct
            ON dc.dossier_coordination_type=dct.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type dt
            ON dct.dossier_type=dt.dossier_type
    WHERE dt.code='PLAN'
      AND dc.dossier_cloture IS FALSE
      AND dc.dossier_coordination NOT IN (
        SELECT dossier_coordination
        FROM ".DB_PREFIXE."dossier_instruction
        WHERE  dossier_instruction.dossier_cloture IS FALSE
        GROUP BY dossier_coordination HAVING count(*)>0
      )
    ORDER BY date_demande
    LIMIT 5
    ";
    // Exécution de la requête et récupération des lignes résultats de la requête
    $results = $p->get_db_query_all($sql);
    // Définition des attributs du tableau
    $results["attr"] = array(
        // On définit les entêtes de colonnes
        "head" => array(
            $p->sprint_icon_info(_("Les 5 plus anciens dossiers de coordination à clôturer")),
            _("DC"),
            _("Etablissement"),
            _("Demande")
        ),
        // On stocke le nombre de ligne résultats de la requête
        "count" => count($results),
    );
    // Les lignes résultat ne contiennent aucun lien
    // On boucle donc sur le tableau pour rajouter les liens vers
    // les éléments
    foreach ($results as $key_line => $line) {
        //
        if ($key_line === "attr") {
            continue;
        }
        // Le lien est identique pour chaque élément de la ligne
        $href = sprintf(
            'form.php?obj=dossier_coordination&amp;action=3&amp;idx=%s&amp;idz=%s',
            $line["dossier_coordination"],
            $line["libelle"]
        );
        // On remplace le contenu de chaque cellule par le lien
        foreach ($line as $key_cell => $cell) {
            //
            $results[$key_line][$key_cell] = $p->sprint_link(array(
                "href" => $href,
                "libelle" =>  ($key_cell == "dossier_coordination" ? "->" : $cell),
            ));
        }
    }
    // La dernière ligne du tableau permet d'afficher qu'il n'y a aucun
    // résultat ou d'afficher un lien vers le listing complet des éléments 
    // en question
    if ($results["attr"]["count"] == 0) {
        $last_line_content = _("Aucun dossier de coordination à clôturer.");
    } else {
        $params = array(
            "href" => "../scr/tab.php?obj=dossier_coordination_a_cloturer",
            "libelle" => $p->template_icon_consulter." "._("Voir tous les dossiers de coordination à clôturer"),
        );
        $last_line_content = $p->sprint_link($params);
    }
    $results[] = array(
        "lastline" => array(
            "content" => $last_line_content,
            "colspan" => count($results["attr"]["head"]),
        ),
    );

    /**
     * Statistiques sur le nombre de dossiers de coordination à clore
     */
    // Requête SQL
    $sql = "
    SELECT
        dct.code,
        count(dc.dossier_coordination)
        FROM ".DB_PREFIXE."dossier_coordination dc 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type dct
            ON dc.dossier_coordination_type=dct.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type dt
            ON dct.dossier_type=dt.dossier_type
    WHERE dt.code='PLAN'
      AND NOT dc.dossier_cloture 
      AND dc.dossier_coordination NOT IN (
        SELECT dossier_coordination
        FROM ".DB_PREFIXE."dossier_instruction
        WHERE  dossier_instruction.dossier_cloture IS FALSE
        GROUP BY dossier_coordination HAVING count(*)>0
      )
    GROUP BY dct.code
    ";
    // Exécution de la requête et récupération des lignes résultats de la requête
    $stats = $p->get_db_query_all($sql);
    //
    $main_elem = array(
        "count" => 0,
        "code" => "DC "._("à cloturer"),
    );
    // Récupération des enregistrements
    $others_elems = array();
    foreach ($stats as $key => $value) {
        $main_elem["count"] += $value["count"];
        $others_elems[] = array(
            "count" => $value["count"],
            "code" => "DC ".$value["code"],
        );
    }

    /**
     *
     */
    //
    $panel_main_elem = sprintf(
        $template_panel_main_elem,
        $main_elem["count"],
        $main_elem["code"]
    );
    //
    $panel_others_elems = "";
    foreach ($others_elems as $key => $value) {
        $panel_others_elems .= sprintf(
            $template_panel_other_elem,
            $value["count"],
            $value["code"]
        );
    }
    //
    $panel = sprintf(
        $p->template_panel,
        $panel_main_elem." ".$panel_others_elems
    );

    /**
     *
     */
    //
    echo $panel;
    //
    echo $p->sprint_table($results);

}

?>
