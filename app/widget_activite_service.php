<?php
/**
 * WIDGET DASHBOARD - widget_activite_service.
 *
 * L'activité du service.
 *
 * L'objet de ce widget est de présenter les statistiques du service 
 * de l'utilisateur.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instanciée
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
require_once "../obj/pilotage.class.php";
$p = new pilotage();
//
$p->view_widget_activite_service();

?>
