<?php
/**
 * WIDGET DASHBOARD - widget_autorites_police_non_notifiees_executees.
 * 
 * Autorités de police non notifiées ou exécutées.
 *
 * L'objet de ce widget est de permettre de visualiser les 
 * autorités de police non notifiées ou exécutées.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("autorite_police_non_notifiee_executee", "autorite_police_non_notifiee_executee_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_autorites_police_non_notifiees_executees();

} else {

    //
    $widget_is_empty = true;

}

?>
