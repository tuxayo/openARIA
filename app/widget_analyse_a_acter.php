<?php
/**
 * WIDGET DASHBOARD - widget_analyse_a_acter.
 *
 * L'objet de ce widget est de permettre au cadre de visualiser les 
 * analyses terminées et donc à valider.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("analyses", "analyses_consulter"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_analyses_a_acter();

} else {

    //
    $widget_is_empty = true;

}

?>
