<?php
/**
 * WIDGET DASHBOARD - widget_mes_visites_a_realiser.
 *
 * Mes visites a realiser.
 *
 * Liste des établissements programmés avec date et horaire de la visite, 
 * triés par date croissante.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(array("visite_mes_visites_a_realiser", "visite_mes_visites_a_realiser_tab"), "OR")) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_mes_visites_a_realiser();

} else {

    //
    $widget_is_empty = true;

}

?>
