<?php
/**
 * WIDGET DASHBOARD - widget_mes_infos.
 *
 * Mes infos.
 *
 * Informations sur l'utilisateur connecté : profil, service auquel il 
 * appartient, acteur rattaché, ...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
require_once "../obj/pilotage.class.php";
$p = new pilotage();
//
$p->view_widget_mes_infos();

?>
