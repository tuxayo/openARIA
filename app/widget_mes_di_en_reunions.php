<?php
/**
 * WIDGET DASHBOARD - widget_mes_di_en_reunions.
 *
 * Mes dossiers en réunion.
 *
 * L'objet de ce widget est de permettre au technicien de visualiser les 
 * dossiers dont il est l'instructeur et qui vont passer en réunion dans 
 * le mois suivant la date du jour.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
require_once "../obj/utils.class.php";

// Si utils n'est pas instancié
if (!isset($f)) {
    // Instanciation de la classe utils
    $f = new utils(null);
}

/**
 *
 */
//
if ($f->isAccredited(
        array("dossier_instruction", "dossier_instruction_tab"),
        "OR"
    )) {

    /**
     *
     */
    require_once "../obj/pilotage.class.php";
    $p = new pilotage();
    //
    $widget_is_empty = $p->view_widget_mes_di_en_reunions();

} else {

    //
    $widget_is_empty = true;

}

?>
