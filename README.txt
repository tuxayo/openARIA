openARIA ReadMe
===============

openARIA est un logiciel libre permettant l'Analyse du Risque Incendie et de
l'Accessibilité pour les Établissements Recevant du Public. Il gère le
Référentiel des ERP, les visites, les analyses de plan, les décisions, les
commissions, les autorités de police et bien plus encore.

Toutes les informations sur http://www.openmairie.org/


Documentation
-------------

Toutes les instructions pour l'installation, l'utilisation et la prise en main
du logiciel dans la documentation en ligne :
http://docs.openmairie.org/?project=openaria&version=1.2&format=html


Licence
-------

Voir le fichier LICENSE.txt


