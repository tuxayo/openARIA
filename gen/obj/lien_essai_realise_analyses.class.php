<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_essai_realise_analyses_gen extends om_dbform {

    var $table = "lien_essai_realise_analyses";
    var $clePrimaire = "lien_essai_realise_analyses";
    var $typeCle = "N";
    var $required_field = array(
        "analyses",
        "essai_realise",
        "lien_essai_realise_analyses"
    );
    
    var $foreign_keys_extended = array(
        "analyses" => array("analyses", ),
        "essai_realise" => array("essai_realise", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_essai_realise_analyses'])) {
            $this->valF['lien_essai_realise_analyses'] = ""; // -> requis
        } else {
            $this->valF['lien_essai_realise_analyses'] = $val['lien_essai_realise_analyses'];
        }
        if (!is_numeric($val['essai_realise'])) {
            $this->valF['essai_realise'] = ""; // -> requis
        } else {
            $this->valF['essai_realise'] = $val['essai_realise'];
        }
        if (!is_numeric($val['analyses'])) {
            $this->valF['analyses'] = ""; // -> requis
        } else {
            $this->valF['analyses'] = $val['analyses'];
        }
        if ($val['concluant'] == 1 || $val['concluant'] == "t" || $val['concluant'] == "Oui") {
            $this->valF['concluant'] = true;
        } else {
            $this->valF['concluant'] = false;
        }
            $this->valF['complement'] = $val['complement'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_essai_realise_analyses", "hidden");
            if ($this->is_in_context_of_foreign_key("essai_realise", $this->retourformulaire)) {
                $form->setType("essai_realise", "selecthiddenstatic");
            } else {
                $form->setType("essai_realise", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("concluant", "checkbox");
            $form->setType("complement", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_essai_realise_analyses", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("essai_realise", $this->retourformulaire)) {
                $form->setType("essai_realise", "selecthiddenstatic");
            } else {
                $form->setType("essai_realise", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("concluant", "checkbox");
            $form->setType("complement", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_essai_realise_analyses", "hiddenstatic");
            $form->setType("essai_realise", "selectstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("concluant", "hiddenstatic");
            $form->setType("complement", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_essai_realise_analyses", "static");
            $form->setType("essai_realise", "selectstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("concluant", "checkboxstatic");
            $form->setType("complement", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_essai_realise_analyses','VerifNum(this)');
        $form->setOnchange('essai_realise','VerifNum(this)');
        $form->setOnchange('analyses','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_essai_realise_analyses", 11);
        $form->setTaille("essai_realise", 11);
        $form->setTaille("analyses", 11);
        $form->setTaille("concluant", 1);
        $form->setTaille("complement", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_essai_realise_analyses", 11);
        $form->setMax("essai_realise", 11);
        $form->setMax("analyses", 11);
        $form->setMax("concluant", 1);
        $form->setMax("complement", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_essai_realise_analyses',_('lien_essai_realise_analyses'));
        $form->setLib('essai_realise',_('essai_realise'));
        $form->setLib('analyses',_('analyses'));
        $form->setLib('concluant',_('concluant'));
        $form->setLib('complement',_('complement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // analyses
        $this->init_select($form, $this->f->db, $maj, null, "analyses", $sql_analyses, $sql_analyses_by_id, false);
        // essai_realise
        $this->init_select($form, $this->f->db, $maj, null, "essai_realise", $sql_essai_realise, $sql_essai_realise_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('analyses', $this->retourformulaire))
                $form->setVal('analyses', $idxformulaire);
            if($this->is_in_context_of_foreign_key('essai_realise', $this->retourformulaire))
                $form->setVal('essai_realise', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
