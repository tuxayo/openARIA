<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class technicien_arrondissement_gen extends om_dbform {

    var $table = "technicien_arrondissement";
    var $clePrimaire = "technicien_arrondissement";
    var $typeCle = "N";
    var $required_field = array(
        "arrondissement",
        "service",
        "technicien",
        "technicien_arrondissement"
    );
    var $unique_key = array(
      array("arrondissement","service"),
    );
    var $foreign_keys_extended = array(
        "arrondissement" => array("arrondissement", ),
        "service" => array("service", ),
        "acteur" => array("acteur", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['technicien_arrondissement'])) {
            $this->valF['technicien_arrondissement'] = ""; // -> requis
        } else {
            $this->valF['technicien_arrondissement'] = $val['technicien_arrondissement'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if (!is_numeric($val['technicien'])) {
            $this->valF['technicien'] = ""; // -> requis
        } else {
            $this->valF['technicien'] = $val['technicien'];
        }
        if (!is_numeric($val['arrondissement'])) {
            $this->valF['arrondissement'] = ""; // -> requis
        } else {
            $this->valF['arrondissement'] = $val['arrondissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("technicien_arrondissement", "hidden");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("technicien", "selecthiddenstatic");
            } else {
                $form->setType("technicien", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("technicien_arrondissement", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("technicien", "selecthiddenstatic");
            } else {
                $form->setType("technicien", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("technicien_arrondissement", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("technicien", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("technicien_arrondissement", "static");
            $form->setType("service", "selectstatic");
            $form->setType("technicien", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('technicien_arrondissement','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('technicien','VerifNum(this)');
        $form->setOnchange('arrondissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("technicien_arrondissement", 11);
        $form->setTaille("service", 11);
        $form->setTaille("technicien", 11);
        $form->setTaille("arrondissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("technicien_arrondissement", 11);
        $form->setMax("service", 11);
        $form->setMax("technicien", 11);
        $form->setMax("arrondissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('technicien_arrondissement',_('technicien_arrondissement'));
        $form->setLib('service',_('service'));
        $form->setLib('technicien',_('technicien'));
        $form->setLib('arrondissement',_('arrondissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // arrondissement
        $this->init_select($form, $this->f->db, $maj, null, "arrondissement", $sql_arrondissement, $sql_arrondissement_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
        // technicien
        $this->init_select($form, $this->f->db, $maj, null, "technicien", $sql_technicien, $sql_technicien_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrondissement', $this->retourformulaire))
                $form->setVal('arrondissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('technicien', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
