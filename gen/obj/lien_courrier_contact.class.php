<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_courrier_contact_gen extends om_dbform {

    var $table = "lien_courrier_contact";
    var $clePrimaire = "lien_courrier_contact";
    var $typeCle = "N";
    var $required_field = array(
        "lien_courrier_contact"
    );
    
    var $foreign_keys_extended = array(
        "contact" => array("contact", "contact_institutionnel", "contact_contexte_dossier_coordination", ),
        "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_courrier_contact'])) {
            $this->valF['lien_courrier_contact'] = ""; // -> requis
        } else {
            $this->valF['lien_courrier_contact'] = $val['lien_courrier_contact'];
        }
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = NULL;
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
        if (!is_numeric($val['contact'])) {
            $this->valF['contact'] = NULL;
        } else {
            $this->valF['contact'] = $val['contact'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_courrier_contact", "hidden");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier", "selecthiddenstatic");
            } else {
                $form->setType("courrier", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact", $this->retourformulaire)) {
                $form->setType("contact", "selecthiddenstatic");
            } else {
                $form->setType("contact", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_courrier_contact", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier", "selecthiddenstatic");
            } else {
                $form->setType("courrier", "select");
            }
            if ($this->is_in_context_of_foreign_key("contact", $this->retourformulaire)) {
                $form->setType("contact", "selecthiddenstatic");
            } else {
                $form->setType("contact", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_courrier_contact", "hiddenstatic");
            $form->setType("courrier", "selectstatic");
            $form->setType("contact", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_courrier_contact", "static");
            $form->setType("courrier", "selectstatic");
            $form->setType("contact", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_courrier_contact','VerifNum(this)');
        $form->setOnchange('courrier','VerifNum(this)');
        $form->setOnchange('contact','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_courrier_contact", 11);
        $form->setTaille("courrier", 11);
        $form->setTaille("contact", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_courrier_contact", 11);
        $form->setMax("courrier", 11);
        $form->setMax("contact", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_courrier_contact',_('lien_courrier_contact'));
        $form->setLib('courrier',_('courrier'));
        $form->setLib('contact',_('contact'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // contact
        $this->init_select($form, $this->f->db, $maj, null, "contact", $sql_contact, $sql_contact_by_id, true);
        // courrier
        $this->init_select($form, $this->f->db, $maj, null, "courrier", $sql_courrier, $sql_courrier_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contact', $this->retourformulaire))
                $form->setVal('contact', $idxformulaire);
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
