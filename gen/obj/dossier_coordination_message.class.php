<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class dossier_coordination_message_gen extends om_dbform {

    var $table = "dossier_coordination_message";
    var $clePrimaire = "dossier_coordination_message";
    var $typeCle = "N";
    var $required_field = array(
        "categorie",
        "date_emission",
        "dossier_coordination_message"
    );
    
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination_message'])) {
            $this->valF['dossier_coordination_message'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_message'] = $val['dossier_coordination_message'];
        }
        $this->valF['categorie'] = $val['categorie'];
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if ($val['type'] == "") {
            $this->valF['type'] = NULL;
        } else {
            $this->valF['type'] = $val['type'];
        }
        if ($val['emetteur'] == "") {
            $this->valF['emetteur'] = NULL;
        } else {
            $this->valF['emetteur'] = $val['emetteur'];
        }
            $this->valF['date_emission'] = $val['date_emission'];
            $this->valF['contenu'] = $val['contenu'];
            $this->valF['contenu_json'] = $val['contenu_json'];
        if ($val['si_cadre_lu'] == 1 || $val['si_cadre_lu'] == "t" || $val['si_cadre_lu'] == "Oui") {
            $this->valF['si_cadre_lu'] = true;
        } else {
            $this->valF['si_cadre_lu'] = false;
        }
        if ($val['si_technicien_lu'] == 1 || $val['si_technicien_lu'] == "t" || $val['si_technicien_lu'] == "Oui") {
            $this->valF['si_technicien_lu'] = true;
        } else {
            $this->valF['si_technicien_lu'] = false;
        }
        if ($val['si_mode_lecture'] == "") {
            $this->valF['si_mode_lecture'] = NULL;
        } else {
            $this->valF['si_mode_lecture'] = $val['si_mode_lecture'];
        }
        if ($val['acc_cadre_lu'] == 1 || $val['acc_cadre_lu'] == "t" || $val['acc_cadre_lu'] == "Oui") {
            $this->valF['acc_cadre_lu'] = true;
        } else {
            $this->valF['acc_cadre_lu'] = false;
        }
        if ($val['acc_technicien_lu'] == 1 || $val['acc_technicien_lu'] == "t" || $val['acc_technicien_lu'] == "Oui") {
            $this->valF['acc_technicien_lu'] = true;
        } else {
            $this->valF['acc_technicien_lu'] = false;
        }
        if ($val['acc_mode_lecture'] == "") {
            $this->valF['acc_mode_lecture'] = NULL;
        } else {
            $this->valF['acc_mode_lecture'] = $val['acc_mode_lecture'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination_message", "hidden");
            $form->setType("categorie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("type", "text");
            $form->setType("emetteur", "text");
            $form->setType("date_emission", "text");
            $form->setType("contenu", "textarea");
            $form->setType("contenu_json", "textarea");
            $form->setType("si_cadre_lu", "checkbox");
            $form->setType("si_technicien_lu", "checkbox");
            $form->setType("si_mode_lecture", "text");
            $form->setType("acc_cadre_lu", "checkbox");
            $form->setType("acc_technicien_lu", "checkbox");
            $form->setType("acc_mode_lecture", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination_message", "hiddenstatic");
            $form->setType("categorie", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("type", "text");
            $form->setType("emetteur", "text");
            $form->setType("date_emission", "text");
            $form->setType("contenu", "textarea");
            $form->setType("contenu_json", "textarea");
            $form->setType("si_cadre_lu", "checkbox");
            $form->setType("si_technicien_lu", "checkbox");
            $form->setType("si_mode_lecture", "text");
            $form->setType("acc_cadre_lu", "checkbox");
            $form->setType("acc_technicien_lu", "checkbox");
            $form->setType("acc_mode_lecture", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination_message", "hiddenstatic");
            $form->setType("categorie", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("type", "hiddenstatic");
            $form->setType("emetteur", "hiddenstatic");
            $form->setType("date_emission", "hiddenstatic");
            $form->setType("contenu", "hiddenstatic");
            $form->setType("contenu_json", "hiddenstatic");
            $form->setType("si_cadre_lu", "hiddenstatic");
            $form->setType("si_technicien_lu", "hiddenstatic");
            $form->setType("si_mode_lecture", "hiddenstatic");
            $form->setType("acc_cadre_lu", "hiddenstatic");
            $form->setType("acc_technicien_lu", "hiddenstatic");
            $form->setType("acc_mode_lecture", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination_message", "static");
            $form->setType("categorie", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("type", "static");
            $form->setType("emetteur", "static");
            $form->setType("date_emission", "static");
            $form->setType("contenu", "textareastatic");
            $form->setType("contenu_json", "textareastatic");
            $form->setType("si_cadre_lu", "checkboxstatic");
            $form->setType("si_technicien_lu", "checkboxstatic");
            $form->setType("si_mode_lecture", "static");
            $form->setType("acc_cadre_lu", "checkboxstatic");
            $form->setType("acc_technicien_lu", "checkboxstatic");
            $form->setType("acc_mode_lecture", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination_message','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination_message", 11);
        $form->setTaille("categorie", 20);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("type", 30);
        $form->setTaille("emetteur", 30);
        $form->setTaille("date_emission", 8);
        $form->setTaille("contenu", 80);
        $form->setTaille("contenu_json", 80);
        $form->setTaille("si_cadre_lu", 1);
        $form->setTaille("si_technicien_lu", 1);
        $form->setTaille("si_mode_lecture", 10);
        $form->setTaille("acc_cadre_lu", 1);
        $form->setTaille("acc_technicien_lu", 1);
        $form->setTaille("acc_mode_lecture", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination_message", 11);
        $form->setMax("categorie", 20);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("type", 60);
        $form->setMax("emetteur", 40);
        $form->setMax("date_emission", 8);
        $form->setMax("contenu", 6);
        $form->setMax("contenu_json", 6);
        $form->setMax("si_cadre_lu", 1);
        $form->setMax("si_technicien_lu", 1);
        $form->setMax("si_mode_lecture", 10);
        $form->setMax("acc_cadre_lu", 1);
        $form->setMax("acc_technicien_lu", 1);
        $form->setMax("acc_mode_lecture", 10);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination_message',_('dossier_coordination_message'));
        $form->setLib('categorie',_('categorie'));
        $form->setLib('dossier_coordination',_('dossier_coordination'));
        $form->setLib('type',_('type'));
        $form->setLib('emetteur',_('emetteur'));
        $form->setLib('date_emission',_('date_emission'));
        $form->setLib('contenu',_('contenu'));
        $form->setLib('contenu_json',_('contenu_json'));
        $form->setLib('si_cadre_lu',_('si_cadre_lu'));
        $form->setLib('si_technicien_lu',_('si_technicien_lu'));
        $form->setLib('si_mode_lecture',_('si_mode_lecture'));
        $form->setLib('acc_cadre_lu',_('acc_cadre_lu'));
        $form->setLib('acc_technicien_lu',_('acc_technicien_lu'));
        $form->setLib('acc_mode_lecture',_('acc_mode_lecture'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_coordination
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination", $sql_dossier_coordination, $sql_dossier_coordination_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
