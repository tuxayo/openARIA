<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_autorite_police_courrier_gen extends om_dbform {

    var $table = "lien_autorite_police_courrier";
    var $clePrimaire = "lien_autorite_police_courrier";
    var $typeCle = "N";
    var $required_field = array(
        "autorite_police",
        "courrier",
        "lien_autorite_police_courrier"
    );
    
    var $foreign_keys_extended = array(
        "autorite_police" => array("autorite_police", ),
        "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_autorite_police_courrier'])) {
            $this->valF['lien_autorite_police_courrier'] = ""; // -> requis
        } else {
            $this->valF['lien_autorite_police_courrier'] = $val['lien_autorite_police_courrier'];
        }
        if (!is_numeric($val['autorite_police'])) {
            $this->valF['autorite_police'] = ""; // -> requis
        } else {
            $this->valF['autorite_police'] = $val['autorite_police'];
        }
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = ""; // -> requis
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_autorite_police_courrier", "hidden");
            if ($this->is_in_context_of_foreign_key("autorite_police", $this->retourformulaire)) {
                $form->setType("autorite_police", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier", "selecthiddenstatic");
            } else {
                $form->setType("courrier", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_autorite_police_courrier", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("autorite_police", $this->retourformulaire)) {
                $form->setType("autorite_police", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier", "selecthiddenstatic");
            } else {
                $form->setType("courrier", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_autorite_police_courrier", "hiddenstatic");
            $form->setType("autorite_police", "selectstatic");
            $form->setType("courrier", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_autorite_police_courrier", "static");
            $form->setType("autorite_police", "selectstatic");
            $form->setType("courrier", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_autorite_police_courrier','VerifNum(this)');
        $form->setOnchange('autorite_police','VerifNum(this)');
        $form->setOnchange('courrier','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_autorite_police_courrier", 11);
        $form->setTaille("autorite_police", 11);
        $form->setTaille("courrier", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_autorite_police_courrier", 11);
        $form->setMax("autorite_police", 11);
        $form->setMax("courrier", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_autorite_police_courrier',_('lien_autorite_police_courrier'));
        $form->setLib('autorite_police',_('autorite_police'));
        $form->setLib('courrier',_('courrier'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // autorite_police
        $this->init_select($form, $this->f->db, $maj, null, "autorite_police", $sql_autorite_police, $sql_autorite_police_by_id, false);
        // courrier
        $this->init_select($form, $this->f->db, $maj, null, "courrier", $sql_courrier, $sql_courrier_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('autorite_police', $this->retourformulaire))
                $form->setVal('autorite_police', $idxformulaire);
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
