<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class dossier_coordination_type_gen extends om_dbform {

    var $table = "dossier_coordination_type";
    var $clePrimaire = "dossier_coordination_type";
    var $typeCle = "N";
    var $required_field = array(
        "analyses_type_acc",
        "analyses_type_si",
        "dossier_coordination_type",
        "dossier_type"
    );
    
    var $foreign_keys_extended = array(
        "analyses_type" => array("analyses_type", ),
        "dossier_type" => array("dossier_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination_type'])) {
            $this->valF['dossier_coordination_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_type'] = $val['dossier_coordination_type'];
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description'] = $val['description'];
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if (!is_numeric($val['dossier_type'])) {
            $this->valF['dossier_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_type'] = $val['dossier_type'];
        }
        if ($val['dossier_instruction_secu'] == 1 || $val['dossier_instruction_secu'] == "t" || $val['dossier_instruction_secu'] == "Oui") {
            $this->valF['dossier_instruction_secu'] = true;
        } else {
            $this->valF['dossier_instruction_secu'] = false;
        }
        if ($val['dossier_instruction_acc'] == 1 || $val['dossier_instruction_acc'] == "t" || $val['dossier_instruction_acc'] == "Oui") {
            $this->valF['dossier_instruction_acc'] = true;
        } else {
            $this->valF['dossier_instruction_acc'] = false;
        }
        if ($val['a_qualifier'] == 1 || $val['a_qualifier'] == "t" || $val['a_qualifier'] == "Oui") {
            $this->valF['a_qualifier'] = true;
        } else {
            $this->valF['a_qualifier'] = false;
        }
        if (!is_numeric($val['analyses_type_si'])) {
            $this->valF['analyses_type_si'] = ""; // -> requis
        } else {
            $this->valF['analyses_type_si'] = $val['analyses_type_si'];
        }
        if (!is_numeric($val['analyses_type_acc'])) {
            $this->valF['analyses_type_acc'] = ""; // -> requis
        } else {
            $this->valF['analyses_type_acc'] = $val['analyses_type_acc'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination_type", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("dossier_type", $this->retourformulaire)) {
                $form->setType("dossier_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_type", "select");
            }
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type_si", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type_si", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type_acc", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type_acc", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination_type", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            if ($this->is_in_context_of_foreign_key("dossier_type", $this->retourformulaire)) {
                $form->setType("dossier_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_type", "select");
            }
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type_si", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type_si", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type_acc", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type_acc", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination_type", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("dossier_type", "selectstatic");
            $form->setType("dossier_instruction_secu", "hiddenstatic");
            $form->setType("dossier_instruction_acc", "hiddenstatic");
            $form->setType("a_qualifier", "hiddenstatic");
            $form->setType("analyses_type_si", "selectstatic");
            $form->setType("analyses_type_acc", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination_type", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("dossier_type", "selectstatic");
            $form->setType("dossier_instruction_secu", "checkboxstatic");
            $form->setType("dossier_instruction_acc", "checkboxstatic");
            $form->setType("a_qualifier", "checkboxstatic");
            $form->setType("analyses_type_si", "selectstatic");
            $form->setType("analyses_type_acc", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination_type','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('dossier_type','VerifNum(this)');
        $form->setOnchange('analyses_type_si','VerifNum(this)');
        $form->setOnchange('analyses_type_acc','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination_type", 11);
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("dossier_type", 11);
        $form->setTaille("dossier_instruction_secu", 1);
        $form->setTaille("dossier_instruction_acc", 1);
        $form->setTaille("a_qualifier", 1);
        $form->setTaille("analyses_type_si", 11);
        $form->setTaille("analyses_type_acc", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination_type", 11);
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("dossier_type", 11);
        $form->setMax("dossier_instruction_secu", 1);
        $form->setMax("dossier_instruction_acc", 1);
        $form->setMax("a_qualifier", 1);
        $form->setMax("analyses_type_si", 11);
        $form->setMax("analyses_type_acc", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination_type',_('dossier_coordination_type'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description',_('description'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
        $form->setLib('dossier_type',_('dossier_type'));
        $form->setLib('dossier_instruction_secu',_('dossier_instruction_secu'));
        $form->setLib('dossier_instruction_acc',_('dossier_instruction_acc'));
        $form->setLib('a_qualifier',_('a_qualifier'));
        $form->setLib('analyses_type_si',_('analyses_type_si'));
        $form->setLib('analyses_type_acc',_('analyses_type_acc'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // analyses_type_acc
        $this->init_select($form, $this->f->db, $maj, null, "analyses_type_acc", $sql_analyses_type_acc, $sql_analyses_type_acc_by_id, true);
        // analyses_type_si
        $this->init_select($form, $this->f->db, $maj, null, "analyses_type_si", $sql_analyses_type_si, $sql_analyses_type_si_by_id, true);
        // dossier_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_type", $sql_dossier_type, $sql_dossier_type_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_type', $this->retourformulaire))
                $form->setVal('dossier_type', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('analyses_type', $this->retourformulaire))
                $form->setVal('analyses_type_acc', $idxformulaire);
            if($this->is_in_context_of_foreign_key('analyses_type', $this->retourformulaire))
                $form->setVal('analyses_type_si', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_coordination
        $this->rechercheTable($this->f->db, "dossier_coordination", "dossier_coordination_type", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_type_analyses_type
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_type_analyses_type", "dossier_coordination_type", $id);
    }


}

?>
