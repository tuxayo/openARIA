<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class etablissement_unite_gen extends om_dbform {

    var $table = "etablissement_unite";
    var $clePrimaire = "etablissement_unite";
    var $typeCle = "N";
    var $required_field = array(
        "archive",
        "etablissement",
        "etablissement_unite",
        "libelle"
    );
    
    var $foreign_keys_extended = array(
        "derogation_scda" => array("derogation_scda", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "etablissement_unite" => array("etablissement_unite", "etablissement_unite__contexte_di_analyse__ua_valide_sur_etab", "etablissement_unite__contexte_di_analyse__ua_en_analyse", "etablissement_unite__contexte_etab__ua_valide", "etablissement_unite__contexte_etab__ua_archive", "etablissement_unite__contexte_etab__ua_enprojet", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['etablissement_unite'])) {
            $this->valF['etablissement_unite'] = ""; // -> requis
        } else {
            $this->valF['etablissement_unite'] = $val['etablissement_unite'];
        }
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['acc_notes_om_html'] = $val['acc_notes_om_html'];
            $this->valF['acc_descriptif_ua_om_html'] = $val['acc_descriptif_ua_om_html'];
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if ($val['acc_handicap_physique'] == 1 || $val['acc_handicap_physique'] == "t" || $val['acc_handicap_physique'] == "Oui") {
            $this->valF['acc_handicap_physique'] = true;
        } else {
            $this->valF['acc_handicap_physique'] = false;
        }
        if ($val['acc_handicap_auditif'] == 1 || $val['acc_handicap_auditif'] == "t" || $val['acc_handicap_auditif'] == "Oui") {
            $this->valF['acc_handicap_auditif'] = true;
        } else {
            $this->valF['acc_handicap_auditif'] = false;
        }
        if ($val['acc_handicap_visuel'] == 1 || $val['acc_handicap_visuel'] == "t" || $val['acc_handicap_visuel'] == "Oui") {
            $this->valF['acc_handicap_visuel'] = true;
        } else {
            $this->valF['acc_handicap_visuel'] = false;
        }
        if ($val['acc_handicap_mental'] == 1 || $val['acc_handicap_mental'] == "t" || $val['acc_handicap_mental'] == "Oui") {
            $this->valF['acc_handicap_mental'] = true;
        } else {
            $this->valF['acc_handicap_mental'] = false;
        }
        if (!is_numeric($val['acc_places_stationnement_amenagees'])) {
            $this->valF['acc_places_stationnement_amenagees'] = NULL;
        } else {
            $this->valF['acc_places_stationnement_amenagees'] = $val['acc_places_stationnement_amenagees'];
        }
        if ($val['acc_ascenseur'] == 1 || $val['acc_ascenseur'] == "t" || $val['acc_ascenseur'] == "Oui") {
            $this->valF['acc_ascenseur'] = true;
        } else {
            $this->valF['acc_ascenseur'] = false;
        }
        if ($val['acc_elevateur'] == 1 || $val['acc_elevateur'] == "t" || $val['acc_elevateur'] == "Oui") {
            $this->valF['acc_elevateur'] = true;
        } else {
            $this->valF['acc_elevateur'] = false;
        }
        if ($val['acc_boucle_magnetique'] == 1 || $val['acc_boucle_magnetique'] == "t" || $val['acc_boucle_magnetique'] == "Oui") {
            $this->valF['acc_boucle_magnetique'] = true;
        } else {
            $this->valF['acc_boucle_magnetique'] = false;
        }
        if ($val['acc_sanitaire'] == 1 || $val['acc_sanitaire'] == "t" || $val['acc_sanitaire'] == "Oui") {
            $this->valF['acc_sanitaire'] = true;
        } else {
            $this->valF['acc_sanitaire'] = false;
        }
        if (!is_numeric($val['acc_places_assises_public'])) {
            $this->valF['acc_places_assises_public'] = NULL;
        } else {
            $this->valF['acc_places_assises_public'] = $val['acc_places_assises_public'];
        }
        if (!is_numeric($val['acc_chambres_amenagees'])) {
            $this->valF['acc_chambres_amenagees'] = NULL;
        } else {
            $this->valF['acc_chambres_amenagees'] = $val['acc_chambres_amenagees'];
        }
        if ($val['acc_douche'] == 1 || $val['acc_douche'] == "t" || $val['acc_douche'] == "Oui") {
            $this->valF['acc_douche'] = true;
        } else {
            $this->valF['acc_douche'] = false;
        }
        if (!is_numeric($val['acc_derogation_scda'])) {
            $this->valF['acc_derogation_scda'] = NULL;
        } else {
            $this->valF['acc_derogation_scda'] = $val['acc_derogation_scda'];
        }
        if ($val['etat'] == "") {
            $this->valF['etat'] = NULL;
        } else {
            $this->valF['etat'] = $val['etat'];
        }
        if ($val['archive'] == 1 || $val['archive'] == "t" || $val['archive'] == "Oui") {
            $this->valF['archive'] = true;
        } else {
            $this->valF['archive'] = false;
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = NULL;
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['etablissement_unite_lie'])) {
            $this->valF['etablissement_unite_lie'] = NULL;
        } else {
            $this->valF['etablissement_unite_lie'] = $val['etablissement_unite_lie'];
        }
        if ($val['adap_date_validation'] != "") {
            $this->valF['adap_date_validation'] = $this->dateDB($val['adap_date_validation']);
        } else {
            $this->valF['adap_date_validation'] = NULL;
        }
        if (!is_numeric($val['adap_duree_validite'])) {
            $this->valF['adap_duree_validite'] = NULL;
        } else {
            $this->valF['adap_duree_validite'] = $val['adap_duree_validite'];
        }
        if (!is_numeric($val['adap_annee_debut_travaux'])) {
            $this->valF['adap_annee_debut_travaux'] = NULL;
        } else {
            $this->valF['adap_annee_debut_travaux'] = $val['adap_annee_debut_travaux'];
        }
        if (!is_numeric($val['adap_annee_fin_travaux'])) {
            $this->valF['adap_annee_fin_travaux'] = NULL;
        } else {
            $this->valF['adap_annee_fin_travaux'] = $val['adap_annee_fin_travaux'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("etablissement_unite", "hidden");
            $form->setType("libelle", "text");
            $form->setType("acc_notes_om_html", "html");
            $form->setType("acc_descriptif_ua_om_html", "html");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("acc_handicap_physique", "checkbox");
            $form->setType("acc_handicap_auditif", "checkbox");
            $form->setType("acc_handicap_visuel", "checkbox");
            $form->setType("acc_handicap_mental", "checkbox");
            $form->setType("acc_places_stationnement_amenagees", "text");
            $form->setType("acc_ascenseur", "checkbox");
            $form->setType("acc_elevateur", "checkbox");
            $form->setType("acc_boucle_magnetique", "checkbox");
            $form->setType("acc_sanitaire", "checkbox");
            $form->setType("acc_places_assises_public", "text");
            $form->setType("acc_chambres_amenagees", "text");
            $form->setType("acc_douche", "checkbox");
            if ($this->is_in_context_of_foreign_key("derogation_scda", $this->retourformulaire)) {
                $form->setType("acc_derogation_scda", "selecthiddenstatic");
            } else {
                $form->setType("acc_derogation_scda", "select");
            }
            $form->setType("etat", "text");
            $form->setType("archive", "checkbox");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_unite", $this->retourformulaire)) {
                $form->setType("etablissement_unite_lie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_unite_lie", "select");
            }
            $form->setType("adap_date_validation", "date");
            $form->setType("adap_duree_validite", "text");
            $form->setType("adap_annee_debut_travaux", "text");
            $form->setType("adap_annee_fin_travaux", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("etablissement_unite", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("acc_notes_om_html", "html");
            $form->setType("acc_descriptif_ua_om_html", "html");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("acc_handicap_physique", "checkbox");
            $form->setType("acc_handicap_auditif", "checkbox");
            $form->setType("acc_handicap_visuel", "checkbox");
            $form->setType("acc_handicap_mental", "checkbox");
            $form->setType("acc_places_stationnement_amenagees", "text");
            $form->setType("acc_ascenseur", "checkbox");
            $form->setType("acc_elevateur", "checkbox");
            $form->setType("acc_boucle_magnetique", "checkbox");
            $form->setType("acc_sanitaire", "checkbox");
            $form->setType("acc_places_assises_public", "text");
            $form->setType("acc_chambres_amenagees", "text");
            $form->setType("acc_douche", "checkbox");
            if ($this->is_in_context_of_foreign_key("derogation_scda", $this->retourformulaire)) {
                $form->setType("acc_derogation_scda", "selecthiddenstatic");
            } else {
                $form->setType("acc_derogation_scda", "select");
            }
            $form->setType("etat", "text");
            $form->setType("archive", "checkbox");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_unite", $this->retourformulaire)) {
                $form->setType("etablissement_unite_lie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_unite_lie", "select");
            }
            $form->setType("adap_date_validation", "date");
            $form->setType("adap_duree_validite", "text");
            $form->setType("adap_annee_debut_travaux", "text");
            $form->setType("adap_annee_fin_travaux", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("etablissement_unite", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("acc_notes_om_html", "hiddenstatic");
            $form->setType("acc_descriptif_ua_om_html", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("acc_handicap_physique", "hiddenstatic");
            $form->setType("acc_handicap_auditif", "hiddenstatic");
            $form->setType("acc_handicap_visuel", "hiddenstatic");
            $form->setType("acc_handicap_mental", "hiddenstatic");
            $form->setType("acc_places_stationnement_amenagees", "hiddenstatic");
            $form->setType("acc_ascenseur", "hiddenstatic");
            $form->setType("acc_elevateur", "hiddenstatic");
            $form->setType("acc_boucle_magnetique", "hiddenstatic");
            $form->setType("acc_sanitaire", "hiddenstatic");
            $form->setType("acc_places_assises_public", "hiddenstatic");
            $form->setType("acc_chambres_amenagees", "hiddenstatic");
            $form->setType("acc_douche", "hiddenstatic");
            $form->setType("acc_derogation_scda", "selectstatic");
            $form->setType("etat", "hiddenstatic");
            $form->setType("archive", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("etablissement_unite_lie", "selectstatic");
            $form->setType("adap_date_validation", "hiddenstatic");
            $form->setType("adap_duree_validite", "hiddenstatic");
            $form->setType("adap_annee_debut_travaux", "hiddenstatic");
            $form->setType("adap_annee_fin_travaux", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("etablissement_unite", "static");
            $form->setType("libelle", "static");
            $form->setType("acc_notes_om_html", "htmlstatic");
            $form->setType("acc_descriptif_ua_om_html", "htmlstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("acc_handicap_physique", "checkboxstatic");
            $form->setType("acc_handicap_auditif", "checkboxstatic");
            $form->setType("acc_handicap_visuel", "checkboxstatic");
            $form->setType("acc_handicap_mental", "checkboxstatic");
            $form->setType("acc_places_stationnement_amenagees", "static");
            $form->setType("acc_ascenseur", "checkboxstatic");
            $form->setType("acc_elevateur", "checkboxstatic");
            $form->setType("acc_boucle_magnetique", "checkboxstatic");
            $form->setType("acc_sanitaire", "checkboxstatic");
            $form->setType("acc_places_assises_public", "static");
            $form->setType("acc_chambres_amenagees", "static");
            $form->setType("acc_douche", "checkboxstatic");
            $form->setType("acc_derogation_scda", "selectstatic");
            $form->setType("etat", "static");
            $form->setType("archive", "checkboxstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("etablissement_unite_lie", "selectstatic");
            $form->setType("adap_date_validation", "datestatic");
            $form->setType("adap_duree_validite", "static");
            $form->setType("adap_annee_debut_travaux", "static");
            $form->setType("adap_annee_fin_travaux", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('etablissement_unite','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('acc_places_stationnement_amenagees','VerifNum(this)');
        $form->setOnchange('acc_places_assises_public','VerifNum(this)');
        $form->setOnchange('acc_chambres_amenagees','VerifNum(this)');
        $form->setOnchange('acc_derogation_scda','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('etablissement_unite_lie','VerifNum(this)');
        $form->setOnchange('adap_date_validation','fdate(this)');
        $form->setOnchange('adap_duree_validite','VerifNum(this)');
        $form->setOnchange('adap_annee_debut_travaux','VerifNum(this)');
        $form->setOnchange('adap_annee_fin_travaux','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement_unite", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("acc_notes_om_html", 80);
        $form->setTaille("acc_descriptif_ua_om_html", 80);
        $form->setTaille("etablissement", 11);
        $form->setTaille("acc_handicap_physique", 1);
        $form->setTaille("acc_handicap_auditif", 1);
        $form->setTaille("acc_handicap_visuel", 1);
        $form->setTaille("acc_handicap_mental", 1);
        $form->setTaille("acc_places_stationnement_amenagees", 11);
        $form->setTaille("acc_ascenseur", 1);
        $form->setTaille("acc_elevateur", 1);
        $form->setTaille("acc_boucle_magnetique", 1);
        $form->setTaille("acc_sanitaire", 1);
        $form->setTaille("acc_places_assises_public", 11);
        $form->setTaille("acc_chambres_amenagees", 11);
        $form->setTaille("acc_douche", 1);
        $form->setTaille("acc_derogation_scda", 11);
        $form->setTaille("etat", 20);
        $form->setTaille("archive", 1);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("etablissement_unite_lie", 11);
        $form->setTaille("adap_date_validation", 12);
        $form->setTaille("adap_duree_validite", 11);
        $form->setTaille("adap_annee_debut_travaux", 11);
        $form->setTaille("adap_annee_fin_travaux", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("etablissement_unite", 11);
        $form->setMax("libelle", 90);
        $form->setMax("acc_notes_om_html", 6);
        $form->setMax("acc_descriptif_ua_om_html", 6);
        $form->setMax("etablissement", 11);
        $form->setMax("acc_handicap_physique", 1);
        $form->setMax("acc_handicap_auditif", 1);
        $form->setMax("acc_handicap_visuel", 1);
        $form->setMax("acc_handicap_mental", 1);
        $form->setMax("acc_places_stationnement_amenagees", 11);
        $form->setMax("acc_ascenseur", 1);
        $form->setMax("acc_elevateur", 1);
        $form->setMax("acc_boucle_magnetique", 1);
        $form->setMax("acc_sanitaire", 1);
        $form->setMax("acc_places_assises_public", 11);
        $form->setMax("acc_chambres_amenagees", 11);
        $form->setMax("acc_douche", 1);
        $form->setMax("acc_derogation_scda", 11);
        $form->setMax("etat", 20);
        $form->setMax("archive", 1);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("etablissement_unite_lie", 11);
        $form->setMax("adap_date_validation", 12);
        $form->setMax("adap_duree_validite", 11);
        $form->setMax("adap_annee_debut_travaux", 11);
        $form->setMax("adap_annee_fin_travaux", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('etablissement_unite',_('etablissement_unite'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('acc_notes_om_html',_('acc_notes_om_html'));
        $form->setLib('acc_descriptif_ua_om_html',_('acc_descriptif_ua_om_html'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('acc_handicap_physique',_('acc_handicap_physique'));
        $form->setLib('acc_handicap_auditif',_('acc_handicap_auditif'));
        $form->setLib('acc_handicap_visuel',_('acc_handicap_visuel'));
        $form->setLib('acc_handicap_mental',_('acc_handicap_mental'));
        $form->setLib('acc_places_stationnement_amenagees',_('acc_places_stationnement_amenagees'));
        $form->setLib('acc_ascenseur',_('acc_ascenseur'));
        $form->setLib('acc_elevateur',_('acc_elevateur'));
        $form->setLib('acc_boucle_magnetique',_('acc_boucle_magnetique'));
        $form->setLib('acc_sanitaire',_('acc_sanitaire'));
        $form->setLib('acc_places_assises_public',_('acc_places_assises_public'));
        $form->setLib('acc_chambres_amenagees',_('acc_chambres_amenagees'));
        $form->setLib('acc_douche',_('acc_douche'));
        $form->setLib('acc_derogation_scda',_('acc_derogation_scda'));
        $form->setLib('etat',_('etat'));
        $form->setLib('archive',_('archive'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('etablissement_unite_lie',_('etablissement_unite_lie'));
        $form->setLib('adap_date_validation',_('adap_date_validation'));
        $form->setLib('adap_duree_validite',_('adap_duree_validite'));
        $form->setLib('adap_annee_debut_travaux',_('adap_annee_debut_travaux'));
        $form->setLib('adap_annee_fin_travaux',_('adap_annee_fin_travaux'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // acc_derogation_scda
        $this->init_select($form, $this->f->db, $maj, null, "acc_derogation_scda", $sql_acc_derogation_scda, $sql_acc_derogation_scda_by_id, true);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
        // etablissement_unite_lie
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_unite_lie", $sql_etablissement_unite_lie, $sql_etablissement_unite_lie_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('derogation_scda', $this->retourformulaire))
                $form->setVal('acc_derogation_scda', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_unite', $this->retourformulaire))
                $form->setVal('etablissement_unite_lie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : etablissement_unite
        $this->rechercheTable($this->f->db, "etablissement_unite", "etablissement_unite_lie", $id);
    }


}

?>
