<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class programmation_gen extends om_dbform {

    var $table = "programmation";
    var $clePrimaire = "programmation";
    var $typeCle = "N";
    var $required_field = array(
        "annee",
        "date_modification",
        "numero_semaine",
        "programmation",
        "programmation_etat",
        "semaine_annee",
        "service",
        "version"
    );
    var $unique_key = array(
      array("annee","numero_semaine","service"),
    );
    var $foreign_keys_extended = array(
        "programmation_etat" => array("programmation_etat", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['programmation'])) {
            $this->valF['programmation'] = ""; // -> requis
        } else {
            $this->valF['programmation'] = $val['programmation'];
        }
        if (!is_numeric($val['annee'])) {
            $this->valF['annee'] = ""; // -> requis
        } else {
            $this->valF['annee'] = $val['annee'];
        }
        if (!is_numeric($val['numero_semaine'])) {
            $this->valF['numero_semaine'] = ""; // -> requis
        } else {
            $this->valF['numero_semaine'] = $val['numero_semaine'];
        }
        if (!is_numeric($val['version'])) {
            $this->valF['version'] = ""; // -> requis
        } else {
            $this->valF['version'] = $val['version'];
        }
        if (!is_numeric($val['programmation_etat'])) {
            $this->valF['programmation_etat'] = ""; // -> requis
        } else {
            $this->valF['programmation_etat'] = $val['programmation_etat'];
        }
        if ($val['date_modification'] != "") {
            $this->valF['date_modification'] = $this->dateDB($val['date_modification']);
        }
        if ($val['convocation_exploitants'] == "") {
            $this->valF['convocation_exploitants'] = NULL;
        } else {
            $this->valF['convocation_exploitants'] = $val['convocation_exploitants'];
        }
        if ($val['date_envoi_ce'] != "") {
            $this->valF['date_envoi_ce'] = $this->dateDB($val['date_envoi_ce']);
        } else {
            $this->valF['date_envoi_ce'] = NULL;
        }
        if ($val['convocation_membres'] == "") {
            $this->valF['convocation_membres'] = NULL;
        } else {
            $this->valF['convocation_membres'] = $val['convocation_membres'];
        }
        if ($val['date_envoi_cm'] != "") {
            $this->valF['date_envoi_cm'] = $this->dateDB($val['date_envoi_cm']);
        } else {
            $this->valF['date_envoi_cm'] = NULL;
        }
        $this->valF['semaine_annee'] = $val['semaine_annee'];
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['planifier_nouveau'] == 1 || $val['planifier_nouveau'] == "t" || $val['planifier_nouveau'] == "Oui") {
            $this->valF['planifier_nouveau'] = true;
        } else {
            $this->valF['planifier_nouveau'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("programmation", "hidden");
            $form->setType("annee", "text");
            $form->setType("numero_semaine", "text");
            $form->setType("version", "text");
            if ($this->is_in_context_of_foreign_key("programmation_etat", $this->retourformulaire)) {
                $form->setType("programmation_etat", "selecthiddenstatic");
            } else {
                $form->setType("programmation_etat", "select");
            }
            $form->setType("date_modification", "date");
            $form->setType("convocation_exploitants", "text");
            $form->setType("date_envoi_ce", "date");
            $form->setType("convocation_membres", "text");
            $form->setType("date_envoi_cm", "date");
            $form->setType("semaine_annee", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("planifier_nouveau", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("programmation", "hiddenstatic");
            $form->setType("annee", "text");
            $form->setType("numero_semaine", "text");
            $form->setType("version", "text");
            if ($this->is_in_context_of_foreign_key("programmation_etat", $this->retourformulaire)) {
                $form->setType("programmation_etat", "selecthiddenstatic");
            } else {
                $form->setType("programmation_etat", "select");
            }
            $form->setType("date_modification", "date");
            $form->setType("convocation_exploitants", "text");
            $form->setType("date_envoi_ce", "date");
            $form->setType("convocation_membres", "text");
            $form->setType("date_envoi_cm", "date");
            $form->setType("semaine_annee", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("planifier_nouveau", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("programmation", "hiddenstatic");
            $form->setType("annee", "hiddenstatic");
            $form->setType("numero_semaine", "hiddenstatic");
            $form->setType("version", "hiddenstatic");
            $form->setType("programmation_etat", "selectstatic");
            $form->setType("date_modification", "hiddenstatic");
            $form->setType("convocation_exploitants", "hiddenstatic");
            $form->setType("date_envoi_ce", "hiddenstatic");
            $form->setType("convocation_membres", "hiddenstatic");
            $form->setType("date_envoi_cm", "hiddenstatic");
            $form->setType("semaine_annee", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("planifier_nouveau", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("programmation", "static");
            $form->setType("annee", "static");
            $form->setType("numero_semaine", "static");
            $form->setType("version", "static");
            $form->setType("programmation_etat", "selectstatic");
            $form->setType("date_modification", "datestatic");
            $form->setType("convocation_exploitants", "static");
            $form->setType("date_envoi_ce", "datestatic");
            $form->setType("convocation_membres", "static");
            $form->setType("date_envoi_cm", "datestatic");
            $form->setType("semaine_annee", "static");
            $form->setType("service", "selectstatic");
            $form->setType("planifier_nouveau", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('programmation','VerifNum(this)');
        $form->setOnchange('annee','VerifNum(this)');
        $form->setOnchange('numero_semaine','VerifNum(this)');
        $form->setOnchange('version','VerifNum(this)');
        $form->setOnchange('programmation_etat','VerifNum(this)');
        $form->setOnchange('date_modification','fdate(this)');
        $form->setOnchange('date_envoi_ce','fdate(this)');
        $form->setOnchange('date_envoi_cm','fdate(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("programmation", 11);
        $form->setTaille("annee", 11);
        $form->setTaille("numero_semaine", 11);
        $form->setTaille("version", 11);
        $form->setTaille("programmation_etat", 11);
        $form->setTaille("date_modification", 12);
        $form->setTaille("convocation_exploitants", 20);
        $form->setTaille("date_envoi_ce", 12);
        $form->setTaille("convocation_membres", 20);
        $form->setTaille("date_envoi_cm", 12);
        $form->setTaille("semaine_annee", 10);
        $form->setTaille("service", 11);
        $form->setTaille("planifier_nouveau", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("programmation", 11);
        $form->setMax("annee", 11);
        $form->setMax("numero_semaine", 11);
        $form->setMax("version", 11);
        $form->setMax("programmation_etat", 11);
        $form->setMax("date_modification", 12);
        $form->setMax("convocation_exploitants", 20);
        $form->setMax("date_envoi_ce", 12);
        $form->setMax("convocation_membres", 20);
        $form->setMax("date_envoi_cm", 12);
        $form->setMax("semaine_annee", 7);
        $form->setMax("service", 11);
        $form->setMax("planifier_nouveau", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('programmation',_('programmation'));
        $form->setLib('annee',_('annee'));
        $form->setLib('numero_semaine',_('numero_semaine'));
        $form->setLib('version',_('version'));
        $form->setLib('programmation_etat',_('programmation_etat'));
        $form->setLib('date_modification',_('date_modification'));
        $form->setLib('convocation_exploitants',_('convocation_exploitants'));
        $form->setLib('date_envoi_ce',_('date_envoi_ce'));
        $form->setLib('convocation_membres',_('convocation_membres'));
        $form->setLib('date_envoi_cm',_('date_envoi_cm'));
        $form->setLib('semaine_annee',_('semaine_annee'));
        $form->setLib('service',_('service'));
        $form->setLib('planifier_nouveau',_('planifier_nouveau'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // programmation_etat
        $this->init_select($form, $this->f->db, $maj, null, "programmation_etat", $sql_programmation_etat, $sql_programmation_etat_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('programmation_etat', $this->retourformulaire))
                $form->setVal('programmation_etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "programmation", $id);
    }


}

?>
