<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class autorite_competente_gen extends om_dbform {

    var $table = "autorite_competente";
    var $clePrimaire = "autorite_competente";
    var $typeCle = "N";
    var $required_field = array(
        "autorite_competente",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['autorite_competente'])) {
            $this->valF['autorite_competente'] = ""; // -> requis
        } else {
            $this->valF['autorite_competente'] = $val['autorite_competente'];
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("autorite_competente", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("autorite_competente", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("autorite_competente", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("service", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("autorite_competente", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("service", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('autorite_competente','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("autorite_competente", 11);
        $form->setTaille("code", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("service", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("autorite_competente", 11);
        $form->setMax("code", 10);
        $form->setMax("libelle", 100);
        $form->setMax("service", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('autorite_competente',_('autorite_competente'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('service',_('service'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_instruction
        $this->rechercheTable($this->f->db, "dossier_instruction", "autorite_competente", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_autorite_competente_plan", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_autorite_competente_visite", $id);
    }


}

?>
