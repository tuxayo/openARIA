<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_contrainte_etablissement_gen extends om_dbform {

    var $table = "lien_contrainte_etablissement";
    var $clePrimaire = "lien_contrainte_etablissement";
    var $typeCle = "N";
    var $required_field = array(
        "contrainte",
        "etablissement",
        "lien_contrainte_etablissement"
    );
    
    var $foreign_keys_extended = array(
        "contrainte" => array("contrainte", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_contrainte_etablissement'])) {
            $this->valF['lien_contrainte_etablissement'] = ""; // -> requis
        } else {
            $this->valF['lien_contrainte_etablissement'] = $val['lien_contrainte_etablissement'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = ""; // -> requis
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['contrainte'])) {
            $this->valF['contrainte'] = ""; // -> requis
        } else {
            $this->valF['contrainte'] = $val['contrainte'];
        }
            $this->valF['texte_complete'] = $val['texte_complete'];
        if ($val['recuperee'] == 1 || $val['recuperee'] == "t" || $val['recuperee'] == "Oui") {
            $this->valF['recuperee'] = true;
        } else {
            $this->valF['recuperee'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_contrainte_etablissement", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrainte", $this->retourformulaire)) {
                $form->setType("contrainte", "selecthiddenstatic");
            } else {
                $form->setType("contrainte", "select");
            }
            $form->setType("texte_complete", "textarea");
            $form->setType("recuperee", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_contrainte_etablissement", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("contrainte", $this->retourformulaire)) {
                $form->setType("contrainte", "selecthiddenstatic");
            } else {
                $form->setType("contrainte", "select");
            }
            $form->setType("texte_complete", "textarea");
            $form->setType("recuperee", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_contrainte_etablissement", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("contrainte", "selectstatic");
            $form->setType("texte_complete", "hiddenstatic");
            $form->setType("recuperee", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_contrainte_etablissement", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("contrainte", "selectstatic");
            $form->setType("texte_complete", "textareastatic");
            $form->setType("recuperee", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_contrainte_etablissement','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('contrainte','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_contrainte_etablissement", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("contrainte", 11);
        $form->setTaille("texte_complete", 80);
        $form->setTaille("recuperee", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_contrainte_etablissement", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("contrainte", 11);
        $form->setMax("texte_complete", 6);
        $form->setMax("recuperee", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_contrainte_etablissement',_('lien_contrainte_etablissement'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('contrainte',_('contrainte'));
        $form->setLib('texte_complete',_('texte_complete'));
        $form->setLib('recuperee',_('recuperee'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // contrainte
        $this->init_select($form, $this->f->db, $maj, null, "contrainte", $sql_contrainte, $sql_contrainte_by_id, true);
        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contrainte', $this->retourformulaire))
                $form->setVal('contrainte', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
