<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class voie_arrondissement_gen extends om_dbform {

    var $table = "voie_arrondissement";
    var $clePrimaire = "voie_arrondissement";
    var $typeCle = "N";
    var $required_field = array(
        "voie_arrondissement"
    );
    var $unique_key = array(
      array("arrondissement","voie"),
    );
    var $foreign_keys_extended = array(
        "arrondissement" => array("arrondissement", ),
        "voie" => array("voie", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['voie_arrondissement'])) {
            $this->valF['voie_arrondissement'] = ""; // -> requis
        } else {
            $this->valF['voie_arrondissement'] = $val['voie_arrondissement'];
        }
        if (!is_numeric($val['voie'])) {
            $this->valF['voie'] = NULL;
        } else {
            $this->valF['voie'] = $val['voie'];
        }
        if (!is_numeric($val['arrondissement'])) {
            $this->valF['arrondissement'] = NULL;
        } else {
            $this->valF['arrondissement'] = $val['arrondissement'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("voie_arrondissement", "hidden");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("voie_arrondissement", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("voie", $this->retourformulaire)) {
                $form->setType("voie", "selecthiddenstatic");
            } else {
                $form->setType("voie", "select");
            }
            if ($this->is_in_context_of_foreign_key("arrondissement", $this->retourformulaire)) {
                $form->setType("arrondissement", "selecthiddenstatic");
            } else {
                $form->setType("arrondissement", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("voie_arrondissement", "hiddenstatic");
            $form->setType("voie", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("voie_arrondissement", "static");
            $form->setType("voie", "selectstatic");
            $form->setType("arrondissement", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('voie_arrondissement','VerifNum(this)');
        $form->setOnchange('voie','VerifNum(this)');
        $form->setOnchange('arrondissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("voie_arrondissement", 11);
        $form->setTaille("voie", 11);
        $form->setTaille("arrondissement", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("voie_arrondissement", 11);
        $form->setMax("voie", 11);
        $form->setMax("arrondissement", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('voie_arrondissement',_('voie_arrondissement'));
        $form->setLib('voie',_('voie'));
        $form->setLib('arrondissement',_('arrondissement'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // arrondissement
        $this->init_select($form, $this->f->db, $maj, null, "arrondissement", $sql_arrondissement, $sql_arrondissement_by_id, true);
        // voie
        $this->init_select($form, $this->f->db, $maj, null, "voie", $sql_voie, $sql_voie_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('arrondissement', $this->retourformulaire))
                $form->setVal('arrondissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('voie', $this->retourformulaire))
                $form->setVal('voie', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
