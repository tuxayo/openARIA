<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class signataire_gen extends om_dbform {

    var $table = "signataire";
    var $clePrimaire = "signataire";
    var $typeCle = "N";
    var $required_field = array(
        "signataire"
    );
    
    var $foreign_keys_extended = array(
        "contact_civilite" => array("contact_civilite", ),
        "signataire_qualite" => array("signataire_qualite", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['signataire'])) {
            $this->valF['signataire'] = ""; // -> requis
        } else {
            $this->valF['signataire'] = $val['signataire'];
        }
        if ($val['nom'] == "") {
            $this->valF['nom'] = NULL;
        } else {
            $this->valF['nom'] = $val['nom'];
        }
        if ($val['prenom'] == "") {
            $this->valF['prenom'] = NULL;
        } else {
            $this->valF['prenom'] = $val['prenom'];
        }
        if (!is_numeric($val['civilite'])) {
            $this->valF['civilite'] = NULL;
        } else {
            $this->valF['civilite'] = $val['civilite'];
        }
        if (!is_numeric($val['signataire_qualite'])) {
            $this->valF['signataire_qualite'] = NULL;
        } else {
            $this->valF['signataire_qualite'] = $val['signataire_qualite'];
        }
            $this->valF['signature'] = $val['signature'];
        if ($val['defaut'] == 1 || $val['defaut'] == "t" || $val['defaut'] == "Oui") {
            $this->valF['defaut'] = true;
        } else {
            $this->valF['defaut'] = false;
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("signataire", "hidden");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            if ($this->is_in_context_of_foreign_key("contact_civilite", $this->retourformulaire)) {
                $form->setType("civilite", "selecthiddenstatic");
            } else {
                $form->setType("civilite", "select");
            }
            if ($this->is_in_context_of_foreign_key("signataire_qualite", $this->retourformulaire)) {
                $form->setType("signataire_qualite", "selecthiddenstatic");
            } else {
                $form->setType("signataire_qualite", "select");
            }
            $form->setType("signature", "textarea");
            $form->setType("defaut", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("signataire", "hiddenstatic");
            $form->setType("nom", "text");
            $form->setType("prenom", "text");
            if ($this->is_in_context_of_foreign_key("contact_civilite", $this->retourformulaire)) {
                $form->setType("civilite", "selecthiddenstatic");
            } else {
                $form->setType("civilite", "select");
            }
            if ($this->is_in_context_of_foreign_key("signataire_qualite", $this->retourformulaire)) {
                $form->setType("signataire_qualite", "selecthiddenstatic");
            } else {
                $form->setType("signataire_qualite", "select");
            }
            $form->setType("signature", "textarea");
            $form->setType("defaut", "checkbox");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("signataire", "hiddenstatic");
            $form->setType("nom", "hiddenstatic");
            $form->setType("prenom", "hiddenstatic");
            $form->setType("civilite", "selectstatic");
            $form->setType("signataire_qualite", "selectstatic");
            $form->setType("signature", "hiddenstatic");
            $form->setType("defaut", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("signataire", "static");
            $form->setType("nom", "static");
            $form->setType("prenom", "static");
            $form->setType("civilite", "selectstatic");
            $form->setType("signataire_qualite", "selectstatic");
            $form->setType("signature", "textareastatic");
            $form->setType("defaut", "checkboxstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('signataire','VerifNum(this)');
        $form->setOnchange('civilite','VerifNum(this)');
        $form->setOnchange('signataire_qualite','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("signataire", 11);
        $form->setTaille("nom", 30);
        $form->setTaille("prenom", 30);
        $form->setTaille("civilite", 11);
        $form->setTaille("signataire_qualite", 11);
        $form->setTaille("signature", 80);
        $form->setTaille("defaut", 1);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("signataire", 11);
        $form->setMax("nom", 100);
        $form->setMax("prenom", 100);
        $form->setMax("civilite", 11);
        $form->setMax("signataire_qualite", 11);
        $form->setMax("signature", 6);
        $form->setMax("defaut", 1);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('signataire',_('signataire'));
        $form->setLib('nom',_('nom'));
        $form->setLib('prenom',_('prenom'));
        $form->setLib('civilite',_('civilite'));
        $form->setLib('signataire_qualite',_('signataire_qualite'));
        $form->setLib('signature',_('signature'));
        $form->setLib('defaut',_('defaut'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // civilite
        $this->init_select($form, $this->f->db, $maj, null, "civilite", $sql_civilite, $sql_civilite_by_id, false);
        // signataire_qualite
        $this->init_select($form, $this->f->db, $maj, null, "signataire_qualite", $sql_signataire_qualite, $sql_signataire_qualite_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('contact_civilite', $this->retourformulaire))
                $form->setVal('civilite', $idxformulaire);
            if($this->is_in_context_of_foreign_key('signataire_qualite', $this->retourformulaire))
                $form->setVal('signataire_qualite', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "signataire", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "signataire", $id);
    }


}

?>
