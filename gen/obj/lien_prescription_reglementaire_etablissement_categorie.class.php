<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_prescription_reglementaire_etablissement_categorie_gen extends om_dbform {

    var $table = "lien_prescription_reglementaire_etablissement_categorie";
    var $clePrimaire = "lien_prescription_reglementaire_etablissement_categorie";
    var $typeCle = "N";
    var $required_field = array(
        "etablissement_categorie",
        "lien_prescription_reglementaire_etablissement_categorie",
        "prescription_reglementaire"
    );
    
    var $foreign_keys_extended = array(
        "etablissement_categorie" => array("etablissement_categorie", ),
        "prescription_reglementaire" => array("prescription_reglementaire", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_prescription_reglementaire_etablissement_categorie'])) {
            $this->valF['lien_prescription_reglementaire_etablissement_categorie'] = ""; // -> requis
        } else {
            $this->valF['lien_prescription_reglementaire_etablissement_categorie'] = $val['lien_prescription_reglementaire_etablissement_categorie'];
        }
        if (!is_numeric($val['prescription_reglementaire'])) {
            $this->valF['prescription_reglementaire'] = ""; // -> requis
        } else {
            $this->valF['prescription_reglementaire'] = $val['prescription_reglementaire'];
        }
        if (!is_numeric($val['etablissement_categorie'])) {
            $this->valF['etablissement_categorie'] = ""; // -> requis
        } else {
            $this->valF['etablissement_categorie'] = $val['etablissement_categorie'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_prescription_reglementaire_etablissement_categorie", "hidden");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_prescription_reglementaire_etablissement_categorie", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_prescription_reglementaire_etablissement_categorie", "hiddenstatic");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_prescription_reglementaire_etablissement_categorie", "static");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_prescription_reglementaire_etablissement_categorie','VerifNum(this)');
        $form->setOnchange('prescription_reglementaire','VerifNum(this)');
        $form->setOnchange('etablissement_categorie','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_prescription_reglementaire_etablissement_categorie", 11);
        $form->setTaille("prescription_reglementaire", 11);
        $form->setTaille("etablissement_categorie", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_prescription_reglementaire_etablissement_categorie", 11);
        $form->setMax("prescription_reglementaire", 11);
        $form->setMax("etablissement_categorie", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_prescription_reglementaire_etablissement_categorie',_('lien_prescription_reglementaire_etablissement_categorie'));
        $form->setLib('prescription_reglementaire',_('prescription_reglementaire'));
        $form->setLib('etablissement_categorie',_('etablissement_categorie'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // etablissement_categorie
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_categorie", $sql_etablissement_categorie, $sql_etablissement_categorie_by_id, true);
        // prescription_reglementaire
        $this->init_select($form, $this->f->db, $maj, null, "prescription_reglementaire", $sql_prescription_reglementaire, $sql_prescription_reglementaire_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement_categorie', $this->retourformulaire))
                $form->setVal('etablissement_categorie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('prescription_reglementaire', $this->retourformulaire))
                $form->setVal('prescription_reglementaire', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
