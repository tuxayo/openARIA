<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class courrier_gen extends om_dbform {

    var $table = "courrier";
    var $clePrimaire = "courrier";
    var $typeCle = "N";
    var $required_field = array(
        "code_barres",
        "courrier",
        "courrier_type",
        "modele_edition"
    );
    
    var $foreign_keys_extended = array(
        "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
        "courrier_type" => array("courrier_type", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "modele_edition" => array("modele_edition", ),
        "proces_verbal" => array("proces_verbal", ),
        "signataire" => array("signataire", ),
        "visite" => array("visite", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['courrier'])) {
            $this->valF['courrier'] = ""; // -> requis
        } else {
            $this->valF['courrier'] = $val['courrier'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = NULL;
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
            $this->valF['complement1_om_html'] = $val['complement1_om_html'];
            $this->valF['complement2_om_html'] = $val['complement2_om_html'];
        if ($val['finalise'] == 1 || $val['finalise'] == "t" || $val['finalise'] == "Oui") {
            $this->valF['finalise'] = true;
        } else {
            $this->valF['finalise'] = false;
        }
        if ($val['om_fichier_finalise_courrier'] == "") {
            $this->valF['om_fichier_finalise_courrier'] = NULL;
        } else {
            $this->valF['om_fichier_finalise_courrier'] = $val['om_fichier_finalise_courrier'];
        }
        if ($val['om_fichier_signe_courrier'] == "") {
            $this->valF['om_fichier_signe_courrier'] = NULL;
        } else {
            $this->valF['om_fichier_signe_courrier'] = $val['om_fichier_signe_courrier'];
        }
        if ($val['om_date_creation'] != "") {
            $this->valF['om_date_creation'] = $this->dateDB($val['om_date_creation']);
        } else {
            $this->valF['om_date_creation'] = NULL;
        }
        if ($val['date_finalisation'] != "") {
            $this->valF['date_finalisation'] = $this->dateDB($val['date_finalisation']);
        } else {
            $this->valF['date_finalisation'] = NULL;
        }
        if ($val['date_envoi_signature'] != "") {
            $this->valF['date_envoi_signature'] = $this->dateDB($val['date_envoi_signature']);
        } else {
            $this->valF['date_envoi_signature'] = NULL;
        }
        if ($val['date_retour_signature'] != "") {
            $this->valF['date_retour_signature'] = $this->dateDB($val['date_retour_signature']);
        } else {
            $this->valF['date_retour_signature'] = NULL;
        }
        if ($val['date_envoi_controle_legalite'] != "") {
            $this->valF['date_envoi_controle_legalite'] = $this->dateDB($val['date_envoi_controle_legalite']);
        } else {
            $this->valF['date_envoi_controle_legalite'] = NULL;
        }
        if ($val['date_retour_controle_legalite'] != "") {
            $this->valF['date_retour_controle_legalite'] = $this->dateDB($val['date_retour_controle_legalite']);
        } else {
            $this->valF['date_retour_controle_legalite'] = NULL;
        }
        if ($val['date_envoi_rar'] != "") {
            $this->valF['date_envoi_rar'] = $this->dateDB($val['date_envoi_rar']);
        } else {
            $this->valF['date_envoi_rar'] = NULL;
        }
        if ($val['date_retour_rar'] != "") {
            $this->valF['date_retour_rar'] = $this->dateDB($val['date_retour_rar']);
        } else {
            $this->valF['date_retour_rar'] = NULL;
        }
        $this->valF['code_barres'] = $val['code_barres'];
        if ($val['date_envoi_mail_om_fichier_finalise_courrier'] != "") {
            $this->valF['date_envoi_mail_om_fichier_finalise_courrier'] = $this->dateDB($val['date_envoi_mail_om_fichier_finalise_courrier']);
        } else {
            $this->valF['date_envoi_mail_om_fichier_finalise_courrier'] = NULL;
        }
        if ($val['date_envoi_mail_om_fichier_signe_courrier'] != "") {
            $this->valF['date_envoi_mail_om_fichier_signe_courrier'] = $this->dateDB($val['date_envoi_mail_om_fichier_signe_courrier']);
        } else {
            $this->valF['date_envoi_mail_om_fichier_signe_courrier'] = NULL;
        }
        if ($val['mailing'] == 1 || $val['mailing'] == "t" || $val['mailing'] == "Oui") {
            $this->valF['mailing'] = true;
        } else {
            $this->valF['mailing'] = false;
        }
        if (!is_numeric($val['courrier_parent'])) {
            $this->valF['courrier_parent'] = NULL;
        } else {
            $this->valF['courrier_parent'] = $val['courrier_parent'];
        }
        if (!is_numeric($val['courrier_joint'])) {
            $this->valF['courrier_joint'] = NULL;
        } else {
            $this->valF['courrier_joint'] = $val['courrier_joint'];
        }
        if (!is_numeric($val['modele_edition'])) {
            $this->valF['modele_edition'] = ""; // -> requis
        } else {
            $this->valF['modele_edition'] = $val['modele_edition'];
        }
        if (!is_numeric($val['signataire'])) {
            $this->valF['signataire'] = NULL;
        } else {
            $this->valF['signataire'] = $val['signataire'];
        }
        if (!is_numeric($val['courrier_type'])) {
            $this->valF['courrier_type'] = ""; // -> requis
        } else {
            $this->valF['courrier_type'] = $val['courrier_type'];
        }
        if (!is_numeric($val['proces_verbal'])) {
            $this->valF['proces_verbal'] = NULL;
        } else {
            $this->valF['proces_verbal'] = $val['proces_verbal'];
        }
        if (!is_numeric($val['visite'])) {
            $this->valF['visite'] = NULL;
        } else {
            $this->valF['visite'] = $val['visite'];
        }
        if ($val['arrete_numero'] == "") {
            $this->valF['arrete_numero'] = NULL;
        } else {
            $this->valF['arrete_numero'] = $val['arrete_numero'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("courrier", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            $form->setType("complement1_om_html", "html");
            $form->setType("complement2_om_html", "html");
            $form->setType("finalise", "checkbox");
            $form->setType("om_fichier_finalise_courrier", "text");
            $form->setType("om_fichier_signe_courrier", "text");
            $form->setType("om_date_creation", "date");
            $form->setType("date_finalisation", "date");
            $form->setType("date_envoi_signature", "date");
            $form->setType("date_retour_signature", "date");
            $form->setType("date_envoi_controle_legalite", "date");
            $form->setType("date_retour_controle_legalite", "date");
            $form->setType("date_envoi_rar", "date");
            $form->setType("date_retour_rar", "date");
            $form->setType("code_barres", "text");
            $form->setType("date_envoi_mail_om_fichier_finalise_courrier", "date");
            $form->setType("date_envoi_mail_om_fichier_signe_courrier", "date");
            $form->setType("mailing", "checkbox");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_parent", "selecthiddenstatic");
            } else {
                $form->setType("courrier_parent", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_joint", "selecthiddenstatic");
            } else {
                $form->setType("courrier_joint", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition", "select");
            }
            if ($this->is_in_context_of_foreign_key("signataire", $this->retourformulaire)) {
                $form->setType("signataire", "selecthiddenstatic");
            } else {
                $form->setType("signataire", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("proces_verbal", $this->retourformulaire)) {
                $form->setType("proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("proces_verbal", "select");
            }
            if ($this->is_in_context_of_foreign_key("visite", $this->retourformulaire)) {
                $form->setType("visite", "selecthiddenstatic");
            } else {
                $form->setType("visite", "select");
            }
            $form->setType("arrete_numero", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("courrier", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            $form->setType("complement1_om_html", "html");
            $form->setType("complement2_om_html", "html");
            $form->setType("finalise", "checkbox");
            $form->setType("om_fichier_finalise_courrier", "text");
            $form->setType("om_fichier_signe_courrier", "text");
            $form->setType("om_date_creation", "date");
            $form->setType("date_finalisation", "date");
            $form->setType("date_envoi_signature", "date");
            $form->setType("date_retour_signature", "date");
            $form->setType("date_envoi_controle_legalite", "date");
            $form->setType("date_retour_controle_legalite", "date");
            $form->setType("date_envoi_rar", "date");
            $form->setType("date_retour_rar", "date");
            $form->setType("code_barres", "text");
            $form->setType("date_envoi_mail_om_fichier_finalise_courrier", "date");
            $form->setType("date_envoi_mail_om_fichier_signe_courrier", "date");
            $form->setType("mailing", "checkbox");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_parent", "selecthiddenstatic");
            } else {
                $form->setType("courrier_parent", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_joint", "selecthiddenstatic");
            } else {
                $form->setType("courrier_joint", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition", "select");
            }
            if ($this->is_in_context_of_foreign_key("signataire", $this->retourformulaire)) {
                $form->setType("signataire", "selecthiddenstatic");
            } else {
                $form->setType("signataire", "select");
            }
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("proces_verbal", $this->retourformulaire)) {
                $form->setType("proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("proces_verbal", "select");
            }
            if ($this->is_in_context_of_foreign_key("visite", $this->retourformulaire)) {
                $form->setType("visite", "selecthiddenstatic");
            } else {
                $form->setType("visite", "select");
            }
            $form->setType("arrete_numero", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("courrier", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("complement1_om_html", "hiddenstatic");
            $form->setType("complement2_om_html", "hiddenstatic");
            $form->setType("finalise", "hiddenstatic");
            $form->setType("om_fichier_finalise_courrier", "hiddenstatic");
            $form->setType("om_fichier_signe_courrier", "hiddenstatic");
            $form->setType("om_date_creation", "hiddenstatic");
            $form->setType("date_finalisation", "hiddenstatic");
            $form->setType("date_envoi_signature", "hiddenstatic");
            $form->setType("date_retour_signature", "hiddenstatic");
            $form->setType("date_envoi_controle_legalite", "hiddenstatic");
            $form->setType("date_retour_controle_legalite", "hiddenstatic");
            $form->setType("date_envoi_rar", "hiddenstatic");
            $form->setType("date_retour_rar", "hiddenstatic");
            $form->setType("code_barres", "hiddenstatic");
            $form->setType("date_envoi_mail_om_fichier_finalise_courrier", "hiddenstatic");
            $form->setType("date_envoi_mail_om_fichier_signe_courrier", "hiddenstatic");
            $form->setType("mailing", "hiddenstatic");
            $form->setType("courrier_parent", "selectstatic");
            $form->setType("courrier_joint", "selectstatic");
            $form->setType("modele_edition", "selectstatic");
            $form->setType("signataire", "selectstatic");
            $form->setType("courrier_type", "selectstatic");
            $form->setType("proces_verbal", "selectstatic");
            $form->setType("visite", "selectstatic");
            $form->setType("arrete_numero", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("courrier", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("complement1_om_html", "htmlstatic");
            $form->setType("complement2_om_html", "htmlstatic");
            $form->setType("finalise", "checkboxstatic");
            $form->setType("om_fichier_finalise_courrier", "static");
            $form->setType("om_fichier_signe_courrier", "static");
            $form->setType("om_date_creation", "datestatic");
            $form->setType("date_finalisation", "datestatic");
            $form->setType("date_envoi_signature", "datestatic");
            $form->setType("date_retour_signature", "datestatic");
            $form->setType("date_envoi_controle_legalite", "datestatic");
            $form->setType("date_retour_controle_legalite", "datestatic");
            $form->setType("date_envoi_rar", "datestatic");
            $form->setType("date_retour_rar", "datestatic");
            $form->setType("code_barres", "static");
            $form->setType("date_envoi_mail_om_fichier_finalise_courrier", "datestatic");
            $form->setType("date_envoi_mail_om_fichier_signe_courrier", "datestatic");
            $form->setType("mailing", "checkboxstatic");
            $form->setType("courrier_parent", "selectstatic");
            $form->setType("courrier_joint", "selectstatic");
            $form->setType("modele_edition", "selectstatic");
            $form->setType("signataire", "selectstatic");
            $form->setType("courrier_type", "selectstatic");
            $form->setType("proces_verbal", "selectstatic");
            $form->setType("visite", "selectstatic");
            $form->setType("arrete_numero", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('courrier','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('om_date_creation','fdate(this)');
        $form->setOnchange('date_finalisation','fdate(this)');
        $form->setOnchange('date_envoi_signature','fdate(this)');
        $form->setOnchange('date_retour_signature','fdate(this)');
        $form->setOnchange('date_envoi_controle_legalite','fdate(this)');
        $form->setOnchange('date_retour_controle_legalite','fdate(this)');
        $form->setOnchange('date_envoi_rar','fdate(this)');
        $form->setOnchange('date_retour_rar','fdate(this)');
        $form->setOnchange('date_envoi_mail_om_fichier_finalise_courrier','fdate(this)');
        $form->setOnchange('date_envoi_mail_om_fichier_signe_courrier','fdate(this)');
        $form->setOnchange('courrier_parent','VerifNum(this)');
        $form->setOnchange('courrier_joint','VerifNum(this)');
        $form->setOnchange('modele_edition','VerifNum(this)');
        $form->setOnchange('signataire','VerifNum(this)');
        $form->setOnchange('courrier_type','VerifNum(this)');
        $form->setOnchange('proces_verbal','VerifNum(this)');
        $form->setOnchange('visite','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("courrier", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("complement1_om_html", 80);
        $form->setTaille("complement2_om_html", 80);
        $form->setTaille("finalise", 1);
        $form->setTaille("om_fichier_finalise_courrier", 30);
        $form->setTaille("om_fichier_signe_courrier", 30);
        $form->setTaille("om_date_creation", 12);
        $form->setTaille("date_finalisation", 12);
        $form->setTaille("date_envoi_signature", 12);
        $form->setTaille("date_retour_signature", 12);
        $form->setTaille("date_envoi_controle_legalite", 12);
        $form->setTaille("date_retour_controle_legalite", 12);
        $form->setTaille("date_envoi_rar", 12);
        $form->setTaille("date_retour_rar", 12);
        $form->setTaille("code_barres", 12);
        $form->setTaille("date_envoi_mail_om_fichier_finalise_courrier", 12);
        $form->setTaille("date_envoi_mail_om_fichier_signe_courrier", 12);
        $form->setTaille("mailing", 1);
        $form->setTaille("courrier_parent", 11);
        $form->setTaille("courrier_joint", 11);
        $form->setTaille("modele_edition", 11);
        $form->setTaille("signataire", 11);
        $form->setTaille("courrier_type", 11);
        $form->setTaille("proces_verbal", 11);
        $form->setTaille("visite", 11);
        $form->setTaille("arrete_numero", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("courrier", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("complement1_om_html", 6);
        $form->setMax("complement2_om_html", 6);
        $form->setMax("finalise", 1);
        $form->setMax("om_fichier_finalise_courrier", 64);
        $form->setMax("om_fichier_signe_courrier", 64);
        $form->setMax("om_date_creation", 12);
        $form->setMax("date_finalisation", 12);
        $form->setMax("date_envoi_signature", 12);
        $form->setMax("date_retour_signature", 12);
        $form->setMax("date_envoi_controle_legalite", 12);
        $form->setMax("date_retour_controle_legalite", 12);
        $form->setMax("date_envoi_rar", 12);
        $form->setMax("date_retour_rar", 12);
        $form->setMax("code_barres", 12);
        $form->setMax("date_envoi_mail_om_fichier_finalise_courrier", 12);
        $form->setMax("date_envoi_mail_om_fichier_signe_courrier", 12);
        $form->setMax("mailing", 1);
        $form->setMax("courrier_parent", 11);
        $form->setMax("courrier_joint", 11);
        $form->setMax("modele_edition", 11);
        $form->setMax("signataire", 11);
        $form->setMax("courrier_type", 11);
        $form->setMax("proces_verbal", 11);
        $form->setMax("visite", 11);
        $form->setMax("arrete_numero", 30);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('courrier',_('courrier'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('dossier_coordination',_('dossier_coordination'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('complement1_om_html',_('complement1_om_html'));
        $form->setLib('complement2_om_html',_('complement2_om_html'));
        $form->setLib('finalise',_('finalise'));
        $form->setLib('om_fichier_finalise_courrier',_('om_fichier_finalise_courrier'));
        $form->setLib('om_fichier_signe_courrier',_('om_fichier_signe_courrier'));
        $form->setLib('om_date_creation',_('om_date_creation'));
        $form->setLib('date_finalisation',_('date_finalisation'));
        $form->setLib('date_envoi_signature',_('date_envoi_signature'));
        $form->setLib('date_retour_signature',_('date_retour_signature'));
        $form->setLib('date_envoi_controle_legalite',_('date_envoi_controle_legalite'));
        $form->setLib('date_retour_controle_legalite',_('date_retour_controle_legalite'));
        $form->setLib('date_envoi_rar',_('date_envoi_rar'));
        $form->setLib('date_retour_rar',_('date_retour_rar'));
        $form->setLib('code_barres',_('code_barres'));
        $form->setLib('date_envoi_mail_om_fichier_finalise_courrier',_('date_envoi_mail_om_fichier_finalise_courrier'));
        $form->setLib('date_envoi_mail_om_fichier_signe_courrier',_('date_envoi_mail_om_fichier_signe_courrier'));
        $form->setLib('mailing',_('mailing'));
        $form->setLib('courrier_parent',_('courrier_parent'));
        $form->setLib('courrier_joint',_('courrier_joint'));
        $form->setLib('modele_edition',_('modele_edition'));
        $form->setLib('signataire',_('signataire'));
        $form->setLib('courrier_type',_('courrier_type'));
        $form->setLib('proces_verbal',_('proces_verbal'));
        $form->setLib('visite',_('visite'));
        $form->setLib('arrete_numero',_('arrete_numero'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // courrier_joint
        $this->init_select($form, $this->f->db, $maj, null, "courrier_joint", $sql_courrier_joint, $sql_courrier_joint_by_id, false);
        // courrier_parent
        $this->init_select($form, $this->f->db, $maj, null, "courrier_parent", $sql_courrier_parent, $sql_courrier_parent_by_id, false);
        // courrier_type
        $this->init_select($form, $this->f->db, $maj, null, "courrier_type", $sql_courrier_type, $sql_courrier_type_by_id, true);
        // dossier_coordination
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination", $sql_dossier_coordination, $sql_dossier_coordination_by_id, false);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
        // modele_edition
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition", $sql_modele_edition, $sql_modele_edition_by_id, true);
        // proces_verbal
        $this->init_select($form, $this->f->db, $maj, null, "proces_verbal", $sql_proces_verbal, $sql_proces_verbal_by_id, false);
        // signataire
        $this->init_select($form, $this->f->db, $maj, null, "signataire", $sql_signataire, $sql_signataire_by_id, true);
        // visite
        $this->init_select($form, $this->f->db, $maj, null, "visite", $sql_visite, $sql_visite_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('courrier_type', $this->retourformulaire))
                $form->setVal('courrier_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition', $idxformulaire);
            if($this->is_in_context_of_foreign_key('proces_verbal', $this->retourformulaire))
                $form->setVal('proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('signataire', $this->retourformulaire))
                $form->setVal('signataire', $idxformulaire);
            if($this->is_in_context_of_foreign_key('visite', $this->retourformulaire))
                $form->setVal('visite', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier_joint', $idxformulaire);
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier_parent', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "courrier_joint", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "courrier_parent", $id);
        // Verification de la cle secondaire : lien_autorite_police_courrier
        $this->rechercheTable($this->f->db, "lien_autorite_police_courrier", "courrier", $id);
        // Verification de la cle secondaire : lien_courrier_contact
        $this->rechercheTable($this->f->db, "lien_courrier_contact", "courrier", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "courrier_genere", $id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "courrier_annulation", $id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "courrier_convocation_exploitants", $id);
    }


}

?>
