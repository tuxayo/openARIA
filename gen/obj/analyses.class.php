<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class analyses_gen extends om_dbform {

    var $table = "analyses";
    var $clePrimaire = "analyses";
    var $typeCle = "N";
    var $required_field = array(
        "analyses",
        "analyses_etat",
        "dossier_instruction",
        "modele_edition_compte_rendu",
        "modele_edition_proces_verbal",
        "modele_edition_rapport",
        "modifiee_sans_gen",
        "service"
    );
    var $unique_key = array(
      "dossier_instruction",
    );
    var $foreign_keys_extended = array(
        "derogation_scda" => array("derogation_scda", ),
        "analyses_type" => array("analyses_type", ),
        "proces_verbal" => array("proces_verbal", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "modele_edition" => array("modele_edition", ),
        "reunion_avis" => array("reunion_avis", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['analyses'])) {
            $this->valF['analyses'] = ""; // -> requis
        } else {
            $this->valF['analyses'] = $val['analyses'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        $this->valF['analyses_etat'] = $val['analyses_etat'];
        if (!is_numeric($val['analyses_type'])) {
            $this->valF['analyses_type'] = NULL;
        } else {
            $this->valF['analyses_type'] = $val['analyses_type'];
        }
            $this->valF['objet'] = $val['objet'];
            $this->valF['descriptif_etablissement_om_html'] = $val['descriptif_etablissement_om_html'];
            $this->valF['reglementation_applicable_om_html'] = $val['reglementation_applicable_om_html'];
            $this->valF['compte_rendu_om_html'] = $val['compte_rendu_om_html'];
            $this->valF['document_presente_pendant_om_html'] = $val['document_presente_pendant_om_html'];
            $this->valF['document_presente_apres_om_html'] = $val['document_presente_apres_om_html'];
            $this->valF['observation_om_html'] = $val['observation_om_html'];
        if (!is_numeric($val['reunion_avis'])) {
            $this->valF['reunion_avis'] = NULL;
        } else {
            $this->valF['reunion_avis'] = $val['reunion_avis'];
        }
        if ($val['avis_complement'] == "") {
            $this->valF['avis_complement'] = NULL;
        } else {
            $this->valF['avis_complement'] = $val['avis_complement'];
        }
        if (!is_numeric($val['si_effectif_public'])) {
            $this->valF['si_effectif_public'] = NULL;
        } else {
            $this->valF['si_effectif_public'] = $val['si_effectif_public'];
        }
        if (!is_numeric($val['si_effectif_personnel'])) {
            $this->valF['si_effectif_personnel'] = NULL;
        } else {
            $this->valF['si_effectif_personnel'] = $val['si_effectif_personnel'];
        }
        if ($val['si_type_ssi'] == "") {
            $this->valF['si_type_ssi'] = NULL;
        } else {
            $this->valF['si_type_ssi'] = $val['si_type_ssi'];
        }
        if ($val['si_type_alarme'] == "") {
            $this->valF['si_type_alarme'] = NULL;
        } else {
            $this->valF['si_type_alarme'] = $val['si_type_alarme'];
        }
        if ($val['si_conformite_l16'] == 1 || $val['si_conformite_l16'] == "t" || $val['si_conformite_l16'] == "Oui") {
            $this->valF['si_conformite_l16'] = true;
        } else {
            $this->valF['si_conformite_l16'] = false;
        }
        if ($val['si_alimentation_remplacement'] == 1 || $val['si_alimentation_remplacement'] == "t" || $val['si_alimentation_remplacement'] == "Oui") {
            $this->valF['si_alimentation_remplacement'] = true;
        } else {
            $this->valF['si_alimentation_remplacement'] = false;
        }
        if ($val['si_service_securite'] == 1 || $val['si_service_securite'] == "t" || $val['si_service_securite'] == "Oui") {
            $this->valF['si_service_securite'] = true;
        } else {
            $this->valF['si_service_securite'] = false;
        }
        if (!is_numeric($val['si_personnel_jour'])) {
            $this->valF['si_personnel_jour'] = NULL;
        } else {
            $this->valF['si_personnel_jour'] = $val['si_personnel_jour'];
        }
        if (!is_numeric($val['si_personnel_nuit'])) {
            $this->valF['si_personnel_nuit'] = NULL;
        } else {
            $this->valF['si_personnel_nuit'] = $val['si_personnel_nuit'];
        }
        if ($val['acc_handicap_mental'] == 1 || $val['acc_handicap_mental'] == "t" || $val['acc_handicap_mental'] == "Oui") {
            $this->valF['acc_handicap_mental'] = true;
        } else {
            $this->valF['acc_handicap_mental'] = false;
        }
        if ($val['acc_handicap_auditif'] == 1 || $val['acc_handicap_auditif'] == "t" || $val['acc_handicap_auditif'] == "Oui") {
            $this->valF['acc_handicap_auditif'] = true;
        } else {
            $this->valF['acc_handicap_auditif'] = false;
        }
        if (!is_numeric($val['acc_places_stationnement_amenagees'])) {
            $this->valF['acc_places_stationnement_amenagees'] = NULL;
        } else {
            $this->valF['acc_places_stationnement_amenagees'] = $val['acc_places_stationnement_amenagees'];
        }
        if ($val['acc_elevateur'] == 1 || $val['acc_elevateur'] == "t" || $val['acc_elevateur'] == "Oui") {
            $this->valF['acc_elevateur'] = true;
        } else {
            $this->valF['acc_elevateur'] = false;
        }
        if ($val['acc_handicap_physique'] == 1 || $val['acc_handicap_physique'] == "t" || $val['acc_handicap_physique'] == "Oui") {
            $this->valF['acc_handicap_physique'] = true;
        } else {
            $this->valF['acc_handicap_physique'] = false;
        }
        if ($val['acc_ascenseur'] == 1 || $val['acc_ascenseur'] == "t" || $val['acc_ascenseur'] == "Oui") {
            $this->valF['acc_ascenseur'] = true;
        } else {
            $this->valF['acc_ascenseur'] = false;
        }
        if ($val['acc_handicap_visuel'] == 1 || $val['acc_handicap_visuel'] == "t" || $val['acc_handicap_visuel'] == "Oui") {
            $this->valF['acc_handicap_visuel'] = true;
        } else {
            $this->valF['acc_handicap_visuel'] = false;
        }
        if ($val['acc_boucle_magnetique'] == 1 || $val['acc_boucle_magnetique'] == "t" || $val['acc_boucle_magnetique'] == "Oui") {
            $this->valF['acc_boucle_magnetique'] = true;
        } else {
            $this->valF['acc_boucle_magnetique'] = false;
        }
        if (!is_numeric($val['acc_chambres_amenagees'])) {
            $this->valF['acc_chambres_amenagees'] = NULL;
        } else {
            $this->valF['acc_chambres_amenagees'] = $val['acc_chambres_amenagees'];
        }
        if ($val['acc_douche'] == 1 || $val['acc_douche'] == "t" || $val['acc_douche'] == "Oui") {
            $this->valF['acc_douche'] = true;
        } else {
            $this->valF['acc_douche'] = false;
        }
        if (!is_numeric($val['acc_derogation_scda'])) {
            $this->valF['acc_derogation_scda'] = NULL;
        } else {
            $this->valF['acc_derogation_scda'] = $val['acc_derogation_scda'];
        }
        if ($val['acc_sanitaire'] == 1 || $val['acc_sanitaire'] == "t" || $val['acc_sanitaire'] == "Oui") {
            $this->valF['acc_sanitaire'] = true;
        } else {
            $this->valF['acc_sanitaire'] = false;
        }
        if (!is_numeric($val['acc_places_assises_public'])) {
            $this->valF['acc_places_assises_public'] = NULL;
        } else {
            $this->valF['acc_places_assises_public'] = $val['acc_places_assises_public'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['modele_edition_rapport'])) {
            $this->valF['modele_edition_rapport'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_rapport'] = $val['modele_edition_rapport'];
        }
        if (!is_numeric($val['modele_edition_compte_rendu'])) {
            $this->valF['modele_edition_compte_rendu'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_compte_rendu'] = $val['modele_edition_compte_rendu'];
        }
        if (!is_numeric($val['modele_edition_proces_verbal'])) {
            $this->valF['modele_edition_proces_verbal'] = ""; // -> requis
        } else {
            $this->valF['modele_edition_proces_verbal'] = $val['modele_edition_proces_verbal'];
        }
        if ($val['modifiee_sans_gen'] == 1 || $val['modifiee_sans_gen'] == "t" || $val['modifiee_sans_gen'] == "Oui") {
            $this->valF['modifiee_sans_gen'] = true;
        } else {
            $this->valF['modifiee_sans_gen'] = false;
        }
        if (!is_numeric($val['dernier_pv'])) {
            $this->valF['dernier_pv'] = NULL;
        } else {
            $this->valF['dernier_pv'] = $val['dernier_pv'];
        }
        if ($val['dec1'] == "") {
            $this->valF['dec1'] = NULL;
        } else {
            $this->valF['dec1'] = $val['dec1'];
        }
        if ($val['delai1'] == "") {
            $this->valF['delai1'] = NULL;
        } else {
            $this->valF['delai1'] = $val['delai1'];
        }
        if ($val['dec2'] == "") {
            $this->valF['dec2'] = NULL;
        } else {
            $this->valF['dec2'] = $val['dec2'];
        }
        if ($val['delai2'] == "") {
            $this->valF['delai2'] = NULL;
        } else {
            $this->valF['delai2'] = $val['delai2'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("analyses", "hidden");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("analyses_etat", "text");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type", "select");
            }
            $form->setType("objet", "textarea");
            $form->setType("descriptif_etablissement_om_html", "html");
            $form->setType("reglementation_applicable_om_html", "html");
            $form->setType("compte_rendu_om_html", "html");
            $form->setType("document_presente_pendant_om_html", "html");
            $form->setType("document_presente_apres_om_html", "html");
            $form->setType("observation_om_html", "html");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("reunion_avis", "selecthiddenstatic");
            } else {
                $form->setType("reunion_avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("si_effectif_public", "text");
            $form->setType("si_effectif_personnel", "text");
            $form->setType("si_type_ssi", "text");
            $form->setType("si_type_alarme", "text");
            $form->setType("si_conformite_l16", "checkbox");
            $form->setType("si_alimentation_remplacement", "checkbox");
            $form->setType("si_service_securite", "checkbox");
            $form->setType("si_personnel_jour", "text");
            $form->setType("si_personnel_nuit", "text");
            $form->setType("acc_handicap_mental", "checkbox");
            $form->setType("acc_handicap_auditif", "checkbox");
            $form->setType("acc_places_stationnement_amenagees", "text");
            $form->setType("acc_elevateur", "checkbox");
            $form->setType("acc_handicap_physique", "checkbox");
            $form->setType("acc_ascenseur", "checkbox");
            $form->setType("acc_handicap_visuel", "checkbox");
            $form->setType("acc_boucle_magnetique", "checkbox");
            $form->setType("acc_chambres_amenagees", "text");
            $form->setType("acc_douche", "checkbox");
            if ($this->is_in_context_of_foreign_key("derogation_scda", $this->retourformulaire)) {
                $form->setType("acc_derogation_scda", "selecthiddenstatic");
            } else {
                $form->setType("acc_derogation_scda", "select");
            }
            $form->setType("acc_sanitaire", "checkbox");
            $form->setType("acc_places_assises_public", "text");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_rapport", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_rapport", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_compte_rendu", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_compte_rendu", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_proces_verbal", "select");
            }
            $form->setType("modifiee_sans_gen", "checkbox");
            if ($this->is_in_context_of_foreign_key("proces_verbal", $this->retourformulaire)) {
                $form->setType("dernier_pv", "selecthiddenstatic");
            } else {
                $form->setType("dernier_pv", "select");
            }
            $form->setType("dec1", "text");
            $form->setType("delai1", "text");
            $form->setType("dec2", "text");
            $form->setType("delai2", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("analyses", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("analyses_etat", "text");
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type", "select");
            }
            $form->setType("objet", "textarea");
            $form->setType("descriptif_etablissement_om_html", "html");
            $form->setType("reglementation_applicable_om_html", "html");
            $form->setType("compte_rendu_om_html", "html");
            $form->setType("document_presente_pendant_om_html", "html");
            $form->setType("document_presente_apres_om_html", "html");
            $form->setType("observation_om_html", "html");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("reunion_avis", "selecthiddenstatic");
            } else {
                $form->setType("reunion_avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("si_effectif_public", "text");
            $form->setType("si_effectif_personnel", "text");
            $form->setType("si_type_ssi", "text");
            $form->setType("si_type_alarme", "text");
            $form->setType("si_conformite_l16", "checkbox");
            $form->setType("si_alimentation_remplacement", "checkbox");
            $form->setType("si_service_securite", "checkbox");
            $form->setType("si_personnel_jour", "text");
            $form->setType("si_personnel_nuit", "text");
            $form->setType("acc_handicap_mental", "checkbox");
            $form->setType("acc_handicap_auditif", "checkbox");
            $form->setType("acc_places_stationnement_amenagees", "text");
            $form->setType("acc_elevateur", "checkbox");
            $form->setType("acc_handicap_physique", "checkbox");
            $form->setType("acc_ascenseur", "checkbox");
            $form->setType("acc_handicap_visuel", "checkbox");
            $form->setType("acc_boucle_magnetique", "checkbox");
            $form->setType("acc_chambres_amenagees", "text");
            $form->setType("acc_douche", "checkbox");
            if ($this->is_in_context_of_foreign_key("derogation_scda", $this->retourformulaire)) {
                $form->setType("acc_derogation_scda", "selecthiddenstatic");
            } else {
                $form->setType("acc_derogation_scda", "select");
            }
            $form->setType("acc_sanitaire", "checkbox");
            $form->setType("acc_places_assises_public", "text");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_rapport", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_rapport", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_compte_rendu", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_compte_rendu", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition_proces_verbal", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition_proces_verbal", "select");
            }
            $form->setType("modifiee_sans_gen", "checkbox");
            if ($this->is_in_context_of_foreign_key("proces_verbal", $this->retourformulaire)) {
                $form->setType("dernier_pv", "selecthiddenstatic");
            } else {
                $form->setType("dernier_pv", "select");
            }
            $form->setType("dec1", "text");
            $form->setType("delai1", "text");
            $form->setType("dec2", "text");
            $form->setType("delai2", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("analyses", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("analyses_etat", "hiddenstatic");
            $form->setType("analyses_type", "selectstatic");
            $form->setType("objet", "hiddenstatic");
            $form->setType("descriptif_etablissement_om_html", "hiddenstatic");
            $form->setType("reglementation_applicable_om_html", "hiddenstatic");
            $form->setType("compte_rendu_om_html", "hiddenstatic");
            $form->setType("document_presente_pendant_om_html", "hiddenstatic");
            $form->setType("document_presente_apres_om_html", "hiddenstatic");
            $form->setType("observation_om_html", "hiddenstatic");
            $form->setType("reunion_avis", "selectstatic");
            $form->setType("avis_complement", "hiddenstatic");
            $form->setType("si_effectif_public", "hiddenstatic");
            $form->setType("si_effectif_personnel", "hiddenstatic");
            $form->setType("si_type_ssi", "hiddenstatic");
            $form->setType("si_type_alarme", "hiddenstatic");
            $form->setType("si_conformite_l16", "hiddenstatic");
            $form->setType("si_alimentation_remplacement", "hiddenstatic");
            $form->setType("si_service_securite", "hiddenstatic");
            $form->setType("si_personnel_jour", "hiddenstatic");
            $form->setType("si_personnel_nuit", "hiddenstatic");
            $form->setType("acc_handicap_mental", "hiddenstatic");
            $form->setType("acc_handicap_auditif", "hiddenstatic");
            $form->setType("acc_places_stationnement_amenagees", "hiddenstatic");
            $form->setType("acc_elevateur", "hiddenstatic");
            $form->setType("acc_handicap_physique", "hiddenstatic");
            $form->setType("acc_ascenseur", "hiddenstatic");
            $form->setType("acc_handicap_visuel", "hiddenstatic");
            $form->setType("acc_boucle_magnetique", "hiddenstatic");
            $form->setType("acc_chambres_amenagees", "hiddenstatic");
            $form->setType("acc_douche", "hiddenstatic");
            $form->setType("acc_derogation_scda", "selectstatic");
            $form->setType("acc_sanitaire", "hiddenstatic");
            $form->setType("acc_places_assises_public", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("modele_edition_rapport", "selectstatic");
            $form->setType("modele_edition_compte_rendu", "selectstatic");
            $form->setType("modele_edition_proces_verbal", "selectstatic");
            $form->setType("modifiee_sans_gen", "hiddenstatic");
            $form->setType("dernier_pv", "selectstatic");
            $form->setType("dec1", "hiddenstatic");
            $form->setType("delai1", "hiddenstatic");
            $form->setType("dec2", "hiddenstatic");
            $form->setType("delai2", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("analyses", "static");
            $form->setType("service", "selectstatic");
            $form->setType("analyses_etat", "static");
            $form->setType("analyses_type", "selectstatic");
            $form->setType("objet", "textareastatic");
            $form->setType("descriptif_etablissement_om_html", "htmlstatic");
            $form->setType("reglementation_applicable_om_html", "htmlstatic");
            $form->setType("compte_rendu_om_html", "htmlstatic");
            $form->setType("document_presente_pendant_om_html", "htmlstatic");
            $form->setType("document_presente_apres_om_html", "htmlstatic");
            $form->setType("observation_om_html", "htmlstatic");
            $form->setType("reunion_avis", "selectstatic");
            $form->setType("avis_complement", "static");
            $form->setType("si_effectif_public", "static");
            $form->setType("si_effectif_personnel", "static");
            $form->setType("si_type_ssi", "static");
            $form->setType("si_type_alarme", "static");
            $form->setType("si_conformite_l16", "checkboxstatic");
            $form->setType("si_alimentation_remplacement", "checkboxstatic");
            $form->setType("si_service_securite", "checkboxstatic");
            $form->setType("si_personnel_jour", "static");
            $form->setType("si_personnel_nuit", "static");
            $form->setType("acc_handicap_mental", "checkboxstatic");
            $form->setType("acc_handicap_auditif", "checkboxstatic");
            $form->setType("acc_places_stationnement_amenagees", "static");
            $form->setType("acc_elevateur", "checkboxstatic");
            $form->setType("acc_handicap_physique", "checkboxstatic");
            $form->setType("acc_ascenseur", "checkboxstatic");
            $form->setType("acc_handicap_visuel", "checkboxstatic");
            $form->setType("acc_boucle_magnetique", "checkboxstatic");
            $form->setType("acc_chambres_amenagees", "static");
            $form->setType("acc_douche", "checkboxstatic");
            $form->setType("acc_derogation_scda", "selectstatic");
            $form->setType("acc_sanitaire", "checkboxstatic");
            $form->setType("acc_places_assises_public", "static");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("modele_edition_rapport", "selectstatic");
            $form->setType("modele_edition_compte_rendu", "selectstatic");
            $form->setType("modele_edition_proces_verbal", "selectstatic");
            $form->setType("modifiee_sans_gen", "checkboxstatic");
            $form->setType("dernier_pv", "selectstatic");
            $form->setType("dec1", "static");
            $form->setType("delai1", "static");
            $form->setType("dec2", "static");
            $form->setType("delai2", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('analyses','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('analyses_type','VerifNum(this)');
        $form->setOnchange('reunion_avis','VerifNum(this)');
        $form->setOnchange('si_effectif_public','VerifNum(this)');
        $form->setOnchange('si_effectif_personnel','VerifNum(this)');
        $form->setOnchange('si_personnel_jour','VerifNum(this)');
        $form->setOnchange('si_personnel_nuit','VerifNum(this)');
        $form->setOnchange('acc_places_stationnement_amenagees','VerifNum(this)');
        $form->setOnchange('acc_chambres_amenagees','VerifNum(this)');
        $form->setOnchange('acc_derogation_scda','VerifNum(this)');
        $form->setOnchange('acc_places_assises_public','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('modele_edition_rapport','VerifNum(this)');
        $form->setOnchange('modele_edition_compte_rendu','VerifNum(this)');
        $form->setOnchange('modele_edition_proces_verbal','VerifNum(this)');
        $form->setOnchange('dernier_pv','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("analyses", 11);
        $form->setTaille("service", 11);
        $form->setTaille("analyses_etat", 30);
        $form->setTaille("analyses_type", 11);
        $form->setTaille("objet", 80);
        $form->setTaille("descriptif_etablissement_om_html", 80);
        $form->setTaille("reglementation_applicable_om_html", 80);
        $form->setTaille("compte_rendu_om_html", 80);
        $form->setTaille("document_presente_pendant_om_html", 80);
        $form->setTaille("document_presente_apres_om_html", 80);
        $form->setTaille("observation_om_html", 80);
        $form->setTaille("reunion_avis", 11);
        $form->setTaille("avis_complement", 30);
        $form->setTaille("si_effectif_public", 11);
        $form->setTaille("si_effectif_personnel", 11);
        $form->setTaille("si_type_ssi", 30);
        $form->setTaille("si_type_alarme", 30);
        $form->setTaille("si_conformite_l16", 1);
        $form->setTaille("si_alimentation_remplacement", 1);
        $form->setTaille("si_service_securite", 1);
        $form->setTaille("si_personnel_jour", 11);
        $form->setTaille("si_personnel_nuit", 11);
        $form->setTaille("acc_handicap_mental", 1);
        $form->setTaille("acc_handicap_auditif", 1);
        $form->setTaille("acc_places_stationnement_amenagees", 11);
        $form->setTaille("acc_elevateur", 1);
        $form->setTaille("acc_handicap_physique", 1);
        $form->setTaille("acc_ascenseur", 1);
        $form->setTaille("acc_handicap_visuel", 1);
        $form->setTaille("acc_boucle_magnetique", 1);
        $form->setTaille("acc_chambres_amenagees", 11);
        $form->setTaille("acc_douche", 1);
        $form->setTaille("acc_derogation_scda", 11);
        $form->setTaille("acc_sanitaire", 1);
        $form->setTaille("acc_places_assises_public", 11);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("modele_edition_rapport", 11);
        $form->setTaille("modele_edition_compte_rendu", 11);
        $form->setTaille("modele_edition_proces_verbal", 11);
        $form->setTaille("modifiee_sans_gen", 1);
        $form->setTaille("dernier_pv", 11);
        $form->setTaille("dec1", 30);
        $form->setTaille("delai1", 30);
        $form->setTaille("dec2", 30);
        $form->setTaille("delai2", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("analyses", 11);
        $form->setMax("service", 11);
        $form->setMax("analyses_etat", 100);
        $form->setMax("analyses_type", 11);
        $form->setMax("objet", 6);
        $form->setMax("descriptif_etablissement_om_html", 6);
        $form->setMax("reglementation_applicable_om_html", 6);
        $form->setMax("compte_rendu_om_html", 6);
        $form->setMax("document_presente_pendant_om_html", 6);
        $form->setMax("document_presente_apres_om_html", 6);
        $form->setMax("observation_om_html", 6);
        $form->setMax("reunion_avis", 11);
        $form->setMax("avis_complement", 250);
        $form->setMax("si_effectif_public", 11);
        $form->setMax("si_effectif_personnel", 11);
        $form->setMax("si_type_ssi", 100);
        $form->setMax("si_type_alarme", 100);
        $form->setMax("si_conformite_l16", 1);
        $form->setMax("si_alimentation_remplacement", 1);
        $form->setMax("si_service_securite", 1);
        $form->setMax("si_personnel_jour", 11);
        $form->setMax("si_personnel_nuit", 11);
        $form->setMax("acc_handicap_mental", 1);
        $form->setMax("acc_handicap_auditif", 1);
        $form->setMax("acc_places_stationnement_amenagees", 11);
        $form->setMax("acc_elevateur", 1);
        $form->setMax("acc_handicap_physique", 1);
        $form->setMax("acc_ascenseur", 1);
        $form->setMax("acc_handicap_visuel", 1);
        $form->setMax("acc_boucle_magnetique", 1);
        $form->setMax("acc_chambres_amenagees", 11);
        $form->setMax("acc_douche", 1);
        $form->setMax("acc_derogation_scda", 11);
        $form->setMax("acc_sanitaire", 1);
        $form->setMax("acc_places_assises_public", 11);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("modele_edition_rapport", 11);
        $form->setMax("modele_edition_compte_rendu", 11);
        $form->setMax("modele_edition_proces_verbal", 11);
        $form->setMax("modifiee_sans_gen", 1);
        $form->setMax("dernier_pv", 11);
        $form->setMax("dec1", 250);
        $form->setMax("delai1", 250);
        $form->setMax("dec2", 250);
        $form->setMax("delai2", 250);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('analyses',_('analyses'));
        $form->setLib('service',_('service'));
        $form->setLib('analyses_etat',_('analyses_etat'));
        $form->setLib('analyses_type',_('analyses_type'));
        $form->setLib('objet',_('objet'));
        $form->setLib('descriptif_etablissement_om_html',_('descriptif_etablissement_om_html'));
        $form->setLib('reglementation_applicable_om_html',_('reglementation_applicable_om_html'));
        $form->setLib('compte_rendu_om_html',_('compte_rendu_om_html'));
        $form->setLib('document_presente_pendant_om_html',_('document_presente_pendant_om_html'));
        $form->setLib('document_presente_apres_om_html',_('document_presente_apres_om_html'));
        $form->setLib('observation_om_html',_('observation_om_html'));
        $form->setLib('reunion_avis',_('reunion_avis'));
        $form->setLib('avis_complement',_('avis_complement'));
        $form->setLib('si_effectif_public',_('si_effectif_public'));
        $form->setLib('si_effectif_personnel',_('si_effectif_personnel'));
        $form->setLib('si_type_ssi',_('si_type_ssi'));
        $form->setLib('si_type_alarme',_('si_type_alarme'));
        $form->setLib('si_conformite_l16',_('si_conformite_l16'));
        $form->setLib('si_alimentation_remplacement',_('si_alimentation_remplacement'));
        $form->setLib('si_service_securite',_('si_service_securite'));
        $form->setLib('si_personnel_jour',_('si_personnel_jour'));
        $form->setLib('si_personnel_nuit',_('si_personnel_nuit'));
        $form->setLib('acc_handicap_mental',_('acc_handicap_mental'));
        $form->setLib('acc_handicap_auditif',_('acc_handicap_auditif'));
        $form->setLib('acc_places_stationnement_amenagees',_('acc_places_stationnement_amenagees'));
        $form->setLib('acc_elevateur',_('acc_elevateur'));
        $form->setLib('acc_handicap_physique',_('acc_handicap_physique'));
        $form->setLib('acc_ascenseur',_('acc_ascenseur'));
        $form->setLib('acc_handicap_visuel',_('acc_handicap_visuel'));
        $form->setLib('acc_boucle_magnetique',_('acc_boucle_magnetique'));
        $form->setLib('acc_chambres_amenagees',_('acc_chambres_amenagees'));
        $form->setLib('acc_douche',_('acc_douche'));
        $form->setLib('acc_derogation_scda',_('acc_derogation_scda'));
        $form->setLib('acc_sanitaire',_('acc_sanitaire'));
        $form->setLib('acc_places_assises_public',_('acc_places_assises_public'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('modele_edition_rapport',_('modele_edition_rapport'));
        $form->setLib('modele_edition_compte_rendu',_('modele_edition_compte_rendu'));
        $form->setLib('modele_edition_proces_verbal',_('modele_edition_proces_verbal'));
        $form->setLib('modifiee_sans_gen',_('modifiee_sans_gen'));
        $form->setLib('dernier_pv',_('dernier_pv'));
        $form->setLib('dec1',_('dec1'));
        $form->setLib('delai1',_('delai1'));
        $form->setLib('dec2',_('dec2'));
        $form->setLib('delai2',_('delai2'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // acc_derogation_scda
        $this->init_select($form, $this->f->db, $maj, null, "acc_derogation_scda", $sql_acc_derogation_scda, $sql_acc_derogation_scda_by_id, true);
        // analyses_type
        $this->init_select($form, $this->f->db, $maj, null, "analyses_type", $sql_analyses_type, $sql_analyses_type_by_id, true);
        // dernier_pv
        $this->init_select($form, $this->f->db, $maj, null, "dernier_pv", $sql_dernier_pv, $sql_dernier_pv_by_id, false);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // modele_edition_compte_rendu
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_compte_rendu", $sql_modele_edition_compte_rendu, $sql_modele_edition_compte_rendu_by_id, true);
        // modele_edition_compte_rendu
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_compte_rendu", $sql_modele_edition_compte_rendu, $sql_modele_edition_compte_rendu_by_id, true);
        // modele_edition_compte_rendu
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_compte_rendu", $sql_modele_edition_compte_rendu, $sql_modele_edition_compte_rendu_by_id, true);
        // modele_edition_compte_rendu
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_compte_rendu", $sql_modele_edition_compte_rendu, $sql_modele_edition_compte_rendu_by_id, true);
        // modele_edition_proces_verbal
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_proces_verbal", $sql_modele_edition_proces_verbal, $sql_modele_edition_proces_verbal_by_id, true);
        // modele_edition_proces_verbal
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_proces_verbal", $sql_modele_edition_proces_verbal, $sql_modele_edition_proces_verbal_by_id, true);
        // modele_edition_proces_verbal
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_proces_verbal", $sql_modele_edition_proces_verbal, $sql_modele_edition_proces_verbal_by_id, true);
        // modele_edition_proces_verbal
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_proces_verbal", $sql_modele_edition_proces_verbal, $sql_modele_edition_proces_verbal_by_id, true);
        // modele_edition_rapport
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_rapport", $sql_modele_edition_rapport, $sql_modele_edition_rapport_by_id, true);
        // modele_edition_rapport
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_rapport", $sql_modele_edition_rapport, $sql_modele_edition_rapport_by_id, true);
        // modele_edition_rapport
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_rapport", $sql_modele_edition_rapport, $sql_modele_edition_rapport_by_id, true);
        // modele_edition_rapport
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition_rapport", $sql_modele_edition_rapport, $sql_modele_edition_rapport_by_id, true);
        // reunion_avis
        $this->init_select($form, $this->f->db, $maj, null, "reunion_avis", $sql_reunion_avis, $sql_reunion_avis_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('derogation_scda', $this->retourformulaire))
                $form->setVal('acc_derogation_scda', $idxformulaire);
            if($this->is_in_context_of_foreign_key('analyses_type', $this->retourformulaire))
                $form->setVal('analyses_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('proces_verbal', $this->retourformulaire))
                $form->setVal('dernier_pv', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('reunion_avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_compte_rendu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_compte_rendu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_compte_rendu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_compte_rendu', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_proces_verbal', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_rapport', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_rapport', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_rapport', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition_rapport', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_essai_realise_analyses
        $this->rechercheTable($this->f->db, "lien_essai_realise_analyses", "analyses", $id);
        // Verification de la cle secondaire : prescription
        $this->rechercheTable($this->f->db, "prescription", "analyses", $id);
    }


}

?>
