<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class proces_verbal_gen extends om_dbform {

    var $table = "proces_verbal";
    var $clePrimaire = "proces_verbal";
    var $typeCle = "N";
    var $required_field = array(
        "date_redaction",
        "dossier_instruction",
        "dossier_instruction_reunion",
        "genere",
        "modele_edition",
        "proces_verbal"
    );
    
    var $foreign_keys_extended = array(
        "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "dossier_instruction_reunion" => array("dossier_instruction_reunion", ),
        "modele_edition" => array("modele_edition", ),
        "signataire" => array("signataire", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['proces_verbal'])) {
            $this->valF['proces_verbal'] = ""; // -> requis
        } else {
            $this->valF['proces_verbal'] = $val['proces_verbal'];
        }
        if ($val['numero'] == "") {
            $this->valF['numero'] = NULL;
        } else {
            $this->valF['numero'] = $val['numero'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['dossier_instruction_reunion'])) {
            $this->valF['dossier_instruction_reunion'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction_reunion'] = $val['dossier_instruction_reunion'];
        }
        if (!is_numeric($val['modele_edition'])) {
            $this->valF['modele_edition'] = ""; // -> requis
        } else {
            $this->valF['modele_edition'] = $val['modele_edition'];
        }
        if ($val['date_redaction'] != "") {
            $this->valF['date_redaction'] = $this->dateDB($val['date_redaction']);
        }
        if (!is_numeric($val['signataire'])) {
            $this->valF['signataire'] = NULL;
        } else {
            $this->valF['signataire'] = $val['signataire'];
        }
        if ($val['genere'] == 1 || $val['genere'] == "t" || $val['genere'] == "Oui") {
            $this->valF['genere'] = true;
        } else {
            $this->valF['genere'] = false;
        }
        if ($val['om_fichier_signe'] == "") {
            $this->valF['om_fichier_signe'] = NULL;
        } else {
            $this->valF['om_fichier_signe'] = $val['om_fichier_signe'];
        }
        if (!is_numeric($val['courrier_genere'])) {
            $this->valF['courrier_genere'] = NULL;
        } else {
            $this->valF['courrier_genere'] = $val['courrier_genere'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("proces_verbal", "hidden");
            $form->setType("numero", "text");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition", "select");
            }
            $form->setType("date_redaction", "date");
            if ($this->is_in_context_of_foreign_key("signataire", $this->retourformulaire)) {
                $form->setType("signataire", "selecthiddenstatic");
            } else {
                $form->setType("signataire", "select");
            }
            $form->setType("genere", "checkbox");
            $form->setType("om_fichier_signe", "text");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_genere", "selecthiddenstatic");
            } else {
                $form->setType("courrier_genere", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("proces_verbal", "hiddenstatic");
            $form->setType("numero", "text");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_edition", "selecthiddenstatic");
            } else {
                $form->setType("modele_edition", "select");
            }
            $form->setType("date_redaction", "date");
            if ($this->is_in_context_of_foreign_key("signataire", $this->retourformulaire)) {
                $form->setType("signataire", "selecthiddenstatic");
            } else {
                $form->setType("signataire", "select");
            }
            $form->setType("genere", "checkbox");
            $form->setType("om_fichier_signe", "text");
            if ($this->is_in_context_of_foreign_key("courrier", $this->retourformulaire)) {
                $form->setType("courrier_genere", "selecthiddenstatic");
            } else {
                $form->setType("courrier_genere", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("proces_verbal", "hiddenstatic");
            $form->setType("numero", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("modele_edition", "selectstatic");
            $form->setType("date_redaction", "hiddenstatic");
            $form->setType("signataire", "selectstatic");
            $form->setType("genere", "hiddenstatic");
            $form->setType("om_fichier_signe", "hiddenstatic");
            $form->setType("courrier_genere", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("proces_verbal", "static");
            $form->setType("numero", "static");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("modele_edition", "selectstatic");
            $form->setType("date_redaction", "datestatic");
            $form->setType("signataire", "selectstatic");
            $form->setType("genere", "checkboxstatic");
            $form->setType("om_fichier_signe", "static");
            $form->setType("courrier_genere", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('proces_verbal','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('dossier_instruction_reunion','VerifNum(this)');
        $form->setOnchange('modele_edition','VerifNum(this)');
        $form->setOnchange('date_redaction','fdate(this)');
        $form->setOnchange('signataire','VerifNum(this)');
        $form->setOnchange('courrier_genere','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("proces_verbal", 11);
        $form->setTaille("numero", 30);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("dossier_instruction_reunion", 11);
        $form->setTaille("modele_edition", 11);
        $form->setTaille("date_redaction", 12);
        $form->setTaille("signataire", 11);
        $form->setTaille("genere", 1);
        $form->setTaille("om_fichier_signe", 30);
        $form->setTaille("courrier_genere", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("proces_verbal", 11);
        $form->setMax("numero", 50);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("dossier_instruction_reunion", 11);
        $form->setMax("modele_edition", 11);
        $form->setMax("date_redaction", 12);
        $form->setMax("signataire", 11);
        $form->setMax("genere", 1);
        $form->setMax("om_fichier_signe", 64);
        $form->setMax("courrier_genere", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('proces_verbal',_('proces_verbal'));
        $form->setLib('numero',_('numero'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('dossier_instruction_reunion',_('dossier_instruction_reunion'));
        $form->setLib('modele_edition',_('modele_edition'));
        $form->setLib('date_redaction',_('date_redaction'));
        $form->setLib('signataire',_('signataire'));
        $form->setLib('genere',_('genere'));
        $form->setLib('om_fichier_signe',_('om_fichier_signe'));
        $form->setLib('courrier_genere',_('courrier_genere'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // courrier_genere
        $this->init_select($form, $this->f->db, $maj, null, "courrier_genere", $sql_courrier_genere, $sql_courrier_genere_by_id, false);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // dossier_instruction_reunion
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction_reunion", $sql_dossier_instruction_reunion, $sql_dossier_instruction_reunion_by_id, false);
        // modele_edition
        $this->init_select($form, $this->f->db, $maj, null, "modele_edition", $sql_modele_edition, $sql_modele_edition_by_id, true);
        // signataire
        $this->init_select($form, $this->f->db, $maj, null, "signataire", $sql_signataire, $sql_signataire_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('courrier', $this->retourformulaire))
                $form->setVal('courrier_genere', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_edition', $idxformulaire);
            if($this->is_in_context_of_foreign_key('signataire', $this->retourformulaire))
                $form->setVal('signataire', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "dernier_pv", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "proces_verbal", $id);
    }


}

?>
