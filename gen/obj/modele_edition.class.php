<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class modele_edition_gen extends om_dbform {

    var $table = "modele_edition";
    var $clePrimaire = "modele_edition";
    var $typeCle = "N";
    var $required_field = array(
        "courrier_type",
        "libelle",
        "modele_edition"
    );
    var $unique_key = array(
      "code",
    );
    var $foreign_keys_extended = array(
        "courrier_type" => array("courrier_type", ),
        "om_etat" => array("om_etat", ),
        "om_lettretype" => array("om_lettretype", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['modele_edition'])) {
            $this->valF['modele_edition'] = ""; // -> requis
        } else {
            $this->valF['modele_edition'] = $val['modele_edition'];
        }
        $this->valF['libelle'] = $val['libelle'];
            $this->valF['description'] = $val['description'];
        if (!is_numeric($val['om_lettretype'])) {
            $this->valF['om_lettretype'] = NULL;
        } else {
            $this->valF['om_lettretype'] = $val['om_lettretype'];
        }
        if (!is_numeric($val['om_etat'])) {
            $this->valF['om_etat'] = NULL;
        } else {
            $this->valF['om_etat'] = $val['om_etat'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if (!is_numeric($val['courrier_type'])) {
            $this->valF['courrier_type'] = ""; // -> requis
        } else {
            $this->valF['courrier_type'] = $val['courrier_type'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("modele_edition", "hidden");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("om_lettretype", $this->retourformulaire)) {
                $form->setType("om_lettretype", "selecthiddenstatic");
            } else {
                $form->setType("om_lettretype", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_etat", $this->retourformulaire)) {
                $form->setType("om_etat", "selecthiddenstatic");
            } else {
                $form->setType("om_etat", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("modele_edition", "hiddenstatic");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("om_lettretype", $this->retourformulaire)) {
                $form->setType("om_lettretype", "selecthiddenstatic");
            } else {
                $form->setType("om_lettretype", "select");
            }
            if ($this->is_in_context_of_foreign_key("om_etat", $this->retourformulaire)) {
                $form->setType("om_etat", "selecthiddenstatic");
            } else {
                $form->setType("om_etat", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("courrier_type", $this->retourformulaire)) {
                $form->setType("courrier_type", "selecthiddenstatic");
            } else {
                $form->setType("courrier_type", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("modele_edition", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("om_lettretype", "selectstatic");
            $form->setType("om_etat", "selectstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("courrier_type", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("modele_edition", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("om_lettretype", "selectstatic");
            $form->setType("om_etat", "selectstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("code", "static");
            $form->setType("courrier_type", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('modele_edition','VerifNum(this)');
        $form->setOnchange('om_lettretype','VerifNum(this)');
        $form->setOnchange('om_etat','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
        $form->setOnchange('courrier_type','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("modele_edition", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("om_lettretype", 11);
        $form->setTaille("om_etat", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("code", 15);
        $form->setTaille("courrier_type", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("modele_edition", 11);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("om_lettretype", 11);
        $form->setMax("om_etat", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("code", 15);
        $form->setMax("courrier_type", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('modele_edition',_('modele_edition'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description',_('description'));
        $form->setLib('om_lettretype',_('om_lettretype'));
        $form->setLib('om_etat',_('om_etat'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
        $form->setLib('code',_('code'));
        $form->setLib('courrier_type',_('courrier_type'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // courrier_type
        $this->init_select($form, $this->f->db, $maj, null, "courrier_type", $sql_courrier_type, $sql_courrier_type_by_id, true);
        // om_etat
        $this->init_select($form, $this->f->db, $maj, null, "om_etat", $sql_om_etat, $sql_om_etat_by_id, false);
        // om_lettretype
        $this->init_select($form, $this->f->db, $maj, null, "om_lettretype", $sql_om_lettretype, $sql_om_lettretype_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('courrier_type', $this->retourformulaire))
                $form->setVal('courrier_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_etat', $this->retourformulaire))
                $form->setVal('om_etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('om_lettretype', $this->retourformulaire))
                $form->setVal('om_lettretype', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_compte_rendu", $id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_proces_verbal", $id);
        // Verification de la cle secondaire : analyses
        $this->rechercheTable($this->f->db, "analyses", "modele_edition_rapport", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_compte_rendu", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_proces_verbal", $id);
        // Verification de la cle secondaire : analyses_type
        $this->rechercheTable($this->f->db, "analyses_type", "modele_edition_rapport", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "modele_edition", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "modele_edition", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_compte_rendu_global", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_compte_rendu_specifique", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_feuille_presence", $id);
        // Verification de la cle secondaire : reunion_type
        $this->rechercheTable($this->f->db, "reunion_type", "modele_ordre_du_jour", $id);
    }


}

?>
