<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class autorite_police_decision_gen extends om_dbform {

    var $table = "autorite_police_decision";
    var $clePrimaire = "autorite_police_decision";
    var $typeCle = "N";
    var $required_field = array(
        "autorite_police_decision"
    );
    
    var $foreign_keys_extended = array(
        "etablissement_etat" => array("etablissement_etat", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['autorite_police_decision'])) {
            $this->valF['autorite_police_decision'] = ""; // -> requis
        } else {
            $this->valF['autorite_police_decision'] = $val['autorite_police_decision'];
        }
        if ($val['code'] == "") {
            $this->valF['code'] = NULL;
        } else {
            $this->valF['code'] = $val['code'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description'] = $val['description'];
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['suivi_delai'] == 1 || $val['suivi_delai'] == "t" || $val['suivi_delai'] == "Oui") {
            $this->valF['suivi_delai'] = true;
        } else {
            $this->valF['suivi_delai'] = false;
        }
        if ($val['avis'] == "") {
            $this->valF['avis'] = NULL;
        } else {
            $this->valF['avis'] = $val['avis'];
        }
        if (!is_numeric($val['etablissement_etat'])) {
            $this->valF['etablissement_etat'] = NULL;
        } else {
            $this->valF['etablissement_etat'] = $val['etablissement_etat'];
        }
        if (!is_numeric($val['delai'])) {
            $this->valF['delai'] = NULL;
        } else {
            $this->valF['delai'] = $val['delai'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if ($val['type_arrete'] == 1 || $val['type_arrete'] == "t" || $val['type_arrete'] == "Oui") {
            $this->valF['type_arrete'] = true;
        } else {
            $this->valF['type_arrete'] = false;
        }
        if ($val['arrete_reglementaire'] == 1 || $val['arrete_reglementaire'] == "t" || $val['arrete_reglementaire'] == "Oui") {
            $this->valF['arrete_reglementaire'] = true;
        } else {
            $this->valF['arrete_reglementaire'] = false;
        }
        if ($val['arrete_notification'] == 1 || $val['arrete_notification'] == "t" || $val['arrete_notification'] == "Oui") {
            $this->valF['arrete_notification'] = true;
        } else {
            $this->valF['arrete_notification'] = false;
        }
        if ($val['arrete_publication'] == 1 || $val['arrete_publication'] == "t" || $val['arrete_publication'] == "Oui") {
            $this->valF['arrete_publication'] = true;
        } else {
            $this->valF['arrete_publication'] = false;
        }
        if ($val['arrete_temporaire'] == 1 || $val['arrete_temporaire'] == "t" || $val['arrete_temporaire'] == "Oui") {
            $this->valF['arrete_temporaire'] = true;
        } else {
            $this->valF['arrete_temporaire'] = false;
        }
        if ($val['nomenclature_actes_nature'] == "") {
            $this->valF['nomenclature_actes_nature'] = NULL;
        } else {
            $this->valF['nomenclature_actes_nature'] = $val['nomenclature_actes_nature'];
        }
        if ($val['nomenclature_actes_matiere_niv1'] == "") {
            $this->valF['nomenclature_actes_matiere_niv1'] = NULL;
        } else {
            $this->valF['nomenclature_actes_matiere_niv1'] = $val['nomenclature_actes_matiere_niv1'];
        }
        if ($val['nomenclature_actes_matiere_niv2'] == "") {
            $this->valF['nomenclature_actes_matiere_niv2'] = NULL;
        } else {
            $this->valF['nomenclature_actes_matiere_niv2'] = $val['nomenclature_actes_matiere_niv2'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("autorite_police_decision", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("suivi_delai", "checkbox");
            $form->setType("avis", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_etat", $this->retourformulaire)) {
                $form->setType("etablissement_etat", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_etat", "select");
            }
            $form->setType("delai", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("type_arrete", "checkbox");
            $form->setType("arrete_reglementaire", "checkbox");
            $form->setType("arrete_notification", "checkbox");
            $form->setType("arrete_publication", "checkbox");
            $form->setType("arrete_temporaire", "checkbox");
            $form->setType("nomenclature_actes_nature", "text");
            $form->setType("nomenclature_actes_matiere_niv1", "text");
            $form->setType("nomenclature_actes_matiere_niv2", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("autorite_police_decision", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("description", "textarea");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("suivi_delai", "checkbox");
            $form->setType("avis", "text");
            if ($this->is_in_context_of_foreign_key("etablissement_etat", $this->retourformulaire)) {
                $form->setType("etablissement_etat", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_etat", "select");
            }
            $form->setType("delai", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("type_arrete", "checkbox");
            $form->setType("arrete_reglementaire", "checkbox");
            $form->setType("arrete_notification", "checkbox");
            $form->setType("arrete_publication", "checkbox");
            $form->setType("arrete_temporaire", "checkbox");
            $form->setType("nomenclature_actes_nature", "text");
            $form->setType("nomenclature_actes_matiere_niv1", "text");
            $form->setType("nomenclature_actes_matiere_niv2", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("autorite_police_decision", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("suivi_delai", "hiddenstatic");
            $form->setType("avis", "hiddenstatic");
            $form->setType("etablissement_etat", "selectstatic");
            $form->setType("delai", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("type_arrete", "hiddenstatic");
            $form->setType("arrete_reglementaire", "hiddenstatic");
            $form->setType("arrete_notification", "hiddenstatic");
            $form->setType("arrete_publication", "hiddenstatic");
            $form->setType("arrete_temporaire", "hiddenstatic");
            $form->setType("nomenclature_actes_nature", "hiddenstatic");
            $form->setType("nomenclature_actes_matiere_niv1", "hiddenstatic");
            $form->setType("nomenclature_actes_matiere_niv2", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("autorite_police_decision", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("description", "textareastatic");
            $form->setType("service", "selectstatic");
            $form->setType("suivi_delai", "checkboxstatic");
            $form->setType("avis", "static");
            $form->setType("etablissement_etat", "selectstatic");
            $form->setType("delai", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("type_arrete", "checkboxstatic");
            $form->setType("arrete_reglementaire", "checkboxstatic");
            $form->setType("arrete_notification", "checkboxstatic");
            $form->setType("arrete_publication", "checkboxstatic");
            $form->setType("arrete_temporaire", "checkboxstatic");
            $form->setType("nomenclature_actes_nature", "static");
            $form->setType("nomenclature_actes_matiere_niv1", "static");
            $form->setType("nomenclature_actes_matiere_niv2", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('autorite_police_decision','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('etablissement_etat','VerifNum(this)');
        $form->setOnchange('delai','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("autorite_police_decision", 11);
        $form->setTaille("code", 20);
        $form->setTaille("libelle", 30);
        $form->setTaille("description", 80);
        $form->setTaille("service", 11);
        $form->setTaille("suivi_delai", 1);
        $form->setTaille("avis", 30);
        $form->setTaille("etablissement_etat", 11);
        $form->setTaille("delai", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("type_arrete", 1);
        $form->setTaille("arrete_reglementaire", 1);
        $form->setTaille("arrete_notification", 1);
        $form->setTaille("arrete_publication", 1);
        $form->setTaille("arrete_temporaire", 1);
        $form->setTaille("nomenclature_actes_nature", 30);
        $form->setTaille("nomenclature_actes_matiere_niv1", 30);
        $form->setTaille("nomenclature_actes_matiere_niv2", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("autorite_police_decision", 11);
        $form->setMax("code", 20);
        $form->setMax("libelle", 100);
        $form->setMax("description", 6);
        $form->setMax("service", 11);
        $form->setMax("suivi_delai", 1);
        $form->setMax("avis", 100);
        $form->setMax("etablissement_etat", 11);
        $form->setMax("delai", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("type_arrete", 1);
        $form->setMax("arrete_reglementaire", 1);
        $form->setMax("arrete_notification", 1);
        $form->setMax("arrete_publication", 1);
        $form->setMax("arrete_temporaire", 1);
        $form->setMax("nomenclature_actes_nature", 30);
        $form->setMax("nomenclature_actes_matiere_niv1", 250);
        $form->setMax("nomenclature_actes_matiere_niv2", 250);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('autorite_police_decision',_('autorite_police_decision'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description',_('description'));
        $form->setLib('service',_('service'));
        $form->setLib('suivi_delai',_('suivi_delai'));
        $form->setLib('avis',_('avis'));
        $form->setLib('etablissement_etat',_('etablissement_etat'));
        $form->setLib('delai',_('delai'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
        $form->setLib('type_arrete',_('type_arrete'));
        $form->setLib('arrete_reglementaire',_('arrete_reglementaire'));
        $form->setLib('arrete_notification',_('arrete_notification'));
        $form->setLib('arrete_publication',_('arrete_publication'));
        $form->setLib('arrete_temporaire',_('arrete_temporaire'));
        $form->setLib('nomenclature_actes_nature',_('nomenclature_actes_nature'));
        $form->setLib('nomenclature_actes_matiere_niv1',_('nomenclature_actes_matiere_niv1'));
        $form->setLib('nomenclature_actes_matiere_niv2',_('nomenclature_actes_matiere_niv2'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // etablissement_etat
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_etat", $sql_etablissement_etat, $sql_etablissement_etat_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement_etat', $this->retourformulaire))
                $form->setVal('etablissement_etat', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "autorite_police_decision", $id);
    }


}

?>
