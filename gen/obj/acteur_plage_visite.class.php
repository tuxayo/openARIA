<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class acteur_plage_visite_gen extends om_dbform {

    var $table = "acteur_plage_visite";
    var $clePrimaire = "acteur_plage_visite";
    var $typeCle = "N";
    var $required_field = array(
        "acteur",
        "acteur_plage_visite"
    );
    
    var $foreign_keys_extended = array(
        "acteur" => array("acteur", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['acteur_plage_visite'])) {
            $this->valF['acteur_plage_visite'] = ""; // -> requis
        } else {
            $this->valF['acteur_plage_visite'] = $val['acteur_plage_visite'];
        }
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
        if ($val['lundi_matin'] == 1 || $val['lundi_matin'] == "t" || $val['lundi_matin'] == "Oui") {
            $this->valF['lundi_matin'] = true;
        } else {
            $this->valF['lundi_matin'] = false;
        }
        if ($val['lundi_apresmidi'] == 1 || $val['lundi_apresmidi'] == "t" || $val['lundi_apresmidi'] == "Oui") {
            $this->valF['lundi_apresmidi'] = true;
        } else {
            $this->valF['lundi_apresmidi'] = false;
        }
        if ($val['mardi_matin'] == 1 || $val['mardi_matin'] == "t" || $val['mardi_matin'] == "Oui") {
            $this->valF['mardi_matin'] = true;
        } else {
            $this->valF['mardi_matin'] = false;
        }
        if ($val['mardi_apresmidi'] == 1 || $val['mardi_apresmidi'] == "t" || $val['mardi_apresmidi'] == "Oui") {
            $this->valF['mardi_apresmidi'] = true;
        } else {
            $this->valF['mardi_apresmidi'] = false;
        }
        if ($val['mercredi_matin'] == 1 || $val['mercredi_matin'] == "t" || $val['mercredi_matin'] == "Oui") {
            $this->valF['mercredi_matin'] = true;
        } else {
            $this->valF['mercredi_matin'] = false;
        }
        if ($val['mercredi_apresmidi'] == 1 || $val['mercredi_apresmidi'] == "t" || $val['mercredi_apresmidi'] == "Oui") {
            $this->valF['mercredi_apresmidi'] = true;
        } else {
            $this->valF['mercredi_apresmidi'] = false;
        }
        if ($val['jeudi_matin'] == 1 || $val['jeudi_matin'] == "t" || $val['jeudi_matin'] == "Oui") {
            $this->valF['jeudi_matin'] = true;
        } else {
            $this->valF['jeudi_matin'] = false;
        }
        if ($val['jeudi_apresmidi'] == 1 || $val['jeudi_apresmidi'] == "t" || $val['jeudi_apresmidi'] == "Oui") {
            $this->valF['jeudi_apresmidi'] = true;
        } else {
            $this->valF['jeudi_apresmidi'] = false;
        }
        if ($val['vendredi_matin'] == 1 || $val['vendredi_matin'] == "t" || $val['vendredi_matin'] == "Oui") {
            $this->valF['vendredi_matin'] = true;
        } else {
            $this->valF['vendredi_matin'] = false;
        }
        if ($val['vendredi_apresmidi'] == 1 || $val['vendredi_apresmidi'] == "t" || $val['vendredi_apresmidi'] == "Oui") {
            $this->valF['vendredi_apresmidi'] = true;
        } else {
            $this->valF['vendredi_apresmidi'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("acteur_plage_visite", "hidden");
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            $form->setType("lundi_matin", "checkbox");
            $form->setType("lundi_apresmidi", "checkbox");
            $form->setType("mardi_matin", "checkbox");
            $form->setType("mardi_apresmidi", "checkbox");
            $form->setType("mercredi_matin", "checkbox");
            $form->setType("mercredi_apresmidi", "checkbox");
            $form->setType("jeudi_matin", "checkbox");
            $form->setType("jeudi_apresmidi", "checkbox");
            $form->setType("vendredi_matin", "checkbox");
            $form->setType("vendredi_apresmidi", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("acteur_plage_visite", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("acteur", $this->retourformulaire)) {
                $form->setType("acteur", "selecthiddenstatic");
            } else {
                $form->setType("acteur", "select");
            }
            $form->setType("lundi_matin", "checkbox");
            $form->setType("lundi_apresmidi", "checkbox");
            $form->setType("mardi_matin", "checkbox");
            $form->setType("mardi_apresmidi", "checkbox");
            $form->setType("mercredi_matin", "checkbox");
            $form->setType("mercredi_apresmidi", "checkbox");
            $form->setType("jeudi_matin", "checkbox");
            $form->setType("jeudi_apresmidi", "checkbox");
            $form->setType("vendredi_matin", "checkbox");
            $form->setType("vendredi_apresmidi", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("acteur_plage_visite", "hiddenstatic");
            $form->setType("acteur", "selectstatic");
            $form->setType("lundi_matin", "hiddenstatic");
            $form->setType("lundi_apresmidi", "hiddenstatic");
            $form->setType("mardi_matin", "hiddenstatic");
            $form->setType("mardi_apresmidi", "hiddenstatic");
            $form->setType("mercredi_matin", "hiddenstatic");
            $form->setType("mercredi_apresmidi", "hiddenstatic");
            $form->setType("jeudi_matin", "hiddenstatic");
            $form->setType("jeudi_apresmidi", "hiddenstatic");
            $form->setType("vendredi_matin", "hiddenstatic");
            $form->setType("vendredi_apresmidi", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("acteur_plage_visite", "static");
            $form->setType("acteur", "selectstatic");
            $form->setType("lundi_matin", "checkboxstatic");
            $form->setType("lundi_apresmidi", "checkboxstatic");
            $form->setType("mardi_matin", "checkboxstatic");
            $form->setType("mardi_apresmidi", "checkboxstatic");
            $form->setType("mercredi_matin", "checkboxstatic");
            $form->setType("mercredi_apresmidi", "checkboxstatic");
            $form->setType("jeudi_matin", "checkboxstatic");
            $form->setType("jeudi_apresmidi", "checkboxstatic");
            $form->setType("vendredi_matin", "checkboxstatic");
            $form->setType("vendredi_apresmidi", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('acteur_plage_visite','VerifNum(this)');
        $form->setOnchange('acteur','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("acteur_plage_visite", 11);
        $form->setTaille("acteur", 11);
        $form->setTaille("lundi_matin", 1);
        $form->setTaille("lundi_apresmidi", 1);
        $form->setTaille("mardi_matin", 1);
        $form->setTaille("mardi_apresmidi", 1);
        $form->setTaille("mercredi_matin", 1);
        $form->setTaille("mercredi_apresmidi", 1);
        $form->setTaille("jeudi_matin", 1);
        $form->setTaille("jeudi_apresmidi", 1);
        $form->setTaille("vendredi_matin", 1);
        $form->setTaille("vendredi_apresmidi", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("acteur_plage_visite", 11);
        $form->setMax("acteur", 11);
        $form->setMax("lundi_matin", 1);
        $form->setMax("lundi_apresmidi", 1);
        $form->setMax("mardi_matin", 1);
        $form->setMax("mardi_apresmidi", 1);
        $form->setMax("mercredi_matin", 1);
        $form->setMax("mercredi_apresmidi", 1);
        $form->setMax("jeudi_matin", 1);
        $form->setMax("jeudi_apresmidi", 1);
        $form->setMax("vendredi_matin", 1);
        $form->setMax("vendredi_apresmidi", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('acteur_plage_visite',_('acteur_plage_visite'));
        $form->setLib('acteur',_('acteur'));
        $form->setLib('lundi_matin',_('lundi_matin'));
        $form->setLib('lundi_apresmidi',_('lundi_apresmidi'));
        $form->setLib('mardi_matin',_('mardi_matin'));
        $form->setLib('mardi_apresmidi',_('mardi_apresmidi'));
        $form->setLib('mercredi_matin',_('mercredi_matin'));
        $form->setLib('mercredi_apresmidi',_('mercredi_apresmidi'));
        $form->setLib('jeudi_matin',_('jeudi_matin'));
        $form->setLib('jeudi_apresmidi',_('jeudi_apresmidi'));
        $form->setLib('vendredi_matin',_('vendredi_matin'));
        $form->setLib('vendredi_apresmidi',_('vendredi_apresmidi'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // acteur
        $this->init_select($form, $this->f->db, $maj, null, "acteur", $sql_acteur, $sql_acteur_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('acteur', $this->retourformulaire))
                $form->setVal('acteur', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
