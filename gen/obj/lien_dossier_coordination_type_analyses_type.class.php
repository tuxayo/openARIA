<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_dossier_coordination_type_analyses_type_gen extends om_dbform {

    var $table = "lien_dossier_coordination_type_analyses_type";
    var $clePrimaire = "lien_dossier_coordination_type_analyses_type";
    var $typeCle = "N";
    var $required_field = array(
        "analyses_type",
        "dossier_coordination_type",
        "lien_dossier_coordination_type_analyses_type",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "analyses_type" => array("analyses_type", ),
        "dossier_coordination_type" => array("dossier_coordination_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_dossier_coordination_type_analyses_type'])) {
            $this->valF['lien_dossier_coordination_type_analyses_type'] = ""; // -> requis
        } else {
            $this->valF['lien_dossier_coordination_type_analyses_type'] = $val['lien_dossier_coordination_type_analyses_type'];
        }
        if (!is_numeric($val['dossier_coordination_type'])) {
            $this->valF['dossier_coordination_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_type'] = $val['dossier_coordination_type'];
        }
        if (!is_numeric($val['analyses_type'])) {
            $this->valF['analyses_type'] = ""; // -> requis
        } else {
            $this->valF['analyses_type'] = $val['analyses_type'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_dossier_coordination_type_analyses_type", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type", "select");
            }
            $form->setType("service", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_dossier_coordination_type_analyses_type", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("analyses_type", $this->retourformulaire)) {
                $form->setType("analyses_type", "selecthiddenstatic");
            } else {
                $form->setType("analyses_type", "select");
            }
            $form->setType("service", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_dossier_coordination_type_analyses_type", "hiddenstatic");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("analyses_type", "selectstatic");
            $form->setType("service", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_dossier_coordination_type_analyses_type", "static");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("analyses_type", "selectstatic");
            $form->setType("service", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_dossier_coordination_type_analyses_type','VerifNum(this)');
        $form->setOnchange('dossier_coordination_type','VerifNum(this)');
        $form->setOnchange('analyses_type','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_dossier_coordination_type_analyses_type", 11);
        $form->setTaille("dossier_coordination_type", 11);
        $form->setTaille("analyses_type", 11);
        $form->setTaille("service", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_dossier_coordination_type_analyses_type", 11);
        $form->setMax("dossier_coordination_type", 11);
        $form->setMax("analyses_type", 11);
        $form->setMax("service", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_dossier_coordination_type_analyses_type',_('lien_dossier_coordination_type_analyses_type'));
        $form->setLib('dossier_coordination_type',_('dossier_coordination_type'));
        $form->setLib('analyses_type',_('analyses_type'));
        $form->setLib('service',_('service'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // analyses_type
        $this->init_select($form, $this->f->db, $maj, null, "analyses_type", $sql_analyses_type, $sql_analyses_type_by_id, true);
        // dossier_coordination_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination_type", $sql_dossier_coordination_type, $sql_dossier_coordination_type_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('analyses_type', $this->retourformulaire))
                $form->setVal('analyses_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination_type', $this->retourformulaire))
                $form->setVal('dossier_coordination_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
