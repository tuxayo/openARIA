<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class acteur_gen extends om_dbform {

    var $table = "acteur";
    var $clePrimaire = "acteur";
    var $typeCle = "N";
    var $required_field = array(
        "acteur"
    );
    
    var $foreign_keys_extended = array(
        "om_utilisateur" => array("om_utilisateur", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['acteur'])) {
            $this->valF['acteur'] = ""; // -> requis
        } else {
            $this->valF['acteur'] = $val['acteur'];
        }
        if ($val['nom_prenom'] == "") {
            $this->valF['nom_prenom'] = NULL;
        } else {
            $this->valF['nom_prenom'] = $val['nom_prenom'];
        }
        if (!is_numeric($val['om_utilisateur'])) {
            $this->valF['om_utilisateur'] = NULL;
        } else {
            $this->valF['om_utilisateur'] = $val['om_utilisateur'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = NULL;
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['role'] == "") {
            $this->valF['role'] = NULL;
        } else {
            $this->valF['role'] = $val['role'];
        }
        if ($val['acronyme'] == "") {
            $this->valF['acronyme'] = NULL;
        } else {
            $this->valF['acronyme'] = $val['acronyme'];
        }
        if ($val['couleur'] == "") {
            $this->valF['couleur'] = NULL;
        } else {
            $this->valF['couleur'] = $val['couleur'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
        if ($val['reference'] == "") {
            $this->valF['reference'] = NULL;
        } else {
            $this->valF['reference'] = $val['reference'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("acteur", "hidden");
            $form->setType("nom_prenom", "text");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("role", "text");
            $form->setType("acronyme", "text");
            $form->setType("couleur", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("reference", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("acteur", "hiddenstatic");
            $form->setType("nom_prenom", "text");
            if ($this->is_in_context_of_foreign_key("om_utilisateur", $this->retourformulaire)) {
                $form->setType("om_utilisateur", "selecthiddenstatic");
            } else {
                $form->setType("om_utilisateur", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("role", "text");
            $form->setType("acronyme", "text");
            $form->setType("couleur", "text");
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
            $form->setType("reference", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("acteur", "hiddenstatic");
            $form->setType("nom_prenom", "hiddenstatic");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("role", "hiddenstatic");
            $form->setType("acronyme", "hiddenstatic");
            $form->setType("couleur", "hiddenstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
            $form->setType("reference", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("acteur", "static");
            $form->setType("nom_prenom", "static");
            $form->setType("om_utilisateur", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("role", "static");
            $form->setType("acronyme", "static");
            $form->setType("couleur", "static");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
            $form->setType("reference", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('acteur','VerifNum(this)');
        $form->setOnchange('om_utilisateur','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("acteur", 11);
        $form->setTaille("nom_prenom", 30);
        $form->setTaille("om_utilisateur", 11);
        $form->setTaille("service", 11);
        $form->setTaille("role", 30);
        $form->setTaille("acronyme", 10);
        $form->setTaille("couleur", 10);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
        $form->setTaille("reference", 30);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("acteur", 11);
        $form->setMax("nom_prenom", 80);
        $form->setMax("om_utilisateur", 11);
        $form->setMax("service", 11);
        $form->setMax("role", 100);
        $form->setMax("acronyme", 10);
        $form->setMax("couleur", 6);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
        $form->setMax("reference", 255);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('acteur',_('acteur'));
        $form->setLib('nom_prenom',_('nom_prenom'));
        $form->setLib('om_utilisateur',_('om_utilisateur'));
        $form->setLib('service',_('service'));
        $form->setLib('role',_('role'));
        $form->setLib('acronyme',_('acronyme'));
        $form->setLib('couleur',_('couleur'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
        $form->setLib('reference',_('reference'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // om_utilisateur
        $this->init_select($form, $this->f->db, $maj, null, "om_utilisateur", $sql_om_utilisateur, $sql_om_utilisateur_by_id, false);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('om_utilisateur', $this->retourformulaire))
                $form->setVal('om_utilisateur', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : acteur_conge
        $this->rechercheTable($this->f->db, "acteur_conge", "acteur", $id);
        // Verification de la cle secondaire : acteur_plage_visite
        $this->rechercheTable($this->f->db, "acteur_plage_visite", "acteur", $id);
        // Verification de la cle secondaire : dossier_instruction
        $this->rechercheTable($this->f->db, "dossier_instruction", "technicien", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "acc_derniere_visite_technicien", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "si_derniere_visite_technicien", $id);
        // Verification de la cle secondaire : technicien_arrondissement
        $this->rechercheTable($this->f->db, "technicien_arrondissement", "technicien", $id);
        // Verification de la cle secondaire : visite
        $this->rechercheTable($this->f->db, "visite", "acteur", $id);
    }


}

?>
