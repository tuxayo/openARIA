<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class dossier_coordination_gen extends om_dbform {

    var $table = "dossier_coordination";
    var $clePrimaire = "dossier_coordination";
    var $typeCle = "N";
    var $required_field = array(
        "date_demande",
        "dossier_coordination",
        "dossier_coordination_type"
    );
    var $unique_key = array(
      "libelle",
    );
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
        "dossier_coordination_type" => array("dossier_coordination_type", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "etablissement_categorie" => array("etablissement_categorie", ),
        "etablissement_type" => array("etablissement_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
        if (!is_numeric($val['dossier_coordination_type'])) {
            $this->valF['dossier_coordination_type'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_type'] = $val['dossier_coordination_type'];
        }
        if ($val['date_demande'] != "") {
            $this->valF['date_demande'] = $this->dateDB($val['date_demande']);
        }
        if ($val['date_butoir'] != "") {
            $this->valF['date_butoir'] = $this->dateDB($val['date_butoir']);
        } else {
            $this->valF['date_butoir'] = NULL;
        }
        if ($val['dossier_autorisation_ads'] == "") {
            $this->valF['dossier_autorisation_ads'] = NULL;
        } else {
            $this->valF['dossier_autorisation_ads'] = $val['dossier_autorisation_ads'];
        }
        if ($val['dossier_instruction_ads'] == "") {
            $this->valF['dossier_instruction_ads'] = NULL;
        } else {
            $this->valF['dossier_instruction_ads'] = $val['dossier_instruction_ads'];
        }
        if ($val['a_qualifier'] == 1 || $val['a_qualifier'] == "t" || $val['a_qualifier'] == "Oui") {
            $this->valF['a_qualifier'] = true;
        } else {
            $this->valF['a_qualifier'] = false;
        }
        if (!is_numeric($val['etablissement_type'])) {
            $this->valF['etablissement_type'] = NULL;
        } else {
            $this->valF['etablissement_type'] = $val['etablissement_type'];
        }
        if (!is_numeric($val['etablissement_categorie'])) {
            $this->valF['etablissement_categorie'] = NULL;
        } else {
            $this->valF['etablissement_categorie'] = $val['etablissement_categorie'];
        }
        if ($val['erp'] == 1 || $val['erp'] == "t" || $val['erp'] == "Oui") {
            $this->valF['erp'] = true;
        } else {
            $this->valF['erp'] = false;
        }
        if ($val['dossier_cloture'] == 1 || $val['dossier_cloture'] == "t" || $val['dossier_cloture'] == "Oui") {
            $this->valF['dossier_cloture'] = true;
        } else {
            $this->valF['dossier_cloture'] = false;
        }
            $this->valF['contraintes_urba_om_html'] = $val['contraintes_urba_om_html'];
        if ($val['etablissement_locaux_sommeil'] == 1 || $val['etablissement_locaux_sommeil'] == "t" || $val['etablissement_locaux_sommeil'] == "Oui") {
            $this->valF['etablissement_locaux_sommeil'] = true;
        } else {
            $this->valF['etablissement_locaux_sommeil'] = false;
        }
            $this->valF['references_cadastrales'] = $val['references_cadastrales'];
        if (!is_numeric($val['dossier_coordination_parent'])) {
            $this->valF['dossier_coordination_parent'] = NULL;
        } else {
            $this->valF['dossier_coordination_parent'] = $val['dossier_coordination_parent'];
        }
            $this->valF['description'] = $val['description'];
        if ($val['dossier_instruction_secu'] == 1 || $val['dossier_instruction_secu'] == "t" || $val['dossier_instruction_secu'] == "Oui") {
            $this->valF['dossier_instruction_secu'] = true;
        } else {
            $this->valF['dossier_instruction_secu'] = false;
        }
        if ($val['dossier_instruction_acc'] == 1 || $val['dossier_instruction_acc'] == "t" || $val['dossier_instruction_acc'] == "Oui") {
            $this->valF['dossier_instruction_acc'] = true;
        } else {
            $this->valF['dossier_instruction_acc'] = false;
        }
        if ($val['autorite_police_encours'] == 1 || $val['autorite_police_encours'] == "t" || $val['autorite_police_encours'] == "Oui") {
            $this->valF['autorite_police_encours'] = true;
        } else {
            $this->valF['autorite_police_encours'] = false;
        }
        if ($val['date_cloture'] != "") {
            $this->valF['date_cloture'] = $this->dateDB($val['date_cloture']);
        } else {
            $this->valF['date_cloture'] = NULL;
        }
        if ($val['geolocalise'] == 1 || $val['geolocalise'] == "t" || $val['geolocalise'] == "Oui") {
            $this->valF['geolocalise'] = true;
        } else {
            $this->valF['geolocalise'] = false;
        }
        if ($val['interface_referentiel_ads'] == 1 || $val['interface_referentiel_ads'] == "t" || $val['interface_referentiel_ads'] == "Oui") {
            $this->valF['interface_referentiel_ads'] = true;
        } else {
            $this->valF['interface_referentiel_ads'] = false;
        }
        if ($val['enjeu_erp'] == 1 || $val['enjeu_erp'] == "t" || $val['enjeu_erp'] == "Oui") {
            $this->valF['enjeu_erp'] = true;
        } else {
            $this->valF['enjeu_erp'] = false;
        }
        if ($val['depot_de_piece'] == 1 || $val['depot_de_piece'] == "t" || $val['depot_de_piece'] == "Oui") {
            $this->valF['depot_de_piece'] = true;
        } else {
            $this->valF['depot_de_piece'] = false;
        }
        if ($val['enjeu_ads'] == 1 || $val['enjeu_ads'] == "t" || $val['enjeu_ads'] == "Oui") {
            $this->valF['enjeu_ads'] = true;
        } else {
            $this->valF['enjeu_ads'] = false;
        }
        if ($val['geom_point'] == "") {
            unset($this->valF['geom_point']);
        } else {
            $this->valF['geom_point'] = $val['geom_point'];
        }
        if ($val['geom_emprise'] == "") {
            unset($this->valF['geom_emprise']);
        } else {
            $this->valF['geom_emprise'] = $val['geom_emprise'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_butoir", "date");
            $form->setType("dossier_autorisation_ads", "text");
            $form->setType("dossier_instruction_ads", "text");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("erp", "checkbox");
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("contraintes_urba_om_html", "html");
            $form->setType("etablissement_locaux_sommeil", "checkbox");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_parent", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_parent", "select");
            }
            $form->setType("description", "textarea");
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("autorite_police_encours", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("geolocalise", "checkbox");
            $form->setType("interface_referentiel_ads", "checkbox");
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("depot_de_piece", "checkbox");
            $form->setType("enjeu_ads", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("libelle", "text");
            if ($this->is_in_context_of_foreign_key("dossier_coordination_type", $this->retourformulaire)) {
                $form->setType("dossier_coordination_type", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_type", "select");
            }
            $form->setType("date_demande", "date");
            $form->setType("date_butoir", "date");
            $form->setType("dossier_autorisation_ads", "text");
            $form->setType("dossier_instruction_ads", "text");
            $form->setType("a_qualifier", "checkbox");
            if ($this->is_in_context_of_foreign_key("etablissement_type", $this->retourformulaire)) {
                $form->setType("etablissement_type", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement_categorie", $this->retourformulaire)) {
                $form->setType("etablissement_categorie", "selecthiddenstatic");
            } else {
                $form->setType("etablissement_categorie", "select");
            }
            $form->setType("erp", "checkbox");
            $form->setType("dossier_cloture", "checkbox");
            $form->setType("contraintes_urba_om_html", "html");
            $form->setType("etablissement_locaux_sommeil", "checkbox");
            $form->setType("references_cadastrales", "textarea");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination_parent", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination_parent", "select");
            }
            $form->setType("description", "textarea");
            $form->setType("dossier_instruction_secu", "checkbox");
            $form->setType("dossier_instruction_acc", "checkbox");
            $form->setType("autorite_police_encours", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("geolocalise", "checkbox");
            $form->setType("interface_referentiel_ads", "checkbox");
            $form->setType("enjeu_erp", "checkbox");
            $form->setType("depot_de_piece", "checkbox");
            $form->setType("enjeu_ads", "checkbox");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("date_demande", "hiddenstatic");
            $form->setType("date_butoir", "hiddenstatic");
            $form->setType("dossier_autorisation_ads", "hiddenstatic");
            $form->setType("dossier_instruction_ads", "hiddenstatic");
            $form->setType("a_qualifier", "hiddenstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("erp", "hiddenstatic");
            $form->setType("dossier_cloture", "hiddenstatic");
            $form->setType("contraintes_urba_om_html", "hiddenstatic");
            $form->setType("etablissement_locaux_sommeil", "hiddenstatic");
            $form->setType("references_cadastrales", "hiddenstatic");
            $form->setType("dossier_coordination_parent", "selectstatic");
            $form->setType("description", "hiddenstatic");
            $form->setType("dossier_instruction_secu", "hiddenstatic");
            $form->setType("dossier_instruction_acc", "hiddenstatic");
            $form->setType("autorite_police_encours", "hiddenstatic");
            $form->setType("date_cloture", "hiddenstatic");
            $form->setType("geolocalise", "hiddenstatic");
            $form->setType("interface_referentiel_ads", "hiddenstatic");
            $form->setType("enjeu_erp", "hiddenstatic");
            $form->setType("depot_de_piece", "hiddenstatic");
            $form->setType("enjeu_ads", "hiddenstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("dossier_coordination_type", "selectstatic");
            $form->setType("date_demande", "datestatic");
            $form->setType("date_butoir", "datestatic");
            $form->setType("dossier_autorisation_ads", "static");
            $form->setType("dossier_instruction_ads", "static");
            $form->setType("a_qualifier", "checkboxstatic");
            $form->setType("etablissement_type", "selectstatic");
            $form->setType("etablissement_categorie", "selectstatic");
            $form->setType("erp", "checkboxstatic");
            $form->setType("dossier_cloture", "checkboxstatic");
            $form->setType("contraintes_urba_om_html", "htmlstatic");
            $form->setType("etablissement_locaux_sommeil", "checkboxstatic");
            $form->setType("references_cadastrales", "textareastatic");
            $form->setType("dossier_coordination_parent", "selectstatic");
            $form->setType("description", "textareastatic");
            $form->setType("dossier_instruction_secu", "checkboxstatic");
            $form->setType("dossier_instruction_acc", "checkboxstatic");
            $form->setType("autorite_police_encours", "checkboxstatic");
            $form->setType("date_cloture", "datestatic");
            $form->setType("geolocalise", "checkboxstatic");
            $form->setType("interface_referentiel_ads", "checkboxstatic");
            $form->setType("enjeu_erp", "checkboxstatic");
            $form->setType("depot_de_piece", "checkboxstatic");
            $form->setType("enjeu_ads", "checkboxstatic");
            $form->setType("geom_point", "geom");
            $form->setType("geom_emprise", "geom");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_coordination_type','VerifNum(this)');
        $form->setOnchange('date_demande','fdate(this)');
        $form->setOnchange('date_butoir','fdate(this)');
        $form->setOnchange('etablissement_type','VerifNum(this)');
        $form->setOnchange('etablissement_categorie','VerifNum(this)');
        $form->setOnchange('dossier_coordination_parent','VerifNum(this)');
        $form->setOnchange('date_cloture','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("dossier_coordination_type", 11);
        $form->setTaille("date_demande", 12);
        $form->setTaille("date_butoir", 12);
        $form->setTaille("dossier_autorisation_ads", 20);
        $form->setTaille("dossier_instruction_ads", 30);
        $form->setTaille("a_qualifier", 1);
        $form->setTaille("etablissement_type", 11);
        $form->setTaille("etablissement_categorie", 11);
        $form->setTaille("erp", 1);
        $form->setTaille("dossier_cloture", 1);
        $form->setTaille("contraintes_urba_om_html", 80);
        $form->setTaille("etablissement_locaux_sommeil", 1);
        $form->setTaille("references_cadastrales", 80);
        $form->setTaille("dossier_coordination_parent", 11);
        $form->setTaille("description", 80);
        $form->setTaille("dossier_instruction_secu", 1);
        $form->setTaille("dossier_instruction_acc", 1);
        $form->setTaille("autorite_police_encours", 1);
        $form->setTaille("date_cloture", 12);
        $form->setTaille("geolocalise", 1);
        $form->setTaille("interface_referentiel_ads", 1);
        $form->setTaille("enjeu_erp", 1);
        $form->setTaille("depot_de_piece", 1);
        $form->setTaille("enjeu_ads", 1);
        $form->setTaille("geom_point", 10);
        $form->setTaille("geom_emprise", 10);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("libelle", 100);
        $form->setMax("dossier_coordination_type", 11);
        $form->setMax("date_demande", 12);
        $form->setMax("date_butoir", 12);
        $form->setMax("dossier_autorisation_ads", 20);
        $form->setMax("dossier_instruction_ads", 30);
        $form->setMax("a_qualifier", 1);
        $form->setMax("etablissement_type", 11);
        $form->setMax("etablissement_categorie", 11);
        $form->setMax("erp", 1);
        $form->setMax("dossier_cloture", 1);
        $form->setMax("contraintes_urba_om_html", 6);
        $form->setMax("etablissement_locaux_sommeil", 1);
        $form->setMax("references_cadastrales", 6);
        $form->setMax("dossier_coordination_parent", 11);
        $form->setMax("description", 6);
        $form->setMax("dossier_instruction_secu", 1);
        $form->setMax("dossier_instruction_acc", 1);
        $form->setMax("autorite_police_encours", 1);
        $form->setMax("date_cloture", 12);
        $form->setMax("geolocalise", 1);
        $form->setMax("interface_referentiel_ads", 1);
        $form->setMax("enjeu_erp", 1);
        $form->setMax("depot_de_piece", 1);
        $form->setMax("enjeu_ads", 1);
        $form->setMax("geom_point", -4);
        $form->setMax("geom_emprise", -4);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination',_('dossier_coordination'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('dossier_coordination_type',_('dossier_coordination_type'));
        $form->setLib('date_demande',_('date_demande'));
        $form->setLib('date_butoir',_('date_butoir'));
        $form->setLib('dossier_autorisation_ads',_('dossier_autorisation_ads'));
        $form->setLib('dossier_instruction_ads',_('dossier_instruction_ads'));
        $form->setLib('a_qualifier',_('a_qualifier'));
        $form->setLib('etablissement_type',_('etablissement_type'));
        $form->setLib('etablissement_categorie',_('etablissement_categorie'));
        $form->setLib('erp',_('erp'));
        $form->setLib('dossier_cloture',_('dossier_cloture'));
        $form->setLib('contraintes_urba_om_html',_('contraintes_urba_om_html'));
        $form->setLib('etablissement_locaux_sommeil',_('etablissement_locaux_sommeil'));
        $form->setLib('references_cadastrales',_('references_cadastrales'));
        $form->setLib('dossier_coordination_parent',_('dossier_coordination_parent'));
        $form->setLib('description',_('description'));
        $form->setLib('dossier_instruction_secu',_('dossier_instruction_secu'));
        $form->setLib('dossier_instruction_acc',_('dossier_instruction_acc'));
        $form->setLib('autorite_police_encours',_('autorite_police_encours'));
        $form->setLib('date_cloture',_('date_cloture'));
        $form->setLib('geolocalise',_('geolocalise'));
        $form->setLib('interface_referentiel_ads',_('interface_referentiel_ads'));
        $form->setLib('enjeu_erp',_('enjeu_erp'));
        $form->setLib('depot_de_piece',_('depot_de_piece'));
        $form->setLib('enjeu_ads',_('enjeu_ads'));
        $form->setLib('geom_point',_('geom_point'));
        $form->setLib('geom_emprise',_('geom_emprise'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_coordination_parent
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination_parent", $sql_dossier_coordination_parent, $sql_dossier_coordination_parent_by_id, false);
        // dossier_coordination_type
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination_type", $sql_dossier_coordination_type, $sql_dossier_coordination_type_by_id, true);
        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
        // etablissement_categorie
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_categorie", $sql_etablissement_categorie, $sql_etablissement_categorie_by_id, true);
        // etablissement_type
        $this->init_select($form, $this->f->db, $maj, null, "etablissement_type", $sql_etablissement_type, $sql_etablissement_type_by_id, true);
        // geom_point
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier_coordination", $this->getParameter("idx"), "0");
            $form->setSelect("geom_point", $contenu);
        }
        // geom_emprise
        if ($maj == 1 || $maj == 3) {
            $contenu = array();
            $contenu[0] = array("dossier_coordination", $this->getParameter("idx"), "1");
            $form->setSelect("geom_emprise", $contenu);
        }
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination_parent', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination_type', $this->retourformulaire))
                $form->setVal('dossier_coordination_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_categorie', $this->retourformulaire))
                $form->setVal('etablissement_categorie', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement_type', $this->retourformulaire))
                $form->setVal('etablissement_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_coordination", $id);
        // Verification de la cle secondaire : courrier
        $this->rechercheTable($this->f->db, "courrier", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_coordination
        $this->rechercheTable($this->f->db, "dossier_coordination", "dossier_coordination_parent", $id);
        // Verification de la cle secondaire : dossier_coordination_message
        $this->rechercheTable($this->f->db, "dossier_coordination_message", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_coordination_parcelle
        $this->rechercheTable($this->f->db, "dossier_coordination_parcelle", "dossier_coordination", $id);
        // Verification de la cle secondaire : dossier_instruction
        $this->rechercheTable($this->f->db, "dossier_instruction", "dossier_coordination", $id);
        // Verification de la cle secondaire : etablissement
        $this->rechercheTable($this->f->db, "etablissement", "dossier_coordination_periodique", $id);
        // Verification de la cle secondaire : lien_contrainte_dossier_coordination
        $this->rechercheTable($this->f->db, "lien_contrainte_dossier_coordination", "dossier_coordination", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_contact
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_contact", "dossier_coordination", $id);
        // Verification de la cle secondaire : lien_dossier_coordination_etablissement_type
        $this->rechercheTable($this->f->db, "lien_dossier_coordination_etablissement_type", "dossier_coordination", $id);
        // Verification de la cle secondaire : piece
        $this->rechercheTable($this->f->db, "piece", "dossier_coordination", $id);
    }


}

?>
