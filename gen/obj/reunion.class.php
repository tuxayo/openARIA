<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class reunion_gen extends om_dbform {

    var $table = "reunion";
    var $clePrimaire = "reunion";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "date_reunion",
        "libelle",
        "numerotation",
        "reunion",
        "reunion_type"
    );
    
    var $foreign_keys_extended = array(
        "reunion_type" => array("reunion_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion'])) {
            $this->valF['reunion'] = ""; // -> requis
        } else {
            $this->valF['reunion'] = $val['reunion'];
        }
        $this->valF['code'] = $val['code'];
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        $this->valF['libelle'] = $val['libelle'];
        if ($val['date_reunion'] != "") {
            $this->valF['date_reunion'] = $this->dateDB($val['date_reunion']);
        }
        if ($val['heure_reunion'] == "") {
            $this->valF['heure_reunion'] = NULL;
        } else {
            $this->valF['heure_reunion'] = $val['heure_reunion'];
        }
        if ($val['lieu_adresse_ligne1'] == "") {
            $this->valF['lieu_adresse_ligne1'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne1'] = $val['lieu_adresse_ligne1'];
        }
        if ($val['lieu_adresse_ligne2'] == "") {
            $this->valF['lieu_adresse_ligne2'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne2'] = $val['lieu_adresse_ligne2'];
        }
        if ($val['lieu_salle'] == "") {
            $this->valF['lieu_salle'] = NULL;
        } else {
            $this->valF['lieu_salle'] = $val['lieu_salle'];
        }
            $this->valF['listes_de_diffusion'] = $val['listes_de_diffusion'];
            $this->valF['participants'] = $val['participants'];
        if (!is_numeric($val['numerotation'])) {
            $this->valF['numerotation'] = ""; // -> requis
        } else {
            $this->valF['numerotation'] = $val['numerotation'];
        }
        if ($val['date_convocation'] != "") {
            $this->valF['date_convocation'] = $this->dateDB($val['date_convocation']);
        } else {
            $this->valF['date_convocation'] = NULL;
        }
        if ($val['reunion_cloture'] == 1 || $val['reunion_cloture'] == "t" || $val['reunion_cloture'] == "Oui") {
            $this->valF['reunion_cloture'] = true;
        } else {
            $this->valF['reunion_cloture'] = false;
        }
        if ($val['date_cloture'] != "") {
            $this->valF['date_cloture'] = $this->dateDB($val['date_cloture']);
        } else {
            $this->valF['date_cloture'] = NULL;
        }
        if ($val['om_fichier_reunion_odj'] == "") {
            $this->valF['om_fichier_reunion_odj'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_odj'] = $val['om_fichier_reunion_odj'];
        }
        if ($val['om_final_reunion_odj'] == 1 || $val['om_final_reunion_odj'] == "t" || $val['om_final_reunion_odj'] == "Oui") {
            $this->valF['om_final_reunion_odj'] = true;
        } else {
            $this->valF['om_final_reunion_odj'] = false;
        }
        if ($val['om_fichier_reunion_cr_global'] == "") {
            $this->valF['om_fichier_reunion_cr_global'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_global'] = $val['om_fichier_reunion_cr_global'];
        }
        if ($val['om_final_reunion_cr_global'] == 1 || $val['om_final_reunion_cr_global'] == "t" || $val['om_final_reunion_cr_global'] == "Oui") {
            $this->valF['om_final_reunion_cr_global'] = true;
        } else {
            $this->valF['om_final_reunion_cr_global'] = false;
        }
        if ($val['om_fichier_reunion_cr_global_signe'] == "") {
            $this->valF['om_fichier_reunion_cr_global_signe'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_global_signe'] = $val['om_fichier_reunion_cr_global_signe'];
        }
        if ($val['om_fichier_reunion_cr_par_dossier_signe'] == "") {
            $this->valF['om_fichier_reunion_cr_par_dossier_signe'] = NULL;
        } else {
            $this->valF['om_fichier_reunion_cr_par_dossier_signe'] = $val['om_fichier_reunion_cr_par_dossier_signe'];
        }
        if ($val['planifier_nouveau'] == 1 || $val['planifier_nouveau'] == "t" || $val['planifier_nouveau'] == "Oui") {
            $this->valF['planifier_nouveau'] = true;
        } else {
            $this->valF['planifier_nouveau'] = false;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion", "hidden");
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("date_reunion", "date");
            $form->setType("heure_reunion", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("participants", "textarea");
            $form->setType("numerotation", "text");
            $form->setType("date_convocation", "date");
            $form->setType("reunion_cloture", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("om_fichier_reunion_odj", "text");
            $form->setType("om_final_reunion_odj", "checkbox");
            $form->setType("om_fichier_reunion_cr_global", "text");
            $form->setType("om_final_reunion_cr_global", "checkbox");
            $form->setType("om_fichier_reunion_cr_global_signe", "text");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "text");
            $form->setType("planifier_nouveau", "checkbox");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion", "hiddenstatic");
            $form->setType("code", "text");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("date_reunion", "date");
            $form->setType("heure_reunion", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("participants", "textarea");
            $form->setType("numerotation", "text");
            $form->setType("date_convocation", "date");
            $form->setType("reunion_cloture", "checkbox");
            $form->setType("date_cloture", "date");
            $form->setType("om_fichier_reunion_odj", "text");
            $form->setType("om_final_reunion_odj", "checkbox");
            $form->setType("om_fichier_reunion_cr_global", "text");
            $form->setType("om_final_reunion_cr_global", "checkbox");
            $form->setType("om_fichier_reunion_cr_global_signe", "text");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "text");
            $form->setType("planifier_nouveau", "checkbox");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("date_reunion", "hiddenstatic");
            $form->setType("heure_reunion", "hiddenstatic");
            $form->setType("lieu_adresse_ligne1", "hiddenstatic");
            $form->setType("lieu_adresse_ligne2", "hiddenstatic");
            $form->setType("lieu_salle", "hiddenstatic");
            $form->setType("listes_de_diffusion", "hiddenstatic");
            $form->setType("participants", "hiddenstatic");
            $form->setType("numerotation", "hiddenstatic");
            $form->setType("date_convocation", "hiddenstatic");
            $form->setType("reunion_cloture", "hiddenstatic");
            $form->setType("date_cloture", "hiddenstatic");
            $form->setType("om_fichier_reunion_odj", "hiddenstatic");
            $form->setType("om_final_reunion_odj", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_global", "hiddenstatic");
            $form->setType("om_final_reunion_cr_global", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_global_signe", "hiddenstatic");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "hiddenstatic");
            $form->setType("planifier_nouveau", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion", "static");
            $form->setType("code", "static");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("date_reunion", "datestatic");
            $form->setType("heure_reunion", "static");
            $form->setType("lieu_adresse_ligne1", "static");
            $form->setType("lieu_adresse_ligne2", "static");
            $form->setType("lieu_salle", "static");
            $form->setType("listes_de_diffusion", "textareastatic");
            $form->setType("participants", "textareastatic");
            $form->setType("numerotation", "static");
            $form->setType("date_convocation", "datestatic");
            $form->setType("reunion_cloture", "checkboxstatic");
            $form->setType("date_cloture", "datestatic");
            $form->setType("om_fichier_reunion_odj", "static");
            $form->setType("om_final_reunion_odj", "checkboxstatic");
            $form->setType("om_fichier_reunion_cr_global", "static");
            $form->setType("om_final_reunion_cr_global", "checkboxstatic");
            $form->setType("om_fichier_reunion_cr_global_signe", "static");
            $form->setType("om_fichier_reunion_cr_par_dossier_signe", "static");
            $form->setType("planifier_nouveau", "checkboxstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('date_reunion','fdate(this)');
        $form->setOnchange('numerotation','VerifNum(this)');
        $form->setOnchange('date_convocation','fdate(this)');
        $form->setOnchange('date_cloture','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion", 11);
        $form->setTaille("code", 20);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("date_reunion", 12);
        $form->setTaille("heure_reunion", 10);
        $form->setTaille("lieu_adresse_ligne1", 30);
        $form->setTaille("lieu_adresse_ligne2", 30);
        $form->setTaille("lieu_salle", 30);
        $form->setTaille("listes_de_diffusion", 80);
        $form->setTaille("participants", 80);
        $form->setTaille("numerotation", 11);
        $form->setTaille("date_convocation", 12);
        $form->setTaille("reunion_cloture", 1);
        $form->setTaille("date_cloture", 12);
        $form->setTaille("om_fichier_reunion_odj", 30);
        $form->setTaille("om_final_reunion_odj", 1);
        $form->setTaille("om_fichier_reunion_cr_global", 30);
        $form->setTaille("om_final_reunion_cr_global", 1);
        $form->setTaille("om_fichier_reunion_cr_global_signe", 30);
        $form->setTaille("om_fichier_reunion_cr_par_dossier_signe", 30);
        $form->setTaille("planifier_nouveau", 1);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion", 11);
        $form->setMax("code", 20);
        $form->setMax("reunion_type", 11);
        $form->setMax("libelle", 100);
        $form->setMax("date_reunion", 12);
        $form->setMax("heure_reunion", 5);
        $form->setMax("lieu_adresse_ligne1", 100);
        $form->setMax("lieu_adresse_ligne2", 100);
        $form->setMax("lieu_salle", 100);
        $form->setMax("listes_de_diffusion", 6);
        $form->setMax("participants", 6);
        $form->setMax("numerotation", 11);
        $form->setMax("date_convocation", 12);
        $form->setMax("reunion_cloture", 1);
        $form->setMax("date_cloture", 12);
        $form->setMax("om_fichier_reunion_odj", 64);
        $form->setMax("om_final_reunion_odj", 1);
        $form->setMax("om_fichier_reunion_cr_global", 64);
        $form->setMax("om_final_reunion_cr_global", 1);
        $form->setMax("om_fichier_reunion_cr_global_signe", 64);
        $form->setMax("om_fichier_reunion_cr_par_dossier_signe", 64);
        $form->setMax("planifier_nouveau", 1);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion',_('reunion'));
        $form->setLib('code',_('code'));
        $form->setLib('reunion_type',_('reunion_type'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('date_reunion',_('date_reunion'));
        $form->setLib('heure_reunion',_('heure_reunion'));
        $form->setLib('lieu_adresse_ligne1',_('lieu_adresse_ligne1'));
        $form->setLib('lieu_adresse_ligne2',_('lieu_adresse_ligne2'));
        $form->setLib('lieu_salle',_('lieu_salle'));
        $form->setLib('listes_de_diffusion',_('listes_de_diffusion'));
        $form->setLib('participants',_('participants'));
        $form->setLib('numerotation',_('numerotation'));
        $form->setLib('date_convocation',_('date_convocation'));
        $form->setLib('reunion_cloture',_('reunion_cloture'));
        $form->setLib('date_cloture',_('date_cloture'));
        $form->setLib('om_fichier_reunion_odj',_('om_fichier_reunion_odj'));
        $form->setLib('om_final_reunion_odj',_('om_final_reunion_odj'));
        $form->setLib('om_fichier_reunion_cr_global',_('om_fichier_reunion_cr_global'));
        $form->setLib('om_final_reunion_cr_global',_('om_final_reunion_cr_global'));
        $form->setLib('om_fichier_reunion_cr_global_signe',_('om_fichier_reunion_cr_global_signe'));
        $form->setLib('om_fichier_reunion_cr_par_dossier_signe',_('om_fichier_reunion_cr_par_dossier_signe'));
        $form->setLib('planifier_nouveau',_('planifier_nouveau'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // reunion_type
        $this->init_select($form, $this->f->db, $maj, null, "reunion_type", $sql_reunion_type, $sql_reunion_type_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_instruction_reunion
        $this->rechercheTable($this->f->db, "dossier_instruction_reunion", "reunion", $id);
        // Verification de la cle secondaire : lien_reunion_r_instance_r_i_membre
        $this->rechercheTable($this->f->db, "lien_reunion_r_instance_r_i_membre", "reunion", $id);
    }


}

?>
