<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class dossier_instruction_reunion_gen extends om_dbform {

    var $table = "dossier_instruction_reunion";
    var $clePrimaire = "dossier_instruction_reunion";
    var $typeCle = "N";
    var $required_field = array(
        "date_souhaitee",
        "dossier_instruction",
        "dossier_instruction_reunion",
        "reunion_type",
        "reunion_type_categorie"
    );
    
    var $foreign_keys_extended = array(
        "reunion_avis" => array("reunion_avis", ),
        "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
        "reunion" => array("reunion", ),
        "reunion_type" => array("reunion_type", ),
        "reunion_categorie" => array("reunion_categorie", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_instruction_reunion'])) {
            $this->valF['dossier_instruction_reunion'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction_reunion'] = $val['dossier_instruction_reunion'];
        }
        if (!is_numeric($val['dossier_instruction'])) {
            $this->valF['dossier_instruction'] = ""; // -> requis
        } else {
            $this->valF['dossier_instruction'] = $val['dossier_instruction'];
        }
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        if (!is_numeric($val['reunion_type_categorie'])) {
            $this->valF['reunion_type_categorie'] = ""; // -> requis
        } else {
            $this->valF['reunion_type_categorie'] = $val['reunion_type_categorie'];
        }
        if ($val['date_souhaitee'] != "") {
            $this->valF['date_souhaitee'] = $this->dateDB($val['date_souhaitee']);
        }
            $this->valF['motivation'] = $val['motivation'];
        if (!is_numeric($val['proposition_avis'])) {
            $this->valF['proposition_avis'] = NULL;
        } else {
            $this->valF['proposition_avis'] = $val['proposition_avis'];
        }
        if ($val['proposition_avis_complement'] == "") {
            $this->valF['proposition_avis_complement'] = NULL;
        } else {
            $this->valF['proposition_avis_complement'] = $val['proposition_avis_complement'];
        }
        if (!is_numeric($val['reunion'])) {
            $this->valF['reunion'] = NULL;
        } else {
            $this->valF['reunion'] = $val['reunion'];
        }
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = NULL;
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
        if (!is_numeric($val['avis'])) {
            $this->valF['avis'] = NULL;
        } else {
            $this->valF['avis'] = $val['avis'];
        }
        if ($val['avis_complement'] == "") {
            $this->valF['avis_complement'] = NULL;
        } else {
            $this->valF['avis_complement'] = $val['avis_complement'];
        }
            $this->valF['avis_motivation'] = $val['avis_motivation'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_instruction_reunion", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_categorie", $this->retourformulaire)) {
                $form->setType("reunion_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type_categorie", "select");
            }
            $form->setType("date_souhaitee", "date");
            $form->setType("motivation", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("proposition_avis", "selecthiddenstatic");
            } else {
                $form->setType("proposition_avis", "select");
            }
            $form->setType("proposition_avis_complement", "text");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("avis", "selecthiddenstatic");
            } else {
                $form->setType("avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("avis_motivation", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_instruction_reunion", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_instruction", $this->retourformulaire)) {
                $form->setType("dossier_instruction", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_categorie", $this->retourformulaire)) {
                $form->setType("reunion_type_categorie", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type_categorie", "select");
            }
            $form->setType("date_souhaitee", "date");
            $form->setType("motivation", "textarea");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("proposition_avis", "selecthiddenstatic");
            } else {
                $form->setType("proposition_avis", "select");
            }
            $form->setType("proposition_avis_complement", "text");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("reunion_avis", $this->retourformulaire)) {
                $form->setType("avis", "selecthiddenstatic");
            } else {
                $form->setType("avis", "select");
            }
            $form->setType("avis_complement", "text");
            $form->setType("avis_motivation", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_instruction_reunion", "hiddenstatic");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_type_categorie", "selectstatic");
            $form->setType("date_souhaitee", "hiddenstatic");
            $form->setType("motivation", "hiddenstatic");
            $form->setType("proposition_avis", "selectstatic");
            $form->setType("proposition_avis_complement", "hiddenstatic");
            $form->setType("reunion", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("avis", "selectstatic");
            $form->setType("avis_complement", "hiddenstatic");
            $form->setType("avis_motivation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_instruction_reunion", "static");
            $form->setType("dossier_instruction", "selectstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_type_categorie", "selectstatic");
            $form->setType("date_souhaitee", "datestatic");
            $form->setType("motivation", "textareastatic");
            $form->setType("proposition_avis", "selectstatic");
            $form->setType("proposition_avis_complement", "static");
            $form->setType("reunion", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("avis", "selectstatic");
            $form->setType("avis_complement", "static");
            $form->setType("avis_motivation", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_instruction_reunion','VerifNum(this)');
        $form->setOnchange('dossier_instruction','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('reunion_type_categorie','VerifNum(this)');
        $form->setOnchange('date_souhaitee','fdate(this)');
        $form->setOnchange('proposition_avis','VerifNum(this)');
        $form->setOnchange('reunion','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
        $form->setOnchange('avis','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_instruction_reunion", 11);
        $form->setTaille("dossier_instruction", 11);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("reunion_type_categorie", 11);
        $form->setTaille("date_souhaitee", 12);
        $form->setTaille("motivation", 80);
        $form->setTaille("proposition_avis", 11);
        $form->setTaille("proposition_avis_complement", 30);
        $form->setTaille("reunion", 11);
        $form->setTaille("ordre", 11);
        $form->setTaille("avis", 11);
        $form->setTaille("avis_complement", 30);
        $form->setTaille("avis_motivation", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_instruction_reunion", 11);
        $form->setMax("dossier_instruction", 11);
        $form->setMax("reunion_type", 11);
        $form->setMax("reunion_type_categorie", 11);
        $form->setMax("date_souhaitee", 12);
        $form->setMax("motivation", 6);
        $form->setMax("proposition_avis", 11);
        $form->setMax("proposition_avis_complement", 250);
        $form->setMax("reunion", 11);
        $form->setMax("ordre", 11);
        $form->setMax("avis", 11);
        $form->setMax("avis_complement", 250);
        $form->setMax("avis_motivation", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_instruction_reunion',_('dossier_instruction_reunion'));
        $form->setLib('dossier_instruction',_('dossier_instruction'));
        $form->setLib('reunion_type',_('reunion_type'));
        $form->setLib('reunion_type_categorie',_('reunion_type_categorie'));
        $form->setLib('date_souhaitee',_('date_souhaitee'));
        $form->setLib('motivation',_('motivation'));
        $form->setLib('proposition_avis',_('proposition_avis'));
        $form->setLib('proposition_avis_complement',_('proposition_avis_complement'));
        $form->setLib('reunion',_('reunion'));
        $form->setLib('ordre',_('ordre'));
        $form->setLib('avis',_('avis'));
        $form->setLib('avis_complement',_('avis_complement'));
        $form->setLib('avis_motivation',_('avis_motivation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // avis
        $this->init_select($form, $this->f->db, $maj, null, "avis", $sql_avis, $sql_avis_by_id, true);
        // dossier_instruction
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction", $sql_dossier_instruction, $sql_dossier_instruction_by_id, false);
        // proposition_avis
        $this->init_select($form, $this->f->db, $maj, null, "proposition_avis", $sql_proposition_avis, $sql_proposition_avis_by_id, true);
        // reunion
        $this->init_select($form, $this->f->db, $maj, null, "reunion", $sql_reunion, $sql_reunion_by_id, false);
        // reunion_type
        $this->init_select($form, $this->f->db, $maj, null, "reunion_type", $sql_reunion_type, $sql_reunion_type_by_id, true);
        // reunion_type_categorie
        $this->init_select($form, $this->f->db, $maj, null, "reunion_type_categorie", $sql_reunion_type_categorie, $sql_reunion_type_categorie_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_instruction', $this->retourformulaire))
                $form->setVal('dossier_instruction', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion', $this->retourformulaire))
                $form->setVal('reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_categorie', $this->retourformulaire))
                $form->setVal('reunion_type_categorie', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('avis', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_avis', $this->retourformulaire))
                $form->setVal('proposition_avis', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_instruction_reunion", $id);
        // Verification de la cle secondaire : autorite_police
        $this->rechercheTable($this->f->db, "autorite_police", "dossier_instruction_reunion_prochain", $id);
        // Verification de la cle secondaire : proces_verbal
        $this->rechercheTable($this->f->db, "proces_verbal", "dossier_instruction_reunion", $id);
    }


}

?>
