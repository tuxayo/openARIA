<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class etablissement_parcelle_gen extends om_dbform {

    var $table = "etablissement_parcelle";
    var $clePrimaire = "etablissement_parcelle";
    var $typeCle = "N";
    var $required_field = array(
        "etablissement_parcelle"
    );
    
    var $foreign_keys_extended = array(
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['etablissement_parcelle'])) {
            $this->valF['etablissement_parcelle'] = ""; // -> requis
        } else {
            $this->valF['etablissement_parcelle'] = $val['etablissement_parcelle'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if ($val['ref_cadastre'] == "") {
            $this->valF['ref_cadastre'] = NULL;
        } else {
            $this->valF['ref_cadastre'] = $val['ref_cadastre'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("etablissement_parcelle", "hidden");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("etablissement_parcelle", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("etablissement_parcelle", "hiddenstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("ref_cadastre", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("etablissement_parcelle", "static");
            $form->setType("etablissement", "selectstatic");
            $form->setType("ref_cadastre", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('etablissement_parcelle','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("etablissement_parcelle", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("ref_cadastre", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("etablissement_parcelle", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("ref_cadastre", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('etablissement_parcelle',_('etablissement_parcelle'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('ref_cadastre',_('ref_cadastre'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
