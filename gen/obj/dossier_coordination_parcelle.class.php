<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class dossier_coordination_parcelle_gen extends om_dbform {

    var $table = "dossier_coordination_parcelle";
    var $clePrimaire = "dossier_coordination_parcelle";
    var $typeCle = "N";
    var $required_field = array(
        "dossier_coordination_parcelle"
    );
    
    var $foreign_keys_extended = array(
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['dossier_coordination_parcelle'])) {
            $this->valF['dossier_coordination_parcelle'] = ""; // -> requis
        } else {
            $this->valF['dossier_coordination_parcelle'] = $val['dossier_coordination_parcelle'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if ($val['ref_cadastre'] == "") {
            $this->valF['ref_cadastre'] = NULL;
        } else {
            $this->valF['ref_cadastre'] = $val['ref_cadastre'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("dossier_coordination_parcelle", "hidden");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("dossier_coordination_parcelle", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            $form->setType("ref_cadastre", "text");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("dossier_coordination_parcelle", "hiddenstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("ref_cadastre", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("dossier_coordination_parcelle", "static");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("ref_cadastre", "static");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('dossier_coordination_parcelle','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("dossier_coordination_parcelle", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("ref_cadastre", 20);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("dossier_coordination_parcelle", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("ref_cadastre", 20);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('dossier_coordination_parcelle',_('dossier_coordination_parcelle'));
        $form->setLib('dossier_coordination',_('dossier_coordination'));
        $form->setLib('ref_cadastre',_('ref_cadastre'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // dossier_coordination
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination", $sql_dossier_coordination, $sql_dossier_coordination_by_id, false);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
