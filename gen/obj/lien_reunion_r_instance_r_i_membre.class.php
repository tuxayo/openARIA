<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class lien_reunion_r_instance_r_i_membre_gen extends om_dbform {

    var $table = "lien_reunion_r_instance_r_i_membre";
    var $clePrimaire = "lien_reunion_r_instance_r_i_membre";
    var $typeCle = "N";
    var $required_field = array(
        "lien_reunion_r_instance_r_i_membre",
        "reunion",
        "reunion_instance"
    );
    
    var $foreign_keys_extended = array(
        "reunion" => array("reunion", ),
        "reunion_instance" => array("reunion_instance", ),
        "reunion_instance_membre" => array("reunion_instance_membre", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['lien_reunion_r_instance_r_i_membre'])) {
            $this->valF['lien_reunion_r_instance_r_i_membre'] = ""; // -> requis
        } else {
            $this->valF['lien_reunion_r_instance_r_i_membre'] = $val['lien_reunion_r_instance_r_i_membre'];
        }
        if (!is_numeric($val['reunion'])) {
            $this->valF['reunion'] = ""; // -> requis
        } else {
            $this->valF['reunion'] = $val['reunion'];
        }
        if (!is_numeric($val['reunion_instance'])) {
            $this->valF['reunion_instance'] = ""; // -> requis
        } else {
            $this->valF['reunion_instance'] = $val['reunion_instance'];
        }
        if (!is_numeric($val['reunion_instance_membre'])) {
            $this->valF['reunion_instance_membre'] = NULL;
        } else {
            $this->valF['reunion_instance_membre'] = $val['reunion_instance_membre'];
        }
            $this->valF['observation'] = $val['observation'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("lien_reunion_r_instance_r_i_membre", "hidden");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance_membre", $this->retourformulaire)) {
                $form->setType("reunion_instance_membre", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance_membre", "select");
            }
            $form->setType("observation", "textarea");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("lien_reunion_r_instance_r_i_membre", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("reunion", $this->retourformulaire)) {
                $form->setType("reunion", "selecthiddenstatic");
            } else {
                $form->setType("reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance_membre", $this->retourformulaire)) {
                $form->setType("reunion_instance_membre", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance_membre", "select");
            }
            $form->setType("observation", "textarea");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("lien_reunion_r_instance_r_i_membre", "hiddenstatic");
            $form->setType("reunion", "selectstatic");
            $form->setType("reunion_instance", "selectstatic");
            $form->setType("reunion_instance_membre", "selectstatic");
            $form->setType("observation", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("lien_reunion_r_instance_r_i_membre", "static");
            $form->setType("reunion", "selectstatic");
            $form->setType("reunion_instance", "selectstatic");
            $form->setType("reunion_instance_membre", "selectstatic");
            $form->setType("observation", "textareastatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('lien_reunion_r_instance_r_i_membre','VerifNum(this)');
        $form->setOnchange('reunion','VerifNum(this)');
        $form->setOnchange('reunion_instance','VerifNum(this)');
        $form->setOnchange('reunion_instance_membre','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("lien_reunion_r_instance_r_i_membre", 11);
        $form->setTaille("reunion", 11);
        $form->setTaille("reunion_instance", 11);
        $form->setTaille("reunion_instance_membre", 11);
        $form->setTaille("observation", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("lien_reunion_r_instance_r_i_membre", 11);
        $form->setMax("reunion", 11);
        $form->setMax("reunion_instance", 11);
        $form->setMax("reunion_instance_membre", 11);
        $form->setMax("observation", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('lien_reunion_r_instance_r_i_membre',_('lien_reunion_r_instance_r_i_membre'));
        $form->setLib('reunion',_('reunion'));
        $form->setLib('reunion_instance',_('reunion_instance'));
        $form->setLib('reunion_instance_membre',_('reunion_instance_membre'));
        $form->setLib('observation',_('observation'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // reunion
        $this->init_select($form, $this->f->db, $maj, null, "reunion", $sql_reunion, $sql_reunion_by_id, false);
        // reunion_instance
        $this->init_select($form, $this->f->db, $maj, null, "reunion_instance", $sql_reunion_instance, $sql_reunion_instance_by_id, true);
        // reunion_instance_membre
        $this->init_select($form, $this->f->db, $maj, null, "reunion_instance_membre", $sql_reunion_instance_membre, $sql_reunion_instance_membre_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion', $this->retourformulaire))
                $form->setVal('reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_instance', $this->retourformulaire))
                $form->setVal('reunion_instance', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_instance_membre', $this->retourformulaire))
                $form->setVal('reunion_instance_membre', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
