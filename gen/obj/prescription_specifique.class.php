<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class prescription_specifique_gen extends om_dbform {

    var $table = "prescription_specifique";
    var $clePrimaire = "prescription_specifique";
    var $typeCle = "N";
    var $required_field = array(
        "prescription_reglementaire",
        "prescription_specifique",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "prescription_reglementaire" => array("prescription_reglementaire", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['prescription_specifique'])) {
            $this->valF['prescription_specifique'] = ""; // -> requis
        } else {
            $this->valF['prescription_specifique'] = $val['prescription_specifique'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['libelle'] == "") {
            $this->valF['libelle'] = NULL;
        } else {
            $this->valF['libelle'] = $val['libelle'];
        }
            $this->valF['description_ps_om_html'] = $val['description_ps_om_html'];
        if (!is_numeric($val['prescription_reglementaire'])) {
            $this->valF['prescription_reglementaire'] = ""; // -> requis
        } else {
            $this->valF['prescription_reglementaire'] = $val['prescription_reglementaire'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("prescription_specifique", "hidden");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("description_ps_om_html", "html");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("prescription_specifique", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            $form->setType("libelle", "text");
            $form->setType("description_ps_om_html", "html");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("prescription_specifique", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("description_ps_om_html", "hiddenstatic");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("prescription_specifique", "static");
            $form->setType("service", "selectstatic");
            $form->setType("libelle", "static");
            $form->setType("description_ps_om_html", "htmlstatic");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('prescription_specifique','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('prescription_reglementaire','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("prescription_specifique", 11);
        $form->setTaille("service", 11);
        $form->setTaille("libelle", 30);
        $form->setTaille("description_ps_om_html", 80);
        $form->setTaille("prescription_reglementaire", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("prescription_specifique", 11);
        $form->setMax("service", 11);
        $form->setMax("libelle", 100);
        $form->setMax("description_ps_om_html", 6);
        $form->setMax("prescription_reglementaire", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('prescription_specifique',_('prescription_specifique'));
        $form->setLib('service',_('service'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('description_ps_om_html',_('description_ps_om_html'));
        $form->setLib('prescription_reglementaire',_('prescription_reglementaire'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // prescription_reglementaire
        $this->init_select($form, $this->f->db, $maj, null, "prescription_reglementaire", $sql_prescription_reglementaire, $sql_prescription_reglementaire_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('prescription_reglementaire', $this->retourformulaire))
                $form->setVal('prescription_reglementaire', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
