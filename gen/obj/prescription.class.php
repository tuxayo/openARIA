<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class prescription_gen extends om_dbform {

    var $table = "prescription";
    var $clePrimaire = "prescription";
    var $typeCle = "N";
    var $required_field = array(
        "analyses",
        "ordre",
        "prescription",
        "prescription_reglementaire"
    );
    
    var $foreign_keys_extended = array(
        "analyses" => array("analyses", ),
        "prescription_reglementaire" => array("prescription_reglementaire", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['prescription'])) {
            $this->valF['prescription'] = ""; // -> requis
        } else {
            $this->valF['prescription'] = $val['prescription'];
        }
        if (!is_numeric($val['analyses'])) {
            $this->valF['analyses'] = ""; // -> requis
        } else {
            $this->valF['analyses'] = $val['analyses'];
        }
        if (!is_numeric($val['ordre'])) {
            $this->valF['ordre'] = ""; // -> requis
        } else {
            $this->valF['ordre'] = $val['ordre'];
        }
        if (!is_numeric($val['prescription_reglementaire'])) {
            $this->valF['prescription_reglementaire'] = ""; // -> requis
        } else {
            $this->valF['prescription_reglementaire'] = $val['prescription_reglementaire'];
        }
            $this->valF['pr_description_om_html'] = $val['pr_description_om_html'];
        if ($val['pr_defavorable'] == 1 || $val['pr_defavorable'] == "t" || $val['pr_defavorable'] == "Oui") {
            $this->valF['pr_defavorable'] = true;
        } else {
            $this->valF['pr_defavorable'] = false;
        }
            $this->valF['ps_description_om_html'] = $val['ps_description_om_html'];
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("prescription", "hidden");
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            $form->setType("pr_description_om_html", "html");
            $form->setType("pr_defavorable", "checkbox");
            $form->setType("ps_description_om_html", "html");
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("prescription", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("analyses", $this->retourformulaire)) {
                $form->setType("analyses", "selecthiddenstatic");
            } else {
                $form->setType("analyses", "select");
            }
            $form->setType("ordre", "text");
            if ($this->is_in_context_of_foreign_key("prescription_reglementaire", $this->retourformulaire)) {
                $form->setType("prescription_reglementaire", "selecthiddenstatic");
            } else {
                $form->setType("prescription_reglementaire", "select");
            }
            $form->setType("pr_description_om_html", "html");
            $form->setType("pr_defavorable", "checkbox");
            $form->setType("ps_description_om_html", "html");
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("prescription", "hiddenstatic");
            $form->setType("analyses", "selectstatic");
            $form->setType("ordre", "hiddenstatic");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("pr_description_om_html", "hiddenstatic");
            $form->setType("pr_defavorable", "hiddenstatic");
            $form->setType("ps_description_om_html", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("prescription", "static");
            $form->setType("analyses", "selectstatic");
            $form->setType("ordre", "static");
            $form->setType("prescription_reglementaire", "selectstatic");
            $form->setType("pr_description_om_html", "htmlstatic");
            $form->setType("pr_defavorable", "checkboxstatic");
            $form->setType("ps_description_om_html", "htmlstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('prescription','VerifNum(this)');
        $form->setOnchange('analyses','VerifNum(this)');
        $form->setOnchange('ordre','VerifNum(this)');
        $form->setOnchange('prescription_reglementaire','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("prescription", 11);
        $form->setTaille("analyses", 11);
        $form->setTaille("ordre", 11);
        $form->setTaille("prescription_reglementaire", 11);
        $form->setTaille("pr_description_om_html", 80);
        $form->setTaille("pr_defavorable", 1);
        $form->setTaille("ps_description_om_html", 80);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("prescription", 11);
        $form->setMax("analyses", 11);
        $form->setMax("ordre", 11);
        $form->setMax("prescription_reglementaire", 11);
        $form->setMax("pr_description_om_html", 6);
        $form->setMax("pr_defavorable", 1);
        $form->setMax("ps_description_om_html", 6);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('prescription',_('prescription'));
        $form->setLib('analyses',_('analyses'));
        $form->setLib('ordre',_('ordre'));
        $form->setLib('prescription_reglementaire',_('prescription_reglementaire'));
        $form->setLib('pr_description_om_html',_('pr_description_om_html'));
        $form->setLib('pr_defavorable',_('pr_defavorable'));
        $form->setLib('ps_description_om_html',_('ps_description_om_html'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // analyses
        $this->init_select($form, $this->f->db, $maj, null, "analyses", $sql_analyses, $sql_analyses_by_id, false);
        // prescription_reglementaire
        $this->init_select($form, $this->f->db, $maj, null, "prescription_reglementaire", $sql_prescription_reglementaire, $sql_prescription_reglementaire_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('analyses', $this->retourformulaire))
                $form->setVal('analyses', $idxformulaire);
            if($this->is_in_context_of_foreign_key('prescription_reglementaire', $this->retourformulaire))
                $form->setVal('prescription_reglementaire', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
