<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class autorite_police_gen extends om_dbform {

    var $table = "autorite_police";
    var $clePrimaire = "autorite_police";
    var $typeCle = "N";
    var $required_field = array(
        "autorite_police",
        "autorite_police_decision",
        "autorite_police_motif",
        "date_decision",
        "delai",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "autorite_police_decision" => array("autorite_police_decision", ),
        "autorite_police_motif" => array("autorite_police_motif", ),
        "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
        "dossier_instruction_reunion" => array("dossier_instruction_reunion", ),
        "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['autorite_police'])) {
            $this->valF['autorite_police'] = ""; // -> requis
        } else {
            $this->valF['autorite_police'] = $val['autorite_police'];
        }
        if (!is_numeric($val['autorite_police_decision'])) {
            $this->valF['autorite_police_decision'] = ""; // -> requis
        } else {
            $this->valF['autorite_police_decision'] = $val['autorite_police_decision'];
        }
        if ($val['date_decision'] != "") {
            $this->valF['date_decision'] = $this->dateDB($val['date_decision']);
        }
        if (!is_numeric($val['delai'])) {
            $this->valF['delai'] = ""; // -> requis
        } else {
            $this->valF['delai'] = $val['delai'];
        }
        if (!is_numeric($val['autorite_police_motif'])) {
            $this->valF['autorite_police_motif'] = ""; // -> requis
        } else {
            $this->valF['autorite_police_motif'] = $val['autorite_police_motif'];
        }
        if ($val['cloture'] == 1 || $val['cloture'] == "t" || $val['cloture'] == "Oui") {
            $this->valF['cloture'] = true;
        } else {
            $this->valF['cloture'] = false;
        }
        if ($val['date_notification'] != "") {
            $this->valF['date_notification'] = $this->dateDB($val['date_notification']);
        } else {
            $this->valF['date_notification'] = NULL;
        }
        if ($val['date_butoir'] != "") {
            $this->valF['date_butoir'] = $this->dateDB($val['date_butoir']);
        } else {
            $this->valF['date_butoir'] = NULL;
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if (!is_numeric($val['dossier_instruction_reunion'])) {
            $this->valF['dossier_instruction_reunion'] = NULL;
        } else {
            $this->valF['dossier_instruction_reunion'] = $val['dossier_instruction_reunion'];
        }
        if (!is_numeric($val['dossier_coordination'])) {
            $this->valF['dossier_coordination'] = NULL;
        } else {
            $this->valF['dossier_coordination'] = $val['dossier_coordination'];
        }
        if (!is_numeric($val['etablissement'])) {
            $this->valF['etablissement'] = NULL;
        } else {
            $this->valF['etablissement'] = $val['etablissement'];
        }
        if (!is_numeric($val['dossier_instruction_reunion_prochain'])) {
            $this->valF['dossier_instruction_reunion_prochain'] = NULL;
        } else {
            $this->valF['dossier_instruction_reunion_prochain'] = $val['dossier_instruction_reunion_prochain'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("autorite_police", "hidden");
            if ($this->is_in_context_of_foreign_key("autorite_police_decision", $this->retourformulaire)) {
                $form->setType("autorite_police_decision", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_decision", "select");
            }
            $form->setType("date_decision", "date");
            $form->setType("delai", "text");
            if ($this->is_in_context_of_foreign_key("autorite_police_motif", $this->retourformulaire)) {
                $form->setType("autorite_police_motif", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_motif", "select");
            }
            $form->setType("cloture", "checkbox");
            $form->setType("date_notification", "date");
            $form->setType("date_butoir", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion_prochain", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion_prochain", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("autorite_police", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("autorite_police_decision", $this->retourformulaire)) {
                $form->setType("autorite_police_decision", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_decision", "select");
            }
            $form->setType("date_decision", "date");
            $form->setType("delai", "text");
            if ($this->is_in_context_of_foreign_key("autorite_police_motif", $this->retourformulaire)) {
                $form->setType("autorite_police_motif", "selecthiddenstatic");
            } else {
                $form->setType("autorite_police_motif", "select");
            }
            $form->setType("cloture", "checkbox");
            $form->setType("date_notification", "date");
            $form->setType("date_butoir", "date");
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_coordination", $this->retourformulaire)) {
                $form->setType("dossier_coordination", "selecthiddenstatic");
            } else {
                $form->setType("dossier_coordination", "select");
            }
            if ($this->is_in_context_of_foreign_key("etablissement", $this->retourformulaire)) {
                $form->setType("etablissement", "selecthiddenstatic");
            } else {
                $form->setType("etablissement", "select");
            }
            if ($this->is_in_context_of_foreign_key("dossier_instruction_reunion", $this->retourformulaire)) {
                $form->setType("dossier_instruction_reunion_prochain", "selecthiddenstatic");
            } else {
                $form->setType("dossier_instruction_reunion_prochain", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("autorite_police", "hiddenstatic");
            $form->setType("autorite_police_decision", "selectstatic");
            $form->setType("date_decision", "hiddenstatic");
            $form->setType("delai", "hiddenstatic");
            $form->setType("autorite_police_motif", "selectstatic");
            $form->setType("cloture", "hiddenstatic");
            $form->setType("date_notification", "hiddenstatic");
            $form->setType("date_butoir", "hiddenstatic");
            $form->setType("service", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_instruction_reunion_prochain", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("autorite_police", "static");
            $form->setType("autorite_police_decision", "selectstatic");
            $form->setType("date_decision", "datestatic");
            $form->setType("delai", "static");
            $form->setType("autorite_police_motif", "selectstatic");
            $form->setType("cloture", "checkboxstatic");
            $form->setType("date_notification", "datestatic");
            $form->setType("date_butoir", "datestatic");
            $form->setType("service", "selectstatic");
            $form->setType("dossier_instruction_reunion", "selectstatic");
            $form->setType("dossier_coordination", "selectstatic");
            $form->setType("etablissement", "selectstatic");
            $form->setType("dossier_instruction_reunion_prochain", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('autorite_police','VerifNum(this)');
        $form->setOnchange('autorite_police_decision','VerifNum(this)');
        $form->setOnchange('date_decision','fdate(this)');
        $form->setOnchange('delai','VerifNum(this)');
        $form->setOnchange('autorite_police_motif','VerifNum(this)');
        $form->setOnchange('date_notification','fdate(this)');
        $form->setOnchange('date_butoir','fdate(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('dossier_instruction_reunion','VerifNum(this)');
        $form->setOnchange('dossier_coordination','VerifNum(this)');
        $form->setOnchange('etablissement','VerifNum(this)');
        $form->setOnchange('dossier_instruction_reunion_prochain','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("autorite_police", 11);
        $form->setTaille("autorite_police_decision", 11);
        $form->setTaille("date_decision", 12);
        $form->setTaille("delai", 11);
        $form->setTaille("autorite_police_motif", 11);
        $form->setTaille("cloture", 1);
        $form->setTaille("date_notification", 12);
        $form->setTaille("date_butoir", 12);
        $form->setTaille("service", 11);
        $form->setTaille("dossier_instruction_reunion", 11);
        $form->setTaille("dossier_coordination", 11);
        $form->setTaille("etablissement", 11);
        $form->setTaille("dossier_instruction_reunion_prochain", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("autorite_police", 11);
        $form->setMax("autorite_police_decision", 11);
        $form->setMax("date_decision", 12);
        $form->setMax("delai", 11);
        $form->setMax("autorite_police_motif", 11);
        $form->setMax("cloture", 1);
        $form->setMax("date_notification", 12);
        $form->setMax("date_butoir", 12);
        $form->setMax("service", 11);
        $form->setMax("dossier_instruction_reunion", 11);
        $form->setMax("dossier_coordination", 11);
        $form->setMax("etablissement", 11);
        $form->setMax("dossier_instruction_reunion_prochain", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('autorite_police',_('autorite_police'));
        $form->setLib('autorite_police_decision',_('autorite_police_decision'));
        $form->setLib('date_decision',_('date_decision'));
        $form->setLib('delai',_('delai'));
        $form->setLib('autorite_police_motif',_('autorite_police_motif'));
        $form->setLib('cloture',_('cloture'));
        $form->setLib('date_notification',_('date_notification'));
        $form->setLib('date_butoir',_('date_butoir'));
        $form->setLib('service',_('service'));
        $form->setLib('dossier_instruction_reunion',_('dossier_instruction_reunion'));
        $form->setLib('dossier_coordination',_('dossier_coordination'));
        $form->setLib('etablissement',_('etablissement'));
        $form->setLib('dossier_instruction_reunion_prochain',_('dossier_instruction_reunion_prochain'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // autorite_police_decision
        $this->init_select($form, $this->f->db, $maj, null, "autorite_police_decision", $sql_autorite_police_decision, $sql_autorite_police_decision_by_id, true);
        // autorite_police_motif
        $this->init_select($form, $this->f->db, $maj, null, "autorite_police_motif", $sql_autorite_police_motif, $sql_autorite_police_motif_by_id, true);
        // dossier_coordination
        $this->init_select($form, $this->f->db, $maj, null, "dossier_coordination", $sql_dossier_coordination, $sql_dossier_coordination_by_id, false);
        // dossier_instruction_reunion
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction_reunion", $sql_dossier_instruction_reunion, $sql_dossier_instruction_reunion_by_id, false);
        // dossier_instruction_reunion_prochain
        $this->init_select($form, $this->f->db, $maj, null, "dossier_instruction_reunion_prochain", $sql_dossier_instruction_reunion_prochain, $sql_dossier_instruction_reunion_prochain_by_id, false);
        // etablissement
        $this->init_select($form, $this->f->db, $maj, null, "etablissement", $sql_etablissement, $sql_etablissement_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('autorite_police_decision', $this->retourformulaire))
                $form->setVal('autorite_police_decision', $idxformulaire);
            if($this->is_in_context_of_foreign_key('autorite_police_motif', $this->retourformulaire))
                $form->setVal('autorite_police_motif', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_coordination', $this->retourformulaire))
                $form->setVal('dossier_coordination', $idxformulaire);
            if($this->is_in_context_of_foreign_key('etablissement', $this->retourformulaire))
                $form->setVal('etablissement', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion', $idxformulaire);
            if($this->is_in_context_of_foreign_key('dossier_instruction_reunion', $this->retourformulaire))
                $form->setVal('dossier_instruction_reunion_prochain', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : lien_autorite_police_courrier
        $this->rechercheTable($this->f->db, "lien_autorite_police_courrier", "autorite_police", $id);
    }


}

?>
