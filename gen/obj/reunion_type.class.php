<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class reunion_type_gen extends om_dbform {

    var $table = "reunion_type";
    var $clePrimaire = "reunion_type";
    var $typeCle = "N";
    var $required_field = array(
        "code",
        "libelle",
        "reunion_type",
        "service"
    );
    
    var $foreign_keys_extended = array(
        "modele_edition" => array("modele_edition", ),
        "reunion_instance" => array("reunion_instance", ),
        "service" => array("service", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = ""; // -> requis
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        $this->valF['code'] = $val['code'];
        $this->valF['libelle'] = $val['libelle'];
        if ($val['lieu_adresse_ligne1'] == "") {
            $this->valF['lieu_adresse_ligne1'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne1'] = $val['lieu_adresse_ligne1'];
        }
        if ($val['lieu_adresse_ligne2'] == "") {
            $this->valF['lieu_adresse_ligne2'] = NULL;
        } else {
            $this->valF['lieu_adresse_ligne2'] = $val['lieu_adresse_ligne2'];
        }
        if ($val['lieu_salle'] == "") {
            $this->valF['lieu_salle'] = NULL;
        } else {
            $this->valF['lieu_salle'] = $val['lieu_salle'];
        }
        if ($val['heure'] == "") {
            $this->valF['heure'] = NULL;
        } else {
            $this->valF['heure'] = $val['heure'];
        }
            $this->valF['listes_de_diffusion'] = $val['listes_de_diffusion'];
            $this->valF['modele_courriel_convoquer'] = $val['modele_courriel_convoquer'];
            $this->valF['modele_courriel_cloturer'] = $val['modele_courriel_cloturer'];
        if ($val['commission'] == 1 || $val['commission'] == "t" || $val['commission'] == "Oui") {
            $this->valF['commission'] = true;
        } else {
            $this->valF['commission'] = false;
        }
        if (!is_numeric($val['president'])) {
            $this->valF['president'] = NULL;
        } else {
            $this->valF['president'] = $val['president'];
        }
        if (!is_numeric($val['modele_ordre_du_jour'])) {
            $this->valF['modele_ordre_du_jour'] = NULL;
        } else {
            $this->valF['modele_ordre_du_jour'] = $val['modele_ordre_du_jour'];
        }
        if (!is_numeric($val['modele_compte_rendu_global'])) {
            $this->valF['modele_compte_rendu_global'] = NULL;
        } else {
            $this->valF['modele_compte_rendu_global'] = $val['modele_compte_rendu_global'];
        }
        if (!is_numeric($val['modele_compte_rendu_specifique'])) {
            $this->valF['modele_compte_rendu_specifique'] = NULL;
        } else {
            $this->valF['modele_compte_rendu_specifique'] = $val['modele_compte_rendu_specifique'];
        }
        if (!is_numeric($val['modele_feuille_presence'])) {
            $this->valF['modele_feuille_presence'] = NULL;
        } else {
            $this->valF['modele_feuille_presence'] = $val['modele_feuille_presence'];
        }
        if (!is_numeric($val['service'])) {
            $this->valF['service'] = ""; // -> requis
        } else {
            $this->valF['service'] = $val['service'];
        }
        if ($val['om_validite_debut'] != "") {
            $this->valF['om_validite_debut'] = $this->dateDB($val['om_validite_debut']);
        } else {
            $this->valF['om_validite_debut'] = NULL;
        }
        if ($val['om_validite_fin'] != "") {
            $this->valF['om_validite_fin'] = $this->dateDB($val['om_validite_fin']);
        } else {
            $this->valF['om_validite_fin'] = NULL;
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }
    /**
     * Methode verifier
     */
    function verifier($val = array(), &$db = null, $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::verifier($val, $this->f->db, null);

        // gestion des dates de validites
        $date_debut = $this->valF['om_validite_debut'];
        $date_fin = $this->valF['om_validite_fin'];

        if ($date_debut != '' and $date_fin != '') {
        
            $date_debut = explode('-', $this->valF['om_validite_debut']);
            $date_fin = explode('-', $this->valF['om_validite_fin']);

            $time_debut = mktime(0, 0, 0, $date_debut[1], $date_debut[2],
                                 $date_debut[0]);
            $time_fin = mktime(0, 0, 0, $date_fin[1], $date_fin[2],
                                 $date_fin[0]);

            if ($time_debut > $time_fin or $time_debut == $time_fin) {
                $this->correct = false;
                $this->addToMessage(_('La date de fin de validite doit etre future a la de debut de validite.'));
            }
        }
    }


    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion_type", "hidden");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("heure", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("modele_courriel_convoquer", "textarea");
            $form->setType("modele_courriel_cloturer", "textarea");
            $form->setType("commission", "checkbox");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("president", "selecthiddenstatic");
            } else {
                $form->setType("president", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_ordre_du_jour", "selecthiddenstatic");
            } else {
                $form->setType("modele_ordre_du_jour", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_global", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_global", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_specifique", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_specifique", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_feuille_presence", "selecthiddenstatic");
            } else {
                $form->setType("modele_feuille_presence", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion_type", "hiddenstatic");
            $form->setType("code", "text");
            $form->setType("libelle", "text");
            $form->setType("lieu_adresse_ligne1", "text");
            $form->setType("lieu_adresse_ligne2", "text");
            $form->setType("lieu_salle", "text");
            $form->setType("heure", "text");
            $form->setType("listes_de_diffusion", "textarea");
            $form->setType("modele_courriel_convoquer", "textarea");
            $form->setType("modele_courriel_cloturer", "textarea");
            $form->setType("commission", "checkbox");
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("president", "selecthiddenstatic");
            } else {
                $form->setType("president", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_ordre_du_jour", "selecthiddenstatic");
            } else {
                $form->setType("modele_ordre_du_jour", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_global", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_global", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_compte_rendu_specifique", "selecthiddenstatic");
            } else {
                $form->setType("modele_compte_rendu_specifique", "select");
            }
            if ($this->is_in_context_of_foreign_key("modele_edition", $this->retourformulaire)) {
                $form->setType("modele_feuille_presence", "selecthiddenstatic");
            } else {
                $form->setType("modele_feuille_presence", "select");
            }
            if ($this->is_in_context_of_foreign_key("service", $this->retourformulaire)) {
                $form->setType("service", "selecthiddenstatic");
            } else {
                $form->setType("service", "select");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_debut", "date");
            } else {
                $form->setType("om_validite_debut", "hiddenstaticdate");
            }
            if ($this->f->isAccredited(array($this->table."_modifier_validite", $this->table, ))) {
                $form->setType("om_validite_fin", "date");
            } else {
                $form->setType("om_validite_fin", "hiddenstaticdate");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion_type", "hiddenstatic");
            $form->setType("code", "hiddenstatic");
            $form->setType("libelle", "hiddenstatic");
            $form->setType("lieu_adresse_ligne1", "hiddenstatic");
            $form->setType("lieu_adresse_ligne2", "hiddenstatic");
            $form->setType("lieu_salle", "hiddenstatic");
            $form->setType("heure", "hiddenstatic");
            $form->setType("listes_de_diffusion", "hiddenstatic");
            $form->setType("modele_courriel_convoquer", "hiddenstatic");
            $form->setType("modele_courriel_cloturer", "hiddenstatic");
            $form->setType("commission", "hiddenstatic");
            $form->setType("president", "selectstatic");
            $form->setType("modele_ordre_du_jour", "selectstatic");
            $form->setType("modele_compte_rendu_global", "selectstatic");
            $form->setType("modele_compte_rendu_specifique", "selectstatic");
            $form->setType("modele_feuille_presence", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("om_validite_debut", "hiddenstatic");
            $form->setType("om_validite_fin", "hiddenstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion_type", "static");
            $form->setType("code", "static");
            $form->setType("libelle", "static");
            $form->setType("lieu_adresse_ligne1", "static");
            $form->setType("lieu_adresse_ligne2", "static");
            $form->setType("lieu_salle", "static");
            $form->setType("heure", "static");
            $form->setType("listes_de_diffusion", "textareastatic");
            $form->setType("modele_courriel_convoquer", "textareastatic");
            $form->setType("modele_courriel_cloturer", "textareastatic");
            $form->setType("commission", "checkboxstatic");
            $form->setType("president", "selectstatic");
            $form->setType("modele_ordre_du_jour", "selectstatic");
            $form->setType("modele_compte_rendu_global", "selectstatic");
            $form->setType("modele_compte_rendu_specifique", "selectstatic");
            $form->setType("modele_feuille_presence", "selectstatic");
            $form->setType("service", "selectstatic");
            $form->setType("om_validite_debut", "datestatic");
            $form->setType("om_validite_fin", "datestatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('president','VerifNum(this)');
        $form->setOnchange('modele_ordre_du_jour','VerifNum(this)');
        $form->setOnchange('modele_compte_rendu_global','VerifNum(this)');
        $form->setOnchange('modele_compte_rendu_specifique','VerifNum(this)');
        $form->setOnchange('modele_feuille_presence','VerifNum(this)');
        $form->setOnchange('service','VerifNum(this)');
        $form->setOnchange('om_validite_debut','fdate(this)');
        $form->setOnchange('om_validite_fin','fdate(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion_type", 11);
        $form->setTaille("code", 10);
        $form->setTaille("libelle", 30);
        $form->setTaille("lieu_adresse_ligne1", 30);
        $form->setTaille("lieu_adresse_ligne2", 30);
        $form->setTaille("lieu_salle", 30);
        $form->setTaille("heure", 10);
        $form->setTaille("listes_de_diffusion", 80);
        $form->setTaille("modele_courriel_convoquer", 80);
        $form->setTaille("modele_courriel_cloturer", 80);
        $form->setTaille("commission", 1);
        $form->setTaille("president", 11);
        $form->setTaille("modele_ordre_du_jour", 11);
        $form->setTaille("modele_compte_rendu_global", 11);
        $form->setTaille("modele_compte_rendu_specifique", 11);
        $form->setTaille("modele_feuille_presence", 11);
        $form->setTaille("service", 11);
        $form->setTaille("om_validite_debut", 12);
        $form->setTaille("om_validite_fin", 12);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion_type", 11);
        $form->setMax("code", 10);
        $form->setMax("libelle", 100);
        $form->setMax("lieu_adresse_ligne1", 150);
        $form->setMax("lieu_adresse_ligne2", 100);
        $form->setMax("lieu_salle", 100);
        $form->setMax("heure", 5);
        $form->setMax("listes_de_diffusion", 6);
        $form->setMax("modele_courriel_convoquer", 6);
        $form->setMax("modele_courriel_cloturer", 6);
        $form->setMax("commission", 1);
        $form->setMax("president", 11);
        $form->setMax("modele_ordre_du_jour", 11);
        $form->setMax("modele_compte_rendu_global", 11);
        $form->setMax("modele_compte_rendu_specifique", 11);
        $form->setMax("modele_feuille_presence", 11);
        $form->setMax("service", 11);
        $form->setMax("om_validite_debut", 12);
        $form->setMax("om_validite_fin", 12);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion_type',_('reunion_type'));
        $form->setLib('code',_('code'));
        $form->setLib('libelle',_('libelle'));
        $form->setLib('lieu_adresse_ligne1',_('lieu_adresse_ligne1'));
        $form->setLib('lieu_adresse_ligne2',_('lieu_adresse_ligne2'));
        $form->setLib('lieu_salle',_('lieu_salle'));
        $form->setLib('heure',_('heure'));
        $form->setLib('listes_de_diffusion',_('listes_de_diffusion'));
        $form->setLib('modele_courriel_convoquer',_('modele_courriel_convoquer'));
        $form->setLib('modele_courriel_cloturer',_('modele_courriel_cloturer'));
        $form->setLib('commission',_('commission'));
        $form->setLib('president',_('president'));
        $form->setLib('modele_ordre_du_jour',_('modele_ordre_du_jour'));
        $form->setLib('modele_compte_rendu_global',_('modele_compte_rendu_global'));
        $form->setLib('modele_compte_rendu_specifique',_('modele_compte_rendu_specifique'));
        $form->setLib('modele_feuille_presence',_('modele_feuille_presence'));
        $form->setLib('service',_('service'));
        $form->setLib('om_validite_debut',_('om_validite_debut'));
        $form->setLib('om_validite_fin',_('om_validite_fin'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // modele_compte_rendu_global
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_global", $sql_modele_compte_rendu_global, $sql_modele_compte_rendu_global_by_id, true);
        // modele_compte_rendu_specifique
        $this->init_select($form, $this->f->db, $maj, null, "modele_compte_rendu_specifique", $sql_modele_compte_rendu_specifique, $sql_modele_compte_rendu_specifique_by_id, true);
        // modele_feuille_presence
        $this->init_select($form, $this->f->db, $maj, null, "modele_feuille_presence", $sql_modele_feuille_presence, $sql_modele_feuille_presence_by_id, true);
        // modele_ordre_du_jour
        $this->init_select($form, $this->f->db, $maj, null, "modele_ordre_du_jour", $sql_modele_ordre_du_jour, $sql_modele_ordre_du_jour_by_id, true);
        // president
        $this->init_select($form, $this->f->db, $maj, null, "president", $sql_president, $sql_president_by_id, true);
        // service
        $this->init_select($form, $this->f->db, $maj, null, "service", $sql_service, $sql_service_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_instance', $this->retourformulaire))
                $form->setVal('president', $idxformulaire);
            if($this->is_in_context_of_foreign_key('service', $this->retourformulaire))
                $form->setVal('service', $idxformulaire);
        }// fin validation
        if ($validation == 0 and $maj == 0) {
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_compte_rendu_global', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_compte_rendu_specifique', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_feuille_presence', $idxformulaire);
            if($this->is_in_context_of_foreign_key('modele_edition', $this->retourformulaire))
                $form->setVal('modele_ordre_du_jour', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    
    /**
     * Methode clesecondaire
     */
    function cleSecondaire($id, &$db = null, $val = array(), $DEBUG = null) {
        // On appelle la methode de la classe parent
        parent::cleSecondaire($id);
        // Verification de la cle secondaire : dossier_instruction_reunion
        $this->rechercheTable($this->f->db, "dossier_instruction_reunion", "reunion_type", $id);
        // Verification de la cle secondaire : reunion
        $this->rechercheTable($this->f->db, "reunion", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_avis
        $this->rechercheTable($this->f->db, "reunion_type_reunion_avis", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_categorie
        $this->rechercheTable($this->f->db, "reunion_type_reunion_categorie", "reunion_type", $id);
        // Verification de la cle secondaire : reunion_type_reunion_instance
        $this->rechercheTable($this->f->db, "reunion_type_reunion_instance", "reunion_type", $id);
    }


}

?>
