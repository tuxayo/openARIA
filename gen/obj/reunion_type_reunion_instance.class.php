<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

require_once "../obj/om_dbform.class.php";

class reunion_type_reunion_instance_gen extends om_dbform {

    var $table = "reunion_type_reunion_instance";
    var $clePrimaire = "reunion_type_reunion_instance";
    var $typeCle = "N";
    var $required_field = array(
        "reunion_type_reunion_instance"
    );
    
    var $foreign_keys_extended = array(
        "reunion_instance" => array("reunion_instance", ),
        "reunion_type" => array("reunion_type", ),
    );



    function setvalF($val = array()) {
        //affectation valeur formulaire
        if (!is_numeric($val['reunion_type_reunion_instance'])) {
            $this->valF['reunion_type_reunion_instance'] = ""; // -> requis
        } else {
            $this->valF['reunion_type_reunion_instance'] = $val['reunion_type_reunion_instance'];
        }
        if (!is_numeric($val['reunion_type'])) {
            $this->valF['reunion_type'] = NULL;
        } else {
            $this->valF['reunion_type'] = $val['reunion_type'];
        }
        if (!is_numeric($val['reunion_instance'])) {
            $this->valF['reunion_instance'] = NULL;
        } else {
            $this->valF['reunion_instance'] = $val['reunion_instance'];
        }
    }

    //=================================================
    //cle primaire automatique [automatic primary key]
    //==================================================

    function setId(&$db = null) {
    //numero automatique
        $this->valF[$this->clePrimaire] = $this->f->db->nextId(DB_PREFIXE.$this->table);
    }

    function setValFAjout($val =  array()) {
    //numero automatique -> pas de controle ajout cle primaire
    }

    function verifierAjout($val = array(), &$db = null) {
    //numero automatique -> pas de verfication de cle primaire
    }

    //==========================
    // Formulaire  [form]
    //==========================
    /**
     *
     */
    function setType(&$form, $maj) {
        // Récupération du mode de l'action
        $crud = $this->get_action_crud($maj);

        // MODE AJOUTER
        if ($maj == 0 || $crud == 'create') {
            $form->setType("reunion_type_reunion_instance", "hidden");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
        }

        // MDOE MODIFIER
        if ($maj == 1 || $crud == 'update') {
            $form->setType("reunion_type_reunion_instance", "hiddenstatic");
            if ($this->is_in_context_of_foreign_key("reunion_type", $this->retourformulaire)) {
                $form->setType("reunion_type", "selecthiddenstatic");
            } else {
                $form->setType("reunion_type", "select");
            }
            if ($this->is_in_context_of_foreign_key("reunion_instance", $this->retourformulaire)) {
                $form->setType("reunion_instance", "selecthiddenstatic");
            } else {
                $form->setType("reunion_instance", "select");
            }
        }

        // MODE SUPPRIMER
        if ($maj == 2 || $crud == 'delete') {
            $form->setType("reunion_type_reunion_instance", "hiddenstatic");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_instance", "selectstatic");
        }

        // MODE CONSULTER
        if ($maj == 3 || $crud == 'read') {
            $form->setType("reunion_type_reunion_instance", "static");
            $form->setType("reunion_type", "selectstatic");
            $form->setType("reunion_instance", "selectstatic");
        }

    }


    function setOnchange(&$form, $maj) {
    //javascript controle client
        $form->setOnchange('reunion_type_reunion_instance','VerifNum(this)');
        $form->setOnchange('reunion_type','VerifNum(this)');
        $form->setOnchange('reunion_instance','VerifNum(this)');
    }
    /**
     * Methode setTaille
     */
    function setTaille(&$form, $maj) {
        $form->setTaille("reunion_type_reunion_instance", 11);
        $form->setTaille("reunion_type", 11);
        $form->setTaille("reunion_instance", 11);
    }

    /**
     * Methode setMax
     */
    function setMax(&$form, $maj) {
        $form->setMax("reunion_type_reunion_instance", 11);
        $form->setMax("reunion_type", 11);
        $form->setMax("reunion_instance", 11);
    }


    function setLib(&$form, $maj) {
    //libelle des champs
        $form->setLib('reunion_type_reunion_instance',_('reunion_type_reunion_instance'));
        $form->setLib('reunion_type',_('reunion_type'));
        $form->setLib('reunion_instance',_('reunion_instance'));
    }
    /**
     *
     */
    function setSelect(&$form, $maj, &$dnu1 = null, $dnu2 = null) {

        // Inclusion du fichier de requêtes
        if (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc.php";
        } elseif (file_exists("../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc")) {
            include "../sql/".OM_DB_PHPTYPE."/".$this->table.".form.inc";
        }

        // reunion_instance
        $this->init_select($form, $this->f->db, $maj, null, "reunion_instance", $sql_reunion_instance, $sql_reunion_instance_by_id, true);
        // reunion_type
        $this->init_select($form, $this->f->db, $maj, null, "reunion_type", $sql_reunion_type, $sql_reunion_type_by_id, true);
    }


    //==================================
    // sous Formulaire 
    //==================================
    

    function setValsousformulaire(&$form, $maj, $validation, $idxformulaire, $retourformulaire, $typeformulaire, &$db = null, $DEBUG = null) {
        $this->retourformulaire = $retourformulaire;
        if($validation == 0) {
            if($this->is_in_context_of_foreign_key('reunion_instance', $this->retourformulaire))
                $form->setVal('reunion_instance', $idxformulaire);
            if($this->is_in_context_of_foreign_key('reunion_type', $this->retourformulaire))
                $form->setVal('reunion_type', $idxformulaire);
        }// fin validation
        $this->set_form_default_values($form, $maj, $validation);
    }// fin setValsousformulaire

    //==================================
    // cle secondaire 
    //==================================
    

}

?>
