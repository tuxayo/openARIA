<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("editions")." -> "._("modeles d'edition");
$tableSelect=DB_PREFIXE."modele_edition";
$champs=array(
    "modele_edition",
    "libelle",
    "description",
    "om_lettretype",
    "om_etat",
    "om_validite_debut",
    "om_validite_fin",
    "code",
    "courrier_type");
//champs select
$sql_courrier_type="SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE))) ORDER BY courrier_type.libelle ASC";
$sql_courrier_type_by_id = "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE courrier_type = <idx>";
$sql_om_etat="SELECT om_etat.om_etat, om_etat.libelle FROM ".DB_PREFIXE."om_etat ORDER BY om_etat.libelle ASC";
$sql_om_etat_by_id = "SELECT om_etat.om_etat, om_etat.libelle FROM ".DB_PREFIXE."om_etat WHERE om_etat = <idx>";
$sql_om_lettretype="SELECT om_lettretype.om_lettretype, om_lettretype.libelle FROM ".DB_PREFIXE."om_lettretype ORDER BY om_lettretype.libelle ASC";
$sql_om_lettretype_by_id = "SELECT om_lettretype.om_lettretype, om_lettretype.libelle FROM ".DB_PREFIXE."om_lettretype WHERE om_lettretype = <idx>";
?>