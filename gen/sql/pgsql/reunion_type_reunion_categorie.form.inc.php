<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("reunion_type_reunion_categorie");
$tableSelect=DB_PREFIXE."reunion_type_reunion_categorie";
$champs=array(
    "reunion_type_reunion_categorie",
    "reunion_type",
    "reunion_categorie");
//champs select
$sql_reunion_categorie="SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_categorie.libelle ASC";
$sql_reunion_categorie_by_id = "SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE reunion_categorie = <idx>";
$sql_reunion_type="SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_id = "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
?>