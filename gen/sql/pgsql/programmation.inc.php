<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("programmation");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."programmation
    LEFT JOIN ".DB_PREFIXE."programmation_etat 
        ON programmation.programmation_etat=programmation_etat.programmation_etat 
    LEFT JOIN ".DB_PREFIXE."service 
        ON programmation.service=service.service ";
// SELECT 
$champAffiche = array(
    'programmation.programmation as "'._("programmation").'"',
    'programmation.annee as "'._("annee").'"',
    'programmation.numero_semaine as "'._("numero_semaine").'"',
    'programmation.version as "'._("version").'"',
    'programmation_etat.libelle as "'._("programmation_etat").'"',
    'to_char(programmation.date_modification ,\'DD/MM/YYYY\') as "'._("date_modification").'"',
    'programmation.convocation_exploitants as "'._("convocation_exploitants").'"',
    'to_char(programmation.date_envoi_ce ,\'DD/MM/YYYY\') as "'._("date_envoi_ce").'"',
    'programmation.convocation_membres as "'._("convocation_membres").'"',
    'to_char(programmation.date_envoi_cm ,\'DD/MM/YYYY\') as "'._("date_envoi_cm").'"',
    'programmation.semaine_annee as "'._("semaine_annee").'"',
    'service.libelle as "'._("service").'"',
    "case programmation.planifier_nouveau when 't' then 'Oui' else 'Non' end as \""._("planifier_nouveau")."\"",
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'programmation.programmation as "'._("programmation").'"',
    'programmation.annee as "'._("annee").'"',
    'programmation.numero_semaine as "'._("numero_semaine").'"',
    'programmation.version as "'._("version").'"',
    'programmation_etat.libelle as "'._("programmation_etat").'"',
    'programmation.convocation_exploitants as "'._("convocation_exploitants").'"',
    'programmation.convocation_membres as "'._("convocation_membres").'"',
    'programmation.semaine_annee as "'._("semaine_annee").'"',
    'service.libelle as "'._("service").'"',
    );
$tri="ORDER BY programmation.annee ASC NULLS LAST";
$edition="programmation";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "programmation_etat" => array("programmation_etat", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - programmation_etat
if (in_array($retourformulaire, $foreign_keys_extended["programmation_etat"])) {
    $selection = " WHERE (programmation.programmation_etat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (programmation.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'visite',
);

?>