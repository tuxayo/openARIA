<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_reunion_r_instance_r_i_membre");
$tableSelect=DB_PREFIXE."lien_reunion_r_instance_r_i_membre";
$champs=array(
    "lien_reunion_r_instance_r_i_membre",
    "reunion",
    "reunion_instance",
    "reunion_instance_membre",
    "observation");
//champs select
$sql_reunion="SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion ORDER BY reunion.libelle ASC";
$sql_reunion_by_id = "SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion WHERE reunion = <idx>";
$sql_reunion_instance="SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance.libelle ASC";
$sql_reunion_instance_by_id = "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE reunion_instance = <idx>";
$sql_reunion_instance_membre="SELECT reunion_instance_membre.reunion_instance_membre, reunion_instance_membre.membre FROM ".DB_PREFIXE."reunion_instance_membre WHERE ((reunion_instance_membre.om_validite_debut IS NULL AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE AND (reunion_instance_membre.om_validite_fin IS NULL OR reunion_instance_membre.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance_membre.membre ASC";
$sql_reunion_instance_membre_by_id = "SELECT reunion_instance_membre.reunion_instance_membre, reunion_instance_membre.membre FROM ".DB_PREFIXE."reunion_instance_membre WHERE reunion_instance_membre = <idx>";
?>