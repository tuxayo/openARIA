<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_essai_realise_analyses");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_essai_realise_analyses
    LEFT JOIN ".DB_PREFIXE."analyses 
        ON lien_essai_realise_analyses.analyses=analyses.analyses 
    LEFT JOIN ".DB_PREFIXE."essai_realise 
        ON lien_essai_realise_analyses.essai_realise=essai_realise.essai_realise ";
// SELECT 
$champAffiche = array(
    'lien_essai_realise_analyses.lien_essai_realise_analyses as "'._("lien_essai_realise_analyses").'"',
    'essai_realise.libelle as "'._("essai_realise").'"',
    'analyses.service as "'._("analyses").'"',
    "case lien_essai_realise_analyses.concluant when 't' then 'Oui' else 'Non' end as \""._("concluant")."\"",
    );
//
$champNonAffiche = array(
    'lien_essai_realise_analyses.complement as "'._("complement").'"',
    );
//
$champRecherche = array(
    'lien_essai_realise_analyses.lien_essai_realise_analyses as "'._("lien_essai_realise_analyses").'"',
    'essai_realise.libelle as "'._("essai_realise").'"',
    'analyses.service as "'._("analyses").'"',
    );
$tri="ORDER BY essai_realise.libelle ASC NULLS LAST";
$edition="lien_essai_realise_analyses";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "analyses" => array("analyses", ),
    "essai_realise" => array("essai_realise", ),
);
// Filtre listing sous formulaire - analyses
if (in_array($retourformulaire, $foreign_keys_extended["analyses"])) {
    $selection = " WHERE (lien_essai_realise_analyses.analyses = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - essai_realise
if (in_array($retourformulaire, $foreign_keys_extended["essai_realise"])) {
    $selection = " WHERE (lien_essai_realise_analyses.essai_realise = ".intval($idxformulaire).") ";
}

?>