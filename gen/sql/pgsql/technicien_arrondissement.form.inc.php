<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("technicien_arrondissement");
$tableSelect=DB_PREFIXE."technicien_arrondissement";
$champs=array(
    "technicien_arrondissement",
    "service",
    "technicien",
    "arrondissement");
//champs select
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
$sql_technicien="SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
$sql_technicien_by_id = "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
?>