<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("editions")." -> "._("types");
$tableSelect=DB_PREFIXE."courrier_type";
$champs=array(
    "courrier_type",
    "code",
    "libelle",
    "description",
    "om_validite_debut",
    "om_validite_fin",
    "courrier_type_categorie",
    "service");
//champs select
$sql_courrier_type_categorie="SELECT courrier_type_categorie.courrier_type_categorie, courrier_type_categorie.libelle FROM ".DB_PREFIXE."courrier_type_categorie WHERE ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY courrier_type_categorie.libelle ASC";
$sql_courrier_type_categorie_by_id = "SELECT courrier_type_categorie.courrier_type_categorie, courrier_type_categorie.libelle FROM ".DB_PREFIXE."courrier_type_categorie WHERE courrier_type_categorie = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>