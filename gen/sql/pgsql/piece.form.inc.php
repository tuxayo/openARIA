<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("piece");
$tableSelect=DB_PREFIXE."piece";
$champs=array(
    "piece",
    "etablissement",
    "dossier_coordination",
    "nom",
    "piece_type",
    "uid",
    "om_date_creation",
    "dossier_instruction",
    "date_reception",
    "date_emission",
    "piece_statut",
    "date_butoir",
    "suivi",
    "commentaire_suivi",
    "lu",
    "choix_lien",
    "service");
//champs select
$sql_dossier_coordination="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_piece_statut="SELECT piece_statut.piece_statut, piece_statut.libelle FROM ".DB_PREFIXE."piece_statut WHERE ((piece_statut.om_validite_debut IS NULL AND (piece_statut.om_validite_fin IS NULL OR piece_statut.om_validite_fin > CURRENT_DATE)) OR (piece_statut.om_validite_debut <= CURRENT_DATE AND (piece_statut.om_validite_fin IS NULL OR piece_statut.om_validite_fin > CURRENT_DATE))) ORDER BY piece_statut.libelle ASC";
$sql_piece_statut_by_id = "SELECT piece_statut.piece_statut, piece_statut.libelle FROM ".DB_PREFIXE."piece_statut WHERE piece_statut = <idx>";
$sql_piece_type="SELECT piece_type.piece_type, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE ((piece_type.om_validite_debut IS NULL AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE)) OR (piece_type.om_validite_debut <= CURRENT_DATE AND (piece_type.om_validite_fin IS NULL OR piece_type.om_validite_fin > CURRENT_DATE))) ORDER BY piece_type.libelle ASC";
$sql_piece_type_by_id = "SELECT piece_type.piece_type, piece_type.libelle FROM ".DB_PREFIXE."piece_type WHERE piece_type = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>