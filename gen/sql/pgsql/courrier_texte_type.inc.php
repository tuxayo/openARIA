<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("documents generes")." -> "._("complements");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."courrier_texte_type
    LEFT JOIN ".DB_PREFIXE."courrier_type 
        ON courrier_texte_type.courrier_type=courrier_type.courrier_type ";
// SELECT 
$champAffiche = array(
    'courrier_texte_type.courrier_texte_type as "'._("courrier_texte_type").'"',
    'courrier_texte_type.libelle as "'._("libelle").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(courrier_texte_type.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(courrier_texte_type.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'courrier_texte_type.contenu_om_html as "'._("contenu_om_html").'"',
    'courrier_texte_type.om_validite_debut as "'._("om_validite_debut").'"',
    'courrier_texte_type.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'courrier_texte_type.courrier_texte_type as "'._("courrier_texte_type").'"',
    'courrier_texte_type.libelle as "'._("libelle").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    );
$tri="ORDER BY courrier_texte_type.libelle ASC NULLS LAST";
$edition="courrier_texte_type";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier_type" => array("courrier_type", ),
);
// Filtre listing sous formulaire - courrier_type
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type"])) {
    $selection = " WHERE (courrier_texte_type.courrier_type = ".intval($idxformulaire).")  AND ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((courrier_texte_type.om_validite_debut IS NULL AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)) OR (courrier_texte_type.om_validite_debut <= CURRENT_DATE AND (courrier_texte_type.om_validite_fin IS NULL OR courrier_texte_type.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

?>