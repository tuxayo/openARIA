<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("dossier_coordination");
$tableSelect=DB_PREFIXE."dossier_coordination";
$champs=array(
    "dossier_coordination",
    "etablissement",
    "libelle",
    "dossier_coordination_type",
    "date_demande",
    "date_butoir",
    "dossier_autorisation_ads",
    "dossier_instruction_ads",
    "a_qualifier",
    "etablissement_type",
    "etablissement_categorie",
    "erp",
    "dossier_cloture",
    "contraintes_urba_om_html",
    "etablissement_locaux_sommeil",
    "references_cadastrales",
    "dossier_coordination_parent",
    "description",
    "dossier_instruction_secu",
    "dossier_instruction_acc",
    "autorite_police_encours",
    "date_cloture",
    "geolocalise",
    "interface_referentiel_ads",
    "enjeu_erp",
    "depot_de_piece",
    "enjeu_ads",
    "geom_point",
    "geom_emprise");
//champs select
$sql_dossier_coordination_parent="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_parent_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
$sql_dossier_coordination_type="SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE ((dossier_coordination_type.om_validite_debut IS NULL AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE)) OR (dossier_coordination_type.om_validite_debut <= CURRENT_DATE AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE))) ORDER BY dossier_coordination_type.libelle ASC";
$sql_dossier_coordination_type_by_id = "SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE dossier_coordination_type = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
$sql_etablissement_categorie_by_id = "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
$sql_etablissement_type="SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
$sql_etablissement_type_by_id = "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
?>