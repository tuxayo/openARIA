<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("courrier");
$tableSelect=DB_PREFIXE."courrier";
$champs=array(
    "courrier",
    "etablissement",
    "dossier_coordination",
    "dossier_instruction",
    "complement1_om_html",
    "complement2_om_html",
    "finalise",
    "om_fichier_finalise_courrier",
    "om_fichier_signe_courrier",
    "om_date_creation",
    "date_finalisation",
    "date_envoi_signature",
    "date_retour_signature",
    "date_envoi_controle_legalite",
    "date_retour_controle_legalite",
    "date_envoi_rar",
    "date_retour_rar",
    "code_barres",
    "date_envoi_mail_om_fichier_finalise_courrier",
    "date_envoi_mail_om_fichier_signe_courrier",
    "mailing",
    "courrier_parent",
    "courrier_joint",
    "modele_edition",
    "signataire",
    "courrier_type",
    "proces_verbal",
    "visite",
    "arrete_numero");
//champs select
$sql_courrier_joint="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_joint_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
$sql_courrier_parent="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_parent_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
$sql_courrier_type="SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE))) ORDER BY courrier_type.libelle ASC";
$sql_courrier_type_by_id = "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE courrier_type = <idx>";
$sql_dossier_coordination="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_modele_edition="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_edition_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_proces_verbal="SELECT proces_verbal.proces_verbal, proces_verbal.numero FROM ".DB_PREFIXE."proces_verbal ORDER BY proces_verbal.numero ASC";
$sql_proces_verbal_by_id = "SELECT proces_verbal.proces_verbal, proces_verbal.numero FROM ".DB_PREFIXE."proces_verbal WHERE proces_verbal = <idx>";
$sql_signataire="SELECT signataire.signataire, signataire.nom FROM ".DB_PREFIXE."signataire WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE))) ORDER BY signataire.nom ASC";
$sql_signataire_by_id = "SELECT signataire.signataire, signataire.nom FROM ".DB_PREFIXE."signataire WHERE signataire = <idx>";
$sql_visite="SELECT visite.visite, visite.visite_motif_annulation FROM ".DB_PREFIXE."visite ORDER BY visite.visite_motif_annulation ASC";
$sql_visite_by_id = "SELECT visite.visite, visite.visite_motif_annulation FROM ".DB_PREFIXE."visite WHERE visite = <idx>";
?>