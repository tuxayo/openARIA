<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("autorite_police");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."autorite_police
    LEFT JOIN ".DB_PREFIXE."autorite_police_decision 
        ON autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision 
    LEFT JOIN ".DB_PREFIXE."autorite_police_motif 
        ON autorite_police.autorite_police_motif=autorite_police_motif.autorite_police_motif 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON autorite_police.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion3 
        ON autorite_police.dossier_instruction_reunion=dossier_instruction_reunion3.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion4 
        ON autorite_police.dossier_instruction_reunion_prochain=dossier_instruction_reunion4.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON autorite_police.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_police.service=service.service ";
// SELECT 
$champAffiche = array(
    'autorite_police.autorite_police as "'._("autorite_police").'"',
    'autorite_police_decision.libelle as "'._("autorite_police_decision").'"',
    'to_char(autorite_police.date_decision ,\'DD/MM/YYYY\') as "'._("date_decision").'"',
    'autorite_police.delai as "'._("delai").'"',
    'autorite_police_motif.libelle as "'._("autorite_police_motif").'"',
    "case autorite_police.cloture when 't' then 'Oui' else 'Non' end as \""._("cloture")."\"",
    'to_char(autorite_police.date_notification ,\'DD/MM/YYYY\') as "'._("date_notification").'"',
    'to_char(autorite_police.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'service.libelle as "'._("service").'"',
    'dossier_instruction_reunion3.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_instruction_reunion4.dossier_instruction as "'._("dossier_instruction_reunion_prochain").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'autorite_police.autorite_police as "'._("autorite_police").'"',
    'autorite_police_decision.libelle as "'._("autorite_police_decision").'"',
    'autorite_police.delai as "'._("delai").'"',
    'autorite_police_motif.libelle as "'._("autorite_police_motif").'"',
    'service.libelle as "'._("service").'"',
    'dossier_instruction_reunion3.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_instruction_reunion4.dossier_instruction as "'._("dossier_instruction_reunion_prochain").'"',
    );
$tri="ORDER BY autorite_police_decision.libelle ASC NULLS LAST";
$edition="autorite_police";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "autorite_police_decision" => array("autorite_police_decision", ),
    "autorite_police_motif" => array("autorite_police_motif", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    "dossier_instruction_reunion" => array("dossier_instruction_reunion", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - autorite_police_decision
if (in_array($retourformulaire, $foreign_keys_extended["autorite_police_decision"])) {
    $selection = " WHERE (autorite_police.autorite_police_decision = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - autorite_police_motif
if (in_array($retourformulaire, $foreign_keys_extended["autorite_police_motif"])) {
    $selection = " WHERE (autorite_police.autorite_police_motif = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (autorite_police.dossier_coordination = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction_reunion
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction_reunion"])) {
    $selection = " WHERE (autorite_police.dossier_instruction_reunion = ".intval($idxformulaire)." OR autorite_police.dossier_instruction_reunion_prochain = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (autorite_police.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (autorite_police.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_autorite_police_courrier',
);

?>