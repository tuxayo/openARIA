<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("dossier_instruction_reunion");
$tableSelect=DB_PREFIXE."dossier_instruction_reunion";
$champs=array(
    "dossier_instruction_reunion",
    "dossier_instruction",
    "reunion_type",
    "reunion_type_categorie",
    "date_souhaitee",
    "motivation",
    "proposition_avis",
    "proposition_avis_complement",
    "reunion",
    "ordre",
    "avis",
    "avis_complement",
    "avis_motivation");
//champs select
$sql_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_proposition_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_proposition_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_reunion="SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion ORDER BY reunion.libelle ASC";
$sql_reunion_by_id = "SELECT reunion.reunion, reunion.libelle FROM ".DB_PREFIXE."reunion WHERE reunion = <idx>";
$sql_reunion_type="SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_id = "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
$sql_reunion_type_categorie="SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_categorie.libelle ASC";
$sql_reunion_type_categorie_by_id = "SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle FROM ".DB_PREFIXE."reunion_categorie WHERE reunion_categorie = <idx>";
?>