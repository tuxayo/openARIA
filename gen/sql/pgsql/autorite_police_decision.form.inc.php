<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("autorites de police")." -> "._("types");
$tableSelect=DB_PREFIXE."autorite_police_decision";
$champs=array(
    "autorite_police_decision",
    "code",
    "libelle",
    "description",
    "service",
    "suivi_delai",
    "avis",
    "etablissement_etat",
    "delai",
    "om_validite_debut",
    "om_validite_fin",
    "type_arrete",
    "arrete_reglementaire",
    "arrete_notification",
    "arrete_publication",
    "arrete_temporaire",
    "nomenclature_actes_nature",
    "nomenclature_actes_matiere_niv1",
    "nomenclature_actes_matiere_niv2");
//champs select
$sql_etablissement_etat="SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE ((etablissement_etat.om_validite_debut IS NULL AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)) OR (etablissement_etat.om_validite_debut <= CURRENT_DATE AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_etat.statut_administratif ASC";
$sql_etablissement_etat_by_id = "SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE etablissement_etat = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>