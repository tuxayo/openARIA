<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("analyses");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."analyses
    LEFT JOIN ".DB_PREFIXE."derogation_scda 
        ON analyses.acc_derogation_scda=derogation_scda.derogation_scda 
    LEFT JOIN ".DB_PREFIXE."analyses_type 
        ON analyses.analyses_type=analyses_type.analyses_type 
    LEFT JOIN ".DB_PREFIXE."proces_verbal 
        ON analyses.dernier_pv=proces_verbal.proces_verbal 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON analyses.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition4 
        ON analyses.modele_edition_compte_rendu=modele_edition4.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition5 
        ON analyses.modele_edition_compte_rendu=modele_edition5.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition6 
        ON analyses.modele_edition_compte_rendu=modele_edition6.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition7 
        ON analyses.modele_edition_compte_rendu=modele_edition7.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition8 
        ON analyses.modele_edition_proces_verbal=modele_edition8.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition9 
        ON analyses.modele_edition_proces_verbal=modele_edition9.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition10 
        ON analyses.modele_edition_proces_verbal=modele_edition10.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition11 
        ON analyses.modele_edition_proces_verbal=modele_edition11.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition12 
        ON analyses.modele_edition_rapport=modele_edition12.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition13 
        ON analyses.modele_edition_rapport=modele_edition13.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition14 
        ON analyses.modele_edition_rapport=modele_edition14.modele_edition 
    LEFT JOIN ".DB_PREFIXE."modele_edition as modele_edition15 
        ON analyses.modele_edition_rapport=modele_edition15.modele_edition 
    LEFT JOIN ".DB_PREFIXE."reunion_avis 
        ON analyses.reunion_avis=reunion_avis.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."service 
        ON analyses.service=service.service ";
// SELECT 
$champAffiche = array(
    'analyses.analyses as "'._("analyses").'"',
    'service.libelle as "'._("service").'"',
    'analyses.analyses_etat as "'._("analyses_etat").'"',
    'analyses_type.libelle as "'._("analyses_type").'"',
    'reunion_avis.libelle as "'._("reunion_avis").'"',
    'analyses.avis_complement as "'._("avis_complement").'"',
    'analyses.si_effectif_public as "'._("si_effectif_public").'"',
    'analyses.si_effectif_personnel as "'._("si_effectif_personnel").'"',
    'analyses.si_type_ssi as "'._("si_type_ssi").'"',
    'analyses.si_type_alarme as "'._("si_type_alarme").'"',
    "case analyses.si_conformite_l16 when 't' then 'Oui' else 'Non' end as \""._("si_conformite_l16")."\"",
    "case analyses.si_alimentation_remplacement when 't' then 'Oui' else 'Non' end as \""._("si_alimentation_remplacement")."\"",
    "case analyses.si_service_securite when 't' then 'Oui' else 'Non' end as \""._("si_service_securite")."\"",
    'analyses.si_personnel_jour as "'._("si_personnel_jour").'"',
    'analyses.si_personnel_nuit as "'._("si_personnel_nuit").'"',
    "case analyses.acc_handicap_mental when 't' then 'Oui' else 'Non' end as \""._("acc_handicap_mental")."\"",
    "case analyses.acc_handicap_auditif when 't' then 'Oui' else 'Non' end as \""._("acc_handicap_auditif")."\"",
    'analyses.acc_places_stationnement_amenagees as "'._("acc_places_stationnement_amenagees").'"',
    "case analyses.acc_elevateur when 't' then 'Oui' else 'Non' end as \""._("acc_elevateur")."\"",
    "case analyses.acc_handicap_physique when 't' then 'Oui' else 'Non' end as \""._("acc_handicap_physique")."\"",
    "case analyses.acc_ascenseur when 't' then 'Oui' else 'Non' end as \""._("acc_ascenseur")."\"",
    "case analyses.acc_handicap_visuel when 't' then 'Oui' else 'Non' end as \""._("acc_handicap_visuel")."\"",
    "case analyses.acc_boucle_magnetique when 't' then 'Oui' else 'Non' end as \""._("acc_boucle_magnetique")."\"",
    'analyses.acc_chambres_amenagees as "'._("acc_chambres_amenagees").'"',
    "case analyses.acc_douche when 't' then 'Oui' else 'Non' end as \""._("acc_douche")."\"",
    'derogation_scda.libelle as "'._("acc_derogation_scda").'"',
    "case analyses.acc_sanitaire when 't' then 'Oui' else 'Non' end as \""._("acc_sanitaire")."\"",
    'analyses.acc_places_assises_public as "'._("acc_places_assises_public").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'modele_edition12.libelle as "'._("modele_edition_rapport").'"',
    'modele_edition4.libelle as "'._("modele_edition_compte_rendu").'"',
    'modele_edition8.libelle as "'._("modele_edition_proces_verbal").'"',
    "case analyses.modifiee_sans_gen when 't' then 'Oui' else 'Non' end as \""._("modifiee_sans_gen")."\"",
    'proces_verbal.numero as "'._("dernier_pv").'"',
    'analyses.dec1 as "'._("dec1").'"',
    'analyses.delai1 as "'._("delai1").'"',
    'analyses.dec2 as "'._("dec2").'"',
    'analyses.delai2 as "'._("delai2").'"',
    );
//
$champNonAffiche = array(
    'analyses.objet as "'._("objet").'"',
    'analyses.descriptif_etablissement_om_html as "'._("descriptif_etablissement_om_html").'"',
    'analyses.reglementation_applicable_om_html as "'._("reglementation_applicable_om_html").'"',
    'analyses.compte_rendu_om_html as "'._("compte_rendu_om_html").'"',
    'analyses.document_presente_pendant_om_html as "'._("document_presente_pendant_om_html").'"',
    'analyses.document_presente_apres_om_html as "'._("document_presente_apres_om_html").'"',
    'analyses.observation_om_html as "'._("observation_om_html").'"',
    );
//
$champRecherche = array(
    'analyses.analyses as "'._("analyses").'"',
    'service.libelle as "'._("service").'"',
    'analyses.analyses_etat as "'._("analyses_etat").'"',
    'analyses_type.libelle as "'._("analyses_type").'"',
    'reunion_avis.libelle as "'._("reunion_avis").'"',
    'analyses.avis_complement as "'._("avis_complement").'"',
    'analyses.si_effectif_public as "'._("si_effectif_public").'"',
    'analyses.si_effectif_personnel as "'._("si_effectif_personnel").'"',
    'analyses.si_type_ssi as "'._("si_type_ssi").'"',
    'analyses.si_type_alarme as "'._("si_type_alarme").'"',
    'analyses.si_personnel_jour as "'._("si_personnel_jour").'"',
    'analyses.si_personnel_nuit as "'._("si_personnel_nuit").'"',
    'analyses.acc_places_stationnement_amenagees as "'._("acc_places_stationnement_amenagees").'"',
    'analyses.acc_chambres_amenagees as "'._("acc_chambres_amenagees").'"',
    'derogation_scda.libelle as "'._("acc_derogation_scda").'"',
    'analyses.acc_places_assises_public as "'._("acc_places_assises_public").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'modele_edition12.libelle as "'._("modele_edition_rapport").'"',
    'modele_edition4.libelle as "'._("modele_edition_compte_rendu").'"',
    'modele_edition8.libelle as "'._("modele_edition_proces_verbal").'"',
    'proces_verbal.numero as "'._("dernier_pv").'"',
    'analyses.dec1 as "'._("dec1").'"',
    'analyses.delai1 as "'._("delai1").'"',
    'analyses.dec2 as "'._("dec2").'"',
    'analyses.delai2 as "'._("delai2").'"',
    );
$tri="ORDER BY service.libelle ASC NULLS LAST";
$edition="analyses";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "derogation_scda" => array("derogation_scda", ),
    "analyses_type" => array("analyses_type", ),
    "proces_verbal" => array("proces_verbal", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "modele_edition" => array("modele_edition", ),
    "reunion_avis" => array("reunion_avis", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - derogation_scda
if (in_array($retourformulaire, $foreign_keys_extended["derogation_scda"])) {
    $selection = " WHERE (analyses.acc_derogation_scda = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - analyses_type
if (in_array($retourformulaire, $foreign_keys_extended["analyses_type"])) {
    $selection = " WHERE (analyses.analyses_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - proces_verbal
if (in_array($retourformulaire, $foreign_keys_extended["proces_verbal"])) {
    $selection = " WHERE (analyses.dernier_pv = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (analyses.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (analyses.modele_edition_compte_rendu = ".intval($idxformulaire)." OR analyses.modele_edition_proces_verbal = ".intval($idxformulaire)." OR analyses.modele_edition_rapport = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_avis
if (in_array($retourformulaire, $foreign_keys_extended["reunion_avis"])) {
    $selection = " WHERE (analyses.reunion_avis = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (analyses.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_essai_realise_analyses',
    'prescription',
);

?>