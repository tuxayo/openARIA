<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("contact");
$tableSelect=DB_PREFIXE."contact";
$champs=array(
    "contact",
    "etablissement",
    "contact_type",
    "civilite",
    "nom",
    "prenom",
    "titre",
    "telephone",
    "mobile",
    "fax",
    "courriel",
    "om_validite_debut",
    "om_validite_fin",
    "adresse_numero",
    "adresse_numero2",
    "adresse_voie",
    "adresse_complement",
    "adresse_cp",
    "adresse_ville",
    "lieu_dit",
    "boite_postale",
    "cedex",
    "pays",
    "qualite",
    "denomination",
    "raison_sociale",
    "siret",
    "categorie_juridique",
    "reception_convocation",
    "service",
    "reception_programmation",
    "reception_commission");
//champs select
$sql_civilite="SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite ORDER BY contact_civilite.libelle ASC";
$sql_civilite_by_id = "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite WHERE contact_civilite = <idx>";
$sql_contact_type="SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE))) ORDER BY contact_type.libelle ASC";
$sql_contact_type_by_id = "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE contact_type = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>