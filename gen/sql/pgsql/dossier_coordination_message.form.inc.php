<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("dossier_coordination_message");
$tableSelect=DB_PREFIXE."dossier_coordination_message";
$champs=array(
    "dossier_coordination_message",
    "categorie",
    "dossier_coordination",
    "type",
    "emetteur",
    "date_emission",
    "contenu",
    "contenu_json",
    "si_cadre_lu",
    "si_technicien_lu",
    "si_mode_lecture",
    "acc_cadre_lu",
    "acc_technicien_lu",
    "acc_mode_lecture");
//champs select
$sql_dossier_coordination="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
?>