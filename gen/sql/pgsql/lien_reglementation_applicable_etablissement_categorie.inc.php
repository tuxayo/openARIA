<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_reglementation_applicable_etablissement_categorie");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_reglementation_applicable_etablissement_categorie
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON lien_reglementation_applicable_etablissement_categorie.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."reglementation_applicable 
        ON lien_reglementation_applicable_etablissement_categorie.reglementation_applicable=reglementation_applicable.reglementation_applicable ";
// SELECT 
$champAffiche = array(
    'lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie as "'._("lien_reglementation_applicable_etablissement_categorie").'"',
    'reglementation_applicable.libelle as "'._("reglementation_applicable").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie as "'._("lien_reglementation_applicable_etablissement_categorie").'"',
    'reglementation_applicable.libelle as "'._("reglementation_applicable").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
    );
$tri="ORDER BY reglementation_applicable.libelle ASC NULLS LAST";
$edition="lien_reglementation_applicable_etablissement_categorie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_categorie" => array("etablissement_categorie", ),
    "reglementation_applicable" => array("reglementation_applicable", ),
);
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (lien_reglementation_applicable_etablissement_categorie.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reglementation_applicable
if (in_array($retourformulaire, $foreign_keys_extended["reglementation_applicable"])) {
    $selection = " WHERE (lien_reglementation_applicable_etablissement_categorie.reglementation_applicable = ".intval($idxformulaire).") ";
}

?>