<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("reunion");
$tableSelect=DB_PREFIXE."reunion";
$champs=array(
    "reunion",
    "code",
    "reunion_type",
    "libelle",
    "date_reunion",
    "heure_reunion",
    "lieu_adresse_ligne1",
    "lieu_adresse_ligne2",
    "lieu_salle",
    "listes_de_diffusion",
    "participants",
    "numerotation",
    "date_convocation",
    "reunion_cloture",
    "date_cloture",
    "om_fichier_reunion_odj",
    "om_final_reunion_odj",
    "om_fichier_reunion_cr_global",
    "om_final_reunion_cr_global",
    "om_fichier_reunion_cr_global_signe",
    "om_fichier_reunion_cr_par_dossier_signe",
    "planifier_nouveau");
//champs select
$sql_reunion_type="SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_id = "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
?>