<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("autorite_police");
$tableSelect=DB_PREFIXE."autorite_police";
$champs=array(
    "autorite_police",
    "autorite_police_decision",
    "date_decision",
    "delai",
    "autorite_police_motif",
    "cloture",
    "date_notification",
    "date_butoir",
    "service",
    "dossier_instruction_reunion",
    "dossier_coordination",
    "etablissement",
    "dossier_instruction_reunion_prochain");
//champs select
$sql_autorite_police_decision="SELECT autorite_police_decision.autorite_police_decision, autorite_police_decision.libelle FROM ".DB_PREFIXE."autorite_police_decision WHERE ((autorite_police_decision.om_validite_debut IS NULL AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE)) OR (autorite_police_decision.om_validite_debut <= CURRENT_DATE AND (autorite_police_decision.om_validite_fin IS NULL OR autorite_police_decision.om_validite_fin > CURRENT_DATE))) ORDER BY autorite_police_decision.libelle ASC";
$sql_autorite_police_decision_by_id = "SELECT autorite_police_decision.autorite_police_decision, autorite_police_decision.libelle FROM ".DB_PREFIXE."autorite_police_decision WHERE autorite_police_decision = <idx>";
$sql_autorite_police_motif="SELECT autorite_police_motif.autorite_police_motif, autorite_police_motif.libelle FROM ".DB_PREFIXE."autorite_police_motif WHERE ((autorite_police_motif.om_validite_debut IS NULL AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE)) OR (autorite_police_motif.om_validite_debut <= CURRENT_DATE AND (autorite_police_motif.om_validite_fin IS NULL OR autorite_police_motif.om_validite_fin > CURRENT_DATE))) ORDER BY autorite_police_motif.libelle ASC";
$sql_autorite_police_motif_by_id = "SELECT autorite_police_motif.autorite_police_motif, autorite_police_motif.libelle FROM ".DB_PREFIXE."autorite_police_motif WHERE autorite_police_motif = <idx>";
$sql_dossier_coordination="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
$sql_dossier_instruction_reunion="SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY dossier_instruction_reunion.dossier_instruction ASC";
$sql_dossier_instruction_reunion_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";
$sql_dossier_instruction_reunion_prochain="SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY dossier_instruction_reunion.dossier_instruction ASC";
$sql_dossier_instruction_reunion_prochain_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>