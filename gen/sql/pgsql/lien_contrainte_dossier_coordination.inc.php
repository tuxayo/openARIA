<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_contrainte_dossier_coordination");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_contrainte_dossier_coordination
    LEFT JOIN ".DB_PREFIXE."contrainte 
        ON lien_contrainte_dossier_coordination.contrainte=contrainte.contrainte 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON lien_contrainte_dossier_coordination.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination as "'._("lien_contrainte_dossier_coordination").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'contrainte.libelle as "'._("contrainte").'"',
    "case lien_contrainte_dossier_coordination.recuperee when 't' then 'Oui' else 'Non' end as \""._("recuperee")."\"",
    );
//
$champNonAffiche = array(
    'lien_contrainte_dossier_coordination.texte_complete as "'._("texte_complete").'"',
    );
//
$champRecherche = array(
    'lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination as "'._("lien_contrainte_dossier_coordination").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'contrainte.libelle as "'._("contrainte").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="lien_contrainte_dossier_coordination";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contrainte" => array("contrainte", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
);
// Filtre listing sous formulaire - contrainte
if (in_array($retourformulaire, $foreign_keys_extended["contrainte"])) {
    $selection = " WHERE (lien_contrainte_dossier_coordination.contrainte = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (lien_contrainte_dossier_coordination.dossier_coordination = ".intval($idxformulaire).") ";
}

?>