<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("visites")." -> "._("durees");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."visite_duree";
// SELECT 
$champAffiche = array(
    'visite_duree.visite_duree as "'._("visite_duree").'"',
    'visite_duree.libelle as "'._("libelle").'"',
    'visite_duree.duree as "'._("duree").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'visite_duree.visite_duree as "'._("visite_duree").'"',
    'visite_duree.libelle as "'._("libelle").'"',
    'visite_duree.duree as "'._("duree").'"',
    );
$tri="ORDER BY visite_duree.libelle ASC NULLS LAST";
$edition="visite_duree";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

?>