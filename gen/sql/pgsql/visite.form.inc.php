<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("visite");
$tableSelect=DB_PREFIXE."visite";
$champs=array(
    "visite",
    "visite_motif_annulation",
    "visite_etat",
    "dossier_instruction",
    "acteur",
    "programmation",
    "date_creation",
    "date_annulation",
    "observation",
    "programmation_version_creation",
    "programmation_version_annulation",
    "heure_debut",
    "heure_fin",
    "convocation_exploitants",
    "date_visite",
    "a_poursuivre",
    "courrier_convocation_exploitants",
    "courrier_annulation",
    "programmation_version_modification");
//champs select
$sql_acteur="SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
$sql_acteur_by_id = "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
$sql_courrier_annulation="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_annulation_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
$sql_courrier_convocation_exploitants="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_convocation_exploitants_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_programmation="SELECT programmation.programmation, programmation.annee FROM ".DB_PREFIXE."programmation ORDER BY programmation.annee ASC";
$sql_programmation_by_id = "SELECT programmation.programmation, programmation.annee FROM ".DB_PREFIXE."programmation WHERE programmation = <idx>";
$sql_visite_etat="SELECT visite_etat.visite_etat, visite_etat.libelle FROM ".DB_PREFIXE."visite_etat WHERE ((visite_etat.om_validite_debut IS NULL AND (visite_etat.om_validite_fin IS NULL OR visite_etat.om_validite_fin > CURRENT_DATE)) OR (visite_etat.om_validite_debut <= CURRENT_DATE AND (visite_etat.om_validite_fin IS NULL OR visite_etat.om_validite_fin > CURRENT_DATE))) ORDER BY visite_etat.libelle ASC";
$sql_visite_etat_by_id = "SELECT visite_etat.visite_etat, visite_etat.libelle FROM ".DB_PREFIXE."visite_etat WHERE visite_etat = <idx>";
$sql_visite_motif_annulation="SELECT visite_motif_annulation.visite_motif_annulation, visite_motif_annulation.libelle FROM ".DB_PREFIXE."visite_motif_annulation WHERE ((visite_motif_annulation.om_validite_debut IS NULL AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE)) OR (visite_motif_annulation.om_validite_debut <= CURRENT_DATE AND (visite_motif_annulation.om_validite_fin IS NULL OR visite_motif_annulation.om_validite_fin > CURRENT_DATE))) ORDER BY visite_motif_annulation.libelle ASC";
$sql_visite_motif_annulation_by_id = "SELECT visite_motif_annulation.visite_motif_annulation, visite_motif_annulation.libelle FROM ".DB_PREFIXE."visite_motif_annulation WHERE visite_motif_annulation = <idx>";
?>