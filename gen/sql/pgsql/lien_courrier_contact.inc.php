<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_courrier_contact");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_courrier_contact
    LEFT JOIN ".DB_PREFIXE."contact 
        ON lien_courrier_contact.contact=contact.contact 
    LEFT JOIN ".DB_PREFIXE."courrier 
        ON lien_courrier_contact.courrier=courrier.courrier ";
// SELECT 
$champAffiche = array(
    'lien_courrier_contact.lien_courrier_contact as "'._("lien_courrier_contact").'"',
    'courrier.etablissement as "'._("courrier").'"',
    'contact.etablissement as "'._("contact").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'lien_courrier_contact.lien_courrier_contact as "'._("lien_courrier_contact").'"',
    'courrier.etablissement as "'._("courrier").'"',
    'contact.etablissement as "'._("contact").'"',
    );
$tri="ORDER BY courrier.etablissement ASC NULLS LAST";
$edition="lien_courrier_contact";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contact" => array("contact", "contact_institutionnel", "contact_contexte_dossier_coordination", ),
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
);
// Filtre listing sous formulaire - contact
if (in_array($retourformulaire, $foreign_keys_extended["contact"])) {
    $selection = " WHERE (lien_courrier_contact.contact = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (lien_courrier_contact.courrier = ".intval($idxformulaire).") ";
}

?>