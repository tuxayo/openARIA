<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("etablissements")." -> "._("periodicites de visites");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."periodicite_visites
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON periodicite_visites.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON periodicite_visites.etablissement_type=etablissement_type.etablissement_type ";
// SELECT 
$champAffiche = array(
    'periodicite_visites.periodicite_visites as "'._("periodicite_visites").'"',
    'periodicite_visites.periodicite as "'._("periodicite").'"',
    'etablissement_type.libelle as "'._("etablissement_type").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
    "case periodicite_visites.avec_locaux_sommeil when 't' then 'Oui' else 'Non' end as \""._("avec_locaux_sommeil")."\"",
    "case periodicite_visites.sans_locaux_sommeil when 't' then 'Oui' else 'Non' end as \""._("sans_locaux_sommeil")."\"",
    );
//
$champNonAffiche = array(
    'periodicite_visites.commentaire as "'._("commentaire").'"',
    );
//
$champRecherche = array(
    'periodicite_visites.periodicite_visites as "'._("periodicite_visites").'"',
    'periodicite_visites.periodicite as "'._("periodicite").'"',
    'etablissement_type.libelle as "'._("etablissement_type").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
    );
$tri="ORDER BY periodicite_visites.periodicite ASC NULLS LAST";
$edition="periodicite_visites";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement_categorie" => array("etablissement_categorie", ),
    "etablissement_type" => array("etablissement_type", ),
);
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (periodicite_visites.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (periodicite_visites.etablissement_type = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
);

?>