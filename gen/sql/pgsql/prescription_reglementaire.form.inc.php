<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("analyses")." -> "._("prescriptions");
$tableSelect=DB_PREFIXE."prescription_reglementaire";
$champs=array(
    "prescription_reglementaire",
    "service",
    "tete_de_chapitre1",
    "tete_de_chapitre2",
    "libelle",
    "description_pr_om_html",
    "defavorable",
    "om_validite_debut",
    "om_validite_fin");
//champs select
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>