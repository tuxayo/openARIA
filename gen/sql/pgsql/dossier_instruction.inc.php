<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_instruction");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."autorite_competente 
        ON dossier_instruction.autorite_competente=autorite_competente.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."service 
        ON dossier_instruction.service=service.service 
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON dossier_instruction.technicien=acteur.acteur ";
// SELECT 
$champAffiche = array(
    'dossier_instruction.dossier_instruction as "'._("dossier_instruction").'"',
    'dossier_instruction.libelle as "'._("libelle").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'acteur.nom_prenom as "'._("technicien").'"',
    'service.libelle as "'._("service").'"',
    "case dossier_instruction.a_qualifier when 't' then 'Oui' else 'Non' end as \""._("a_qualifier")."\"",
    "case dossier_instruction.incompletude when 't' then 'Oui' else 'Non' end as \""._("incompletude")."\"",
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    "case dossier_instruction.dossier_cloture when 't' then 'Oui' else 'Non' end as \""._("dossier_cloture")."\"",
    "case dossier_instruction.prioritaire when 't' then 'Oui' else 'Non' end as \""._("prioritaire")."\"",
    'dossier_instruction.statut as "'._("statut").'"',
    'to_char(dossier_instruction.date_cloture ,\'DD/MM/YYYY\') as "'._("date_cloture").'"',
    'to_char(dossier_instruction.date_ouverture ,\'DD/MM/YYYY\') as "'._("date_ouverture").'"',
    );
//
$champNonAffiche = array(
    'dossier_instruction.piece_attendue as "'._("piece_attendue").'"',
    'dossier_instruction.description as "'._("description").'"',
    'dossier_instruction.notes as "'._("notes").'"',
    );
//
$champRecherche = array(
    'dossier_instruction.dossier_instruction as "'._("dossier_instruction").'"',
    'dossier_instruction.libelle as "'._("libelle").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'acteur.nom_prenom as "'._("technicien").'"',
    'service.libelle as "'._("service").'"',
    'autorite_competente.libelle as "'._("autorite_competente").'"',
    'dossier_instruction.statut as "'._("statut").'"',
    );
$tri="ORDER BY dossier_instruction.libelle ASC NULLS LAST";
$edition="dossier_instruction";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "autorite_competente" => array("autorite_competente", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    "service" => array("service", ),
    "acteur" => array("acteur", ),
);
// Filtre listing sous formulaire - autorite_competente
if (in_array($retourformulaire, $foreign_keys_extended["autorite_competente"])) {
    $selection = " WHERE (dossier_instruction.autorite_competente = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_instruction.dossier_coordination = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (dossier_instruction.service = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (dossier_instruction.technicien = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'analyses',
    'courrier',
    'dossier_instruction_reunion',
    'etablissement_unite',
    'piece',
    'proces_verbal',
    'visite',
);

?>