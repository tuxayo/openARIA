<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("reunion");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."reunion
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON reunion.reunion_type=reunion_type.reunion_type ";
// SELECT 
$champAffiche = array(
    'reunion.reunion as "'._("reunion").'"',
    'reunion.code as "'._("code").'"',
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion.libelle as "'._("libelle").'"',
    'to_char(reunion.date_reunion ,\'DD/MM/YYYY\') as "'._("date_reunion").'"',
    'reunion.lieu_adresse_ligne1 as "'._("lieu_adresse_ligne1").'"',
    'reunion.lieu_adresse_ligne2 as "'._("lieu_adresse_ligne2").'"',
    "case reunion.reunion_cloture when 't' then 'Oui' else 'Non' end as \""._("reunion_cloture")."\"",
    );
//
$champNonAffiche = array(
    'reunion.heure_reunion as "'._("heure_reunion").'"',
    'reunion.lieu_salle as "'._("lieu_salle").'"',
    'reunion.listes_de_diffusion as "'._("listes_de_diffusion").'"',
    'reunion.participants as "'._("participants").'"',
    'reunion.numerotation as "'._("numerotation").'"',
    'reunion.date_convocation as "'._("date_convocation").'"',
    'reunion.date_cloture as "'._("date_cloture").'"',
    'reunion.om_fichier_reunion_odj as "'._("om_fichier_reunion_odj").'"',
    'reunion.om_final_reunion_odj as "'._("om_final_reunion_odj").'"',
    'reunion.om_fichier_reunion_cr_global as "'._("om_fichier_reunion_cr_global").'"',
    'reunion.om_final_reunion_cr_global as "'._("om_final_reunion_cr_global").'"',
    'reunion.om_fichier_reunion_cr_global_signe as "'._("om_fichier_reunion_cr_global_signe").'"',
    'reunion.om_fichier_reunion_cr_par_dossier_signe as "'._("om_fichier_reunion_cr_par_dossier_signe").'"',
    'reunion.planifier_nouveau as "'._("planifier_nouveau").'"',
    );
//
$champRecherche = array(
    'reunion.reunion as "'._("reunion").'"',
    'reunion.code as "'._("code").'"',
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion.libelle as "'._("libelle").'"',
    'reunion.lieu_adresse_ligne1 as "'._("lieu_adresse_ligne1").'"',
    'reunion.lieu_adresse_ligne2 as "'._("lieu_adresse_ligne2").'"',
    );
$tri="ORDER BY reunion.libelle ASC NULLS LAST";
$edition="reunion";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_type" => array("reunion_type", ),
);
// Filtre listing sous formulaire - reunion_type
if (in_array($retourformulaire, $foreign_keys_extended["reunion_type"])) {
    $selection = " WHERE (reunion.reunion_type = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'dossier_instruction_reunion',
    'lien_reunion_r_instance_r_i_membre',
);

?>