<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("adresses")." -> "._("voies");
$tableSelect=DB_PREFIXE."voie";
$champs=array(
    "voie",
    "libelle",
    "rivoli",
    "om_validite_debut",
    "om_validite_fin",
    "om_collectivite",
    "id_voie_ref");
//champs select
$sql_om_collectivite="SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite ORDER BY om_collectivite.libelle ASC";
$sql_om_collectivite_by_id = "SELECT om_collectivite.om_collectivite, om_collectivite.libelle FROM ".DB_PREFIXE."om_collectivite WHERE om_collectivite = <idx>";
?>