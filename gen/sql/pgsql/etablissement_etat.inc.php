<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("etablissements")." -> "._("etats");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."etablissement_etat";
// SELECT 
$champAffiche = array(
    'etablissement_etat.etablissement_etat as "'._("etablissement_etat").'"',
    'etablissement_etat.statut_administratif as "'._("statut_administratif").'"',
    'etablissement_etat.description as "'._("description").'"',
    'etablissement_etat.code as "'._("code").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(etablissement_etat.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(etablissement_etat.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'etablissement_etat.om_validite_debut as "'._("om_validite_debut").'"',
    'etablissement_etat.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'etablissement_etat.etablissement_etat as "'._("etablissement_etat").'"',
    'etablissement_etat.statut_administratif as "'._("statut_administratif").'"',
    'etablissement_etat.description as "'._("description").'"',
    'etablissement_etat.code as "'._("code").'"',
    );
$tri="ORDER BY etablissement_etat.statut_administratif ASC NULLS LAST";
$edition="etablissement_etat";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((etablissement_etat.om_validite_debut IS NULL AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)) OR (etablissement_etat.om_validite_debut <= CURRENT_DATE AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((etablissement_etat.om_validite_debut IS NULL AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)) OR (etablissement_etat.om_validite_debut <= CURRENT_DATE AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'autorite_police_decision',
    //'etablissement',
);

?>