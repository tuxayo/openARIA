<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("editions")." -> "._("categories");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."courrier_type_categorie";
// SELECT 
$champAffiche = array(
    'courrier_type_categorie.courrier_type_categorie as "'._("courrier_type_categorie").'"',
    'courrier_type_categorie.code as "'._("code").'"',
    'courrier_type_categorie.libelle as "'._("libelle").'"',
    'courrier_type_categorie.objet as "'._("objet").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(courrier_type_categorie.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(courrier_type_categorie.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'courrier_type_categorie.description as "'._("description").'"',
    'courrier_type_categorie.om_validite_debut as "'._("om_validite_debut").'"',
    'courrier_type_categorie.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'courrier_type_categorie.courrier_type_categorie as "'._("courrier_type_categorie").'"',
    'courrier_type_categorie.code as "'._("code").'"',
    'courrier_type_categorie.libelle as "'._("libelle").'"',
    'courrier_type_categorie.objet as "'._("objet").'"',
    );
$tri="ORDER BY courrier_type_categorie.libelle ASC NULLS LAST";
$edition="courrier_type_categorie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'courrier_type',
);

?>