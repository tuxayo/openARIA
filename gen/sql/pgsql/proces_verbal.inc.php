<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("proces_verbal");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."proces_verbal
    LEFT JOIN ".DB_PREFIXE."courrier 
        ON proces_verbal.courrier_genere=courrier.courrier 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON proces_verbal.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion 
        ON proces_verbal.dossier_instruction_reunion=dossier_instruction_reunion.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."modele_edition 
        ON proces_verbal.modele_edition=modele_edition.modele_edition 
    LEFT JOIN ".DB_PREFIXE."signataire 
        ON proces_verbal.signataire=signataire.signataire ";
// SELECT 
$champAffiche = array(
    'proces_verbal.proces_verbal as "'._("proces_verbal").'"',
    'proces_verbal.numero as "'._("numero").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'dossier_instruction_reunion.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
    'to_char(proces_verbal.date_redaction ,\'DD/MM/YYYY\') as "'._("date_redaction").'"',
    'signataire.nom as "'._("signataire").'"',
    "case proces_verbal.genere when 't' then 'Oui' else 'Non' end as \""._("genere")."\"",
    'proces_verbal.om_fichier_signe as "'._("om_fichier_signe").'"',
    'courrier.etablissement as "'._("courrier_genere").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'proces_verbal.proces_verbal as "'._("proces_verbal").'"',
    'proces_verbal.numero as "'._("numero").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'dossier_instruction_reunion.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
    'signataire.nom as "'._("signataire").'"',
    'proces_verbal.om_fichier_signe as "'._("om_fichier_signe").'"',
    'courrier.etablissement as "'._("courrier_genere").'"',
    );
$tri="ORDER BY proces_verbal.numero ASC NULLS LAST";
$edition="proces_verbal";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "dossier_instruction_reunion" => array("dossier_instruction_reunion", ),
    "modele_edition" => array("modele_edition", ),
    "signataire" => array("signataire", ),
);
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (proces_verbal.courrier_genere = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (proces_verbal.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction_reunion
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction_reunion"])) {
    $selection = " WHERE (proces_verbal.dossier_instruction_reunion = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (proces_verbal.modele_edition = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - signataire
if (in_array($retourformulaire, $foreign_keys_extended["signataire"])) {
    $selection = " WHERE (proces_verbal.signataire = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'analyses',
    'courrier',
);

?>