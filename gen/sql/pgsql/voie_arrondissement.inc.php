<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("voie_arrondissement");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."voie_arrondissement
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON voie_arrondissement.arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON voie_arrondissement.voie=voie.voie ";
// SELECT 
$champAffiche = array(
    'voie_arrondissement.voie_arrondissement as "'._("voie_arrondissement").'"',
    'voie.libelle as "'._("voie").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'voie_arrondissement.voie_arrondissement as "'._("voie_arrondissement").'"',
    'voie.libelle as "'._("voie").'"',
    'arrondissement.libelle as "'._("arrondissement").'"',
    );
$tri="ORDER BY voie.libelle ASC NULLS LAST";
$edition="voie_arrondissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "arrondissement" => array("arrondissement", ),
    "voie" => array("voie", ),
);
// Filtre listing sous formulaire - arrondissement
if (in_array($retourformulaire, $foreign_keys_extended["arrondissement"])) {
    $selection = " WHERE (voie_arrondissement.arrondissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - voie
if (in_array($retourformulaire, $foreign_keys_extended["voie"])) {
    $selection = " WHERE (voie_arrondissement.voie = ".intval($idxformulaire).") ";
}

?>