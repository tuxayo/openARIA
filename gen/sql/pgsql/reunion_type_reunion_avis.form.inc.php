<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("reunion_type_reunion_avis");
$tableSelect=DB_PREFIXE."reunion_type_reunion_avis";
$champs=array(
    "reunion_type_reunion_avis",
    "reunion_type",
    "reunion_avis");
//champs select
$sql_reunion_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_reunion_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_reunion_type="SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_id = "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
?>