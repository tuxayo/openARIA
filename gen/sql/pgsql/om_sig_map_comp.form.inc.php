<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration")." -> "._("om_sig_map_comp");
$tableSelect=DB_PREFIXE."om_sig_map_comp";
$champs=array(
    "om_sig_map_comp",
    "om_sig_map",
    "libelle",
    "ordre",
    "actif",
    "comp_maj",
    "type_geometrie",
    "comp_table_update",
    "comp_champ",
    "comp_champ_idx",
    "obj_class");
//champs select
$sql_om_sig_map="SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map ORDER BY om_sig_map.libelle ASC";
$sql_om_sig_map_by_id = "SELECT om_sig_map.om_sig_map, om_sig_map.libelle FROM ".DB_PREFIXE."om_sig_map WHERE om_sig_map = <idx>";
?>