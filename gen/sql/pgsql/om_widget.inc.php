<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("tableaux de bord")." -> "._("widgets");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_widget";
// SELECT 
$champAffiche = array(
    'om_widget.om_widget as "'._("om_widget").'"',
    'om_widget.libelle as "'._("libelle").'"',
    'om_widget.type as "'._("type").'"',
    );
//
$champNonAffiche = array(
    'om_widget.lien as "'._("lien").'"',
    'om_widget.texte as "'._("texte").'"',
    'om_widget.script as "'._("script").'"',
    'om_widget.arguments as "'._("arguments").'"',
    );
//
$champRecherche = array(
    'om_widget.om_widget as "'._("om_widget").'"',
    'om_widget.libelle as "'._("libelle").'"',
    'om_widget.type as "'._("type").'"',
    );
$tri="ORDER BY om_widget.libelle ASC NULLS LAST";
$edition="om_widget";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'om_dashboard',
);

?>