<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("lien_reunion_r_instance_r_i_membre");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."lien_reunion_r_instance_r_i_membre
    LEFT JOIN ".DB_PREFIXE."reunion 
        ON lien_reunion_r_instance_r_i_membre.reunion=reunion.reunion 
    LEFT JOIN ".DB_PREFIXE."reunion_instance 
        ON lien_reunion_r_instance_r_i_membre.reunion_instance=reunion_instance.reunion_instance 
    LEFT JOIN ".DB_PREFIXE."reunion_instance_membre 
        ON lien_reunion_r_instance_r_i_membre.reunion_instance_membre=reunion_instance_membre.reunion_instance_membre ";
// SELECT 
$champAffiche = array(
    'lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre as "'._("lien_reunion_r_instance_r_i_membre").'"',
    'reunion.libelle as "'._("reunion").'"',
    'reunion_instance.libelle as "'._("reunion_instance").'"',
    'reunion_instance_membre.membre as "'._("reunion_instance_membre").'"',
    );
//
$champNonAffiche = array(
    'lien_reunion_r_instance_r_i_membre.observation as "'._("observation").'"',
    );
//
$champRecherche = array(
    'lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre as "'._("lien_reunion_r_instance_r_i_membre").'"',
    'reunion.libelle as "'._("reunion").'"',
    'reunion_instance.libelle as "'._("reunion_instance").'"',
    'reunion_instance_membre.membre as "'._("reunion_instance_membre").'"',
    );
$tri="ORDER BY reunion.libelle ASC NULLS LAST";
$edition="lien_reunion_r_instance_r_i_membre";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion" => array("reunion", ),
    "reunion_instance" => array("reunion_instance", ),
    "reunion_instance_membre" => array("reunion_instance_membre", ),
);
// Filtre listing sous formulaire - reunion
if (in_array($retourformulaire, $foreign_keys_extended["reunion"])) {
    $selection = " WHERE (lien_reunion_r_instance_r_i_membre.reunion = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_instance
if (in_array($retourformulaire, $foreign_keys_extended["reunion_instance"])) {
    $selection = " WHERE (lien_reunion_r_instance_r_i_membre.reunion_instance = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_instance_membre
if (in_array($retourformulaire, $foreign_keys_extended["reunion_instance_membre"])) {
    $selection = " WHERE (lien_reunion_r_instance_r_i_membre.reunion_instance_membre = ".intval($idxformulaire).") ";
}

?>