<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_dossier_coordination_contact");
$tableSelect=DB_PREFIXE."lien_dossier_coordination_contact";
$champs=array(
    "lien_dossier_coordination_contact",
    "dossier_coordination",
    "contact");
//champs select
$sql_contact="SELECT contact.contact, contact.etablissement FROM ".DB_PREFIXE."contact WHERE ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE))) ORDER BY contact.etablissement ASC";
$sql_contact_by_id = "SELECT contact.contact, contact.etablissement FROM ".DB_PREFIXE."contact WHERE contact = <idx>";
$sql_dossier_coordination="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
?>