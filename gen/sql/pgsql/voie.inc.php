<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("adresses")." -> "._("voies");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."voie
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON voie.om_collectivite=om_collectivite.om_collectivite ";
// SELECT 
$champAffiche = array(
    'voie.voie as "'._("voie").'"',
    'voie.libelle as "'._("libelle").'"',
    'voie.rivoli as "'._("rivoli").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(voie.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(voie.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'voie.om_validite_debut as "'._("om_validite_debut").'"',
    'voie.om_validite_fin as "'._("om_validite_fin").'"',
    'voie.om_collectivite as "'._("om_collectivite").'"',
    'voie.id_voie_ref as "'._("id_voie_ref").'"',
    );
//
$champRecherche = array(
    'voie.voie as "'._("voie").'"',
    'voie.libelle as "'._("libelle").'"',
    'voie.rivoli as "'._("rivoli").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY voie.libelle ASC NULLS LAST";
$edition="voie";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = " WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
} else {
    // Filtre MONO
    $selection = " WHERE (voie.om_collectivite = '".$_SESSION["collectivite"]."')  AND ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (voie.om_collectivite = ".intval($idxformulaire).")  AND ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
    } else {
        // Filtre MONO
        $selection = " WHERE (voie.om_collectivite = '".$_SESSION["collectivite"]."') AND (voie.om_collectivite = ".intval($idxformulaire).")  AND ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
    }
$where_om_validite = " AND ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
    //'voie_arrondissement',
);

?>