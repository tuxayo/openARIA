<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("gestion des utilisateurs")." -> "._("permissions");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_permission";
// SELECT 
$champAffiche = array(
    'om_permission.om_permission as "'._("om_permission").'"',
    'om_permission.libelle as "'._("libelle").'"',
    'om_permission.type as "'._("type").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'om_permission.om_permission as "'._("om_permission").'"',
    'om_permission.libelle as "'._("libelle").'"',
    'om_permission.type as "'._("type").'"',
    );
$tri="ORDER BY om_permission.libelle ASC NULLS LAST";
$edition="om_permission";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

?>