<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_coordination_message");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_coordination_message
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination_message.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'dossier_coordination_message.dossier_coordination_message as "'._("dossier_coordination_message").'"',
    'dossier_coordination_message.categorie as "'._("categorie").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_coordination_message.type as "'._("type").'"',
    'dossier_coordination_message.emetteur as "'._("emetteur").'"',
    'dossier_coordination_message.date_emission as "'._("date_emission").'"',
    "case dossier_coordination_message.si_cadre_lu when 't' then 'Oui' else 'Non' end as \""._("si_cadre_lu")."\"",
    "case dossier_coordination_message.si_technicien_lu when 't' then 'Oui' else 'Non' end as \""._("si_technicien_lu")."\"",
    'dossier_coordination_message.si_mode_lecture as "'._("si_mode_lecture").'"',
    "case dossier_coordination_message.acc_cadre_lu when 't' then 'Oui' else 'Non' end as \""._("acc_cadre_lu")."\"",
    "case dossier_coordination_message.acc_technicien_lu when 't' then 'Oui' else 'Non' end as \""._("acc_technicien_lu")."\"",
    'dossier_coordination_message.acc_mode_lecture as "'._("acc_mode_lecture").'"',
    );
//
$champNonAffiche = array(
    'dossier_coordination_message.contenu as "'._("contenu").'"',
    'dossier_coordination_message.contenu_json as "'._("contenu_json").'"',
    );
//
$champRecherche = array(
    'dossier_coordination_message.dossier_coordination_message as "'._("dossier_coordination_message").'"',
    'dossier_coordination_message.categorie as "'._("categorie").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_coordination_message.type as "'._("type").'"',
    'dossier_coordination_message.emetteur as "'._("emetteur").'"',
    'dossier_coordination_message.si_mode_lecture as "'._("si_mode_lecture").'"',
    'dossier_coordination_message.acc_mode_lecture as "'._("acc_mode_lecture").'"',
    );
$tri="ORDER BY dossier_coordination_message.categorie ASC NULLS LAST";
$edition="dossier_coordination_message";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination_message.dossier_coordination = ".intval($idxformulaire).") ";
}

?>