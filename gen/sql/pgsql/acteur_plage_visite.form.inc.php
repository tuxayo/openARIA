<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("acteur_plage_visite");
$tableSelect=DB_PREFIXE."acteur_plage_visite";
$champs=array(
    "acteur_plage_visite",
    "acteur",
    "lundi_matin",
    "lundi_apresmidi",
    "mardi_matin",
    "mardi_apresmidi",
    "mercredi_matin",
    "mercredi_apresmidi",
    "jeudi_matin",
    "jeudi_apresmidi",
    "vendredi_matin",
    "vendredi_apresmidi");
//champs select
$sql_acteur="SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
$sql_acteur_by_id = "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
?>