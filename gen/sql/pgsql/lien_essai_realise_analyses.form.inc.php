<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_essai_realise_analyses");
$tableSelect=DB_PREFIXE."lien_essai_realise_analyses";
$champs=array(
    "lien_essai_realise_analyses",
    "essai_realise",
    "analyses",
    "concluant",
    "complement");
//champs select
$sql_analyses="SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses ORDER BY analyses.service ASC";
$sql_analyses_by_id = "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses WHERE analyses = <idx>";
$sql_essai_realise="SELECT essai_realise.essai_realise, essai_realise.libelle FROM ".DB_PREFIXE."essai_realise WHERE ((essai_realise.om_validite_debut IS NULL AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE)) OR (essai_realise.om_validite_debut <= CURRENT_DATE AND (essai_realise.om_validite_fin IS NULL OR essai_realise.om_validite_fin > CURRENT_DATE))) ORDER BY essai_realise.libelle ASC";
$sql_essai_realise_by_id = "SELECT essai_realise.essai_realise, essai_realise.libelle FROM ".DB_PREFIXE."essai_realise WHERE essai_realise = <idx>";
?>