<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("visite");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."visite
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON visite.acteur=acteur.acteur 
    LEFT JOIN ".DB_PREFIXE."courrier as courrier1 
        ON visite.courrier_annulation=courrier1.courrier 
    LEFT JOIN ".DB_PREFIXE."courrier as courrier2 
        ON visite.courrier_convocation_exploitants=courrier2.courrier 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON visite.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."programmation 
        ON visite.programmation=programmation.programmation 
    LEFT JOIN ".DB_PREFIXE."visite_etat 
        ON visite.visite_etat=visite_etat.visite_etat 
    LEFT JOIN ".DB_PREFIXE."visite_motif_annulation 
        ON visite.visite_motif_annulation=visite_motif_annulation.visite_motif_annulation ";
// SELECT 
$champAffiche = array(
    'visite.visite as "'._("visite").'"',
    'visite_motif_annulation.libelle as "'._("visite_motif_annulation").'"',
    'visite_etat.libelle as "'._("visite_etat").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'acteur.nom_prenom as "'._("acteur").'"',
    'programmation.annee as "'._("programmation").'"',
    'to_char(visite.date_creation ,\'DD/MM/YYYY\') as "'._("date_creation").'"',
    'to_char(visite.date_annulation ,\'DD/MM/YYYY\') as "'._("date_annulation").'"',
    'visite.programmation_version_creation as "'._("programmation_version_creation").'"',
    'visite.programmation_version_annulation as "'._("programmation_version_annulation").'"',
    'visite.heure_debut as "'._("heure_debut").'"',
    'visite.heure_fin as "'._("heure_fin").'"',
    'visite.convocation_exploitants as "'._("convocation_exploitants").'"',
    'to_char(visite.date_visite ,\'DD/MM/YYYY\') as "'._("date_visite").'"',
    "case visite.a_poursuivre when 't' then 'Oui' else 'Non' end as \""._("a_poursuivre")."\"",
    'courrier2.etablissement as "'._("courrier_convocation_exploitants").'"',
    'courrier1.etablissement as "'._("courrier_annulation").'"',
    'visite.programmation_version_modification as "'._("programmation_version_modification").'"',
    );
//
$champNonAffiche = array(
    'visite.observation as "'._("observation").'"',
    );
//
$champRecherche = array(
    'visite.visite as "'._("visite").'"',
    'visite_motif_annulation.libelle as "'._("visite_motif_annulation").'"',
    'visite_etat.libelle as "'._("visite_etat").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'acteur.nom_prenom as "'._("acteur").'"',
    'programmation.annee as "'._("programmation").'"',
    'visite.programmation_version_creation as "'._("programmation_version_creation").'"',
    'visite.programmation_version_annulation as "'._("programmation_version_annulation").'"',
    'visite.heure_debut as "'._("heure_debut").'"',
    'visite.heure_fin as "'._("heure_fin").'"',
    'visite.convocation_exploitants as "'._("convocation_exploitants").'"',
    'courrier2.etablissement as "'._("courrier_convocation_exploitants").'"',
    'courrier1.etablissement as "'._("courrier_annulation").'"',
    'visite.programmation_version_modification as "'._("programmation_version_modification").'"',
    );
$tri="ORDER BY visite_motif_annulation.libelle ASC NULLS LAST";
$edition="visite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "acteur" => array("acteur", ),
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "programmation" => array("programmation", ),
    "visite_etat" => array("visite_etat", ),
    "visite_motif_annulation" => array("visite_motif_annulation", ),
);
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (visite.acteur = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (visite.courrier_annulation = ".intval($idxformulaire)." OR visite.courrier_convocation_exploitants = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (visite.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - programmation
if (in_array($retourformulaire, $foreign_keys_extended["programmation"])) {
    $selection = " WHERE (visite.programmation = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - visite_etat
if (in_array($retourformulaire, $foreign_keys_extended["visite_etat"])) {
    $selection = " WHERE (visite.visite_etat = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - visite_motif_annulation
if (in_array($retourformulaire, $foreign_keys_extended["visite_motif_annulation"])) {
    $selection = " WHERE (visite.visite_motif_annulation = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'courrier',
);

?>