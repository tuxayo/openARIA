<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("prescription");
$tableSelect=DB_PREFIXE."prescription";
$champs=array(
    "prescription",
    "analyses",
    "ordre",
    "prescription_reglementaire",
    "pr_description_om_html",
    "pr_defavorable",
    "ps_description_om_html");
//champs select
$sql_analyses="SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses ORDER BY analyses.service ASC";
$sql_analyses_by_id = "SELECT analyses.analyses, analyses.service FROM ".DB_PREFIXE."analyses WHERE analyses = <idx>";
$sql_prescription_reglementaire="SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE))) ORDER BY prescription_reglementaire.libelle ASC";
$sql_prescription_reglementaire_by_id = "SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE prescription_reglementaire = <idx>";
?>