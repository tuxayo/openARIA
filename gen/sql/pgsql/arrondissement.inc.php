<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("adresses")." -> "._("arrondissements");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."arrondissement";
// SELECT 
$champAffiche = array(
    'arrondissement.arrondissement as "'._("arrondissement").'"',
    'arrondissement.code as "'._("code").'"',
    'arrondissement.libelle as "'._("libelle").'"',
    'arrondissement.description as "'._("description").'"',
    'arrondissement.code_impots as "'._("code_impots").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(arrondissement.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(arrondissement.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'arrondissement.om_validite_debut as "'._("om_validite_debut").'"',
    'arrondissement.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'arrondissement.arrondissement as "'._("arrondissement").'"',
    'arrondissement.code as "'._("code").'"',
    'arrondissement.libelle as "'._("libelle").'"',
    'arrondissement.description as "'._("description").'"',
    'arrondissement.code_impots as "'._("code_impots").'"',
    );
$tri="ORDER BY arrondissement.libelle ASC NULLS LAST";
$edition="arrondissement";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'etablissement',
    //'quartier',
    //'technicien_arrondissement',
    //'voie_arrondissement',
);

?>