<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_coordination");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination.dossier_coordination_parent=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
        ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON dossier_coordination.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON dossier_coordination.etablissement_type=etablissement_type.etablissement_type ";
// SELECT 
$champAffiche = array(
    'dossier_coordination.dossier_coordination as "'._("dossier_coordination").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("libelle").'"',
    'dossier_coordination_type.libelle as "'._("dossier_coordination_type").'"',
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    "case dossier_coordination.a_qualifier when 't' then 'Oui' else 'Non' end as \""._("a_qualifier")."\"",
    "case dossier_coordination.erp when 't' then 'Oui' else 'Non' end as \""._("erp")."\"",
    "case dossier_coordination.dossier_cloture when 't' then 'Oui' else 'Non' end as \""._("dossier_cloture")."\"",
    );
//
$champNonAffiche = array(
    'dossier_coordination.dossier_autorisation_ads as "'._("dossier_autorisation_ads").'"',
    'dossier_coordination.dossier_instruction_ads as "'._("dossier_instruction_ads").'"',
    'dossier_coordination.etablissement_type as "'._("etablissement_type").'"',
    'dossier_coordination.etablissement_categorie as "'._("etablissement_categorie").'"',
    'dossier_coordination.contraintes_urba_om_html as "'._("contraintes_urba_om_html").'"',
    'dossier_coordination.etablissement_locaux_sommeil as "'._("etablissement_locaux_sommeil").'"',
    'dossier_coordination.references_cadastrales as "'._("references_cadastrales").'"',
    'dossier_coordination.dossier_coordination_parent as "'._("dossier_coordination_parent").'"',
    'dossier_coordination.description as "'._("description").'"',
    'dossier_coordination.dossier_instruction_secu as "'._("dossier_instruction_secu").'"',
    'dossier_coordination.dossier_instruction_acc as "'._("dossier_instruction_acc").'"',
    'dossier_coordination.autorite_police_encours as "'._("autorite_police_encours").'"',
    'dossier_coordination.date_cloture as "'._("date_cloture").'"',
    'dossier_coordination.geolocalise as "'._("geolocalise").'"',
    'dossier_coordination.interface_referentiel_ads as "'._("interface_referentiel_ads").'"',
    'dossier_coordination.enjeu_erp as "'._("enjeu_erp").'"',
    'dossier_coordination.depot_de_piece as "'._("depot_de_piece").'"',
    'dossier_coordination.enjeu_ads as "'._("enjeu_ads").'"',
    'dossier_coordination.geom_point as "'._("geom_point").'"',
    'dossier_coordination.geom_emprise as "'._("geom_emprise").'"',
    );
//
$champRecherche = array(
    'dossier_coordination.dossier_coordination as "'._("dossier_coordination").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("libelle").'"',
    'dossier_coordination_type.libelle as "'._("dossier_coordination_type").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="dossier_coordination";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    "dossier_coordination_type" => array("dossier_coordination_type", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "etablissement_categorie" => array("etablissement_categorie", ),
    "etablissement_type" => array("etablissement_type", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination.dossier_coordination_parent = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination_type
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination_type"])) {
    $selection = " WHERE (dossier_coordination.dossier_coordination_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (dossier_coordination.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_categorie
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_categorie"])) {
    $selection = " WHERE (dossier_coordination.etablissement_categorie = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement_type
if (in_array($retourformulaire, $foreign_keys_extended["etablissement_type"])) {
    $selection = " WHERE (dossier_coordination.etablissement_type = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'autorite_police',
    'courrier',
    'dossier_coordination',
    'dossier_coordination_message',
    'dossier_coordination_parcelle',
    'dossier_instruction',
    'etablissement',
    'lien_contrainte_dossier_coordination',
    'lien_dossier_coordination_contact',
    'lien_dossier_coordination_etablissement_type',
    'piece',
);

?>