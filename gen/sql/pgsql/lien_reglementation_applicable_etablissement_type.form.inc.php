<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_reglementation_applicable_etablissement_type");
$tableSelect=DB_PREFIXE."lien_reglementation_applicable_etablissement_type";
$champs=array(
    "lien_reglementation_applicable_etablissement_type",
    "reglementation_applicable",
    "etablissement_type");
//champs select
$sql_etablissement_type="SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
$sql_etablissement_type_by_id = "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
$sql_reglementation_applicable="SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE))) ORDER BY reglementation_applicable.libelle ASC";
$sql_reglementation_applicable_by_id = "SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE reglementation_applicable = <idx>";
?>