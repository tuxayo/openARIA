<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("documents generes")." -> "._("complements");
$tableSelect=DB_PREFIXE."courrier_texte_type";
$champs=array(
    "courrier_texte_type",
    "libelle",
    "courrier_type",
    "contenu_om_html",
    "om_validite_debut",
    "om_validite_fin");
//champs select
$sql_courrier_type="SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE))) ORDER BY courrier_type.libelle ASC";
$sql_courrier_type_by_id = "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE courrier_type = <idx>";
?>