<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("etablissement");
$tableSelect=DB_PREFIXE."etablissement";
$champs=array(
    "etablissement",
    "code",
    "libelle",
    "adresse_numero",
    "adresse_numero2",
    "adresse_voie",
    "adresse_complement",
    "lieu_dit",
    "boite_postale",
    "adresse_cp",
    "adresse_ville",
    "adresse_arrondissement",
    "cedex",
    "npai",
    "telephone",
    "fax",
    "etablissement_nature",
    "siret",
    "annee_de_construction",
    "etablissement_statut_juridique",
    "etablissement_tutelle_adm",
    "ref_patrimoine",
    "etablissement_type",
    "etablissement_categorie",
    "etablissement_etat",
    "date_arrete_ouverture",
    "autorite_police_encours",
    "om_validite_debut",
    "om_validite_fin",
    "si_effectif_public",
    "si_effectif_personnel",
    "si_locaux_sommeil",
    "si_periodicite_visites",
    "si_prochaine_visite_periodique_date_previsionnelle",
    "si_visite_duree",
    "si_derniere_visite_periodique_date",
    "si_derniere_visite_date",
    "si_derniere_visite_avis",
    "si_derniere_visite_technicien",
    "si_prochaine_visite_date",
    "si_prochaine_visite_type",
    "acc_derniere_visite_date",
    "acc_derniere_visite_avis",
    "acc_derniere_visite_technicien",
    "acc_consignes_om_html",
    "acc_descriptif_om_html",
    "si_consignes_om_html",
    "si_descriptif_om_html",
    "si_autorite_competente_visite",
    "si_autorite_competente_plan",
    "si_dernier_plan_avis",
    "si_type_alarme",
    "si_type_ssi",
    "si_conformite_l16",
    "si_alimentation_remplacement",
    "si_service_securite",
    "si_personnel_jour",
    "si_personnel_nuit",
    "references_cadastrales",
    "dossier_coordination_periodique",
    "geolocalise",
    "geom_point",
    "geom_emprise");
//champs select
$sql_acc_derniere_visite_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_acc_derniere_visite_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_acc_derniere_visite_technicien="SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
$sql_acc_derniere_visite_technicien_by_id = "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
$sql_adresse_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.libelle ASC";
$sql_adresse_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_adresse_voie="SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE))) ORDER BY voie.libelle ASC";
$sql_adresse_voie_by_id = "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE voie = <idx>";
$sql_dossier_coordination_periodique="SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination ORDER BY dossier_coordination.libelle ASC";
$sql_dossier_coordination_periodique_by_id = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination = <idx>";
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
$sql_etablissement_categorie_by_id = "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
$sql_etablissement_etat="SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE ((etablissement_etat.om_validite_debut IS NULL AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE)) OR (etablissement_etat.om_validite_debut <= CURRENT_DATE AND (etablissement_etat.om_validite_fin IS NULL OR etablissement_etat.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_etat.statut_administratif ASC";
$sql_etablissement_etat_by_id = "SELECT etablissement_etat.etablissement_etat, etablissement_etat.statut_administratif FROM ".DB_PREFIXE."etablissement_etat WHERE etablissement_etat = <idx>";
$sql_etablissement_nature="SELECT etablissement_nature.etablissement_nature, etablissement_nature.nature FROM ".DB_PREFIXE."etablissement_nature WHERE ((etablissement_nature.om_validite_debut IS NULL AND (etablissement_nature.om_validite_fin IS NULL OR etablissement_nature.om_validite_fin > CURRENT_DATE)) OR (etablissement_nature.om_validite_debut <= CURRENT_DATE AND (etablissement_nature.om_validite_fin IS NULL OR etablissement_nature.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_nature.nature ASC";
$sql_etablissement_nature_by_id = "SELECT etablissement_nature.etablissement_nature, etablissement_nature.nature FROM ".DB_PREFIXE."etablissement_nature WHERE etablissement_nature = <idx>";
$sql_etablissement_statut_juridique="SELECT etablissement_statut_juridique.etablissement_statut_juridique, etablissement_statut_juridique.libelle FROM ".DB_PREFIXE."etablissement_statut_juridique WHERE ((etablissement_statut_juridique.om_validite_debut IS NULL AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE)) OR (etablissement_statut_juridique.om_validite_debut <= CURRENT_DATE AND (etablissement_statut_juridique.om_validite_fin IS NULL OR etablissement_statut_juridique.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_statut_juridique.libelle ASC";
$sql_etablissement_statut_juridique_by_id = "SELECT etablissement_statut_juridique.etablissement_statut_juridique, etablissement_statut_juridique.libelle FROM ".DB_PREFIXE."etablissement_statut_juridique WHERE etablissement_statut_juridique = <idx>";
$sql_etablissement_tutelle_adm="SELECT etablissement_tutelle_adm.etablissement_tutelle_adm, etablissement_tutelle_adm.libelle FROM ".DB_PREFIXE."etablissement_tutelle_adm WHERE ((etablissement_tutelle_adm.om_validite_debut IS NULL AND (etablissement_tutelle_adm.om_validite_fin IS NULL OR etablissement_tutelle_adm.om_validite_fin > CURRENT_DATE)) OR (etablissement_tutelle_adm.om_validite_debut <= CURRENT_DATE AND (etablissement_tutelle_adm.om_validite_fin IS NULL OR etablissement_tutelle_adm.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_tutelle_adm.libelle ASC";
$sql_etablissement_tutelle_adm_by_id = "SELECT etablissement_tutelle_adm.etablissement_tutelle_adm, etablissement_tutelle_adm.libelle FROM ".DB_PREFIXE."etablissement_tutelle_adm WHERE etablissement_tutelle_adm = <idx>";
$sql_etablissement_type="SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
$sql_etablissement_type_by_id = "SELECT etablissement_type.etablissement_type, etablissement_type.libelle FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";
$sql_si_autorite_competente_plan="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
$sql_si_autorite_competente_plan_by_id = "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
$sql_si_autorite_competente_visite="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente ORDER BY autorite_competente.libelle ASC";
$sql_si_autorite_competente_visite_by_id = "SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente WHERE autorite_competente = <idx>";
$sql_si_dernier_plan_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_si_dernier_plan_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_si_derniere_visite_avis="SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_avis.libelle ASC";
$sql_si_derniere_visite_avis_by_id = "SELECT reunion_avis.reunion_avis, reunion_avis.libelle FROM ".DB_PREFIXE."reunion_avis WHERE reunion_avis = <idx>";
$sql_si_derniere_visite_technicien="SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE ((acteur.om_validite_debut IS NULL AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE)) OR (acteur.om_validite_debut <= CURRENT_DATE AND (acteur.om_validite_fin IS NULL OR acteur.om_validite_fin > CURRENT_DATE))) ORDER BY acteur.nom_prenom ASC";
$sql_si_derniere_visite_technicien_by_id = "SELECT acteur.acteur, acteur.nom_prenom FROM ".DB_PREFIXE."acteur WHERE acteur = <idx>";
$sql_si_periodicite_visites="SELECT periodicite_visites.periodicite_visites, periodicite_visites.periodicite FROM ".DB_PREFIXE."periodicite_visites ORDER BY periodicite_visites.periodicite ASC";
$sql_si_periodicite_visites_by_id = "SELECT periodicite_visites.periodicite_visites, periodicite_visites.periodicite FROM ".DB_PREFIXE."periodicite_visites WHERE periodicite_visites = <idx>";
$sql_si_prochaine_visite_type="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_si_prochaine_visite_type_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE analyses_type = <idx>";
$sql_si_visite_duree="SELECT visite_duree.visite_duree, visite_duree.libelle FROM ".DB_PREFIXE."visite_duree ORDER BY visite_duree.libelle ASC";
$sql_si_visite_duree_by_id = "SELECT visite_duree.visite_duree, visite_duree.libelle FROM ".DB_PREFIXE."visite_duree WHERE visite_duree = <idx>";
?>