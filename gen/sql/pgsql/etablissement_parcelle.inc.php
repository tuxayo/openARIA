<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("etablissement_parcelle");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."etablissement_parcelle
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON etablissement_parcelle.etablissement=etablissement.etablissement ";
// SELECT 
$champAffiche = array(
    'etablissement_parcelle.etablissement_parcelle as "'._("etablissement_parcelle").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'etablissement_parcelle.ref_cadastre as "'._("ref_cadastre").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'etablissement_parcelle.etablissement_parcelle as "'._("etablissement_parcelle").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'etablissement_parcelle.ref_cadastre as "'._("ref_cadastre").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="etablissement_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
);
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (etablissement_parcelle.etablissement = ".intval($idxformulaire).") ";
}

?>