<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("reunions")." -> "._("types");
$tableSelect=DB_PREFIXE."reunion_type";
$champs=array(
    "reunion_type",
    "code",
    "libelle",
    "lieu_adresse_ligne1",
    "lieu_adresse_ligne2",
    "lieu_salle",
    "heure",
    "listes_de_diffusion",
    "modele_courriel_convoquer",
    "modele_courriel_cloturer",
    "commission",
    "president",
    "modele_ordre_du_jour",
    "modele_compte_rendu_global",
    "modele_compte_rendu_specifique",
    "modele_feuille_presence",
    "service",
    "om_validite_debut",
    "om_validite_fin");
//champs select
$sql_modele_compte_rendu_global="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_compte_rendu_global_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_modele_compte_rendu_specifique="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_compte_rendu_specifique_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_modele_feuille_presence="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_feuille_presence_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_modele_ordre_du_jour="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_ordre_du_jour_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_president="SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance.libelle ASC";
$sql_president_by_id = "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE reunion_instance = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>