<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("contraintes")." -> "._("listing");
$tableSelect=DB_PREFIXE."contrainte";
$champs=array(
    "contrainte",
    "id_referentiel",
    "nature",
    "groupe",
    "sousgroupe",
    "libelle",
    "texte",
    "texte_surcharge",
    "ordre_d_affichage",
    "lie_a_un_referentiel",
    "om_validite_debut",
    "om_validite_fin");
?>