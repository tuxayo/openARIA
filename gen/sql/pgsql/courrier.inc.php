<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("courrier");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."courrier
    LEFT JOIN ".DB_PREFIXE."courrier as courrier0 
        ON courrier.courrier_joint=courrier0.courrier 
    LEFT JOIN ".DB_PREFIXE."courrier as courrier1 
        ON courrier.courrier_parent=courrier1.courrier 
    LEFT JOIN ".DB_PREFIXE."courrier_type 
        ON courrier.courrier_type=courrier_type.courrier_type 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON courrier.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON courrier.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON courrier.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."modele_edition 
        ON courrier.modele_edition=modele_edition.modele_edition 
    LEFT JOIN ".DB_PREFIXE."proces_verbal 
        ON courrier.proces_verbal=proces_verbal.proces_verbal 
    LEFT JOIN ".DB_PREFIXE."signataire 
        ON courrier.signataire=signataire.signataire 
    LEFT JOIN ".DB_PREFIXE."visite 
        ON courrier.visite=visite.visite ";
// SELECT 
$champAffiche = array(
    'courrier.courrier as "'._("courrier").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    "case courrier.finalise when 't' then 'Oui' else 'Non' end as \""._("finalise")."\"",
    'courrier.om_fichier_finalise_courrier as "'._("om_fichier_finalise_courrier").'"',
    'courrier.om_fichier_signe_courrier as "'._("om_fichier_signe_courrier").'"',
    'to_char(courrier.om_date_creation ,\'DD/MM/YYYY\') as "'._("om_date_creation").'"',
    'to_char(courrier.date_finalisation ,\'DD/MM/YYYY\') as "'._("date_finalisation").'"',
    'to_char(courrier.date_envoi_signature ,\'DD/MM/YYYY\') as "'._("date_envoi_signature").'"',
    'to_char(courrier.date_retour_signature ,\'DD/MM/YYYY\') as "'._("date_retour_signature").'"',
    'to_char(courrier.date_envoi_controle_legalite ,\'DD/MM/YYYY\') as "'._("date_envoi_controle_legalite").'"',
    'to_char(courrier.date_retour_controle_legalite ,\'DD/MM/YYYY\') as "'._("date_retour_controle_legalite").'"',
    'to_char(courrier.date_envoi_rar ,\'DD/MM/YYYY\') as "'._("date_envoi_rar").'"',
    'to_char(courrier.date_retour_rar ,\'DD/MM/YYYY\') as "'._("date_retour_rar").'"',
    'courrier.code_barres as "'._("code_barres").'"',
    'to_char(courrier.date_envoi_mail_om_fichier_finalise_courrier ,\'DD/MM/YYYY\') as "'._("date_envoi_mail_om_fichier_finalise_courrier").'"',
    'to_char(courrier.date_envoi_mail_om_fichier_signe_courrier ,\'DD/MM/YYYY\') as "'._("date_envoi_mail_om_fichier_signe_courrier").'"',
    "case courrier.mailing when 't' then 'Oui' else 'Non' end as \""._("mailing")."\"",
    'courrier1.etablissement as "'._("courrier_parent").'"',
    'courrier0.etablissement as "'._("courrier_joint").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
    'signataire.nom as "'._("signataire").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    'proces_verbal.numero as "'._("proces_verbal").'"',
    'visite.visite_motif_annulation as "'._("visite").'"',
    'courrier.arrete_numero as "'._("arrete_numero").'"',
    );
//
$champNonAffiche = array(
    'courrier.complement1_om_html as "'._("complement1_om_html").'"',
    'courrier.complement2_om_html as "'._("complement2_om_html").'"',
    );
//
$champRecherche = array(
    'courrier.courrier as "'._("courrier").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'courrier.om_fichier_finalise_courrier as "'._("om_fichier_finalise_courrier").'"',
    'courrier.om_fichier_signe_courrier as "'._("om_fichier_signe_courrier").'"',
    'courrier.code_barres as "'._("code_barres").'"',
    'courrier1.etablissement as "'._("courrier_parent").'"',
    'courrier0.etablissement as "'._("courrier_joint").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
    'signataire.nom as "'._("signataire").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    'proces_verbal.numero as "'._("proces_verbal").'"',
    'visite.visite_motif_annulation as "'._("visite").'"',
    'courrier.arrete_numero as "'._("arrete_numero").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="courrier";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier" => array("courrier", "courrier_a_editer", "courrier_attente_signature", "courrier_attente_retour_ar", ),
    "courrier_type" => array("courrier_type", ),
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "modele_edition" => array("modele_edition", ),
    "proces_verbal" => array("proces_verbal", ),
    "signataire" => array("signataire", ),
    "visite" => array("visite", ),
);
// Filtre listing sous formulaire - courrier
if (in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    $selection = " WHERE (courrier.courrier_joint = ".intval($idxformulaire)." OR courrier.courrier_parent = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - courrier_type
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type"])) {
    $selection = " WHERE (courrier.courrier_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (courrier.dossier_coordination = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (courrier.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (courrier.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - modele_edition
if (in_array($retourformulaire, $foreign_keys_extended["modele_edition"])) {
    $selection = " WHERE (courrier.modele_edition = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - proces_verbal
if (in_array($retourformulaire, $foreign_keys_extended["proces_verbal"])) {
    $selection = " WHERE (courrier.proces_verbal = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - signataire
if (in_array($retourformulaire, $foreign_keys_extended["signataire"])) {
    $selection = " WHERE (courrier.signataire = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - visite
if (in_array($retourformulaire, $foreign_keys_extended["visite"])) {
    $selection = " WHERE (courrier.visite = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'courrier',
    'lien_autorite_police_courrier',
    'lien_courrier_contact',
    'proces_verbal',
    'visite',
);

?>