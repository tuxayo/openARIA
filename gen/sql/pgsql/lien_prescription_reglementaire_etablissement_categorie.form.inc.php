<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_prescription_reglementaire_etablissement_categorie");
$tableSelect=DB_PREFIXE."lien_prescription_reglementaire_etablissement_categorie";
$champs=array(
    "lien_prescription_reglementaire_etablissement_categorie",
    "prescription_reglementaire",
    "etablissement_categorie");
//champs select
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
$sql_etablissement_categorie_by_id = "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
$sql_prescription_reglementaire="SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE ((prescription_reglementaire.om_validite_debut IS NULL AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE)) OR (prescription_reglementaire.om_validite_debut <= CURRENT_DATE AND (prescription_reglementaire.om_validite_fin IS NULL OR prescription_reglementaire.om_validite_fin > CURRENT_DATE))) ORDER BY prescription_reglementaire.libelle ASC";
$sql_prescription_reglementaire_by_id = "SELECT prescription_reglementaire.prescription_reglementaire, prescription_reglementaire.libelle FROM ".DB_PREFIXE."prescription_reglementaire WHERE prescription_reglementaire = <idx>";
?>