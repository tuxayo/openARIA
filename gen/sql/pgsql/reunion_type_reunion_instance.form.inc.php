<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("reunion_type_reunion_instance");
$tableSelect=DB_PREFIXE."reunion_type_reunion_instance";
$champs=array(
    "reunion_type_reunion_instance",
    "reunion_type",
    "reunion_instance");
//champs select
$sql_reunion_instance="SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_instance.libelle ASC";
$sql_reunion_instance_by_id = "SELECT reunion_instance.reunion_instance, reunion_instance.libelle FROM ".DB_PREFIXE."reunion_instance WHERE reunion_instance = <idx>";
$sql_reunion_type="SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE ((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))) ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_id = "SELECT reunion_type.reunion_type, reunion_type.libelle FROM ".DB_PREFIXE."reunion_type WHERE reunion_type = <idx>";
?>