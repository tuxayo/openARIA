<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("programmation");
$tableSelect=DB_PREFIXE."programmation";
$champs=array(
    "programmation",
    "annee",
    "numero_semaine",
    "version",
    "programmation_etat",
    "date_modification",
    "convocation_exploitants",
    "date_envoi_ce",
    "convocation_membres",
    "date_envoi_cm",
    "semaine_annee",
    "service",
    "planifier_nouveau");
//champs select
$sql_programmation_etat="SELECT programmation_etat.programmation_etat, programmation_etat.libelle FROM ".DB_PREFIXE."programmation_etat WHERE ((programmation_etat.om_validite_debut IS NULL AND (programmation_etat.om_validite_fin IS NULL OR programmation_etat.om_validite_fin > CURRENT_DATE)) OR (programmation_etat.om_validite_debut <= CURRENT_DATE AND (programmation_etat.om_validite_fin IS NULL OR programmation_etat.om_validite_fin > CURRENT_DATE))) ORDER BY programmation_etat.libelle ASC";
$sql_programmation_etat_by_id = "SELECT programmation_etat.programmation_etat, programmation_etat.libelle FROM ".DB_PREFIXE."programmation_etat WHERE programmation_etat = <idx>";
$sql_service="SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE ((service.om_validite_debut IS NULL AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE)) OR (service.om_validite_debut <= CURRENT_DATE AND (service.om_validite_fin IS NULL OR service.om_validite_fin > CURRENT_DATE))) ORDER BY service.libelle ASC";
$sql_service_by_id = "SELECT service.service, service.libelle FROM ".DB_PREFIXE."service WHERE service = <idx>";
?>