<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("contacts")." -> "._("civilites");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."contact_civilite";
// SELECT 
$champAffiche = array(
    'contact_civilite.contact_civilite as "'._("contact_civilite").'"',
    'contact_civilite.libelle as "'._("libelle").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'contact_civilite.contact_civilite as "'._("contact_civilite").'"',
    'contact_civilite.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY contact_civilite.libelle ASC NULLS LAST";
$edition="contact_civilite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'contact',
    //'signataire',
);

?>