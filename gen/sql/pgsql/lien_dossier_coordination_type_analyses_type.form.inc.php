<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_dossier_coordination_type_analyses_type");
$tableSelect=DB_PREFIXE."lien_dossier_coordination_type_analyses_type";
$champs=array(
    "lien_dossier_coordination_type_analyses_type",
    "dossier_coordination_type",
    "analyses_type",
    "service");
//champs select
$sql_analyses_type="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_analyses_type_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE analyses_type = <idx>";
$sql_dossier_coordination_type="SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE ((dossier_coordination_type.om_validite_debut IS NULL AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE)) OR (dossier_coordination_type.om_validite_debut <= CURRENT_DATE AND (dossier_coordination_type.om_validite_fin IS NULL OR dossier_coordination_type.om_validite_fin > CURRENT_DATE))) ORDER BY dossier_coordination_type.libelle ASC";
$sql_dossier_coordination_type_by_id = "SELECT dossier_coordination_type.dossier_coordination_type, dossier_coordination_type.libelle FROM ".DB_PREFIXE."dossier_coordination_type WHERE dossier_coordination_type = <idx>";
?>