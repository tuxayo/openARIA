<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("proces_verbal");
$tableSelect=DB_PREFIXE."proces_verbal";
$champs=array(
    "proces_verbal",
    "numero",
    "dossier_instruction",
    "dossier_instruction_reunion",
    "modele_edition",
    "date_redaction",
    "signataire",
    "genere",
    "om_fichier_signe",
    "courrier_genere");
//champs select
$sql_courrier_genere="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_genere_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_dossier_instruction_reunion="SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY dossier_instruction_reunion.dossier_instruction ASC";
$sql_dossier_instruction_reunion_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, dossier_instruction_reunion.dossier_instruction FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";
$sql_modele_edition="SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) ORDER BY modele_edition.libelle ASC";
$sql_modele_edition_by_id = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE modele_edition = <idx>";
$sql_signataire="SELECT signataire.signataire, signataire.nom FROM ".DB_PREFIXE."signataire WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE))) ORDER BY signataire.nom ASC";
$sql_signataire_by_id = "SELECT signataire.signataire, signataire.nom FROM ".DB_PREFIXE."signataire WHERE signataire = <idx>";
?>