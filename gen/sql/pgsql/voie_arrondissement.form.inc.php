<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("voie_arrondissement");
$tableSelect=DB_PREFIXE."voie_arrondissement";
$champs=array(
    "voie_arrondissement",
    "voie",
    "arrondissement");
//champs select
$sql_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) ORDER BY arrondissement.libelle ASC";
$sql_arrondissement_by_id = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE arrondissement = <idx>";
$sql_voie="SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE ((voie.om_validite_debut IS NULL AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE)) OR (voie.om_validite_debut <= CURRENT_DATE AND (voie.om_validite_fin IS NULL OR voie.om_validite_fin > CURRENT_DATE))) ORDER BY voie.libelle ASC";
$sql_voie_by_id = "SELECT voie.voie, voie.libelle FROM ".DB_PREFIXE."voie WHERE voie = <idx>";
?>