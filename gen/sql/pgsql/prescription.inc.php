<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("prescription");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."prescription
    LEFT JOIN ".DB_PREFIXE."analyses 
        ON prescription.analyses=analyses.analyses 
    LEFT JOIN ".DB_PREFIXE."prescription_reglementaire 
        ON prescription.prescription_reglementaire=prescription_reglementaire.prescription_reglementaire ";
// SELECT 
$champAffiche = array(
    'prescription.prescription as "'._("prescription").'"',
    'analyses.service as "'._("analyses").'"',
    'prescription.ordre as "'._("ordre").'"',
    'prescription_reglementaire.libelle as "'._("prescription_reglementaire").'"',
    "case prescription.pr_defavorable when 't' then 'Oui' else 'Non' end as \""._("pr_defavorable")."\"",
    );
//
$champNonAffiche = array(
    'prescription.pr_description_om_html as "'._("pr_description_om_html").'"',
    'prescription.ps_description_om_html as "'._("ps_description_om_html").'"',
    );
//
$champRecherche = array(
    'prescription.prescription as "'._("prescription").'"',
    'analyses.service as "'._("analyses").'"',
    'prescription.ordre as "'._("ordre").'"',
    'prescription_reglementaire.libelle as "'._("prescription_reglementaire").'"',
    );
$tri="ORDER BY analyses.service ASC NULLS LAST";
$edition="prescription";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "analyses" => array("analyses", ),
    "prescription_reglementaire" => array("prescription_reglementaire", ),
);
// Filtre listing sous formulaire - analyses
if (in_array($retourformulaire, $foreign_keys_extended["analyses"])) {
    $selection = " WHERE (prescription.analyses = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - prescription_reglementaire
if (in_array($retourformulaire, $foreign_keys_extended["prescription_reglementaire"])) {
    $selection = " WHERE (prescription.prescription_reglementaire = ".intval($idxformulaire).") ";
}

?>