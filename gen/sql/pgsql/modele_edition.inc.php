<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("editions")." -> "._("modeles d'edition");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."modele_edition
    LEFT JOIN ".DB_PREFIXE."courrier_type 
        ON modele_edition.courrier_type=courrier_type.courrier_type 
    LEFT JOIN ".DB_PREFIXE."om_etat 
        ON modele_edition.om_etat=om_etat.om_etat 
    LEFT JOIN ".DB_PREFIXE."om_lettretype 
        ON modele_edition.om_lettretype=om_lettretype.om_lettretype ";
// SELECT 
$champAffiche = array(
    'modele_edition.modele_edition as "'._("modele_edition").'"',
    'modele_edition.libelle as "'._("libelle").'"',
    'om_lettretype.libelle as "'._("om_lettretype").'"',
    'om_etat.libelle as "'._("om_etat").'"',
    'modele_edition.code as "'._("code").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(modele_edition.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(modele_edition.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'modele_edition.description as "'._("description").'"',
    'modele_edition.om_validite_debut as "'._("om_validite_debut").'"',
    'modele_edition.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'modele_edition.modele_edition as "'._("modele_edition").'"',
    'modele_edition.libelle as "'._("libelle").'"',
    'om_lettretype.libelle as "'._("om_lettretype").'"',
    'om_etat.libelle as "'._("om_etat").'"',
    'modele_edition.code as "'._("code").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    );
$tri="ORDER BY modele_edition.libelle ASC NULLS LAST";
$edition="modele_edition";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "courrier_type" => array("courrier_type", ),
    "om_etat" => array("om_etat", ),
    "om_lettretype" => array("om_lettretype", ),
);
// Filtre listing sous formulaire - courrier_type
if (in_array($retourformulaire, $foreign_keys_extended["courrier_type"])) {
    $selection = " WHERE (modele_edition.courrier_type = ".intval($idxformulaire).")  AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - om_etat
if (in_array($retourformulaire, $foreign_keys_extended["om_etat"])) {
    $selection = " WHERE (modele_edition.om_etat = ".intval($idxformulaire).")  AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - om_lettretype
if (in_array($retourformulaire, $foreign_keys_extended["om_lettretype"])) {
    $selection = " WHERE (modele_edition.om_lettretype = ".intval($idxformulaire).")  AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'analyses',
    //'analyses_type',
    //'courrier',
    //'proces_verbal',
    //'reunion_type',
);

?>