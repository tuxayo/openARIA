<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("piece");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."piece
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON piece.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON piece.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON piece.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."piece_statut 
        ON piece.piece_statut=piece_statut.piece_statut 
    LEFT JOIN ".DB_PREFIXE."piece_type 
        ON piece.piece_type=piece_type.piece_type 
    LEFT JOIN ".DB_PREFIXE."service 
        ON piece.service=service.service ";
// SELECT 
$champAffiche = array(
    'piece.piece as "'._("piece").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'piece.nom as "'._("nom").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'._("om_date_creation").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    );
//
$champNonAffiche = array(
    'piece.piece_type as "'._("piece_type").'"',
    'piece.uid as "'._("uid").'"',
    'piece.date_reception as "'._("date_reception").'"',
    'piece.date_emission as "'._("date_emission").'"',
    'piece.piece_statut as "'._("piece_statut").'"',
    'piece.suivi as "'._("suivi").'"',
    'piece.commentaire_suivi as "'._("commentaire_suivi").'"',
    'piece.lu as "'._("lu").'"',
    'piece.choix_lien as "'._("choix_lien").'"',
    'piece.service as "'._("service").'"',
    );
//
$champRecherche = array(
    'piece.piece as "'._("piece").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'piece.nom as "'._("nom").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="piece";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "piece_statut" => array("piece_statut", ),
    "piece_type" => array("piece_type", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (piece.dossier_coordination = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (piece.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (piece.etablissement = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - piece_statut
if (in_array($retourformulaire, $foreign_keys_extended["piece_statut"])) {
    $selection = " WHERE (piece.piece_statut = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - piece_type
if (in_array($retourformulaire, $foreign_keys_extended["piece_type"])) {
    $selection = " WHERE (piece.piece_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (piece.service = ".intval($idxformulaire).") ";
}

?>