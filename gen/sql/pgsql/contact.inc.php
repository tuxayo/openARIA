<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("contact");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."contact
    LEFT JOIN ".DB_PREFIXE."contact_civilite 
        ON contact.civilite=contact_civilite.contact_civilite 
    LEFT JOIN ".DB_PREFIXE."contact_type 
        ON contact.contact_type=contact_type.contact_type 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON contact.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."service 
        ON contact.service=service.service ";
// SELECT 
$champAffiche = array(
    'contact.contact as "'._("contact").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'contact_type.libelle as "'._("contact_type").'"',
    'contact_civilite.libelle as "'._("civilite").'"',
    'contact.nom as "'._("nom").'"',
    'contact.prenom as "'._("prenom").'"',
    'contact.titre as "'._("titre").'"',
    'contact.telephone as "'._("telephone").'"',
    'contact.mobile as "'._("mobile").'"',
    'contact.fax as "'._("fax").'"',
    'contact.courriel as "'._("courriel").'"',
    'contact.adresse_numero as "'._("adresse_numero").'"',
    'contact.adresse_numero2 as "'._("adresse_numero2").'"',
    'contact.adresse_voie as "'._("adresse_voie").'"',
    'contact.adresse_complement as "'._("adresse_complement").'"',
    'contact.adresse_cp as "'._("adresse_cp").'"',
    'contact.adresse_ville as "'._("adresse_ville").'"',
    'contact.lieu_dit as "'._("lieu_dit").'"',
    'contact.boite_postale as "'._("boite_postale").'"',
    'contact.cedex as "'._("cedex").'"',
    'contact.pays as "'._("pays").'"',
    'contact.qualite as "'._("qualite").'"',
    'contact.denomination as "'._("denomination").'"',
    'contact.raison_sociale as "'._("raison_sociale").'"',
    'contact.siret as "'._("siret").'"',
    'contact.categorie_juridique as "'._("categorie_juridique").'"',
    "case contact.reception_convocation when 't' then 'Oui' else 'Non' end as \""._("reception_convocation")."\"",
    'service.libelle as "'._("service").'"',
    "case contact.reception_programmation when 't' then 'Oui' else 'Non' end as \""._("reception_programmation")."\"",
    "case contact.reception_commission when 't' then 'Oui' else 'Non' end as \""._("reception_commission")."\"",
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(contact.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(contact.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'contact.om_validite_debut as "'._("om_validite_debut").'"',
    'contact.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'contact.contact as "'._("contact").'"',
    'etablissement.libelle as "'._("etablissement").'"',
    'contact_type.libelle as "'._("contact_type").'"',
    'contact_civilite.libelle as "'._("civilite").'"',
    'contact.nom as "'._("nom").'"',
    'contact.prenom as "'._("prenom").'"',
    'contact.titre as "'._("titre").'"',
    'contact.telephone as "'._("telephone").'"',
    'contact.mobile as "'._("mobile").'"',
    'contact.fax as "'._("fax").'"',
    'contact.courriel as "'._("courriel").'"',
    'contact.adresse_numero as "'._("adresse_numero").'"',
    'contact.adresse_numero2 as "'._("adresse_numero2").'"',
    'contact.adresse_voie as "'._("adresse_voie").'"',
    'contact.adresse_complement as "'._("adresse_complement").'"',
    'contact.adresse_cp as "'._("adresse_cp").'"',
    'contact.adresse_ville as "'._("adresse_ville").'"',
    'contact.lieu_dit as "'._("lieu_dit").'"',
    'contact.boite_postale as "'._("boite_postale").'"',
    'contact.cedex as "'._("cedex").'"',
    'contact.pays as "'._("pays").'"',
    'contact.qualite as "'._("qualite").'"',
    'contact.denomination as "'._("denomination").'"',
    'contact.raison_sociale as "'._("raison_sociale").'"',
    'contact.siret as "'._("siret").'"',
    'contact.categorie_juridique as "'._("categorie_juridique").'"',
    'service.libelle as "'._("service").'"',
    );
$tri="ORDER BY etablissement.libelle ASC NULLS LAST";
$edition="contact";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "contact_civilite" => array("contact_civilite", ),
    "contact_type" => array("contact_type", ),
    "etablissement" => array("etablissement", "etablissement_referentiel_erp", "etablissement_tous", ),
    "service" => array("service", ),
);
// Filtre listing sous formulaire - contact_civilite
if (in_array($retourformulaire, $foreign_keys_extended["contact_civilite"])) {
    $selection = " WHERE (contact.civilite = ".intval($idxformulaire).")  AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - contact_type
if (in_array($retourformulaire, $foreign_keys_extended["contact_type"])) {
    $selection = " WHERE (contact.contact_type = ".intval($idxformulaire).")  AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $selection = " WHERE (contact.etablissement = ".intval($idxformulaire).")  AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
}
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (contact.service = ".intval($idxformulaire).")  AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " AND ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)))";
}
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_courrier_contact',
    'lien_dossier_coordination_contact',
);

?>