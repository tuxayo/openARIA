<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("acteur_conge");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."acteur_conge
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON acteur_conge.acteur=acteur.acteur ";
// SELECT 
$champAffiche = array(
    'acteur_conge.acteur_conge as "'._("acteur_conge").'"',
    'acteur.nom_prenom as "'._("acteur").'"',
    'to_char(acteur_conge.date_debut ,\'DD/MM/YYYY\') as "'._("date_debut").'"',
    'acteur_conge.heure_debut as "'._("heure_debut").'"',
    'to_char(acteur_conge.date_fin ,\'DD/MM/YYYY\') as "'._("date_fin").'"',
    'acteur_conge.heure_fin as "'._("heure_fin").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'acteur_conge.acteur_conge as "'._("acteur_conge").'"',
    'acteur.nom_prenom as "'._("acteur").'"',
    'acteur_conge.heure_debut as "'._("heure_debut").'"',
    'acteur_conge.heure_fin as "'._("heure_fin").'"',
    );
$tri="ORDER BY acteur.nom_prenom ASC NULLS LAST";
$edition="acteur_conge";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "acteur" => array("acteur", ),
);
// Filtre listing sous formulaire - acteur
if (in_array($retourformulaire, $foreign_keys_extended["acteur"])) {
    $selection = " WHERE (acteur_conge.acteur = ".intval($idxformulaire).") ";
}

?>