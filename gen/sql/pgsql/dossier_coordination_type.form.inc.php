<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("dossiers")." -> "._("types de DC");
$tableSelect=DB_PREFIXE."dossier_coordination_type";
$champs=array(
    "dossier_coordination_type",
    "code",
    "libelle",
    "description",
    "om_validite_debut",
    "om_validite_fin",
    "dossier_type",
    "dossier_instruction_secu",
    "dossier_instruction_acc",
    "a_qualifier",
    "analyses_type_si",
    "analyses_type_acc");
//champs select
$sql_analyses_type_acc="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_analyses_type_acc_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE analyses_type = <idx>";
$sql_analyses_type_si="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_analyses_type_si_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE analyses_type = <idx>";
$sql_dossier_type="SELECT dossier_type.dossier_type, dossier_type.libelle FROM ".DB_PREFIXE."dossier_type WHERE ((dossier_type.om_validite_debut IS NULL AND (dossier_type.om_validite_fin IS NULL OR dossier_type.om_validite_fin > CURRENT_DATE)) OR (dossier_type.om_validite_debut <= CURRENT_DATE AND (dossier_type.om_validite_fin IS NULL OR dossier_type.om_validite_fin > CURRENT_DATE))) ORDER BY dossier_type.libelle ASC";
$sql_dossier_type_by_id = "SELECT dossier_type.dossier_type, dossier_type.libelle FROM ".DB_PREFIXE."dossier_type WHERE dossier_type = <idx>";
?>