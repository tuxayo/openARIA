<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_coordination_parcelle");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_coordination_parcelle
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON dossier_coordination_parcelle.dossier_coordination=dossier_coordination.dossier_coordination ";
// SELECT 
$champAffiche = array(
    'dossier_coordination_parcelle.dossier_coordination_parcelle as "'._("dossier_coordination_parcelle").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_coordination_parcelle.ref_cadastre as "'._("ref_cadastre").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'dossier_coordination_parcelle.dossier_coordination_parcelle as "'._("dossier_coordination_parcelle").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_coordination_parcelle.ref_cadastre as "'._("ref_cadastre").'"',
    );
$tri="ORDER BY dossier_coordination.libelle ASC NULLS LAST";
$edition="dossier_coordination_parcelle";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "dossier_coordination" => array("dossier_coordination", "dossier_coordination_nouveau", "dossier_coordination_a_qualifier", ),
);
// Filtre listing sous formulaire - dossier_coordination
if (in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $selection = " WHERE (dossier_coordination_parcelle.dossier_coordination = ".intval($idxformulaire).") ";
}

?>