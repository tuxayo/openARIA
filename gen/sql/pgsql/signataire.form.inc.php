<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("documents generes")." -> "._("signataires");
$tableSelect=DB_PREFIXE."signataire";
$champs=array(
    "signataire",
    "nom",
    "prenom",
    "civilite",
    "signataire_qualite",
    "signature",
    "defaut",
    "om_validite_debut",
    "om_validite_fin");
//champs select
$sql_civilite="SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite ORDER BY contact_civilite.libelle ASC";
$sql_civilite_by_id = "SELECT contact_civilite.contact_civilite, contact_civilite.libelle FROM ".DB_PREFIXE."contact_civilite WHERE contact_civilite = <idx>";
$sql_signataire_qualite="SELECT signataire_qualite.signataire_qualite, signataire_qualite.libelle FROM ".DB_PREFIXE."signataire_qualite WHERE ((signataire_qualite.om_validite_debut IS NULL AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE)) OR (signataire_qualite.om_validite_debut <= CURRENT_DATE AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE))) ORDER BY signataire_qualite.libelle ASC";
$sql_signataire_qualite_by_id = "SELECT signataire_qualite.signataire_qualite, signataire_qualite.libelle FROM ".DB_PREFIXE."signataire_qualite WHERE signataire_qualite = <idx>";
?>