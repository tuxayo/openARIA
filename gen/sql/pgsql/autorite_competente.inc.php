<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("metiers")." -> "._("autorites competentes");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."autorite_competente
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_competente.service=service.service ";
// SELECT 
$champAffiche = array(
    'autorite_competente.autorite_competente as "'._("autorite_competente").'"',
    'autorite_competente.code as "'._("code").'"',
    'autorite_competente.libelle as "'._("libelle").'"',
    'service.libelle as "'._("service").'"',
    );
//
$champNonAffiche = array(
    );
//
$champRecherche = array(
    'autorite_competente.autorite_competente as "'._("autorite_competente").'"',
    'autorite_competente.code as "'._("code").'"',
    'autorite_competente.libelle as "'._("libelle").'"',
    'service.libelle as "'._("service").'"',
    );
$tri="ORDER BY autorite_competente.libelle ASC NULLS LAST";
$edition="autorite_competente";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "service" => array("service", ),
);
// Filtre listing sous formulaire - service
if (in_array($retourformulaire, $foreign_keys_extended["service"])) {
    $selection = " WHERE (autorite_competente.service = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'dossier_instruction',
    //'etablissement',
);

?>