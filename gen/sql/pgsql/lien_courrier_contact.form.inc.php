<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_courrier_contact");
$tableSelect=DB_PREFIXE."lien_courrier_contact";
$champs=array(
    "lien_courrier_contact",
    "courrier",
    "contact");
//champs select
$sql_contact="SELECT contact.contact, contact.etablissement FROM ".DB_PREFIXE."contact WHERE ((contact.om_validite_debut IS NULL AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE)) OR (contact.om_validite_debut <= CURRENT_DATE AND (contact.om_validite_fin IS NULL OR contact.om_validite_fin > CURRENT_DATE))) ORDER BY contact.etablissement ASC";
$sql_contact_by_id = "SELECT contact.contact, contact.etablissement FROM ".DB_PREFIXE."contact WHERE contact = <idx>";
$sql_courrier="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
?>