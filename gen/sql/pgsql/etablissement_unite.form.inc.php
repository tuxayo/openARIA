<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("etablissement_unite");
$tableSelect=DB_PREFIXE."etablissement_unite";
$champs=array(
    "etablissement_unite",
    "libelle",
    "acc_notes_om_html",
    "acc_descriptif_ua_om_html",
    "etablissement",
    "acc_handicap_physique",
    "acc_handicap_auditif",
    "acc_handicap_visuel",
    "acc_handicap_mental",
    "acc_places_stationnement_amenagees",
    "acc_ascenseur",
    "acc_elevateur",
    "acc_boucle_magnetique",
    "acc_sanitaire",
    "acc_places_assises_public",
    "acc_chambres_amenagees",
    "acc_douche",
    "acc_derogation_scda",
    "etat",
    "archive",
    "dossier_instruction",
    "etablissement_unite_lie",
    "adap_date_validation",
    "adap_duree_validite",
    "adap_annee_debut_travaux",
    "adap_annee_fin_travaux");
//champs select
$sql_acc_derogation_scda="SELECT derogation_scda.derogation_scda, derogation_scda.libelle FROM ".DB_PREFIXE."derogation_scda WHERE ((derogation_scda.om_validite_debut IS NULL AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE)) OR (derogation_scda.om_validite_debut <= CURRENT_DATE AND (derogation_scda.om_validite_fin IS NULL OR derogation_scda.om_validite_fin > CURRENT_DATE))) ORDER BY derogation_scda.libelle ASC";
$sql_acc_derogation_scda_by_id = "SELECT derogation_scda.derogation_scda, derogation_scda.libelle FROM ".DB_PREFIXE."derogation_scda WHERE derogation_scda = <idx>";
$sql_dossier_instruction="SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction ORDER BY dossier_instruction.libelle ASC";
$sql_dossier_instruction_by_id = "SELECT dossier_instruction.dossier_instruction, dossier_instruction.libelle FROM ".DB_PREFIXE."dossier_instruction WHERE dossier_instruction = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
$sql_etablissement_unite_lie="SELECT etablissement_unite.etablissement_unite, etablissement_unite.libelle FROM ".DB_PREFIXE."etablissement_unite ORDER BY etablissement_unite.libelle ASC";
$sql_etablissement_unite_lie_by_id = "SELECT etablissement_unite.etablissement_unite, etablissement_unite.libelle FROM ".DB_PREFIXE."etablissement_unite WHERE etablissement_unite = <idx>";
?>