<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("application")." -> "._("dossier_instruction_reunion");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."dossier_instruction_reunion
    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis0 
        ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON dossier_instruction_reunion.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."reunion_avis as reunion_avis2 
        ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis 
    LEFT JOIN ".DB_PREFIXE."reunion 
        ON dossier_instruction_reunion.reunion=reunion.reunion 
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON dossier_instruction_reunion.reunion_type=reunion_type.reunion_type 
    LEFT JOIN ".DB_PREFIXE."reunion_categorie 
        ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie ";
// SELECT 
$champAffiche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'._("dossier_instruction_reunion").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion_categorie.libelle as "'._("reunion_type_categorie").'"',
    'to_char(dossier_instruction_reunion.date_souhaitee ,\'DD/MM/YYYY\') as "'._("date_souhaitee").'"',
    'reunion_avis2.libelle as "'._("proposition_avis").'"',
    'dossier_instruction_reunion.proposition_avis_complement as "'._("proposition_avis_complement").'"',
    'reunion.libelle as "'._("reunion").'"',
    'dossier_instruction_reunion.ordre as "'._("ordre").'"',
    'reunion_avis0.libelle as "'._("avis").'"',
    'dossier_instruction_reunion.avis_complement as "'._("avis_complement").'"',
    );
//
$champNonAffiche = array(
    'dossier_instruction_reunion.motivation as "'._("motivation").'"',
    'dossier_instruction_reunion.avis_motivation as "'._("avis_motivation").'"',
    );
//
$champRecherche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'._("dossier_instruction_reunion").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion_categorie.libelle as "'._("reunion_type_categorie").'"',
    'reunion_avis2.libelle as "'._("proposition_avis").'"',
    'dossier_instruction_reunion.proposition_avis_complement as "'._("proposition_avis_complement").'"',
    'reunion.libelle as "'._("reunion").'"',
    'dossier_instruction_reunion.ordre as "'._("ordre").'"',
    'reunion_avis0.libelle as "'._("avis").'"',
    'dossier_instruction_reunion.avis_complement as "'._("avis_complement").'"',
    );
$tri="ORDER BY dossier_instruction.libelle ASC NULLS LAST";
$edition="dossier_instruction_reunion";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = "";
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "reunion_avis" => array("reunion_avis", ),
    "dossier_instruction" => array("dossier_instruction", "dossier_instruction_mes_plans", "dossier_instruction_mes_visites", "dossier_instruction_tous_plans", "dossier_instruction_tous_visites", "dossier_instruction_a_qualifier", "dossier_instruction_a_affecter", ),
    "reunion" => array("reunion", ),
    "reunion_type" => array("reunion_type", ),
    "reunion_categorie" => array("reunion_categorie", ),
);
// Filtre listing sous formulaire - reunion_avis
if (in_array($retourformulaire, $foreign_keys_extended["reunion_avis"])) {
    $selection = " WHERE (dossier_instruction_reunion.avis = ".intval($idxformulaire)." OR dossier_instruction_reunion.proposition_avis = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - dossier_instruction
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    $selection = " WHERE (dossier_instruction_reunion.dossier_instruction = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion
if (in_array($retourformulaire, $foreign_keys_extended["reunion"])) {
    $selection = " WHERE (dossier_instruction_reunion.reunion = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_type
if (in_array($retourformulaire, $foreign_keys_extended["reunion_type"])) {
    $selection = " WHERE (dossier_instruction_reunion.reunion_type = ".intval($idxformulaire).") ";
}
// Filtre listing sous formulaire - reunion_categorie
if (in_array($retourformulaire, $foreign_keys_extended["reunion_categorie"])) {
    $selection = " WHERE (dossier_instruction_reunion.reunion_type_categorie = ".intval($idxformulaire).") ";
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'autorite_police',
    'proces_verbal',
);

?>