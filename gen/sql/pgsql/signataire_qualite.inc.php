<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("documents generes")." -> "._("qualites de signataire");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."signataire_qualite";
// SELECT 
$champAffiche = array(
    'signataire_qualite.signataire_qualite as "'._("signataire_qualite").'"',
    'signataire_qualite.code as "'._("code").'"',
    'signataire_qualite.libelle as "'._("libelle").'"',
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(signataire_qualite.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(signataire_qualite.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'signataire_qualite.description as "'._("description").'"',
    'signataire_qualite.om_validite_debut as "'._("om_validite_debut").'"',
    'signataire_qualite.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'signataire_qualite.signataire_qualite as "'._("signataire_qualite").'"',
    'signataire_qualite.code as "'._("code").'"',
    'signataire_qualite.libelle as "'._("libelle").'"',
    );
$tri="ORDER BY signataire_qualite.libelle ASC NULLS LAST";
$edition="signataire_qualite";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((signataire_qualite.om_validite_debut IS NULL AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE)) OR (signataire_qualite.om_validite_debut <= CURRENT_DATE AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((signataire_qualite.om_validite_debut IS NULL AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE)) OR (signataire_qualite.om_validite_debut <= CURRENT_DATE AND (signataire_qualite.om_validite_fin IS NULL OR signataire_qualite.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'signataire',
);

?>