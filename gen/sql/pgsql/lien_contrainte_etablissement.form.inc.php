<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_contrainte_etablissement");
$tableSelect=DB_PREFIXE."lien_contrainte_etablissement";
$champs=array(
    "lien_contrainte_etablissement",
    "etablissement",
    "contrainte",
    "texte_complete",
    "recuperee");
//champs select
$sql_contrainte="SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE ((contrainte.om_validite_debut IS NULL AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)) OR (contrainte.om_validite_debut <= CURRENT_DATE AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE))) ORDER BY contrainte.libelle ASC";
$sql_contrainte_by_id = "SELECT contrainte.contrainte, contrainte.libelle FROM ".DB_PREFIXE."contrainte WHERE contrainte = <idx>";
$sql_etablissement="SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, etablissement.libelle FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";
?>