<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_autorite_police_courrier");
$tableSelect=DB_PREFIXE."lien_autorite_police_courrier";
$champs=array(
    "lien_autorite_police_courrier",
    "autorite_police",
    "courrier");
//champs select
$sql_autorite_police="SELECT autorite_police.autorite_police, autorite_police.autorite_police_decision FROM ".DB_PREFIXE."autorite_police ORDER BY autorite_police.autorite_police_decision ASC";
$sql_autorite_police_by_id = "SELECT autorite_police.autorite_police, autorite_police.autorite_police_decision FROM ".DB_PREFIXE."autorite_police WHERE autorite_police = <idx>";
$sql_courrier="SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier ORDER BY courrier.etablissement ASC";
$sql_courrier_by_id = "SELECT courrier.courrier, courrier.etablissement FROM ".DB_PREFIXE."courrier WHERE courrier = <idx>";
?>