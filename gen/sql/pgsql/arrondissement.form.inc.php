<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("administration_parametrage")." -> "._("adresses")." -> "._("arrondissements");
$tableSelect=DB_PREFIXE."arrondissement";
$champs=array(
    "arrondissement",
    "code",
    "libelle",
    "description",
    "om_validite_debut",
    "om_validite_fin",
    "code_impots");
?>