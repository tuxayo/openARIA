<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$ent = _("application")." -> "._("lien_reglementation_applicable_etablissement_categorie");
$tableSelect=DB_PREFIXE."lien_reglementation_applicable_etablissement_categorie";
$champs=array(
    "lien_reglementation_applicable_etablissement_categorie",
    "reglementation_applicable",
    "etablissement_categorie");
//champs select
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
$sql_etablissement_categorie_by_id = "SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
$sql_reglementation_applicable="SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE ((reglementation_applicable.om_validite_debut IS NULL AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE)) OR (reglementation_applicable.om_validite_debut <= CURRENT_DATE AND (reglementation_applicable.om_validite_fin IS NULL OR reglementation_applicable.om_validite_fin > CURRENT_DATE))) ORDER BY reglementation_applicable.libelle ASC";
$sql_reglementation_applicable_by_id = "SELECT reglementation_applicable.reglementation_applicable, reglementation_applicable.libelle FROM ".DB_PREFIXE."reglementation_applicable WHERE reglementation_applicable = <idx>";
?>