<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration")." -> "._("om_sig_map");
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."om_sig_map
    LEFT JOIN ".DB_PREFIXE."om_collectivite 
        ON om_sig_map.om_collectivite=om_collectivite.om_collectivite 
    LEFT JOIN ".DB_PREFIXE."om_sig_extent 
        ON om_sig_map.om_sig_extent=om_sig_extent.om_sig_extent 
    LEFT JOIN ".DB_PREFIXE."om_sig_map 
        ON om_sig_map.source_flux=om_sig_map.om_sig_map ";
// SELECT 
$champAffiche = array(
    'om_sig_map.om_sig_map as "'._("om_sig_map").'"',
    'om_sig_map.id as "'._("id").'"',
    'om_sig_map.libelle as "'._("libelle").'"',
    "case om_sig_map.actif when 't' then 'Oui' else 'Non' end as \""._("actif")."\"",
    'om_sig_map.zoom as "'._("zoom").'"',
    "case om_sig_map.fond_osm when 't' then 'Oui' else 'Non' end as \""._("fond_osm")."\"",
    "case om_sig_map.fond_bing when 't' then 'Oui' else 'Non' end as \""._("fond_bing")."\"",
    "case om_sig_map.fond_sat when 't' then 'Oui' else 'Non' end as \""._("fond_sat")."\"",
    "case om_sig_map.layer_info when 't' then 'Oui' else 'Non' end as \""._("layer_info")."\"",
    'om_sig_map.projection_externe as "'._("projection_externe").'"',
    'om_sig_map.retour as "'._("retour").'"',
    "case om_sig_map.util_idx when 't' then 'Oui' else 'Non' end as \""._("util_idx")."\"",
    "case om_sig_map.util_reqmo when 't' then 'Oui' else 'Non' end as \""._("util_reqmo")."\"",
    "case om_sig_map.util_recherche when 't' then 'Oui' else 'Non' end as \""._("util_recherche")."\"",
    'om_sig_map.libelle as "'._("source_flux").'"',
    'om_sig_map.fond_default as "'._("fond_default").'"',
    'om_sig_extent.nom as "'._("om_sig_extent").'"',
    "case om_sig_map.restrict_extent when 't' then 'Oui' else 'Non' end as \""._("restrict_extent")."\"",
    'om_sig_map.sld_marqueur as "'._("sld_marqueur").'"',
    'om_sig_map.sld_data as "'._("sld_data").'"',
    'om_sig_map.point_centrage as "'._("point_centrage").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champAffiche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
//
$champNonAffiche = array(
    'om_sig_map.om_collectivite as "'._("om_collectivite").'"',
    'om_sig_map.url as "'._("url").'"',
    'om_sig_map.om_sql as "'._("om_sql").'"',
    );
//
$champRecherche = array(
    'om_sig_map.om_sig_map as "'._("om_sig_map").'"',
    'om_sig_map.id as "'._("id").'"',
    'om_sig_map.libelle as "'._("libelle").'"',
    'om_sig_map.zoom as "'._("zoom").'"',
    'om_sig_map.projection_externe as "'._("projection_externe").'"',
    'om_sig_map.retour as "'._("retour").'"',
    'om_sig_map.libelle as "'._("source_flux").'"',
    'om_sig_map.fond_default as "'._("fond_default").'"',
    'om_sig_extent.nom as "'._("om_sig_extent").'"',
    'om_sig_map.sld_marqueur as "'._("sld_marqueur").'"',
    'om_sig_map.sld_data as "'._("sld_data").'"',
    );
//
if ($_SESSION['niveau'] == '2') {
    array_push($champRecherche, "om_collectivite.libelle as \""._("collectivite")."\"");
}
$tri="ORDER BY om_sig_map.libelle ASC NULLS LAST";
$edition="om_sig_map";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
if ($_SESSION["niveau"] == "2") {
    // Filtre MULTI
    $selection = "";
} else {
    // Filtre MONO
    $selection = " WHERE (om_sig_map.om_collectivite = '".$_SESSION["collectivite"]."') ";
}
// Liste des clés étrangères avec leurs éventuelles surcharges
$foreign_keys_extended = array(
    "om_collectivite" => array("om_collectivite", ),
    "om_sig_extent" => array("om_sig_extent", ),
    "om_sig_map" => array("om_sig_map", ),
);
// Filtre listing sous formulaire - om_collectivite
if (in_array($retourformulaire, $foreign_keys_extended["om_collectivite"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (om_sig_map.om_collectivite = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (om_sig_map.om_collectivite = '".$_SESSION["collectivite"]."') AND (om_sig_map.om_collectivite = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_sig_extent
if (in_array($retourformulaire, $foreign_keys_extended["om_sig_extent"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (om_sig_map.om_sig_extent = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (om_sig_map.om_collectivite = '".$_SESSION["collectivite"]."') AND (om_sig_map.om_sig_extent = ".intval($idxformulaire).") ";
    }
}
// Filtre listing sous formulaire - om_sig_map
if (in_array($retourformulaire, $foreign_keys_extended["om_sig_map"])) {
    if ($_SESSION["niveau"] == "2") {
        // Filtre MULTI
        $selection = " WHERE (om_sig_map.source_flux = ".intval($idxformulaire).") ";
    } else {
        // Filtre MONO
        $selection = " WHERE (om_sig_map.om_collectivite = '".$_SESSION["collectivite"]."') AND (om_sig_map.source_flux = ".intval($idxformulaire).") ";
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'om_sig_map',
    'om_sig_map_comp',
    'om_sig_map_flux',
);

?>