<?php
//$Id$ 
//gen openMairie le 19/05/2017 10:15

$DEBUG=0;
$serie=15;
$ent = _("administration_parametrage")." -> "._("contraintes")." -> "._("listing");
$om_validite = true;
if(!isset($premier)) $premier='';
if(!isset($recherche1)) $recherche1='';
if(!isset($tricolsf)) $tricolsf='';
if(!isset($premiersf)) $premiersf='';
if(!isset($selection)) $selection='';
if(!isset($retourformulaire)) $retourformulaire='';
if (!isset($idxformulaire)) {
    $idxformulaire = '';
}
if (!isset($selectioncol)) {
    $selectioncol = '';
}
if (!isset($tricol)) {
    $tricol = '';
}
if (!isset($valide)) {
    $valide = '';
}
if (!isset($recherche)) {
    $recherche = '';
}
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".strtoupper($idz)."&nbsp;";
}
// FROM 
$table = DB_PREFIXE."contrainte";
// SELECT 
$champAffiche = array(
    'contrainte.contrainte as "'._("contrainte").'"',
    'contrainte.id_referentiel as "'._("id_referentiel").'"',
    'contrainte.nature as "'._("nature").'"',
    'contrainte.groupe as "'._("groupe").'"',
    'contrainte.sousgroupe as "'._("sousgroupe").'"',
    'contrainte.libelle as "'._("libelle").'"',
    'contrainte.ordre_d_affichage as "'._("ordre_d_affichage").'"',
    "case contrainte.lie_a_un_referentiel when 't' then 'Oui' else 'Non' end as \""._("lie_a_un_referentiel")."\"",
    );
// Spécificité des dates de validité
$displayed_fields_validite = array(
    'to_char(contrainte.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(contrainte.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if (isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}

//
$champNonAffiche = array(
    'contrainte.texte as "'._("texte").'"',
    'contrainte.texte_surcharge as "'._("texte_surcharge").'"',
    'contrainte.om_validite_debut as "'._("om_validite_debut").'"',
    'contrainte.om_validite_fin as "'._("om_validite_fin").'"',
    );
//
$champRecherche = array(
    'contrainte.contrainte as "'._("contrainte").'"',
    'contrainte.id_referentiel as "'._("id_referentiel").'"',
    'contrainte.nature as "'._("nature").'"',
    'contrainte.groupe as "'._("groupe").'"',
    'contrainte.sousgroupe as "'._("sousgroupe").'"',
    'contrainte.libelle as "'._("libelle").'"',
    'contrainte.ordre_d_affichage as "'._("ordre_d_affichage").'"',
    );
$tri="ORDER BY contrainte.libelle ASC NULLS LAST";
$edition="contrainte";
/**
 * Gestion de la clause WHERE => $selection
 */
// Filtre listing standard
$selection = " WHERE ((contrainte.om_validite_debut IS NULL AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)) OR (contrainte.om_validite_debut <= CURRENT_DATE AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)))";
$where_om_validite = " WHERE ((contrainte.om_validite_debut IS NULL AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)) OR (contrainte.om_validite_debut <= CURRENT_DATE AND (contrainte.om_validite_fin IS NULL OR contrainte.om_validite_fin > CURRENT_DATE)))";
// Gestion OMValidité - Suppression du filtre si paramètre
if (isset($_GET["valide"]) and $_GET["valide"] == "false") {
    if (!isset($where_om_validite) 
        or (isset($where_om_validite) and $where_om_validite == "")) {
        if (trim($selection) != "") {
            $selection = "";
        }
    } else {
        $selection = trim(str_replace($where_om_validite, "", $selection));
    }
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    //'lien_contrainte_dossier_coordination',
    //'lien_contrainte_etablissement',
);

?>