*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Test suite Analyse :
...  génération automatique à la création d'un DI,
...  consultation,
...  modification,
...  widgets,
...  workflow,
...  éditions.
...  Dans ce test suite, après avoir qualifié un dossier de coordination
...  pour une demande de réception, on rédigera dans son intégralité
...  l'analyse SI. Ce test case est donc à exécuter en premier. Pour les
...  suivants l'ordre n'a par contre aucune incidence à l'exception des
...  réglementations applicables dont les textes-types dépendent des types
...  secondaires de l'établissement.


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement du sous-sol
    ...  date_demande=25/11/2014
    ...  a_qualifier=false
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}
    # On définit les variables  globables utilisées pour ce test
    Set Suite Variable    ${di_si}    ${dc01_libelle}-SI
    Set Suite Variable    ${di_acc}    ${dc01_libelle}-ACC


Modification des paramètres des variables de remplacement &contraintes

    [Documentation]  Vérifie que les 3 paramètres de &contrainte liste_groupe,
    ...  liste_ssgroupe et affichage_sans_arborescence modifient l'affichage des
    ...  contraintes sans erreur.
    ...  Les contraintes étant applicables à des établissements, et ces derniers n'étant
    ...  pas obligatoirement rattachés aux dossiers de coordination, on vérifie également
    ...  que ce cas est correctement géré.

    #
    # Jeu de données initial
    #

    # Paramétrage des contraintes : ajout d'une filtrée et la deuxième non pour le DC
    # puis une dernière pour l'établissement
    Depuis la page d'accueil  admin  admin
    &{contrainte} =  Create Dictionary
    ...  groupe=groupe_test
    ...  sousgroupe=sousgroupe_test
    ...  nature=POS
    ...  libelle=Contrainte filtrée
    ...  texte=Texte
    ...  texte_surcharge=Surcharge filtrée
    Ajouter la contrainte  ${contrainte}
    &{contrainte} =  Create Dictionary
    ...  groupe=groupe_non_test
    ...  sousgroupe=sousgroupe_non_test
    ...  nature=POS
    ...  libelle=Contrainte non filtrée
    ...  texte=Texte
    ...  texte_surcharge=Surcharge non filtrée
    Ajouter la contrainte  ${contrainte}
        &{contrainte} =  Create Dictionary
    ...  groupe=un_groupe
    ...  sousgroupe=un_sousgroupe
    ...  nature=POS
    ...  libelle=Contrainte établissement
    ...  texte=Texte
    ...  texte_surcharge=Surcharge établissement
    Ajouter la contrainte  ${contrainte}

    # Création d'un DC
    Depuis la page d'accueil  cadre-si  cadre-si
    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement de l'accueil
    ...  date_demande=12/12/2014
    ...  a_qualifier=false
    ${dc_libelle} =  Ajouter le dossier de coordination  ${dc}
    # Application de contraintes
    Depuis le contexte du dossier de coordination  ${dc_libelle}
    On clique sur l'onglet  lien_contrainte_dossier_coordination  Contraintes
    Click Element  ajouter_contrainte
    Appliquer la contrainte au DC  groupe_test  sousgroupe_test
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#sformulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-lien_contrainte_dossier_coordination div.message
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  css=#sousform-lien_contrainte_dossier_coordination div.message.ui-state-valid p span.text  La contrainte Contrainte filtrée a été appliquée au DC.
    Click On Back Button In Subform
    Click Element  ajouter_contrainte
    Appliquer la contrainte au DC  groupe_non_test  sousgroupe_non_test
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#sformulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-lien_contrainte_dossier_coordination div.message
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  css=#sousform-lien_contrainte_dossier_coordination div.message.ui-state-valid p span.text  La contrainte Contrainte non filtrée a été appliquée au DC.

    #
    # CAS 1 : contraintes du DC sans paramètre
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_dc
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  GROUPE_TEST
    Page Should Contain  SOUSGROUPE_TEST
    Page Should Contain  Surcharge filtrée
    Page Should Contain  GROUPE_NON_TEST
    Page Should Contain  SOUSGROUPE_NON_TEST
    Page Should Contain  Surcharge non filtrée
    Close PDF

    #
    # CAS 2 : contraintes du DC avec paramètre liste_groupe
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_dc(liste_groupe=groupe_test)
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  GROUPE_TEST
    Page Should Contain  SOUSGROUPE_TEST
    Page Should Contain  Surcharge filtrée
    Page Should Not Contain  GROUPE_NON_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST
    Page Should Not Contain  Surcharge non filtrée
    Close PDF

    #
    # CAS 3 : contraintes du DC avec paramètre liste_ssgroupe
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_dc(liste_ssgroupe=sousgroupe_test)
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  GROUPE_TEST
    Page Should Contain  SOUSGROUPE_TEST
    Page Should Contain  Surcharge filtrée
    Page Should Not Contain  GROUPE_NON_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST
    Page Should Not Contain  Surcharge non filtrée
    Close PDF

    #
    # CAS 4 : contraintes du DC sans arborescence
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_dc(affichage_sans_arborescence=t)
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surcharge filtrée
    Page Should Contain  Surcharge non filtrée
    Page Should Not Contain  GROUPE_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST_
    Page Should Not Contain  GROUPE_NON_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST

    Close PDF

    #
    # CAS 5 : contraintes de l'établissement qui n'existe pas
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_etab
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  RAPPORT
    Page Should Not Contain  Surcharge établissement
    Close PDF

    #
    # Mise à jour du jeu données
    #

    # Création d'un établissement
    &{etab} =  Create Dictionary
    ...  libelle=OUTSIDER
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  adresse_numero=7
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    Depuis la page d'accueil  cadre-si  cadre-si
    ${etab_code} =  Ajouter l'établissement  ${etab}
    ${etab_titre} =  Set Variable  ${etab_code} - ${etab.libelle}
    # Application de contraintes
    Depuis le contexte de l'établissement  ${etab_code}  ${etab.libelle}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click Element  ajouter_contrainte
    Appliquer la contrainte à l'établissement  un_groupe  un_sousgroupe
        Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#sformulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-lien_contrainte_etablissement div.message
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  css=#sousform-lien_contrainte_etablissement div.message.ui-state-valid p span.text  La contrainte Contrainte établissement a été appliquée à l'établissement.
    # Rattachement de l'établissement au DC
    Depuis le formulaire de modification du dossier de coordination  ${dc_libelle}
    Input Text  css=#autocomplete-etablissement_tous-search    ${etab_titre}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Link  ${etab_titre}
    Click On Submit Button

    #
    # CAS 6 : contraintes de l'établissement qui existe
    #

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surcharge établissement
    Close PDF

    #
    # CAS 7 : contraintes du DC avec paramètres multiples
    #

    # Paramétrage de la lettre-type 'Analyse - Rapport SI'
    ${corps} =  Set Variable  &contraintes_dc(liste_groupe=groupe_test;liste_ssgroupe=sousgroupe_test;affichage_sans_arborescence=t)
    Depuis la page d'accueil  admin  admin
    Modifier la lettre-type  analyse_rapport_si  null  null  ${corps}

    # Édition du rapport
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${dc_libelle}-SI
    Cliquer action analyse  rapport_analyse
    Open PDF  form
    # Vérification du contenu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  Surcharge filtrée
    Page Should Not Contain  GROUPE_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST_
    Page Should Not Contain  GROUPE_NON_TEST
    Page Should Not Contain  SOUSGROUPE_NON_TEST
    Page Should Not Contain  Surcharge non filtrée
    Close PDF


Vérification des permissions

    [Documentation]  L'objet de ce 'Test Case' est de vérifier les droits de
    ...  consultation et de modification qu'ont les profils technicien,
    ...  secrétaire et cadre sur les blocs de données de l'analyse.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # Création d'un DC
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement de l'accueil
    ...  date_demande=12/12/2014
    ...  a_qualifier=false
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable    ${dc02}
    Set Suite Variable    ${dc02_libelle}
    # 1/4 Un technicien peut modifier une analyse en cours
    Depuis la page d'accueil    technicien-si    technicien-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Édition analyse en cours est activée    si
    Cliquer action analyse    finaliser
    Vérifier état terminé    true    false
    Édition analyse terminée est désactivée
    Cliquer action analyse    reouvrir_terminee
    # 2/4 Un cadre peut modifier une analyse en cours ou terminée
    Depuis la page d'accueil    cadre-si    cadre-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Édition analyse en cours est activée    si
    Cliquer action analyse    finaliser
    Vérifier état terminé    true    true
    Édition analyse terminée est activée    si
    Cliquer action analyse    reouvrir_terminee
    # 3/4 Une secrétaire peut modifier une analyse en cours
    Depuis la page d'accueil    secretaire-si    secretaire-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Édition analyse en cours est activée    si
    Depuis la page d'accueil    cadre-si    cadre-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Cliquer action analyse    finaliser
    Depuis la page d'accueil    secretaire-si    secretaire-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Vérifier état terminé    false    false
    Édition analyse terminée est désactivée
    # 4/4 Un profil ACC ne peut pas classifier l'établissement
    Depuis la page d'accueil    cadre-acc    cadre-acc
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-ACC
    Édition analyse en cours est activée    acc
    Cliquer action analyse    finaliser
    Vérifier état terminé    true    true
    Édition analyse terminée est activée    acc
    Cliquer action analyse    reouvrir_terminee

    # Vérification de la présence ou absence des actions d'éditions PDF
    # sur l'onglet analyse
    # Les profils SIont accès aux trois éditions
    Depuis la page d'accueil    cadre-si    cadre-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal
    Depuis la page d'accueil    technicien-si    technicien-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal
    Depuis la page d'accueil    secretaire-si    secretaire-si
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-SI
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal
    # Les profils ACC n'ont pas accès au CR d'analyse
    Depuis la page d'accueil    cadre-acc    cadre-acc
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-ACC
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Not Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal
    Depuis la page d'accueil    technicien-acc    technicien-acc
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-ACC
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Not Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal
    Depuis la page d'accueil    secretaire-acc    secretaire-acc
    Accéder à l'analyse du dossier d'instruction    ${dc02_libelle}-ACC
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-rapport_analyse
    Page Should Not Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-compte_rendu_analyse
    Page Should Contain Element  css=#sousform-container #portlet-actions #action-form-analyses-proces_verbal


Naviguer dans l'onglet "Analyse"

    [Documentation]    L'onglet "Analyse" possède une ergonomie particulière puisque
    ...    à l'ouverture de l'onglet c'est la visualisation de l'analyse qui est
    ...    disponible et qu'il est ensuite possible de modifier chaque champ dans un
    ...    formulaire spécifique au champ en question. L'objet de ce 'Test Case' est
    ...    de tester que cette navigation fonctionne correctement.

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si

    # On accède à l'analyse du DI
    Accéder à l'analyse du dossier d'instruction    ${di_si}

    # On accède au formulaire de modification de l'objet de l'étude
    Éditer bloc analyse  objet
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    # On clique sur le bouton retour sans valider le formulaire pour retourner sur l'écran de visualisation de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

    # On accède au formulaire de modification des données techniques
    Éditer bloc analyse  donnees_techniques
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    # On valide le formulaire
    Input Text    css=#si_effectif_public    12
    Submit Form    css=#form_analyses_modifier
    # On accède à la page de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    # On clique sur le bouton retour pour retourner sur l'écran de visualisation de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

    # On accède au formulaire de modification de l'objet de l'étude
    Éditer bloc analyse  objet
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    # On valide le formulaire
    Input Text    css=#objet    azerty
    Submit Form    css=#form_analyses_modifier
    # On accède à la page de validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    # On clique sur le bouton retour pour retourner sur l'écran de visualisation de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

    #
    Depuis la page d'accueil    technicien-si    technicien-si

    # On accède à l'analyse du DI
    Accéder à l'analyse du dossier d'instruction    ${di_si}

    # On termine la rédaction de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Terminer
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Rédaction terminée.
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # On accède à l'analyse du DI
    Accéder à l'analyse du dossier d'instruction    ${di_si}

    # On valide la rédaction de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Valider
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Validation de l'analyse.
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

    # On réouvre la rédaction de l'analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Ré-ouvrir
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Réouverture de l'analyse.
    Page Title Should Contain    ${di_si}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse

Modification du type de l'analyse
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie le type d'analyse
    Page Should Not Contain    Visite périodique sécurité
    Page Should Contain    Visite de réception sécurité
    # On ouvre sa vue de modification
    Éditer bloc analyse  analyses_type
    # On modifie le type d'analyse par un inexistant
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#analyses_type    Choisir type d'analyse
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie l'erreur de validation du formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Error Message Should Be    Le type d'analyse renseigné est invalide.
    # On remodifie le type d'analyse par un existant
    Select From List By Label    css=#analyses_type    Visite périodique sécurité
    # On revalide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie cette fois la validation du formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que le nouveau type d'analyse a été pris en compte
    Page Should Not Contain    Visite de réception sécurité
    Page Should Contain    Visite périodique sécurité


Modification de l'objet de l'analyse
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que l'objet de l'analyse ne contient pas sa future valeur
    Page Should Not Contain    Objet de l'analyse
    # On ouvre sa vue de modification
    Éditer bloc analyse  objet
    # On modifie l'objet
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    css=#objet    Objet de l'analyse
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que le nouvel objet de l'analyse a été pris en compte
    Page Should Contain    Objet de l'analyse


Modification du descriptif de l'établissement
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que le descriptif de l'établissement ne contient pas sa future valeur
    Page Should Not Contain    Description de l'établissement
    # On ouvre sa vue de modification
    Éditer bloc analyse  descriptif_etablissement
    # On modifie le descriptif
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input HTML    css=#descriptif_etablissement_om_html    Description de l'établissement
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que le nouveau descriptif de l'établissement a été pris en compte
    Page Should Contain    Description de l'établissement

Modification de la classification de l'établissement
    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    # On vérifie que cette vue est inaccessible pour le service accessibilité
    Accéder à l'analyse du dossier d'instruction    ${dc01_libelle}-ACC
    Page Should Not Contain    Classification de l'établissement
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse SI
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Page Should Contain    Classification de l'établissement
    Page Should Contain    ERP : non
    Page Should Contain    Locaux à sommeil : non
    # On ouvre sa vue de modification
    Éditer bloc analyse  classification_etablissement
    # On coche ERP
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#erp
    # On coche locaux sommeil
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#etablissement_locaux_sommeil
    # On sélectionne un type principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_type    [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    # On sélectionne deux types secondaires dont le principal
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_type_secondaire    [CTS] Chapiteaux, Tentes et Structures toile    [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    # On sélectionne une catégorie
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_categorie    [3] de 301 à 700 personnes
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Not Contain Errors
    # On vérifie que les données ont été prises en compte
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain    ERP : oui
    Page Should Contain    Locaux à sommeil : oui
    Page Should Contain    Type : [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    Page Should Contain    [CTS] Chapiteaux, Tentes et Structures toile
    Page Should Contain    Catégorie : [3] de 301 à 700 personnes
    # On vérifie les nouvelles données du DC
    Depuis le contexte du dossier de coordination    ${dc01_libelle}
    Page Should Contain    [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    Page Should Contain    [CTS] Chapiteaux, Tentes et Structures toile
    Page Should Contain    [3] de 301 à 700 personnes
    Element Text Should Be    css=#etablissement_locaux_sommeil    Oui
    Element Text Should Be    css=#erp    Oui

Modification des données techniques SI
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI SI créé au début de ce test suite
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que les nouvelles données techniques sont absentes
    Page Should Not Contain    type SSI : A
    Page Should Not Contain    type d'alarme : 1
    # On rentre de nouvelles valeurs
    Modifier données techniques depuis analyse SI    ${di_si}    1    A
    # On vérifie que les nouvelles valeurs ont été prises en compte
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#donnee_technique    type SSI : A
    Element Should Contain    css=#donnee_technique    type d'alarme : 1
    Page Should Not Contain    conformité L16


Modification de la réglementation applicable
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que la R.A. ne contient pas sa future valeur
    Page Should Not Contain    Suite à un déclenchement de l'alarme incendie, le bâtiment doit pouvoir être évacué entièrement en moins de cinq minutes.
    # On ouvre sa vue de modification
    Éditer bloc analyse  reglementation_applicable
    # On modifie la R.A.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input HTML    css=#reglementation_applicable_om_html    Suite à un déclenchement de l'alarme incendie, le bâtiment doit pouvoir être évacué entièrement en moins de cinq minutes.
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que la nouvelle R.A. a été prise en compte
    Page Should Contain    Suite à un déclenchement de l'alarme incendie, le bâtiment doit pouvoir être évacué entièrement en moins de cinq minutes.
    # On teste la réglementation-type non paramétrée
    Éditer bloc analyse  reglementation_applicable
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Insérer une réglementation-type
    Page Should Contain    Aucune réglementation-type trouvée.
    Click Element    css=#form-analyses-overlay input[type=submit]

    #
    Depuis la page d'accueil    admin    admin
    # On ajoute une réglementation-type SI liée au type d'établissement CTS et à la catégorie 3
    Go To Dashboard
    Go To Tab    reglementation_applicable
    Click On Add Button
    Select From List By Label    css=#service    Sécurité Incendie
    Input Text    css=#libelle    Évacuation des locaux
    Input Text    css=#description    Moins de cinq minutes
    Select From List By Label    css=#etablissement_type    CTS
    Select From List By Label    css=#etablissement_categorie    3
    Click On Submit Button
    # On ajoute une réglementation-type ACC liée à tous les types et catégories
    # d'établissement
    Go To Dashboard
    Go To Tab    reglementation_applicable
    Click On Add Button
    Select From List By Label    css=#service    Accessibilité
    Input Text    css=#libelle    Pente douce
    Input Text    css=#description    L'entrée doit présentée une pente d'1% maximum
    Select From List By Label    css=#etablissement_type    J  L  M  N  O  P  R  S  T  U  V  W  X  Y  PA  CTS  SG  PS  OA  GA  EF  REF  IGH
    Select From List By Label    css=#etablissement_categorie    1  2  3  4  5
    Click On Submit Button

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On revient à l'édition de notre analyse pour classifier l'établissement
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Éditer bloc analyse  classification_etablissement
    # On coche ERP
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#erp
    # On coche locaux sommeil
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#etablissement_locaux_sommeil
    # On sélectionne un type principal autre que CTS
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_type    [GA] Gares Accessibles au public (chemins de fer, téléphériques, remonte-pentes...)
    # On sélectionne CTS en type secondaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_type_secondaire    [CTS] Chapiteaux, Tentes et Structures toile
    # On sélectionne la catégorie 1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_categorie    [1] plus de 1500 personnes
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    # On vérifie que la réglementation-type n'apparaît pas encore
    Sleep    1
    Page Should Not Contain    Moins de cinq minutes
    # On réouvre la modification de la réglementation applicable
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Éditer bloc analyse  reglementation_applicable
    # On ouvre l'overlay
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Insérer une réglementation-type
    # Le type du DC correspond à la RA SI, mais pas la catégorie
    # Le type et la catégorie correspondent à la RA ACC
    # On vérifie donc qu'il n'y a pas de résultat
    Page Should Contain    Aucune réglementation-type trouvée.
    Click Element    css=#form-analyses-overlay input[type=submit]
    # On reclassifie l'établissement avec la bonne catégorie
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Éditer bloc analyse  classification_etablissement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#etablissement_categorie    [3] de 301 à 700 personnes
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On réouvre les réglementations applicables
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#sousform-analyses a.retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Éditer bloc analyse  reglementation_applicable
    # On ouvre l'overlay
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Insérer une réglementation-type
    # On choisit la réglementation-type qui soit dorénavant apparaître
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#checkbox_1
    Click Element    css=input[value=Valider]
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Vos modifications ont bien été enregistrées.
    # On revient sur la vue de l'analyse
    Click Element  css=#sousform-analyses a.retour
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#analyse_etat
    Page Should Not Contain Errors
    # On vérifie la présence de la réglementation-type
    Page Should Contain    Moins de cinq minutes

Modification des prescriptions
    [Documentation]     CRUD des prescriptions plus reclassement.
    ...    On teste ici l'interface spécifique des prescriptions de l'analyse.
    ...    Celle-ci consiste à pouvoir, au delà de les consulter, en ajouter,
    ...    modifier, supprimer ainsi que les ré-ordonner.
    ...    En AJAX les fèches haut et bas se masquent suivant la position de
    ...    la prescription. De plus si une prescription réglementaire est choisie
    ...    cela affiche certains champs, notamment l'ajout de prescription
    ...    spécifique-type. L'utilisateur doit renseigner au moins une prescription,
    ...    qu'elle soit réglementaire ou spécifique. Une prescription réglementaire
    ...    étant liée à zéro, un ou plusieurs types et catégories d'établissement, le
    ...    test case "classification de l'établissement" permet d'avoir du contenu
    ...    malgré le filtre appliqué.

    #
    Depuis la page d'accueil  technicien-si  technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction  ${di_si}
    # On vérifie qu'il n'y a aucune prescription
    Page Should Not Contain  Prescriptions (
    # On ouvre sa vue de modification
    Éditer bloc analyse  prescriptions
    # On crée une nouvelle prescription
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Click Element  add_a_prescription
    # On vérifie qu'il y a le bouton ajouter dans l'overlay
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Element Should Be Visible  add_the_prescription
    # On vérifie qu'il y n'y a pas de message d'erreur
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Element Should Not Be Visible  css=.erreur_prescription
    # On ajoute sans prescription réglementaire ni spécifique
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Click Element  css=#add_the_prescription
    # On vérifie qu'il y a un message d'erreur
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Element Should Be Visible  css=.erreur_prescription
    # On vérifie qu'il n'y a pas de prescription spécifique-type disponible
    Element Should Not Be Visible  css=#specifique-type
    # On vérifie que le contenu du select est vide
    @{list_pr} =  Create List  choisir la prescription réglementaire
    Select List Should Be  prescription_reglementaire  ${list_pr}
    # On associe la PR 'SI-PR3' à tous les types d'établissement
    # en plus de l'association à toutes les catégories déjà existante
    Depuis la page d'accueil  admin  admin
    @{list_typ} =  Create List  J  L  M  N  O  P  R  S  T  U  V  W  X  Y  PA  CTS  SG  PS  OA  GA  EF  REF  IGH
    Modifier la prescription réglementaire depuis le menu  SI - PR3  null  null  null  null  null  ${EMPTY}  ${list_typ}  null  null
    # On réouvre l'ajout de nouvelle prescription
    Depuis la page d'accueil  technicien-si  technicien-si
    Accéder à l'analyse du dossier d'instruction  ${di_si}
    Éditer bloc analyse  prescriptions
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Click Element  add_a_prescription
    # On sélectionne la seule prescription réglementaire disponible
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#prescription_reglementaire    T1 - T2 - SI - PR3
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be    pr_description_om_html    Description PR3 : favorable.
    # On vérifie qu'il y a des prescriptions spécifiques-types disponibles
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    css=#specifique-type
    # On ajoute une prescription spécifique-type
    Input HTML    ps_description_om_html    Specific content
    Click Link    Insérer une ou plusieurs prescription(s) spécifique(s)-type(s)
    Select Checkbox    css=#checkbox_5
    Click Element    css=.btn_ajouter_overlay input[value=Ajouter]
    Click Element    css=#add_the_prescription
    Page Should Contain    Specific content
    Page Should Contain    Description PR3
    # On valide la modification des prescriptions
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie qu'il n'y a que la nouvelle prescription
    Page Should Contain    Prescriptions (1)
    Page Should Contain    Specific content
    Page Should Contain    Description PR3
    # On ré-ouvre sa vue de modification
    Éditer bloc analyse  prescriptions
    # On édite la nouvelle
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#edit_prescription_1
    Input HTML    ps_description_om_html    Contenu spécifique
    Click Element    css=#edit_the_prescription
    Page Should Contain    Contenu spécifique
    Page Should Not Contain    Specific content
    # On valide la modification des prescriptions
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie qu'il y a le nouveau contenu
    Page Should Contain    Prescriptions (1)
    Page Should Not Contain    Specific content
    Page Should Contain    Contenu spécifique
    # On ré-ouvre sa vue de modification
    Éditer bloc analyse  prescriptions
    # On ajoute une seconde prescription
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#add_a_prescription
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#prescription_reglementaire    T1 - T2 - SI - PR3
    Input HTML    ps_description_om_html    Seconde prescription
    Click Element    css=#add_the_prescription
    # On ajoute une troisième prescription
    Click Element    css=#add_a_prescription
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#prescription_reglementaire    T1 - T2 - SI - PR3
    Input HTML    ps_description_om_html    Troisième prescription
    Click Element    css=#add_the_prescription
    # On remonte la troisième en première position
    Element Should Contain    css=#td_ps_1    Contenu spécifique
    Click Element    css=#move_up_prescription_3
    Click Element    css=#move_up_prescription_2
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain    css=#td_ps_1    Troisième prescription
    # On vérifie l'affichage des fèches
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    css=#move_down_prescription_1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible    css=#move_up_prescription_1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    css=#move_up_prescription_2
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    css=#move_down_prescription_2
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible    css=#move_down_prescription_3
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    css=#move_up_prescription_3
    # On valide la modification des prescriptions
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie qu'il y a le nouveau contenu réordonné
    Page Should Contain    Prescriptions (3)
    Element Should Contain    css=#ps_view_1    Troisième prescription
    Element Should Contain    css=#ps_view_2    Contenu spécifique
    Element Should Contain    css=#ps_view_3    Seconde prescription
    # On réouvre sa vue de modification
    Éditer bloc analyse  prescriptions
    # On les supprime toutes en partant de la première
    # ce qui décrémente l'ordre des suivantes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#delete_prescription_1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#delete_prescription_1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=#delete_prescription_1
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie qu'il n'y a plus de prescription
    Page Should Not Contain    Prescriptions (


Modification des documents présentés
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que les documents ne sont pas déjà renseignés
    Page Should Not Contain    Trop tôt
    Page Should Not Contain    Trop tard
    # On ouvre sa vue de modification
    Éditer bloc analyse  documents_presentes
    # On modifie les documents pendant et après
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input HTML    css=#document_presente_pendant_om_html    Trop tôt
    Input HTML    css=#document_presente_apres_om_html    Trop tard
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que les nouveaux documents ont été pris en compte
    Page Should Contain    Trop tôt
    Page Should Contain    Trop tard
    # On teste les documents-types pour le champ pendant
    Éditer bloc analyse  documents_presentes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=.fieldset_pendant a
    Select Checkbox    css=#checkbox_2
    Click Element    css=.btn_ajouter_overlay input[name=add]
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    Page Should Contain    Évacuation dans les temps réglementaires.

    #
    Depuis la page d'accueil    admin    admin
    # On supprime les documents-types SI
    Go To Dashboard
    Go To Tab    document_presente
    Click Link    Conformité alarme incendie
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Form Portlet Action    document_presente    supprimer
    Click On Submit Button
    Click On Back Button
    Click Link    Conformité L16
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Form Portlet Action    document_presente    supprimer
    Click On Submit Button
    Click On Back Button
    Click Link    Conformité poste de sécurité
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click On Form Portlet Action    document_presente    supprimer
    Click On Submit Button

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On vérifie l'absence de document-type SI dans l'overlay
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Éditer bloc analyse  documents_presentes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=.fieldset_pendant a
    Page Should Contain    Aucun document-type trouvé.


Modification des essais réalisés
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On ouvre le vue de modification des essais
    Éditer bloc analyse  essais_realises
    # On sélectionne l'essai "Détection de fumée"
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox    css=#essai_3
    # On spécifie qu'il a été concluant
    Select Checkbox    css=#conc_3
    # On précise avec un complément
    Input Text    css=#comp_3    OK avec un mètre cube de fumée dispersée
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie le succès de la validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On supprime d'abord l'essai réalisé de l'analyse (pas de on cascade)
    Éditer bloc analyse  essais_realises
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Unselect Checkbox    css=#essai_3
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Puis on les supprime dans le paramétrage
    Supprimer l'essai réalisé    SI-DF
    Supprimer l'essai réalisé    ACC-ETR
    Supprimer l'essai réalisé    ACC-ETG
    Supprimer l'essai réalisé    SI-EVC

    # On revient à l'édition des essais de l'analyse
    Depuis la page d'accueil    technicien-si    technicien-si
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Éditer bloc analyse  essais_realises
    # On vérifie l'absence de paramétrage
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain    Aucun essai réalisé disponible.

    # On crée trois essais dans le paramétrage
    # 1/3 service SI sans date de validité
    Ajouter l'essai réalisé depuis le tableau  Sécurité Incendie  SI-SD  Sécurité sans date
    # 2/3 service SI avec date de début de validité
    Ajouter l'essai réalisé depuis le tableau  Sécurité Incendie  SI-AD  Sécurité avec date  null  01/01/2015
    # 3/3 service ACC sans date de validité
    Ajouter l'essai réalisé depuis le tableau  Accessibilité  ACC-SD  Accessibilité sans date

    # On ajoute à l'analyse les deux essais SI en vérifiant qu'il n'y a pas le ACC
    Depuis la page d'accueil    technicien-si    technicien-si
    Accéder à l'analyse du dossier d'instruction  ${di_si}
    Éditer bloc analyse  essais_realises
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Select Checkbox  essai_6
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Select Checkbox  essai_5
    Page Should Not Contain  Accessibilité sans date
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # On invalide un des deux essais SI
    Modifier l'essai réalisé depuis le tableau  Sécurité avec date  null  null  null  null  02/01/2015
    # On vérifie qu'il est toujours disponible en modification
    Depuis la page d'accueil    technicien-si    technicien-si
    Accéder à l'analyse du dossier d'instruction  ${di_si}
    Éditer bloc analyse  essais_realises
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Page Should Contain  Sécurité avec date
    # On le supprime
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Unselect Checkbox  essai_6
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On vérifie qu'il n'est plus disponible en modification
    Accéder à l'analyse du dossier d'instruction  ${di_si}
    Éditer bloc analyse  essais_realises
    Page Should Not Contain  Sécurité avec date

Modification du compte-rendu
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que le compte-rendu ne contient pas sa future valeur
    Page Should Not Contain    Compte-rendu en cours de rédaction
    # On ouvre sa vue de modification
    Éditer bloc analyse  compte_rendu
    # On modifie le compte-rendu
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input HTML    css=#compte_rendu_om_html    Compte-rendu en cours de rédaction
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que le nouveau compte-rendu a été pris en compte
    Page Should Contain    Compte-rendu en cours de rédaction


Modification de l'observation
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que l'observation ne contient pas sa future valeur
    Page Should Not Contain    Observation en cours de rédaction
    # On ouvre sa vue de modification
    Éditer bloc analyse  observation
    # On modifie l'observation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input HTML    css=#observation_om_html    Observation en cours de rédaction
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    # On vérifie que la nouvelle observation a été prise en compte
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain    Observation en cours de rédaction
    Page Should Not Contain Errors


Modification de l'avis proposé
    #
    Depuis la page d'accueil    admin    admin

    ##
    ## AVIS DE REUNION 01
    ##
    &{reuavis01} =  Create Dictionary
    ...  code=FAV
    ...  libelle=Sous réserve
    ...  description=Des modifications sont à apporter
    ...  service=Sécurité Incendie
    Ajouter l'avis de réunion  ${reuavis01}
    Set Suite Variable  ${reuavis01}

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que le type d'avis n'a pas encore été défini
    Page Should Not Contain    Avis : Sous réserve
    Page Should Not Contain    Des modifications sont à apporter
    # On vérifie que le complément d'avis n'a pas encore été défini
    Page Should Not Contain    Aucune réserve formulée
    # On ouvre sa vue de modification
    Éditer bloc analyse  avis_propose
    # On modifie le type d'avis
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#reunion_avis    Choisir Avis de l'analyse
    # On modifie le complément
    Input Text    css=#avis_complement    Aucune réserve formulée
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie l'erreur de validation du formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Error Message Should Be    L'avis d'analyse renseigné est invalide.
    # On remodifie le type d'analyse par un existant
    Select From List By Label    css=#reunion_avis    Sous réserve
    # On revalide le formulaire, le complément a dû être gardé
    Submit Form    css=#form_analyses_modifier
    # On vérifie cette fois la validation du formulaire
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que le nouveau type d'avis a été pris en compte
    Page Should Contain    Avis : Sous réserve
    Page Should Contain    Des modifications sont à apporter
    # On vérifie que le nouveau complément d'avis a été pris en compte
    Page Should Contain    Aucune réserve formulée

Modification de la proposition de décision autorité de police de l'analyse
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que l'analyse ne contient pas sa future valeur
    Page Should Not Contain    Décision proposée 1
    Page Should Not Contain    Délai proposé 1
    Page Should Not Contain    Décision proposée 2
    Page Should Not Contain    Délai proposé 2
    # On ouvre sa vue de modification
    Éditer bloc analyse  proposition_decision_ap
    # On modifie les deux propositions
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Input Text  dec1  Décision proposée 1
    Input Text  delai1  Délai proposé 1
    Input Text  dec2  Décision proposée 2
    Input Text  delai2  Délai proposé 2
    # On valide le formulaire
    Submit Form    css=#form_analyses_modifier
    # On vérifie la bonne validation
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On retourne à la vue analyse
    Click Element    css=#sousform-analyses a.retour
    Sleep    1
    Page Should Not Contain Errors
    # On vérifie que les nouvelles valeurs ont été prises en compte
    Page Should Contain    Décision proposée 1
    Page Should Contain    Délai proposé 1
    Page Should Contain    Décision proposée 2
    Page Should Contain    Délai proposé 2

Vérifier le workflow d'analyse
    [Documentation]    On doit pouvoir dans l'ordre :
    ...    terminer, valider et ré-ouvrir une analyse.
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On vérifie que de base l'analyse est en cours de rédaction
    Vérifier état en cours
    # On termine la rédaction de l'analyse
    Cliquer action analyse    finaliser
    Vérifier état terminé    true    true
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Rédaction terminée.
    # On valide la rédaction de l'analyse
    Cliquer action analyse    valider
    Vérifier état validé
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Validation de l'analyse.
    # On réouvre la rédaction de l'analyse
    Cliquer action analyse    reouvrir_validee
    Vérifier état en cours
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    Réouverture de l'analyse.


Widget analyses à valider
    [Documentation]    Un cadre doit voir dans son tableau de bord les analyses terminées afin de les valider.

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On termine une analyse
    # On ouvre l'analyse du DI du jeu de données
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    # On termine l'analyse
    Click On Form Portlet Action    analyses    finaliser

    # On se reconnecte en cadre sécurité
    Depuis la page d'accueil    cadre-si    cadre-si
    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le DI
    Click On Link    ${di_si}
    # On clique sur l'onglet Analyse
    Click On Link    analyse
    Page Should Not Contain Errors
    # On valide l'analyse
    Click On Form Portlet Action    analyses    valider
    # On vérifie qu'elle n'est plus dans le widget
    Go To Dashboard
    Page Should Not Contain    Visite de réception sécurité
    # On revient l'analyse pour la ré-ouvrir
    Accéder à l'analyse du dossier d'instruction    ${di_si}
    Cliquer action analyse    reouvrir_validee
    Vérifier état en cours


Génération des éditions sur l'analyse
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement du sous-sol
    ...  date_demande=25/11/2014
    ...  a_qualifier=false
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}
    Set Suite Variable  ${dc03}
    Set Suite Variable  ${dc03_libelle}

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    # On accède à l'analyse SI du DI
    Accéder à l'analyse du dossier d'instruction    ${dc03_libelle}-SI
    # On édite le rapport SI
    Cliquer action analyse    rapport_analyse
    # On ouvre le PDF
    Open PDF    form
    # On revient à la fenêtre principale
    Close PDF
    # On édite le compte-rendu SI
    Cliquer action analyse    compte_rendu_analyse
    # On ouvre le PDF
    Open PDF    form
    # On revient à la fenêtre principale
    Close PDF

    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    # On accède à l'analyse ACC du DI
    Accéder à l'analyse du dossier d'instruction    ${dc03_libelle}-ACC
    # On édite le rapport ACC
    Cliquer action analyse    rapport_analyse
    # On ouvre le PDF
    Open PDF    form
    # On revient à la fenêtre principale
    Close PDF
