*** Settings ***
Documentation  Lien avec le référentiel ADS.

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown


*** Test Cases ***
Constitution du jeu de données

    [Documentation]  ...

    #
    Depuis la page d'accueil  admin  admin
    Activer l'option référentiel ADS

    Ajouter le paramètre depuis le menu  ads__liste_services__si  SI;si;  null

    ##
    ## SIGNATAIRE 01
    ##
    &{signataire01} =  Create Dictionary
    ...  nom=Dubois 170
    ...  prenom=Julien
    ...  signature=Monsieur le Maire, Julien Dubois
    ...  civilite=M.
    ...  signataire_qualite=Maire
    Ajouter le signataire  ${signataire01}
    Set Suite Variable  ${signataire01}

    ##
    ## AVIS DE REUNION 01
    ##
    &{reuavis01} =  Create Dictionary
    ...  code=FAV170
    ...  libelle=Favorable170
    ...  description=Favorable170
    ...  service=Sécurité Incendie
    ...  categorie=Favorable
    Ajouter l'avis de réunion  ${reuavis01}
    Set Suite Variable  ${reuavis01}


    ##
    ## CATEGORIE DE REUNION 01
    ##
    &{reucategorie01} =  Create Dictionary
    ...  code=DIV170
    ...  libelle=DiversTest170
    ...  description=Tout ce ne qui ne rentre pas dans les autres catégories
    ...  service=Sécurité Incendie
    ...  ordre=40
    Ajouter la catégorie de réunion  ${reucategorie01}
    Set Suite Variable  ${reucategorie01}

    ##
    ## INSTANCE DE REUNION 01
    ##
    &{reuinstance01} =  Create Dictionary
    ...  code=MP170
    ...  libelle=Marins Pompier 170
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance01}
    Set Suite Variable  ${reuinstance01}

    ##
    ## TYPE DE REUNION 01
    ##
    @{categories_autorisees}  Create List  ${reucategorie01.libelle}
    @{avis_autorises}  Create List  ${reuavis01.libelle}
    @{instances_autorisees}  Create List  ${reuinstance01.libelle}
    &{reutype01} =  Create Dictionary
    ...  code=NC170
    ...  libelle=Ordre du jour Test 170
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype01}
    Set Suite Variable  ${reutype01}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=${DATE_FORMAT_DD/MM/YYYY}
    ...  date_reunion_yyyy_mm_dd=${DATE_FORMAT_YYYY-MM-DD}
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =  Set Variable  ${reutype01.code}-${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion01}
    Set Suite Variable  ${reunion01_code}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=LEVALET ETS TEST170
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=R
    ...  etablissement_categorie=1
    ...  siret=73282932000074
    ...  adresse_numero=12
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  exp_civilite=M.
    ...  exp_nom=DEUXRE
    ...  exp_prenom=Javier
    ...  etablissement_etat=Ouvert
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=true
    ...  dossier_autorisation_ads=AT0130551300001
    ...  dossier_instruction_ads=AT0130551300001P0
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}
    Set Suite Variable  ${dc01_di_si}  ${dc01_libelle}-SI
    Set Suite Variable  ${dc01_di_acc}  ${dc01_libelle}-ACC

    ##
    ## DOSSIER DE COORDINATION 02
    ##
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=true
    ...  dossier_autorisation_ads=AT0130551300001
    ...  dossier_instruction_ads=AT0130551300001P0
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}
    Set Suite Variable  ${dc02_di_si}  ${dc02_libelle}-SI
    Set Suite Variable  ${dc02_di_acc}  ${dc02_libelle}-ACC


Les champs "dossier d'autorisation ADS" et "dossier d'instruction ADS" sur les DC et DI

    [Documentation]  Comportement des champs "dossier d'autorisation ADS" et
    ...  "dossier d'instruction ADS" :
    ...
    ...  - Cas d'un DC non connecté au référentiel ADS : Ces deux champs sont
    ...    modifiables par l'utilisateur qui a le droit de modifier le DC. Lors
    ...    de la validation du formulaire, si l'option 'référentiel ADS' est
    ...    activée on vérifie si les valeurs saisies existent dans le
    ...    référentiel ADS. Si elles n'existent pas on indique à l'utilisateur
    ...    que les dossiers n'existent pas. La valeur saisie doit être la
    ...    référence ADS sans espace.
    ...  - Cas d'un DC connecté au référentiel ADS :  Ces deux champs sont
    ...    remplis par le référentiel ADS et non modifiables.
    ...  - Lien vers openADS sur le champ "dossier d'autorisation ADS" : Si
    ...    l'option 'référentiel ADS' est activée et qu'une valeur est présente
    ...    dans le champ "dossier d'autorisation ADS" alors un lien apparaît à
    ...    côté de la valeur et permet lors du clic sur ce lien d'ouvrir une
    ...    nouvelle fenêtre vers le lien paramétré dans le script de config
    ...    dyn/services.inc.php ($ADS_URL_VISUALISATION_DA) avec une variable
    ...    de remplacement <ID_DA> pour remplacer dans le lien cette chaîne par
    ...    le numéro du dossier d'autorisation ADS.


    #
    Depuis la page d'accueil  cadre-si  cadre-si

    # Valeurs par défaut pour la saisie des DC
    &{values} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Aménagement du sous-sol
    ...  date_demande=08/01/2015
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=true

    # Cas d'un DC non connecté au référentiel ADS : champs modifiables avec
    # correspondace dans le référentiel ADS si une valeur est saisie

    ## Aucune valeur saisie > validation du formulaire > pas de lien
    Depuis le formulaire d'ajout du dossier de coordination
    Saisir les valeurs dans le formulaire du dossier de coordination  ${values}
    # Aucune valeur
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # Vérification de l'absence du lien vers openADS
    Click On Back Button
    Element Should Not Be Visible  css=#dossier_autorisation_ads_link_openads

    ## Au moins une valeur saisie > validation du formulaire > lien
    Depuis le formulaire d'ajout du dossier de coordination
    Saisir les valeurs dans le formulaire du dossier de coordination  ${values}
    # DI vide et DA inexistant
    Input Text  css=#dossier_autorisation_ads  DAquinexistepas
    Click On Submit Button
    Error Message Should Contain  Le dossier d'autorisation saisi n'existe pas dans le référentiel ADS.
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # Les deux sont inexistant
    Input Text  css=#dossier_instruction_ads  DIquinexistepas
    Click On Submit Button
    Error Message Should Contain  Le dossier d'autorisation saisi n'existe pas dans le référentiel ADS.
    Error Message Should Contain  Le dossier d'instruction saisi n'existe pas dans le référentiel ADS.
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # DA vide et DI inexistant
    Input Text  css=#dossier_autorisation_ads  ${EMPTY}
    Click On Submit Button
    Error Message Should Contain  Le dossier d'instruction saisi n'existe pas dans le référentiel ADS.
    Error Message Should Contain  SAISIE NON ENREGISTRÉE
    # Les deux valeurs existent dans le référentiel
    Input Text  css=#dossier_autorisation_ads  AT0130551300001
    Input Text  css=#dossier_instruction_ads  AT0130551300001P0
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # Vérification de la présence du lien vers openADS
    Click On Back Button
    Element Should Be Visible  css=#dossier_autorisation_ads_link_openads


Processus AT

    [Documentation]  ...

    #
    ${dossier_instruction_ads} =  Set Variable  AT0990991000001P0


    ##
    ## Échange [108]
    ## > ADS_ERP__AT__DEPOT_INITIAL
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AT__DEPOT_INITIAL
    ${emetteur} =  Set Variable  108_guichetunique_at_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "01/01/2014 12:00", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    #
    ${json} =  Set Variable  { "module": "messagesasync" }
    Vérifier le code retour du web service et vérifier que son message contient  Post  maintenance  ${json}  200  Traitement terminé.
    # On récupère le libellé du dossier de coordination grâce à l'emetteur du message supposé unique dans les tests
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Dossiers > Messages > Tous Les Messages > 01/01/2014 12:00:00
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=span#dossier_coordination
    ${dc_libelle} =  Get Text  css=span#dossier_coordination
    # On vérifie que l'onglet Message(s) du DC contient bien un seul message et que c'est celui qui vient d'être envoyé
    Depuis le listing des messages dans le contexte du dossier de coordination  ${dc_libelle}
    ELement Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  1 - 1 enregistrement(s) sur 1
    ELement Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${type}
    ELement Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${emetteur}
    # On vérifie que les marqueurs de lecture sont corrects
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc #type  ${type}
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_non_lu


    # [207] L'échange ne doit pas se produire sur une AT
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${dc_libelle}
    Click On Form Portlet Action    dossier_coordination    marquer_a_enjeu
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Marqueur 'enjeu ERP' activé.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (207) du référentiel ADS OK.
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${dc_libelle}
    Click On Form Portlet Action    dossier_coordination    demarquer_a_enjeu
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Marqueur 'enjeu ERP' désactivé.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (207) du référentiel ADS OK.

    ##
    ## Échange [101]
    ## > ADS_ERP__AT__INFORMATION_DE_QUALIFICATION_ADS
    ## > Marqueur(s) de lecture du message : mode 1.
    ##
    ${type} =  Set Variable  ADS_ERP__AT__INFORMATION_DE_QUALIFICATION_ADS
    ${emetteur} =  Set Variable  101_instr_at_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu" : { "competence" : "aa", "contraintes_plu" : "aa", "references_cadastrales" : "aa" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    # On vérifie que l'onglet Message(s) du DC contient bien le message qui vient d'être envoyé
    Depuis le listing des messages dans le contexte du dossier de coordination  ${dc_libelle}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  1 - 2 enregistrement(s) sur 2
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${type}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${emetteur}
    # On vérifie que les marqueurs de lecture sont corrects
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc #type  ${type}
    Portlet Action Should Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_non_lu


    ##
    ## Échange [113]
    ## > ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_at_test_170_msg1
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    ##
    ## Qualification de l'AT pour créer les DI
    ##
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le formulaire de modification du dossier de coordination  ${dc_libelle}
    # On décoche "a_qualifier"
    Unselect Checkbox  css=#a_qualifier
    # On coche les deux DI
    Select Checkbox  css=#dossier_instruction_secu
    Select Checkbox  css=#dossier_instruction_acc
    # On sélectionne un établissement
    Input Text  css=#autocomplete-etablissement_tous-search  ${etab01_titre}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link  ${etab01_titre}
    # On sélectionne des types secondaires pour pouvoir vérifier qu'ils sont
    # correctement récupérés lors de l'échange [110]
    Select From List By Label  css=#etablissement_type_secondaire  [CTS] Chapiteaux, Tentes et Structures toile  [EF] Établissements flottants (eaux intérieures)
    # On valide le formulaire et vérifie que les messages sont corrects.
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Le dossier d'instruction ${dc_libelle}-SI a été créé.
    Valid Message Should Contain  Le dossier d'instruction ${dc_libelle}-ACC a été créé.
    # On vérifie que les types secondaires sélectionnés sont bien ceux saisis
    Depuis le formulaire de modification du dossier de coordination  ${dc_libelle}
    Element Should Contain  css=#etablissement_type_secondaire  [CTS] Chapiteaux, Tentes et Structures toile
    Element Should Contain  css=#etablissement_type_secondaire  [EF] Établissements flottants (eaux intérieures)


    ##
    ## Échange [210]
    ## > ERP_ADS__AT__MAJ_COMPLETUDE_INCOMPLETUDE
    ##
    ${type} =  Set Variable  ERP_ADS__AT__MAJ_COMPLETUDE_INCOMPLETUDE
    Depuis la page d'accueil  technicien-si  technicien-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  gerer_completude
    Element Should Contain  css=#fieldset-form-dossier_instruction-gestion-de-la-completude  Gestion de la complétude
    Select Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  plop
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (210) du référentiel ADS OK.
    # On vérifie que l'onglet Message(s) du DC contient bien le message qui vient d'être envoyé
    Depuis le listing des messages dans le contexte du dossier de coordination  ${dc_libelle}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  1 - 4 enregistrement(s) sur 4
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${type}
    # On vérifie que si on décoche la case le message n'est pas envoyé (on ne notifie pas une complétude sur une AT)
    Depuis la page d'accueil  technicien-si  technicien-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  gerer_completude
    Element Should Contain  css=#fieldset-form-dossier_instruction-gestion-de-la-completude  Gestion de la complétude
    Unselect Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  plip
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (210) du référentiel ADS OK.
    # Le profil CADRE n'a pas accès à l'action de gestion de la complétude
    # Dans le formulaire de modification du DI, il peut modifier les informations de complétude
    # Si il modifie la case à cocher et/ou la liste des pièces
    # alors on envoi la notification (210) si on est dans un cas d'incomplétude
    # > Aucune modification, on envoi rien
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (210) du référentiel ADS OK.
    # > Modification des deux champs > incomplétude
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Select Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  azerty
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (210) du référentiel ADS OK.
    # > Aucune modification, on envoi rien
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (210) du référentiel ADS OK.
    # > Modification des deux champs > complétude
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Unselect Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  qwerty
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (210) du référentiel ADS OK.


    ##
    ## Échange [112]
    ## > ADS_ERP__AT__DEPOT_DE_PIECE_PAR_LE_PETITIONNAIRE
    ## > Marqueur(s) de lecture du message : mode 3.
    ##
    ${type} =  Set Variable  ADS_ERP__AT__DEPOT_DE_PIECE_PAR_LE_PETITIONNAIRE
    ${emetteur} =  Set Variable  112_guichetunique_at_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "type_piece" : "complémentaire" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    # On vérifie que l'onglet Message(s) du DC contient bien le message qui vient d'être envoyé
    Depuis le listing des messages dans le contexte du dossier de coordination  ${dc_libelle}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  1 - 6 enregistrement(s) sur 6
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${type}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${emetteur}
    # On vérifie que les marqueurs de lecture sont corrects
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc #type  ${type}
    Portlet Action Should Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_non_lu


    ##
    ## Échange [113]
    ## > ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_at_test_170_msg2
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    ##
    ## Échange [109]
    ## > ADS_ERP__AT__RETRAIT_DU_PETITIONNAIRE
    ## > Marqueur(s) de lecture du message : mode 3.
    ##
    ${type} =  Set Variable  ADS_ERP__AT__RETRAIT_DU_PETITIONNAIRE
    ${emetteur} =  Set Variable  109_admin_at_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    # On vérifie que l'onglet Message(s) du DC contient bien le message qui vient d'être envoyé
    Depuis le listing des messages dans le contexte du dossier de coordination  ${dc_libelle}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  1 - 8 enregistrement(s) sur 8
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${type}
    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc  ${emetteur}
    # On vérifie que les marqueurs de lecture sont corrects
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain  css=#sousform-dossier_coordination_message_contexte_dc #type  ${type}
    Portlet Action Should Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_si_technicien_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_cadre_comme_non_lu
    Portlet Action Should Not Be In SubForm  dossier_coordination_message_contexte_dc  marquer_acc_technicien_comme_non_lu


    ##
    ## Échange [113]
    ## > ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_at_test_170_msg3
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    ##
    ## Qualification des DI pour pouvoir clôturer le DC
    ##
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-SI
    Qualifier le dossier d'instruction    ${dc_libelle}-SI
    Depuis la page d'accueil  cadre-acc  cadre-acc
    Depuis le contexte du dossier d'instruction  ${dc_libelle}-ACC
    Qualifier le dossier d'instruction    ${dc_libelle}-ACC


    ##
    ## Échange [211]
    ## > ERP_ADS__AT__MAJ_CLOTURE
    ##
    ${type} =  Set Variable  ERP_ADS__AT__MAJ_CLOTURE
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination    ${dc_libelle}
    Click On Form Portlet Action  dossier_coordination  cloturer
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Le dossier de coordination a été correctement cloturé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (211) du référentiel ADS OK.


    ##
    ## Échange [110]
    ## > ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP
    ${emetteur} =  Set Variable  110_guichetunique_at_test_170_msg1
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message 'ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP' OK.
    # On récupère le libellé du dossier de coordination grâce à l'emetteur du message supposé unique dans les tests
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Dossiers > Messages > Tous Les Messages > 31/12/2015 14:42:00
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=span#dossier_coordination
    ${dc_vr_libelle} =  Get Text  css=span#dossier_coordination
    ## Double envoi
    ${type} =  Set Variable  ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP
    ${emetteur} =  Set Variable  110_guichetunique_at_test_170_msg2
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message 'ADS_ERP__AT__DEMANDE_DE_VISITE_D_OUVERTURE_ERP' OK.
    # On vérifie que le dossier parent a correctement été sélectionné ainsi
    # que les données qui le composent : type / catégorie / locaux à sommeil
    # / erp / types secondaires.
    Depuis le contexte du dossier de coordination  ${dc_vr_libelle}
    Form Static Value Should Be  etablissement_type  [R] Etablissements d'enseignement colonies de vacances
    Form Static Value Should Be  etablissement_categorie  [1] plus de 1500 personnes
    Form Static Value Should Be  etablissement_locaux_sommeil  Non
    Form Static Value Should Be  erp  Oui
    Element Should Contain  css=#etablissement_type_secondaire  [CTS] Chapiteaux, Tentes et Structures toile
    Element Should Contain  css=#etablissement_type_secondaire  [EF] Établissements flottants (eaux intérieures)
    Link Value Should Be  dossier_coordination_parent  ${dc_libelle}

    ##
    ## Échange [113]
    ## > ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_at_test_170_msg4
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    # On vérifie que le message 113 a bien été ajouté sur l'at et le vr
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${dc_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${dc_vr_libelle}

    ##
    ## Échange [201]
    ## > ERP_ADS__MAJ_NUMERO_ERP_DOSSIER_AUTORISATION
    ##
    ${type} =  Set Variable  ERP_ADS__MAJ_NUMERO_ERP_DOSSIER_AUTORISATION
    ##
    ## Échange [208]
    ## > ERP_ADS__AT__MAJ_ARRETE_ERP_DOSSIER_AUTORISATION
    ##
    ${type} =  Set Variable  ERP_ADS__AT__MAJ_ARRETE_ERP_DOSSIER_AUTORISATION
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC02    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC02 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc_libelle}
    ...  ${params}
    ...  (*) Décision
    ...  ME - Arrêté d'ouverture
    Click On Back Button In Subform
    Click On Link  AQWEDC02 Jacques
    ${docgen01_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen01_code_barres} =  Get Text  css=#code_barres
    Finaliser le document généré  ${docgen01_code_barres}
    Depuis le contexte du document généré    ${docgen01_code_barres}
    Click On Form Portlet Action    courrier    modifier
    Add File    om_fichier_signe_courrier    pv_signe.pdf
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (201) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (202) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (208) du référentiel ADS OK.
    #
    Depuis le contexte du document généré    ${docgen01_code_barres}
    Click On Form Portlet Action    courrier    modifier
    Suivi des dates du documente généré     null    null    null    null    null     ${DATE_FORMAT_DD/MM/YYYY}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (201) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (202) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (208) du référentiel ADS OK.


    ##
    ## Échange [113]
    ## > ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ## > Marqueur(s) de lecture du message : mode 0.
    ##
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_at_test_170_msg5
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "${dossier_instruction_ads}", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.



Processus PC

    [Documentation]  ...

    # [102]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_COMPLETUDE_ERP
    ${emetteur} =  Set Variable  102_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [103]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_QUALIFICATION_ERP
    ${emetteur} =  Set Variable  103_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [113]
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  104_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # On récupère le libellé du dossier de coordination grâce à l'emetteur du message supposé unique dans les tests
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Dossiers > Messages > Tous Les Messages > 31/12/2015 14:42:00
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=span#dossier_coordination
    ${pc_libelle} =  Get Text  css=span#dossier_coordination



    # [114]
    #
    ${type} =  Set Variable  ADS_ERP__PC__ENJEU_ADS
    ${emetteur} =  Set Variable  104_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0", "contenu" : { "Dossier à enjeu ADS" : "oui"} }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    Depuis le contexte du dossier de coordination  ${pc_libelle}
    Element should contain  fieldset-form-dossier_coordination-informations-generales  enjeu ADS

    # [207]
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${pc_libelle}
    Click On Form Portlet Action    dossier_coordination    marquer_a_enjeu
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Marqueur 'enjeu ERP' activé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (207) du référentiel ADS OK.
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier de coordination  ${pc_libelle}
    Click On Form Portlet Action    dossier_coordination    demarquer_a_enjeu
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Marqueur 'enjeu ERP' désactivé.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (207) du référentiel ADS OK.

    ##
    ## [206]
    ##
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le formulaire de modification du dossier de coordination  ${pc_libelle}
    Unselect Checkbox  css=#a_qualifier
    Select Checkbox  css=#dossier_instruction_secu
    Select Checkbox  css=#dossier_instruction_acc
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01_titre}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Link    ${etab01_titre}
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Notification (206) du référentiel ADS OK.
    Valid Message Should Contain    Le dossier d'instruction ${pc_libelle}-SI a été créé.
    Valid Message Should Contain    Le dossier d'instruction ${pc_libelle}-ACC a été créé.

    # [205]
    Depuis la page d'accueil  technicien-si  technicien-si
    Depuis le contexte du dossier d'instruction  ${pc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  gerer_completude
    Element Should Contain  css=#fieldset-form-dossier_instruction-gestion-de-la-completude  Gestion de la complétude
    Select Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  plip
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (205) du référentiel ADS OK.
    # Le profil CADRE n'a pas accès à l'action de gestion de la complétude
    # Dans le formulaire de modification du DI, il peut modifier les informations de complétude
    # Si il modifie la case à cocher et/ou la liste des pièces
    # alors on envoi la notification (205)
    # > Aucune modification, on envoi rien
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${pc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (205) du référentiel ADS OK.
    # > Modification des deux champs > incomplétude
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${pc_libelle}-SI
    Click On Form Portlet Action  dossier_instruction  modifier
    Unselect Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  azerty
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (205) du référentiel ADS OK.

    # [204]
    Depuis la page d'accueil  technicien-acc  technicien-acc
    Depuis le contexte du dossier d'instruction  ${pc_libelle}-ACC
    Click On Form Portlet Action  dossier_instruction  gerer_completude
    Element Should Contain  css=#fieldset-form-dossier_instruction-gestion-de-la-completude  Gestion de la complétude
    Select Checkbox  css=#incompletude
    Input Text  css=#piece_attendue  ${EMPTY}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (204) du référentiel ADS OK.

    # [104]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS
    ${emetteur} =  Set Variable  104_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0", "contenu" : { "consultation" : 2, "date_envoi" : "01/03/2013", "service_abrege" : "SI", "service_libelle" : "Service Sécurité", "date_limite" : "01/04/2013" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    ##
    ## [209]
    ##
    Depuis la page d'accueil  cadre-si  cadre-si
    # Création de la demande de passage en réunion puis saisie de l'avis
    &{dpr01} =  Create Dictionary
    ...  dossier_instruction=${pc_libelle}-SI
    ...  date_souhaitee=${DATE_FORMAT_DD/MM/YYYY}
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_type_categorie=${reucategorie01.libelle}
    ...  reunion_code=${reunion01_code}
    ...  reunion_date_format_yyyy_mm_dd=${reunion01.date_reunion_yyyy_mm_dd}
    Planifier directement le DC ou DI pour la réunion dans la catégorie
    ...  ${dpr01.dossier_instruction}
    ...  ${dpr01.reunion_code}
    ...  ${dpr01.reunion_type_categorie}
    Set Suite Variable  ${dpr01}
    Numéroter l'ordre du jour de la réunion  ${reunion01_code}
    # On rédige la proposition d'avis de l'analyse sécurité incendie
    Accéder à l'analyse du dossier d'instruction  ${pc_libelle}-SI
    Éditer bloc analyse  avis_propose
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    css=#reunion_avis    ${reuavis01.libelle}
    Input Text    css=#avis_complement    Nécessite une nouvelle visite
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click Element    css=#sousform-analyses a.retour
    Accéder à l'analyse du dossier d'instruction  ${pc_libelle}-SI
    Terminer l'analyse
    Valider l'analyse
    On clique sur l'onglet  proces_verbal  PV
    L'action 'Générer un nouveau PV' doit être disponible
    # On clique sur le bouton 'Générer un nouveau PV'
    Click Element  css=#generer_pv
    # On vérifie que le formulaire de génération est correctement ouvert
    # en vérifiant qu'un des champs du formulaire est présent à l'écran
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#sousform-proces_verbal  odèle d'édition à utiliser lors de la génération
    # On vérifie que la page ne contient pas d'erreurs
    Page Should Not Contain Errors
    # On sélectionne un passage en réunion
    Select From List By Label  css=#dossier_instruction_reunion  ${reunion01.date_reunion} - ${reutype01.libelle}
    # On sélectionne un signataire
    Select From List By Label  css=#signataire  ${signataire01.civilite} ${signataire01.signataire_qualite} ${signataire01.nom} ${signataire01.prenom}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie que le message de validation est correct
    Valid Message Should Contain In Subform  Vos modifications ont bien été enregistrées.
    # On vérifie que la page ne contient pas d'erreurs
    Page Should Not Contain Errors
    # On récupère le numéro de PV présent dans le message de validation
    ${dc01_di_si_pv01_numero} =  Get Text  css=#new_pv_number
    # On la valeur pour une utilisation dans la suite du test
    Set Suite Variable  ${dc01_di_si_pv01_numero}
    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin
    ${docgen01_id} =  Récupérer l'identifiant du document généré lié depuis le procès verbal numéro  ${pc_libelle}-SI  ${dc01_di_si_pv01_numero}
    ${docgen01_code_barres} =  Récupérer le code barres du document généré à partir de son identifiant  ${docgen01_id}
    Set Suite Variable  ${docgen01_id}
    Set Suite Variable  ${docgen01_code_barres}
    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le procès verbal généré dans le contexte du DI  ${pc_libelle}-SI  ${dc01_di_si_pv01_numero}
    Portlet Action Should Not Be In SubForm  proces_verbal  send_pv_to_referentiel_ads
    #
    Depuis le contexte du document généré    ${docgen01_code_barres}
    Portlet Action Should Not Be In Form  courrier  send_pv_to_referentiel_ads
    Click On Form Portlet Action    courrier    modifier
    Suivi des dates du documente généré     null    null    null    null    null     ${DATE_FORMAT_DD/MM/YYYY}
    Add File    om_fichier_signe_courrier    pv_signe.pdf
    Click On Submit Button
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    #
    Depuis le procès verbal généré dans le contexte du DI  ${pc_libelle}-SI  ${dc01_di_si_pv01_numero}
    Portlet Action Should Be In SubForm  proces_verbal  send_pv_to_referentiel_ads
    #
    Depuis le contexte du document généré    ${docgen01_code_barres}
    Click On Form Portlet Action  courrier  send_pv_to_referentiel_ads
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Error Message Should Contain  Un avis rendu sur le passage en réunion est obligatoire.
    # On rend l'avis
    Depuis l'ordre du jour de la réunion  ${reunion01_code}
    Click Element    css=table.meeting tr a
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page SubTitle Should Be  > Ordre du jour > n°1
    Click On SubForm Portlet Action    dossier_instruction_reunion_contexte_reunion    rendre_l_avis
    Select From List By Label    css=#avis    ${reuavis01.libelle}
    Click On Submit Button In Subform
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.
    #
    Depuis le contexte du document généré    ${docgen01_code_barres}
    Click On Form Portlet Action  courrier  send_pv_to_referentiel_ads
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (209) du référentiel ADS OK.


    # [105]
    ${type} =  Set Variable  ADS_ERP__PC__INFORMATION_DE_DECISION_ADS
    ${emetteur} =  Set Variable  105_instr_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0", "contenu" : { "decision" : "plop" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    # [106]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE
    ${emetteur} =  Set Variable  106_instr_daact_test_170_msg1
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001DAACT", "contenu" : { "consultation" : 2, "date_envoi" : "31/12/2015", "service_abrege" : "ACC", "service_libelle" : "Service Accessibilité", "date_limite" : "31/01/2016" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    # On récupère le libellé du dossier de coordination grâce à l'emetteur du message supposé unique dans les tests
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing de tous les messages
    Input Text  css=div#adv-search-adv-fields input#emetteur  ${emetteur}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#tab-dossier_coordination_message_tous  ${emetteur}
    Click Link  ${emetteur}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Dossiers > Messages > Tous Les Messages > 31/12/2015 14:42:00
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=span#dossier_coordination
    ${dc_daact_libelle} =  Get Text  css=span#dossier_coordination
    # Double envoi
    ${emetteur} =  Set Variable  106_instr_daact_test_170_msg2
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:45", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001DAACT", "contenu" : { "consultation" : 3, "date_envoi" : "31/12/2015", "service_abrege" : "SPGR", "service_libelle" : "Service Sécurité", "date_limite" : "31/01/2016" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    # On vérifie que le dossier parent a correctement été sélectionné ainsi
    # que les données qui le composent : type / catégorie / locaux à sommeil
    # / erp / types secondaires / a_qualifier.
    Depuis le contexte du dossier de coordination  ${dc_daact_libelle}
    Form Static Value Should Be  etablissement_type  [R] Etablissements d'enseignement colonies de vacances
    Form Static Value Should Be  etablissement_categorie  [1] plus de 1500 personnes
    Form Static Value Should Be  etablissement_locaux_sommeil  Non
    Form Static Value Should Be  erp  Oui
    Form Static Value Should Be  a_qualifier  Non
    Link Value Should Be  dossier_coordination_parent  ${pc_libelle}


    # [105]
    ${type} =  Set Variable  ADS_ERP__PC__INFORMATION_DE_DECISION_ADS
    ${emetteur} =  Set Variable  105_instr_daact_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001DAACT", "contenu" : { "decision" : "plop" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.


    # [107]
    ${type} =  Set Variable  ADS_ERP__PC__DEMANDE_DE_VISITE_D_OUVERTURE_ERP
    ${emetteur} =  Set Variable  107_guichetunique_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.
    # [107] - Double envoi
    ${type} =  Set Variable  ADS_ERP__PC__DEMANDE_DE_VISITE_D_OUVERTURE_ERP
    ${emetteur} =  Set Variable  107_guichetunique_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [113]
    ${type} =  Set Variable  ADS_ERP__AJOUT_D_UNE_NOUVELLE_PIECE_NUMERISEE
    ${emetteur} =  Set Variable  113_admin_pc_test_170
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000001DAACT", "contenu": { "date_creation" : "31/12/2015", "nom_fichier" : "DGIMPC.pdf", "type" : "Imprimé de demande de permis de construire", "categorie" : "Définition Générale" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    Depuis la page d'accueil  admin  admin

    ##
    ## DEMANDE DE PASSAGE EN REUNION 01
    ##
    # On ajoute une demande de passage en réunion
    ${dpr01_id} =    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${pc_libelle}
    ...  SI
    ...  ${DATE_FORMAT_DD/MM/YYYY}
    ...  ${reutype01.libelle}
    ...  ${reucategorie01.libelle}
    Set Suite Variable    ${dpr01_id}
    # Par défaut c'est la date du jour qui est positionnée
    &{dpr01} =  Create Dictionary
    ...  dossier_instruction=${dc01_di_si}
    ...  date_souhaitee=${DATE_FORMAT_DD/MM/YYYY}
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_type_categorie=${reucategorie01.libelle}
    ...  reunion_code=${reunion01_code}
    ...  reunion_date_format_yyyy_mm_dd=${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${dpr01}
    Planifier toutes les demandes de passages pressenties pour la réunion    ${reunion01_code}

    ##
    ## TYPE DE DECISION D'AUTORITE DE POLICE 01
    ##
    &{typedap01} =  Create Dictionary
    ...  code=ART01
    ...  libelle=Arrêté d'ouverture
    ...  description=
    ...  service=Sécurité Incendie
    ...  suivi_delai=
    ...  avis=favorable
    ...  etablissement_etat=Ouvert
    ...  delai=
    ...  type_arrete=true
    ...  arrete_reglementaire=Oui
    ...  arrete_notification=Non
    ...  arrete_publication=Choisir...
    ...  arrete_temporaire=Oui
    # ...  nomenclature_actes_nature=
    # ...  nomenclature_actes_matiere_niv1=
    # ...  nomenclature_actes_matiere_niv2=
    Ajouter le type de décision d'autorité de police  ${typedap01}
    Set Suite Variable  ${typedap01}

    ##
    ## Échange [201]
    ## > ERP_ADS__MAJ_NUMERO_ERP_DOSSIER_AUTORISATION
    ##
    ${type} =  Set Variable  ERP_ADS__MAJ_NUMERO_ERP_DOSSIER_AUTORISATION
    ##
    ## Échange [202]
    ## > ERP_ADS__MAJ_STATUT_ERP_DOSSIER_AUTORISATION
    ##
    ${type} =  Set Variable  ERP_ADS__MAJ_STATUT_ERP_DOSSIER_AUTORISATION
    #
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC03    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC03 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${pc_libelle}
    ...  ${params}
    ...  (*) Décision
    ...  ME - Arrêté d'ouverture
    Click On Back Button In Subform
    Click On Link  AQWEDC03 Jacques
    ${docgen02_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen02_code_barres} =  Get Text  css=#code_barres
    Finaliser le document généré  ${docgen02_code_barres}

    #
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion  ${reunion01_code}  ${dpr01_id}
    Sleep  1
    Execute JavaScript  window.jQuery("#formSpecificContent_autorite_police #liste_autorite_police").attr("style", "display:table;");
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#formSpecificContent_autorite_police #liste_autorite_police
    Click Element  css=#add_autorite_police
    Select From List By Label    css=#autorite_police_decision    Arrêté d'ouverture
    Input Text    css=#delai    10
    Select From List By Label    css=#autorite_police_motif  Vérifications
    Select From List By Value    css=#courriers_lies    ${docgen02_id}
    Click Element  css=#sousform-autorite_police .om-button
    Sleep  1
    Click Element  css=#sousform-autorite_police a.retour
    #
    Depuis le contexte du document généré    ${docgen02_code_barres}
    Click On Form Portlet Action    courrier    modifier
    Add File    om_fichier_signe_courrier    pv_signe.pdf
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (201) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (202) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (208) du référentiel ADS OK.
    #
    Depuis le contexte du document généré    ${docgen02_code_barres}
    Click On Form Portlet Action    courrier    modifier
    Suivi des dates du documente généré     null    null    null    null    null     ${DATE_FORMAT_DD/MM/YYYY}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (201) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Notification (202) du référentiel ADS OK.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=div.message.ui-state-valid p span.text  Notification (208) du référentiel ADS OK.


Déconstitution du jeu de données

    [Documentation]  ...

    Depuis la page d'accueil  admin  admin
    Désactiver l'option référentiel ADS


