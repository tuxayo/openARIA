<?php
/**
 * Ce fichier permet de déclarer la classe referentieladsditest, ressource
 * exposée à travers l'interface REST qui hérite de la classe de base Services.
 * Cette classe servira pour les tests afin de simuler l'échange avec le
 * référentiel patrimoine
 *
 * @package openaria
 * @version SVN : $Id: referentieladsditest.php 1526 2015-12-18 17:07:38Z fmichon $
 */
 
//Inclusion de la classe de base Services
include_once('../services/REST/services.php');

/**
 * Cette classe définie la ressource 'referentieladsditest' qui permet de
 * tester l'échange avec le référentiel patrimoine
 */
class referentieladsditest extends Services {

    public function put($id, $request_data) {
        header("HTTP/1.0 200 OK");
        header('Content-Type: application/json');
        echo json_encode(array());
    }

    /**
     * Cette méthode permet de définir le traitement du GET sur une requête
     * REST. Elle retourne des données.
     *
     * @param string $id L'identifiant de la ressource
     */
    public function get($di) {
        if ($di == "PC0130421500001P0") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "PC0130421500001P0",
                "dossier_autorisation" => "PC0130421500001",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales"=> array(
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0012"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0013"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0014"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0015"
                    ),
                ),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "Dupont",
                    "particulier_prenom" => "Jean",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
                "autres_petitionnaires" => array(
                    array(
                        "demandeur" => "12",
                        "qualite" => "particulier",
                        "particulier_civilite" => "",
                        "particulier_nom" => "DUPONT",
                        "particulier_prenom" => "FRANCOIS",
                        "particulier_date_naissance" => "",
                        "particulier_commune_naissance" => "",
                        "particulier_departement_naissance" => "",
                        "numero" => "",
                        "voie" => "",
                        "complement" => "",
                        "lieu_dit" => "",
                        "localite" => "",
                        "code_postal" => "",
                        "bp" => "",
                        "cedex" => "",
                        "pays" => "France",
                        "division_territoriale" => "",
                        "telephone_fixe" => "",
                        "telephone_mobile" => "",
                        "indicatif" => "",
                        "courriel" => "",
                        "fax" => "",
                    ),
                ),
                "donnees_techniques" => array(
                    "su_tot_shon_tot" => "",
                    "su_avt_shon_tot" => "",
                ),
            ));
            return;
        } elseif ($di == "AT0130551300001P0") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "AT0130551300001P0",
                "dossier_autorisation" => "AT0130551300001",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales"=> array(
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0012"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0013"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0014"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0015"
                    ),
                ),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "Dupont",
                    "particulier_prenom" => "Jean",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
                "autres_petitionnaires" => array(
                    array(
                        "demandeur" => "12",
                        "qualite" => "particulier",
                        "particulier_civilite" => "",
                        "particulier_nom" => "DUPONT",
                        "particulier_prenom" => "FRANCOIS",
                        "particulier_date_naissance" => "",
                        "particulier_commune_naissance" => "",
                        "particulier_departement_naissance" => "",
                        "numero" => "",
                        "voie" => "",
                        "complement" => "",
                        "lieu_dit" => "",
                        "localite" => "",
                        "code_postal" => "",
                        "bp" => "",
                        "cedex" => "",
                        "pays" => "France",
                        "division_territoriale" => "",
                        "telephone_fixe" => "",
                        "telephone_mobile" => "",
                        "indicatif" => "",
                        "courriel" => "",
                        "fax" => "",
                    ),
                ),
                "donnees_techniques" => array(
                    "su_tot_shon_tot" => "",
                    "su_avt_shon_tot" => "",
                ),
            ));
            return;
        } elseif ($di == "AT0990991000001P0") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "AT0990991000001P0",
                "dossier_autorisation" => "AT0990991000001",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales" => array(
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0012"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0013"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0014"
                    ),
                    array(
                        "prefixe" => "201",
                        "quartier" => "803",
                        "section" => "A",
                        "parcelle" => "0015"
                    ),
                ),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "M.",
                    "particulier_nom" => "Dupont",
                    "particulier_prenom" => "Jean",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
                "autres_petitionnaires" => array(
                    array(
                        "demandeur" => "12",
                        "qualite" => "personne_morale",
                        "personne_morale_civilite" => "Madame",
                        "personne_morale_denomination" => "MONTGRAN",
                        "personne_morale_raison_sociale" => "",
                        "personne_morale_siret" => "",
                        "personne_morale_categorie_juridique" => "SARL",
                        "personne_morale_nom" => "DUPONT",
                        "personne_morale_prenom" => "Jeanne",
                        "numero" => "",
                        "voie" => "",
                        "complement" => "",
                        "lieu_dit" => "",
                        "localite" => "",
                        "code_postal" => "",
                        "bp" => "",
                        "cedex" => "",
                        "pays" => "France",
                        "division_territoriale" => "",
                        "telephone_fixe" => "",
                        "telephone_mobile" => "",
                        "indicatif" => "",
                        "courriel" => "",
                        "fax" => "",
                    ),
                ),
                "donnees_techniques" => array(
                    "su_tot_shon_tot" => "",
                    "su_avt_shon_tot" => "",
                ),
            ));
            return;
        } elseif ($di == "PC0990991000001P0") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "PC0990991000001P0",
                "dossier_autorisation" => "PC0990991000001",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales" => array(),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "Dupont",
                    "particulier_prenom" => "Jean",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
                "autres_petitionnaires" => array(
                    array(
                        "demandeur" => "12",
                        "qualite" => "personne_morale",
                        "personne_morale_civilite" => "Madame",
                        "personne_morale_denomination" => "MONTGRAN",
                        "personne_morale_raison_sociale" => "",
                        "personne_morale_siret" => "",
                        "personne_morale_categorie_juridique" => "SARL",
                        "personne_morale_nom" => "DUPONT",
                        "personne_morale_prenom" => "Jeanne",
                        "numero" => "",
                        "voie" => "",
                        "complement" => "",
                        "lieu_dit" => "",
                        "localite" => "",
                        "code_postal" => "",
                        "bp" => "",
                        "cedex" => "",
                        "pays" => "France",
                        "division_territoriale" => "",
                        "telephone_fixe" => "",
                        "telephone_mobile" => "",
                        "indicatif" => "",
                        "courriel" => "",
                        "fax" => "",
                    ),
                ),
                "donnees_techniques" => array(
                    "su_tot_shon_tot" => "",
                    "su_avt_shon_tot" => "",
                ),
            ));
            return;
        } elseif ($di == "PC0990991000001DAACT") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "PC0990991000001DAACT",
                "dossier_autorisation" => "PC0990991000001",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales" => array(),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "Dupont",
                    "particulier_prenom" => "Jean",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
            "autres_petitionnaires" => array(
                array(
                    "demandeur" => "12",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "DUPONT",
                    "particulier_prenom" => "FRANCOIS",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
            ),
            "donnees_techniques" => array(
                "su_tot_shon_tot" => "",
                "su_avt_shon_tot" => "",
            ),
            ));
            return;
        } elseif ($di == "PC0990991000002P0") {
            header("HTTP/1.0 200 OK");
            header('Content-Type: application/json');
            echo json_encode(array(
                "dossier_instruction" => "PC0990991000002P0",
                "dossier_autorisation" => "PC0990991000002",
                "terrain_adresse_voie_numero" => "",
                "terrain_adresse_lieu_dit" => "",
                "terrain_adresse_code_postal" => "",
                "terrain_adresse_cedex" => "",
                "terrain_adresse_voie" => "",
                "terrain_adresse_bp" => "",
                "terrain_adresse_localite" => "",
                "terrain_superficie" => "",
                "references_cadastrales" => array(),
                "dossier_autorisation_type" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "dossier_autorisation_type_detaille" => "Demande d'autorisation de construire, d'aménager ou de modifier un ERP",
                "collectivite" => "MARSEILLE",
                "instructeur" => "",
                "division" => "",
                "etat_dossier" => "dossier incomplet",
                "statut_dossier" => "encours",
                "date_depot_initial" => "2013-11-06",
                "date_limite_instruction" => "2014-01-06",
                "date_decision" => "",
                "enjeu_urbanisme" => "false",
                "enjeu_erp" => "false",
                "petitionnaire_principal" => array(
                    "demandeur" => "9",
                    "qualite" => "particulier",
                    "particulier_civilite" => "",
                    "particulier_nom" => "Pont",
                    "particulier_prenom" => "Maxime",
                    "particulier_date_naissance" => "",
                    "particulier_commune_naissance" => "",
                    "particulier_departement_naissance" => "",
                    "numero" => "",
                    "voie" => "",
                    "complement" => "",
                    "lieu_dit" => "",
                    "localite" => "",
                    "code_postal" => "",
                    "bp" => "",
                    "cedex" => "",
                    "pays" => "France",
                    "division_territoriale" => "",
                    "telephone_fixe" => "",
                    "telephone_mobile" => "",
                    "indicatif" => "",
                    "courriel" => "",
                    "fax" => "",
                ),
                "autres_petitionnaires" => array(
                    array(
                        "demandeur" => "12",
                        "qualite" => "personne_morale",
                        "personne_morale_civilite" => "Madame",
                        "personne_morale_denomination" => "DURANT",
                        "personne_morale_raison_sociale" => "",
                        "personne_morale_siret" => "",
                        "personne_morale_categorie_juridique" => "SARL",
                        "personne_morale_nom" => "DUPONT",
                        "personne_morale_prenom" => "Jeanne",
                        "numero" => "",
                        "voie" => "",
                        "complement" => "",
                        "lieu_dit" => "",
                        "localite" => "",
                        "code_postal" => "",
                        "bp" => "",
                        "cedex" => "",
                        "pays" => "France",
                        "division_territoriale" => "",
                        "telephone_fixe" => "",
                        "telephone_mobile" => "",
                        "indicatif" => "",
                        "courriel" => "",
                        "fax" => "",
                    ),
                ),
                "donnees_techniques" => array(
                    "su_tot_shon_tot" => "",
                    "su_avt_shon_tot" => "",
                ),
            ));
            return;
        }
        // Aucun de nos da de test
        header("HTTP/1.0 400 Bad Request");
        header('Content-Type: application/json');
        echo json_encode(array(
            "http_code" => 400,
            "http_code_message" => "400 Bad Request",
            "message" => "Le dossier d'instruction ".$di." n'etait pas trouve",
        ));
        return;
    }
}

    
?>
