<?php
/**
 * Ce fichier permet de déclarer la classe referentielpatrimoinetest, ressource 
 * exposée à travers l'interface REST qui hérite de la classe de base Services. 
 * Cette classe servira pour les tests afin de simuler l'échange avec le 
 * référentiel patrimoine
 *
 * @package openaria
 * @version SVN : $Id$
 */
 
//Inclusion de la classe de base Services
include_once('../services/REST/services.php');

/**
 * Cette classe définie la ressource 'referentielpatrimoinetest' qui permet de 
 * tester l'échange avec le référentiel patrimoine
 */
class referentielpatrimoinetest extends Services {
    
    /**
     * Cette méthode permet de définir le traitement du GET sur une requête
     * REST. Elle retourne des données.
     *
     * @param string $id L'identifiant de la ressource
     */
    public function get($parcelle) {
        // Envoi de l'en-tête HTTP
        header("HTTP/1.0 200 OK");
        header('Content-Type: application/json');
        
        //Si la parcelle avec des références patrimoines est fournie
        if ($parcelle == "203812 A0077"){
            echo json_encode(array(
                array("attributes"=>
                    array("value"=>"215904 M0030",
                    "nom"=>"parcelle",
                    "idEquipement"=>"91"
                    ),
                "nom"=>"Parking Public",
                "arrondissement"=>"13015",
                "nature"=>"Parking",
                "adresse"=>"0. Boulevard DU COMMANDANT ROBERT THOLLON",
                "affectataire"=>"50004     ",
                "idEquipement"=>"91",
                "gpsX"=>null,
                "gpsY"=>null
                ),
                array("attributes"=>
                    array("value"=>"215904 M0030",
                    "nom"=>"parcelle",
                    "idEquipement"=>"1016"
                    ),
                "nom"=>"Voirie Realisee",
                "arrondissement"=>"13015",
                "nature"=>"Voirie",
                "adresse"=>"0. Boulevard DU COMMANDANT ROBERT THOLLON",
                "affectataire"=>"50004     ",
                "idEquipement"=>"1016",
                "gpsX"=>null,
                "gpsY"=>null
               ),
               array("attributes"=>
                    array("value"=>"215904 M0085",
                    "nom"=>"parcelle",
                    "idEquipement"=>"3840"),
                "nom"=>"Aire Sportive",
                "arrondissement"=>"13015",
                "nature"=>"Aire Sportive",
                "adresse"=>"0. Boulevard DU COMMANDANT ROBERT THOLLON",
                "affectataire"=>"51504     ",
                "idEquipement"=>"3840",
                "gpsX"=>null,
                "gpsY"=>null),
                array("attributes"=>
                    array("value"=>"215904 M0028",
                    "nom"=>"parcelle",
                    "idEquipement"=>"1037"
                    ),
                "nom"=>"plan d\'Aou",
                "arrondissement"=>"13015",
                "nature"=>"Groupe Scolaire",
                "adresse"=>"0. Boulevard DU COMMANDANT ROBERT THOLLON",
                "affectataire"=>"20204     ",
                "idEquipement"=>"1037",
                "gpsX"=>null,
                "gpsY"=>null
                )
            ));
        }
        //Si aucune parcelle n'est fournie ou si la parcelle avec des références 
        //patrimoines est fournie
        else {
            echo json_encode(array());
        }
    }
}

    
?>