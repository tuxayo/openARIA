<?php
/**
 * Ce fichier permet de déclarer la classe referentieladsmessagestest, ressource 
 * exposée à travers l'interface REST qui hérite de la classe de base Services. 
 * Cette classe servira pour les tests afin de simuler l'échange avec le 
 * référentiel patrimoine
 *
 * @package openaria
 * @version SVN : $Id: referentieladsmessagestest.php 1526 2015-12-18 17:07:38Z fmichon $
 */
 
//Inclusion de la classe de base Services
include_once('../services/REST/services.php');

/**
 * Cette classe définie la ressource 'referentieladsmessagestest' qui permet de 
 * tester l'échange avec le référentiel patrimoine
 */
class referentieladsmessagestest extends Services {

    public function post($request_data) {
        header("HTTP/1.0 200 OK");
        header('Content-Type: application/json');
        echo json_encode(array());
    }
    
}

    
?>