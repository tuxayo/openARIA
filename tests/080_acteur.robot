*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les acteurs...


*** Test Cases ***
Saisie des congés d'un acteur
    #
    Depuis la page d'accueil    admin    admin
    Ajouter l'acteur    George MARTIN    technicien-si    Sécurité Incendie    technicien
    Depuis le contexte de l'acteur    George MARTIN

    # On clique sur l'onglet "Congés"
    On clique sur l'onglet    acteur_conge    Congés
    # On clique sur le bouton ajouter
    Click On Add Button JS
    # On récupère le jour en cours et l'année courante
    ${yyyy} =    Get Time    year
    ${dd} =    Get Time    day
    # On saisie la date de début
    Input Datepicker    date_debut    ${dd}/07/${yyyy}
    # On saisie l'heure de début
    Input Hour Minute    heure_debut    09    30
    # On saisie la date de fin
    Input Datepicker    date_debut    ${dd}/08/${yyyy}
    # On saisie l'heure de fin
    Input Hour Minute    heure_debut    18    30
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be In Subform    Vos modifications ont bien été enregistrées.


Saisie des plages privilégiées de visites d'un acteur
    #
    Depuis la page d'accueil    admin    admin
    Ajouter l'acteur    Claude BERTRAND    technicien-si    Sécurité Incendie    technicien
    Depuis le contexte de l'acteur    Claude BERTRAND

    # On clique sur l'onglet "Plages de visites"
    On clique sur l'onglet    acteur_plage_visite    Plages Privilégiées De Visites
    # On clique sur le bouton ajouter
    Click On Add Button JS
    # On coche les plages de disponibilité
    Select Checkbox    css=#mardi_matin
    Select Checkbox    css=#mercredi_matin
    Select Checkbox    css=#jeudi_matin
    Select Checkbox    css=#jeudi_apresmidi
    Select Checkbox    css=#vendredi_matin
    Select Checkbox    css=#vendredi_apresmidi
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Suppression d'un utilisateur référencé par un acteur

    [Documentation]    Lors de la suppression d'un utilisateur (enregistrement
    ...    de la table des utilisateurs qui sont autorisés à se connecter à
    ...    l'application), on souhaite que l'utilisateur soit supprimé sans
    ...    qu'une référence depuis un acteur vers cet utilisateur ne bloque la
    ...    suppression. En effet, le rôle de l'acteur est de perdurer pour
    ...    historique, même quand l'utilisateur est dépourvu de son
    ...    habilitation à utiliser openARIA.

    #
    Depuis la page d'accueil  admin  admin

    #
    Ajouter l'utilisateur depuis le menu  Sarah Walter  dui.lectus.rutrum@non.co.uk  swalter  LDXK9Lkqwk3Cc  TECHNICIEN SI

    #
    Ajouter l'acteur    Sarah Walter    Sarah Walter    Sécurité Incendie    technicien

    #
    Depuis le contexte de l'acteur    Sarah Walter
    Element Text Should Be    css=#om_utilisateur    Sarah Walter

    #
    Supprimer l'utilisateur    swalter
    Valid Message Should Be    La suppression a été correctement effectuée.

    #
    Depuis le contexte de l'acteur    Sarah Walter
    Element Text Should Be    css=#om_utilisateur    ${EMPTY}

