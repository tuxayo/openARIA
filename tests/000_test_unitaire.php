<?php
/**
 * Ce fichier regroupe les tests unitaire général à l'application openARIA
 *
 * @package openaria
 * @version SVN : $Id$
 */

/**
 * Cette classe permet de tester unitairement les fonctions de l'application.
 */
class General extends PHPUnit_Framework_TestCase {

    /**
     * Méthode lancée en début de traitement
     */
    public function setUp() {

        // Instancie la timezone
        date_default_timezone_set('Europe/Paris');
    }

    /**
     * Méthode lancée en fin de traitement
     */
    public function tearDown() {
        
        //
    }

    /**
     * Test la fonction define_next_week() de la classe Programmation.
     */
    public function test_next_week() {
        // Instance de la classe Utils
        require_once "../obj/utils.class.php";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();
        
        // Année en cours
        $year = date("Y");
        // Nombre de semaine de l'année en cours
        $weeks_per_year =  date("W", mktime(0,0,0,12,28,$year));
        
        // Initialisation des variables
        $i = 1;
        // Test sur toute les semaines de l'année en cours
        while ($i < $weeks_per_year){
            $date = $f->define_next_week($year, $i);
            $this->assertEquals($year, $date["annee"]);
            $this->assertEquals(str_pad(++$i, 2, "0",STR_PAD_LEFT), $date["semaine"]);
        }
        
        //Test sur la première semaine de l'année suivante
        $date = $f->define_next_week($year, $i);
        $this->assertEquals($year+1, $date["annee"]);
        $this->assertEquals("01", $date["semaine"]);
        
        // Destruction de la classe Utils
        $f->__destruct();
    }

}

?>