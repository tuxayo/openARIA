*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les établissements...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Création d'un établissement
    &{etab01} =  Create Dictionary
    ...  libelle=JEUX DE PLEIN AIR
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    # Création d'un établissement
    &{etab02} =  Create Dictionary
    ...  libelle=MAGASIN SPAR TEST010
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  adresse_numero=31
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=M.
    ...  exp_nom=Gaultier
    ...  exp_prenom=Jacques
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}

    # Création d'un établissement avec un exploitant
    &{etab03} =  Create Dictionary
    ...  libelle=KGB
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=3
    ...  exp_civilite=M.
    ...  exp_nom=Poutine
    ...  exp_prenom=Vladimir
    ...  exp_adresse_numero=1
    ...  exp_adresse_cp=101000
    ...  exp_adresse_ville=Moscou
    ...  exp_pays=Russie
    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}


Consulter la fiche de l'établissement

    [Documentation]    On vérifie que la fiche établissement contient les informations qu'elle doit contenir.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    # On vérifie que le libellé du champ dossier_coordination_periodique est correctement traduit
    Element Should Contain    css=#lib-dossier_coordination_periodique     dossier de coordination périodique


Recherche avancée d'un établissement
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous

    # Par ID
    Input Text    css=#etablissement    ${etab02_code}
    # Par libellé
    Input Text    css=#libelle    ${etab02.libelle}
    # Par numéro, voie et arrondissement
    Input Text    css=#adresse_numero    31
    Input Text    css=#adresse_voie    RUE DE ROME
    Select From List By Label    css=#adresse_arrondissement    6ème
    # Par type
    Select From List By Label    css=#etablissement_type    M
    # Par catégorie
    Select From List By Label    css=#etablissement_type    M
    # Par état
    Select From List By Label    css=#etablissement_type    M
    # Par nom de contact
    Input Text    css=#nom    Gaultier
    # Par numéro de SIRET
    Input Text    css=#siret    73282932000074

    #
    Click On Search Button
    Page Should Contain    ${etab02.libelle}


Modifier un établissement
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    #
    Element Should Contain    css=#adresse_ville    MARSEILLE
    #
    Page Should Contain    ${DATE_FORMAT_DD/MM/YYYY}
    #
    Click On Form Portlet Action    etablissement_tous    modifier
    #
    Input Text    css=#libelle    CHEVEUX AUX VENTS
    # On change la nature de l'établissement
    Select From List By Label    css=#etablissement_nature    ERP Référentiel
    # On change le type de l'établissement
    Select From List By Label    css=#etablissement_type    Choisir type
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Error Message Should Contain    Le type et la catégorie de l'établissement doivent être saisis si l'établissement est un ERP

    # On change le type de l'établissement
    Select From List By Label    css=#etablissement_type    PA
    # On valide le formulaire
    Click On Submit Button
    Click On Back Button
    Element Should Contain    css=#libelle    CHEVEUX AUX VENTS
    #
    Click On Form Portlet Action    etablissement_tous    modifier
    #
    Input Text    css=#libelle    ${etab01.libelle}
    # On valide le formulaire
    Click On Submit Button


Supprimer établissement
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Supprimer l'établissement    ${etab01.libelle}


Numéro de SIRET invalide
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    Input Text    css=#libelle    FAKE
    Select From List By Label    css=#etablissement_nature    ERP Référentiel
    Select From List By Label    css=#etablissement_type    PA
    Input Text    css=#siret    73482742011233

    Click On Submit Button
    Error Message Should Contain    Le numéro de SIRET est invalide.


Références cadastrales
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Saisie
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Input Text    css=input.refquart    77
    Input Text    css=input.refsect    G
    Input Text    css=input.refparc    77
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # Vérification de la présence des références cadastrales saisies en mode 'CONSULTER'
    Element Should Contain    css=div.field-type-referencescadastralesstatic    077G0077
    # Suppression des informations de références cadastrales
    Click On Form Portlet Action    etablissement_tous    modifier
    Input Text    css=input.refquart    ${empty}
    Input Text    css=input.refsect    ${empty}
    Input Text    css=input.refparc    ${empty}
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}
    # Vérification de la non présence des références cadastrales saisies en mode 'CONSULTER'
    Page Should Not Contain    077G0077


Ajout d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Element Should Contain    css=#exp_nom    Poutine
    On clique sur l'onglet    contact    Contacts
    Click On Link    M. Poutine Vladimir
    Sleep    1
    Page Should Contain    Russie


Modification d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Modification de l'exploitant d'un établissement
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Select From List By Label    css=#exp_civilite    Mme
    Input Text    css=#exp_nom    Merkel
    Input Text    css=#exp_prenom    Angela
    Input Text    css=#exp_adresse_numero    1
    Input Text    css=#exp_adresse_cp    ${empty}
    Input Text    css=#exp_adresse_ville    Berlin
    Input Text    css=#exp_pays    Allemagne
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Element Should Contain    css=#exp_nom    Merkel
    On clique sur l'onglet    contact    Contacts
    Click On Link    Mme Merkel Angela
    Sleep    1
    Page Should Contain    Allemagne


Suppression d'un exploitant
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Suppression de l'exploitant d'un établissement
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Click On Form Portlet Action    etablissement_tous    modifier
    Select From List By Index    css=#exp_civilite    0
    Input Text    css=#exp_nom    ${empty}
    Input Text    css=#exp_prenom    ${empty}
    Input Text    css=#exp_adresse_numero    ${empty}
    Input Text    css=#exp_adresse_numero2    ${empty}
    Input Text    css=#exp_adresse_cp    ${empty}
    Input Text    css=#exp_adresse_ville    ${empty}
    Input Text    css=#exp_pays    ${empty}
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

    # Vérification
    Depuis le contexte de l'établissement    ${etab03_code}    ${etab03.libelle}
    Page Should Not Contain    Merkel
    Element Should Contain    css=#exp    Aucun
    On clique sur l'onglet    contact    Contacts
    Page Should Not Contain    Merkel


Copie de l'adresse
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    # Saisie informations établissement
    Input Text    css=#libelle    Open Inc

    # Saisie adresse établissement
    Input Text    css=#adresse_numero    1
    Input Text    css=#adresse_numero2    bis
    Input Text    css=#autocomplete-voie-search    RUE PAPERE
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Link    RUE PAPERE
    Select From List By Label    css=#adresse_arrondissement    1er
    Select From List By Label    css=#etablissement_nature    ERP Référentiel
    Sleep    1

    # Copie l'adresse
    Select Checkbox    css=#meme_adresse

    # Vérification
    Textfield Value Should Be    css=#exp_adresse_ville   MARSEILLE
    Textfield Value Should Be    css=#exp_adresse_numero   1
    Textfield Value Should Be    css=#exp_adresse_numero2   bis
    Textfield Value Should Be    css=#exp_adresse_voie   RUE PAPERE
    Element Should Be Disabled    css=#exp_adresse_numero
    Element Should Be Disabled    css=#exp_adresse_numero2
    Element Should Be Disabled    css=#exp_adresse_voie

    # Efface l'adresse
    Unselect Checkbox   css=#meme_adresse

    # Nouvelle vérification
    Textfield Value Should Be    css=#exp_adresse_ville   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_numero   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_numero2   ${EMPTY}
    Textfield Value Should Be    css=#exp_adresse_voie   ${EMPTY}
    Element Should Be Enabled    css=#exp_adresse_numero
    Element Should Be Enabled    css=#exp_adresse_numero2
    Element Should Be Enabled    css=#exp_adresse_voie

    # A FAIRE : tester le submit du form
    # Actuellement fail modal dialog present



Champs périodicité
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    # Vérification que par défaut les champs soient masqués
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_prochaine_visite_periodique_date_previsionnelle
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_derniere_visite_periodique_date

    # Changement de la nature pour afficher
    Select From List By Label    css=#etablissement_nature    ERP Référentiel
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#si_prochaine_visite_periodique_date_previsionnelle
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#si_derniere_visite_periodique_date

    # Vérification de l'exception avec le type plein air
    Select From List By Label    css=#etablissement_type    PA
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_prochaine_visite_periodique_date_previsionnelle
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_derniere_visite_periodique_date

    # Réaffichage en rechangeant le type
    Select From List By Label    css=#etablissement_type    L
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#si_prochaine_visite_periodique_date_previsionnelle
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#si_derniere_visite_periodique_date

    # Remasquage en rechangeant la nature
    Select From List By Label    css=#etablissement_nature    ERP potentiel
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_prochaine_visite_periodique_date_previsionnelle
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Not Be Visible    css=#si_derniere_visite_periodique_date


Archivage
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    Depuis le contexte de l'établissement    ${etab02_code}    ${etab02.libelle}

    # L'action 'DESARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    desarchiver
    # On clique sur l'action directe 'ARCHIVER'
    Click On Form Portlet Action    etablissement_tous    archiver
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Archivage correctement effectué.
    # L'action 'ARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    archiver
    # L'action 'DESARCHIVER' doit être disponible
    Portlet Action Should Be In Form    etablissement_tous    desarchiver

    # Vérification
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab02.libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=table.tab-tab  ${etab02.libelle}
    Click On Link    Afficher les éléments archivés

    # Désarchivage
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=table.tab-tab  ${etab02.libelle}
    Click On Link    ${etab02.libelle}
    # On clique sur l'action directe 'DESARCHIVER'
    Click On Form Portlet Action    etablissement_tous    desarchiver
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Désarchivage correctement effectué.
    # L'action 'DESARCHIVER' ne doit pas être disponible
    Portlet Action Should Not Be In Form    etablissement_tous    desarchiver
    # L'action 'ARCHIVER' doit être disponible
    Portlet Action Should Be In Form    etablissement_tous    archiver

    # Nouvelle vérification
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab02.libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=table.tab-tab  ${etab02.libelle}


Gestion des références patrimoine
    #
    Depuis la page d'accueil    admin    admin
    #Activation du paramètre option_referentiel_patrimoine
    Modifier le paramètre  option_referentiel_patrimoine  true

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #Ajout d'un nouvel établissement
    Go To Submenu In Menu    etablissements    etablissement_tous
    Click On Add Button

    #On vérifie que le champ "ref_patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#lien_referentiel_patrimoine

    Input Text    css=#libelle    test_etablissement_patrimoine
    Select From List By Label    css=#etablissement_nature    ERP potentiel
    Select From List By Label    css=#etablissement_statut_juridique    Ville
    Sleep    1

    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Click Button    css=#lien_referentiel_patrimoine
    Sleep    1
    Confirm Action

    Input Text    css=input.refquart    801
    Input Text    css=input.refsect    G
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    Page Should Contain    Aucune référence patrimoine liée.
    Click Element    css=.ui-dialog-titlebar-close

    Input Text    css=input.refquart    812
    Input Text    css=input.refsect    A
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page Should Contain    Liste des références patrimoine trouvées :
    Click Button    css=input[value='Valider']
    Element Text Should Be    css=#ref_patrimoine    ${EMPTY}

    Input Text    css=input.refquart    812
    Input Text    css=input.refsect    A
    Input Text    css=input.refparc    0077
    Click Button    css=#lien_referentiel_patrimoine
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page Should Contain    Liste des références patrimoine trouvées :
    Select Checkbox    css=input[value='91']
    Click Button    css=input[value='Valider']
    Sleep    2
    Element Text Should Be    css=#ref_patrimoine    91

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors

    #Test en modification
    Click On Back Button
    Page Should Not Contain Errors
    Input Text  css=div#adv-search-adv-fields input#libelle  test_etablissement_patrimoine
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=table.tab-tab  test_etablissement_patrimoine
    Click On Link    test_etablissement_patrimoine
    Page Should Not Contain Errors
    Element Text Should Be    css=#ref_patrimoine    91
    Click On Form Portlet Action    etablissement_tous    modifier
    Page Should Not Contain Errors

    Element Text Should Be    css=#ref_patrimoine    91

    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Select From List By Label    css=#etablissement_statut_juridique    Public
    Sleep    1
    #On vérifie que le champ "ref_patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" n'est pas affiché
    Element Should Not Be Visible    css=#lien_referentiel_patrimoine

    Select From List By Label    css=#etablissement_statut_juridique    Ville
    #On vérifie que le champ "ref_patrimoine" est affiché
    Element Should Be Visible    css=#ref_patrimoine
    #On vérifie que le bouton "Récupérer le(s) référence(s) patrimoine" est affiché
    Element Should Be Visible    css=#lien_referentiel_patrimoine

    Click Button    css=#lien_referentiel_patrimoine
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page Should Contain    Liste des références patrimoine trouvées :
    Select Checkbox    css=input[value='91']
    Click Button    css=input[value='Valider']
    ${ref_result}=  catenate  SEPARATOR=\n
    ...  91
    ...  91
    Element Text Should Be  css=#ref_patrimoine  ${ref_result}
    Input Text    css=#ref_patrimoine    1284

    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Page Should Not Contain Errors

    Element Text Should Be    css=#ref_patrimoine    1284

    #Désactivation du paramètre option_referentiel_patrimoine
    Depuis la page d'accueil    admin    admin
    Modifier le paramètre  option_referentiel_patrimoine  false


Importer un établissement depuis un fichier CSV
    #
    Depuis la page d'accueil    admin    admin
    # On importe l'établissement
    Depuis l'import    import_etablissement
    Add File    fic1    import_etablissement.csv
    Click On Submit Button In Import CSV
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    1 ligne(s) importée(s)
    # On vérifie les valeurs importées
    Depuis le contexte de l'établissement    T777
    Element Text Should Be    css=#libelle    Musée de l'Import-Export
    Element Text Should Be    css=#annee_de_construction    2015
    Element Text Should Be    css=#adresse_numero    91
    Element Text Should Be    css=#adresse_numero2    bis
    Element Text Should Be    css=#adresse_voie    BD DALLEST
    Element Text Should Be    css=#adresse_cp    13009
    Element Text Should Be    css=#adresse_ville    MARSEILLE
    Element Text Should Be    css=#adresse_arrondissement    9ème
    Element Text Should Be    css=#etablissement_nature    ERP potentiel
    Open Fieldset    etablissement_tous    details
    Element Text Should Be    css=#si_locaux_sommeil    Non

Gestion des contraintes appliquées aux établissements

    [Documentation]    Test des vues et actions contraintes appliquées :
    ...  - tableau
    ...  - ajouter
    ...  - modifier
    ...  - supprimer
    ...  - récupérer
    ...  - démarquer comme récupérée

    @{ref_cad} =  Create List  800  A  1
    &{etab} =  Create Dictionary
    ...  libelle=FIX N MIX
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=28
    ...  adresse_voie=BD DE LA BLANCARDE
    ...  adresse_cp=13004
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=4ème
    ...  siret=73282932000074
    ...  exp_nom=Ribérou
    ...  exp_prenom=Laurent
    Depuis la page d'accueil  cadre-si  cadre-si
    ${etab_code} =  Ajouter l'établissement  ${etab}
    # Cas 1 : pas d'option SIG
    Depuis le contexte de l'établissement  ${etab_code}  FIX N MIX
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Page Should Not Contain Link  recuperer_contraintes
    Page Should Contain  Aucune contrainte appliquée à l'établissement.
    # Cas 2 : pas de référence cadastrale
    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    &{contrainte} =  Create Dictionary
    ...  groupe=ZONE DU POS
    ...  sousgroupe=M5
    ...  nature=POS
    ...  libelle=Contrainte 13055.M50
    ...  texte=Texte
    ...  texte_surcharge=Surcharge
    Ajouter la contrainte  ${contrainte}
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click Element  recuperer_contraintes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  L'établissement n'a aucune référence cadastrale renseignée.
    # Cas 3 : Besoin de synchronisation
    Depuis le formulaire de modification de l'établissement  ${etab_code}
    Saisir les références cadastrales  ${ref_cad}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Page Should Contain  Aucune contrainte appliquée à l'établissement.
    Click Element  recuperer_contraintes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[name="btn_recuperer"]
    Error Message Should Be In Subform  Les contraintes doivent être synchronisées.
    Click On Back Button In Subform
    # Cas 4 :Ajout
    Click Element  ajouter_contrainte
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-zone-du-pos legend
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#fieldset-sousform-lien_contrainte_etablissement-m5 legend
    Sleep  1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Checkbox  css=input[id^="contrainte_"]
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element  css=#sformulaire div.formControls input[type="submit"]
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible  css=#sousform-lien_contrainte_etablissement div.message
    Page Should Not Contain Errors
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Text Should Be  css=#sousform-lien_contrainte_etablissement div.message.ui-state-valid p span.text  La contrainte Contrainte 13055.M50 a été appliquée à l'établissement.
    Click On Back Button In Subform
    Page Should Contain  Surcharge
    # Cas 5 : Récupération
    Depuis la page d'accueil  admin  admin
    Synchroniser les contraintes
    Valid Message Should Contain  4 ajoutées
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click Element  recuperer_contraintes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  2 ajoutées
    Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  0 ajoutée
    Valid Message Should Contain In Subform  2 mises à jour
    Click On Back Button In Subform
    # Cas 6 : Démarquage
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  demarquer_recuperee
    Valid Message Should Be In Subform  La contrainte n'est plus marquée comme récupérée.
    Click On Back Button In Subform
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  modifier
    Input Text  texte_complete  Une première contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 7 : Modification
    Click On Link  Une seconde contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  modifier
    Input Text  texte_complete  Une seconde contrainte du PLU modifiée
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    # Cas 8 : Nouvelle récupération
    # → la contrainte modifiée doit être écrasée
    # → une nouvelle contrainte doit apparaître, une ayant été démarquée
    Click Element  recuperer_contraintes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=input[name="btn_recuperer"]
    Valid Message Should Contain In Subform  1 ajoutée
    Valid Message Should Contain In Subform  1 mise à jour
    Click On Back Button In Subform
    Page Should Contain  Une première contrainte du PLU modifiée
    Page Should Not Contain  Une seconde contrainte du PLU modifiée
    Page Should Contain  Une seconde contrainte du PLU
    # Désactivation du SIG et des contraintes afin de revenir à l'état initial
    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig
    Depuis le contexte de l'établissement  ${etab_code}
    On clique sur l'onglet  lien_contrainte_etablissement  Contraintes
    Click On Link  Une première contrainte du PLU modifiée
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Click On Back Button In Subform
    Click On Link  Une première contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Click On Back Button In Subform
    Click On Link  Une seconde contrainte du PLU
    Click On SubForm Portlet Action  lien_contrainte_etablissement  supprimer
    Click On Submit Button In Subform
    Supprimer la contrainte  Contrainte 13055.M70
    Supprimer la contrainte  Contrainte 13055.M80
    Supprimer la contrainte  Contrainte 13055.M81
    Supprimer la contrainte  Contrainte 13055.M90
