*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestCase services REST


*** Variables ***
${json_data_voies_1}    {"module":"voies","data":{"file_name":"../tests/binary_files/ref_voies_test_1.csv"}}
${json_data_voies_2}    {"module":"voies","data":{"file_name":"../tests/binary_files/ref_voies_test_2.csv"}}
#Numérisation des documents ACC
${numerisation_acc}    {"module": "import","data": {"service": "ACC"}}
#Numérisation des documents SI
${numerisation_si}    {"module": "import","data": {"service": "SI"}}
#Purge des documents numérisés et importés ACC
${purge_acc}    {"module":"purge", "data":{"service":"ACC"}}
#Purge des documents numérisés et importés SI
${purge_si}    {"module":"purge", "data":{"service":"SI"}}
#Envoie des accusés de réception des consultations officielles
${consultations}    {"module":"ar_consultation"}


*** Test Cases ***
Referentiel voies
    [Tags]    post
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${json_data_voies_1}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Synchronisation terminée : 3 ajout(s) - 0 mise(s) à jour - 0 fusionnée(s) - 1 archivée(s)

    #Vérification de l'ajout des voies
    Depuis la page d'accueil  admin  admin
    Go To Tab    voie
    Input Text    name=recherche    XXX
    Select From List By Label    name=selectioncol    RIVOLI
    Click Element  css=button[name="s1"]
    Page Should Not Contain Errors
    Page Should Contain    Voie1
    Page Should Contain    Voie2
    Page Should Contain    Voie3
    Click On Link    Voie3
    Page Should Not Contain Errors
    Element Should Contain    css=#libelle    Voie3
    Element Should Contain    css=#rivoli    XXX3
    Click Link    css=#voie_arrondissement
    Sleep    1
    Page Should Contain    6ème
    Page Should Contain    7ème

    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${json_data_voies_2}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Synchronisation terminée : 0 ajout(s) - 2 mise(s) à jour - 0 fusionnée(s) - 2 archivée(s)

    #Vérification de la mise à jour des voies
    Depuis la page d'accueil  admin  admin
    Go To Tab    voie
    Input Text    name=recherche    XXX
    Select From List By Label    name=selectioncol    RIVOLI
    Click Element  css=button[name="s1"]
    Sleep  1
    Page Should Not Contain Errors
    Page Should Contain    Voie2ModifArr
    Page Should Contain    Voie3
    Click On Link    Voie3
    Page Should Not Contain Errors
    Element Should Contain    css=#libelle    Voie3
    Element Should Contain    css=#rivoli    XXX3
    Click Link    css=#voie_arrondissement
    Sleep    1
    Page Should Contain    6ème
    Page Should Contain    8ème


Numérisation des documents
    [Tags]    post
    [Documentation]    Import des documents numérisés

    # Création d'un établissement
    &{etab01} =  Create Dictionary
    ...  libelle=JEUX DE PLEIN AIR
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  adresse_numero=12
    ...  adresse_numero2=bis
    ...  adresse_voie=RUE DE LA REPUBLIQUE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73482742011234

    # Formatage sur 5 chiffres du code établissement
    # Ex. : T1 devient T00001
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_code} =  STR_REPLACE  T  ${EMPTY}  ${etab01_code}
    ${etab01_code} =  STR_PAD_LEFT  ${etab01_code}  5  0
    ${etab01_code} =  Catenate  SEPARATOR=  T  ${etab01_code}

    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}numerisation1.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}test.jpg  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}test.jpg
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}SI${/}Todo${/}${etab01_code}DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}T99999DVISITE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}PLOP23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}DVISITE23-13-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}ARRETE23-10-2016.pdf
    Copy File  ${EXECDIR}${/}binary_files${/}numerisation1.pdf  ${EXECDIR}${/}..${/}var${/}digitalization${/}SI${/}Todo${/}${etab01_code}ARRETE23-10-2016.pdf

    # Accessibilité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should Contain    ${jsondata['message']}    Tous les documents ont été traités
    Should Contain    ${jsondata['message']}    Liste des fichiers en erreur : ${etab01_code}DVISITE23-13-2016.pdf,${etab01_code}PLOP23-10-2016.pdf,T99999DVISITE23-10-2016.pdf,test.jpg

    # Sécurité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${numerisation_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter

    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}T99999DVISITE23-10-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}PLOP23-10-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}${etab01_code}DVISITE23-13-2016.pdf
    Remove File    ${EXECDIR}${/}..${/}var${/}digitalization${/}ACC${/}Todo${/}test.jpg

Purge des documents numérisés et importés
    [Tags]    post
    [Documentation]    Purge des documents numérisés et importés

    # Accessibilité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_acc}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter

    # Sécurité
    # Documents dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Tous les documents ont été traités

    # Aucun document dans le dossier
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${purge_si}    headers=${headers}
    ${jsondata}=    To Json    ${resp.content}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should be Equal    ${jsondata['message']}    Aucun document à traiter


Envoi des accusés réception des consultations
    [Tags]    post
    [Documentation]    Envoi des accusés réception des consultations


    # Création du dossier connecté
    # [102]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_COMPLETUDE_ERP
    ${emetteur} =  Set Variable  102_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [103]
    ${type} =  Set Variable  ADS_ERP__PC__PRE_DEMANDE_DE_QUALIFICATION_ERP
    ${emetteur} =  Set Variable  103_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0" }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # Ajout des consultations
    # [106]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_CONFORMITE
    ${emetteur} =  Set Variable  106_instr_daact_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0", "contenu" : { "consultation" : 2, "date_envoi" : "31/12/2015", "service_abrege" : "ACC", "service_libelle" : "Service Accessibilité", "date_limite" : "31/01/2016" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # [104]
    ${type} =  Set Variable  ADS_ERP__PC__CONSULTATION_OFFICIELLE_POUR_AVIS
    ${emetteur} =  Set Variable  104_instr_pc_test_040
    ${json} =  Set Variable  { "type" : "${type}", "date" : "31/12/2015 14:42", "emetteur" : "${emetteur}", "dossier_instruction" : "PC0990991000002P0", "contenu" : { "consultation" : 2, "date_envoi" : "01/03/2013", "service_abrege" : "SI", "service_libelle" : "Service Sécurité", "date_limite" : "01/04/2013" } }
    Vérifier le code retour du web service et vérifier que son message est  Post  messages  ${json}  200  Insertion du message '${type}' OK.

    # Test du service
    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${consultations}    headers=${headers}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should Contain    ${resp.content}  2 message(s)

    Create Session    httpopenaria    http://localhost/openaria/services/rest_entry.php
    ${headers}=    Create Dictionary    'Content-Type=application/json'
    ${resp}=    Post Request    httpopenaria    /maintenance    data=${consultations}    headers=${headers}
    Should Be Equal As Strings    ${resp.status_code}    200
    Should Contain    ${resp.content}  0 message(s)