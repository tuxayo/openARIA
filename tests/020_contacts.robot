*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  TestCase Contacts...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Création d'un établissement
    &{etab01} =  Create Dictionary
    ...  libelle=Parc de la maison blanche
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  adresse_numero=150
    ...  adresse_voie=BD PAUL CLAUDEL
    ...  adresse_cp=13010
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=10ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martin
    ...  exp_prenom=Josiane
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}


Création du contact de l'établissement

    [Documentation]    Création du contact sur un établissement.

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    #
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    #
    On clique sur l'onglet    contact    Contacts
    Click On Add Button JS

    # Par défaut la qualité est 'particulier' et les champs concernant une
    # personne morale sont cachés
    Selected List Label Should Be    css=#sformulaire #qualite    particulier
    Element Should Not Be Visible  css=#sformulaire #denomination
    Element Should Not Be Visible  css=#sformulaire #raison_sociale
    Element Should Not Be Visible  css=#sformulaire #siret
    Element Should Not Be Visible  css=#sformulaire #categorie_juridique
    Element Should Be Visible  css=#sformulaire #civilite
    Element Should Be Visible  css=#sformulaire #nom
    Element Should Be Visible  css=#sformulaire #lib-nom span.not-null-tag
    Element Should Be Visible  css=#sformulaire #prenom
    Element Should Be Visible  css=#sformulaire #titre

    # On valide le formulaire sans aucune valeur pour vérifier le caractère
    # obligatoire des champs
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Error Message Should Contain In Subform    Le champ nom est obligatoire.
    Error Message Should Contain In Subform    SAISIE NON ENREGISTRÉE

    #
    Select From List By Label    css=#sformulaire #qualite    personne morale
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sformulaire #denomination
    Element Should Be Visible  css=#sformulaire #lib-denomination span.not-null-tag
    Element Should Be Visible  css=#sformulaire #raison_sociale
    Element Should Be Visible  css=#sformulaire #lib-raison_sociale span.not-null-tag
    Element Should Be Visible  css=#sformulaire #siret
    Element Should Be Visible  css=#sformulaire #categorie_juridique
    Element Should Be Visible  css=#sformulaire #civilite
    Element Should Be Visible  css=#sformulaire #nom
    Element Should Not Be Visible  css=#sformulaire #lib-nom span.not-null-tag
    Element Should Be Visible  css=#sformulaire #prenom
    Element Should Be Visible  css=#sformulaire #titre

    # On valide le formulaire sans aucune valeur pour vérifier le caractère
    # obligatoire des champs
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Error Message Should Contain In Subform    Un des champs dénomination ou raison sociale doit être rempli.
    Error Message Should Contain In Subform    SAISIE NON ENREGISTRÉE

    # Cas du particulier
    Select From List By Label    css=#qualite    particulier
    Select From List By Label    css=#contact_type    Mandataire
    Select From List By Label    css=#civilite    M.
    Input Text    css=#nom    Oiseau
    Input Text    css=#prenom    Jacques
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link    M. Oiseau Jacques
    Page Should Contain    Parc de la maison blanche
    Page Should Contain    Mandataire
    Click On Back Button In Subform

    # Cas de la personne morale avec représentant
    Click On Add Button JS
    Select From List By Label    css=#qualite    personne morale
    Select From List By Label    css=#civilite    M.
    Input Text    css=#nom    Mailhot
    Input Text    css=#prenom    Jacques
    Input Text    css=#denomination    LEROY SAS
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link    LEROY SAS représenté(e) par M. Mailhot Jacques
    Page Should Contain    Parc de la maison blanche
    Page Should Contain    personne morale
    Click On Back Button In Subform

    # Cas de la personne morale sans représentant
    Click On Add Button JS
    Select From List By Label    css=#qualite    personne morale
    Input Text    css=#denomination    GRADASSO SAS
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.
    Click On Back Button In Subform
    Click On Link    GRADASSO SAS
    Page Should Contain    Parc de la maison blanche
    Page Should Contain    personne morale
    Click On Back Button In Subform


Impossible de modifer le type exploitant

    [Documentation]    Vérifie l'impossibilité de supprimer un exploitant sur
    ...    l'établissement

    #
    Depuis la page d'accueil    technicien-si    technicien-si
    #
    Depuis l'onglet contact de l'établissement    ${etab01_code}    ${etab01.libelle}
    #
    Click On Link    Mme Martin Josiane
    Click On SubForm Portlet Action    contact    modifier
    Page Should Contain Element    css=div.field-type-selecthiddenstatic #contact_type


Création du contact institutionnel

    [Documentation]    Création d'un contact institutionnel. C'est un contact
    ...    qui n'est rattaché à aucun établissement ou autre.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre le menu
    Go To Submenu In Menu    suivi    contact
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Générés > Contact Institutionnel
    # On clique sur le bouton ajouter
    Click On Add Button

    # On vérifie que le type du contact n'apparaît pas
    Form Field Attribute Should Be    contact_type    type    hidden
    # On vérifie que l'établissement n'apparait pas
    Form Field Attribute Should Be    etablissement    type    hidden

    # Par défaut la qualité est 'particulier' et les champs concernant une
    # personne morale sont cachés
    Selected List Label Should Be    css=#qualite    particulier
    Element Should Not Be Visible  css=#denomination
    Element Should Not Be Visible  css=#raison_sociale
    Element Should Not Be Visible  css=#siret
    Element Should Not Be Visible  css=#categorie_juridique
    Element Should Be Visible  css=#civilite
    Element Should Be Visible  css=#nom
    Element Should Be Visible  css=#lib-nom span.not-null-tag
    Element Should Be Visible  css=#prenom
    Element Should Be Visible  css=#titre

    # On valide le formulaire sans aucune valeur pour vérifier le caractère
    # obligatoire des champs
    Click On Submit Button
    # On vérifie le message de validation
    Error Message Should Contain    Le champ nom est obligatoire.
    Error Message Should Contain    SAISIE NON ENREGISTRÉE

    #
    Select From List By Label    css=#qualite    personne morale
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#denomination
    Element Should Be Visible  css=#lib-denomination span.not-null-tag
    Element Should Be Visible  css=#raison_sociale
    Element Should Be Visible  css=#lib-raison_sociale span.not-null-tag
    Element Should Be Visible  css=#siret
    Element Should Be Visible  css=#categorie_juridique
    Element Should Be Visible  css=#civilite
    Element Should Be Visible  css=#nom
    Element Should Not Be Visible  css=#lib-nom span.not-null-tag
    Element Should Be Visible  css=#prenom
    Element Should Be Visible  css=#titre

    # On valide le formulaire sans aucune valeur pour vérifier le caractère
    # obligatoire des champs
    Click On Submit Button
    # On vérifie le message de validation
    Error Message Should Contain    Un des champs dénomination ou raison sociale doit être rempli.
    Error Message Should Contain    SAISIE NON ENREGISTRÉE

    # Cas du particulier
    Select From List By Label    css=#qualite    particulier
    # On saisit la civilité
    ${civilite} =    Set Variable    M.
    Set Suite Variable    ${civilite}
    Select From List By Label    css=#civilite    M.
    # On saisit le nom et le prénom
    ${nom} =    Set Variable    Durand
    Set Suite Variable    ${nom}
    ${prenom} =    Set Variable    Michel
    Set Suite Variable    ${prenom}
    Input Text    css=#nom    ${nom}
    Input Text    css=#prenom    ${prenom}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button
    Click On Link    M. Durand Michel
    Element Should Not Be Visible  css=#etablissement
    Page Should Contain    particulier
    Click On Back Button

    # Cas de la personne morale avec représentant
    Click On Add Button
    Select From List By Label    css=#qualite    personne morale
    Select From List By Label    css=#civilite    M.
    Input Text    css=#nom    Neufville
    Input Text    css=#prenom    Bruno
    Input Text    css=#denomination  SARL CONSTANCE
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button
    Click On Link    SARL CONSTANCE représenté(e) par M. Neufville Bruno
    Element Should Not Be Visible  css=#etablissement
    Page Should Contain    personne morale
    Click On Back Button

    # Cas de la personne morale avec représentant
    Click On Add Button
    Select From List By Label    css=#qualite    personne morale
    Input Text    css=#denomination  SARL SABOURIN
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button
    Click On Link    SARL SABOURIN
    Element Should Not Be Visible  css=#etablissement
    Page Should Contain    personne morale
    Click On Back Button


Utilisation du contact institutionnel

    [Documentation]    Un contact institutionnel est sélectionnable en tant
    ...    que destinataire lors de la qualification d'un document généré.

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    # On ajoute un document généré avec comme destinataire le contact
    # institutionnel
    @{contacts_lies}    Create List
    ...  (Institutionnel) ${civilite} ${nom} ${prenom}
    ...  (Institutionnel) SARL CONSTANCE représenté(e) par M. Neufville Bruno
    ...  (Institutionnel) SARL SABOURIN
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}    ${params}    Courrier simple    Courrier de courtoisie

