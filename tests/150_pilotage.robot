*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Ce test suite permet de falider le bon fonctionnement des fonctionnalités
...  des menus Statistiques, Édition et Requêtes mémorisées


*** Test Cases ***
# Constitution d'un jeu de données

#     [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
#     ...  données cohérent pour les scénarios fonctionnels qui suivent.

#     Comment    @todo Écrire le Test Case'


# Naviguer dans les statistiques accessibilité

#     [Documentation]   Vérification des valeurs des statistiques accessibillité
#     Comment    @todo Écrire le Test Case'

# Naviguer dans les statistiques sécurité incendie

#     [Documentation]   Vérification des valeurs des statistiques sécurité incendie
#     Comment    @todo Écrire le Test Case'

# Générer les éditions pdf SI de pilotage
#     [Documentation]   Vérification du bon fonctionnement des éditions pdf

#     #
#     Depuis la page d'accueil    cadre-si    cadre-si
#     # On ouvre la vue statistiques
#     Go To Submenu In Menu    suivi    statistiques
#     # On génère l'édition
#     Click Element    css=input[name=editions-stats-submit]
#     # On ouvre le PDF
#     Open PDF    pilotage
#     # On vérifie le contenu
#     Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page Should Contain    Visites réalisées
#     PDF Pages Number Should Be    4
#     # On revient à la fenêtre principale
#     Close PDF


# Générer les éditions pdf ACC de pilotage
#     [Documentation]   Vérification du bon fonctionnement des éditions pdf
#     Comment    @todo Ajouter les permissions au cadre-acc

#     #
#     Depuis la page d'accueil    cadre-acc    cadre-acc
#     # On ouvre la vue statistiques
#     Go To Submenu In Menu    suivi    statistiques
#     # On génère l'édition
#     Click Element    css=input[name=editions-stats-submit]
#     # On ouvre le PDF
#     Open PDF    pilotage
#     # On vérifie le contenu
#     Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page Should Contain    Par date et avis de réunion
#     PDF Pages Number Should Be    1
#     # On revient à la fenêtre principale
#     Close PDF


Générer les exports csv de pilotage

    [Documentation]   Vérification du bon fonctionnement de la génération des csv


    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # csv etablissement
    Go To Submenu In Menu    suivi    reqmo
    # On accède à la configuration de l'export
    Click On Link    établissement
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # Retour
    Click On Back Button
    # Vérification du csv
    # On sélectionne la sortie csv
    Select From List By Value    css=#sortie    csv
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # On click sur le lien du csv
    Click On Link    css=#reqmo-out-link

    # csv liste_modeles
    Go To Submenu In Menu    suivi    reqmo
    # On accède à la configuration de l'export
    Click On Link    modèle d'édition
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # Retour
    Click On Back Button
    # Vérification du csv
    # On sélectionne la sortie csv
    Select From List By Value    css=#sortie    csv
    # On valide le formulaire (sortie tableau)
    Click On Submit Button In Reqmo
    # On click sur le lien du csv
    Click On Link    css=#reqmo-out-link


