<?php
/**
 * Ce fichier regroupe les tests unitaire sur la classe DigitalizedDocument.
 *
 * @package openaria
 * @version SVN : $Id$
 */

require_once('../obj/digitalizedDocument.class.php');
require_once "../obj/utils.class.php";


/**
 * Cette classe permet de faire des tests unitaires sur la classe
 * DigitalizedDocument.
 *
 * La constante DEBUG doit être définis sur DEGUG_MODE ou PRODUCTION_MODE
 *
 * Lors de l'envoi de la commande ajouter --bootstrap bootstrap.php
 */
class DigitalizedDocumentTest extends PHPUnit_Framework_TestCase {

    /**
     * Instance de la classe digitalizedDocument.
     *
     * @var object
     */
    var $digitalizedDocument = NULL;

    /**
     * Fonction lancée en début de classe
     */
    public static function setUpBeforeClass() {
        // Disposition des fichiers de tests
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/numerisation1.pdf'
        );
        copy(
            'binary_files/numerisation2.pdf',
            '../var/digitalization/SI/Todo/numerisation2.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/T1DVISITE23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/T00004DVISITE23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/T00001DVISITE23-13-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/T00001PLOP23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/ACC/Todo/T1ARRETE23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/SI/Todo/T1DVISITE23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/SI/Todo/T00004DVISITE23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/SI/Todo/T00001DVISITE23-13-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/SI/Todo/T00001PLOP23-10-2016.pdf'
        );
        copy(
            'binary_files/numerisation1.pdf',
            '../var/digitalization/SI/Todo/T1ARRETE23-10-2016.pdf'
        );

    }

    /**
     * Fonction lancée en fin de classe.
     */
    public static function tearDownAfterClass() {
        unlink('../var/digitalization/SI/Done/numerisation2.pdf');
        unlink('../var/digitalization/SI/Done/T1DVISITE23-10-2016.pdf');
        unlink('../var/digitalization/SI/Todo/T00004DVISITE23-10-2016.pdf');
        unlink('../var/digitalization/SI/Todo/T00001PLOP23-10-2016.pdf');
        unlink('../var/digitalization/SI/Todo/T00001DVISITE23-13-2016.pdf');
        unlink('../var/digitalization/SI/Done/T1ARRETE23-10-2016.pdf');
        
        unlink('../var/digitalization/ACC/Done/numerisation1.pdf');
        unlink('../var/digitalization/ACC/Done/T1DVISITE23-10-2016.pdf');
        unlink('../var/digitalization/ACC/Todo/T00004DVISITE23-10-2016.pdf');
        unlink('../var/digitalization/ACC/Todo/T00001PLOP23-10-2016.pdf');
        unlink('../var/digitalization/ACC/Todo/T00001DVISITE23-13-2016.pdf');
        unlink('../var/digitalization/ACC/Done/T1ARRETE23-10-2016.pdf');
    }

    /**
     * Fonction lancée en debut de chaque test.
     */
    public function setUp() {
        // Instancie la timezone
        date_default_timezone_set('Europe/Paris');
        echo ' = '.get_class().'.'.
            str_replace('test_', '', $this->getName())."\r\n";
        @session_start();
        $_SESSION['collectivite'] = 1;
        $_SESSION['login'] = "admin";
        $_SERVER['REQUEST_URI'] = "";
        $f = new utils("nohtml");
        $f->disableLog();
        $this->digitalizedDocument = new DigitalizedDocument($f);
        $test_test = 0;
    }

    /**
     * Méthode appelée en cas de d'echec sur un test.
     *
     * @param Exception $e Exception a lever.
     */
    public function onNotSuccessfulTest(Exception $e) {
        echo 'Line '.$e->getLine().' : '.$e->getMessage()."\r\n";
        parent::onNotSuccessfulTest($e);
        
    }

    /**
     * Fonction lancée en fin de chaque test
     */
    public function tearDown() {
        // On détruit l'instance de la classe DigitalizedDocument
        $this->digitalizedDocument->__destruct();
    }

    /**
     * Test la fonction extractMetadataFromFilename.
     */
    public function testExtractMetadataFromFilename() {
        // Insertion d'un établissement en base
        $val['etablissement'] = 1;
        $val['code'] = "T1";
        $val['libelle'] = "Étab";
        $val['etablissement_nature'] = "1";
        $this->digitalizedDocument->f->db->autoExecute(
            DB_PREFIXE."etablissement",
            $val,
            DB_AUTOQUERY_INSERT
        );

        // Path de l'error log
        $log_file = "../var/log/error.log";

        // Sans convention de nommage
        $filename = "20091106AUTPCP.pdf";
        // Retour de la fonction extractMetadataFromFilename
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // On vérifie les données retournées
        $this->assertEquals(
            $filename,
            $extractMetadataFromFilename["title"]
        );
        $this->assertEquals(
            1,
            count($extractMetadataFromFilename)
        );

        // Sans convention de nommage :
        // le code établissement n'est pas sur 5 chiffres
        $filename = "T0001RVRAT05-10-2006.pdf";
        // Retour de la fonction extractMetadataFromFilename
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // On vérifie les données retournées
        $this->assertEquals(
            $filename,
            $extractMetadataFromFilename["title"]
        );
        $this->assertEquals(
            1,
            count($extractMetadataFromFilename)
        );

        // Sans convention de nommage :
        // la date n'est pas au bon format
        $filename = "T00001RVRAT2016-05-10.pdf";
        // Retour de la fonction extractMetadataFromFilename
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // On vérifie les données retournées
        $this->assertEquals(
            $filename,
            $extractMetadataFromFilename["title"]
        );
        $this->assertEquals(
            1,
            count($extractMetadataFromFilename)
        );

        // Avec convention de nommage
        // et informations valides
        $filename = "T00001RVRAT05-10-2006.pdf";
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        $this->assertEquals(
            "T00001RVRAT05-10-2006",
            $extractMetadataFromFilename["title"]
        );
        $this->assertEquals(
            "RVRAT",
            $extractMetadataFromFilename["type"]
        );
        $this->assertEquals(
            "1",
            $extractMetadataFromFilename["etablissement"]
        );
        $this->assertEquals(
            "7",
            $extractMetadataFromFilename["piece_type"]
        );
        $this->assertEquals(
            "2006-10-05",
            $extractMetadataFromFilename["date_creation"]
        );
        $this->assertEquals(
            5,
            count($extractMetadataFromFilename)
        );

        // Avec convention de nommage
        // mais date invalide
        $filename = "T00001RVRAT05-13-2006.pdf";
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // Écriture des erreurs (log de type DEBUG) dans le fichier d'erreurs
        logger::instance()->writeErrorLogToFile();
        logger::instance()->cleanLog();
        $this->assertEquals(
            null,
            $extractMetadataFromFilename
        );
        if (file_exists($log_file)) {
            $log_contents = file_get_contents($log_file);
        } else {
            $log_contents = "";
        }
        $pattern = "/Le format de date 05-13-2006 n'est pas valide\./";
        $this->assertNotEquals(preg_match($pattern, $log_contents), 0);

        // Avec convention de nommage
        // mais code du type de pièce invalide
        $filename = "T00001INVALIDCODE05-10-2006.pdf";
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // Écriture des erreurs (log de type DEBUG) dans le fichier d'erreurs
        logger::instance()->writeErrorLogToFile();
        logger::instance()->cleanLog();
        $this->assertEquals(
            null,
            $extractMetadataFromFilename
        );
        if (file_exists($log_file)) {
            $log_contents = file_get_contents($log_file);
        } else {
            $log_contents = "";
        }
        $pattern = "/Le type de pièce INVALIDCODE n'existe pas\./";
        $this->assertNotEquals(preg_match($pattern, $log_contents), 0);

        // Avec convention de nommage
        // mais code établissement inexistant
        $filename = "T12345RVAT05-10-2006.pdf";
        $extractMetadataFromFilename =
            $this->digitalizedDocument->extractMetadataFromFilename($filename);
        // Écriture des erreurs (log de type DEBUG) dans le fichier d'erreurs
        logger::instance()->writeErrorLogToFile();
        logger::instance()->cleanLog();
        $this->assertEquals(
            null,
            $extractMetadataFromFilename
        );
        if (file_exists($log_file)) {
            $log_contents = file_get_contents($log_file);
        } else {
            $log_contents = "";
        }
        $pattern = "/L'établissement T12345 n'existe pas\./";
        $this->assertNotEquals(preg_match($pattern, $log_contents), 0);
    }

    /**
     * Test la fonction extractMetadataFromFilename.
     */
    public function testRunImport() {

        //Nom de dossier
        $pathSrc_ACC = "../var/digitalization/ACC/Todo/";
        $pathSrc_SI = "../var/digitalization/SI/Todo/";
        //Nom dossier destination
        $pathDes_ACC = "../var/digitalization/ACC/Done/";
        $pathDes_SI = "../var/digitalization/SI/Done/";

        // Fichier non importés attendus
        $fileError = array(
            "T00004DVISITE23-10-2016.pdf",
            "T00001PLOP23-10-2016.pdf",
            "T00001DVISITE23-13-2016",
        );
        // Retour de la fonction run_import
        $run_import_ACC = $this->digitalizedDocument->run_import(
            $pathSrc_ACC,
            $pathDes_ACC,
            'ACC'
        );
        //On vérifie que l'action s'est bien déroulée
        $this->assertEquals($run_import_ACC, true);
        $this->assertEquals(sort($this->digitalizedDocument->filenameError), sort($fileError));
        
        $run_import_SI = $this->digitalizedDocument->run_import(
            $pathSrc_SI,
            $pathDes_SI,
            'SI'
        );
        //On vérifie que l'action s'est bien déroulée
        $this->assertEquals($run_import_SI, true);
        $this->assertEquals(sort($this->digitalizedDocument->filenameError), sort($fileError));

    }
}
