*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Les dossiers d'instruction (DI)...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    &{etab01} =  Create Dictionary
    ...  libelle=HOTEL RELAX
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=4
    ...  adresse_voie=AVE ROGER SALENGRO
    ...  adresse_complement=CHEZ M. DUPONT
    ...  adresse_cp=13002
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=2ème
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}
    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab01_titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}


Vérifier le widget Mes dossiers plan
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    Page Should Contain Element    css=div.widget_dossier_instruction_mes_plans
    Page Should Not Contain Errors
    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    Page Should Contain Element    css=div.widget_dossier_instruction_mes_plans
    Page Should Not Contain Errors


Vérifier le widget Mes dossiers visite
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    Page Should Contain Element    css=div.widget_dossier_instruction_mes_visites
    Page Should Not Contain Errors
    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    Page Should Contain Element    css=div.widget_dossier_instruction_mes_visites
    Page Should Not Contain Errors


Vérifier le menu
    #
    Depuis la page d'accueil    technicien-si    technicien-si
    Menu Should Contain Submenu    dossier_instruction_mes_plans
    Go To Submenu In Menu    dossiers    dossier_instruction_mes_plans
    Menu Should Contain Submenu    dossier_instruction_mes_visites
    Go To Submenu    dossier_instruction_mes_visites
    Menu Should Contain Submenu    dossier_instruction_tous_plans
    Go To Submenu    dossier_instruction_tous_plans
    Menu Should Contain Submenu    dossier_instruction_tous_visites
    Go To Submenu    dossier_instruction_tous_visites
    #
    Depuis la page d'accueil    technicien-acc    technicien-acc
    Menu Should Contain Submenu    dossier_instruction_mes_plans
    Go To Submenu In Menu    dossiers    dossier_instruction_mes_plans
    Menu Should Contain Submenu    dossier_instruction_mes_visites
    Go To Submenu    dossier_instruction_mes_visites
    Menu Should Contain Submenu    dossier_instruction_tous_plans
    Go To Submenu    dossier_instruction_tous_plans
    Menu Should Contain Submenu    dossier_instruction_tous_visites
    Go To Submenu    dossier_instruction_tous_visites
    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    Menu Should Contain Submenu    dossier_instruction_tous_plans
    Go To Submenu In Menu    dossiers    dossier_instruction_tous_plans
    Menu Should Contain Submenu    dossier_instruction_tous_visites
    Go To Submenu    dossier_instruction_tous_visites
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    Menu Should Contain Submenu    dossier_instruction_tous_plans
    Go To Submenu In Menu    dossiers    dossier_instruction_tous_plans
    Menu Should Contain Submenu    dossier_instruction_tous_visites
    Go To Submenu    dossier_instruction_tous_visites


Qualification d'un dossier d'instruction
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # On clique sur le bouton modifier
    Depuis le formulaire de modification du dossier d'instruction    ${dc01_libelle}-SI
    # On vérifie le select de l'autorité compétente
    ${list} =    Create List    Choisir autorité compétente    Commission communale de sécurité    Sous-commission départementale de sécurité
    Select List Should Be    css=#autorite_competente    ${list}
    # On sélectionne une autorité compétente
    Select From List By Label    css=#autorite_competente    Sous-commission départementale de sécurité
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.


Vérifier l'interface du dossier d'instruction
    #
    Depuis la page d'accueil    cadre-acc    cadre-acc
    #
    Depuis le contexte du dossier d'instruction    ${dc01_libelle}-ACC
    #On vérifie qu'on voit les champs urbanisme
    Element Should Be Visible    css=#lib-dossier_coordination_dossier_autorisation_ads
    Element Should Be Visible    css=#lib-dossier_coordination_dossier_instruction_ads
    #On vérifie qu'on voit les données saisies
    Element Should Contain    css=#libelle   ${dc01_libelle}-ACC
    Element Should Contain    css=#description   Dossier de coordination concernant un permis de construire
    Element Should Contain    css=#autorite_competente   ${EMPTY}
    Element Should Contain    css=#a_qualifier   Oui
    Element Should Contain    css=#dossier_cloture   Non
    Element Should Contain    css=#incompletude   Non
    Element Should Contain    css=#notes   ${EMPTY}
    Element Should Contain    css=.field-type-dossier_coordination_link .lienFormulaire   ${dc01_libelle}
    # On vérifie que la date de la demande est à aujourd'hui
    Element Should Contain    css=#dossier_coordination_date_demande    ${DATE_FORMAT_DD/MM/YYYY}
    Element Should Contain    css=#dossier_coordination_date_butoir    ${DATE_FORMAT_DD/MM/YYYY}
    Element Should Contain    css=#courrier_dernier_arrete   ${EMPTY}
    Element Should Contain    css=#dossier_coordination_autorite_police_encours   Non
    Element Should Contain    css=#dossier_coordination_dossier_cloture   Non
    Element Should Contain    css=#etablissement_coordonnees_adresse_ligne1   ${etab01.adresse_numero} ${etab01.adresse_voie}
    Element Should Contain    css=#etablissement_coordonnees_adresse_ligne2   ${etab01.adresse_complement}
    Element Should Contain    css=#etablissement_coordonnees_adresse_ligne3   ${etab01.adresse_cp} ${etab01.adresse_ville} ${etab01.adresse_arrondissement}
    Page Should Not Contain Element    css=#exploitant
    Page Should Not Contain Element    css=#etablissement_type_libelle
    Page Should Not Contain Element    css=#etablissement_categorie_libelle
    Page Should Not Contain Element    css=#si_locaux_sommeil
    Element Text Should Be    css=#etablissement_nature_libelle    ${etab01.etablissement_nature}
    Element Should Contain    css=#dossier_coordination_dossier_autorisation_ads   ${EMPTY}
    Element Should Contain    css=#dossier_coordination_dossier_instruction_ads   ${EMPTY}
    #On vérifie les actions
    Portlet Action Should Be In Form    dossier_instruction    modifier
    #
    Qualifier le dossier d'instruction    ${dc01_libelle}-ACC
    # On clique sur le bouton retour
    Click On Back Button
    Element Should Be Visible    css=.lock-16
    #On clôture le dossier
    Click On Link    clôturer
    Page Should Not Contain Errors
    Valid Message Should Contain    Le dossier d'instruction a été correctement cloturé.
    #On vérifie que le dossier est marqué comme clôturé
    Element Should Contain    css=#dossier_cloture   Oui
    #On vérifie les actions
    Element Should Not Be Visible    css=.edit-16
    Element Should Be Visible    css=.unlock-16
    #On déclôture le dossier
    Click On Link    réouvrir
    Page Should Not Contain Errors
    Valid Message Should Contain    Le dossier d'instruction a été correctement réouvert.
    #On vérifie que le dossier est marqué comme non-clôturé
    Element Should Contain    css=#dossier_cloture   Non
    #On vérifie les actions
    Element Should Be Visible    css=.edit-16
    Element Should Be Visible    css=.lock-16
    #On vérifie les onglets
    Page Should Contain Element    css=#piece
    Page Should Contain Element    css=#courrier
    # On vérifie que depuis le DI du service SI du même DC en tant que cadre ACC
    # on n'accède pas aux actions de modification
    Depuis le contexte du dossier de coordination    ${dc01_libelle}
    Click On Link    ${dc01_libelle}-SI
    Portlet Action Should Not Be In Form    dossier_instruction_tous_plans    modifier


Affectation automatique d'un technicien

    [Documentation]    ...

    # En tant qu'un utilisateur ADMINISTRATEUR
    Depuis la page d'accueil    admin    admin
    # On paramètre un technicien SI sur le 1er arrondissement
    Paramétrer l'arrondissement pour l'acteur    1er    Paul DURAND
    # On paramètre un technicien ACC sur le 1er arrondissement
    Ajouter l'acteur    Jacques ROBERT    technicien-acc    Accessibilité    technicien
    Paramétrer l'arrondissement pour l'acteur    1er    Jacques ROBERT

    # En tant qu'un utilisateur CADRE ACC
    Depuis la page d'accueil    cadre-acc    cadre-acc
    # On cré l'établissement n°1 sur le 1er arrondissement
    &{etab02} =  Create Dictionary
    ...  libelle=OCÉAN INDIEN
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=39
    ...  adresse_voie=RUE PAPERE
    ...  adresse_cp=13001
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=1er
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martel
    ...  exp_prenom=Lyne
    ${etab02_code} =  Ajouter l'établissement  ${etab02}
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}
    # On cré le DC n°1 lié à l'établissement n°1
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab02_titre}
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  a_qualifier=false
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}
    Set Suite Variable    ${dc02}
    Set Suite Variable    ${dc02_libelle}
    # On cré l'établissement n°2 sur le 10ème arrondissement
    &{etab03} =  Create Dictionary
    ...  libelle=OCÉAN PACIFIQUE
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=42
    ...  adresse_voie=AVE LOUIS TORRESANI
    ...  adresse_cp=13010
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=10ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martel
    ...  exp_prenom=Lyne
    ${etab03_code} =  Ajouter l'établissement  ${etab03}
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}
    # On cré le DC n°2 lié à l'établissement n°2
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination concernant un permis de construire
    ...  etablissement=${etab03_titre}
    ...  date_demande=${DATE_FORMAT_DD/MM/YYYY}
    ...  a_qualifier=false
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}
    Set Suite Variable    ${dc03}
    Set Suite Variable    ${dc03_libelle}
    # On vérifie que le technicien ACC paramétré sur le 1er arrondissement est bien été affecté sur le DI n°1
    Depuis le contexte du dossier d'instruction    ${dc02_libelle}-ACC
    Element Should Contain    css=#technicien    Jacques ROBERT
    # On vérifie qu'aucun technicien n'a été affecté sur le DI n°2
    Depuis le contexte du dossier d'instruction    ${dc03_libelle}-ACC
    Element Should Contain    css=#technicien    ${EMPTY}


Affectation manuelle d'un technicien
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    Page Should Not Contain Errors
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Permis de construire
    # On saisie une description
    Input Text    css=#description    Dossier de coordination concernant un permis de construire
    # On vérifie que la date de la demande est à aujourd'hui
    Form Value Should Be    css=#date_demande    ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie les données valorisé par le paramétrage
    Checkbox Should Be Selected    css=#a_qualifier
    Checkbox Should Be Selected    css=#dossier_instruction_secu
    Checkbox Should Be Selected    css=#dossier_instruction_acc
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01.libelle}
    # On ajoute un établissement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Link    css=.autocomplete-etablissement_tous-newrecord
    # On renseigne seulement les champs obligatoire
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Input Text    css=#form-etablissement_tous-overlay #libelle    ${etab01.libelle}
    Select From List By Label    css=#form-etablissement_tous-overlay #etablissement_nature    ERP potentiel
    # On valide le sous-formulaire
    Click On Submit Button In Overlay    etablissement_tous
    Page Should Not Contain Errors
    # On ferme l'overlay
    Click On Back Button In Overlay    etablissement_tous
    # On vérifie que le dossier de coordination est bien lié à l'établissement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#autocomplete-etablissement_tous-check
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    Page Should Not Contain Errors
    # On récupère l'identifiant du dossier de coordination
    ${dossier_coordination_libelle} =    Get Text    css=div.form-content span#libelle
    Set Suite Variable    ${dossier_coordination_libelle}
    #
    Depuis le contexte du dossier d'instruction    ${dossier_coordination_libelle}-SI
    #Aucun technicien ne devrait être affecté
    Element Should Contain    css=#technicien    ${EMPTY}
    Click On Form Portlet Action    dossier_instruction    modifier
    #On vérifie que le select ne contient pas de technicien du service accessibilité
    Page Should Not Contain    Pierre BERNARD
    #On choisit un technicien
    Select From List By Label    css=#technicien    Paul DURAND
    Click On Submit Button
    Click On Back Button
    #On vérifie que le technicien a bien été affecté
    Element Should Contain    css=#technicien    Paul DURAND


Affectation par lot d'un technicien

    #On va créer deux dossiers de coordination qui auront chacun un dossier PLAN
    #SI sans technicien affecté
    Depuis la page d'accueil    cadre-si    cadre-si
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    Page Should Not Contain Errors
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Permis de construire
    # On saisie une description
    Input Text    css=#description    Dossier de coordination concernant un permis de construire
    # On vérifie que la date de la demande est à aujourd'hui
    Form Value Should Be    css=#date_demande    ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie les données valorisé par le paramétrage
    Checkbox Should Be Selected    css=#a_qualifier
    Checkbox Should Be Selected    css=#dossier_instruction_secu
    Checkbox Should Be Selected    css=#dossier_instruction_acc
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01.libelle}
    # On ajoute un établissement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Link    css=.autocomplete-etablissement_tous-newrecord
    # On renseigne seulement les champs obligatoire
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Input Text    css=#form-etablissement_tous-overlay #libelle    ${etab01.libelle}
    Select From List By Label    css=#form-etablissement_tous-overlay #etablissement_nature    ERP potentiel
    # On valide le sous-formulaire
    Click On Submit Button In Overlay    etablissement_tous
    Page Should Not Contain Errors
    # On ferme l'overlay
    Click On Back Button In Overlay    etablissement_tous
    # On vérifie que le dossier de coordination est bien lié à l'établissement
    Element Should Be Visible    css=#autocomplete-etablissement_tous-check
    # On valide le formulaire
    Click On Submit Button
    Page Should Not Contain Errors
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    Page Should Not Contain Errors
    # On récupère l'identifiant du dossier de coordination
    ${dossier_coordination_libelle_1} =    Get Text    css=div.form-content span#libelle
    Set Suite Variable    ${dossier_coordination_libelle_1}
    # On clique sur le tableau de bord
    Go To Dashboard
    Page Should Not Contain Errors
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    dossier_coordination_nouveau
    Page Should Not Contain Errors
    # On sélectionne le type du dossier de coordination
    Select From List By Label    css=#dossier_coordination_type    Permis de construire
    # On saisie une description
    Input Text    css=#description    Dossier de coordination concernant un permis de construire
    # On vérifie que la date de la demande est à aujourd'hui
    Form Value Should Be    css=#date_demande    ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie les données valorisé par le paramétrage
    Checkbox Should Be Selected    css=#a_qualifier
    Checkbox Should Be Selected    css=#dossier_instruction_secu
    Checkbox Should Be Selected    css=#dossier_instruction_acc
    # On décoche "a_qualifier"
    Unselect Checkbox    css=#a_qualifier
    # On recherche un établissement grâce au champ "autocomplete"
    Input Text    css=#autocomplete-etablissement_tous-search    ${etab01.libelle}
    # On ajoute un établissement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Link    css=.autocomplete-etablissement_tous-newrecord
    # On renseigne seulement les champs obligatoire
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Input Text    css=#form-etablissement_tous-overlay #libelle    ${etab01.libelle}
    Select From List By Label    css=#form-etablissement_tous-overlay #etablissement_nature    ERP potentiel
    # On valide le sous-formulaire
    Click On Submit Button In Overlay    etablissement_tous
    Page Should Not Contain Errors
    # On ferme l'overlay
    Click On Back Button In Overlay    etablissement_tous
    # On vérifie que le dossier de coordination est bien lié à l'établissement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Be Visible    css=#autocomplete-etablissement_tous-check
    # On valide le formulaire
    Click On Submit Button
    Page Should Not Contain Errors
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    Page Should Not Contain Errors
    # On récupère l'identifiant du dossier de coordination
    ${dossier_coordination_libelle_2} =    Get Text    css=div.form-content span#libelle
    Set Suite Variable    ${dossier_coordination_libelle_2}
    ${dossier_instruction_si_libelle_1} =    Catenate    SEPARATOR=    ${dossier_coordination_libelle_1}    -SI
    ${dossier_instruction_si_libelle_2} =    Catenate    SEPARATOR=    ${dossier_coordination_libelle_2}    -SI


    # On vérifie qu'aucun technicien n'est affecté sur le premier dossier d'instruction
    Depuis le contexte du dossier d'instruction    ${dossier_instruction_si_libelle_1}
    Element Should Contain    css=#technicien    ${EMPTY}
    # On récupère l'identifiant du dossier d'instruction pour l'utiliser plus tard
    ${dossier_coordination_id_1} =    Get Value    css=div.form-content #dossier_instruction
    Set Suite Variable    ${dossier_coordination_id_1}
    # On vérifie qu'aucun technicien n'est affecté sur le second dossier d'instruction
    Depuis le contexte du dossier d'instruction    ${dossier_instruction_si_libelle_2}
    Element Should Contain    css=#technicien    ${EMPTY}

    #
    Depuis l'interface d'affectation par lot
    #On vérifie que seul les techniciens SI sont affichés
    Page Should Not Contain    Pierre BERNARD
    #On choisit le technicien
    Select From List By Label    css=#technicien    Paul DURAND
    #On choisit un des dossiers d'instruction
    #Select Checkbox    xpath=//input[@name='dossier_instruction[]'][1]
    ${xpath_selector} =    Catenate    SEPARATOR=    xpath=//input[@value='    ${dossier_coordination_id_1}
    ${xpath_selector} =    Catenate    SEPARATOR=    ${xpath_selector}    ']
    Select Checkbox    ${xpath_selector}
    #On affecte un technicien en validant le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain    Affectation du technicien effectuée avec succés

    # On vérifie que le technicien est bien affecté sur le dossier d'instruction choisit
    Depuis le contexte du dossier d'instruction    ${dossier_instruction_si_libelle_1}
    Element Should Contain    css=#technicien    Paul DURAND
    # On vérifie qu'aucun technicien n'est affecté sur le dossier d'instruction non choisit
    Depuis le contexte du dossier d'instruction    ${dossier_instruction_si_libelle_2}
    Element Should Contain    css=#technicien    ${EMPTY}

Vérification des droits selon le service
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Visite de réception
    ...  description=Aménagement du sous-sol
    ...  date_butoir=25/11/2014
    ...  a_qualifier=false
    ${dc04_libelle} =  Ajouter le dossier de coordination  ${dc04}
    Set Suite Variable  ${dc04}
    Set Suite Variable  ${dc04_libelle}

    # En profil SI sur DI SI
    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le contexte du dossier d'instruction  ${dc04_libelle}-SI
    # Analyse
    On clique sur l'onglet  analyses  Analyse
    Element Should Be Visible  css=div.bloc_titre_widget_analyses span.edit-16
    # PV
    On clique sur l'onglet  proces_verbal  PV
    Element Should Be Visible  css=#ajouter_pv
    # Documents entrants
    On clique sur l'onglet  piece  Documents Entrants
    Element Should Be Visible  css=span.add-16
    # Documents générés
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Be Visible  css=span.add-16
    # Réunions
    On clique sur l'onglet  dossier_instruction_reunion_contexte_di  Réunions
    Element Should Be Visible  css=span.add-16
    # Visites
    On clique sur l'onglet  visite  Visites
    Element Should Not Be Visible  css=span.add-16

    # En profil SI sur DI ACC
    Depuis le contexte du dossier de coordination  ${dc04_libelle}
    Click On Link  ${dc04_libelle}-ACC
    # Analyse
    On clique sur l'onglet  analyses  Analyse
    Element Should Not Be Visible  css=div.bloc_titre_widget_analyses span.edit-16
    # PV
    On clique sur l'onglet  proces_verbal  PV
    Element Should Not Be Visible  css=#ajouter_pv
    # Documents entrants
    On clique sur l'onglet  piece  Documents Entrants
    Element Should Not Be Visible  css=span.add-16
    # Documents générés
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Not Be Visible  css=span.add-16
    # Réunions
    On clique sur l'onglet  dossier_instruction_reunion_contexte_di  Réunions
    Element Should Not Be Visible  css=span.add-16
    # Visites
    On clique sur l'onglet  visite  Visites
    Element Should Not Be Visible  css=span.add-16

    # En profil ACC sur DI ACC
    Depuis la page d'accueil  cadre-acc  cadre-acc
    Depuis le contexte du dossier d'instruction  ${dc04_libelle}-ACC
    # Analyse
    On clique sur l'onglet  analyses  Analyse
    Element Should Be Visible  css=div.bloc_titre_widget_analyses span.edit-16
    # PV
    On clique sur l'onglet  proces_verbal  PV
    Element Should Be Visible  css=#ajouter_pv
    # Documents entrants
    On clique sur l'onglet  piece  Documents Entrants
    Element Should Be Visible  css=span.add-16
    # Documents générés
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Be Visible  css=span.add-16
    # Réunions
    On clique sur l'onglet  dossier_instruction_reunion_contexte_di  Réunions
    Element Should Be Visible  css=span.add-16
    # Visites
    On clique sur l'onglet  visite  Visites
    Element Should Not Be Visible  css=span.add-16

    # En profil ACC sur DI SI
    Depuis le contexte du dossier de coordination  ${dc04_libelle}
    Click On Link  ${dc04_libelle}-SI
    # Analyse
    On clique sur l'onglet  analyses  Analyse
    Element Should Not Be Visible  css=div.bloc_titre_widget_analyses span.edit-16
    # PV
    On clique sur l'onglet  proces_verbal  PV
    Element Should Not Be Visible  css=#ajouter_pv
    # Documents entrants
    On clique sur l'onglet  piece  Documents Entrants
    Element Should Not Be Visible  css=span.add-16
    # Documents générés
    On clique sur l'onglet  courrier  Documents Générés
    Element Should Not Be Visible  css=span.add-16
    # Réunions
    On clique sur l'onglet  dossier_instruction_reunion_contexte_di  Réunions
    Element Should Not Be Visible  css=span.add-16
    # Visites
    On clique sur l'onglet  visite  Visites
    Element Should Not Be Visible  css=span.add-16