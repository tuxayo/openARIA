*** Settings ***
Documentation  Teste les fonctionnalités SIG : liens de redirection vers le SIG pour un
...  ou plusieurs éléments, géocodage d'un objet,

# On inclut les mots-clefs
Resource  resources/resources.robot
# On ouvre/ferme le navigateur au début/à la fin du Test Suite.
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown

*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de de données
    ...  cohérent pour les scénarios fonctionnels qui suivent.

    ${date_du_jour} =  Date du jour au format dd/mm/yyyy
    Set Suite Variable    ${date_du_jour}

    Depuis la page d'accueil    admin    admin
    # On importe les établissements
    Depuis l'import    import_etablissement
    Add File    fic1    import_etablissements_sig.csv
    Click On Submit Button In Import CSV
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    11 ligne(s) importée(s)


    Depuis la page d'accueil    cadre-si    cadre-si

    # Établissement 1
    ${etab_1_libelle} =  Set Variable  HOTEL LE MISTRAL
    &{etab} =  Create Dictionary
    ...  libelle=${etab_1_libelle}
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=77
    ...  adresse_voie=RUE ALDEBERT
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73482742011234
    ...  exp_nom=Daigle
    ...  exp_prenom=Luc
    ${etab_1_code} =  Ajouter l'établissement  ${etab}
    ${etab_1_id} =  Get Mandatory Value  css=#etablissement
    Set Suite Variable  ${etab_1_id}
    Set Suite Variable  ${etab_1_code}
    Set Suite Variable  ${etab_1_libelle}

    # Dossier de coordination 1
    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab_1_code} - ${etab_1_libelle}
    ...  date_demande=${date_du_jour}
    ${dc_1_libelle} =  Ajouter le dossier de coordination  ${dc}
    Set Suite Variable  ${dc_1_libelle}

    # Établissement 2
    ${etab_2_libelle} =  Set Variable  PARC DU MISTRAL
    &{etab} =  Create Dictionary
    ...  libelle=${etab_2_libelle}
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=54
    ...  adresse_voie=BD AIME BOISSY
    ...  adresse_cp=13004
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=4ème
    ...  siret=73482742011234
    ...  exp_nom=Brunelle
    ...  exp_prenom=Thibault
    ${etab_2_code} =  Ajouter l'établissement  ${etab}
    ${etab_2_id} =  Get Mandatory Value  css=#etablissement
    Set Suite Variable  ${etab_2_id}
    Set Suite Variable  ${etab_2_code}
    Set Suite Variable  ${etab_2_libelle}
    # Dossier de coordination 2
    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab_2_code} - ${etab_2_libelle}
    ...  date_demande=${date_du_jour}
    ${dc_2_libelle} =  Ajouter le dossier de coordination  ${dc}
    Set Suite Variable  ${dc_2_libelle}

    # Établissement 3
    ${etab_3_libelle} =  Set Variable  BAR DU MISTRAL
    &{etab} =  Create Dictionary
    ...  libelle=${etab_3_libelle}
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=74
    ...  adresse_voie=TRA AMEDEE PIERRE
    ...  adresse_cp=13016
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=16ème
    ...  siret=73482742011234
    ...  exp_nom=Lafrenière
    ...  exp_prenom=Tristan
    ${etab_3_code} =  Ajouter l'établissement  ${etab}
    Set Suite Variable  ${etab_3_code}
    Set Suite Variable  ${etab_3_libelle}
    # Dossier de coordination 3
    &{dc} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab_3_code} - ${etab_3_libelle}
    ...  date_demande=${date_du_jour}
    ${dc_3_libelle} =  Ajouter le dossier de coordination  ${dc}
    Set Suite Variable  ${dc_3_libelle}

    &{etab01} =  Create Dictionary
    ...  libelle=KIOSQUE
    ...  etablissement_nature=ERP potentiel

    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_id} =  Get Mandatory Value  css=#etablissement
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    ${etab01_libelle} =  Set Variable  ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_id}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}
    Set Suite Variable  ${etab01_libelle}

    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Permis
    ...  etablissement=${etab01_titre}
    ...  date_demande=28/01/2015
    ...  a_qualifier=false
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    # On récupère l'identifiant du dossier de coordination
    ${dc01} =  Get Mandatory Value  css=#fieldset-form-dossier_coordination-informations-generales #dossier_coordination
    Set Suite Variable  ${dc01}
    Set Suite Variable  ${dc01_libelle}

    # Établissement 4 initialement créé pour le test sur la récupération des
    # propriétaires
    # Ses références cadastrales sont modifiées au cours du test
    ${etab_4_libelle} =  Set Variable  MUSEE TUSSO
    @{etab_4_ref_cad} =  Create List  806  AB  0025
    &{etab_4} =  Create Dictionary
    ...  libelle=${etab_4_libelle}
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=PA
    ...  etablissement_categorie=1
    ...  etablissement_etat=Ouvert
    ...  adresse_numero=39
    ...  adresse_voie=IMP BOULANG-COLAVERI
    ...  adresse_cp=13009
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=9ème
    ...  siret=73482742011234
    ...  exp_nom=Binet
    ...  exp_prenom=Perrin
    ...  terrain_references_cadastrales=${etab_4_ref_cad}
    ${etab_4_code} =  Ajouter l'établissement  ${etab_4}
    Set Suite Variable  ${etab_4_code}
    Set Suite Variable  ${etab_4_libelle}
    # Dossier de coordination 4 créé pour le test sur la récupération des
    # propriétaires
    # Ses références cadastrales sont modifiées au cours du test
    &{dc_4} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab_4_code} - ${etab_4_libelle}
    ...  date_demande=${date_du_jour}
    ...  terrain_references_cadastrales=${etab_4_ref_cad}
    ${dc_4_libelle} =  Ajouter le dossier de coordination  ${dc_4}
    Set Suite Variable  ${dc_4_libelle}


Liste des visites sur les établissements proches
    [Documentation]    On le teste le filtre sur établissement proche

    # Test de la fonctionnalité sans le SIG activé
    Depuis la page d'accueil    cadre-si    cadre-si
    # Création de la programmation
    Ajouter la programmation depuis le menu    null    2015    02

    Page Should Not Contain Element    id=code_etab
    Page Should Not Contain Element    id=rayon
    Page Should Not Contain Element    id=btn_valider

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    #
    Depuis la page d'accueil    cadre-si    cadre-si

    # Création des 4 visites
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=T2500 - ALDI
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}

    Depuis la page d'accueil    cadre-si    cadre-si
    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=T2501 - CORA
    ${dc02_libelle} =  Ajouter le dossier de coordination  ${dc02}

    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=T2502 - MAMMOUTH
    ${dc03_libelle} =  Ajouter le dossier de coordination  ${dc03}

    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=T2503 - CARROUF
    ${dc04_libelle} =  Ajouter le dossier de coordination  ${dc04}

    # On accède à la programmation
    Depuis l'interface de planification de la programmation    2015/02
    # Vérification de l'état des champs
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible    id=code_etab
    Element Should Not Be Visible    id=rayon
    Element Should Not Be Visible    id=btn_valider
    Click Button    id=btn_proches
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible    id=code_etab
    Element Should Be Visible    id=rayon
    Element Should Be Visible    id=btn_valider

    # Vérification des visites des établissements à 50m
    Click Button    id=btn_valider
    Error Message Should Be    Veuillez saisir un code établissement valide (numéro T).

    # NB : s'il n'y a aucun résultat un message apparaît dans une ligne,
    # d'où le fait que l'on ne compare jamais à 0 le nombre de visites proposées
    # mais que l'on vérifie la présence/absence de ce message.

    # Vérification des visites des établissements à 50m
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Input Text    id=code_etab    T666
    Click Button    id=btn_valider
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    1    Mauvais nombre de visite(s) listée(s)
    Valid Message Should Be    Aucun établissement trouvé dans un rayon de 50 mètres autour de l'établissement T666.
    Page Should Contain  Aucun enregistrement.
    # Vérification des visites des établissements à 100m
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    id=rayon    100m
    Click Button    id=btn_valider
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    1    Mauvais nombre de visite(s) listée(s)
    Valid Message Should Be    Géolocalisé autour de l'établissement T666 dans un rayon de 100 mètres mais aucun dossier visite ne correspond.
    Page Should Contain  Aucun enregistrement.
    # Vérification des visites des établissements à 200m
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    id=rayon    200m
    Click Button    id=btn_valider
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    1    Mauvais nombre de visite(s) listée(s)
    Valid Message Should Be    Géolocalisé autour de l'établissement T666 dans un rayon de 200 mètres.
    Page Should Not Contain  Aucun enregistrement.
    # Vérification des visites des établissements à 300m
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    id=rayon    300m
    Click Button    id=btn_valider
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    2    Mauvais nombre de visite(s) listée(s)
    Valid Message Should Be    Géolocalisé autour de l'établissement T666 dans un rayon de 300 mètres.
    # Vérification des visites des établissements à 500m
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select From List By Label    id=rayon    500m
    Click Button    id=btn_valider
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    2    Mauvais nombre de visite(s) listée(s)
    Valid Message Should Be    Géolocalisé autour de l'établissement T666 dans un rayon de 500 mètres.
    # Vérification des visites des établissements à 500m avec filtre
    Select Checkbox    id=locaux_a_sommeil_btn
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    1    Mauvais nombre de visite(s) listée(s)
    # Retour sur toutes les visites
    Click Button    id=btn_tous
    # Vérification de l'état des champs
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Be Visible    id=code_etab
    Element Should Not Be Visible    id=rayon
    Element Should Not Be Visible    id=btn_valider
    # Vérification du message
    Element Should Not Be Visible    css=#msg_view_programmation div.message
    # Vérification des résultats
    ${visit_etab} =    Get Matching Xpath Count    //table[@id='tableau_propositions']/tbody/tr
    Should Be Equal    ${visit_etab}    2    Mauvais nombre de visite(s) listée(s)

    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig


Redirection entrante
    [Documentation]  Vérifie la redirection vers les établissements et les
    ...  dossiers de coordination depuis l'extérieur vers openARIA.

    Depuis la page d'accueil  cadre-si  cadre-si

    ## Rédirection vers la page d'accueil
    # Les paramètres 'obj', 'action' et 'by' ne sont pas renseignés
    Go To  ${PROJECT_URL}app/entry.php
    Page Title Should Be  Tableau De Bord
    # La valeur du paramètre 'obj' est différent de 'etablissement' ou de 'dc'
    Go To  ${PROJECT_URL}app/entry.php?action=view&by=code&id=${etab_1_code}&obj=etab
    Page Title Should Be  Tableau De Bord
    # La valeur du parmètre 'action' est différent de 'view' ou de 'list'
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&by=code&id=${etab_1_code}&action=3
    Page Title Should Be  Tableau De Bord
    # La valeur du paramètre 'action' est égale à 'view' mais le paramètre 'id'
    # n'est pas renseigné
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&action=view&by=code&ids=${etab_1_code}
    Page Title Should Be  Tableau De Bord
    # La valeur du paramètre 'action' est égale à 'list' mais le paramètre 'ids'
    # n'est pas renseigné
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&action=list&by=code&id=${etab_1_code}
    Page Title Should Be  Tableau De Bord

    ## Redirection vers le listings des établissements
    # Recherche de plusieurs établissements
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&action=list&by=code&ids=${etab_1_code},${etab_2_code}
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  ${etab_1_code},${etab_2_code}
    Element Should Contain  css=table.tab-tab  ${etab_1_code}
    Element Should Contain  css=table.tab-tab  ${etab_2_code}
    Element Should Not Contain  css=table.tab-tab  ${etab_3_code}
    # Recherche d'un établissement
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&action=list&by=code&ids=${etab_1_code}
    Page Title Should Be  Établissements > Tous Les Établissements
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#etablissement  ${etab_1_code}
    Element Should Contain  css=table.tab-tab  ${etab_1_code}
    Element Should Not Contain  css=table.tab-tab  ${etab_2_code}
    Element Should Not Contain  css=table.tab-tab  ${etab_3_code}

    ## Redirection vers le formulaire d'un établissement
    # Recherche depuis le champ 'code'
    Go To  ${PROJECT_URL}app/entry.php?obj=etablissement&action=view&by=code&id=${etab_1_code}
    Page Title Should Be  Établissements > Tous Les Établissements > ${etab_1_code} - ${etab_1_libelle}

    ## Redirection vers le listings des dossiers de coordination
    # Recherche de plusieurs dossiers de coordination
    Go To  ${PROJECT_URL}app/entry.php?obj=dc&action=list&by=libelle&ids=${dc_1_libelle},${dc_2_libelle}
    Page Title Should Be  Dossiers > DC > Tous Les Dossiers
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#libelle  ${dc_1_libelle},${dc_2_libelle}
    Element Should Contain  css=table.tab-tab  ${dc_1_libelle}
    Element Should Contain  css=table.tab-tab  ${dc_2_libelle}
    Element Should Not Contain  css=table.tab-tab  ${dc_3_libelle}
    # Recherche d'un dossier de coordination
    Go To  ${PROJECT_URL}app/entry.php?obj=dc&action=list&by=libelle&ids=${dc_1_libelle}
    Page Title Should Be  Dossiers > DC > Tous Les Dossiers
    Form Value Should Be  css=div#adv-search-adv-fields div.form-content input#libelle  ${dc_1_libelle}
    Element Should Contain  css=table.tab-tab  ${dc_1_libelle}
    Element Should Not Contain  css=table.tab-tab  ${dc_2_libelle}
    Element Should Not Contain  css=table.tab-tab  ${dc_3_libelle}

    ## Redirection vers le formulaire d'un dossier de coordination
    # Recherche depuis le champ 'libelle'
    Go To  ${PROJECT_URL}app/entry.php?obj=dc&action=view&by=libelle&id=${dc_1_libelle}
    Page Title Should Contain  Dossiers > DC > Tous Les Dossiers > ${dc_1_libelle}


Synchronisation des contraintes
    [Documentation]  Test de la synchronisation du paramétrage avec le référentiel :
    ...  - archivage si contrainte obsolète
    ...  - ajout si contrainte absente
    ...  - modification si contrainte présente

    Depuis la page d'accueil  admin  admin
    # Le menu doit être absent sans option SIG activée
    Go To  ${PROJECT_URL}app/settings.php
    Page Should Not Contain  synchronisation
    Activer le plugin geoaria_tests et l'option sig
    Go To  ${PROJECT_URL}app/settings.php
    Page Should Contain  synchronisation
    # 1ère synchro
    # → il n'y a que des ajouts au nombre de 4
    Synchroniser les contraintes
    Valid Message Should Contain  4 ajoutées
    Valid Message Should Contain  0 archivée
    Valid Message Should Contain  0 mise à jour
    # 2ème synchro
    # → les 4 sont mises à jour
    Synchroniser les contraintes
    Valid Message Should Contain  0 ajoutée
    Valid Message Should Contain  0 archivée
    Valid Message Should Contain  4 mises à jour
    # 3ème synchro : changement de commune
    # → les 4 anciennes sont archivées et 2 nouvelles sont ajoutées
    Modifier le paramètre  insee  13005
    Synchroniser les contraintes
    Valid Message Should Contain  2 ajoutées
    Valid Message Should Contain  4 archivées
    Valid Message Should Contain  0 mise à jour
    # 4ème synchro
    # → les 2 sont mises à jour
    Synchroniser les contraintes
    Valid Message Should Contain  0 ajoutée
    Valid Message Should Contain  0 archivée
    Valid Message Should Contain  2 mises à jour
    # Vérification du paramétrage
    Go To Tab  contrainte
    Page Should Contain  Contrainte 13005.A70
    Page Should Contain  Contrainte 13005.A80
    Page Should Not Contain  Contrainte 13055.M70
    Page Should Not Contain  Contrainte 13055.M80
    Page Should Not Contain  Contrainte 13055.M81
    Page Should Not Contain  Contrainte 13055.M90
    Click Element  css=span.om_validite_link > a
    Page Should Contain  Contrainte 13055.M70
    Page Should Contain  Contrainte 13055.M80
    Page Should Contain  Contrainte 13055.M81
    Page Should Contain  Contrainte 13055.M90
    # Remise dans l'état initial
    Modifier le paramètre  insee  13055
    Désactiver le plugin geoaria_tests et l'option sig
    Supprimer la contrainte  Contrainte 13005.A70
    Supprimer la contrainte  Contrainte 13005.A80
    Supprimer la contrainte archivée  Contrainte 13055.M70
    Supprimer la contrainte archivée  Contrainte 13055.M80
    Supprimer la contrainte archivée  Contrainte 13055.M81
    Supprimer la contrainte archivée  Contrainte 13055.M90

Création d'établissements avec géocodage et test des liens de dessin d'emprise manuel

    [Documentation]  Teste les fonctionnalités de géocodage d'un établissement. Les
    ...  établissements créés sont utilisés dans d'autres test suites.

    ##
    ## Cas d'utilisation n°1 :
    ##
    ## Création d'un établissement sans références cadastrales mais avec une voie
    ## très précise par rapport au connecteur test.
    ## Lors de la création, la tentative de géocodage échoue, on clique alors sur le lien
    ## de redirection pour dessiner l'établissement manuellement sur le SIG. On relance
    ## alors l'action de géolocalisation depuis la fiche détaillée de l'établissement, qui
    ## doit fonctionner. Le champ "Géolocalisé" doit être mis à jour.
    ##

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig

    # On modifie le champ id_voie_ref de la rue de Rome qui est envoyé par la
    # suite au SIG
    &{voie} =  Create Dictionary
    ...  libelle=Rue de Rome
    ...  id_voie_ref=100

    Modifier voie  Rue de Rome  ${voie}

    Depuis la page d'accueil  cadre-si  cadre-si

    &{etab02} =  Create Dictionary
    ...  libelle=Métro la Fourragère
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=350
    ...  adresse_voie=Rue de Rome

    # Découpage de l'ajout de l'établissement pour vérifier les messages de géolocalisation
    Go To  ${PROJECT_URL}scr/form.php?obj=etablissement_tous&action=0
    Saisir les valeurs dans le formulaire de l'établissement  ${etab02}
    # XXX
    Click Element  css=legend.collapsed
    Input HTML  acc_consignes_om_html  Consigne
    Input HTML  acc_descriptif_om_html  Description
    Input HTML  si_consignes_om_html  Consigne
    Input HTML  si_descriptif_om_html  Description
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    Select From List By Label  css=#si_autorite_competente_plan  Commission communale de sécurité
    Select From List By Label  css=#si_type_alarme  2a
    Select From List By Label  css=#si_type_ssi  A
    Click On Submit Button
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  L'établissement n'a pas pu être créé automatiquement sur le SIG.
    Valid Message Should Contain  Cliquez ici pour le dessiner.
    Click Element  css=#link_dessin_emprise_sig
    @{URL} =  Create List  http://localhost/erp_etab/create/T
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  ${URL}  false

    # On clique sur le bouton retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On se rend sur la fiche de l'établissement
    Input Text  css=#libelle  ${etab02.libelle}
    Click On Search Button
    Click On Link  ${etab02.libelle}

    # Récupération du code de l'établissement
    ${etab02_code} =  Get Text  css=#code
    ${etab02_id} =  Get Mandatory Value  css=#etablissement
    ${etab02_titre} =  Set Variable  ${etab02_code} - ${etab02.libelle}
    ${etab02_libelle} =  Set Variable  ${etab02.libelle}
    Set Suite Variable  ${etab02}
    Set Suite Variable  ${etab02_id}
    Set Suite Variable  ${etab02_code}
    Set Suite Variable  ${etab02_titre}
    Set Suite Variable  ${etab02_libelle}

    #
    Element Should Contain  geolocalise  Non
    Portlet Action Should Be In Form  etablissement_tous  geocoder
    Click Element  action-form-etablissement_tous-geocoder
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  L'établissement a été géolocalisé avec une précision de 10m.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser

    ##
    ## Cas d'utilisation n°2 :
    ##
    ## Création d'un établissement avec références cadastrales et adresse.
    ## Lors de la création, la tentative de géocodage doit fonctionner. On vérifie que le
    ## champ "géolocalisé" sur la fiche détaille de l'établissement a bien été mis à jour.
    ## L'action du portlet "géolocaliser" ne doit pas être présente.
    ##

    @{ref_cad} =  Create List  806  AB  25
    &{etab03} =  Create Dictionary
    ...  libelle=Centre Commercial des Caillols
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=150
    ...  adresse_cp=13012
    ...  adresse_ville=MARSEILLE
    ...  terrain_references_cadastrales=${ref_cad}

    # Découpage de l'ajout de l'établissement pour vérifier les messages de géolocalisation
    Go To  ${PROJECT_URL}scr/form.php?obj=etablissement_tous&action=0
    Saisir les valeurs dans le formulaire de l'établissement  ${etab03}
    # XXX
    Click Element  css=legend.collapsed
    Input HTML  acc_consignes_om_html  Consigne
    Input HTML  acc_descriptif_om_html  Description
    Input HTML  si_consignes_om_html  Consigne
    Input HTML  si_descriptif_om_html  Description
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    Select From List By Label  css=#si_autorite_competente_plan  Commission communale de sécurité
    Select From List By Label  css=#si_type_alarme  2a
    Select From List By Label  css=#si_type_ssi  A
    Click On Submit Button
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  L'établissement a été géolocalisé avec une précision de 0m.

    # On clique sur le bouton retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On se rend sur la fiche de l'établissement
    Input Text  css=#libelle  ${etab03.libelle}
    Click On Search Button
    Click On Link  ${etab03.libelle}

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser
    Portlet Action Should Not Be In Form  etablissement_tous  geocoder

    # Récupération du code de l'établissement
    ${etab03_code} =  Get Text  css=#code
    ${etab03_titre} =  Set Variable  ${etab03_code} - ${etab03.libelle}
    ${etab03_libelle} =  Set Variable  ${etab03.libelle}

    Set Suite Variable  ${etab03}
    Set Suite Variable  ${etab03_code}
    Set Suite Variable  ${etab03_titre}
    Set Suite Variable  ${etab03_libelle}

    ##
    ## Cas d'utilisation n°3 :
    ##
    ## Création d'un établissement déjà géolocalisé dans le SIG (numéro 9797 pour
    ## le connecteur de test).
    ## Lors de la création, la tentative de géocodage doit échouer mais retourner
    ## un message précisant que cet établissement est déjà géocodé. On vérifie que le
    ## champ "géolocalisé" sur la fiche détaille de l'établissement a bien été mis à jour.
    ## L'action du portlet "géolocaliser" ne doit pas être présente.
    ##

    &{etab04} =  Create Dictionary
    ...  libelle=Boutique OPEN
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=9797
    ...  adresse_cp=13012
    ...  adresse_ville=MARSEILLE

    # Découpage de l'ajout de l'établissement pour vérifier les messages de géolocalisation
    Go To  ${PROJECT_URL}scr/form.php?obj=etablissement_tous&action=0
    Saisir les valeurs dans le formulaire de l'établissement  ${etab04}
    # XXX
    Click Element  css=legend.collapsed
    Input HTML  acc_consignes_om_html  Consigne
    Input HTML  acc_descriptif_om_html  Description
    Input HTML  si_consignes_om_html  Consigne
    Input HTML  si_descriptif_om_html  Description
    Select From List By Label  css=#si_autorite_competente_visite  Commission communale de sécurité
    Select From List By Label  css=#si_autorite_competente_plan  Commission communale de sécurité
    Select From List By Label  css=#si_type_alarme  2a
    Select From List By Label  css=#si_type_ssi  A
    Click On Submit Button
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  L'établissement est déjà géolocalisé.

    # On clique sur le bouton retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On se rend sur la fiche de l'établissement
    Input Text  css=#libelle  ${etab04.libelle}
    Click On Search Button
    Click On Link  ${etab04.libelle}

    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser
    Portlet Action Should Not Be In Form  etablissement_tous  geocoder

    # Récupération du code de l'établissement
    ${etab04_code} =  Get Text  css=#code
    ${etab04_titre} =  Set Variable  ${etab04_code} - ${etab04.libelle}
    ${etab04_libelle} =  Set Variable  ${etab04.libelle}

    Set Suite Variable  ${etab04}
    Set Suite Variable  ${etab04_code}
    Set Suite Variable  ${etab04_titre}
    Set Suite Variable  ${etab04_libelle}

    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig


Création de dossiers de coordination avec géocodage et test des liens de dessin d'emprise manuel

    [Documentation]  Teste les fonctionnalités de géocodage d'un dossier de coordination.
    ...  Les dossiers créés sont utilisés dans d'autres test suites.

    ##
    ## Cas d'utilisation n°1 :
    ##
    ## Création d'un DC sans références cadastrales et adresse, mais avec un
    ## dossier ADS très précis pour le connecteur de test.
    ## Lors de la création, la tentative de géocodage échoue, on clique alors sur le lien
    ## de redirection pour dessiner manuellement l'emprise sur le SIG. On relance
    ## alors l'action de géolocalisation depuis la fiche détaillée du DC, qui
    ## doit fonctionner. Le champ "Géolocalisé" doit être mis à jour.
    ##

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig

    Depuis la page d'accueil  cadre-si  cadre-si

    &{dc02} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Dossier de coordination lambda
    ...  etablissement=${etab02_titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  dossier_instruction_ads=PC0130551609998P0

    Go To  ${PROJECT_URL}scr/form.php?obj=dossier_coordination_nouveau&action=0
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc02}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Le dossier de coordination n'a pas pu être créé automatiquement sur le SIG.
    Valid Message Should Contain  Cliquez ici pour le dessiner.
    #
    Click Element  link_dessin_emprise_sig
    @{URL} =  Create List  http://localhost/erp_dc/create/?id=AT-PLAN
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  ${URL}  false

    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On récupère le libelle du dossier de coordination
    ${dc02_libelle} =  Get Text  css=div.form-content span#libelle
    ${dc02} =  Get Mandatory Value  css=#fieldset-form-dossier_coordination-informations-generales #dossier_coordination
    Set Suite Variable  ${dc02}
    Set Suite Variable  ${dc02_libelle}

    Element Should Contain  geolocalise  Non
    Portlet Action Should Be In Form  dossier_coordination  geocoder
    Click Element  action-form-dossier_coordination-geocoder
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Le dossier de coordination a été géolocalisé avec une précision de 10m.
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser

    ##
    ## Cas d'utilisation n°2 :
    ##
    ## Création d'un DC avec références cadastrales et numéro de dossier ADS.
    ## Lors de la création, la tentative de géocodage doit fonctionner. On vérifie que le
    ## champ "géolocalisé" sur la fiche détaille du dossier a bien été mis à jour.
    ## L'action du portlet "géolocaliser" ne doit pas être présente.
    ##

    @{ref_cad} =  Create List  806  AB  0025
    &{dc03} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination lambda
    ...  etablissement=${etab03_titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  terrain_references_cadastrales=${ref_cad}
    ...  dossier_instruction_ads=PC0130551600001P0

    Go To  ${PROJECT_URL}scr/form.php?obj=dossier_coordination_nouveau&action=0
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc03}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Le dossier de coordination a été géolocalisé avec une précision de 0m.

    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On récupère le libelle du dossier de coordination
    ${dc03_libelle} =  Get Text  css=div.form-content span#libelle
    ${dc03} =  Get Mandatory Value  css=#fieldset-form-dossier_coordination-informations-generales #dossier_coordination
    Set Suite Variable  ${dc03}
    Set Suite Variable  ${dc03_libelle}

    Depuis le contexte du dossier de coordination  ${dc03_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser
    Portlet Action Should Not Be In Form  dossier_coordination  geocoder

    ##
    ## Cas d'utilisation n°3 :
    ##
    ## Création d'un dossier de coordination déjà géolocalisé dans le SIG
    ## (dossier ADS PC0130551609999P0 pour le connecteur de test).
    ## Lors de la création, la tentative de géocodage doit échouer mais retourner
    ## un message précisant que ce DC est déjà géocodé. On vérifie que le
    ## champ "géolocalisé" sur la fiche détaille du dossier a bien été mis à jour.
    ## L'action du portlet "géolocaliser" ne doit pas être présente.
    ##

    &{dc04} =  Create Dictionary
    ...  dossier_coordination_type=Permis de construire
    ...  description=Dossier de coordination lambda
    ...  etablissement=${etab03_titre}
    ...  a_qualifier=false
    ...  date_butoir=${DATE_FORMAT_DD/MM/YYYY}
    ...  dossier_instruction_ads=PC0130551609999P0

    Go To  ${PROJECT_URL}scr/form.php?obj=dossier_coordination_nouveau&action=0
    Saisir les valeurs dans le formulaire du dossier de coordination  ${dc04}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    Valid Message Should Contain  Le dossier de coordination est déjà géolocalisé.

    # On clique sur le bonton de retour pour afficher le nouvel enregistrement
    Click On Back Button
    # On récupère le libelle du dossier de coordination
    ${dc04_libelle} =  Get Text  css=div.form-content span#libelle
    ${dc04} =  Get Mandatory Value  css=#fieldset-form-dossier_coordination-informations-generales #dossier_coordination
    Set Suite Variable  ${dc04}
    Set Suite Variable  ${dc04_libelle}

    Depuis le contexte du dossier de coordination  ${dc04_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  geolocalise_localiser_sig_externe  Localiser
    Portlet Action Should Not Be In Form  dossier_coordination  geocoder

    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig


Liens de redirection vers un SIG externe

    [Documentation]  Vérifie que les boutons "Localiser" renvoient
    ...  vers le bon URL du SIG.
    ...  Le bouton est existe dans plusieurs contextes :
    ...    - dans le listing des dossiers de coordination, des dossiers d'instruction, des
    ...    établissements, des UA. On teste avec un élémént non géolocalisé, puis avec un
    ...    qui est géolocalisé.
    ...    - sur les fiches détaillées des 4 éléments, dans les champs référence cad.,
    ...    établissement lié, dossier de coordination lié, le champ "géolocalisé". On
    ...    teste avec des éléments non géolocalisé puis avec des éléments géolocalisés.
    ...    - dans les listings des DC et établissements, après une recherche avancée, le
    ...    lien doit rediriger vers les résultats de la recherche avancée.

    ##
    ## Test des liens de redirection d'un DC, avec un établissement lié non localisé
    ##

    # On récupère le titre de la page http://localhost/
    Go To  http://localhost
    ${titles} =  Get Window Titles
    ${title} =  Get From List  ${titles}  0

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig

    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le listing des dossiers de coordination
    # On fait une recherche sur le libellé du DC
    Input Text  css=div#adv-search-adv-fields input#libelle  ${dc01_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-dossier_coordination-left-localiser-sig-externe-${dc01}
    Click Element  action-tab-dossier_coordination-left-localiser-sig-externe-${dc01}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    # On se rend sur le dossier de coordination
    Click On Link  ${dc01_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  css=#fieldset-form-dossier_coordination-etablissement  Localiser
    Element Should Not Contain  css=#fieldset-form-dossier_coordination-urbanisme  Localiser

    ##
    ## Test des liens de redirection d'un DC, avec un établissement lié non localisé et
    ## test de l'absence du lien de redirection vers l'établissement lié non localisé
    ##

    &{dc_visit_1} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab01_titre}
    ...  dossier_instruction_ads=PC0130551609998P0
    ${dc_visit_1_libelle} =  Ajouter le dossier de coordination  ${dc_visit_1}
    Click Link  ${dc_visit_1_libelle}-SI
    ${di_visit_1_id} =  Get Mandatory Value  css=#dossier_instruction
    Element Should Not Contain  css=.field.field-type-etablissement .form-content  Localiser
    ${di_visit_1_libelle} =  Get Text  css=#libelle
    Go To Tab  dossier_instruction_tous_visites
    Input Text  css=div#adv-search-adv-fields input#dossier  ${di_visit_1_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-dossier_instruction_tous_visites-left-localiser-sig-externe-${di_visit_1_id}
    Click Element  action-tab-dossier_instruction_tous_visites-left-localiser-sig-externe-${di_visit_1_id}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    ##
    ## Test des liens de redirection d'un DI, avec DC et établissement lié non localisés
    ##

    # On se rend sur le DI "SI" du DC n°1
    Depuis le contexte du dossier de coordination  ${dc01_libelle}
    Click Link    ${dc01_libelle}-SI

    ${di01_SI} =  Get Mandatory Value  css=#dossier_instruction
    # L'établissement et le dossier de coordination liés ne sont pas localisés, les liens
    # de redirection ne doivent pas être affichés
    Element Should Not Contain  css=.field.field-type-dossier_coordination_link .form-content  Localiser
    Element Should Not Contain  css=.field.field-type-etablissement .form-content  Localiser

    Go To Tab  dossier_instruction
    Input Text  css=div#adv-search-adv-fields input#dossier  ${dc01_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-dossier_instruction-left-localiser-sig-externe-${di01_SI}
    Click Element  action-tab-dossier_instruction-left-localiser-sig-externe-${di01_SI}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    ##
    ## Test du lien de redirection d'un établissement non localisé
    ##

    Go To Tab  etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab01_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-etablissement_tous-left-localiser-sig-externe-${etab01_id}
    Click Element  action-tab-etablissement_tous-left-localiser-sig-externe-${etab01_id}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    ##
    ## Test des liens de redirection d'une UA, avec un établissement lié non localisé
    ##

    Go To Tab  etablissement_unite
    Input Text  css=div#adv-search-adv-fields input#etablissement  ${etab01_libelle}
    Click On Search Button

    Click Link  ${etab01_libelle}
    ${ua1} =  Get Mandatory Value  css=#etablissement_unite
    # La fieldset d'établissement lié ne doit pas contenir la mappemonde car il n'est pas géolocalisé
    Element Should Not Contain  css=.field.field-type-etablissement .form-content  Localiser

    Click On Back Button
    Click Element  action-tab-etablissement_unite-left-localiser-sig-externe-${ua1}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    ##
    ## Test des liens de redirection d'un DC, avec un établissement lié localisé, et qui a
    ## des références cadastrales
    ##

    Depuis le listing des dossiers de coordination
    # On fait une recherche sur le libellé du DC
    Input Text  css=div#adv-search-adv-fields input#libelle  ${dc03_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-dossier_coordination-left-localiser-sig-externe-${dc03}
    Click Element  action-tab-dossier_coordination-left-localiser-sig-externe-${dc03}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_dc?id=${dc03_libelle}

    # On se rend sur le dossier de coordination
    Click Link  ${dc03_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  css=#fieldset-form-dossier_coordination-etablissement  Localiser
    Click Element  etablissement_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab03_code}

    Click Element  geolocalise_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_dc?id=${dc03_libelle}

    Click Element  references_cadastrales_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/parcelles/201806AB0025

    ##
    ## Test des liens de redirection d'un DI, avec un DC et établissement liés localisés
    ##

    Click Link  ${dc03_libelle}-SI
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  dossier_coordination_localiser_sig_externe  Localiser
    ${di03_SI} =  Get Mandatory Value  css=#dossier_instruction
    ${di03_SI_libelle} =  Get Text  css=#libelle

    Click Element  etablissement_coordonnees_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab03_code}

    Click Element  dossier_coordination_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_dc?id=${dc03_libelle}

    Go To Tab  dossier_instruction
    Input Text  css=div#adv-search-adv-fields input#dossier    ${di03_SI_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-dossier_instruction-left-localiser-sig-externe-${di03_SI}
    Click Element  action-tab-dossier_instruction-left-localiser-sig-externe-${di03_SI}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_dc?id=${dc03_libelle}

    ##
    ## Test du lien de redirection d'un établissement localisé avec des parcelles
    ##

    Go To Tab  etablissement_tous
    Input Text  css=div#adv-search-adv-fields input#libelle    ${etab02_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-etablissement_tous-left-localiser-sig-externe-${etab02_id}
    Click Element  action-tab-etablissement_tous-left-localiser-sig-externe-${etab02_id}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab02_code}

    Input Text  css=div#adv-search-adv-fields input#libelle    ${etab03_libelle}
    Click On Search Button
    Click Link  ${etab03_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  references_cadastrales_localiser_sig_externe
    Click Element  references_cadastrales_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/parcelles/201806AB0025

    ##
    ## Test des liens de redirection d'une UA, avec établissement lié localisé
    ##

    Go To Tab  etablissement_unite
    Input Text  css=div#adv-search-adv-fields input#etablissement  ${etab02_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${etab02_libelle}

    Click Link  ${etab02_libelle}
    ${ua2} =  Get Mandatory Value  css=#etablissement_unite
    Click Element  etablissement_localiser_sig_externe
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab02_code}

    Click On Back Button
    Click Element  action-tab-etablissement_unite-left-localiser-sig-externe-${ua2}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab02_code}

    ##
    ## Test du lien de redirection du listing des établissements après une recherche avancée
    ##

    Go To Tab  etablissement_tous
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    @{URL} =  Create List  http://localhost/consult/erp_etab?id=  ${etab_1_code}  ${etab_2_code}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  ${URL}  false

    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab01_libelle}
    Click On Search Button
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab01_code}

    ##
    ## Test du lien de redirection du listing des Référentiels ERP avant et après une recherche avancée
    ## pour un établissement et pour une sélection
    ##

    # Etablissement non géolocalisé
    Go To Tab  etablissement_referentiel_erp
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_1_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-etablissement_referentiel_erp-left-localiser-sig-externe-${etab_1_id}
    Click Element  action-tab-etablissement_referentiel_erp-left-localiser-sig-externe-${etab_1_id}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    # Etablissement géolocalisé
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_2_libelle}
    Click On Search Button
    Click Link  ${etab_2_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  action-form-etablissement_referentiel_erp-geocoder
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  L'établissement a été géolocalisé avec une précision de 15m.
    Go To Tab  etablissement_referentiel_erp
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_2_libelle}
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  action-tab-etablissement_referentiel_erp-left-localiser-sig-externe-${etab_2_id}
    Click Element  action-tab-etablissement_referentiel_erp-left-localiser-sig-externe-${etab_2_id}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab_2_code}

    # Test de la redirection vers une sélection sans recherche
    Go To Tab  etablissement_referentiel_erp
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    # Test de la redirection vers une sélection avec une recherche vide
    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25

    # Décomposition de la vérification de l'URL de la nouvelle fenêtre, afin de vérifier
    # que l'URL contient seulement les établissements ERP référentiels
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Select Window  404 Not Found
    Location Should Contain  http://localhost/consult/erp_etab?id=
    Location Should Contain  ${etab_1_code}
    Location Should Contain  ${etab_2_code}
    Run Keyword And Expect Error  *  Location Should Contain  ${etab01_code}
    Close Window
    Select Window

    # Test de la redirection vers une sélection d'un élément
    Input Text  css=div#adv-search-adv-fields input#libelle  ${etab_1_libelle}
    Click On Search Button
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_etab?id=${etab_1_code}

    ##
    ## Test du lien de redirection du listing des dossiers de coordination après une recherche avancée
    ##

    Depuis le listing des dossiers de coordination
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  ${title}  http://localhost/

    Click On Search Button
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  css=.om-icon.om-icon-25.geoaria-25
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    @{URL} =  Create List  http://localhost/consult/erp_dc?id=  ${dc_1_libelle}  ${dc_2_libelle}
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  ${URL}  false

    Input Text  css=div#adv-search-adv-fields input#libelle  ${dc01_libelle}
    Click On Search Button
    Click Element  css=.om-icon.om-icon-25.geoaria-25
    Sélectionner la fenêtre et vérifier l'URL puis fermer la fenêtre  404 Not Found  http://localhost/consult/erp_dc?id=${dc01_libelle}

    Depuis la page d'accueil  admin  admin
    Désactiver le plugin geoaria_tests et l'option sig

Géolocalisation de tous les établissements et dossiers de coordination
    [Documentation]  Géocode tous les objets non localisés

    Depuis la page d'accueil  admin  admin
    # Le menu doit être absent sans option SIG activée
    Go To  ${PROJECT_URL}app/settings.php
    Page Should Not Contain  géolocalisation
    Activer le plugin geoaria_tests et l'option sig
    Go To  ${PROJECT_URL}app/settings.php
    Page Should Contain  géolocalisation
    # Récupération des éléments non localisés avant traitement
    Depuis le listing de tous les établissements
    Select From List By Label  geolocalise  Non
    Click On Search Button
    ${nb_etablissements_before} =  Get Total Results
    Depuis le listing des dossiers de coordination
    Select From List By Label  geolocalise  Non
    Click On Search Button
    ${nb_dc_before} =  Get Total Results
    # 1er traitement : récupération du nombre d'éléments localisés affichés par le traitement
    Tout géolocaliser
    ${nb_etablissements_traites} =  Get Text  tot_etab_ok
    ${nb_dc_traites} =  Get Text  tot_dc_ok
    # 2ème traitement : les éléments ne sont pas localisés deux fois d'affilée
    Tout géolocaliser
    ${nb_etablissements_traites_bis} =  Get Text  tot_etab_ok
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Should Be Equal As Integers  ${nb_etablissements_traites_bis}  0
    ${nb_dc_traites_bis} =  Get Text  tot_dc_ok
    Should Be Equal As Integers  ${nb_dc_traites_bis}  0
    # Récupération des éléments non localisés après traitement
    Depuis le listing de tous les établissements
    Select From List By Label  geolocalise  Non
    Click On Search Button
    ${nb_etablissements_after} =  Get Total Results
    Depuis le listing des dossiers de coordination
    Select From List By Label  geolocalise  Non
    Click On Search Button
    ${nb_dc_after} =  Get Total Results
    # Calcul du nombre d'éléments réellement localisés par le traitement
    ${nb_etablissements_localises} =  Evaluate  ${nb_etablissements_before}-${nb_etablissements_after}
    ${nb_dc_localises} =  Evaluate  ${nb_dc_before}-${nb_dc_after}
    # Comparaison des résultats
    Should Be Equal As Integers  ${nb_etablissements_traites}  ${nb_etablissements_localises}
    Should Be Equal As Integers  ${nb_dc_traites}  ${nb_dc_localises}


Récupération des propriétaires depuis un établissement/DC
    [Documentation]  Vérifie la récupération des propriétaires depuis un
    ...  établissement et depuis un dossier de coordination.

    Depuis la page d'accueil  admin  admin

    ##
    ## Option SIG non activée
    ##
    Depuis le contexte de l'établissement  ${etab_4_code}
    Page Should Not Contain  css=#references_cadastrales_get_plot_owner

    Activer le plugin geoaria_tests et l'option sig

    ##
    ## Cas n°1 : Récupère un propriétaire
    ##

    Depuis le contexte de l'établissement  ${etab_4_code}
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  806AB0025
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie que le propriétaire attendu est affiché
    Wait Until Element Is Visible  css=#fieldset-form-plot_owner_list
    Element Should Contain  css=#plot_owner_list  DUPONT
    Element Should Contain  css=#plot_owner_list  Claude
    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container


    ##
    ## Cas n°2 : Récupère plusieurs propriétaires
    ##

    # On modifie les références cadastrales de l'établissement
    @{ref_cad} =  Create List  806  AB  0026
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab_4_code}
    #
    Click On Back Button
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  806AB0026
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie que les propriétaires attendus sont affichés
    Wait Until Element Is Visible  css=#fieldset-form-plot_owner_list
    Element Should Contain  css=#plot_owner_list  Archambault
    Element Should Contain  css=#plot_owner_list  Sibyla
    Element Should Contain  css=#plot_owner_list  Fongemie
    Element Should Contain  css=#plot_owner_list  Oriel
    Element Should Contain  css=#plot_owner_list  Ruel
    Element Should Contain  css=#plot_owner_list  Bruno
    Element Should Contain  css=#plot_owner_list  Gladu
    Element Should Contain  css=#plot_owner_list  Kerman
    Element Should Contain  css=#plot_owner_list  Lajoie
    Element Should Contain  css=#plot_owner_list  Tanguy
    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container

    ##
    ## Cas n°3 : Récupère aucun propriétaires
    ##

    # On modifie les références cadastrales de l'établissement
    @{ref_cad} =  Create List  111  AA  1111
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab_4_code}
    #
    Click On Back Button
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  111AA1111
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie qu'il n'y a aucun propriétaires retournés
    Wait Until Page Contains  Aucun propriétaires
    Element Should Contain  css=#plot_owner_list  Aucun propriétaires
    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container

    ##
    ## Cas n°4 : Erreur HTTP catchée
    ##

    # On modifie les références cadastrales de l'établissement
    @{ref_cad} =  Create List  777  ZZ  7777
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab_4_code}
    #
    Click On Back Button
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  777ZZ7777
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie le message d'erreur openmairie
    Wait Until Element Is Visible  css=div.message.ui-state-error
    Error Message Should Be  Une erreur s'est produite lors de la récupération des propriétaires. Veuillez contacter votre administrateur.
    # On ferme l'overlay
    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    ##
    ## Cas n°5 : Erreur HTTP non catchée
    ##

    # On modifie les références cadastrales de l'établissement
    @{ref_cad} =  Create List  888  BB  8888
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab_4_code}
    #
    Click On Back Button
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  888BB8888
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie le message d'erreur (retourné depuis l'ajax)
    Wait Until Page Contains  Veuillez contacter votre administrateur.
    Element Should Contain  css=div#overlay-container  Une erreur s'est produite lors de la récupération des propriétaires. Veuillez contacter votre administrateur.
    # On ferme l'overlay
    Click Element  css=span.ui-icon-closethick
    Wait Until Element Is Not Visible  css=div#overlay-container

    ##
    ## DC
    ##

    # # On vérifie seulement le cas 1
    Depuis le contexte du dossier de coordination  ${dc_4_libelle}
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  806AB0025
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires
    # On vérifie que le propriétaire attendu est affiché
    Wait Until Element Is Visible  css=#fieldset-form-plot_owner_list
    Element Should Contain  css=#plot_owner_list  DUPONT
    Element Should Contain  css=#plot_owner_list  Claude
    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container

    # Remise dans l'état initial
    Désactiver le plugin geoaria_tests et l'option sig


Ajout d'un contact depuis l'interface de récupération des propriétaires
    [Documentation]  Ajoute un contact sur un établissement et sur un DC depuis
    ...  l'interface de récupération des propriétaire.

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig

    ##
    ## Établissement
    ##

    # On modifie les références cadastrales de l'établissement
    @{ref_cad} =  Create List  806  AB  0026
    &{values} =  Create Dictionary
    ...  terrain_references_cadastrales=${ref_cad}
    Modifier l'établissement  ${values}  ${etab_4_code}
    #
    Click On Back Button
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  806AB0026
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires

    # On clique sur le bouton d'ajout
    Wait Until Element Is Visible  css=#fieldset-form-plot_owner_list
    Click On Add Button JS
    # On vérifie que le formulaire d'ajout d'un contact est affiché
    Wait Until Element Is Visible  css=#sousform-contact
    # On vérifie que le bouton d'ajout est désactivé
    Form Field Attribute Should Be  plot_owner_add_btn_contact  disabled  true
    # On ajoute un contact
    Saisir le contact  null  null  null  null  DUPONT  Claude
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On clique sur le bouton retour qui devrait cacher le formulaire d'ajout
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=div#overlay-container a.retour
    Wait Until Element Is Not Visible  css=#sousform-contact
    # On vérifie que le bouton d'ajout est activé
    Form Field Attribute Should Be  plot_owner_add_btn_contact  disabled  ${None}

    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container

    # On vérifie que le contact apparaît dans la liste des contacts de
    # l'établissement
    Depuis l'onglet contact de l'établissement  ${etab_4_code}
    Element Should Contain  css=table.tab-tab  DUPONT Claude

    ##
    ## DC
    ##

    Depuis le contexte du dossier de coordination  ${dc_4_libelle}
    # On vérifie les références cadastrales
    Form Static Value Should Be  css=span.reference-cadastrale-0  806AB0025
    # On clique sur le bouton de récupération des propriétaires
    Click Element  css=#references_cadastrales_get_plot_owner
    # On vérifie le titre de l'overlay
    Wait Until Element Is Visible  css=#overlay-container
    Element Text Should Be  css=#ui-dialog-title-overlay-container  Liste des propriétaires

    # On clique sur le bouton d'ajout
    Wait Until Element Is Visible  css=#fieldset-form-plot_owner_list
    Click On Add Button JS
    # On vérifie que le formulaire d'ajout d'un contact est affiché
    Wait Until Element Is Visible  css=#sousform-contact_contexte_dossier_coordination
    # On vérifie que le bouton d'ajout est désactivé
    Form Field Attribute Should Be  plot_owner_add_btn_contact  disabled  true
    # On ajoute un contact
    Saisir le contact  null  null  null  null  DUPONT  Claude
    Click On Submit Button In Subform
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Be  Vos modifications ont bien été enregistrées.

    # On clique sur le bouton retour qui devrait cacher le formulaire d'ajout
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element    css=div#overlay-container a.retour
    Wait Until Element Is Not Visible  css=#sousform-contact_contexte_dossier_coordination
    # On vérifie que le bouton d'ajout est activé
    Form Field Attribute Should Be  plot_owner_add_btn_contact  disabled  ${None}

    # On ferme l'overlay
    Click Element  css=input#plot_owner_close_btn
    Wait Until Element Is Not Visible  css=div#overlay-container

    # On vérifie que le contact apparaît dans la liste des contacts du DC
    Depuis l'onglet contact de l'établissement  ${etab_4_code}
    Element Should Contain  css=table.tab-tab  DUPONT Claude

    # Remise dans l'état initial
    Désactiver le plugin geoaria_tests et l'option sig


Liste des établissements proches lors de la qualification d'un DC
    [Documentation]  Teste la fonctionnalité de listage des établissements proches des
    ...  références cadastrales, sur un dossier de coordination.
    ...  On teste plusieurs cas :
    ...  - il n'y a pas de référence cadastrale saisie
    ...  - il n'y a pas d'établissement proche
    ...  - il y a des établissements proches → sélection et test des listes déroulantes

    Depuis la page d'accueil  cadre-si  cadre-si
    Depuis le formulaire de modification du dossier de coordination    ${dc_1_libelle}
    Page Should Not Contain Element  autocomplete-etablissement_tous-link-selection

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig

    # # On importe les établissements
    # Depuis l'import    import_etablissement
    # Add File    fic1    import_etablissements_sig.csv
    # Click On Submit Button In Import CSV
    # Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    7 ligne(s) importée(s)

    #
    Depuis la page d'accueil  cadre-si  cadre-si
    #
    Depuis le formulaire de modification du dossier de coordination    ${dc_1_libelle}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  fieldset-form-dossier_coordination-qualification  Liste d'établissements proches
    Click Element  autocomplete-etablissement_tous-link-selection
    ${alert_message} =  Get Alert Message
    Should Be Equal As Strings  ${alert_message}  Aucune référence cadastrale fournie

    @{ref_cad} =  Create List  77  G  77
    Saisir les références cadastrales  ${ref_cad}

    Click Element  autocomplete-etablissement_tous-link-selection
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  overlay-etablissements-proches  Il n'y a aucun établissement proche des références cadastrales saisies.
    Form Field Attribute Should Be  limite  disabled  true
    Form Field Attribute Should Be  nature  disabled  true
    Click Element  close-button

    @{ref_cad} =  Create List  806  AB  25
    Saisir les références cadastrales  ${ref_cad}

    Click Element  autocomplete-etablissement_tous-link-selection
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain Element  limite
    ${limite_selectionnee} =  Get Selected List Label  limite
    ${nature_selectionnee} =  Get Selected List Label  nature
    Should Be Equal As Strings  10  ${limite_selectionnee}
    Should Be Equal As Strings  Choisir nature  ${nature_selectionnee}

    @{overlay_should_contain} =  Create List
    ...  T2398 - CRECHE LOUIS ARAGON 0m
    ...  T2399 - CRECHE ALBERT CAMUS B 7m
    ...  T2397 - MONOPRIX DOIZE 9m
    ...  T2402 - CRECHE LES MAGNANARELLES 12m
    ...  T2444 - CRECHE LE PETIT BOIS 19m
    ...  T2400 - CRECHE GEORGES BRASSENS 50m
    Element Should Contain From List  overlay-etablissements-proches  ${overlay_should_contain}

    # Établissement retourné par le SIG mais n'existant pas dans la BDD openARIA
    Element Should Not Contain  overlay-etablissements-proches  T7004

    # 11ème établissement retourné : n'apparaît que si limite > 10
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Not Contain  overlay-etablissements-proches  T2401 - CENTRE COMMERCIAL L'ODYSSEE
    Element Should Not Contain  overlay-etablissements-proches  100m
    Element Should Contain From List  overlay-etablissements-proches  ${overlay_should_contain}
    Select From List By Label  limite  20
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Contain  overlay-etablissements-proches  T2401 - CENTRE COMMERCIAL L'ODYSSEE
    Element Should Contain From List  overlay-etablissements-proches  ${overlay_should_contain}

    Select From List By Label  nature  ERP Référentiel
    #
    @{overlay_should_contain} =  Create List
    ...  T2397 - MONOPRIX DOIZE 9m
    ...  T2401 - CENTRE COMMERCIAL L'ODYSSEE 100m

    Element Should Contain From List  overlay-etablissements-proches  ${overlay_should_contain}
    Element Should Not Contain  form-container  ERP potentiel

    Click Element  css=.tab-data.odd:first-child .col-1
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Form Value Should Be  etablissement  2397
    Form Value Should Be  autocomplete-etablissement_tous-search  T2397 - MONOPRIX DOIZE
    ${categorie_selectionnee} =  Get Selected List Label  etablissement_categorie
    Should Be Equal As Strings  ${categorie_selectionnee}  [2] de 701 à 1500 personnes

    # On vérifie que le lien avec l'établissement a bien fonctionné
    Click On Submit Button
    Click On Back Button
    Form Value Should Be  etablissement  2397

    Depuis la page d'accueil  admin  admin
    # Remise dans l'état initial
    Désactiver le plugin geoaria_tests et l'option sig


Désactivation de l'option SIG
    [Documentation]  Nécessaire en cas de fail sur un des précédents TestCase
    ...  pour être sûr que l'option est bien désactivée

    Depuis la page d'accueil  admin  admin
    Activer le plugin geoaria_tests et l'option sig
    Désactiver le plugin geoaria_tests et l'option sig

    # On remet le jeu de test dans l'état initial
    &{voie} =  Create Dictionary
    ...  libelle=Rue de Rome
    ...  id_voie_ref=${EMPTY}

    Modifier voie  Rue de Rome  ${voie}