*** Settings ***
Documentation  Objet 'reunion_categorie'.


*** Keywords ***
Depuis le listing des catégories de réunion
    [Documentation]  Accède directement via URL au listing des catégories de
    ...  réunion.
    Go To Tab  reunion_categorie


Depuis le formulaire d'ajout de catégorie de réunion
    [Documentation]  Accède directement via URL au formulaire d'ajout d'une
    ...  catégorie de réunion.
    Go To  ${PROJECT_URL}scr/form.php?obj=reunion_categorie&action=0


Depuis le contexte de la catégorie de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le listing des catégories de réunion
    Use Simple Search  code  ${code}
    Click On Link  ${code}


Depuis le formulaire de modification de la catégorie de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le contexte de la catégorie de réunion  ${code}
    Click On Form Portlet Action  reunion_categorie  modifier


Modifier la catégorie de réunion
    [Documentation]
    [Arguments]  ${code}  ${values}
    Depuis le formulaire de modification de la catégorie de réunion  ${code}
    Saisir les valeurs dans le formulaire de catégorie de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Ajouter la catégorie de réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    Depuis le formulaire d'ajout de catégorie de réunion
    Saisir les valeurs dans le formulaire de catégorie de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire de catégorie de réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire


