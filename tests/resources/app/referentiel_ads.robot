*** Settings ***
Documentation   Interface avec le référentiel ADS.

*** Keywords ***
Activer l'option référentiel ADS
    [Documentation]
    #Copy File  ${EXECDIR}${/}binary_files${/}dyn${/}services.inc.php  ${EXECDIR}${/}..${/}dyn${/}services.inc.php
    Ajouter le paramètre depuis le menu  option_referentiel_ads  true  null


Désactiver l'option référentiel ADS
    [Documentation]
    #Remove File  ${EXECDIR}${/}..${/}dyn${/}services.inc.php
    Supprimer le paramètre  option_referentiel_ads  true


