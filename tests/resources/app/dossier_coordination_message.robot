*** Settings ***
Documentation     Actions spécifiques aux messages.

*** Keywords ***
Depuis le listing de tous les messages
    [Documentation]  ...
    Go To  ${PROJECT_URL}scr/tab.php?obj=dossier_coordination_message_tous

Depuis le listing de mes messages non lus
    [Documentation]  ...
    Go To  ${PROJECT_URL}scr/tab.php?obj=dossier_coordination_message_mes_non_lu


Depuis le listing des messages dans le contexte du dossier de coordination
    [Documentation]  ...
    [Arguments]  ${dc_libelle}
    Depuis le contexte du dossier de coordination  ${dc_libelle}
    On clique sur l'onglet  dossier_coordination_message_contexte_dc  Messages


Depuis le listing des messages dans le contexte du dossier d'instruction
    [Documentation]  ...
    [Arguments]  ${di_libelle}
    Depuis le contexte du dossier d'instruction  ${di_libelle}
    On clique sur l'onglet  dossier_coordination_message_contexte_di  Messages


