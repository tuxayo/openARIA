*** Settings ***
Documentation     Actions spécifiques aux contacts.

*** Keywords ***
Depuis le listing des contacts institutionnels

    [Documentation]    Permet d'accéder au listing des contacts institutionnel.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    contact
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Générés > Contact Institutionnel


Depuis le contexte d'un contact institutionnel
    [Documentation]
    [Arguments]    ${nom}     ${institutionnel}
    Depuis le listing des contacts institutionnels
    # On recherche le contact
    Use Simple Search    nom    ${nom}
    # On clique sur la réunion
    Click On Link    ${institutionnel}
    # On vérifie que le page contient bien le contact
    Page Title Should Contain    ${institutionnel}


Saisir le contact

    [Documentation]   Permet de saisir le formulaire du contact institutionnel.

    [Arguments]    ${contact_type}=null    ${service}=null    ${qualite}=null    ${civilite}=null    ${nom}=null    ${prenom}=null    ${titre}=null    ${adresse_numero}=null    ${adresse_numero2}=null    ${adresse_voie}=null    ${adresse_complement}=null    ${lieu_dit}=null    ${boite_postale}=null    ${adresse_cp}=null    ${adresse_ville}=null    ${cedex}=null    ${pays}=null    ${telephone}=null    ${mobile}=null    ${fax}=null    ${courriel}=null    ${reception_programmation}=null    ${reception_commission}=null    ${reception_convocation}=null

    #
    Run Keyword If    '${contact_type}' != 'null'    Select From List By Label    css=#contact_type    ${contact_type}
    #
    Run Keyword If    '${service}' != 'null'    Select From List By Label    css=#service    ${service}
    #
    Run Keyword If    '${qualite}' != 'null'    Select From List By Label    css=#qualite    ${qualite}
    #
    Run Keyword If    '${civilite}' != 'null'    Select From List By Label    css=#civilite    ${civilite}
    #
    Run Keyword If    '${nom}' != 'null'    Input Text    css=#nom    ${nom}
    #
    Run Keyword If    '${prenom}' != 'null'    Input Text    css=#prenom    ${prenom}
    #
    Run Keyword If    '${titre}' != 'null'    Input Text    css=#titre    ${titre}
    #
    Run Keyword If    '${adresse_numero}' != 'null'    Input Text    css=#adresse_numero    ${adresse_numero}
    #
    Run Keyword If    '${adresse_numero2}' != 'null'    Input Text    css=#adresse_numero2    ${adresse_numero2}
    #
    Run Keyword If    '${adresse_voie}' != 'null'    Input Text    css=#adresse_voie    ${adresse_voie}
    #
    Run Keyword If    '${adresse_complement}' != 'null'    Input Text    css=#adresse_complement    ${adresse_complement}
    #
    Run Keyword If    '${lieu_dit}' != 'null'    Input Text    css=#lieu_dit    ${lieu_dit}
    #
    Run Keyword If    '${boite_postale}' != 'null'    Input Text    css=#boite_postale    ${boite_postale}
    #
    Run Keyword If    '${adresse_cp}' != 'null'    Input Text    css=#adresse_cp    ${adresse_cp}
    #
    Run Keyword If    '${adresse_ville}' != 'null'    Input Text    css=#adresse_ville    ${adresse_ville}
    #
    Run Keyword If    '${cedex}' != 'null'    Input Text    css=#cedex    ${cedex}
    #
    Run Keyword If    '${pays}' != 'null'    Input Text    css=#pays    ${pays}
    #
    Run Keyword If    '${telephone}' != 'null'    Input Text    css=#telephone    ${telephone}
    #
    Run Keyword If    '${mobile}' != 'null'    Input Text    css=#mobile    ${mobile}
    #
    Run Keyword If    '${fax}' != 'null'    Input Text    css=#fax    ${fax}
    #
    Run Keyword If    '${courriel}' != 'null'    Input Text    css=#courriel    ${courriel}
    #
    Run keyword If    '${reception_programmation}' == 'true'    Select Checkbox    css=#reception_programmation
    Run keyword If    '${reception_programmation}' == 'false'    Unselect Checkbox    css=#reception_programmation
    #
    Run keyword If    '${reception_commission}' == 'true'    Select Checkbox    css=#reception_commission
    Run keyword If    '${reception_commission}' == 'false'    Unselect Checkbox    css=#reception_commission


Ajouter le contact institutionnel depuis le menu

    [Documentation]    Permet d'ajouter le contact institutionnel depuis le
    ...    menu.

    [Arguments]    ${contact_type}=null    ${service}=null    ${qualite}=null    ${civilite}=null    ${nom}=null    ${prenom}=null    ${titre}=null    ${adresse_numero}=null    ${adresse_numero2}=null    ${adresse_voie}=null    ${adresse_complement}=null    ${lieu_dit}=null    ${boite_postale}=null    ${adresse_cp}=null    ${adresse_ville}=null    ${cedex}=null    ${pays}=null    ${telephone}=null    ${mobile}=null    ${fax}=null    ${courriel}=null    ${reception_programmation}=null    ${reception_commission}=null    ${reception_convocation}=null

    Depuis le listing des contacts institutionnels
    #
    Click On Add Button
    #
    Saisir le contact    ${contact_type}    ${service}    ${qualite}    ${civilite}    ${nom}    ${prenom}    ${titre}    ${adresse_numero}    ${adresse_numero2}    ${adresse_voie}    ${adresse_complement}    ${lieu_dit}    ${boite_postale}    ${adresse_cp}    ${adresse_ville}    ${cedex}    ${pays}    ${telephone}    ${mobile}    ${fax}    ${courriel}    ${reception_programmation}    ${reception_commission}    ${reception_convocation}
    #
    Click On Submit Button
    #
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Ajouter le contact depuis l'établissement

    [Documentation]    Permet d'ajouter le contact sur un établissement.

    [Arguments]    ${code}=null    ${libelle}=null    ${contact_type}=null    ${service}=null    ${qualite}=null    ${civilite}=null    ${nom}=null    ${prenom}=null    ${titre}=null    ${adresse_numero}=null    ${adresse_numero2}=null    ${adresse_voie}=null    ${adresse_complement}=null    ${lieu_dit}=null    ${boite_postale}=null    ${adresse_cp}=null    ${adresse_ville}=null    ${cedex}=null    ${pays}=null    ${telephone}=null    ${mobile}=null    ${fax}=null    ${courriel}=null    ${reception_programmation}=null    ${reception_commission}=null    ${reception_convocation}=null

    #
    Depuis l'onglet contact de l'établissement    ${code}    ${libelle}
    # On clique sur le btn ajouter
    Click On Add Button
    #
    Saisir le contact    ${contact_type}    ${service}    ${qualite}    ${civilite}    ${nom}    ${prenom}    ${titre}    ${adresse_numero}    ${adresse_numero2}    ${adresse_voie}    ${adresse_complement}    ${lieu_dit}    ${boite_postale}    ${adresse_cp}    ${adresse_ville}    ${cedex}    ${pays}    ${telephone}    ${mobile}    ${fax}    ${courriel}    ${reception_programmation}    ${reception_commission}    ${reception_convocation}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Ajouter le contact depuis le DC
    [Documentation]  Permet d'ajouter le contact sur un DC.
    [Arguments]  ${dossier_coordination}  ${values}
    Depuis l'onglet "Contacts" du DC  ${dossier_coordination}
    Click On Add Button
    Saisir contact  ${values}
    Click On Submit Button In Subform
    Valid Message Should Be In Subform  Vos modifications ont bien été enregistrées.


Saisir contact
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contact_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "civilite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "titre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "mobile" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fax" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adresse_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_numero2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_voie" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "boite_postale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "pays" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "qualite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "denomination" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "raison_sociale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "categorie_juridique" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reception_convocation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reception_programmation" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "reception_commission" existe dans "${values}" on execute "Set Checkbox" dans le formulaire

