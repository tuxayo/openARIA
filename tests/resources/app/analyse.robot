*** Settings ***
Documentation     Actions spécifiques aux analyses

*** Keywords ***
Modifier données techniques depuis analyse SI
    [Arguments]    ${DI_SI}    ${alarme}=null    ${ssi}=null    ${conformite}=null    ${alimentation}=null    ${service_securite}=null    ${personnel_jour}=null    ${personnel_nuit}=null    ${effectif_public}=null    ${effectif_personnel}=null

    Accéder à l'analyse du dossier d'instruction    ${DI_SI}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Éditer bloc analyse  donnees_techniques
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Saisir données techniques SI    ${alarme}    ${ssi}    ${conformite}    ${alimentation}    ${service_securite}    ${personnel_jour}    ${personnel_nuit}    ${effectif_public}    ${effectif_personnel}
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element    css=#sousform-analyses a.retour

Modifier données techniques depuis analyse ACC
    [Arguments]    ${DI_ACC}    ${auditif}=null    ${mental}=null    ${physique}=null    ${visuel}=null    ${stationnement}=null    ${ascenseur}=null    ${elevateur}=null    ${boucle}=null    ${sanitaire}=null    ${places_assises}=null    ${chambres}=null    ${douche}=null    ${derogation}=null

    Accéder à l'analyse du dossier d'instruction    ${DI_ACC}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Éditer bloc analyse  donnees_techniques
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Saisir données techniques ACC    ${auditif}    ${mental}    ${physique}    ${visuel}    ${stationnement}    ${ascenseur}    ${elevateur}    ${boucle}    ${sanitaire}    ${places_assises}    ${chambres}    ${douche}    ${derogation}
    Submit Form    css=#form_analyses_modifier
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element    css=#sousform-analyses a.retour



Saisir données techniques SI
    [Arguments]    ${alarme}=null    ${ssi}=null    ${conformite}=null    ${alimentation}=null    ${service_securite}=null    ${personnel_jour}=null    ${personnel_nuit}=null    ${effectif_public}=null    ${effectif_personnel}=null

    # type d'alarme
    Run Keyword If    '${alarme}' != 'null'    Select From List By Label    css=#si_type_alarme    ${alarme}
    # type SSI
    Run Keyword If    '${ssi}' != 'null'    Select From List By Label    css=#si_type_ssi    ${ssi}
    # conformité L16
    Run Keyword If    '${conformite}' == 'true'    Select From List By Label    css=#si_conformite_l16    Oui
    Run Keyword If    '${conformite}' == 'false'    Select From List By Label    css=#si_conformite_l16    Non
    Run Keyword If    '${conformite}' == 'nc'    Select From List By Label    css=#si_conformite_l16    NC
    # alimentation de remplacement
    Run Keyword If    '${alimentation}' == 'true'    Select From List By Label    css=#si_alimentation_remplacement    Oui
    Run Keyword If    '${alimentation}' == 'false'    Select From List By Label    css=#si_alimentation_remplacement    Non
    Run Keyword If    '${alimentation}' == 'nc'    Select From List By Label    css=#si_alimentation_remplacement    NC
    # Service sécurité
    Run Keyword If    '${service_securite}' == 'true'    Select From List By Label    css=#si_service_securite    Oui
    Run Keyword If    '${service_securite}' == 'false'    Select From List By Label    css=#si_service_securite    Non
    Run Keyword If    '${service_securite}' == 'nc'    Select From List By Label    css=#si_service_securite    NC
    # effectif du personnel de jour
    Run Keyword If    '${personnel_jour}' != 'null'    Input Text    css=#si_personnel_jour    ${personnel_jour}
    # effectif du personnel de nuit
    Run Keyword If    '${personnel_nuit}' != 'null'    Input Text    css=#si_personnel_nuit    ${personnel_nuit}
    # effectif public
    Run Keyword If    '${effectif_public}' != 'null'    Input Text    css=#si_effectif_public    ${effectif_public}
    # effectif personnel
    Run Keyword If    '${effectif_personnel}' != 'null'    Input Text    css=#si_effectif_personnel    ${effectif_personnel}

Saisir données techniques ACC
    [Arguments]    ${auditif}=null    ${mental}=null    ${physique}=null    ${visuel}=null    ${stationnement}=null    ${ascenseur}=null    ${elevateur}=null    ${boucle}=null    ${sanitaire}=null    ${places_assises}=null    ${chambres}=null    ${douche}=null    ${derogation}=null

    # accessible auditif
    Run Keyword If    '${auditif}' == 'true'    Select From List By Label    css=#acc_handicap_auditif    Oui
    Run Keyword If    '${auditif}' == 'false'    Select From List By Label    css=#acc_handicap_auditif    Non
    Run Keyword If    '${auditif}' == 'nc'    Select From List By Label    css=#acc_handicap_auditif    NC
    # accessible mental
    Run Keyword If    '${mental}' == 'true'    Select From List By Label    css=#acc_handicap_mental    Oui
    Run Keyword If    '${mental}' == 'false'    Select From List By Label    css=#acc_handicap_mental    Non
    Run Keyword If    '${mental}' == 'nc'    Select From List By Label    css=#acc_handicap_mental    NC
    # accessible physique
    Run Keyword If    '${physique}' == 'true'    Select From List By Label    css=#acc_handicap_physique    Oui
    Run Keyword If    '${physique}' == 'false'    Select From List By Label    css=#acc_handicap_physique    Non
    Run Keyword If    '${physique}' == 'nc'    Select From List By Label    css=#acc_handicap_physique    NC
    # accessible visuel
    Run Keyword If    '${visuel}' == 'true'    Select From List By Label    css=#acc_handicap_visuel    Oui
    Run Keyword If    '${visuel}' == 'false'    Select From List By Label    css=#acc_handicap_visuel    Non
    Run Keyword If    '${visuel}' == 'nc'    Select From List By Label    css=#acc_handicap_visuel    NC
    # nb places stationnements aménagées
    Run Keyword If    '${stationnement}' != 'null'    Input Text    css=#acc_places_stationnement_amenagees    ${stationnement}
    # Ascenseur
    Run Keyword If    '${ascenseur}' == 'true'    Select From List By Label    css=#acc_ascenseur    Oui
    Run Keyword If    '${ascenseur}' == 'false'    Select From List By Label    css=#acc_ascenseur    Non
    Run Keyword If    '${ascenseur}' == 'nc'    Select From List By Label    css=#acc_ascenseur    NC
    # élévateur
    Run Keyword If    '${elevateur}' == 'true'    Select From List By Label    css=#acc_elevateur    Oui
    Run Keyword If    '${elevateur}' == 'false'    Select From List By Label    css=#acc_elevateur    Non
    Run Keyword If    '${elevateur}' == 'nc'    Select From List By Label    css=#acc_elevateur    NC
    # Boucle magnétique
    Run Keyword If    '${boucle}' == 'true'    Select From List By Label    css=#acc_boucle_magnetique    Oui
    Run Keyword If    '${boucle}' == 'false'    Select From List By Label    css=#acc_boucle_magnetique    Non
    Run Keyword If    '${boucle}' == 'nc'    Select From List By Label    css=#acc_boucle_magnetique    NC
    # Sanitaire
    Run Keyword If    '${sanitaire}' == 'true'    Select From List By Label    css=#acc_sanitaire    Oui
    Run Keyword If    '${sanitaire}' == 'false'    Select From List By Label    css=#acc_sanitaire    Non
    Run Keyword If    '${sanitaire}' == 'nc'    Select From List By Label    css=#acc_sanitaire    NC
    # nb places assises public
    Run Keyword If    '${places_assises}' != 'null'    Input Text    css=#acc_places_assises_public    ${places_assises}
    # nb chambres aménagées
    Run Keyword If    '${chambres}' != 'null'    Input Text    css=#acc_chambres_amenagees    ${chambres}
    # Douche
    Run Keyword If    '${douche}' == 'true'    Select From List By Label    css=#acc_douche    Oui
    Run Keyword If    '${douche}' == 'false'    Select From List By Label    css=#acc_douche    Non
    Run Keyword If    '${douche}' == 'nc'    Select From List By Label    css=#acc_douche    NC
    # Dérogation SCDA
    Run Keyword If    '${derogation}' != 'null'    Select From List By Label    css=#acc_derogation_scda    ${derogation}

Accéder à l'analyse du dossier d'instruction

    [Documentation]    Permet d'accèder à l'écran d'analyse d'un dossier d'instruction.
    ...
    ...    Attention : l'utilisateur doit déjà être authentifié pour réaliser cette action.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    # On accède à l'analyse du DI
    Click Element    css=#analyses
    #
    Page Title Should Contain    ${dossier_instruction}
    Selected Tab Title Should Be    analyses    Analyse
    Wait Until Element Is Visible    titre_etat_analyse
    #

Vérifier état en cours
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Text Should Be    css=#analyse_etat    en cours de rédaction
    Action analyse présente    finaliser
    Action analyse absente    valider
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_validee
    Action analyse absente    reouvrir_actee

Vérifier état terminé
    [Arguments]    ${reouvrir}    ${valider}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Text Should Be    css=#analyse_etat    terminée
    Run Keyword If    '${reouvrir}' == 'true'    Action analyse présente    reouvrir_terminee
    Run Keyword If    '${reouvrir}' == 'false'    Action analyse absente    reouvrir_terminee
    Run Keyword If    '${valider}' == 'true'    Action analyse présente    valider
    Run Keyword If    '${valider}' == 'false'    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_validee
    Action analyse absente    reouvrir_actee

Vérifier état validé
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Text Should Be    css=#analyse_etat    validée
    Action analyse présente    reouvrir_validee
    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_actee

Vérifier état acté
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Text Should Be    css=#analyse_etat    actée
    Action analyse présente    reouvrir_actee
    Action analyse absente    valider
    Action analyse absente    finaliser
    Action analyse absente    reouvrir_terminee
    Action analyse absente    reouvrir_validee

Action analyse présente
    [Arguments]    ${action}
    Page Should Contain Element    css=#sousform-container #portlet-actions #action-form-analyses-${action}

Action analyse absente
    [Arguments]    ${action}
    Page Should Not Contain Element    css=#sousform-container #portlet-actions #action-form-analyses-${action}

Cliquer action analyse
    [Arguments]    ${action}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#action-form-analyses-${action}
    Page Should Not Contain Errors


Édition analyse en cours est activée
    [Arguments]  ${service}
    Element Should Be Visible  action_edit_en_cours_analyses_type
    Element Should Be Visible  action_edit_en_cours_objet
    Element Should Be Visible  action_edit_en_cours_descriptif_etablissement
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_classification_etablissement
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_classification_etablissement
    Element Should Be Visible  action_edit_en_cours_donnees_techniques
    Element Should Be Visible  action_edit_en_cours_reglementation_applicable
    Element Should Be Visible  action_edit_en_cours_prescriptions
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_documents_presentes
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_documents_presentes
    Element Should Be Visible  action_edit_en_cours_essais_realises
    Element Should Be Visible  action_edit_en_cours_compte_rendu
    Element Should Be Visible  action_edit_en_cours_observation
    Element Should Be Visible  action_edit_en_cours_avis_propose
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_en_cours_proposition_decision_ap
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_en_cours_proposition_decision_ap


Édition analyse en cours est désactivée
    Element Should Not Be Visible  action_edit_en_cours_analyses_type
    Element Should Not Be Visible  action_edit_en_cours_objet
    Element Should Not Be Visible  action_edit_en_cours_descriptif_etablissement
    Element Should Not Be Visible  action_edit_en_cours_classification_etablissement
    Element Should Not Be Visible  action_edit_en_cours_donnees_techniques
    Element Should Not Be Visible  action_edit_en_cours_reglementation_applicable
    Element Should Not Be Visible  action_edit_en_cours_prescriptions
    Element Should Not Be Visible  action_edit_en_cours_documents_presentes
    Element Should Not Be Visible  action_edit_en_cours_essais_realises
    Element Should Not Be Visible  action_edit_en_cours_compte_rendu
    Element Should Not Be Visible  action_edit_en_cours_observation
    Element Should Not Be Visible  action_edit_en_cours_avis_propose
    Element Should Not Be Visible  action_edit_en_cours_proposition_decision_ap

Édition analyse terminée est activée
    [Arguments]  ${service}
    Element Should Be Visible  action_edit_termine_analyses_type
    Element Should Be Visible  action_edit_termine_objet
    Element Should Be Visible  action_edit_termine_descriptif_etablissement
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_classification_etablissement
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_classification_etablissement
    Element Should Be Visible  action_edit_termine_donnees_techniques
    Element Should Be Visible  action_edit_termine_reglementation_applicable
    Element Should Be Visible  action_edit_termine_prescriptions
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_documents_presentes
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_documents_presentes
    Element Should Be Visible  action_edit_termine_essais_realises
    Element Should Be Visible  action_edit_termine_compte_rendu
    Element Should Be Visible  action_edit_termine_observation
    Element Should Be Visible  action_edit_termine_avis_propose
    Run Keyword If  '${service}' == 'si'  Element Should Be Visible  action_edit_termine_proposition_decision_ap
    Run Keyword If  '${service}' == 'acc'  Element Should Not Be Visible  action_edit_termine_proposition_decision_ap

Édition analyse terminée est désactivée
    Element Should Not Be Visible  action_edit_termine_analyses_type
    Element Should Not Be Visible  action_edit_termine_objet
    Element Should Not Be Visible  action_edit_termine_descriptif_etablissement
    Element Should Not Be Visible  action_edit_termine_classification_etablissement
    Element Should Not Be Visible  action_edit_termine_donnees_techniques
    Element Should Not Be Visible  action_edit_termine_reglementation_applicable
    Element Should Not Be Visible  action_edit_termine_prescriptions
    Element Should Not Be Visible  action_edit_termine_documents_presentes
    Element Should Not Be Visible  action_edit_termine_essais_realises
    Element Should Not Be Visible  action_edit_termine_compte_rendu
    Element Should Not Be Visible  action_edit_termine_observation
    Element Should Not Be Visible  action_edit_termine_avis_propose
    Element Should Not Be Visible  action_edit_termine_proposition_decision_ap


Éditer bloc analyse

    [Arguments]  ${bloc}  ${etat_analyse}=en_cours

    # On clique sur l'action modifier du bloc passé en paramètre sur la vue de
    # l'analyse
    Click Element  action_edit_${etat_analyse}_${bloc}
    # On attend que le formulaire soit visibile
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#form_analyses_modifier


Valider l'analyse du dossier d'instruction
    [Arguments]  ${dossier_instruction}
    Accéder à l'analyse du dossier d'instruction  ${dossier_instruction}
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Link    Valider


Valider l'analyse
    Cliquer action analyse  valider
    Valid Message Should Be In Subform  Validation de l'analyse.


Terminer l'analyse
    Cliquer action analyse  finaliser
    Valid Message Should Be In Subform  Rédaction terminée.


Réouvrir l'analyse
    Cliquer action analyse  reouvrir_actee
    Valid Message Should Be In Subform  Réouverture de l'analyse.