*** Settings ***
Documentation     Actions spécifiques aux unités d'accessibilité

*** Keywords ***
Depuis le listing des UA

    [Documentation]    Contexte « UA », listing « UA »

    #
    Go To Dashboard
    #
    Go To Submenu In Menu    etablissements    etablissement_unite


On clique sur l'onglet UA
    [Documentation]
    [Arguments]    ${id}=null    ${libelle}=null

    #
    ${locator} =    Catenate    SEPARATOR=    css=#ua-tabs ul.ui-tabs-nav li a#    ${id}
    #
    L'onglet doit être présent    ${id}    ${libelle}
    #
    Click Element    ${locator}
    #
    L'onglet doit être sélectionné    ${id}    ${libelle}
    #
    Sleep    1
    #
    Page Should Not Contain Errors


Depuis le listing des 'UA validées' sur l'établissement

    [Documentation]    Contexte « établissement », onglet « UA », listing « Validées »

    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null

    Depuis le contexte de l'établissement    ${code_etablissement}    ${libelle_etablissement}
    On clique sur l'onglet    etablissement_unite    UA
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_valide     enregistrement(s) sur


Depuis le listing des 'UA en projet' sur l'établissement

    [Documentation]    Contexte « établissement », onglet « UA », listing « En projet »

    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null

    Depuis le contexte de l'établissement    ${code_etablissement}    ${libelle_etablissement}
    On clique sur l'onglet    etablissement_unite    UA
    On clique sur l'onglet UA    onglet-etablissement_unite__contexte_etab__ua_enprojet    En Projet
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_enprojet     enregistrement(s) sur


Depuis le listing des 'UA archivées' sur l'établissement

    [Documentation]    Contexte « établissement », onglet « UA », listing « Archivées »

    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null

    Depuis le contexte de l'établissement    ${code_etablissement}    ${libelle_etablissement}
    On clique sur l'onglet    etablissement_unite    UA
    On clique sur l'onglet UA    onglet-etablissement_unite__contexte_etab__ua_archive    Archivées
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain    css=#sousform-etablissement_unite__contexte_etab__ua_archive     enregistrement(s) sur


Depuis le listing des 'UA analysées' sur le dossier d'instruction

    [Documentation]    Contexte « DI », onglet « analyse », listing « UA Analysées »

    [Arguments]    ${dossier_instruction}=null

    Accéder à l'analyse du dossier d'instruction    ${dossier_instruction}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element  action_edit_en_cours_donnees_techniques
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Element Should Contain    css=#sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse     enregistrement(s) sur


Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction

    [Documentation]    Contexte « DI », onglet « analyse », listing « UA Validées sur l'établissement »

    [Arguments]    ${dossier_instruction}=null

    Accéder à l'analyse du dossier d'instruction    ${dossier_instruction}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element  action_edit_en_cours_donnees_techniques
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    On clique sur l'onglet UA    onglet-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab    UA Validées Sur L'établissement


Ajouter l'unité d'accessibilité depuis le contexte d'un établissement
    [Documentation]    Permet d'ajouter une unité d'accessibilité depuis le
    ...    contexte d'un établissement.
    ...    Les paramètres par défaut à 'aucun' peuvent avoir comme valeur 'null'.
    [Arguments]    ${etablissement_code}    ${etablissement_libelle}    ${libelle}    ${accessible_auditif}=aucun    ${accessible_mental}=aucun    ${accessible_physique}=aucun    ${accessible_visuel}=aucun    ${nb_stationnements_amenages}=null    ${ascenseur}=aucun    ${elevateur}=aucun    ${boucle_magnetique}=aucun    ${sanitaire}=aucun    ${nb_public_assis}=null    ${nb_chambres_amenagees}=null    ${douche}=aucun    ${derogation_scda}=null    ${consignes_om_html}=null    ${description_om_html}=null
    # On va sur l'établissement
    Depuis le contexte de l'établissement    ${etablissement_code}    ${etablissement_libelle}
    # On change d'onglet
    On clique sur l'onglet    etablissement_unite    UA
    # On vérifie que l'action ajouter est disponible
    L'action ajouter doit être disponible
    # On clique sur le bouton ajouter
    Click On Add Button JS
    # On saisit les valeurs de l'unité d'accessibilité
    Saisir l'unité d'accessibilité    ${libelle}    ${accessible_auditif}    ${accessible_mental}    ${accessible_physique}    ${accessible_visuel}    ${nb_stationnements_amenages}    ${ascenseur}    ${elevateur}    ${boucle_magnetique}    ${sanitaire}    ${nb_public_assis}    ${nb_chambres_amenagees}    ${douche}    ${derogation_scda}    ${consignes_om_html}    ${description_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be In Subform    Vos modifications ont bien été enregistrées.

Saisir l'unité d'accessibilité
    [Documentation]    Permet de saisir le formulaire d'une unité d'accessiblité.
    ...    Les paramètres par défaut à 'aucun' peuvent avoir comme valeur 'null'.
    [Arguments]    ${libelle}    ${accessible_auditif}=aucun    ${accessible_mental}=aucun    ${accessible_physique}=aucun    ${accessible_visuel}=aucun    ${nb_stationnements_amenages}=null    ${ascenseur}=aucun    ${elevateur}=aucun    ${boucle_magnetique}=aucun    ${sanitaire}=aucun    ${nb_public_assis}=null    ${nb_chambres_amenagees}=null    ${douche}=aucun    ${derogation_scda}=null    ${consignes_om_html}=null    ${description_om_html}=null
    # On saisit le libellé
    Input Text    css=#sousform-container #libelle    ${libelle}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${accessible_auditif}' != 'aucun'    Select From List By Value    css=#sousform-container #acc_handicap_auditif    ${accessible_auditif}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${accessible_mental}' != 'aucun'    Select From List By Value    css=#sousform-container #acc_handicap_mental    ${accessible_mental}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${accessible_physique}' != 'aucun'    Select From List By Value    css=#sousform-container #acc_handicap_physique    ${accessible_physique}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${accessible_visuel}' != 'aucun'    Select From List By Value    css=#sousform-container #acc_handicap_visuel    ${accessible_visuel}
    # On saisit le nombre de stationnement aménagé
    Run keyword If    '${nb_stationnements_amenages}' != 'null'    Input Text    css=#sousform-container #nb_stationnements_amenages    ${nb_stationnements_amenages}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${ascenseur}' != 'aucun'    Select From List By Value    css=#sousform-container #ascenseur    ${ascenseur}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${elevateur}' != 'aucun'    Select From List By Value    css=#sousform-container #elevateur    ${elevateur}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${boucle_magnetique}' != 'aucun'    Select From List By Value    css=#sousform-container #boucle_magnetique    ${boucle_magnetique}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${sanitaire}' != 'aucun'    Select From List By Value    css=#sousform-container #sanitaire    ${sanitaire}
    # On saisit le nombre de place public assise
    Run keyword If    '${nb_public_assis}' != 'null'    Input Text    css=#sousform-container #nb_public_assis    ${nb_public_assis}
    # On saisit le nombre de chambre aménagée
    Run keyword If    '${nb_chambres_amenagees}' != 'null'    Input Text    css=#sousform-container #nb_chambres_amenagees    ${nb_chambres_amenagees}
    # On sélectionne OUI, NON ou RIEN
    Run keyword If    '${douche}' != 'aucun'    Select From List By Value    css=#sousform-container #douche    ${douche}
    # On sélectionne la dérogation SCDA
    Run keyword If    '${derogation_scda}' != 'null'    Select From List By Label    css=#sousform-container #acc_derogation_scda    ${derogation_scda}
    # On saisit les condignes
    Run keyword If    '${consignes_om_html}' != 'null'    Input HTML    consignes_om_html    ${consignes_om_html}
    # On saisit la description
    Run keyword If    '${description_om_html}' != 'null'    Input HTML    acc_descriptif_ua_om_html    ${description_om_html}


Depuis la fiche de l'UA
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    Depuis le listing des UA
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-form-etablissement_unite-ua---etablissement #libelle  ${libelle_ua}


Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    Depuis le listing des 'UA validées' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-sousform-etablissement_unite__contexte_etab__ua_valide-ua---etablissement #libelle  ${libelle_ua}


Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    Depuis le listing des 'UA en projet' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-sousform-etablissement_unite__contexte_etab__ua_enprojet-ua---etablissement #libelle  ${libelle_ua}


Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    Depuis le listing des 'UA archivées' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-sousform-etablissement_unite__contexte_etab__ua_archive-ua---etablissement #libelle  ${libelle_ua}


Depuis la fiche de l'UA dans le contexte des 'UA analysées' sur le dossier d'instruction
    [Arguments]    ${dossier_instruction}=null    ${libelle_ua}=null
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${dossier_instruction}
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-sousform-etablissement_unite__contexte_di_analyse__ua_en_analyse-ua---etablissement #libelle  ${libelle_ua}


Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction
    [Arguments]    ${dossier_instruction}=null    ${libelle_ua}=null
    Depuis le listing des 'UA validées sur l'établissement' sur le dossier d'instruction    ${dossier_instruction}
    Click On Link    ${libelle_ua}
    Element Should Contain  css=#fieldset-sousform-etablissement_unite__contexte_di_analyse__ua_valide_sur_etab-ua---etablissement #libelle  ${libelle_ua}


Ajouter l'UA depuis le contexte d'un établissement
    [Documentation]    Permet d'ajouter une unité d'accessibilité depuis le
    ...    contexte d'un établissement.
    ...    Les paramètres par défaut à 'aucun' peuvent avoir comme valeur 'null'.
    [Arguments]    ${etablissement_code}    ${etablissement_libelle}    ${libelle}    ${accessible_auditif}=aucun    ${accessible_mental}=aucun    ${accessible_physique}=aucun    ${accessible_visuel}=aucun    ${nb_stationnements_amenages}=null    ${ascenseur}=aucun    ${elevateur}=aucun    ${boucle_magnetique}=aucun    ${sanitaire}=aucun    ${nb_public_assis}=null    ${nb_chambres_amenagees}=null    ${douche}=aucun    ${derogation_scda}=null    ${consignes_om_html}=null    ${description_om_html}=null

    #
    Depuis le listing des 'UA validées' sur l'établissement    ${etablissement_code}    ${etablissement_libelle}
    # On vérifie que l'action ajouter est disponible
    L'action ajouter doit être disponible
    # On clique sur le bouton ajouter
    Click On Add Button JS
    # On saisit les valeurs de l'unité d'accessibilité
    Saisir l'unité d'accessibilité    ${libelle}    ${accessible_auditif}    ${accessible_mental}    ${accessible_physique}    ${accessible_visuel}    ${nb_stationnements_amenages}    ${ascenseur}    ${elevateur}    ${boucle_magnetique}    ${sanitaire}    ${nb_public_assis}    ${nb_chambres_amenagees}    ${douche}    ${derogation_scda}    ${consignes_om_html}    ${description_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be In Subform    Vos modifications ont bien été enregistrées.


Ajouter l'UA depuis le contexte d'un dossier d'instruction
    [Documentation]    Permet d'ajouter une unité d'accessibilité depuis le
    ...    contexte d'un dossier d'instruction
    ...    Les paramètres par défaut à 'aucun' peuvent avoir comme valeur 'null'.
    [Arguments]    ${dossier_instruction}    ${libelle}    ${accessible_auditif}=aucun    ${accessible_mental}=aucun    ${accessible_physique}=aucun    ${accessible_visuel}=aucun    ${nb_stationnements_amenages}=null    ${ascenseur}=aucun    ${elevateur}=aucun    ${boucle_magnetique}=aucun    ${sanitaire}=aucun    ${nb_public_assis}=null    ${nb_chambres_amenagees}=null    ${douche}=aucun    ${derogation_scda}=null    ${consignes_om_html}=null    ${description_om_html}=null

    #
    Depuis le listing des 'UA analysées' sur le dossier d'instruction    ${dossier_instruction}
    # On vérifie que l'action ajouter est disponible
    L'action ajouter doit être disponible
    # On clique sur le bouton ajouter
    Click On Add Button JS
    # On saisit les valeurs de l'unité d'accessibilité
    Saisir l'unité d'accessibilité    ${libelle}    ${accessible_auditif}    ${accessible_mental}    ${accessible_physique}    ${accessible_visuel}    ${nb_stationnements_amenages}    ${ascenseur}    ${elevateur}    ${boucle_magnetique}    ${sanitaire}    ${nb_public_assis}    ${nb_chambres_amenagees}    ${douche}    ${derogation_scda}    ${consignes_om_html}    ${description_om_html}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be In Subform    Vos modifications ont bien été enregistrées.


Archiver l'UA en projet
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    #
    Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}    ${libelle_ua}
    #
    Click On SubForm Portlet Action    etablissement_unite__contexte_etab__ua_enprojet    archiver
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be     Archivage correctement effectué.


Archiver l'UA validée
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    #
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}    ${libelle_ua}
    #
    Click On SubForm Portlet Action    etablissement_unite__contexte_etab__ua_valide    archiver
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be     Archivage correctement effectué.


Désarchiver l'UA
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    #
    Depuis la fiche de l'UA dans le contexte des 'UA archivées' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}    ${libelle_ua}
    #
    Click On SubForm Portlet Action    etablissement_unite__contexte_etab__ua_archive    desarchiver
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be     Désarchivage correctement effectué.


Valider l'UA en projet
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    #
    Depuis la fiche de l'UA dans le contexte des 'UA en projet' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}    ${libelle_ua}
    #
    Click On SubForm Portlet Action    etablissement_unite__contexte_etab__ua_enprojet    valider
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be     UA passée dans l'état 'validé'.


Repasser en projet l'UA validée
    [Arguments]    ${code_etablissement}=null    ${libelle_etablissement}=null    ${libelle_ua}=null
    #
    Depuis la fiche de l'UA dans le contexte des 'UA validées' sur l'établissement    ${code_etablissement}    ${libelle_etablissement}    ${libelle_ua}
    #
    Click On SubForm Portlet Action    etablissement_unite__contexte_etab__ua_valide    repasser_en_projet
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be     UA repassée dans l'état 'en projet'.


Analyser l'UA validée
    [Arguments]    ${dossier_instruction}=null    ${libelle_ua}=null
    # On visualise l'UA validée dans le contexte du DI
    Depuis la fiche de l'UA dans le contexte des 'UA validées sur l'établissement' sur le dossier d'instruction    ${dossier_instruction}    ${libelle_ua}
    # On clique sur l'action analyser
    Click On SubForm Portlet Action  etablissement_unite__contexte_di_analyse__ua_valide_sur_etab  analyser
    #
    Click On Submit Button In Subform
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Vos modifications ont bien été enregistrées.


