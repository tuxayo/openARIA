*** Settings ***
Documentation     Actions spécifiques aux programmations.

*** Keywords ***
Depuis le contexte de la programmation

    [Documentation]    Permet d'accéder à l'écran de visualisation d'une programmation.

    [Arguments]    ${programmation}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    programmation
    # On recherche la réunion récemment crée
    Use Simple Search    semaine    ${programmation}
    # On clique sur la réunion
    Click On Link    ${programmation}
    #
    Page Title Should Contain    Suivi > Programmations > Gestion > Semaine



Depuis le listing des programmations

    [Documentation]

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    suivi    programmation
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Programmations > Gestion


Ajouter la programmation depuis le menu

    [Arguments]  ${service}=null  ${annee}=null  ${semaine}=null

    # On ouvre le tableau
    Go To Tab  programmation
    # On clique sur l'action ajouter du tableau
    Click On Add Button
    # On remplit le formulaire
    Saisir la programmation  ${service}  ${annee}  ${semaine}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Saisir la programmation

    [Arguments]  ${service}=null  ${annee}=null  ${semaine}=null

    # Service
    Run Keyword If  '${service}' != 'null'  Select From List By Label  service  ${service}
    # Tête de chapitre 1
    Run Keyword If  '${annee}' != 'null'  Input Value With JS  annee  ${annee}
    # Tête de chapitre 2
    Run Keyword If  '${semaine}' != 'null'  Input Value With JS  numero_semaine  ${semaine}


Finaliser la programmation

    [Arguments]    ${programmation}

    Depuis le contexte de la programmation    ${programmation}
    # On finalise la programmation
    Click On Form Portlet Action    programmation    finaliser
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Finalisation correctement effectuée.


Valider la programmation

    [Arguments]    ${programmation}

    Depuis le contexte de la programmation    ${programmation}
    # On finalise la programmation
    Click On Form Portlet Action    programmation    valider
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Validation correctement effectuée.


Finaliser puis valider la programmation

    [Arguments]    ${programmation}

    Depuis le contexte de la programmation    ${programmation}
    # On finalise la programmation
    Click On Form Portlet Action    programmation    finaliser
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Finalisation correctement effectuée.
    # On finalise la programmation
    Click On Form Portlet Action    programmation    valider
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Validation correctement effectuée.


Désactiver évènements heure minute
    Disable Event  heure_debut  onchange
    Disable Event  heure_debut_heure  onchange
    Disable Event  heure_debut_minute  onchange
    Disable Event  heure_fin  onchange
    Disable Event  heure_fin_heure  onchange
    Disable Event  heure_fin_minute  onchange


Depuis l'interface de planification de la programmation

    [Documentation]    Permet d'accéder à l'écran de planification d'une programmation.

    [Arguments]    ${programmation}

    Depuis le contexte de la programmation    ${programmation}
    Click On Form Portlet Action    programmation    programmer
    Element Text Should Be    css=#bloc_entete    Semaine ${programmation}

