*** Settings ***
Documentation     Actions spécifiques aux membres d'instances.

*** Keywords ***
Depuis l'onglet membre de l'instance

    [Documentation]

    [Arguments]    ${instance}

    #
    Depuis le contexte d'une instance    ${instance}
    # On clique sur l'onglet membre
    On clique sur l'onglet    reunion_instance_membre    Membres


Ajouter un membre depuis le contexte de l'instance

    [Documentation]

    [Arguments]    ${instance}    ${membre}    ${description}=null

    #
    Depuis l'onglet membre de l'instance    ${instance}
    #
    Click On Add Button JS
    #
    Input Text    css=#membre    ${membre}
    Run Keyword If    '${description}' != 'null'    Input Text    css=#description    ${description}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    Vos modifications ont bien été enregistrées.

