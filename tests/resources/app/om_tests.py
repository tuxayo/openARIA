#!/usr/bin/python
# -*- coding: utf-8 -*-
from resources.core.om_tests import om_tests_core


class om_tests(om_tests_core):
    """
    """

    _database_name_default = "openaria"

    _instance_name_default = "openaria"

    _params_copy_files = [
        #
        {'in': 'tests/binary_files/tests_services/rest_entry.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/referentielpatrimoinetest.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/referentieladsditest.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/referentieladsdatest.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/referentieladsconsultationstest.php',
         'out': 'tests_services/'},
        {'in': 'tests/binary_files/tests_services/referentieladsmessagestest.php',
         'out': 'tests_services/'},
        #
        {'in': 'tests/binary_files/htaccess_deny_from_all', 'out': 'var/.htaccess'},
        {'in': 'tests/binary_files/dyn/*', 'out': 'dyn/'},
        #
    ]

    _params_delete_files = [
        'var/tmp/*-is-being-geolocalised',
    ]

    _params_create_folders = [
        'tests_services/',
        'var/',
        'var/log/',
        'var/tmp/',
        'var/filestorage/',
        'var/digitalization/',
        'var/digitalization/ACC/Todo/',
        'var/digitalization/ACC/Done/',
        'var/digitalization/SI/Todo/',
        'var/digitalization/SI/Done/',
    ]

    _params_chmod_777 = [
        # apache doit pouvoir écrire dans les répertoires de storage et de log
        'var/',
        # apache doit pouvoir écrire dans les répertoires destinés à recevoir
        # des fichiers générés
        'gen/obj/', 'gen/sql/pgsql/',
        'core/obj/', 'core/sql/pgsql/',
        'obj', 'sql/pgsql/',
        'tests/resources/core/gen/',
        'tests/resources/app/gen/',
    ]

