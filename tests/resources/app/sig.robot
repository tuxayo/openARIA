*** Settings ***
Documentation     Actions spécifiques au SIG.

*** Keywords ***
Activer le plugin geoaria_tests et l'option sig
    [Tags]  SIG
    Copy File  ${EXECDIR}${/}binary_files${/}sig.inc.php  ${EXECDIR}${/}..${/}dyn${/}sig.inc.php
    Modifier le paramètre  option_sig  sig_externe


Désactiver le plugin geoaria_tests et l'option sig
    [Tags]  SIG
    Remove File  ${EXECDIR}${/}..${/}dyn${/}sig.inc.php
    Modifier le paramètre  option_sig  false

Tout géolocaliser
    [Tags]  SIG
    [Documentation]  Géocode tous les DC et établissements non localisés

    Go To    ${PROJECT_URL}scr/form.php?obj=dossier_coordination_geocoder_tous&action=260&idx=0
    Submit Form
