*** Settings ***
Documentation  Objet 'reunion_type'.


*** Keywords ***
Depuis le listing des types de réunion
    [Documentation]  Accède directement via URL au listing des types de
    ...  réunion.
    Go To Tab  reunion_type


Depuis le formulaire d'ajout de type de réunion
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  type de réunion.
    Go To  ${PROJECT_URL}scr/form.php?obj=reunion_type&action=0


Depuis le contexte du type de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le listing des types de réunion
    Use Simple Search  code  ${code}
    Click On Link  ${code}


Depuis le formulaire de modification du type de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le contexte du type de réunion  ${code}
    Click On Form Portlet Action  reunion_type  modifier


Ajouter le type de réunion
    [Documentation]  Crée l'enregistrement.
    [Arguments]  ${values}
    Depuis le formulaire d'ajout de type de réunion
    Saisir les valeurs dans le formulaire de type de réunion  ${values}
    Click On Submit Button
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire de type de réunion
    [Documentation]  Remplit le formulaire.
    [Arguments]  ${values}
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_salle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "heure" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "listes_de_diffusion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele_courriel_convoquer" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "modele_courriel_cloturer" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "commission" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "president" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_ordre_du_jour" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_compte_rendu_global" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_compte_rendu_specifique" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_feuille_presence" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "categories_autorisees" existe dans "${values}" on execute "Select From List By Label" sur la liste dans le formulaire
    Si "avis_autorises" existe dans "${values}" on execute "Select From List By Label" sur la liste dans le formulaire
    Si "instances_autorisees" existe dans "${values}" on execute "Select From List By Label" sur la liste dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire


