*** Settings ***
Documentation     Actions spécifiques aux documents entrants.

*** Keywords ***
Depuis le listing des documents entrants bannette

    [Documentation]    Permet d'accéder au listing des documents entrants depuis
    ...    le menu bannette.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    suivi    piece_bannette
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > Bannette


Depuis le listing des documents entrants suivi

    [Documentation]    Permet d'accéder au listing des documents entrants depuis
    ...    le menu suivi.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    suivi    piece_suivi
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > Suivi


Depuis le listing des documents entrants à valider

    [Documentation]    Permet d'accéder au listing des documents entrants depuis
    ...    le menu suivi.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    suivi    piece_a_valider
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Documents Entrants > À Valider


Depuis le listing des documents entrants non lu

    [Documentation]    Permet d'accéder au listing des documents entrants depuis
    ...    le menu des non lus.

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    dossiers    piece_non_lu
    # On vérifie le fil d'Ariane
    Page Title Should Be    Dossiers > Documents Entrants > Mes Non Lus


Depuis l'onglet document entrant du dossier d'instruction

    [Documentation]    Permet d'accéder à l'onglet "Documents Entrants" dans le contexte d'un dossier d'instruction.

    [Arguments]    ${dossier_instruction}

    #
    Depuis le contexte du dossier d'instruction    ${dossier_instruction}
    # On clique sur l'onglet réunion
    On clique sur l'onglet    piece    Documents Entrants


Depuis l'onglet document entrant du dossier de coordination

    [Documentation]    Permet d'accéder à l'onglet "Documents Entrants" dans le contexte d'un dossier de coordination.

    [Arguments]    ${dossier_coordination}

    #
    Depuis le contexte du dossier de coordination    ${dossier_coordination}
    # On clique sur l'onglet réunion
    On clique sur l'onglet    piece    Documents Entrants


Depuis l'onglet document entrant de l'établissement

    [Documentation]    Permet d'accéder à l'onglet "Documents Entrants" dans le contexte d'un établissement.

    [Arguments]    ${etablissement_code}=null    ${etablissement_libelle}=null

    #
    Depuis le contexte de l'établissement    ${etablissement_code}    ${etablissement_libelle}
    #
    On clique sur l'onglet    piece    Documents Entrants


Depuis le contexte d'une document entrant bannette

    [Documentation]    Permet d'accéder à l'écran de visualisation d'un document
    ...    entrant depuis le menu bannette

    [Arguments]    ${piece_nom}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    piece_bannette
    # On recherche la réunion récemment crée
    Use Simple Search    nom    ${piece_nom}
    # On clique sur la réunion
    Click On Link    ${piece_nom}
    #
    Page Title Should Contain    Suivi > Documents Entrants > Bannette >


Depuis le contexte d'une document entrant à valider

    [Documentation]    Permet d'accéder à l'écran de visualisation d'un document
    ...    entrant depuis le menu bannette

    [Arguments]    ${piece_nom}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    piece_a_valider
    # On recherche la réunion récemment crée
    Use Simple Search    nom    ${piece_nom}
    # On clique sur la réunion
    Click On Link    ${piece_nom}
    #
    Page Title Should Contain    Suivi > Documents Entrants > À Valider >


Depuis le contexte d'un document entrant non lu

    [Documentation]    Permet d'accéder à l'écran de visualisation d'un document
    ...    entrant depuis le menu des non lus

    [Arguments]    ${piece_nom}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    mes_non_lus
    # On recherche la réunion récemment crée
    Use Simple Search    nom    ${piece_nom}
    # On clique sur la réunion
    Click On Link    ${piece_nom}
    #
    Page Title Should Contain    Dossiers > Documents Entrants > Mes Non Lus >


Saisir le document entrant

    [Documentation]  Remplit le formulaire
    ...  &{document_entrant} =  Create Dictionary
    ...  nom=Document entrant 1245
    ...  piece_type=Courrier de l'exploitant
    ...  uid=piece1245.pdf
    ...  date_reception=13/04/2016
    ...  date_emission=12/04/2016
    ...  service=Sécurité Incendie
    ...  choix_lien=établissement|dossier d'instruction|dossier de coordination
    ...  etablissement=T345 - MONOPRIX
    ...  dossier_instruction=VPS-VISIT-000001-SI
    ...  dossier_coordination=VPS-VISIT-000001
    ...  suivi=true
    ...  date_butoir=15/04/2016
    ...  commentaire_suivi=blablabla

    [Arguments]  ${values}

    Si "nom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "piece_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

    Si "uid" existe dans "${values}" on execute "Add File" dans le formulaire

    Si "date_reception" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "date_emission" existe dans "${values}" on execute "Input Datepicker" dans le formulaire

    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

    Si "choix_lien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

    Si "etablissement" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "etablissement_tous" dans le formulaire
    Si "dossier_coordination" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "dossier_coordination" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on sélectionne la valeur sur l'autocomplete "dossier_instruction" dans le formulaire

    Si "suivi" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_butoir" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "commentaire_suivi" existe dans "${values}" on execute "Input Text" dans le formulaire


Ajouter un document entrant depuis la bannette

    [Documentation]    Ajoute un document entrant depuis le menu "Bannette".

    [Arguments]    ${values}

    Depuis le listing des documents entrants bannette
    # On clique sur l'action ajouter du tableau
    Click On Add Button
    # On saisit le formulaire
    Saisir le document entrant  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Marquer comme lu un dossier entrant

    [Documentation]    Marque un document entrant comme lu.

    [Arguments]    ${form}

    #
    Click On Form Portlet Action    ${form}    lu
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Le document entrant a été marqué comme lu.


Marquer comme non lu un dossier entrant

    [Documentation]    Marque un document entrant comme non lu.

    [Arguments]    ${form}

    #
    Click On Form Portlet Action    ${form}    non_lu
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Le document entrant a été marqué comme non lu.


Marquer comme suivi un dossier entrant

    [Documentation]    Marque un document entrant comme suivi.

    [Arguments]    ${form}

    #
    Click On Form Portlet Action    ${form}    suivi
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Le document entrant a été marqué comme suivi.


Marquer comme non suivi un dossier entrant

    [Documentation]    Marque un document entrant comme non suivi.

    [Arguments]    ${form}

    #
    Click On Form Portlet Action    ${form}    non_suivi
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Le document entrant a été marqué comme non suivi.


Depuis le listing des documents entrants

    [Documentation]  Permet d'accéder au listing des documents entrants
    ...  directement depuis l'URL.

    #
    Go To Tab  piece


Depuis le contexte du document entrant

    [Documentation]    Permet d'accéder à l'écran de visualisation d'un document
    ...    entrant.

    [Arguments]  ${document_entrant_nom}

    Depuis le listing des documents entrants
    # On cherche le nom du document entrant
    Use Simple Search    nom    ${document_entrant_nom}
    # On clique sur la réunion
    Click On Link    ${document_entrant_nom}
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#fieldset-form-piece-informations-generales #piece


Récupérer l'identifiant du document entrant

    [Documentation]  Attention cela sous entend que le nom du document entrant
    ...  doit être unique.

    [Arguments]  ${document_entrant_nom}
    Depuis le contexte du document entrant  ${document_entrant_nom}
    ${document_entrant_id} =  Get Text  css=span#piece
    Run Keyword If  ${document_entrant_id} is ${None}  Fail
    [Return]  ${document_entrant_id}


Depuis le contexte du document entrant (accès par URL)

    [Documentation]
    [Arguments]  ${document_entrant_id}
    Go To  ${PROJECT_URL}scr/form.php?obj=piece&action=3&idx=${document_entrant_id}


Depuis le formulaire de modification du document entrant (accès par URL)

    [Documentation]
    [Arguments]  ${document_entrant_id}
    Go To  ${PROJECT_URL}scr/form.php?obj=piece&action=1&idx=${document_entrant_id}


Récupérer l'uid du fichier du document entrant

    [Arguments]  ${document_entrant_id}
    Depuis le formulaire de modification du document entrant (accès par URL)  ${document_entrant_id}
    ${document_entrant_fichier_uid} =  Get Value  css=#uid
    [Return]  ${document_entrant_fichier_uid}


Activer le plugin swrodaria_tests et l'option
    [Documentation]
    Copy File  ${EXECDIR}${/}binary_files${/}swrod.inc.php  ${EXECDIR}${/}..${/}dyn${/}swrod.inc.php
    Ajouter le paramètre depuis le menu  swrod  true  null


Désactiver le plugin swrodaria_tests et l'option
    [Documentation]
    Remove File  ${EXECDIR}${/}..${/}dyn${/}swrod.inc.php
    Supprimer le paramètre  swrod  true


Modifier le document entrant (accès par URL)

    [Documentation]
    [Arguments]  ${id}  ${values}
    Depuis le formulaire de modification du document entrant (accès par URL)  ${id}
    Saisir le document entrant  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.


Supprimer le document entrant (accès par URL)

    [Documentation]
    [Arguments]  ${id}
    Depuis le contexte du document entrant (accès par URL)  ${id}
    Click On Form Portlet Action    piece    supprimer
    Click On Submit Button
    Valid Message Should Be    La suppression a été correctement effectuée.

