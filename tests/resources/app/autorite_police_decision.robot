*** Settings ***
Documentation  Objet 'autorite_police_decision'.


*** Keywords ***
Depuis le listing des types de décision d'autorité de police
    [Documentation]  Accède directement via URL au listing des types de
    ...  décision d'autorité de police.
    Go To  ${PROJECT_URL}scr/tab.php?obj=autorite_police_decision

Depuis le formulaire d'ajout de type de décision d'autorité de police
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  type de décision d'autorité de police.
    Go To  ${PROJECT_URL}scr/form.php?obj=autorite_police_decision&action=0


Ajouter le type de décision d'autorité de police
    [Documentation]  Crée l'enregistrement.
    [Arguments]  ${values}
    Depuis le formulaire d'ajout de type de décision d'autorité de police
    Saisir les valeurs du formulaire de type de décision d'autorité de police  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs du formulaire de type de décision d'autorité de police
    [Documentation]  Remplit le formulaire.
    ...  &{typedap01} =  Create Dictionary
    ...  code=
    ...  libelle=
    ...  description=
    ...  service=
    ...  suivi_delai=
    ...  avis=
    ...  etablissement_etat=
    ...  delai=
    ...  type_arrete=
    ...  arrete_reglementaire=
    ...  arrete_notification=
    ...  arrete_publication=
    ...  arrete_temporaire=
    ...  nomenclature_actes_nature=
    ...  nomenclature_actes_matiere_niv1=
    ...  nomenclature_actes_matiere_niv2=
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "suivi_delai" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "delai" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_arrete" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "arrete_reglementaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrete_notification" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrete_publication" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "arrete_temporaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nomenclature_actes_nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nomenclature_actes_matiere_niv1" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "nomenclature_actes_matiere_niv2" existe dans "${values}" on execute "Select From List By Label" dans le formulaire


