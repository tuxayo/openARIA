*** Settings ***
Documentation  Objet 'reunion_instance'.

*** Keywords ***
Depuis le listing des instances de réunion
    [Documentation]  Accède directement via URL au listing des instances de
    ...  réunion.
    Go To Tab  reunion_instance


Depuis le formulaire d'ajout d'instance de réunion
    [Documentation]  Accède directement via URL au formulaire d'ajout d'une
    ...  instance de réunion.
    Go To  ${PROJECT_URL}scr/form.php?obj=reunion_instance&action=0


Depuis le contexte d'une instance
    [Documentation]  @deprecated Keyword remplacé par "Depuis le contexte de l'instance de réunion"
    [Arguments]  ${code}
    Depuis le contexte de l'instance de réunion  ${code}


Depuis le contexte de l'instance de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le listing des instances de réunion
    Use Simple Search  code  ${code}
    Click On Link  ${code}


Depuis le formulaire de modification de l'instance de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le contexte de l'instance de réunion  ${code}
    Click On Form Portlet Action  reunion_instance  modifier


Modifier l'instance de réunion
    [Documentation]
    [Arguments]  ${code}  ${values}
    Depuis le formulaire de modification de l'instance de réunion  ${code}
    Saisir les valeurs dans le formulaire d'instance de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Ajouter l'instance de réunion
    [Documentation]  Crée l'enregistrement.
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'instance de réunion
    Saisir les valeurs dans le formulaire d'instance de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire d'instance de réunion
    [Documentation]  Remplit le formulaire.
    ...
    ...  &{reuinstance01} =  Create Dictionary
    ...  code=
    ...  libelle=
    ...  description=
    ...  courriels=
    ...  service=
    ...  ordre=
    [Arguments]  ${values}
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courriels" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire



