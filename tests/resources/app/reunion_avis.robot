*** Settings ***
Documentation  Objet 'reunion_avis'.

*** Keywords ***
Depuis le listing des avis de réunion
    [Documentation]  Accède directement via URL au listing des avis de
    ...  réunion.
    Go To Tab  reunion_avis


Depuis le formulaire d'ajout d'avis de réunion
    [Documentation]  Accède directement via URL au formulaire d'ajout d'un
    ...  avis de réunion.
    Go To  ${PROJECT_URL}scr/form.php?obj=reunion_avis&action=0


Depuis le contexte de l'avis de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le listing des avis de réunion
    Use Simple Search  code  ${code}
    Click On Link  ${code}


Depuis le formulaire de modification de l'avis de réunion
    [Documentation]
    [Arguments]  ${code}
    Depuis le contexte de l'avis de réunion  ${code}
    Click On Form Portlet Action  reunion_avis  modifier


Modifier l'avis de réunion
    [Documentation]
    [Arguments]  ${code}  ${values}
    Depuis le formulaire de modification de l'avis de réunion  ${code}
    Saisir les valeurs dans le formulaire d'avis de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Ajouter l'avis de réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}
    Depuis le formulaire d'ajout d'avis de réunion
    Saisir les valeurs dans le formulaire d'avis de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Saisir les valeurs dans le formulaire d'avis de réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}

    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire

