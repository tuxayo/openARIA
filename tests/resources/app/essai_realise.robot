*** Settings ***
Documentation  Actions spécifiques au paramétrage des essais réalisés

*** Keywords ***
Ajouter l'essai réalisé depuis le tableau
    [Arguments]  ${service}  ${code}  ${libelle}  ${description}=null  ${debut}=null  ${fin}=null

    # On ouvre le tableau de bord en admin
    Depuis la page d'accueil    admin    admin
    # On accède au tableau des essais
    Go To Tab  essai_realise
    # On clique le bouton "+" pour accéder au formulaire d'ajout
    Click On Add Button
    # On saisit les champs
    Saisir l'essai réalisé  ${service}  ${code}  ${libelle}  ${description}  ${debut}  ${fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie que l'ajout a fonctionné
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Be  Vos modifications ont bien été enregistrées.

Modifier l'essai réalisé depuis le tableau
    [Arguments]  ${libelle}  ${service}=null  ${code}=null  ${description}=null  ${debut}=null  ${fin}=null

    # On ouvre le tableau de bord en admin
    Depuis la page d'accueil    admin    admin
    # On accède au tableau des essais
    Go To Tab  essai_realise
    # On clique le libellé de l'essai
    Click On Link  ${libelle}
    # On clique sur l'action modifier
    Click On Form Portlet Action  essai_realise  modifier
    # On saisit les champs
    Saisir l'essai réalisé  ${service}  ${code}  ${libelle}  ${description}  ${debut}  ${fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie que la modification a fonctionné
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Be  Vos modifications ont bien été enregistrées.

Saisir l'essai réalisé
    [Documentation]  Remplie les champs du formulaire du document généré.
    [Arguments]  ${service}=null  ${code}=null  ${libelle}=null  ${description}=null  ${debut}=null  ${fin}=null

    # Sélectionne le service
    Run keyword If  '${service}' != 'null'  Select From List By Label  service  ${service}
    # Saisit le code
    Run keyword If  '${code}' != 'null'  Input Text  code  ${code}
    # Saisit le libellé
    Run keyword If  '${libelle}' != 'null'  Input Text  libelle  ${libelle}
    # Saisit la description
    Run keyword If  '${description}' != 'null'  Input Text  description  ${description}
    # Saisit la date de début de validité
    Run keyword If  '${debut}' != 'null'  Input Datepicker  om_validite_debut  ${debut}
    # Saisit la date de fin de validité
    Run keyword If  '${fin}' != 'null'  Input Datepicker  om_validite_fin  ${fin}

Supprimer l'essai réalisé
    [Arguments]  ${libelle}

    # On ouvre le tableau de bord en admin
    Depuis la page d'accueil    admin    admin
    # On accède au tableau des essais
    Go To Tab  essai_realise
    # On clique le libellé de l'essai
    Click On Link  ${libelle}
    # On clique sur l'action supprimer
    Click On Form Portlet Action  essai_realise  supprimer
    # On valide le formulaire de suppression
    Click On Submit Button