*** Settings ***
Documentation     Actions spécifiques aux prescriptions.

*** Keywords ***
Ajouter la prescription réglementaire depuis le menu
    [Arguments]  ${service}  ${tc1}=null  ${tc2}=null  ${lib}=null  ${desc}=null  ${def}=null  ${cat}=${EMPTY}  ${typ}=${EMPTY}  ${debut}=null  ${fin}=null

    # On ouvre le tableau
    Go To Tab  prescription_reglementaire
    # On clique sur l'action ajouter du tableau
    Click On Add Button
    # On remplit le formulaire
    Saisir la prescription réglementaire  ${service}  ${tc1}  ${tc2}  ${lib}  ${desc}  ${def}  ${cat}  ${typ}  ${debut}  ${fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Modifier la prescription réglementaire depuis le menu
    [Arguments]  ${lib}  ${service}=null  ${tc1}=null  ${tc2}=null  ${desc}=null  ${def}=null  ${cat}=${EMPTY}  ${typ}=${EMPTY}  ${debut}=null  ${fin}=null

    # On ouvre le tableau
    Go To Tab  prescription_reglementaire
    # On clique sur la ligne de l'enregistrement à modifier
    Click On Link  ${lib}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  prescription_reglementaire  modifier
    # On remplit le formulaire
    Saisir la prescription réglementaire  ${service}  ${tc1}  ${tc2}  ${lib}  ${desc}  ${def}  ${cat}  ${typ}  ${debut}  ${fin}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Wait Until Keyword Succeeds  5 sec  0.2 sec  Valid Message Should Contain  Vos modifications ont bien été enregistrées.

Saisir la prescription réglementaire
    [Arguments]  ${service}=null  ${tc1}=null  ${tc2}=null  ${lib}=null  ${desc}=null  ${def}=null  ${cat}=${EMPTY}  ${typ}=${EMPTY}  ${debut}=null  ${fin}=null

    # Service
    Run Keyword If  '${service}' != 'null'  Select From List By Label  service  ${service}
    # Tête de chapitre 1
    Run Keyword If  '${tc1}' != 'null'  Input Text  tete_de_chapitre1  ${tc1}
    # Tête de chapitre 2
    Run Keyword If  '${tc2}' != 'null'  Input Text  tete_de_chapitre2  ${tc2}
    # Libellé
    Run Keyword If  '${lib}' != 'null'  Input Text  libelle  ${lib}
    # Description
    Run Keyword If  '${desc}' != 'null'  Input HTML  description_pr_om_html  ${desc}
    # Checkbox défavorable
    Run Keyword If  '${def}' == 'true'  Select Checkbox  defavorable
    Run Keyword If  '${def}' == 'false'  Unselect Checkbox  defavorable
    # Catégorie(s) d'établissement
    Select Multiple By Label  etablissement_categorie  ${cat}
    # Type(s) d'établissement
    Select Multiple By Label  etablissement_type  ${typ}
    # Date de début de validité
    Run Keyword If  '${debut}' != 'null'  Input Datepicker  om_validite_debut  ${debut}
    # Date de fin de validité
    Run Keyword If  '${fin}' != 'null'  Input Datepicker  om_validite_fin  ${fin}