*** Settings ***
Documentation     Actions spécifiques aux réunions.

*** Keywords ***
Depuis le contexte de la réunion

    [Documentation]    Permet d'accéder à l'écran de visualisation d'une réunion.

    [Arguments]    ${reunion}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    suivi    reunion
    # On recherche la réunion récemment crée
    Use Simple Search    code    ${reunion}
    # On clique sur la réunion
    Click On Link    ${reunion}
    #
    Page Title Should Contain    ${reunion}


Depuis le listing des réunions

    [Documentation]

    # On clique sur le tableau de bord
    Go To Dashboard
    # On clique sur l'item correspondant du menu
    Go To Submenu In Menu    suivi    reunion
    # On vérifie le fil d'Ariane
    Page Title Should Be    Suivi > Réunions > Gestion


Depuis le formulaire d'ajout de réunion
    [Documentation]  Accède directement via URL au formulaire d'ajout d'une
    ...  réunion.
    Go To  ${PROJECT_URL}scr/form.php?obj=reunion&action=0


Depuis le formulaire de modification de la réunion
    [Documentation]  Accède au formulaire de modification de la réunion dont le
    ...  code est passé en paramètre.
    [Arguments]  ${reunion_code}
    Depuis le contexte de la réunion  ${reunion_code}
    Click On Form Portlet Action  reunion  modifier


Ajouter la réunion
    [Documentation]  Crée l'enregistrement.
    [Arguments]  ${values}
    Depuis le formulaire d'ajout de réunion
    Saisir les valeurs dans le formulaire de réunion  ${values}
    Click On Submit Button
    Valid Message Should Be  Vos modifications ont bien été enregistrées.


Depuis l'ordre du jour de la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    meeting


Depuis l'interface de planification des demandes de passage pressenties pour la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    planifier
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page SubTitle Should Be    > Planifier des demandes de passage existantes


Depuis l'interface de déplanification des demandes de passage pour la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    deplanifier
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page SubTitle Should Be    > Déplanifier des demandes de pages de l'ordre du jour


Depuis l'interface de planification de dossier d'instruction sans demande de passage pour la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    planifier-nouveau
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page SubTitle Should Be    > Planifier un ou plusieurs dossiers sans demande de passage


Planifier toutes les demandes de passages pressenties pour la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis l'interface de planification des demandes de passage pressenties pour la réunion    ${reunion}
    #
    Select Checkbox    css=#dir-checkall
    #
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Click Element  css=#form_reunion_planning div.formControls input[type="submit"]
    # On gère le message au signulier et au pluriel
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Valid Message Should Contain    été correctement planifié


Numéroter l'ordre du jour de la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    numeroter
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Be    Numérotation terminée.


Planifier directement le DC ou DI pour la réunion dans la catégorie

    [Documentation]

    [Arguments]    ${dossier_a_planifier}    ${reunion}    ${categorie}

    #
    Depuis le formulaire de planification directe en mode pour la réunion    dossier    ${reunion}
    # On saisit le numéro de dossier
    Input Text    css=#dossier    ${dossier_a_planifier}
    # On sélectionne la catégorie
    Select From List By Label    css=#categorie    ${categorie}
    # On valide le formulaire
    Click On Submit Button
    # On gère le message au signulier et au pluriel
    Valid Message Should Contain    Dossier(s) planifié(s) avec succès.


Planifier directement les dossiers de la programmation pour la réunion dans la catégorie

    [Documentation]

    [Arguments]    ${programmation_a_planifier}    ${reunion}    ${categorie}

    #
    Depuis le formulaire de planification directe en mode pour la réunion    programmation    ${reunion}
    # On saisit la programmation
    Select From List By Label    css=#programmation    ${programmation_a_planifier}
    # On sélectionne la catégorie
    Select From List By Label    css=#categorie    ${categorie}
    # On valide le formulaire
    Click On Submit Button
    # On gère le message au signulier et au pluriel
    Valid Message Should Contain    Dossier(s) planifié(s) avec succès.


Planifier directement les dossiers de la réunion pour la réunion dans la catégorie

    [Documentation]

    [Arguments]    ${reunion_a_planifier}    ${reunion}    ${categorie}

    #
    Depuis le formulaire de planification directe en mode pour la réunion    réunion    ${reunion}
    # On saisit la réunion
    Select From List By Label    css=#reunion    ${reunion_a_planifier}
    # On sélectionne la catégorie
    Select From List By Label    css=#categorie    ${categorie}
    # On valide le formulaire
    Click On Submit Button
    # On gère le message au signulier et au pluriel
    Valid Message Should Contain    Dossier(s) planifié(s) avec succès.


Depuis le formulaire de planification directe en mode pour la réunion

    [Documentation]

    [Arguments]    ${mode_de_planification}    ${reunion}

    #
    Depuis l'interface de planification de dossier d'instruction sans demande de passage pour la réunion    ${reunion}
    # On sélectionne le type de planification
    Select From List By Label    css=#reunion_choix_mode_planif    ${mode_de_planification}


Rendre l'avis sur tous les dossiers de la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis l'ordre du jour de la réunion    ${reunion}
    #
    Click Element    css=table.meeting tr a
    #
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Page SubTitle Should Be    > Ordre du jour > n°1
    #
    :FOR    ${INDEX}    IN RANGE    1    10
    \    Rendre un avis favorable depuis la fiche d'une demande de passage
    \    ${status} =    Run Keyword And Return Status    Click On SubForm Portlet Action    dossier_instruction_reunion_contexte_reunion    next
    \    Run Keyword If    '${status}' == 'False'    Exit For Loop


Rendre un avis favorable depuis la fiche d'une demande de passage

    [Documentation]

    #
    Click On SubForm Portlet Action    dossier_instruction_reunion_contexte_reunion    rendre_l_avis
    #
    Select From List By Label    css=#avis    FAVORABLE
    #
    Click On Submit Button In Subform
    #
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.
    #
    Click On Back Button In Subform


Clôturer la réunion

    [Documentation]

    [Arguments]    ${reunion}

    #
    Depuis le contexte de la réunion    ${reunion}
    #
    Click On Form Portlet Action    reunion    cloturer
    # On valide le formulaire
    Click On Submit Button
    #
    Valid Message Should Contain    La clôture de la réunion a été correctement effectuée.


Saisir les valeurs dans le formulaire de réunion
    [Documentation]  Remplit le formulaire.
    ...
    ...  &{reunion01} =  Create Dictionary
    ...  reunion_type=
    ...  libelle=
    ...  date_reunion=
    ...  heure_reunion=
    ...  lieu_adresse_ligne1=
    ...  lieu_adresse_ligne2=
    ...  lieu_salle=
    ...  listes_de_diffusion=
    ...  participants=
    [Arguments]  ${values}
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_reunion" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_reunion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_salle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "listes_de_diffusion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "participants" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_fichier_reunion_cr_global_signe" existe dans "${values}" on execute "Add File" dans le formulaire
    Si "om_fichier_reunion_cr_par_dossier_signe" existe dans "${values}" on execute "Add File" dans le formulaire


Récupérer les uid des champs fichier de la réunion
    [Documentation]  Retourne un dictionnaire contenant les valeurs des champs
    ...  fichier de la réunion dont le code est passé en paramètre.
    [Arguments]  ${reunion_code}
    Depuis le contexte de la réunion  ${reunion_code}
    ${om_fichier_reunion_odj} =  Get Value  css=#om_fichier_reunion_odj
    ${om_fichier_reunion_cr_global} =  Get Value  css=#om_fichier_reunion_cr_global
    ${om_fichier_reunion_cr_global_signe} =  Get Value  css=#om_fichier_reunion_cr_global_signe
    ${om_fichier_reunion_cr_par_dossier_signe} =  Get Value  css=#om_fichier_reunion_cr_par_dossier_signe
    ${reunion_fichiers_uid} =  Create Dictionary
    ...  om_fichier_reunion_odj=${om_fichier_reunion_odj}
    ...  om_fichier_reunion_cr_global=${om_fichier_reunion_cr_global}
    ...  om_fichier_reunion_cr_global_signe=${om_fichier_reunion_cr_global_signe}
    ...  om_fichier_reunion_cr_par_dossier_signe=${om_fichier_reunion_cr_par_dossier_signe}
    [Return]  ${reunion_fichiers_uid}


Depuis l'interface de gestion des documents signés de la réunion
    [Arguments]  ${reunion_code}
    Depuis le contexte de la réunion  ${reunion_code}
    Click On Form Portlet Action  reunion  integrer-documents-numerises

