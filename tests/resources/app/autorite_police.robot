*** Settings ***
Documentation     Actions spécifiques à l'autorité de police

*** Keywords ***
Depuis le contexte de l'autorité de police de la demande de passage

    [Documentation]    Permet d'accéder à l'autorité de police dans la réunion.

    [Arguments]

    Comment    @todo Écrire le 'Keyword'


Depuis le contexte de l'autorité de police non notifiée ou exécutée

    [Documentation]    Permet d'accéder à l'autorité de police depuis le menu
    ...    des autorités de police non notifiées ou exécutées.

    [Arguments]    ${autorite_police}

    # On clique sur le tableau de bord
    Go To Dashboard
    # On ouvre le menu
    Go To Submenu In Menu    dossiers    autorite_police_non_notifiee_executee
    # On vérifie le fil d'Ariane
    Page Title Should Be    Dossiers > Autorité De Police > AP Non Notifiées Ou Exécutées
    # On fait une recherche sur le code barres
    Use Simple Search    autorité de police    ${autorite_police}
    # On accède à la visualisation du document généré
    Click Element    css=a#action-tab-autorite_police_non_notifiee_executee-left-consulter-${autorite_police}
    #
    Page Title Should Contain    ${autorite_police}


Ajouter l'autorité de police depuis la demande de passage (retour d'avis) dans le contexte de la réunion

    [Documentation]    Ajoute l'autorité de police depuis une demande de passage
    ...    en réunion dans une réunion.

    [Arguments]

    Comment    @todo Écrire le 'Keyword'


Saisir l'autorité de police

    [Documentation]    Saisit les champs du formulaire de l'autorité de police.

    [Arguments]    ${params}    ${autorite_police_decision}    ${date_decision}    ${delai}    ${autorite_police_motif}    ${service}    ${cloture}=false    ${date_notification}=null    ${date_butoir}=null

    # On sélectionne la décision
    Select From List By Label    css=#autorite_police_decision    ${autorite_police_decision}
    # On saisit la date de décision
    #Input Datepicker    date_decision    ${date_decision}
    # On saisit le délai
    Input Text    css=#delai    ${delai}
    # On sélectionne le motif
    Select From List By Label    css=#autorite_police_motif    ${autorite_police_motif}
    # On coche/décoche la clôture
    Run keyword If    '${cloture}' == 'true'    Select Checkbox    css=#cloture
    Run keyword If    '${cloture}' == 'false'    Unselect Checkbox    css=#cloture
    # On saisit la date de notification
    Run keyword If    '${date_notification}' != 'null'    Input Datepicker    date_notification    ${date_notitifcation}
    # On saisit la date de butoir
    Run keyword If    '${date_butoir}' != 'null'    Input Datepicker    date_butoir    ${date_butoir}
    # On sélectionne les courriers liés
    ${courriers_lies} =    Get From Dictionary    ${params}    courriers_lies
    @{courriers_lies} =    Convert To List    ${courriers_lies}
    Select From List By Label    css=#courriers_lies    @{courriers_lies}

