*** Settings ***
Documentation    CRUD de la table reunion_avis
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte avis de réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_avis}
    
    # On accède au tableau
    Go To Tab  reunion_avis
    # On recherche l'enregistrement
    Use Simple Search  avis de réunion  ${reunion_avis}
    # On clique sur le résultat
    Click On Link  ${reunion_avis}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter avis de réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_avis
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir avis de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_avis} =  Get Text  css=div.form-content span#reunion_avis
    # On le retourne
    [Return]  ${reunion_avis}

Modifier avis de réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_avis}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte avis de réunion  ${reunion_avis}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_avis  modifier
    # On saisit des valeurs
    Saisir avis de réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer avis de réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_avis}

    # On accède à l'enregistrement
    Depuis le contexte avis de réunion  ${reunion_avis}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_avis  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir avis de réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire