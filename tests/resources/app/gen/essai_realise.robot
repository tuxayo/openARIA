*** Settings ***
Documentation    CRUD de la table essai_realise
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte essai réalisé
    [Documentation]  Accède au formulaire
    [Arguments]  ${essai_realise}
    
    # On accède au tableau
    Go To Tab  essai_realise
    # On recherche l'enregistrement
    Use Simple Search  essai réalisé  ${essai_realise}
    # On clique sur le résultat
    Click On Link  ${essai_realise}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter essai réalisé
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  essai_realise
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir essai réalisé  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${essai_realise} =  Get Text  css=div.form-content span#essai_realise
    # On le retourne
    [Return]  ${essai_realise}

Modifier essai réalisé
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${essai_realise}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte essai réalisé  ${essai_realise}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  essai_realise  modifier
    # On saisit des valeurs
    Saisir essai réalisé  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer essai réalisé
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${essai_realise}

    # On accède à l'enregistrement
    Depuis le contexte essai réalisé  ${essai_realise}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  essai_realise  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir essai réalisé
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire