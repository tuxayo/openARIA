*** Settings ***
Documentation    CRUD de la table autorite_police_decision
...    @author  generated
...    @package openARIA
...    @version 31/08/2016 08:08

*** Keywords ***

Depuis le contexte décision
    [Documentation]  Accède au formulaire
    [Arguments]  ${autorite_police_decision}
    
    # On accède au tableau
    Go To Tab  autorite_police_decision
    # On recherche l'enregistrement
    Use Simple Search  décision  ${autorite_police_decision}
    # On clique sur le résultat
    Click On Link  ${autorite_police_decision}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter décision
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  autorite_police_decision
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir décision  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${autorite_police_decision} =  Get Text  css=div.form-content span#autorite_police_decision
    # On le retourne
    [Return]  ${autorite_police_decision}

Modifier décision
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${autorite_police_decision}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte décision  ${autorite_police_decision}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  autorite_police_decision  modifier
    # On saisit des valeurs
    Saisir décision  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer décision
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${autorite_police_decision}

    # On accède à l'enregistrement
    Depuis le contexte décision  ${autorite_police_decision}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  autorite_police_decision  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir décision
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "suivi_delai" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "avis" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "delai" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "type_arrete" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "arrete_reglementaire" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "arrete_notification" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "arrete_publication" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "arrete_temporaire" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "nomenclature_actes_nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nomenclature_actes_matiere_niv1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "nomenclature_actes_matiere_niv2" existe dans "${values}" on execute "Input Text" dans le formulaire