*** Settings ***
Documentation    CRUD de la table reunion_type_reunion_avis
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte reunion_type_reunion_avis
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_type_reunion_avis}
    
    # On accède au tableau
    Go To Tab  reunion_type_reunion_avis
    # On recherche l'enregistrement
    Use Simple Search  reunion_type_reunion_avis  ${reunion_type_reunion_avis}
    # On clique sur le résultat
    Click On Link  ${reunion_type_reunion_avis}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter reunion_type_reunion_avis
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_type_reunion_avis
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir reunion_type_reunion_avis  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_type_reunion_avis} =  Get Text  css=div.form-content span#reunion_type_reunion_avis
    # On le retourne
    [Return]  ${reunion_type_reunion_avis}

Modifier reunion_type_reunion_avis
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_type_reunion_avis}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte reunion_type_reunion_avis  ${reunion_type_reunion_avis}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_type_reunion_avis  modifier
    # On saisit des valeurs
    Saisir reunion_type_reunion_avis  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer reunion_type_reunion_avis
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_type_reunion_avis}

    # On accède à l'enregistrement
    Depuis le contexte reunion_type_reunion_avis  ${reunion_type_reunion_avis}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_type_reunion_avis  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir reunion_type_reunion_avis
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire