*** Settings ***
Documentation    CRUD de la table lien_dossier_coordination_etablissement_type
...    @author  generated
...    @package openARIA
...    @version 08/09/2016 14:09

*** Keywords ***

Depuis le contexte lien DC/type établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_dossier_coordination_etablissement_type}
    
    # On accède au tableau
    Go To Tab  lien_dossier_coordination_etablissement_type
    # On recherche l'enregistrement
    Use Simple Search  lien DC/type établissement  ${lien_dossier_coordination_etablissement_type}
    # On clique sur le résultat
    Click On Link  ${lien_dossier_coordination_etablissement_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien DC/type établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_dossier_coordination_etablissement_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien DC/type établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_dossier_coordination_etablissement_type} =  Get Text  css=div.form-content span#lien_dossier_coordination_etablissement_type
    # On le retourne
    [Return]  ${lien_dossier_coordination_etablissement_type}

Modifier lien DC/type établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_dossier_coordination_etablissement_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien DC/type établissement  ${lien_dossier_coordination_etablissement_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_dossier_coordination_etablissement_type  modifier
    # On saisit des valeurs
    Saisir lien DC/type établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien DC/type établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_dossier_coordination_etablissement_type}

    # On accède à l'enregistrement
    Depuis le contexte lien DC/type établissement  ${lien_dossier_coordination_etablissement_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_dossier_coordination_etablissement_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien DC/type établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_coordination" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire