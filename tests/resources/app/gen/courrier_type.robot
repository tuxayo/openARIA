*** Settings ***
Documentation    CRUD de la table courrier_type
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte type du courrier
    [Documentation]  Accède au formulaire
    [Arguments]  ${courrier_type}
    
    # On accède au tableau
    Go To Tab  courrier_type
    # On recherche l'enregistrement
    Use Simple Search  type du courrier  ${courrier_type}
    # On clique sur le résultat
    Click On Link  ${courrier_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type du courrier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  courrier_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type du courrier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${courrier_type} =  Get Text  css=div.form-content span#courrier_type
    # On le retourne
    [Return]  ${courrier_type}

Modifier type du courrier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${courrier_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type du courrier  ${courrier_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  courrier_type  modifier
    # On saisit des valeurs
    Saisir type du courrier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type du courrier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${courrier_type}

    # On accède à l'enregistrement
    Depuis le contexte type du courrier  ${courrier_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  courrier_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type du courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "courrier_type_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire