*** Settings ***
Documentation    CRUD de la table dossier_coordination_type
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte type de dossier de coordination
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_coordination_type}
    
    # On accède au tableau
    Go To Tab  dossier_coordination_type
    # On recherche l'enregistrement
    Use Simple Search  type de dossier de coordination  ${dossier_coordination_type}
    # On clique sur le résultat
    Click On Link  ${dossier_coordination_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type de dossier de coordination
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_coordination_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type de dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_coordination_type} =  Get Text  css=div.form-content span#dossier_coordination_type
    # On le retourne
    [Return]  ${dossier_coordination_type}

Modifier type de dossier de coordination
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_coordination_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type de dossier de coordination  ${dossier_coordination_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_coordination_type  modifier
    # On saisit des valeurs
    Saisir type de dossier de coordination  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type de dossier de coordination
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_coordination_type}

    # On accède à l'enregistrement
    Depuis le contexte type de dossier de coordination  ${dossier_coordination_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_coordination_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type de dossier de coordination
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "dossier_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction_secu" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dossier_instruction_acc" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "a_qualifier" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "analyses_type_si" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "analyses_type_acc" existe dans "${values}" on execute "Select From List By Label" dans le formulaire