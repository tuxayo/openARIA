*** Settings ***
Documentation    CRUD de la table analyses
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte analyses
    [Documentation]  Accède au formulaire
    [Arguments]  ${analyses}
    
    # On accède au tableau
    Go To Tab  analyses
    # On recherche l'enregistrement
    Use Simple Search  analyses  ${analyses}
    # On clique sur le résultat
    Click On Link  ${analyses}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter analyses
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  analyses
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir analyses  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${analyses} =  Get Text  css=div.form-content span#analyses
    # On le retourne
    [Return]  ${analyses}

Modifier analyses
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${analyses}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte analyses  ${analyses}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  analyses  modifier
    # On saisit des valeurs
    Saisir analyses  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer analyses
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${analyses}

    # On accède à l'enregistrement
    Depuis le contexte analyses  ${analyses}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  analyses  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir analyses
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "analyses_etat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "analyses_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "objet" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "descriptif_etablissement_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "reglementation_applicable_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "compte_rendu_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "document_presente_pendant_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "document_presente_apres_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "observation_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "reunion_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "avis_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_effectif_public" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_effectif_personnel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_type_ssi" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_type_alarme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_conformite_l16" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_alimentation_remplacement" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_service_securite" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_personnel_jour" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_personnel_nuit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_handicap_mental" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_auditif" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_places_stationnement_amenagees" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_elevateur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_physique" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_ascenseur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_visuel" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_boucle_magnetique" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_chambres_amenagees" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_douche" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_derogation_scda" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_sanitaire" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_places_assises_public" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition_rapport" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition_compte_rendu" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition_proces_verbal" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modifiee_sans_gen" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dernier_pv" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dec1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "delai1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dec2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "delai2" existe dans "${values}" on execute "Input Text" dans le formulaire