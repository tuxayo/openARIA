*** Settings ***
Documentation    CRUD de la table reglementation_applicable
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte réglementation applicable
    [Documentation]  Accède au formulaire
    [Arguments]  ${reglementation_applicable}
    
    # On accède au tableau
    Go To Tab  reglementation_applicable
    # On recherche l'enregistrement
    Use Simple Search  réglementation applicable  ${reglementation_applicable}
    # On clique sur le résultat
    Click On Link  ${reglementation_applicable}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter réglementation applicable
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reglementation_applicable
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir réglementation applicable  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reglementation_applicable} =  Get Text  css=div.form-content span#reglementation_applicable
    # On le retourne
    [Return]  ${reglementation_applicable}

Modifier réglementation applicable
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reglementation_applicable}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte réglementation applicable  ${reglementation_applicable}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reglementation_applicable  modifier
    # On saisit des valeurs
    Saisir réglementation applicable  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer réglementation applicable
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reglementation_applicable}

    # On accède à l'enregistrement
    Depuis le contexte réglementation applicable  ${reglementation_applicable}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reglementation_applicable  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir réglementation applicable
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "annee_debut_application" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annee_fin_application" existe dans "${values}" on execute "Input Text" dans le formulaire