*** Settings ***
Documentation    CRUD de la table lien_prescription_reglementaire_etablissement_categorie
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte lien prescription réglementaire / catégorie établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_prescription_reglementaire_etablissement_categorie}
    
    # On accède au tableau
    Go To Tab  lien_prescription_reglementaire_etablissement_categorie
    # On recherche l'enregistrement
    Use Simple Search  lien prescription réglementaire / catégorie établissement  ${lien_prescription_reglementaire_etablissement_categorie}
    # On clique sur le résultat
    Click On Link  ${lien_prescription_reglementaire_etablissement_categorie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien prescription réglementaire / catégorie établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_prescription_reglementaire_etablissement_categorie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien prescription réglementaire / catégorie établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_prescription_reglementaire_etablissement_categorie} =  Get Text  css=div.form-content span#lien_prescription_reglementaire_etablissement_categorie
    # On le retourne
    [Return]  ${lien_prescription_reglementaire_etablissement_categorie}

Modifier lien prescription réglementaire / catégorie établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_prescription_reglementaire_etablissement_categorie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien prescription réglementaire / catégorie établissement  ${lien_prescription_reglementaire_etablissement_categorie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_prescription_reglementaire_etablissement_categorie  modifier
    # On saisit des valeurs
    Saisir lien prescription réglementaire / catégorie établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien prescription réglementaire / catégorie établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_prescription_reglementaire_etablissement_categorie}

    # On accède à l'enregistrement
    Depuis le contexte lien prescription réglementaire / catégorie établissement  ${lien_prescription_reglementaire_etablissement_categorie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_prescription_reglementaire_etablissement_categorie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien prescription réglementaire / catégorie établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "prescription_reglementaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire