*** Settings ***
Documentation    CRUD de la table etablissement
...    @author  generated
...    @package openARIA
...    @version 18/09/2016 22:09

*** Keywords ***

Depuis le contexte établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement}
    
    # On accède au tableau
    Go To Tab  etablissement
    # On recherche l'enregistrement
    Use Simple Search  établissement  ${etablissement}
    # On clique sur le résultat
    Click On Link  ${etablissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement} =  Get Text  css=div.form-content span#etablissement
    # On le retourne
    [Return]  ${etablissement}

Modifier établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte établissement  ${etablissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement  modifier
    # On saisit des valeurs
    Saisir établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement}

    # On accède à l'enregistrement
    Depuis le contexte établissement  ${etablissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_numero2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_voie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "adresse_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_dit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "boite_postale" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_cp" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_ville" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adresse_arrondissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "cedex" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "npai" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "telephone" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "fax" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_nature" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "siret" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "annee_de_construction" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_statut_juridique" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_tutelle_adm" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ref_patrimoine" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_etat" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_arrete_ouverture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "autorite_police_encours" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "si_effectif_public" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_effectif_personnel" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_locaux_sommeil" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_periodicite_visites" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_prochaine_visite_periodique_date_previsionnelle" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "si_visite_duree" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_derniere_visite_periodique_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "si_derniere_visite_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "si_derniere_visite_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_derniere_visite_technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_prochaine_visite_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "si_prochaine_visite_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_derniere_visite_date" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "acc_derniere_visite_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_derniere_visite_technicien" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_consignes_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "acc_descriptif_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_consignes_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_descriptif_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "si_autorite_competente_visite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_autorite_competente_plan" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_dernier_plan_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "si_type_alarme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_type_ssi" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_conformite_l16" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_alimentation_remplacement" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_service_securite" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "si_personnel_jour" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "si_personnel_nuit" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "references_cadastrales" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_coordination_periodique" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "geolocalise" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "geom_point" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "geom_emprise" existe dans "${values}" on execute "Input Text" dans le formulaire