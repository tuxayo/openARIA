*** Settings ***
Documentation    CRUD de la table dossier_instruction_reunion
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte Demande de passage en réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${dossier_instruction_reunion}
    
    # On accède au tableau
    Go To Tab  dossier_instruction_reunion
    # On recherche l'enregistrement
    Use Simple Search  Demande de passage en réunion  ${dossier_instruction_reunion}
    # On clique sur le résultat
    Click On Link  ${dossier_instruction_reunion}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter Demande de passage en réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  dossier_instruction_reunion
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir Demande de passage en réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${dossier_instruction_reunion} =  Get Text  css=div.form-content span#dossier_instruction_reunion
    # On le retourne
    [Return]  ${dossier_instruction_reunion}

Modifier Demande de passage en réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${dossier_instruction_reunion}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte Demande de passage en réunion  ${dossier_instruction_reunion}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  dossier_instruction_reunion  modifier
    # On saisit des valeurs
    Saisir Demande de passage en réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer Demande de passage en réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${dossier_instruction_reunion}

    # On accède à l'enregistrement
    Depuis le contexte Demande de passage en réunion  ${dossier_instruction_reunion}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  dossier_instruction_reunion  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir Demande de passage en réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "reunion_type_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_souhaitee" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "motivation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "proposition_avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "proposition_avis_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reunion" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ordre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "avis" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "avis_complement" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "avis_motivation" existe dans "${values}" on execute "Input Text" dans le formulaire