*** Settings ***
Documentation    CRUD de la table lien_autorite_police_courrier
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte lien autorité de police / courrier
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_autorite_police_courrier}
    
    # On accède au tableau
    Go To Tab  lien_autorite_police_courrier
    # On recherche l'enregistrement
    Use Simple Search  lien autorité de police / courrier  ${lien_autorite_police_courrier}
    # On clique sur le résultat
    Click On Link  ${lien_autorite_police_courrier}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien autorité de police / courrier
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_autorite_police_courrier
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien autorité de police / courrier  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_autorite_police_courrier} =  Get Text  css=div.form-content span#lien_autorite_police_courrier
    # On le retourne
    [Return]  ${lien_autorite_police_courrier}

Modifier lien autorité de police / courrier
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_autorite_police_courrier}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien autorité de police / courrier  ${lien_autorite_police_courrier}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_autorite_police_courrier  modifier
    # On saisit des valeurs
    Saisir lien autorité de police / courrier  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien autorité de police / courrier
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_autorite_police_courrier}

    # On accède à l'enregistrement
    Depuis le contexte lien autorité de police / courrier  ${lien_autorite_police_courrier}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_autorite_police_courrier  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien autorité de police / courrier
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "autorite_police" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "courrier" existe dans "${values}" on execute "Select From List By Label" dans le formulaire