*** Settings ***
Documentation    CRUD de la table prescription_specifique
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte prescription spécifique
    [Documentation]  Accède au formulaire
    [Arguments]  ${prescription_specifique}
    
    # On accède au tableau
    Go To Tab  prescription_specifique
    # On recherche l'enregistrement
    Use Simple Search  prescription spécifique  ${prescription_specifique}
    # On clique sur le résultat
    Click On Link  ${prescription_specifique}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter prescription spécifique
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  prescription_specifique
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir prescription spécifique  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${prescription_specifique} =  Get Text  css=div.form-content span#prescription_specifique
    # On le retourne
    [Return]  ${prescription_specifique}

Modifier prescription spécifique
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${prescription_specifique}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte prescription spécifique  ${prescription_specifique}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  prescription_specifique  modifier
    # On saisit des valeurs
    Saisir prescription spécifique  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer prescription spécifique
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${prescription_specifique}

    # On accède à l'enregistrement
    Depuis le contexte prescription spécifique  ${prescription_specifique}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  prescription_specifique  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir prescription spécifique
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description_ps_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "prescription_reglementaire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire