*** Settings ***
Documentation    CRUD de la table etablissement_parcelle
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte parcelles
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_parcelle}
    
    # On accède au tableau
    Go To Tab  etablissement_parcelle
    # On recherche l'enregistrement
    Use Simple Search  parcelles  ${etablissement_parcelle}
    # On clique sur le résultat
    Click On Link  ${etablissement_parcelle}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter parcelles
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_parcelle
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir parcelles  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_parcelle} =  Get Text  css=div.form-content span#etablissement_parcelle
    # On le retourne
    [Return]  ${etablissement_parcelle}

Modifier parcelles
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_parcelle}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte parcelles  ${etablissement_parcelle}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_parcelle  modifier
    # On saisit des valeurs
    Saisir parcelles  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer parcelles
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_parcelle}

    # On accède à l'enregistrement
    Depuis le contexte parcelles  ${etablissement_parcelle}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_parcelle  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir parcelles
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "ref_cadastre" existe dans "${values}" on execute "Input Text" dans le formulaire