*** Settings ***
Documentation    CRUD de la table voie
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte voie
    [Documentation]  Accède au formulaire
    [Arguments]  ${voie}
    
    # On accède au tableau
    Go To Tab  voie
    # On recherche l'enregistrement
    Use Simple Search  voie  ${voie}
    # On clique sur le résultat
    Click On Link  ${voie}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter voie
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  voie
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${voie} =  Get Text  css=div.form-content span#voie
    # On le retourne
    [Return]  ${voie}

Modifier voie
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${voie}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${voie}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  voie  modifier
    # On saisit des valeurs
    Saisir voie  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer voie
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${voie}

    # On accède à l'enregistrement
    Depuis le contexte voie  ${voie}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  voie  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir voie
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "rivoli" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_collectivite" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "id_voie_ref" existe dans "${values}" on execute "Input Text" dans le formulaire