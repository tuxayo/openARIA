*** Settings ***
Documentation    CRUD de la table reunion
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte réunion
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion}
    
    # On accède au tableau
    Go To Tab  reunion
    # On recherche l'enregistrement
    Use Simple Search  réunion  ${reunion}
    # On clique sur le résultat
    Click On Link  ${reunion}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter réunion
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir réunion  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion} =  Get Text  css=div.form-content span#reunion
    # On le retourne
    [Return]  ${reunion}

Modifier réunion
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte réunion  ${reunion}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion  modifier
    # On saisit des valeurs
    Saisir réunion  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer réunion
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion}

    # On accède à l'enregistrement
    Depuis le contexte réunion  ${reunion}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir réunion
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reunion_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_reunion" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "heure_reunion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne1" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_adresse_ligne2" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "lieu_salle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "listes_de_diffusion" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "participants" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "numerotation" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "date_convocation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "reunion_cloture" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "date_cloture" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_fichier_reunion_odj" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_final_reunion_odj" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_fichier_reunion_cr_global" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_final_reunion_cr_global" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_fichier_reunion_cr_global_signe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_fichier_reunion_cr_par_dossier_signe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "planifier_nouveau" existe dans "${values}" on execute "Set Checkbox" dans le formulaire