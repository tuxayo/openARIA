*** Settings ***
Documentation    CRUD de la table etablissement_nature
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte nature
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_nature}
    
    # On accède au tableau
    Go To Tab  etablissement_nature
    # On recherche l'enregistrement
    Use Simple Search  nature  ${etablissement_nature}
    # On clique sur le résultat
    Click On Link  ${etablissement_nature}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter nature
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_nature
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir nature  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_nature} =  Get Text  css=div.form-content span#etablissement_nature
    # On le retourne
    [Return]  ${etablissement_nature}

Modifier nature
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_nature}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte nature  ${etablissement_nature}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_nature  modifier
    # On saisit des valeurs
    Saisir nature  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer nature
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_nature}

    # On accède à l'enregistrement
    Depuis le contexte nature  ${etablissement_nature}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_nature  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir nature
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nature" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire