*** Settings ***
Documentation    CRUD de la table etablissement_type
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte type
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_type}
    
    # On accède au tableau
    Go To Tab  etablissement_type
    # On recherche l'enregistrement
    Use Simple Search  type  ${etablissement_type}
    # On clique sur le résultat
    Click On Link  ${etablissement_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_type} =  Get Text  css=div.form-content span#etablissement_type
    # On le retourne
    [Return]  ${etablissement_type}

Modifier type
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type  ${etablissement_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_type  modifier
    # On saisit des valeurs
    Saisir type  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_type}

    # On accède à l'enregistrement
    Depuis le contexte type  ${etablissement_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire