*** Settings ***
Documentation    CRUD de la table etablissement_unite
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte unité d'accessibilité
    [Documentation]  Accède au formulaire
    [Arguments]  ${etablissement_unite}
    
    # On accède au tableau
    Go To Tab  etablissement_unite
    # On recherche l'enregistrement
    Use Simple Search  unité d'accessibilité  ${etablissement_unite}
    # On clique sur le résultat
    Click On Link  ${etablissement_unite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter unité d'accessibilité
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  etablissement_unite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir unité d'accessibilité  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${etablissement_unite} =  Get Text  css=div.form-content span#etablissement_unite
    # On le retourne
    [Return]  ${etablissement_unite}

Modifier unité d'accessibilité
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${etablissement_unite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte unité d'accessibilité  ${etablissement_unite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  etablissement_unite  modifier
    # On saisit des valeurs
    Saisir unité d'accessibilité  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer unité d'accessibilité
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${etablissement_unite}

    # On accède à l'enregistrement
    Depuis le contexte unité d'accessibilité  ${etablissement_unite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  etablissement_unite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir unité d'accessibilité
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_notes_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "acc_descriptif_ua_om_html" existe dans "${values}" on execute "Input HTML" dans le formulaire
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "acc_handicap_physique" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_auditif" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_visuel" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_handicap_mental" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_places_stationnement_amenagees" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_ascenseur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_elevateur" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_boucle_magnetique" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_sanitaire" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_places_assises_public" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_chambres_amenagees" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acc_douche" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "acc_derogation_scda" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etat" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "archive" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_unite_lie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "adap_date_validation" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "adap_duree_validite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adap_annee_debut_travaux" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "adap_annee_fin_travaux" existe dans "${values}" on execute "Input Text" dans le formulaire