*** Settings ***
Documentation    CRUD de la table contact_civilite
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte civilité
    [Documentation]  Accède au formulaire
    [Arguments]  ${contact_civilite}
    
    # On accède au tableau
    Go To Tab  contact_civilite
    # On recherche l'enregistrement
    Use Simple Search  civilité  ${contact_civilite}
    # On clique sur le résultat
    Click On Link  ${contact_civilite}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter civilité
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  contact_civilite
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir civilité  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${contact_civilite} =  Get Text  css=div.form-content span#contact_civilite
    # On le retourne
    [Return]  ${contact_civilite}

Modifier civilité
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${contact_civilite}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte civilité  ${contact_civilite}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  contact_civilite  modifier
    # On saisit des valeurs
    Saisir civilité  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer civilité
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${contact_civilite}

    # On accède à l'enregistrement
    Depuis le contexte civilité  ${contact_civilite}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  contact_civilite  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir civilité
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire