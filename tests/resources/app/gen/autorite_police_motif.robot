*** Settings ***
Documentation    CRUD de la table autorite_police_motif
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte motif
    [Documentation]  Accède au formulaire
    [Arguments]  ${autorite_police_motif}
    
    # On accède au tableau
    Go To Tab  autorite_police_motif
    # On recherche l'enregistrement
    Use Simple Search  motif  ${autorite_police_motif}
    # On clique sur le résultat
    Click On Link  ${autorite_police_motif}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter motif
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  autorite_police_motif
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir motif  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${autorite_police_motif} =  Get Text  css=div.form-content span#autorite_police_motif
    # On le retourne
    [Return]  ${autorite_police_motif}

Modifier motif
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${autorite_police_motif}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte motif  ${autorite_police_motif}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  autorite_police_motif  modifier
    # On saisit des valeurs
    Saisir motif  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer motif
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${autorite_police_motif}

    # On accède à l'enregistrement
    Depuis le contexte motif  ${autorite_police_motif}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  autorite_police_motif  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir motif
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire