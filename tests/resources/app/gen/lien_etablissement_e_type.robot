*** Settings ***
Documentation    CRUD de la table lien_etablissement_e_type
...    @author  generated
...    @package openARIA
...    @version 08/09/2016 14:09

*** Keywords ***

Depuis le contexte lien établissement / types secondaires
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_etablissement_e_type}
    
    # On accède au tableau
    Go To Tab  lien_etablissement_e_type
    # On recherche l'enregistrement
    Use Simple Search  lien établissement / types secondaires  ${lien_etablissement_e_type}
    # On clique sur le résultat
    Click On Link  ${lien_etablissement_e_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien établissement / types secondaires
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_etablissement_e_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien établissement / types secondaires  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_etablissement_e_type} =  Get Text  css=div.form-content span#lien_etablissement_e_type
    # On le retourne
    [Return]  ${lien_etablissement_e_type}

Modifier lien établissement / types secondaires
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_etablissement_e_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien établissement / types secondaires  ${lien_etablissement_e_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_etablissement_e_type  modifier
    # On saisit des valeurs
    Saisir lien établissement / types secondaires  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien établissement / types secondaires
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_etablissement_e_type}

    # On accède à l'enregistrement
    Depuis le contexte lien établissement / types secondaires  ${lien_etablissement_e_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_etablissement_e_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien établissement / types secondaires
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire