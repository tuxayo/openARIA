*** Settings ***
Documentation    CRUD de la table lien_contrainte_etablissement
...    @author  generated
...    @package openARIA
...    @version 08/09/2016 14:09

*** Keywords ***

Depuis le contexte lien contrainte/établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_contrainte_etablissement}
    
    # On accède au tableau
    Go To Tab  lien_contrainte_etablissement
    # On recherche l'enregistrement
    Use Simple Search  lien contrainte/établissement  ${lien_contrainte_etablissement}
    # On clique sur le résultat
    Click On Link  ${lien_contrainte_etablissement}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien contrainte/établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_contrainte_etablissement
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien contrainte/établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_contrainte_etablissement} =  Get Text  css=div.form-content span#lien_contrainte_etablissement
    # On le retourne
    [Return]  ${lien_contrainte_etablissement}

Modifier lien contrainte/établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_contrainte_etablissement}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien contrainte/établissement  ${lien_contrainte_etablissement}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_contrainte_etablissement  modifier
    # On saisit des valeurs
    Saisir lien contrainte/établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien contrainte/établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_contrainte_etablissement}

    # On accède à l'enregistrement
    Depuis le contexte lien contrainte/établissement  ${lien_contrainte_etablissement}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_contrainte_etablissement  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien contrainte/établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "etablissement" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "contrainte" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "texte_complete" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "recuperee" existe dans "${values}" on execute "Set Checkbox" dans le formulaire