*** Settings ***
Documentation    CRUD de la table analyses_type
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte type d'analyse
    [Documentation]  Accède au formulaire
    [Arguments]  ${analyses_type}
    
    # On accède au tableau
    Go To Tab  analyses_type
    # On recherche l'enregistrement
    Use Simple Search  type d'analyse  ${analyses_type}
    # On clique sur le résultat
    Click On Link  ${analyses_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter type d'analyse
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  analyses_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir type d'analyse  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${analyses_type} =  Get Text  css=div.form-content span#analyses_type
    # On le retourne
    [Return]  ${analyses_type}

Modifier type d'analyse
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${analyses_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte type d'analyse  ${analyses_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  analyses_type  modifier
    # On saisit des valeurs
    Saisir type d'analyse  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer type d'analyse
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${analyses_type}

    # On accède à l'enregistrement
    Depuis le contexte type d'analyse  ${analyses_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  analyses_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir type d'analyse
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "code" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "libelle" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "modele_edition_rapport" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition_compte_rendu" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition_proces_verbal" existe dans "${values}" on execute "Select From List By Label" dans le formulaire