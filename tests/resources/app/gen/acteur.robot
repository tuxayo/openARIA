*** Settings ***
Documentation    CRUD de la table acteur
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte acteur
    [Documentation]  Accède au formulaire
    [Arguments]  ${acteur}
    
    # On accède au tableau
    Go To Tab  acteur
    # On recherche l'enregistrement
    Use Simple Search  acteur  ${acteur}
    # On clique sur le résultat
    Click On Link  ${acteur}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter acteur
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  acteur
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir acteur  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${acteur} =  Get Text  css=div.form-content span#acteur
    # On le retourne
    [Return]  ${acteur}

Modifier acteur
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${acteur}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte acteur  ${acteur}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  acteur  modifier
    # On saisit des valeurs
    Saisir acteur  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer acteur
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${acteur}

    # On accède à l'enregistrement
    Depuis le contexte acteur  ${acteur}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  acteur  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir acteur
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "nom_prenom" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_utilisateur" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "service" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "role" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "acronyme" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "couleur" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "reference" existe dans "${values}" on execute "Input Text" dans le formulaire