*** Settings ***
Documentation    CRUD de la table lien_reglementation_applicable_etablissement_type
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte lien réglementation applicable / type d'établissement
    [Documentation]  Accède au formulaire
    [Arguments]  ${lien_reglementation_applicable_etablissement_type}
    
    # On accède au tableau
    Go To Tab  lien_reglementation_applicable_etablissement_type
    # On recherche l'enregistrement
    Use Simple Search  lien réglementation applicable / type d'établissement  ${lien_reglementation_applicable_etablissement_type}
    # On clique sur le résultat
    Click On Link  ${lien_reglementation_applicable_etablissement_type}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter lien réglementation applicable / type d'établissement
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  lien_reglementation_applicable_etablissement_type
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir lien réglementation applicable / type d'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${lien_reglementation_applicable_etablissement_type} =  Get Text  css=div.form-content span#lien_reglementation_applicable_etablissement_type
    # On le retourne
    [Return]  ${lien_reglementation_applicable_etablissement_type}

Modifier lien réglementation applicable / type d'établissement
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${lien_reglementation_applicable_etablissement_type}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte lien réglementation applicable / type d'établissement  ${lien_reglementation_applicable_etablissement_type}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  lien_reglementation_applicable_etablissement_type  modifier
    # On saisit des valeurs
    Saisir lien réglementation applicable / type d'établissement  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer lien réglementation applicable / type d'établissement
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${lien_reglementation_applicable_etablissement_type}

    # On accède à l'enregistrement
    Depuis le contexte lien réglementation applicable / type d'établissement  ${lien_reglementation_applicable_etablissement_type}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  lien_reglementation_applicable_etablissement_type  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir lien réglementation applicable / type d'établissement
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "reglementation_applicable" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire