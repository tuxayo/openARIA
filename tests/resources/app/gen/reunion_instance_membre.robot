*** Settings ***
Documentation    CRUD de la table reunion_instance_membre
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte reunion_instance_membre
    [Documentation]  Accède au formulaire
    [Arguments]  ${reunion_instance_membre}
    
    # On accède au tableau
    Go To Tab  reunion_instance_membre
    # On recherche l'enregistrement
    Use Simple Search  reunion_instance_membre  ${reunion_instance_membre}
    # On clique sur le résultat
    Click On Link  ${reunion_instance_membre}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter reunion_instance_membre
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  reunion_instance_membre
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir reunion_instance_membre  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${reunion_instance_membre} =  Get Text  css=div.form-content span#reunion_instance_membre
    # On le retourne
    [Return]  ${reunion_instance_membre}

Modifier reunion_instance_membre
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${reunion_instance_membre}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte reunion_instance_membre  ${reunion_instance_membre}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  reunion_instance_membre  modifier
    # On saisit des valeurs
    Saisir reunion_instance_membre  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer reunion_instance_membre
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${reunion_instance_membre}

    # On accède à l'enregistrement
    Depuis le contexte reunion_instance_membre  ${reunion_instance_membre}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  reunion_instance_membre  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir reunion_instance_membre
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "membre" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "description" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "reunion_instance" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "om_validite_debut" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "om_validite_fin" existe dans "${values}" on execute "Input Datepicker" dans le formulaire