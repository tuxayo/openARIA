*** Settings ***
Documentation    CRUD de la table periodicite_visites
...    @author  generated
...    @package openARIA
...    @version 22/12/2015 11:12

*** Keywords ***

Depuis le contexte périodicité de visites (ans)
    [Documentation]  Accède au formulaire
    [Arguments]  ${periodicite_visites}
    
    # On accède au tableau
    Go To Tab  periodicite_visites
    # On recherche l'enregistrement
    Use Simple Search  périodicité de visites (ans)  ${periodicite_visites}
    # On clique sur le résultat
    Click On Link  ${periodicite_visites}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter périodicité de visites (ans)
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  periodicite_visites
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir périodicité de visites (ans)  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${periodicite_visites} =  Get Text  css=div.form-content span#periodicite_visites
    # On le retourne
    [Return]  ${periodicite_visites}

Modifier périodicité de visites (ans)
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${periodicite_visites}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte périodicité de visites (ans)  ${periodicite_visites}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  periodicite_visites  modifier
    # On saisit des valeurs
    Saisir périodicité de visites (ans)  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer périodicité de visites (ans)
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${periodicite_visites}

    # On accède à l'enregistrement
    Depuis le contexte périodicité de visites (ans)  ${periodicite_visites}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  periodicite_visites  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir périodicité de visites (ans)
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "periodicite" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "etablissement_type" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "etablissement_categorie" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "commentaire" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "avec_locaux_sommeil" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "sans_locaux_sommeil" existe dans "${values}" on execute "Set Checkbox" dans le formulaire