*** Settings ***
Documentation    CRUD de la table proces_verbal
...    @author  generated
...    @package openARIA
...    @version 16/08/2016 16:08

*** Keywords ***

Depuis le contexte procès-verbal
    [Documentation]  Accède au formulaire
    [Arguments]  ${proces_verbal}
    
    # On accède au tableau
    Go To Tab  proces_verbal
    # On recherche l'enregistrement
    Use Simple Search  procès-verbal  ${proces_verbal}
    # On clique sur le résultat
    Click On Link  ${proces_verbal}
    # On vérifie qu'il n'y a pas d'erreur
    Page Should Not Contain Errors

Ajouter procès-verbal
    [Documentation]  Crée l'enregistrement
    [Arguments]  ${values}

    # On accède au tableau
    Go To Tab  proces_verbal
    # On clique sur le bouton ajouter
    Click On Add Button
    # On saisit des valeurs
    Saisir procès-verbal  ${values}
    # On valide le formulaire
    Click On Submit Button
    # On récupère l'ID du nouvel enregistrement
    ${proces_verbal} =  Get Text  css=div.form-content span#proces_verbal
    # On le retourne
    [Return]  ${proces_verbal}

Modifier procès-verbal
    [Documentation]  Modifie l'enregistrement
    [Arguments]  ${proces_verbal}  ${values}

    # On accède à l'enregistrement
    Depuis le contexte procès-verbal  ${proces_verbal}
    # On clique sur le bouton modifier
    Click On Form Portlet Action  proces_verbal  modifier
    # On saisit des valeurs
    Saisir procès-verbal  ${values}
    # On valide le formulaire
    Click On Submit Button

Supprimer procès-verbal
    [Documentation]  Supprime l'enregistrement
    [Arguments]  ${proces_verbal}

    # On accède à l'enregistrement
    Depuis le contexte procès-verbal  ${proces_verbal}
    # On clique sur le bouton supprimer
    Click On Form Portlet Action  proces_verbal  supprimer
    # On valide le formulaire
    Click On Submit Button

Saisir procès-verbal
    [Documentation]  Remplit le formulaire
    [Arguments]  ${values}
    
    Si "numero" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "dossier_instruction" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "dossier_instruction_reunion" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "modele_edition" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "date_redaction" existe dans "${values}" on execute "Input Datepicker" dans le formulaire
    Si "signataire" existe dans "${values}" on execute "Select From List By Label" dans le formulaire
    Si "genere" existe dans "${values}" on execute "Set Checkbox" dans le formulaire
    Si "om_fichier_signe" existe dans "${values}" on execute "Input Text" dans le formulaire
    Si "courrier_genere" existe dans "${values}" on execute "Select From List By Label" dans le formulaire