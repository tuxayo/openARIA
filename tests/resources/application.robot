*** Settings ***
Documentation     Actions spécifiques à l'application

*** Keywords ***
Supprimer l'établissement
    [Arguments]    ${libelle}
    Go To Dashboard
    Go To Submenu In Menu    etablissements    etablissement_tous

    Input Text    css=#libelle    ${libelle}
    Click On Search Button
    Click On Link    ${libelle}

    Click On Form Portlet Action    etablissement_tous    supprimer
    Click On Submit Button
    Valid Message Should Be    La suppression a été correctement effectuée.


Modifier exploitant depuis la fiche d'un établissement
    [Documentation]    Permet de modifier les données de l'exploitant
    ...    d'un établissement.
    [Arguments]    ${etablissement_code}    ${exp_civilite}    ${exp_nom}    ${exp_prenom}    ${meme_adresse}=true
    # On clique sur le tableau de bord
    Go To Dashboard
    # On accède au menu
    Go To Submenu In Menu    etablissements    etablissement_tous
    # On recherche l'établissement
    Input Text    css=div#adv-search-adv-fields input#etablissement    ${etablissement_code}
    Click On Search Button
    # On clique sur l'établissement
    Click Link    ${etablissement_code}
    # On clique sur le bouton modifier
    Click On Form Portlet Action    etablissement_tous    modifier
    # On sélectionne la civilité
    Select From List By Label    css=#exp_civilite    ${exp_civilite}
    # On saisit le nom
    Input Text    css=#exp_nom    ${exp_nom}
    # On saisit le prénom
    Input Text    css=#exp_prenom    ${exp_prenom}
    # On coche/décoche la case même adresse
    Run keyword If    '${meme_adresse}' == 'true'    Select Checkbox    css=#meme_adresse
    Run keyword If    '${meme_adresse}' == 'false'    Unselect Checkbox    css=#meme_adresse
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

Ajouter un particulier depuis un établissement
    [Documentation]    Permet d'ajouter un contact à un établissement.
    [Arguments]    ${etablissement_code}    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}=false
    #
    Depuis le contexte de l'établissement    ${etablissement_code}
    # On clique sur l'onglet "Contacts"
    On clique sur l'onglet    contact    Contacts
    # On clique sur le bouton d'ajout
    Click On Add Button JS
    # On saisit le contact
    Saisir un particulier    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

Saisir un particulier
    [Documentation]    Permet de remplir le formulaire d'un contact.
    [Arguments]    ${contact_type}    ${civilite}    ${nom}    ${prenom}    ${reception_convocation}=false
    # On sélectionne le type
    Select From List By Label    css=#contact_type    ${contact_type}
    # On sélectionne la civilité
    Select From List By Label    css=#civilite    ${civilite}
    # On saisit le nom
    Input Text    css=#nom    ${nom}
    # On saisit le prénom
    Input Text    css=#prenom    ${prenom}
    # On coche/décoche la case reception_convocation
    Run keyword If    '${reception_convocation}' == 'true'    Select Checkbox    css=#reception_convocation
    Run keyword If    '${reception_convocation}' == 'false'    Unselect Checkbox    css=#reception_convocation


Ajouter un utilisateur
    [Arguments]    ${nom}    ${email}    ${login}    ${password}    ${profil}
    # On saisit le nom
    Input Text    css=#nom    ${nom}
    # On saisit le mail
    Input Text    css=#email    ${email}
    # On saisit le login
    Input Text    css=#login    ${login}
    # On saisit le password
    Input Text    css=#password    ${password}
    # On sélectionne le profil
    Input Text    css=#profil    ${profil}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.

Ajouter un acteur
    [Arguments]    ${nom_prenom}    ${utilisateur}    ${service}    ${role}
    # On saisie le nom et prénom
    Input Text    css=#nom_prenom    ${nom_prenom}
    # On sélectionne un utilisateur
    Select From List By Label    css=#om_utilisateur    ${utilisateur}
    # On sélectionne le service
    Select From List By Label    css=#service    ${service}
    # On sélectionne le rôle
    Select From List By Label    css=#role    ${role}
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.