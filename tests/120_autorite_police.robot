*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  Une autorité de police...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil    admin    admin

    ##
    ## CATEGORIE DE REUNION 01
    ##
    &{reucategorie01} =  Create Dictionary
    ...  code=gen
    ...  libelle=Générale
    ...  description=Générale
    ...  service=Sécurité Incendie
    ...  ordre=100
    Ajouter la catégorie de réunion  ${reucategorie01}

    ##
    ## AVIS DE REUNION 01
    ##
    &{reuavis01} =  Create Dictionary
    ...  code=REV
    ...  libelle=A revoir
    ...  description=Description
    ...  service=Sécurité Incendie
    Ajouter l'avis de réunion  ${reuavis01}
    Set Suite Variable  ${reuavis01}

    ##
    ## INSTANCE DE REUNION 01
    ##
    &{reuinstance01} =  Create Dictionary
    ...  code=AZ
    ...  libelle=Instance
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance01}

    ##
    ## TYPE DE REUNION 01
    ##
    @{categories_autorisees}  Create List  ${reucategorie01.libelle}
    @{avis_autorises}    Create List    A revoir
    @{instances_autorisees}  Create List  ${reuinstance01.libelle}
    &{reutype01} =  Create Dictionary
    ...  code=CCS-GEN
    ...  libelle=CCS générale
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype01}

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=MOTEL SEVEN
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=85
    ...  adresse_voie=AVE DE LA GROGNARDE
    ...  adresse_cp=13011
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=11ème
    ...  exp_civilite=M.
    ...  exp_nom=Martin
    ...  exp_prenom=Paul
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab01_titre}
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=${DATE_FORMAT_DD/MM/YYYY}
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =    Set Variable    ${reutype01.code}-${DATE_FORMAT_YYYY-MM-DD}
    Set Suite Variable    ${reunion01_code}

    # On ajoute une demande de passage en réunion
    ${dossier_instruction_reunion_id} =    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction    ${dc01_libelle}    SI    ${DATE_FORMAT_DD/MM/YYYY}    CCS générale    Générale
    Set Suite Variable    ${dossier_instruction_reunion_id}

    # On planifie la demande de passage pour la réunion
    Planifier toutes les demandes de passages pressenties pour la réunion    ${reunion01_code}

    # On numérote les dossiers
    Numéroter l'ordre du jour de la réunion    ${reunion01_code}

    # On ajoute un document généré
    @{contacts_lies}    Create List    (Exploitant) M. Martin Paul
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc01_libelle}
    ...  ${params}
    ...  (*) Notification d'Autorité de Police
    ...  Notification d'Autorité de Police
    # On clique sur le bouton retour
    Click On Back Button In Subform
    # On clique sur l'enregistrement nouvellement crée
    Click On Link    Martin Paul
    # On récupère le code barres
    ${code_barres} =    Get Text    css=#code_barres
    ${courrier} =    Get Text    css=#sousform-courrier #courrier
    Set Suite Variable    ${courrier}
    Set Suite Variable    ${code_barres}
    #
    Finaliser le document généré    ${code_barres}


Paramétrage d'une décision d'autorité de police en type 'Arrêté'

    [Documentation]  ...
    ...
    ...  - les champs qui concernent le paramétrage de l'arrêté doivent être
    ...  cachés au chargement du formulaire, puis affichés lorsque la case à
    ...  cocher type_arrete est cochée (en ajout, modification, consultation,
    ...  rechargement de formulaire),
    ...  - les nomenclature_actes_matiere_niv1 doivent modifier la liste des
    ...  valeurs disponibles nomenclature_actes_matiere_niv2.
    ...

    Depuis la page d'accueil  admin  admin

    #
    Depuis le formulaire d'ajout de type de décision d'autorité de police
    #
    Element Should Not Be Visible  css=#arrete_reglementaire
    Element Should Not Be Visible  css=#arrete_notification
    Element Should Not Be Visible  css=#arrete_publication
    Element Should Not Be Visible  css=#arrete_temporaire
    Element Should Not Be Visible  css=#nomenclature_actes_nature
    Element Should Not Be Visible  css=#nomenclature_actes_matiere_niv1
    Element Should Not Be Visible  css=#nomenclature_actes_matiere_niv2
    #
    Set Checkbox  css=#type_arrete  true
    #
    Element Should Be Visible  css=#arrete_reglementaire
    Element Should Be Visible  css=#arrete_notification
    Element Should Be Visible  css=#arrete_publication
    Element Should Be Visible  css=#arrete_temporaire
    Element Should Be Visible  css=#nomenclature_actes_nature
    Element Should Be Visible  css=#nomenclature_actes_matiere_niv1
    Element Should Be Visible  css=#nomenclature_actes_matiere_niv2


Création d'une autorité de police

    [Documentation]    Création d'une autorité de police depuis une demande de
    ...    passage en réunion.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion    ${reunion01_code}    ${dossier_instruction_reunion_id}
    # On déplit le fieldset des autorités de police
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Click Element    css=#formSpecificContent_autorite_police legend
    Sleep    1
    # On ajoute une autorité de police
    Click Element    css=#add_autorite_police span.add-16
    Sleep    1
    # On sélectionne la décision
    Select From List By Label    css=#autorite_police_decision    Différée
    # On vérifie que le délai est saisit automatiquement
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Form Value Should Be    css=#delai    10
    # On sélectionne le motif
    Select From List By Label    css=#autorite_police_motif    Vérifications
    # On sélectionne le courrier liés
    @{courriers_lies} =    Create List    ${courrier}
    Select From List By Value    css=#courriers_lies    @{courriers_lies}
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message affiché à l'utilisateur
    Valid Message Should Contain In Subform    Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Link    close
    # On vérifie que l'autorité de police s'est ajoutée à la liste
    Page Should Contain    AP n°
    Page should Contain    du ${DATE_FORMAT_DD/MM/YYYY}
    #
    Click Element    css=.liste_autorite_police > div:first-child a span.edit_autorite_police
    ${autorite_police} =    Get Text    css=#autorite_police_value
    Set Suite Variable    ${autorite_police}


Vérification des flags du dossier de coordination et de l'établissement

    [Documentation]    Sur l'établissement et le dossier de coordination, le
    ...    champ indiquant si une autorité de police est en cours doit être à
    ...    Oui.

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    # On vérifie la valeur du champ 'Autorité de police en cours' sur le dossier
    # de coordination
    Depuis le contexte du dossier de coordination    ${dc01_libelle}
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Form Static Value Should Be    css=#autorite_police_encours    Oui
    # On vérifie la valeur du champ 'Autorité de police en cours' sur l'établissement
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    Form Static Value Should Be    css=#autorite_police_encours    Oui


Notification de l'autorité de police

    [Documentation]    Notifie une autorité de police depuis un courrier lié.
    ...    Permet de mettre à jour sa date de notification et si tous les
    ...    courriers liés sont notifiés, sa date butoir en saisisant la date
    ...    retour AR du document généré.
    ...    Cette notification met aussi à jour l'établissement en fonction de la
    ...    décision de l'autorité de police : ouvre/ferme l'établissement et en
    ...    cas d'ouverture l'établissement devient un ERP référentiel.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Modifier le document généré depuis le menu    ${code_barres}    null    null    null    null    null     ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie les autres messages de validation
    Valid Message Should Contain    L'établissement ${etab01.libelle} a été mis a jour.
    Valid Message Should Contain    L'autorité de police liée a été notifiée.
    # On clique sur le bouton retour
    Click On Back Button
    # Vérifie le select-multiple des AP
    ${autorites_police_liees} =    Set Variable    AP n°${autorite_police} du ${DATE_FORMAT_DD/MM/YYYY}
    Form Static Value Should Be    css=#autorites_police_liees    ${autorites_police_liees}
    #
    Depuis le contexte de l'autorité de police non notifiée ou exécutée    ${autorite_police}
    # On vérifie la date de notification et la date butoir
    Form Static Value Should Be    css=#date_notification    ${DATE_FORMAT_DD/MM/YYYY}
    Form Static Value Should Be    css=#date_butoir    ${DATE_FORMAT_DD/MM/YYYY}
    #
    Depuis le contexte de l'établissement    ${etab01_code}    ${etab01.libelle}
    # On vérifie l'état et la nature de l'établissement
    Form Static Value Should Be    css=#etablissement_etat    Ouvert
    Form Static Value Should Be    css=#etablissement_nature    ERP Référentiel
    Form Static Value Should Be    css=#date_arrete_ouverture    ${DATE_FORMAT_DD/MM/YYYY}


Clôture de l'autorité de police

    [Documentation]    Clôture l'autorité de police et supprime les demandes de
    ...    passage en réunion liées qui ne sont pas planifiées.

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis le contexte de l'autorité de police non notifiée ou exécutée    ${autorite_police}
    # On vérifie le nom de l'établissement
    Element Should Contain    css=#link_etablissement    ${etab01_code} - ${etab01.libelle}
    # On vérifie le numéro du dossier de coordination
    Element Should Contain    css=#link_dossier_coordination    ${dc01_libelle}

    # On vérifie la demande de passage en réunion
    Form Static Value Should Be    css=#dossier_instruction_reunion    Demande de passage n°${dossier_instruction_reunion_id} du ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie la prochaine demande de passage en réunion
    ${dossier_instruction_reunion_id} =    Convert to Integer    ${dossier_instruction_reunion_id}
    Element Should Contain    css=#dossier_instruction_reunion_prochain    Demande de passage n°
    # On vérifie que l'action "clôturer" est affichée
    Portlet Action Should Be In Form    autorite_police_non_notifiee_executee    cloturer
    # On vérifie que l'action "réouvrir" n'est pas affichée
    Portlet Action Should Not Be In Form    autorite_police_non_notifiee_executee    decloturer
    # On clique sur l'action "clôturer"
    Click On Form Portlet Action    autorite_police_non_notifiee_executee    cloturer
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    L'autorité de police a été correctement clôturée.
    # On vérifie la valeur du champ
    Form Static Value Should Be    css=#cloture    Oui
    # On vérifie la demande de passage en réunion n'est pas été supprimée
    Form Static Value Should Be    css=#dossier_instruction_reunion    Demande de passage n°${dossier_instruction_reunion_id} du ${DATE_FORMAT_DD/MM/YYYY}
    # On vérifie que la prochaine demande de passage de réunion a été supprimée
    Form Static Value Should Be    css=#dossier_instruction_reunion_prochain    ${empty}
    # On vérifie que l'action "réouvrir" est affichée
    Portlet Action Should Be In Form    autorite_police_non_notifiee_executee    decloturer
    # On vérifie que l'action "clôturer" n'est pas affichée
    Portlet Action Should Not Be In Form    autorite_police_non_notifiee_executee    cloturer
    # On clique sur l'action "réouvrir"
    Click On Form Portlet Action    autorite_police_non_notifiee_executee    decloturer
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    L'autorité de police a été correctement réouverte.



Gestion de l'arrêté

    [Documentation]  ...

    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)

    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)


    # En tant que profil ADMINISTRATEUR
    Depuis la page d'accueil  admin  admin

    # On définit une liste de clés pour lesquelles on souhaite vérifier
    # l'absence dans le fichier de métadonnées. Ce sont les métadonnées
    # spécifiques aux arrêtés.
    @{md_no} =  Create List
    ...  pv_erp_numero
    ...  pv_erp_nature_analyse
    ...  pv_erp_reference_urbanisme
    ...  pv_erp_avis_rendu
    ...  code_reunion
    ...  date_reunion
    ...  type_reunion
    ...  commission
    Set Suite Variable  ${md_no}

    ##
    ## TYPE DE DECISION D'AUTORITE DE POLICE 01
    ##
    &{typedap01} =  Create Dictionary
    ...  code=ART01
    ...  libelle=Arrêté d'ouverture
    ...  description=
    ...  service=Sécurité Incendie
    ...  suivi_delai=
    ...  avis=favorable
    ...  etablissement_etat=Ouvert
    ...  delai=
    ...  type_arrete=true
    ...  arrete_reglementaire=Oui
    ...  arrete_notification=Non
    ...  arrete_publication=Choisir...
    ...  arrete_temporaire=Oui
    # ...  nomenclature_actes_nature=
    # ...  nomenclature_actes_matiere_niv1=
    # ...  nomenclature_actes_matiere_niv2=
    Ajouter le type de décision d'autorité de police  ${typedap01}
    Set Suite Variable  ${typedap01}

    ##
    ## TYPE DE DECISION D'AUTORITE DE POLICE 02
    ##
    &{typedap02} =  Create Dictionary
    ...  code=ART02
    ...  libelle=Arrêté d'autorisation de travaux
    ...  description=
    ...  service=Sécurité Incendie
    ...  suivi_delai=
    ...  avis=favorable
    ...  type_arrete=true
    ...  arrete_reglementaire=Choisir...
    ...  arrete_notification=Oui
    ...  arrete_publication=Non
    ...  arrete_temporaire=Non
    ...  nomenclature_actes_nature=Arrêtés réglementaires
    ...  nomenclature_actes_matiere_niv1=7 Finances locales
    ...  nomenclature_actes_matiere_niv2=7.8 Fonds de concours
    Ajouter le type de décision d'autorité de police  ${typedap02}
    Set Suite Variable  ${typedap02}

    ##
    ## SIGNATAIRE 01
    ##
    &{signataire01} =  Create Dictionary
    ...  nom=QUEVILLE
    ...  prenom=Jacques
    ...  signature=Monsieur le Maire, Jacques QUEVILLE
    ...  civilite=M.
    ...  signataire_qualite=Maire
    Ajouter le signataire  ${signataire01}
    Set Suite Variable  ${signataire01}

    ##
    ## CATEGORIE DE REUNION 01
    ##
    &{reucategorie01} =  Create Dictionary
    ...  code=DIV180
    ...  libelle=Divers TEST180
    ...  description=Tout ce ne qui ne rentre pas dans les autres catégories
    ...  service=Sécurité Incendie
    ...  ordre=40
    Ajouter la catégorie de réunion  ${reucategorie01}
    Set Suite Variable  ${reucategorie01}

    ##
    ## INSTANCE DE REUNION 01
    ##
    &{reuinstance01} =  Create Dictionary
    ...  code=MP180
    ...  libelle=Marins Pompier TEST180
    ...  service=Sécurité Incendie
    Ajouter l'instance de réunion  ${reuinstance01}
    Set Suite Variable  ${reuinstance01}

    ##
    ## TYPE DE REUNION 01
    ##
    @{categories_autorisees}  Create List  ${reucategorie01.libelle}
    @{avis_autorises}  Create List  DIFFERE
    @{instances_autorisees}  Create List  ${reuinstance01.libelle}
    &{reutype01} =  Create Dictionary
    ...  code=R180
    ...  libelle=Réunion TEST180
    ...  service=Sécurité Incendie
    ...  categories_autorisees=@{categories_autorisees}
    ...  avis_autorises=@{avis_autorises}
    ...  instances_autorisees=@{instances_autorisees}
    Ajouter le type de réunion  ${reutype01}
    Set Suite Variable  ${reutype01}

    ##
    ## ETABLISSEMENT 01
    ##
    &{etab01} =  Create Dictionary
    ...  libelle=PETIT CASINO
    ...  etablissement_nature=ERP potentiel
    ...  adresse_numero=12
    ...  adresse_voie=RUE MARCEL ROMAN
    ...  adresse_cp=13015
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=15ème
    ...  exp_civilite=M.
    ...  exp_nom=MAYER
    ...  exp_prenom=Jacques
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}


    ##
    ## DOSSIER DE COORDINATION 01
    ##
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Autorisation de Travaux
    ...  description=Description
    ...  etablissement=${etab01_titre}
    ...  date_demande=10/05/2016
    ...  a_qualifier=false
    ...  dossier_instruction_secu=true
    ...  dossier_instruction_acc=true
    Set Suite Variable  ${dc01}
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    ${dc01_di_si} =  Catenate  SEPARATOR=-  ${dc01_libelle}  SI
    ${dc01_di_acc} =  Catenate  SEPARATOR=-  ${dc01_libelle}  ACC
    Set Suite Variable  ${dc01_libelle}
    Set Suite Variable  ${dc01_di_si}
    Set Suite Variable  ${dc01_di_acc}

    ##
    ## REUNION 01
    ##
    &{reunion01} =  Create Dictionary
    ...  reunion_type=${reutype01.libelle}
    ...  date_reunion=${DATE_FORMAT_DD/MM/YYYY}
    ...  date_reunion_yyyy_mm_dd=${DATE_FORMAT_YYYY-MM-DD}
    Ajouter la réunion  ${reunion01}
    ${reunion01_code} =  Set Variable  ${reutype01.code}-${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${reunion01}
    Set Suite Variable  ${reunion01_code}

    ##
    ## DEMANDE DE PASSAGE EN REUNION 01
    ##
    # On ajoute une demande de passage en réunion
    ${dpr01_id} =    Ajouter la demande de passage en réunion depuis le contexte du dossier d'instruction
    ...  ${dc01_libelle}
    ...  SI
    ...  ${DATE_FORMAT_DD/MM/YYYY}
    ...  ${reutype01.libelle}
    ...  ${reucategorie01.libelle}
    Set Suite Variable    ${dpr01_id}
    # Par défaut c'est la date du jour qui est positionnée
    &{dpr01} =  Create Dictionary
    ...  dossier_instruction=${dc01_di_si}
    ...  date_souhaitee=${DATE_FORMAT_DD/MM/YYYY}
    ...  reunion_type=${reutype01.libelle}
    ...  reunion_type_categorie=${reucategorie01.libelle}
    ...  reunion_code=${reunion01_code}
    ...  reunion_date_format_yyyy_mm_dd=${reunion01.date_reunion_yyyy_mm_dd}
    Set Suite Variable  ${dpr01}
    Planifier toutes les demandes de passages pressenties pour la réunion    ${reunion01_code}
    Numéroter l'ordre du jour de la réunion  ${reunion01_code}

    ##
    ##
    ##
    ##
    ## Document 01 - Cas d'utilisation A & B
    ##
    Ajouter un particulier depuis un établissement    ${etab01_code}    Mandataire    M.    AQWEDC01    Jacques    true
    @{contacts_lies}    Create List    (Mandataire) M. AQWEDC01 Jacques
    ${params}    Create Dictionary    contacts_lies=@{contacts_lies}
    Ajouter le document généré depuis le contexte du dossier de coordination
    ...  ${dc01_libelle}
    ...  ${params}
    ...  (*) Décision
    ...  ME - Arrêté d'ouverture
    Click On Back Button In Subform
    Click On Link  AQWEDC01 Jacques
    ${docgen01_id} =  Get Text  css=#sousform-courrier #courrier
    ${docgen01_code_barres} =  Get Text  css=#code_barres

    #
    Finaliser le document généré  ${docgen01_code_barres}
    ## => [courrier.om_fichier_finalise_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_finalise_uid} =  Récupérer l'uid du fichier finalisé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_finalise_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    ${docgen01_fichier_finalise_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_finalise_uid}
    Le fichier doit exister  ${docgen01_fichier_finalise_path}
    Le fichier doit exister  ${docgen01_fichier_finalise_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_finalise_path}
    ${annee}=  Get Time  year
    ${md} =  Create Dictionary
    # ...  filename=nom de fichier généré
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_libelle} - ME - Arrêté d'ouverture
    ...  description=document généré finalisé
    ...  application=openARIA
    ...  origine=généré
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    ...  arrete_numero=${annee}_00001_ERP
    ...  arrete_reglementaire=
    ...  arrete_notification=
    ...  arrete_date_notification=
    ...  arrete_publication=
    ...  arrete_date_publication=
    ...  arrete_temporaire=
    ...  arrete_expiration=
    ...  arrete_date_controle_legalite=
    ...  arrete_nature_acte=
    ...  arrete_nature_acte_niv1=
    ...  arrete_nature_acte_niv2=
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_finalise_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_finalise_md_path}

    #
    Depuis la demande de passage (retour d'avis) dans le contexte de la réunion  ${reunion01_code}  ${dpr01_id}
    Sleep  1
    Execute JavaScript  window.jQuery("#formSpecificContent_autorite_police #liste_autorite_police").attr("style", "display:table;");
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#formSpecificContent_autorite_police #liste_autorite_police
    Click Element  css=#add_autorite_police
    Select From List By Label    css=#autorite_police_decision    Arrêté d'autorisation de travaux
    Input Text    css=#delai    10
    Select From List By Label    css=#autorite_police_motif  Vérifications
    Select From List By Value    css=#courriers_lies    ${docgen01_id}
    Click Element  css=#sousform-autorite_police .om-button
    Sleep  1
    Click Element  css=#sousform-autorite_police a.retour
    #
    Ajouter le fichier signé du document généré  ${docgen01_code_barres}  numerisation1.pdf
    ## => [courrier.om_fichier_signe_courrier] (METADATA FILESTORAGE)
    ${docgen01_fichier_signe_uid} =  Récupérer l'uid du fichier signé du document généré  ${docgen01_code_barres}
    ${docgen01_fichier_signe_path} =  Récupérer le chemin vers le fichier correspondant à l'uid  ${docgen01_fichier_signe_uid}
    ${docgen01_fichier_signe_md_path} =  Récupérer le chemin vers le fichier de métadonnées correspondant à l'uid  ${docgen01_fichier_signe_uid}
    Le fichier doit exister  ${docgen01_fichier_signe_path}
    Le fichier doit exister  ${docgen01_fichier_signe_md_path}
    # Composition du dictionnaire représentant les métadonnées qui doivent
    # apparaître dans le fichier de métadonnées.
    ${size}=  Get File Size  ${docgen01_fichier_signe_path}
    ${md} =  Create Dictionary
    ...  filename=numerisation1.pdf
    ...  mimetype=application/pdf
    ...  size=${size}
    ...  titre=Établissement ${etab01_code} - Dossier ${dc01_libelle} - ME - Arrêté d'ouverture (signé)
    ...  description=document généré numérisé signé
    ...  application=openARIA
    ...  origine=téléversé
    ...  etablissement_code=${etab01_code}
    ...  etablissement_libelle=${etab01.libelle}
    ...  etablissement_siret=
    ...  etablissement_referentiel=false
    ...  etablissement_exploitant=${etab01.exp_prenom} ${etab01.exp_nom}
    ...  etablissement_adresse_numero=${etab01.adresse_numero}
    ...  etablissement_adresse_mention=
    ...  etablissement_adresse_voie=${etab01.adresse_voie}
    ...  etablissement_adresse_cp=${etab01.adresse_cp}
    ...  etablissement_adresse_ville=${etab01.adresse_ville}
    ...  etablissement_adresse_arrondissement=${etab01.adresse_arrondissement}
    ...  etablissement_ref_patrimoine=
    ...  dossier_coordination=${dc01_libelle}
    ...  dossier_instruction=
    ...  signataire=
    ...  signataire_qualite=
    ...  date_signature=
    ...  arrete_numero=${annee}_00001_ERP
    ...  arrete_reglementaire=
    ...  arrete_notification=true
    ...  arrete_date_notification=
    ...  arrete_publication=false
    ...  arrete_date_publication=
    ...  arrete_temporaire=false
    ...  arrete_expiration=
    ...  arrete_date_controle_legalite=
    ...  arrete_nature_acte=Arrêtés réglementaires
    ...  arrete_nature_acte_niv1=7 Finances locales
    ...  arrete_nature_acte_niv2=7.8 Fonds de concours
    Les métadonnées (clé/valeur) doivent être présentes dans le fichier  ${md}  ${docgen01_fichier_signe_md_path}
    Les métadonnées (clé) ne doivent pas être présentes dans le fichier  ${md_no}  ${docgen01_fichier_signe_md_path}
