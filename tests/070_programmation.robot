*** Settings ***
Resource  resources/resources.robot
Suite Setup  For Suite Setup
Suite Teardown  For Suite Teardown
Documentation  La programmations des visites...


*** Test Cases ***
Constitution d'un jeu de données

    [Documentation]  L'objet de ce 'Test Case' est de constituer un jeu de
    ...  données cohérent pour les scénarios fonctionnels qui suivent.

    #
    Depuis la page d'accueil    cadre-si    cadre-si

    #
    &{etab01} =  Create Dictionary
    ...  libelle=MONOP
    ...  etablissement_nature=ERP Référentiel
    ...  etablissement_type=M
    ...  etablissement_categorie=1
    ...  adresse_numero=39
    ...  adresse_voie=RUE DE ROME
    ...  adresse_cp=13006
    ...  adresse_ville=MARSEILLE
    ...  adresse_arrondissement=6ème
    ...  siret=73282932000074
    ...  exp_civilite=Mme
    ...  exp_nom=Martel
    ...  exp_prenom=Lyne
    ${etab01_code} =  Ajouter l'établissement  ${etab01}
    ${etab01_titre} =  Set Variable  ${etab01_code} - ${etab01.libelle}
    Set Suite Variable  ${etab01}
    Set Suite Variable  ${etab01_code}
    Set Suite Variable  ${etab01_titre}

    #
    Ajouter le contact institutionnel depuis le menu    null    Sécurité Incendie    null    M.    Bouchard    Gerard    null    null    null    null    null    null    null    null    null    null    null    null    null    null    nospam@atreal.fr    true

    #
    Ajouter la programmation depuis le menu    null    2014    39
    ${programmation} =    Catenate    SEPARATOR=/    2014    39
    Set Suite Variable    ${programmation}


Création de deux semaines de programmation
    #
    Depuis la page d'accueil    admin    admin
    # 1ère semaine
    # On ouvre le menu
    Go To Submenu In Menu    suivi    programmation
    # On passe en mode ajout
    Click On Add Button
    # On saisit les valeurs
    Input Value With JS    annee    2015
    Input Value With JS    numero_semaine    53
    Select From List By Label    css=#service    Sécurité Incendie
    # On valide le formulaire
    Click On Submit Button
    # On vérifie le message de validation affiché à l'utilisateur
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    # On clique sur le bonton de retour pour retourner au tableau
    Click On Back Button
    # On rentre dans la semaine créée
    Click On Link    2015/53
    # On vérifie le fil d'ariane
    Breadcrumb Should Contain    Suivi > Programmations > Gestion > Semaine N°53 Du 28/12/2015 Au 03/01/2016 V1
    # On vérifie les valeurs
    Element Text Should Be    css=#version    1
    Element Text Should Be    css=#programmation_etat    En préparation
    Element Text Should Be    css=#date_modification    ${DATE_FORMAT_DD/MM/YYYY}
    Element Text Should Be    css=#service    Sécurité Incendie

    # 2ème semaine
    Click On Back Button
    Click On Add Button
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Valid Message Should Be    Vos modifications ont bien été enregistrées.
    Click On Back Button
    Click On Link    2016/01

    # Vérification de l'unicité
    Click On Back Button
    Click On Add Button
    Input Value With JS    numero_semaine    1
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Error Message Should Contain    Les valeurs saisies dans les champs année, numéro de la semaine, service existent déjà, veuillez saisir de nouvelles valeurs.

    # Contrôle de la saisie
    Click On Back Button
    Click On Add Button
    Input Value With JS Failed    annee   123    Vous n'avez pas saisi une année valide.
    Input Value With JS Failed    numero_semaine   123    Vous n'avez pas saisi un numéro de semaine valide.
    Input Value With JS    annee   2016
    Input Value With JS    numero_semaine   53
    Select From List By Label    css=#service    Sécurité Incendie
    Click On Submit Button
    Error Message Should Contain    L'année 2016 ne comporte pas de semaine 53.


Création d'une visite
    [Documentation]    On le teste le drag & drop ainsi que le formulaire d'ajout

    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    #
    &{dc01} =  Create Dictionary
    ...  dossier_coordination_type=Visite de contrôle SI
    ...  description=Visite de contrôle du service sécurité incendie
    ...  etablissement=${etab01_titre}
    ${dc01_libelle} =  Ajouter le dossier de coordination  ${dc01}
    Set Suite Variable    ${dc01}
    Set Suite Variable    ${dc01_libelle}
    #
    Depuis l'interface de planification de la programmation     ${programmation}

    # On vérifie que le drag and drop est désactivé
    Element Should Be Visible    css=#filtre_technicien_info

    # On sélectionne un technicien
    Select From List By Label    css=#acteur    Paul DURAND
    # On vérifie que le drag and drop est activé
    Element Should Not Be Visible    css=#filtre_technicien_info

    # On fait une recherche sur le libelle du DI
    Input Text    css=table#tableau_propositions tr.search_tab_row th.dossier_instruction_libelle input.search_tab_input   ${dc01_libelle}-SI

    ## Création de la visite n°1 : à poursuivre
    # On crée une visite
    Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(4) td.fc-widget-content:nth-child(2)
    # On vérifie que l'overlay est correctement ouvert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On coche "a_poursuivre"
    Select Checkbox  css=#sousform-visite #a_poursuivre
    # On valide le formulaire
    Click On Submit Button In Subform
    # On vérifie le message de validation
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di est toujours présent dans le tableau
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${dc01_libelle}-SI

    ## Création de la visite n°2 : à poursuivre + vérification de la validité de la date de la visite
    # On crée une autre visite
    Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(20) td.fc-widget-content:nth-child(2)
    # On vérifie que l'overlay est correctement ouvert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On coche "a_poursuivre"
    Select Checkbox    css=#sousform-visite #a_poursuivre
    #
    Input Text    css=#date_visite    21/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   La date doit être comprise dans la semaine de programmation.
    #
    Input Text    css=#date_visite    ${EMPTY}
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   Le champ date de la visite est obligatoire
    #
    Input Text    css=#date_visite    29/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Error Message Should Contain   La date doit être comprise dans la semaine de programmation.
    #
    Input Text    css=#date_visite    27/09/2014
    # On valide le formulaire
    Click On Submit Button In Subform
    #
    Valid Message Should Contain    Vos modifications ont bien été enregistrées.
    #
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di est toujours présent dans le tableau
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Contain  ${dc01_libelle}-SI

    ## Création de la visite n°3 : PAS à poursuivre + vérification de la validité des heures de début et de fin
    # On crée une autre visite le 25/09 de 12:30 à 15:30
    Drag And Drop    css=table#tableau_propositions tbody tr    css=tr.fc-minor:nth-child(20) td.fc-widget-content:nth-child(2)
    # On vérifie que l'overlay est correctement ouvert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # On vérifie que les heures sélectionnées sont correctes
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_debut_heure  12
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_debut_minute  30
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_fin_heure  15
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_fin_minute  30
    # On saisit une heure de début égale à l'heure de fin (de 15:30 à 15:30)
    # Cela crée une alerte que l'on vérifie
    Select Value With JS Failed   heure_debut_heure  15  L'heure de fin doit être supérieure à l'heure de début.
    # On vérifie que le JS a corrigé les heures (de 15:30 à 15:45)
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_debut_heure  15
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Selected List Label Should Be  heure_fin_minute  45
    # On désactive le contrôle JavaScript pour vérifier le contrôle PHP
    Désactiver évènements heure minute
    # On saisit une heure de début égale à celle de fin (de 15:30 à 15:30)
    Select From List By Label  heure_fin_minute  30
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  L' heure de début est égale à l' heure de fin.
    # On redésactive le contrôle JavaScript (nouveau formulaire = nouveau DOM)
    Désactiver évènements heure minute
    # On saisit une heure de début supérieure à celle de fin (de 15:45 à 15:30)
    Select From List By Label  heure_debut_minute  45
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  L' heure de début est postérieure à l' heure de fin.
    # On désactive encore le contrôle JavaScript
    Désactiver évènements heure minute
    # On ne saisit ni l'heure de début ni celle de fin
    Select From List By Label  heure_debut_heure  Choisir
    Execute JavaScript  window.heure_minute('heure_debut', 'heure');
    Select From List By Label  heure_debut_minute  Choisir
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    Select From List By Label  heure_fin_heure  Choisir
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Select From List By Label  heure_fin_minute  Choisir
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    #
    Error Message Should Contain  Le champ heure de début est obligatoire
    Error Message Should Contain  Le champ heure de fin est obligatoire
    # On désactive une dernière fois le contrôle JavaScript
    Désactiver évènements heure minute
    # On saisit des heures valides, les mêmes qu'initialement crées lors du drop
    Select From List By Label  heure_debut_heure  12
    Execute JavaScript  window.heure_minute('heure_debut', 'heure');
    Select From List By Label  heure_debut_minute  30
    Execute JavaScript  window.heure_minute('heure_debut', 'minute');
    Select From List By Label  heure_fin_heure  15
    Execute JavaScript  window.heure_minute('heure_fin', 'heure');
    Select From List By Label  heure_fin_minute  30
    Execute JavaScript  window.heure_minute('heure_fin', 'minute');
    #
    Click On Submit Button In Subform
    Valid Message Should Contain  Vos modifications ont bien été enregistrées.
    # On ferme l'overlay
    Click On Back Button In Subform
    # On vérifie que le di n'est plus présent dans le tableau
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Should Not Contain  ${dc01_libelle}-SI

    #
    # Fiche de visualisation d'une visite
    #
    # On clique sur une visite que l'on vient d'ajouter
    Click Element  css=div.fc-event-container a
    # On vérifie que l'overlay est correctement ouvert
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Element Should Be Visible  css=#sousform-visite.overlay div.subtitle h3
    # Vérification du lien vers l'établissement, on clqiue sur le lien et on doit arriver sur la fiche de l'établissement
    Click Element  css=#link_etablissement
    Wait Until Keyword Succeeds  ${TIMEOUT}  ${RETRY_INTERVAL}  Page Title Should Be  Établissements > Tous Les Établissements > ${etab01_titre}


Générer les convocations exploitants
    #
    Depuis la page d'accueil    cadre-si    cadre-si
    #
    Depuis le contexte de la programmation    ${programmation}
    # On finalise la programmation
    Click On Form Portlet Action    programmation    finaliser
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Finalisation correctement effectuée.
    # On valide la programmation
    Click On Form Portlet Action    programmation    valider
    # On vérifie le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain     Validation correctement effectuée.
    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    #
    Depuis le contexte de la programmation    ${programmation}
    # On génère toutes les convocations aux visites
    Click On Form Portlet Action    programmation    envoyer_convoc_exploit
    # Le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    Toutes les convocations exploitants ont été générées.
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    Lettre de convocation des exploitants : Convocation


Générer les convocations partenaires

    [Documentation]    Permet de vérifier l'envoi des mails aux partenaires.

    #
    Depuis la page d'accueil    secretaire-si    secretaire-si
    #
    Depuis le contexte de la programmation    ${programmation}
    # On génère toutes les convocations aux visites
    Click On Form Portlet Action    programmation    envoyer_part
    #
    Wait Until Element Is Visible    css=.ui-dialog .ui-dialog-buttonset button
    Click Element    css=.ui-dialog .ui-dialog-buttonset button
    # Le message de validation
    Wait Until Keyword Succeeds     ${TIMEOUT}     ${RETRY_INTERVAL}    Valid Message Should Contain    La convocations des partenaires a été générée et envoyée par mail.

