--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a8
--
-- XXX Ce fichier doit être renommé en v1.0.0-a8.sql au moment de la release
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------
-- Ajout du champ service dans la table piece
ALTER TABLE piece ADD COLUMN service integer NOT NULL;
COMMENT ON COLUMN piece.service IS 'Service lié au document.';
ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_service_fkey FOREIGN KEY (service) REFERENCES service(service);

-- Ajout du champ dossier_instruction_reunion_prochain dans la table
-- autorite_police
ALTER TABLE autorite_police 
    ADD COLUMN dossier_instruction_reunion_prochain integer;
COMMENT ON COLUMN autorite_police.dossier_instruction_reunion_prochain IS 'Prochaine demande de passage en réunion.';
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_instruction_reunion_prochain_fkey FOREIGN KEY (dossier_instruction_reunion_prochain) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion);

----
-- BEGIN / Suppression des informations d'établissement dans l'analyse
----
-- 1/2 - Suppression des clés étrangères déjà présentes dans le DC
ALTER TABLE analyses
    DROP etablissement_type,
    DROP etablissement_categorie;
-- 2/2 - Modification de la liaison n/n des types secondaires
ALTER TABLE lien_analyses_etablissement_type RENAME lien_analyses_etablissement_type TO lien_dossier_coordination_etablissement_type;
ALTER TABLE lien_analyses_etablissement_type RENAME analyses TO dossier_coordination;
ALTER TABLE lien_analyses_etablissement_type RENAME TO lien_dossier_coordination_etablissement_type;
COMMENT ON COLUMN lien_dossier_coordination_etablissement_type.dossier_coordination
    IS 'Dossien de coordination';
COMMENT ON TABLE lien_dossier_coordination_etablissement_type
    IS 'Liste des types secondaires d''établissement renseignés dans le dossier de coordination';
ALTER TABLE lien_dossier_coordination_etablissement_type
    DROP CONSTRAINT lien_analyses_etablissement_type_analyses_fkey,
    ADD FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination (dossier_coordination) ON DELETE NO ACTION ON UPDATE NO ACTION;
CREATE SEQUENCE lien_dossier_coordination_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_dossier_coordination_etablissement_type.lien_dossier_coordination_etablissement_type;
----
-- END / Suppression des informations d'établissement dans l'analyse
----


----
-- BEGIN / Renommage table de liaison etablissement / type d'établissement
----
ALTER TABLE etablissement_type_secondaire
    RENAME etablissement_type_secondaire TO lien_etablissement_e_type;
ALTER TABLE etablissement_type_secondaire
    RENAME etablissement_type_secondaire_etablissement TO etablissement;
ALTER TABLE etablissement_type_secondaire
    RENAME etablissement_type_secondaire_type TO etablissement_type;
ALTER TABLE etablissement_type_secondaire
    RENAME TO lien_etablissement_e_type;
CREATE SEQUENCE lien_etablissement_e_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_etablissement_e_type.lien_etablissement_e_type;
----
-- END / Renommage table de liaison etablissement / type d'établissement
----

----
-- BEGIN / Modification éditions analyse
----
-- Mise à jour requête SI en commune inter-services
UPDATE om_requete SET
libelle = 'Analyse',
description = 'Tous les champs de fusion spécifiques à une analyse, son DI, son DC et son établissement s''il existe.',
requete = '',
merge_fields = '<table><thead><tr>
                        <th colspan="2">Analyse</th></tr></thead><tbody><tr><td>[anls_id]</td><td>Identifiant unique</td></tr><tr><td>[anls_type]</td><td>type d''analyse</td></tr><tr><td>[anls_obj]</td><td>objet</td></tr><tr><td>[anls_desc]</td><td>descriptif de l''établissement</td></tr><tr><td>[anls_cr]</td><td>compte-rendu</td></tr><tr><td>[anls_obs]</td><td>observation</td></tr><tr><td>[anls_reg_app]</td><td>réglementation applicable</td></tr><tr><td>[anls_doc_pdnt]</td><td>document présenté lors des visites</td></tr><tr><td>[anls_avis_prop]</td><td>Avis de l''analyse</td></tr><tr><td>[anls_avis_comp]</td><td>complément d''avis</td></tr><tr><td>[anls_presc]</td><td>tableau des prescriptions</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Données techniques accessibilité</th></tr></thead><tbody><tr><td>[dt_desc_acc]</td><td>description</td></tr><tr><td>[dt_h_ment]</td><td>accessible mental</td></tr><tr><td>[dt_h_audtf]</td><td>accessible auditif</td></tr><tr><td>[dt_h_phsq]</td><td>accessible physique</td></tr><tr><td>[dt_h_visu]</td><td>accessible visuel</td></tr><tr><td>[dt_elvtr]</td><td>élévateur</td></tr><tr><td>[dt_ascsr]</td><td>Ascenseur</td></tr><tr><td>[dt_douche]</td><td>Douche</td></tr><tr><td>[dt_bcl_mgnt]</td><td>Boucle magnétique</td></tr><tr><td>[dt_sanitaire]</td><td>Sanitaire</td></tr><tr><td>[dt_plc_stmnt_amng]</td><td>nb places stationnements aménagées</td></tr><tr><td>[dt_chmbr_amng]</td><td>nb places stationnements aménagées</td></tr><tr><td>[dt_plc_ass_pub]</td><td>nb places assises plublic</td></tr><tr><td>[dt_scda]</td><td>Dérogation SCDA</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Données techniques sécurité incendie</th></tr></thead><tbody><tr><td>[dt_desc_si]</td><td>description</td></tr><tr><td>[dt_eff_pub]</td><td>effectif public</td></tr><tr><td>[dt_eff_pers]</td><td>si_effectif_personnell</td></tr><tr><td>[dt_ssi]</td><td>type SSI</td></tr><tr><td>[dt_alarme]</td><td>type d''alarme</td></tr><tr><td>[dt_i16]</td><td>conformité I16</td></tr><tr><td>[dt_alim]</td><td>alimentation de remplacement</td></tr><tr><td>[dt_svc_sec]</td><td>Service sécurité</td></tr><tr><td>[dt_pers_jour]</td><td>effectif du personnel de jour</td></tr><tr><td>[dt_pers_nuit]</td><td>effectif du personnel de nuit</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Dossier d''instruction</th></tr></thead><tbody><tr><td>[di_lib]</td><td>libellé</td></tr><tr><td>[di_tech]</td><td>Technicien en charge</td></tr><tr><td>[di_ac]</td><td>autorité compétente</td></tr><tr><td>[di_qualif]</td><td>à qualifier</td></tr><tr><td>[di_incomp]</td><td>incomplétude</td></tr><tr><td>[di_clos]</td><td>clôturé</td></tr><tr><td>[di_prio]</td><td>prioritaire</td></tr><tr><td>[di_piece]</td><td>pièce attendue</td></tr><tr><td>[di_desc]</td><td>description</td></tr><tr><td>[di_notes]</td><td>notes</td></tr><tr><td>[di_statut]</td><td>statut</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Dossier de coordination</th></tr></thead><tbody><tr><td>[dc_lib]</td><td>libellé</td></tr><tr><td>[dc_type]</td><td>type de dossier de coordination</td></tr><tr><td>[dc_date_dmnd]</td><td>date de la demande</td></tr><tr><td>[dc_date_butr]</td><td>date butoir</td></tr><tr><td>[dc_da_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dc_di_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dc_qualif]</td><td>à qualifier</td></tr><tr><td>[dc_etab_type]</td><td>type</td></tr><tr><td>[dc_etab_type_sec]</td><td>type(s) secondaire(s)</td></tr><tr><td>[dc_etab_cat]</td><td>catégorie</td></tr><tr><td>[dc_etab_erp]</td><td>erp</td></tr><tr><td>[dc_clos]</td><td>clôturé</td></tr><tr><td>[dc_ctr_urb]</td><td>contraintes URBA</td></tr><tr><td>[dc_etab_loc_som]</td><td>locaux sommeil</td></tr><tr><td>[dc_parent]</td><td>dossier de coordination parent</td></tr><tr><td>[dc_desc]</td><td>description</td></tr><tr><td>[dc_ap]</td><td>autorité de police en cours</td></tr><tr><td>[dc_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Établissement</th></tr></thead><tbody><tr><td>[etab_code]</td><td>Numero T</td></tr><tr><td>[etab_lib]</td><td>libellé</td></tr><tr><td>[etab_siret]</td><td>SIRET</td></tr><tr><td>[etab_tel]</td><td>téléphone</td></tr><tr><td>[etab_fax]</td><td>fax</td></tr><tr><td>[etab_annee_const]</td><td>année de construction</td></tr><tr><td>[etab_type]</td><td>type</td></tr><tr><td>[etab_cat]</td><td>catégorie</td></tr><tr><td>[etab_nat]</td><td>nature</td></tr><tr><td>[etab_stat_jur]</td><td>statut juridique</td></tr><tr><td>[etab_tut_adm]</td><td>tutelle administrative</td></tr><tr><td>[etab_etat]</td><td>état</td></tr><tr><td>[etab_adresse]</td><td>adresse</td></tr><tr><td>[etab_date_ar_ouv]</td><td>date d''arrêté d''ouverture</td></tr><tr><td>[etab_exp]</td><td>exploitant</td></tr><tr><td>[etab_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>',
type = 'objet',
classe = 'analyses',
methode = '',
code = 'anl'
WHERE code = 'si_rapport_analyse';

-- Suppression requête ACC
UPDATE om_lettretype SET om_sql = (SELECT om_requete FROM om_requete WHERE code = 'anl') WHERE id LIKE 'acc_%' OR id LIKE 'si_%';
DELETE FROM om_requete WHERE code = 'acc_rapport_analyse';

-- Lettres-types SI
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description sécurité : [dt_desc_si]</p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité I16 : [dt_i16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p> </p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id LIKE 'si_%';
-- Lettres-types ACC
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description accessibilité : [dt_desc_acc]</p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id LIKE 'acc_%';
----
-- END / Modification éditions analyse
----

----
-- BEGIN / Suppression du champ prescription reglementaire de l'analyse
-- devenu inutile avec la table prescription.
----
ALTER TABLE analyses
    DROP prescription_reglementaire;
----
-- END / Suppression du champ prescription reglementaire de l'analyse
-- devenu inutile avec la table prescription.
----

----
-- BEGIN / Création de la table métier procès-verbal
----

CREATE TABLE proces_verbal (
    proces_verbal integer NOT NULL,
    numero character varying(50) NOT NULL,
    dossier_instruction integer NOT NULL,
    dossier_instruction_reunion integer NOT NULL,
    modele_edition integer NOT NULL,
    date_redaction date NOT NULL,
    signataire integer NULL,
    genere boolean NOT NULL,
    om_fichier_finalise character varying(64) NULL,
    om_fichier_signe character varying(64) NULL
);
COMMENT ON TABLE proces_verbal IS 'Procès verbaux du dossier d''instruction';
COMMENT ON COLUMN proces_verbal.proces_verbal IS 'Identifiant unique';
COMMENT ON COLUMN proces_verbal.numero IS 'Numéro du PV';
COMMENT ON COLUMN proces_verbal.dossier_instruction IS 'Dossier d''instruction auquel est rattaché le PV';
COMMENT ON COLUMN proces_verbal.dossier_instruction_reunion IS 'Demande de passage en réunion du dossier d''instruction';
COMMENT ON COLUMN proces_verbal.modele_edition IS 'Modèle d''édition du PV : par exemple avec ou sans avis';
COMMENT ON COLUMN proces_verbal.date_redaction IS 'Date de rédaction du PV';
COMMENT ON COLUMN proces_verbal.signataire IS 'Signataire du PV';
COMMENT ON COLUMN proces_verbal.genere IS 'Vrai si généré, faux si ajouté';
COMMENT ON COLUMN proces_verbal.om_fichier_finalise IS 'Document généré automatiquement';
COMMENT ON COLUMN proces_verbal.om_fichier_signe IS 'Document déposé par l''utilisateur';
ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_pkey PRIMARY KEY (proces_verbal),
    ADD CONSTRAINT proces_verbal_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction),
    ADD CONSTRAINT proces_verbal_dossier_instruction_reunion_fkey FOREIGN KEY (dossier_instruction_reunion) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion),
    ADD CONSTRAINT proces_verbal_modele_edition_fkey FOREIGN KEY (modele_edition) REFERENCES modele_edition(modele_edition),
    ADD CONSTRAINT proces_verbal_signataire_fkey FOREIGN KEY (signataire) REFERENCES signataire(signataire),
    ADD CONSTRAINT proces_verbal_numero_unique UNIQUE (numero);
CREATE SEQUENCE proces_verbal_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY proces_verbal.proces_verbal;

----
-- END / Création de la table métier procès-verbal
----

-- Homogénité du type du champ complément d'avis entre les tables
-- analyses et dossier_instruction_reunion
ALTER TABLE dossier_instruction_reunion
ALTER avis_complement TYPE text;

----
-- BEGIN / Ajout du paramétrage des modèles d'édition dans analyse
----
-- analyse
ALTER TABLE analyses
    ADD modele_edition_rapport integer NOT NULL,
    ADD modele_edition_compte_rendu integer NOT NULL,
    ADD modele_edition_proces_verbal integer NOT NULL;
COMMENT ON COLUMN analyses.modele_edition_rapport IS 'Modèle d''édition du rapport d''analyse';
COMMENT ON COLUMN analyses.modele_edition_compte_rendu IS 'Modèle d''édition du compte-rendu d''analyse';
COMMENT ON COLUMN analyses.modele_edition_proces_verbal IS 'Modèle d''édition du procès-verbal (auquel un filigranne sera ajouté par traitement puisqu''il s''agit dans ce contexte d''une prévisualisation)';
ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_modele_edition_rapport_fkey FOREIGN KEY (modele_edition_rapport) REFERENCES modele_edition(modele_edition),
    ADD CONSTRAINT analyses_modele_edition_compte_rendu_fkey FOREIGN KEY (modele_edition_compte_rendu) REFERENCES modele_edition(modele_edition),
    ADD CONSTRAINT analyses_modele_edition_proces_verbal_fkey FOREIGN KEY (modele_edition_proces_verbal) REFERENCES modele_edition(modele_edition);
-- type d'analyse (modèles par défaut suivant le type)
ALTER TABLE analyses_type
    ADD modele_edition_rapport integer NULL,
    ADD modele_edition_compte_rendu integer NULL,
    ADD modele_edition_proces_verbal integer NULL;
COMMENT ON COLUMN analyses_type.modele_edition_rapport IS 'Modèle d''édition du rapport d''analyse par défaut pour ce type';
COMMENT ON COLUMN analyses_type.modele_edition_compte_rendu IS 'Modèle d''édition du compte-rendu d''analyse par défaut pour ce type';
COMMENT ON COLUMN analyses_type.modele_edition_proces_verbal IS 'Modèle d''édition du procès-verbal par défaut pour ce type (auquel un filigranne sera ajouté par traitement puisqu''il s''agit dans ce contexte d''une prévisualisation)';
ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_modele_edition_rapport_fkey FOREIGN KEY (modele_edition_rapport) REFERENCES modele_edition(modele_edition),
    ADD CONSTRAINT analyses_modele_edition_compte_rendu_fkey FOREIGN KEY (modele_edition_compte_rendu) REFERENCES modele_edition(modele_edition),
    ADD CONSTRAINT analyses_modele_edition_proces_verbal_fkey FOREIGN KEY (modele_edition_proces_verbal) REFERENCES modele_edition(modele_edition);
----
-- END / Ajout du paramétrage des modèles d'édition dans analyse
----

----
-- BEGIN / Modèles d'édition
----
-- catégorie courrier-type (une par service et une par objet, non modifiable)
-- code : obj-service
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin)
VALUES (nextval('courrier_type_categorie_seq'), 'ANL-ACC', 'Documents générés liés aux Analyses du service accessibilité', NULL, NULL, NULL),
(nextval('courrier_type_categorie_seq'), 'ANL-SI', 'Documents générés liés aux Analyses du service sécurité incendie', NULL, NULL, NULL),
(nextval('courrier_type_categorie_seq'), 'PV-ACC', 'Documents générés liés aux Procès-Verbaux du service accessibilité', NULL, NULL, NULL),
(nextval('courrier_type_categorie_seq'), 'PV-SI', 'Documents générés liés aux Procès-Verbaux du service sécurité incendie', NULL, NULL, NULL);
-- courriers-types (un par édition, non modifiable)
-- code : obj-service-édition
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service)
VALUES (nextval('courrier_type_seq'), 'ANL-SI-RPT', 'Analyse (SI) - Rapport', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'ANL-SI'), NULL),
(nextval('courrier_type_seq'), 'ANL-SI-CRD', 'Analyse (SI) - Compte-rendu', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'ANL-SI'), NULL),
(nextval('courrier_type_seq'), 'ANL-ACC-RPT', 'Analyse (ACC) - Rapport', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'ANL-ACC'), NULL),
(nextval('courrier_type_seq'), 'ANL-ACC-CRD', 'Analyse (ACC) - Compte-rendu', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'ANL-ACC'), NULL),
(nextval('courrier_type_seq'), 'PV-SI', 'Procès-verbal (SI)', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'PV-SI'), NULL),
(nextval('courrier_type_seq'), 'PV-ACC', 'Procès-verbal (ACC)', NULL, NULL, NULL, (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE code = 'PV-ACC'), NULL);
-- modèles (un par défaut pour chaque édition, extensible par l'utilisateur)
-- 1 modèle = 1 courrier-type = 1 lettre-type
-- code : obj-service-édition
UPDATE om_lettretype SET id = 'analyse_rapport_si' WHERE id = 'si_rapport_analyse';
UPDATE om_lettretype SET id = 'analyse_compte_rendu_si' WHERE id = 'si_compte_rendu_analyse';
UPDATE om_lettretype SET id = 'analyse_rapport_acc' WHERE id = 'acc_rapport_analyse';
UPDATE om_lettretype SET id = 'analyse_compte_rendu_acc' WHERE id = 'acc_compte_rendu_analyse';
UPDATE om_lettretype SET id = 'proces_verbal_si' WHERE id = 'si_proces_verbal';
UPDATE om_lettretype SET id = 'proces_verbal_acc' WHERE id = 'acc_proces_verbal';
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type)
VALUES (nextval('modele_edition_seq'), 'Rapport d''analyse sécurité incendie', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'analyse_rapport_si'), NULL, NULL, NULL, 'ANL-SI-RPT', (SELECT courrier_type FROM courrier_type WHERE code = 'ANL-SI-RPT')),
(nextval('modele_edition_seq'), 'Compte-rendu d''analyse sécurité incendie', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'analyse_compte_rendu_si'), NULL, NULL, NULL, 'ANL-SI-CRD', (SELECT courrier_type FROM courrier_type WHERE code = 'ANL-SI-CRD')),
(nextval('modele_edition_seq'), 'Rapport d''analyse accessibilité', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'analyse_rapport_acc'), NULL, NULL, NULL, 'ANL-ACC-RPT', (SELECT courrier_type FROM courrier_type WHERE code = 'ANL-ACC-RPT')),
(nextval('modele_edition_seq'), 'Compte-rendu d''analyse accessibilité', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'analyse_compte_rendu_acc'), NULL, NULL, NULL, 'ANL-ACC-CRD', (SELECT courrier_type FROM courrier_type WHERE code = 'ANL-ACC-CRD')),
(nextval('modele_edition_seq'), 'Procès-verbal sécurité incendie', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'proces_verbal_si'), NULL, NULL, NULL, 'PV-SI', (SELECT courrier_type FROM courrier_type WHERE code = 'PV-SI')),
(nextval('modele_edition_seq'), 'Procès-verbal accessibilité', NULL, (SELECT om_lettretype FROM om_lettretype WHERE id = 'proces_verbal_acc'), NULL, NULL, NULL, 'PV-ACC', (SELECT courrier_type FROM courrier_type WHERE code = 'PV-ACC'));
----
-- FIN / Modèles d'édition
----

----
-- BEGIN / Définition des modèles d'édition par défaut pour les types d'analyse
----
-- SI
UPDATE analyses_type
    SET modele_edition_rapport = (SELECT modele_edition FROM modele_edition WHERE code = 'ANL-SI-RPT'),
    modele_edition_compte_rendu = (SELECT modele_edition FROM modele_edition WHERE code = 'ANL-SI-CRD'),
    modele_edition_proces_verbal = (SELECT modele_edition FROM modele_edition WHERE code = 'PV-SI')
    WHERE code LIKE '%-SI';
-- ACC
UPDATE analyses_type
    SET modele_edition_rapport = (SELECT modele_edition FROM modele_edition WHERE code = 'ANL-ACC-RPT'),
    modele_edition_compte_rendu = (SELECT modele_edition FROM modele_edition WHERE code = 'ANL-ACC-CRD'),
    modele_edition_proces_verbal = (SELECT modele_edition FROM modele_edition WHERE code = 'PV-ACC')
    WHERE code LIKE '%-ACC';
-- Champ obligatoire après insertion du jeu de données
ALTER TABLE analyses_type
    ALTER modele_edition_rapport SET NOT NULL,
    ALTER modele_edition_compte_rendu SET NOT NULL,
    ALTER modele_edition_proces_verbal SET NOT NULL;
----
-- END / Définition des modèles d'édition par défaut pour les types d'analyse
----

----
-- BEGIN / Ajout de la gestion de la régénération de PV
-- On ajoute un marqueur et le numéro du dernier PV
----
ALTER TABLE analyses
    ADD modifiee_sans_gen boolean NOT NULL,
    ADD dernier_pv integer NULL;
COMMENT ON COLUMN analyses.modifiee_sans_gen IS 'Marqueur indiquant, si vrai, qu''il n''y a pas eu de (re)génération de PV après une modification de l''analyse';
COMMENT ON COLUMN analyses.dernier_pv IS 'Dernier procès-verbal généré (peut être regénéré)';
ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_proces_verbal_fkey FOREIGN KEY (dernier_pv) REFERENCES proces_verbal(proces_verbal);
-- END / Ajout de la gestion de la régénération de PV
----

----
-- BEGIN / Ajout de la requête objet PV
----
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode)
VALUES (nextval('om_requete_seq'), 'pv', 'Procès-verbal', 'Tous les champs de fusion de la requête Analyse, ainsi que ceux du PV', '', '', 'objet', 'proces_verbal', '');
----
-- END / Ajout de la requête objet PV et mise à jour des lettres-types associées
----

----
-- BEGIN - Lettres-types procès-verbaux
----
-- PV SI
UPDATE om_lettretype SET
om_sql = (SELECT om_requete FROM om_requete WHERE code ='pv'),
titre_om_htmletat = '<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''>Procès verbal - sécurité incendie<br /></span></p>',
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description sécurité : [dt_desc_si]</p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité I16 : [dt_i16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p> </p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_si';
-- PV ACC
UPDATE om_lettretype SET
om_sql = (SELECT om_requete FROM om_requete WHERE code ='pv'),
titre_om_htmletat = '<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''>Procès verbal - accessibilité<br /></span></p>',
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description accessibilité : [dt_desc_acc]</p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_acc';
----
-- END - Lettres-types procès-verbaux
----

-- Ajout de la liaison signataire/civilité
ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_civilite_fkey FOREIGN KEY (civilite) REFERENCES contact_civilite(contact_civilite);

-- Ajout du champ code à la table etablissement_etat
ALTER TABLE etablissement_etat 
    ADD COLUMN code character varying(7);
COMMENT ON COLUMN etablissement_etat.code IS 'Code de l''état de l''établissement.';

-- Ajout des codes des tables etablissement_nature, etablissement_etat et
-- dossier_coordination_type dans les om_parametre
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'etablissement_nature_periodique', 'ERPR', 1);
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'etablissement_etat_periodique', 'OUVE', 1);
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'dossier_coordination_type_periodique', 'VPS', 1);

-- Ajout d'une liaison entre l'établissement et le DC de visite périodique
ALTER TABLE etablissement 
    ADD COLUMN dossier_coordination_periodique integer;
COMMENT ON COLUMN etablissement.dossier_coordination_periodique IS 'Lien vers le dossier de coordination pour la visite périodique.';
ALTER TABLE etablissement
    ADD CONSTRAINT etablissement_dossier_coordination_periodique_fkey FOREIGN KEY (dossier_coordination_periodique) REFERENCES dossier_coordination(dossier_coordination);
