-- Supprime la contrainte d'unicité sur le champ numero
ALTER TABLE proces_verbal DROP CONSTRAINT proces_verbal_numero_unique;

-- Supprime les numéros des pv externes
UPDATE proces_verbal SET numero = NULL WHERE genere = 'f';

-- Création des séquences
-- /!\ Doit être lancée qu'une seule fois
CREATE OR REPLACE FUNCTION create_sequence_pv() RETURNS boolean AS
$BODY$
DECLARE
proces_verbal RECORD;
BEGIN
FOR proces_verbal IN
    SELECT DISTINCT substring(numero from 1 for 4) as annee, substring(numero from 10 for 1) as service FROM proces_verbal WHERE length(numero) > 10 AND genere = 't'
LOOP
    EXECUTE 'CREATE SEQUENCE proces_verbal_' ||proces_verbal.annee|| '_' ||proces_verbal.service|| '_seq';
END LOOP;
RETURN true;
END;
$BODY$
LANGUAGE 'plpgsql' VOLATILE;
select create_sequence_pv();
DROP FUNCTION IF EXISTS create_sequence_pv();

-- Modification du numéro du PV
UPDATE proces_verbal SET numero = CONCAT(substring(numero from 1 for 4), '/', lpad(nextval(CONCAT('proces_verbal_', substring(numero from 1 for 4), '_', substring(numero from 10 for 1), '_seq'))::text, 5, '0')) WHERE length(numero) > 10 AND genere = 't';
