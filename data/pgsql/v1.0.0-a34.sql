--
-- Commentaires manquants des colonnes dans la table etablissement_unite.
--
COMMENT ON COLUMN etablissement_unite.etat IS 'État de l''unité. Valeurs possibles : valide, enprojet.';
COMMENT ON COLUMN etablissement_unite.archive IS 'Marqueur d''archivage de l''unité.';
COMMENT ON COLUMN etablissement_unite.dossier_instruction IS 'Lien vers un dossier d''instruction : une unité analysée est liée à un dossier d''instruction.';
COMMENT ON COLUMN etablissement_unite.etablissement_unite_lie IS 'Lien vers une unité : une unité analysée peut avoir pour origine une autre unité.';
COMMENT ON COLUMN etablissement_unite.adap_date_validation IS 'ADAP Date de validation.';
COMMENT ON COLUMN etablissement_unite.adap_duree_validite IS 'ADAP Durée de validité (en années).';
COMMENT ON COLUMN etablissement_unite.adap_annee_debut_travaux IS 'ADAP Année de début des travaux.';
COMMENT ON COLUMN etablissement_unite.adap_annee_fin_travaux IS 'ADAP Année de fin des travaux.';

