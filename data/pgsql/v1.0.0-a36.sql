---------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a36
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- BEGIN - Caractérisation d'un arrêté
-- Ajout d'informations sur une décision d'autorité de police pour la marquer
-- explicitement comme un arrêté et pouvoir caractériser cet arrêté.
--

ALTER TABLE autorite_police_decision

ADD COLUMN type_arrete boolean,

ADD COLUMN arrete_reglementaire boolean,
ADD COLUMN arrete_notification boolean,
ADD COLUMN arrete_publication boolean,
ADD COLUMN arrete_temporaire boolean,

ADD COLUMN nomenclature_actes_nature character varying(30),
ADD COLUMN nomenclature_actes_matiere_niv1 character varying(250),
ADD COLUMN nomenclature_actes_matiere_niv2 character varying(250);

--
-- END - Caractérisation d'un arrêté
--

--
-- START / #407 — Ajout du bouton localiser une séléction de dossiers ou d'établissements
--

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );

INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_referentiel_erp_geoaria',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_referentiel_erp_geoaria' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );

--
-- END / #407 — Ajout du bouton localiser une séléction de dossiers ou d'établissements
--

--
-- BEGIN - Ajout du numéro d'arrêté dans les documents générés.
--

ALTER TABLE courrier ADD COLUMN arrete_numero character varying(30);

--
-- END - Ajout du numéro d'arrêté dans les documents générés.
--

