-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- Création des widgets de tableaux de bord
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (1, 'Activité du service', 'activite_service', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (2, 'Dossiers de coordination à qualifier', 'dossier_coordination_a_qualifier', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (3, 'Mon activité', 'mon_activite', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (4, 'Mes dossiers plans', 'dossier_instruction_mes_plans', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (5, 'Mes dossiers visites', 'dossier_instruction_mes_visites', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (6, 'Mes dossiers en réunion', 'mes_di_en_reunions', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (7, 'Mes visites à réaliser', 'mes_visites_a_realiser', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (8, 'Programmations urgentes', 'programmation_urgentes', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (9, 'Programmations à valider', 'programmation_a_valider', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (10, 'Convocations exploitants à envoyer', 'convocations_exploitants_a_envoyer', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (11, 'Convocations membres à envoyer', 'convocations_membres_a_envoyer', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (12, 'Établissements NPAI', 'etablissements_npai', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (13, 'Analyses à valider', 'analyse_a_valider', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (14, 'Analyses à acter', 'analyse_a_acter', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (15, 'Mes infos', 'mes_infos', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (16, 'Autorités de police qui n''ont pas été notifiées ou exécutées', 'autorites_police_non_notifiees_executees', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (17, 'Mes documents entrants non lus', 'mes_documents_entrants_non_lus', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (18, 'Documents entrants à valider', 'documents_entrants_a_valider', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (19, 'Documents entrants suivis', 'documents_entrants_suivis', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (20, 'Profil non configuré', '', 'Votre profil ne peut accéder à aucune fonction du logiciel. Veuillez contacter l''administrateur pour que vos permissions soient configurées.', 'web');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (21, 'Dossiers de coordination à clôturer', 'dossier_coordination_a_cloturer', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (22, 'Mes messages', 'message_mes_non_lu', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (23, 'Documents générés à éditer', 'documents_generes_a_editer', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (24, 'Documents générés en attente de signature', 'documents_generes_attente_signature', '', 'file');
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (25, 'Documents générés en attente de retour AR', 'documents_generes_attente_retour_ar', '', 'file');


-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- Création des profils, utilisateurs et tableaux de bord
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

--
--
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (1, 'PROFIL NON CONFIGURÉ', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (2, 'nonconfig', 'nospam@atreal.fr', 'nonconfig', '135201940a2bfaf3b878d6e3887c1343', 1, 'db', 1);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 1, 'C1', 1, 20);

--
-- Profil 'CADRE SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (2, 'CADRE SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (5, 'cadre-si', 'nospam@atreal.fr', 'cadre-si', '28f8247f9970bdfcf101911d76a1d19e', 1, 'db', 2);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C1', 1, 1);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 2, 2);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C1', 2, 8);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 3, 9);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 4, 13);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 5, 18);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 6, 19);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 2, 'C2', 7, 21);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));


--
-- Profil 'CADRE ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (3, 'CADRE ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (6, 'cadre-acc', 'nospam@atreal.fr', 'cadre-acc', '06ff6e28cfb3cce326fa7bfc2f17b9f9', 1, 'db', 3);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C1', 1, 1);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 2, 2);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C1', 2, 8);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 3, 9);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 4, 13);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 5, 18);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 6, 19);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 3, 'C2', 7, 21);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));


--
-- Profil 'TECHNICIEN SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (4, 'TECHNICIEN SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (9, 'technicien-si', 'nospam@atreal.fr', 'technicien-si', '27ca8279fed45a2f0259d3390743a782', 1, 'db', 4);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C1', 1, 3);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C1', 2, 6);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C2', 2, 4);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C2', 3, 5);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C1', 3, 7);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 4, 'C1', 3, 17);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));


--
-- Profil 'TECHNICIEN ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (5, 'TECHNICIEN ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (10, 'technicien-acc', 'nospam@atreal.fr', 'technicien-acc', '0ae54d3a09a236243800fb29dca264a3', 1, 'db', 5);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C1', 1, 3);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C1', 2, 6);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C2', 2, 4);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C2', 2, 5);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C1', 3, 7);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 5, 'C1', 3, 17);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));


--
-- Profil 'SECRETAIRE SI'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (6, 'SECRETAIRE SI', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (11, 'secretaire-si', 'nospam@atreal.fr', 'secretaire-si', '6b8a509cde10f837639417c2ea101106', 1, 'db', 6);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C1', 2,  1);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C2', 2, 10);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C2', 2, 11);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C2', 2, 12);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 6, 'C2', 2, 14);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));


--
-- Profil 'SECRETAIRE ACC'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (7, 'SECRETAIRE ACC', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (12, 'secretaire-acc', 'nospam@atreal.fr', 'secretaire-acc', 'b9f374fbe7696fb2b07ce4e05d459b35', 1, 'db', 7);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C2', 1, 15);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C1', 2,  1);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C2', 2, 10);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C2', 2, 11);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C2', 2, 12);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 7, 'C2', 2, 14);
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));


--
-- Profil 'ADMINISTRATEUR'
--
INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (8, 'ADMINISTRATEUR', 0);
INSERT INTO om_utilisateur (om_utilisateur, nom, email, login, pwd, om_collectivite, om_type, om_profil) VALUES (1, 'Administrateur', 'contact@openmairie.org', 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 'DB', 8);

