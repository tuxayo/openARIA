--------------------------------------------------------------------------------
-- Script d'installation
--
-- ATTENTION ce script peut supprimer des données de votre base de données
-- il n'est à utiliser qu'en connaissance de cause
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Usage :
-- cd data/pgsql/
-- dropdb openaria && createdb openaria && psql openaria -f install.sql

--
START TRANSACTION;

--
\set schema 'openaria'

-- Initialisation de postgis : A CHANGER selon les configurations
-- A commenter/décommenter pour initialiser postgis
-- --> postgis 1.5 / postgresql 9.1
--\i /usr/share/postgresql/9.1/contrib/postgis-1.5/postgis.sql
--\i /usr/share/postgresql/9.1/contrib/postgis-1.5/spatial_ref_sys.sql
-- --> postgis 2
CREATE EXTENSION IF NOT EXISTS postgis;

-- Suppression, Création et Utilisation du schéma
DROP SCHEMA IF EXISTS :schema CASCADE;
CREATE SCHEMA :schema;
SET search_path = :schema, public, pg_catalog;

-- Instructions de base du framework openmairie
\i init.sql

-- Instructions de base de l'applicatif 
\i init_metier.sql

-- Instructions pour l'utilisation de postgis
\i init_metier_sig.sql

-- Initialisation du paramétrage
\i init_permissions.sql
\i init_parametrage.sql
\i init_parametrage_permissions.sql
\i init_parametrage_permissions_matrice.sql
\i init_parametrage_editions_1.sql
\i init_parametrage_editions_2.sql
\i init_parametrage_sig.sql

-- Initialisation d'un jeu de données
-- A commenter/décommenter pour installer un jeu de données
\i init_data.sql

-- Mise à jour des séquences
\i update_sequences.sql

-- Mise à jour depuis la dernière version (en cours de développement)
\i v1.2.2.dev0.sql
\i v1.2.2.dev0.init_data.sql

-- Mise à jour des séquences
\i update_sequences.sql

--
COMMIT;

