
--
-- START - Unités d'accessibilité
--
ALTER TABLE etablissement_unite ADD COLUMN etat character varying(20);
ALTER TABLE etablissement_unite ADD COLUMN archive boolean;
ALTER TABLE etablissement_unite DROP COLUMN date_derniere_cca;
ALTER TABLE etablissement_unite DROP COLUMN avis_derniere_cca;
ALTER TABLE etablissement_unite RENAME acc_consignes_om_html TO acc_notes_om_html;
ALTER TABLE etablissement_unite RENAME acc_descriptif_om_html TO acc_descriptif_ua_om_html;
ALTER TABLE etablissement_unite ADD COLUMN dossier_instruction integer;
ALTER TABLE etablissement_unite ADD COLUMN etablissement_unite_lie integer;
ALTER TABLE etablissement_unite ADD COLUMN adap_date_validation date;
ALTER TABLE etablissement_unite ADD COLUMN adap_duree_validite integer;
ALTER TABLE etablissement_unite ADD COLUMN adap_annee_debut_travaux integer;
ALTER TABLE etablissement_unite ADD COLUMN adap_annee_fin_travaux integer;
ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction);
ALTER TABLE ONLY etablissement_unite
    ADD CONSTRAINT etablissement_unite_etablissement_unite_fkey FOREIGN KEY (etablissement_unite_lie) REFERENCES etablissement_unite(etablissement_unite);
UPDATE etablissement_unite SET etat='valide';
UPDATE etablissement_unite SET archive=FALSE;
ALTER TABLE etablissement_unite ALTER COLUMN archive SET NOT NULL;
--
-- END - Unités d'accessibilité
--


--
-- START - Établissement
--
ALTER TABLE etablissement
	DROP acc_handicap_auditif,
	DROP acc_handicap_mental,
	DROP acc_handicap_physique,
	DROP acc_handicap_visuel,
	DROP acc_places_stationnement_amenagees,
	DROP acc_ascenseur,
	DROP acc_elevateur,
	DROP acc_boucle_magnetique,
	DROP acc_sanitaire,
	DROP acc_places_assises_public,
	DROP acc_chambres_amenagees,
	DROP acc_douche,
	DROP acc_dernier_plan_date,
	DROP acc_dernier_plan_avis,
	DROP acc_derogation_scda;
--
-- END - Établissement
--