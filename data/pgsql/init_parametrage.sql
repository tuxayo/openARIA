--
-- PostgreSQL database dump
--

-- SET statement_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;

-- SET search_path = openaria, pg_catalog;

--
-- Data for Name: om_collectivite; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO om_collectivite (om_collectivite, libelle, niveau) VALUES (1, 'MARSEILLE', '1');


--
-- Name: om_collectivite_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_collectivite_seq', 2, false);


--
-- Data for Name: om_parametre; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (1, 'ville', 'MARSEILLE', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (4, 'option_referentiel_patrimoine', 'false', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (5, 'etablissement_code_prefixe', 'T', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (6, 'etablissement_nature_periodique', 'ERPR', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (7, 'etablissement_etat_periodique', 'OUVE', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (8, 'dossier_coordination_type_periodique', 'VPS', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (9, 'etablissement_nature_erpr', 'ERPR', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (10, 'option_sig', 'aucun', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (11, 'departement', '013', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (12, 'commune', '055', 1);
INSERT INTO om_parametre (om_parametre, libelle, valeur, om_collectivite) VALUES (13, 'insee', '13055', 1);


--
-- Name: om_parametre_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('om_parametre_seq', 14, false);


--
-- Data for Name: service; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO service (service, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (1, 'ACC', 'Accessibilité', 'Service d''accessibilité', NULL, NULL);
INSERT INTO service (service, code, libelle, description, om_validite_debut, om_validite_fin) VALUES (2, 'SI', 'Sécurité Incendie', 'Service de la sécurité incendie', NULL, NULL);


--
-- Name: service_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('service_seq', 3, false);


--
-- PostgreSQL database dump complete
--

