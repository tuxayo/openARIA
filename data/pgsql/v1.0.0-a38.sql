---------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a38
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- START / #427 — Développement "Reprise des fichiers"
--

INSERT INTO piece_type (piece_type, libelle, code, description, om_validite_debut, om_validite_fin)
SELECT nextval('piece_type_seq'), 'Arrêté', 'ARRETE', NULL, NULL, NULL
WHERE
    NOT EXISTS (
        SELECT piece_type FROM piece_type WHERE code = 'ARRETE'
    );

--
-- END / #427 — Développement "Reprise des fichiers"
--
