#! /bin/bash
##
# Ce script permet de générer les fichiers sql d'initialisation de la base de
# données pour permettre de publier une nouvelle version facilement
#
# @package openaria
# @version SVN : $Id: make_init.sh 2467 2013-09-09 08:01:04Z fmichon $
##

schema="openaria"
database="openaria"

# Génération du fichier init.sql
sudo su postgres -c "pg_dump --column-inserts -s -O -n $schema -t $schema.om_* $database" > init.sql

# Génération du fichier init_metier.sql
sudo su postgres -c "pg_dump --column-inserts -s -O -n $schema -T $schema.om_* $database" > init_metier.sql

# Génération du fichier init_parametrage.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.om_collectivite -t $schema.om_parametre -t $schema.service $database" > init_parametrage.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.om_logo -t $schema.om_requete -t $schema.om_sousetat -t $schema.om_etat -t $schema.om_lettretype $database" > init_parametrage_editions_1.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.courrier_type_categorie -t $schema.courrier_type -t $schema.modele_edition $database" > init_parametrage_editions_2.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -t $schema.om_sig_* $database" > init_parametrage_sig.sql

# Génération du fichier init_data.sql
sudo su postgres -c "pg_dump --column-inserts -a -O -n $schema -T $schema.service -T $schema.courrier_type_categorie -T $schema.courrier_type -T $schema.modele_edition -T $schema.om_* $database" > init_data.sql

# Suppression du schéma
sed -i "s/CREATE SCHEMA $schema;/-- CREATE SCHEMA $schema;/g" init*.sql
sed -i "s/^SET/-- SET/g" init*.sql

