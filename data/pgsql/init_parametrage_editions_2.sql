--
-- PostgreSQL database dump
--

-- SET statement_timeout = 0;
-- SET client_encoding = 'UTF8';
-- SET standard_conforming_strings = on;
-- SET check_function_bodies = false;
-- SET client_min_messages = warning;

-- SET search_path = openaria, pg_catalog;

--
-- Data for Name: courrier_type_categorie; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (1, 'REU', 'Éditions spécifiques aux réunions', '', NULL, NULL, 'reunion');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (2, 'ANL', 'Éditions spécifiques aux analyses', NULL, NULL, NULL, 'analyses');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (13, 'PRG', 'Éditions spécifiques aux programmations', NULL, NULL, NULL, 'programmation');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (15, 'DI', 'Documents générés liés aux dossiers d''instruction', '', NULL, NULL, 'dossier_instruction');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (14, 'DC', 'Documents générés liés aux dossiers de coordination', '', NULL, NULL, 'dossier_coordination');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (16, 'ET', 'Documents générés liés aux établissements', '', NULL, NULL, 'etablissement');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (12, 'CI', 'Documents générés liés aux dossiers de coordination et aux dossiers d''instruction', '', NULL, NULL, 'dossier_coordination;dossier_instruction');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (10, 'EC', 'Documents générés liés aux établissements et aux dossiers de coordination', '', NULL, NULL, 'etablissement;dossier_coordination');
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin, objet) VALUES (11, 'ECI', 'Documents générés liés aux établissements, aux dossiers de coordination et aux dossiers d''instruction', '', NULL, NULL, 'etablissement;dossier_coordination;dossier_instruction');


--
-- Data for Name: courrier_type; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (1, 'EVN', 'événement', NULL, NULL, NULL, 11, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (3, 'INCOMP', 'Incomplétude', NULL, NULL, NULL, 12, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (6, 'ENVPV', 'Envoi d''un Procés-Verbal', NULL, NULL, NULL, 11, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (5, 'COUS', 'Courrier simple', NULL, NULL, NULL, 11, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (8, 'REU-ODJ', '(*) Réunion - Ordre du jour', '', NULL, NULL, 1, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (9, 'REU-FP', '(*) Réunion - Feuille de présence', '', NULL, NULL, 1, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (7, 'REU-CRG', '(*) Réunion - Compte rendu', '', NULL, NULL, 1, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (10, 'REU-CRA', '(*) Réunion - Compte rendu d''avis', '', NULL, NULL, 1, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (2, 'DEC', '(*) Décision', '', NULL, NULL, 14, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (4, 'NOTIFAP', '(*) Notification d''Autorité de Police', '', NULL, NULL, 14, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (12, 'ANL-SI-CRD', '(*) Analyse - Compte-rendu SI', '', NULL, NULL, 2, 2);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (14, 'ANL-ACC-CRD', '(*) Analyse - Compte-rendu ACC', '', NULL, NULL, 2, 1);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (11, 'ANL-SI-RPT', '(*) Analyse - Rapport SI', '', NULL, NULL, 2, 2);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (13, 'ANL-ACC-RPT', '(*) Analyse - Rapport ACC', '', NULL, NULL, 2, 1);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (15, 'ANL-SI-PV', '(*) Analyse - Procès-verbal SI', '', NULL, NULL, 2, 2);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (16, 'ANL-ACC-PV', '(*) Analyse - Procès-verbal ACC', '', NULL, NULL, 2, 1);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (17, 'DG-ETAB', 'Document Générés - Établissement', '', NULL, NULL, 16, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (18, 'DG-DC', 'Document Générés - Dossier de coordination', '', NULL, NULL, 14, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) VALUES (19, 'DG-DI', 'Document Générés - Dossier d''instruction', '', NULL, NULL, 15, NULL);


--
-- Name: courrier_type_categorie_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('courrier_type_categorie_seq', 17, false);


--
-- Name: courrier_type_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('courrier_type_seq', 20, false);


--
-- Data for Name: modele_edition; Type: TABLE DATA; Schema: openaria; Owner: -
--

INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (7, 'Rapport d''analyse sécurité incendie', NULL, 4, NULL, NULL, NULL, 'ANL-SI-RPT', 11);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (8, 'Compte-rendu d''analyse sécurité incendie', NULL, 6, NULL, NULL, NULL, 'ANL-SI-CRD', 12);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (9, 'Rapport d''analyse accessibilité', NULL, 5, NULL, NULL, NULL, 'ANL-ACC-RPT', 13);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (10, 'Compte-rendu d''analyse accessibilité', NULL, 7, NULL, NULL, NULL, 'ANL-ACC-CRD', 14);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (11, 'Procès-verbal sécurité incendie', NULL, 8, NULL, NULL, NULL, 'PV-SI', 15);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (12, 'Procès-verbal accessibilité', NULL, 9, NULL, NULL, NULL, 'PV-ACC', 16);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (13, 'Notification d''Autorité de Police', 'Édition de la lettre de notification d''Autorité de Police.', 1, NULL, NULL, NULL, 'NOTIF_AP', 4);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (19, 'Courrier de courtoisie', '', 18, NULL, NULL, NULL, 'CC', 5);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (4, 'Réunion - Compte rendu d''avis', '', 13, NULL, NULL, NULL, 'REU-CRA', 10);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (2, 'Réunion - Compte-rendu général', '', 11, NULL, NULL, NULL, 'REU-CRG', 7);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (16, 'Réunion - Compte-rendu général CCA', '', 16, NULL, NULL, NULL, 'REU-CRG-CCA', 7);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (17, 'Réunion - Compte-rendu général CCS', '', 17, NULL, NULL, NULL, 'REU-CRG-CCS', 7);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (5, 'Réunion - Feuille de présence', '', 12, NULL, NULL, NULL, 'REU-FP', 9);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (3, 'Réunion - Ordre du jour', '', 10, NULL, NULL, NULL, 'REU-ODJ', 8);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (14, 'Réunion - Ordre du jour CCA', '', 14, NULL, NULL, NULL, 'REU-ODJ-CCA', 8);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (15, 'Réunion - Ordre du jour CCS', '', 15, NULL, NULL, NULL, 'REU-ODJ-CCS', 8);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (18, 'Courrier simple', '', 18, NULL, NULL, NULL, 'CS', 5);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (1, '(*) Convocation exploitant', 'Convocation des exploitants aux visites. Le code ''CONVOC_EXPL'' ne doit pas être modifié.', 3, NULL, NULL, NULL, 'CONVOC_EXPL', 1);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (6, '(*) Annulation convocation exploitant', 'Édition de la lettre d''annulation de convocation des exploitants aux visites.', 1, NULL, NULL, NULL, 'ANN_CONVOC_EXPL', 1);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (20, 'ME - Courrier standard (ETAB)', '', 101, NULL, NULL, NULL, 'ME-CS-ETAB', 17);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (21, 'ME - Courrier standard (DC)', '', 102, NULL, NULL, NULL, 'ME-CS-DC', 18);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (22, 'ME - Courrier standard (DI)', '', 103, NULL, NULL, NULL, 'ME-CS-DI', 19);
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES (23, 'ME - Arrêté d''ouverture', '', 115, NULL, NULL, NULL, 'ME-ART-OUV', 2);


--
-- Name: modele_edition_seq; Type: SEQUENCE SET; Schema: openaria; Owner: -
--

SELECT pg_catalog.setval('modele_edition_seq', 24, false);


--
-- PostgreSQL database dump complete
--

