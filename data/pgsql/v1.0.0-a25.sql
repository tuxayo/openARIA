-- Suppression de clé étrangère analyse dans le dossier d'instruction
-- (liaison déjà présente dans l'autre sens)
ALTER TABLE dossier_instruction DROP analyses;