----
-- BEGIN / Prescriptions de l'analyse
-- Modification du paramétrage et des champs requis.
----

-- Suppression de l'obligation d'une prescription spécifique
ALTER TABLE prescription
ALTER ps_description_om_html DROP NOT NULL;

-- Ajout de toutes les catégories aux prescriptions réglementaires
INSERT INTO lien_prescription_reglementaire_etablissement_categorie
VALUES
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '1')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '2')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '3')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '4')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '5')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '1')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '2')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '3')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '4')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '5')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '1')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '2')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '3')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '4')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '5')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '1')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '2')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '3')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '4')),
    (nextval('lien_prescription_reglementaire_etablissement_categorie_seq'), (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'), (SELECT etablissement_categorie FROM etablissement_categorie WHERE libelle = '5'));

----
-- END / Prescriptions de l'analyse
----