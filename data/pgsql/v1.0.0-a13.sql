--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a13
--
-- XXX Ce fichier doit être renommé en v1.0.0-a13.sql au moment de la release
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Ajout du code permettant de définir un établissement ERP Référentiel dans
-- om_parametre
INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'etablissement_nature_erpr', 'ERPR', 1);

-- BEGIN / Suppression champ descriptif etablissement en doublon dans l'analyse
ALTER TABLE analyses
DROP si_descriptif_om_html,
DROP acc_descriptif_om_html;
COMMENT ON COLUMN analyses.descriptif_etablissement_om_html IS 'Descriptif de l''établissement du point de vue du service de l''analyse';
-- END / Suppression champ descriptif etablissement en doublon dans l'analyse