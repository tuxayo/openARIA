---------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a35
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- BEGIN - Ajout d'un marqueur "est géolocalisé" sur les tables DC et étab
--

ALTER TABLE dossier_coordination ADD COLUMN geolocalise boolean  DEFAULT false;
COMMENT ON COLUMN dossier_coordination.geolocalise IS 'Marqueur signalant que le dossier a été localisé sur le SIG';

ALTER TABLE etablissement ADD COLUMN geolocalise boolean  DEFAULT false;
COMMENT ON COLUMN etablissement.geolocalise IS 'Marqueur signalant que l''établissement a été localisé sur le SIG';

--
-- END - Ajout d'un marqueur "est géolocalisé" sur les tables DC et étab
-- 

--
-- BEGIN - Suppression du champ inutilise 'proces_verbal.om_fichier_finalise'.
--
ALTER TABLE proces_verbal DROP COLUMN om_fichier_finalise;
--
-- END - Suppression du champ inutilise 'proces_verbal.om_fichier_finalise'.
--

--
-- BEGIN - Ajout des droits de localisation d'une sélection de dossiers et d'établissements
--

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 2);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 4);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 5);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 6);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'dossier_coordination_geoaria', 7);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 2);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 4);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 5);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 6);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'etablissement_tous_geoaria', 7);

--
-- END - Ajout des droits de localisation d'une sélection de dossiers et d'établissements
--

--
-- BEGIN // GESTION DES CONTRAINTES
--

--
-- Création de la table contrainte
--
CREATE TABLE contrainte (
    contrainte integer NOT NULL,
    id_referentiel character varying(250),
    nature character varying(10) NOT NULL,
    groupe character varying(250),
    sousgroupe character varying(250),
    libelle character varying(250) NOT NULL,
    texte text,
    texte_surcharge text,
    ordre_d_affichage integer,
    lie_a_un_referentiel boolean DEFAULT false,
    om_validite_debut date,
    om_validite_fin date
);
--
CREATE SEQUENCE contrainte_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE contrainte_seq OWNED BY contrainte.contrainte;
--
ALTER TABLE ONLY contrainte
    ADD CONSTRAINT contrainte_pkey PRIMARY KEY (contrainte);
--
COMMENT ON TABLE contrainte IS 'Table de référence des contraintes';
COMMENT ON COLUMN contrainte.contrainte IS 'Identifiant unique.';
COMMENT ON COLUMN contrainte.id_referentiel IS 'Correspond à l’identifiant dans le référentiel SIG.';
COMMENT ON COLUMN contrainte.nature IS 'Nature de la contrainte (POS/PLU/CC/RNU).';
COMMENT ON COLUMN contrainte.groupe IS 'Texte libre représentant la catégorie de la contrainte.';
COMMENT ON COLUMN contrainte.sousgroupe IS 'Texte libre représentant la sous-catégorie de la contrainte.';
COMMENT ON COLUMN contrainte.libelle IS 'Phrase résumé de la contrainte utilisée dans les interfaces de sélection.';
COMMENT ON COLUMN contrainte.texte IS 'Texte de la contrainte à respecter.';
COMMENT ON COLUMN contrainte.texte_surcharge IS 'Éventuelle surcharge du texte standard.';
COMMENT ON COLUMN contrainte.ordre_d_affichage IS 'Éventuelle clé de tri de la contrainte à l''intérieur de son groupe et sous-groupe.';
COMMENT ON COLUMN contrainte.lie_a_un_referentiel IS 'Si vrai, contrainte récupérée depuis le SIG.';
COMMENT ON COLUMN contrainte.om_validite_debut IS 'Date de début de validité. Permet l''archivage de la contrainte.';
COMMENT ON COLUMN contrainte.om_validite_fin IS 'Date de fin de validité. Permet l''archivage de la contrainte.';
--
-- Liaison DC / contrainte
--
CREATE TABLE lien_contrainte_dossier_coordination (
    lien_contrainte_dossier_coordination integer NOT NULL,
    dossier_coordination integer NOT NULL,
    contrainte integer NOT NULL,
    texte_complete text,
    recuperee boolean DEFAULT false
);
--
CREATE SEQUENCE lien_contrainte_dossier_coordination_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_contrainte_dossier_coordination_seq OWNED BY lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination;
--
ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_pkey PRIMARY KEY (lien_contrainte_dossier_coordination);
ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_contrainte_fkey FOREIGN KEY (contrainte) REFERENCES contrainte(contrainte);
ALTER TABLE ONLY lien_contrainte_dossier_coordination
    ADD CONSTRAINT lien_contrainte_dossier_coordination_dossier_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);
--
COMMENT ON TABLE lien_contrainte_dossier_coordination IS 'Table des contraintes appliquées au DC';
COMMENT ON COLUMN lien_contrainte_dossier_coordination.lien_contrainte_dossier_coordination IS 'Identifiant unique';
COMMENT ON COLUMN lien_contrainte_dossier_coordination.dossier_coordination IS 'DC auquel est appliqué la contrainte';
COMMENT ON COLUMN lien_contrainte_dossier_coordination.contrainte IS 'Contrainte qui est appliquée au DC';
COMMENT ON COLUMN lien_contrainte_dossier_coordination.texte_complete IS 'Texte complété de la contrainte';
COMMENT ON COLUMN lien_contrainte_dossier_coordination.recuperee IS 'Si vrai, contrainte récupérée depuis le SIG pour ce DC.';
--
-- Liaison établissement / contrainte
--
CREATE TABLE lien_contrainte_etablissement (
    lien_contrainte_etablissement integer NOT NULL,
    etablissement integer NOT NULL,
    contrainte integer NOT NULL,
    texte_complete text,
    recuperee boolean DEFAULT false
);
--
CREATE SEQUENCE lien_contrainte_etablissement_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_contrainte_etablissement_seq OWNED BY lien_contrainte_etablissement.lien_contrainte_etablissement;
--
ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_pkey PRIMARY KEY (lien_contrainte_etablissement);
ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_contrainte_fkey FOREIGN KEY (contrainte) REFERENCES contrainte(contrainte);
ALTER TABLE ONLY lien_contrainte_etablissement
    ADD CONSTRAINT lien_contrainte_etablissement_dossier_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);
--
COMMENT ON TABLE lien_contrainte_etablissement IS 'Table des contraintes appliquées au DC';
COMMENT ON COLUMN lien_contrainte_etablissement.lien_contrainte_etablissement IS 'Identifiant unique';
COMMENT ON COLUMN lien_contrainte_etablissement.etablissement IS 'Établissement auquel est appliqué la contrainte';
COMMENT ON COLUMN lien_contrainte_etablissement.contrainte IS 'Contrainte qui est appliquée à l''établissement';
COMMENT ON COLUMN lien_contrainte_etablissement.texte_complete IS 'Texte complété de la contrainte';
COMMENT ON COLUMN lien_contrainte_etablissement.recuperee IS 'Si vrai, contrainte récupérée depuis le SIG pour cet établissement.';
--
-- Droits
--
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
-- Paramétrage
(nextval('om_droit_seq'), 'contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')),
-- Synchronisation
(nextval('om_droit_seq'), 'contrainte_synchronisation', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')),
(nextval('om_droit_seq'), 'contrainte_synchronisation_synchroniser', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')),
-- Application DC
(nextval('om_droit_seq'), 'lien_contrainte_dossier_coordination', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')),
(nextval('om_droit_seq'), 'lien_contrainte_dossier_coordination', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')),
(nextval('om_droit_seq'), 'lien_contrainte_dossier_coordination', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')),
(nextval('om_droit_seq'), 'lien_contrainte_dossier_coordination', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')),
(nextval('om_droit_seq'), 'dossier_coordination_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')),
(nextval('om_droit_seq'), 'dossier_coordination_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')),
(nextval('om_droit_seq'), 'dossier_coordination_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')),
(nextval('om_droit_seq'), 'dossier_coordination_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')),
-- Application établissement
(nextval('om_droit_seq'), 'lien_contrainte_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')),
(nextval('om_droit_seq'), 'lien_contrainte_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')),
(nextval('om_droit_seq'), 'lien_contrainte_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')),
(nextval('om_droit_seq'), 'lien_contrainte_etablissement', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')),
(nextval('om_droit_seq'), 'etablissement_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')),
(nextval('om_droit_seq'), 'etablissement_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')),
(nextval('om_droit_seq'), 'etablissement_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')),
(nextval('om_droit_seq'), 'etablissement_contrainte', (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'));

--
-- END // GESTION DES CONTRAINTES
--

--
-- BEGIN // GEOCODER TOUS LES ÉLÉMENTS NON LOCALISES
--
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
(nextval('om_droit_seq'), 'dossier_coordination_geocoder_tous', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')),
(nextval('om_droit_seq'), 'dossier_coordination_geocoder_tous_geocoder_tous', (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR'));
--
-- END // GEOCODER TOUS LES ÉLÉMENTS NON LOCALISES
--

