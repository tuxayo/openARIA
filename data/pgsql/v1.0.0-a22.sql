--
UPDATE om_parametre SET libelle = 'prefixe_edition_substitution_vars' WHERE libelle = 'prefixe_edition_subtitution_vars';

--
INSERT INTO om_requete (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) VALUES 
(	
	12, 
	'document_genere', 
	'Contexte ''document genere''', 
	'Tous les champs de fusion spécifiques à une analyse, son DI, son DC et son établissement s''il existe.', 
	'', 
	'', 
	'objet', 
	'courrier', 
	'get_all_merge_fields'
);

--
UPDATE om_lettretype SET om_sql = 12 WHERE om_sql = 8 OR om_sql = 9 OR om_sql = 10;

--
DELETE FROM om_requete WHERE om_requete = 8 OR om_requete = 9 OR om_requete = 10;

--
UPDATE om_requete SET methode = 'get_all_merge_fields' WHERE om_requete = 2 or om_requete = 11;
UPDATE om_requete SET classe = 'visite' WHERE om_requete = 11;

