--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a18
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description sécurité : [dt_desc_si]</p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''anl_essai_realise'' class=''mce_sousetat''>Analyse - Essais réalisés</span></p>
<p> </p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_si';

--
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description accessibilité : [dt_desc_acc]</p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span id=''anl_essai_realise'' class=''mce_sousetat''>Analyse - Essais réalisés</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_acc';

-- Champs obligatoire sur la table courrier
ALTER TABLE courrier 
    ALTER COLUMN code_barres SET NOT NULL,
    ALTER COLUMN modele_edition SET NOT NULL,
    ALTER COLUMN courrier_type SET NOT NULL;

--
DROP SEQUENCE courrier_param_seq;

--
DELETE from om_parametre WHERE libelle = 'option_localisation';

--
DROP TABLE message_ws;

--
ALTER TABLE analyses RENAME COLUMN si_conformite_i16 TO si_conformite_l16;
ALTER TABLE etablissement RENAME COLUMN si_conformite_i16 TO si_conformite_l16;
COMMENT ON COLUMN analyses.si_conformite_l16 IS 'Conforme au référentiel L16 ?';
COMMENT ON COLUMN etablissement.si_conformite_l16 IS 'Conforme au référentiel L16 ?';

--
ALTER TABLE visite DROP COLUMN duree_prevue;

----
-- BEGIN / Correction édition compte-rendu spécifique de réunion
----
-- Requête
UPDATE om_requete SET
merge_fields = '<table><thead><tr>
                        <th colspan=2>Enregistrement de type Demande de passage en réunion</th></tr></thead><tbody><tr><td>[dir_avis]</td><td>proposition d''avis</td></tr><tr><td>[dir_avis_comp]</td><td>complément de la proposition d''avis</td></tr><tr><td>[dir_ordre]</td><td>ordre</td></tr><tr style=height: 10px !important;><td colspan=2></td></tr><tr>
                        <th colspan=2>Enregistrement de type avis</th></tr></thead><tbody><tr><td>[avis_libelle]</td><td>libellé</td></tr><tr><td>[avis_complement]</td><td>complément</td></tr><tr><td>[avis_motivation]</td><td>motivation</td></tr><tr style=height: 10px !important;><td colspan=2></td></tr><tr>
                        <th colspan=2>Enregistrement de type dossier d''instruction</th></tr></thead><tbody><tr><td>[di_libelle]</td><td>libellé</td></tr><tr style=height: 10px !important;><td colspan=2></td></tr><tr>
                        <th colspan=2>Enregistrement de type établissement</th></tr></thead><tbody><tr><td>[et_code]</td><td>code</td></tr><tr><td>[et_libelle]</td><td>libellé</td></tr><tr style=height: 10px !important;><td colspan=2></td></tr><tr>
                        <th colspan=2>Enregistrement de type réunion</th></tr></thead><tbody><tr><td>[rn_date]</td><td>date</td></tr><tr><td>[rn_heure]</td><td>heure</td></tr><tr><td>[rn_salle]</td><td>salle</td></tr><tr><td>[rn_adresse]</td><td>adresse</td></tr><tr><td>[rn_adresse_comp]</td><td>complement d''adresse</td></tr><tr><td>[rn_president]</td><td>président</td></tr><tr style=height: 10px !important;><td colspan=2></td></tr></tbody></table>',
classe = 'dossier_instruction_reunion'
WHERE code = 'dossier_instruction_reunion';
-- Lettre-type
UPDATE om_lettretype SET
titre_om_htmletat = '<table>
<tbody>
<tr>
<td>
<p>[rn_salle]</p>
<p>[rn_adresse]</p>
<p>[rn_adresse_comp]</p>
</td>
<td style=''text-align: right;''><span class=''mce_maj'' style=''font-weight: bold;''>[rn_type]</span></td>
</tr>
</tbody>
</table>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>COMPTE RENDU SPÉCIFIQUE</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>RÉUNION DU [rn_date] À [rn_heure]<br /></span></p>',
corps_om_htmletatex = '<p>Ordre de passage : [dir_ordre]</p>
<p>Dossier : [di_libelle]</p>
<p>Établissement : <span style=''font-weight: bold;''>[et_code] - [et_libelle]</span></p>
<p>Proposition d''avis : [dir_avis]</p>
<p>Complément de la proposition : [dir_avis_comp]</p>
<p> </p>
<p><span id=''reu_cra'' class=''mce_sousetat''>Réunion - Compte-rendu d''avis</span></p>
<p> </p>
<p> </p>
<table border=''1''>
<tbody>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-weight: bold;''>AVIS DE [rn_type]</span></p>
<p> </p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-weight: bold;''>[avis_libelle]</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>[avis_complement]</span></p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>[avis_motivation]</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Ne change pas l''avis de visite</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p style=''text-align: right;''>Le Président</p>
<p style=''text-align: right;''>[rn_president]</p>'
WHERE id = 'reunion_compte_rendu_avis';

----
-- END / Correction édition compte-rendu spécifique de réunion
----

-- Correction requête programmation
UPDATE om_requete
SET classe = 'programmation',
    merge_fields = '<table><thead><tr>
    <th colspan="2">Enregistrement de type programmation</th></tr></thead><tbody><tr><td>[programmation.programmation]</td><td>programmation</td></tr><tr><td>[programmation.semaine_annee]</td><td>semaine</td></tr><tr><td>[programmation.annee]</td><td>année</td></tr><tr><td>[programmation.numero_semaine]</td><td>numéro de la semaine</td></tr><tr><td>[programmation.service]</td><td>service</td></tr><tr><td>[programmation.version]</td><td>version</td></tr><tr><td>[programmation.date_modification]</td><td>date de modification</td></tr><tr><td>[programmation.programmation_etat]</td><td>état de la programmation</td></tr><tr><td>[programmation.convocation_exploitants]</td><td>convocation exploitants</td></tr><tr><td>[programmation.convocation_membres]</td><td>convocation membres</td></tr><tr><td>[programmation.date_envoi_ce]</td><td>date d''envoi CE</td></tr><tr><td>[programmation.date_envoi_cm]</td><td>date d''envoi CM</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>'
WHERE code = 'programmation';

-- Création requête visite
INSERT INTO om_requete
    (om_requete, code, libelle, description, requete, merge_fields, type, classe, methode)
VALUES
    (nextval('om_requete_seq'), 'visite', 'Contexte ''visite''', 'Tous les champs de fusion spécifiques aux visites', '', '<table><thead><tr>
                        <th colspan="2">Enregistrement de type programmation</th></tr></thead><tbody><tr><td>[programmation.programmation]</td><td>programmation</td></tr><tr><td>[programmation.semaine_annee]</td><td>semaine</td></tr><tr><td>[programmation.annee]</td><td>année</td></tr><tr><td>[programmation.numero_semaine]</td><td>numéro de la semaine</td></tr><tr><td>[programmation.service]</td><td>service</td></tr><tr><td>[programmation.version]</td><td>version</td></tr><tr><td>[programmation.date_modification]</td><td>date de modification</td></tr><tr><td>[programmation.programmation_etat]</td><td>état de la programmation</td></tr><tr><td>[programmation.convocation_exploitants]</td><td>convocation exploitants</td></tr><tr><td>[programmation.convocation_membres]</td><td>convocation membres</td></tr><tr><td>[programmation.date_envoi_ce]</td><td>date d''envoi CE</td></tr><tr><td>[programmation.date_envoi_cm]</td><td>date d''envoi CM</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type visite</th></tr></thead><tbody><tr><td>[visite.visite]</td><td>visite</td></tr><tr><td>[visite.programmation]</td><td>programmation</td></tr><tr><td>[visite.date_creation]</td><td>date de création</td></tr><tr><td>[visite.etablissement]</td><td>établissement</td></tr><tr><td>[visite.visite_etat]</td><td>état de la visite</td></tr><tr><td>[visite.convocation_exploitants]</td><td>convocation exploitants</td></tr><tr><td>[visite.dossier_instruction]</td><td>dossier d''instruction</td></tr><tr><td>[visite.acteur]</td><td>acteur</td></tr><tr><td>[visite.date_visite]</td><td>date de la visite</td></tr><tr><td>[visite.heure_debut]</td><td>heure de début</td></tr><tr><td>[visite.heure_fin]</td><td>heure de fin</td></tr><tr><td>[visite.programmation_version_creation]</td><td>version de la programmation lors de la création</td></tr><tr><td>[visite.programmation_version_modification]</td><td>Version de la programmation lors de la modification</td></tr><tr><td>[visite.programmation_version_annulation]</td><td>annulation de la programmation de la version</td></tr><tr><td>[visite.date_annulation]</td><td>date d''annulation</td></tr><tr><td>[visite.visite_motif_annulation]</td><td>motif de l''annulation de la visite</td></tr><tr><td>[visite.observation]</td><td>observation</td></tr><tr><td>[visite.a_poursuivre]</td><td>à poursuivre</td></tr><tr><td>[visite.courrier_convocation_exploitants]</td><td>courrier de convocation des exploitants</td></tr><tr><td>[visite.courrier_annulation]</td><td>courrier d''annulation</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier d''instruction</th></tr></thead><tbody><tr><td>[dossier_instruction.dossier_instruction]</td><td>dossier d''instruction</td></tr><tr><td>[dossier_instruction.libelle]</td><td>libellé</td></tr><tr><td>[dossier_instruction.description]</td><td>description</td></tr><tr><td>[dossier_instruction.autorite_competente]</td><td>autorité compétente</td></tr><tr><td>[dossier_instruction.technicien]</td><td>technicien</td></tr><tr><td>[dossier_instruction.date_ouverture]</td><td>date d''ouverture</td></tr><tr><td>[dossier_instruction.date_cloture]</td><td>date clôture</td></tr><tr><td>[dossier_instruction.a_qualifier]</td><td>à qualifier</td></tr><tr><td>[dossier_instruction.dossier_cloture]</td><td>clôturé</td></tr><tr><td>[dossier_instruction.incompletude]</td><td>incomplétude</td></tr><tr><td>[dossier_instruction.prioritaire]</td><td>prioritaire</td></tr><tr><td>[dossier_instruction.statut]</td><td>statut</td></tr><tr><td>[dossier_instruction.piece_attendue]</td><td>pièce attendue</td></tr><tr><td>[dossier_instruction.notes]</td><td>notes</td></tr><tr><td>[dossier_instruction.analyses]</td><td>analyses</td></tr><tr><td>[dossier_instruction.dossier_coordination]</td><td>dossier de coordination</td></tr><tr><td>[dossier_instruction.dossier_coordination_date_demande]</td><td>date demande</td></tr><tr><td>[dossier_instruction.dossier_coordination_date_butoir]</td><td>date butoir</td></tr><tr><td>[dossier_instruction.dossier_coordination_date_cloture]</td><td>date clôture</td></tr><tr><td>[dossier_instruction.courrier_dernier_arrete]</td><td>dernier arrêté</td></tr><tr><td>[dossier_instruction.dossier_coordination_dossier_cloture]</td><td>dossier clôturé</td></tr><tr><td>[dossier_instruction.dossier_coordination_autorite_police_encours]</td><td>autorité de police encours</td></tr><tr><td>[dossier_instruction.dossier_coordination_etablissement_type]</td><td>type</td></tr><tr><td>[dossier_instruction.dossier_coordination_etablissement_categorie]</td><td>catégorie</td></tr><tr><td>[dossier_instruction.dossier_coordination_etablissement_locaux_sommeil]</td><td>locaux à sommeil</td></tr><tr><td>[dossier_instruction.etablissement_coordonnees]</td><td>etablissement_coordonnees</td></tr><tr><td>[dossier_instruction.exploitant]</td><td>exploitant</td></tr><tr><td>[dossier_instruction.dossier_coordination_dossier_autorisation_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dossier_instruction.dossier_coordination_dossier_instruction_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dossier_instruction.service]</td><td>service</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier de coordination</th></tr></thead><tbody><tr><td>[dossier_coordination.dossier_coordination]</td><td>dossier de coordination</td></tr><tr><td>[dossier_coordination.libelle]</td><td>libellé</td></tr><tr><td>[dossier_coordination.dossier_coordination_type]</td><td>type de dossier de coordination</td></tr><tr><td>[dossier_coordination.description]</td><td>description</td></tr><tr><td>[dossier_coordination.date_demande]</td><td>date de la demande</td></tr><tr><td>[dossier_coordination.date_butoir]</td><td>date butoir</td></tr><tr><td>[dossier_coordination.date_cloture]</td><td>date clôture</td></tr><tr><td>[dossier_coordination.autorite_police_encours]</td><td>autorité de police en cours</td></tr><tr><td>[dossier_coordination.dossier_cloture]</td><td>clôturé</td></tr><tr><td>[dossier_coordination.a_qualifier]</td><td>à qualifier</td></tr><tr><td>[dossier_coordination.dossier_instruction_secu]</td><td>dossier d''instruction sécurité</td></tr><tr><td>[dossier_coordination.dossier_instruction_secu_lien]</td><td>dossier_instruction_secu_lien</td></tr><tr><td>[dossier_coordination.dossier_instruction_acc]</td><td>dossier d''instruction accessibilité</td></tr><tr><td>[dossier_coordination.dossier_instruction_acc_lien]</td><td>dossier_instruction_acc_lien</td></tr><tr><td>[dossier_coordination.etablissement]</td><td>établissement</td></tr><tr><td>[dossier_coordination.dossier_coordination_parent]</td><td>dossier de coordination parent</td></tr><tr><td>[dossier_coordination.exploitant]</td><td>exploitant</td></tr><tr><td>[dossier_coordination.erp]</td><td>erp</td></tr><tr><td>[dossier_coordination.etablissement_locaux_sommeil]</td><td>locaux sommeil</td></tr><tr><td>[dossier_coordination.etablissement_type]</td><td>type</td></tr><tr><td>[dossier_coordination.etablissement_type_secondaire]</td><td>etablissement_type_secondaire</td></tr><tr><td>[dossier_coordination.etablissement_categorie]</td><td>catégorie</td></tr><tr><td>[dossier_coordination.dossier_autorisation_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dossier_coordination.dossier_instruction_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dossier_coordination.references_cadastrales]</td><td>réf. cadastrales</td></tr><tr><td>[dossier_coordination.geom_point]</td><td>centroïde</td></tr><tr><td>[dossier_coordination.geom_emprise]</td><td>emprise</td></tr><tr><td>[dossier_coordination.contraintes_urba_om_html]</td><td>contraintes URBA</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type établissement</th></tr></thead><tbody><tr><td>[etablissement.etablissement]</td><td>établissement</td></tr><tr><td>[etablissement.code]</td><td>code</td></tr><tr><td>[etablissement.libelle]</td><td>libellé</td></tr><tr><td>[etablissement.adresse_numero]</td><td>numéro</td></tr><tr><td>[etablissement.adresse_numero2]</td><td>mention</td></tr><tr><td>[etablissement.adresse_voie]</td><td>voie</td></tr><tr><td>[etablissement.adresse_complement]</td><td>complément</td></tr><tr><td>[etablissement.lieu_dit]</td><td>lieu-dit</td></tr><tr><td>[etablissement.boite_postale]</td><td>Boîte postale</td></tr><tr><td>[etablissement.adresse_cp]</td><td>Code postal</td></tr><tr><td>[etablissement.adresse_ville]</td><td>ville</td></tr><tr><td>[etablissement.adresse_arrondissement]</td><td>arrondissement</td></tr><tr><td>[etablissement.cedex]</td><td>cedex</td></tr><tr><td>[etablissement.npai]</td><td>npai</td></tr><tr><td>[etablissement.telephone]</td><td>téléphone</td></tr><tr><td>[etablissement.fax]</td><td>fax</td></tr><tr><td>[etablissement.etablissement_nature]</td><td>nature</td></tr><tr><td>[etablissement.siret]</td><td>SIRET</td></tr><tr><td>[etablissement.annee_de_construction]</td><td>année de construction</td></tr><tr><td>[etablissement.etablissement_statut_juridique]</td><td>statut juridique</td></tr><tr><td>[etablissement.etablissement_tutelle_adm]</td><td>tutelle administrative</td></tr><tr><td>[etablissement.ref_patrimoine]</td><td>Réf. patrimoine</td></tr><tr><td>[etablissement.etablissement_type]</td><td>type</td></tr><tr><td>[etablissement.etablissement_categorie]</td><td>catégorie</td></tr><tr><td>[etablissement.etablissement_etat]</td><td>état</td></tr><tr><td>[etablissement.date_arrete_ouverture]</td><td>date d''arrêté d''ouverture</td></tr><tr><td>[etablissement.autorite_police_encours]</td><td>autorité de police en cours</td></tr><tr><td>[etablissement.om_validite_debut]</td><td>date de début</td></tr><tr><td>[etablissement.om_validite_fin]</td><td>date de fin</td></tr><tr><td>[etablissement.si_effectif_public]</td><td>effectif public</td></tr><tr><td>[etablissement.si_effectif_personnel]</td><td>effectif personnel</td></tr><tr><td>[etablissement.si_locaux_sommeil]</td><td>locaux à sommeil</td></tr><tr><td>[etablissement.si_periodicite_visites]</td><td>périodicité des visites</td></tr><tr><td>[etablissement.si_prochaine_visite_periodique_date_previsionnelle]</td><td>date de prochaine visite périodique</td></tr><tr><td>[etablissement.si_visite_duree]</td><td>durée de visite</td></tr><tr><td>[etablissement.si_derniere_visite_periodique_date]</td><td>date de dernière visite périodique</td></tr><tr><td>[etablissement.si_derniere_visite_date]</td><td>date de dernière visite</td></tr><tr><td>[etablissement.si_derniere_visite_avis]</td><td>avis de dernière visite</td></tr><tr><td>[etablissement.si_derniere_visite_technicien]</td><td>technicien de dernière visite</td></tr><tr><td>[etablissement.si_prochaine_visite_date]</td><td>date de prochaine visite</td></tr><tr><td>[etablissement.si_prochaine_visite_type]</td><td>type de prochaine visite</td></tr><tr><td>[etablissement.acc_derniere_visite_date]</td><td>date de dernière visite</td></tr><tr><td>[etablissement.acc_derniere_visite_avis]</td><td>avis de dernière visite</td></tr><tr><td>[etablissement.acc_derniere_visite_technicien]</td><td>technicien de dernière visite</td></tr><tr><td>[etablissement.acc_consignes_om_html]</td><td>Consignes</td></tr><tr><td>[etablissement.acc_descriptif_om_html]</td><td>description</td></tr><tr><td>[etablissement.acc_handicap_auditif]</td><td>accessible auditif</td></tr><tr><td>[etablissement.acc_handicap_mental]</td><td>accessible mental</td></tr><tr><td>[etablissement.acc_handicap_physique]</td><td>accessible physique</td></tr><tr><td>[etablissement.acc_handicap_visuel]</td><td>accessible visuel</td></tr><tr><td>[etablissement.acc_places_stationnement_amenagees]</td><td>nb places stationnement aménagées</td></tr><tr><td>[etablissement.acc_ascenseur]</td><td>Ascenseur</td></tr><tr><td>[etablissement.acc_elevateur]</td><td>élévateur</td></tr><tr><td>[etablissement.acc_boucle_magnetique]</td><td>Boucle magnétique</td></tr><tr><td>[etablissement.acc_sanitaire]</td><td>sanitaire</td></tr><tr><td>[etablissement.acc_places_assises_public]</td><td>nb places assises public</td></tr><tr><td>[etablissement.acc_chambres_amenagees]</td><td>nb chambres aménagées</td></tr><tr><td>[etablissement.acc_douche]</td><td>douche</td></tr><tr><td>[etablissement.acc_dernier_plan_date]</td><td>date du dernier plan</td></tr><tr><td>[etablissement.acc_dernier_plan_avis]</td><td>avis du dernier plan</td></tr><tr><td>[etablissement.acc_derogation_scda]</td><td>Dérogation SCDA</td></tr><tr><td>[etablissement.si_consignes_om_html]</td><td>consignes</td></tr><tr><td>[etablissement.si_descriptif_om_html]</td><td>description</td></tr><tr><td>[etablissement.si_autorite_competente_visite]</td><td>commission compétente visite</td></tr><tr><td>[etablissement.si_autorite_competente_plan]</td><td>commission compétente plan</td></tr><tr><td>[etablissement.si_dernier_plan_avis]</td><td>avis du dernier plan</td></tr><tr><td>[etablissement.si_type_alarme]</td><td>type d''alarme</td></tr><tr><td>[etablissement.si_type_ssi]</td><td>type SSI</td></tr><tr><td>[etablissement.si_conformite_l16]</td><td>conformité L16</td></tr><tr><td>[etablissement.si_alimentation_remplacement]</td><td>alimentation de remplacement</td></tr><tr><td>[etablissement.si_service_securite]</td><td>Service sécurité</td></tr><tr><td>[etablissement.si_personnel_jour]</td><td>effectif du personnel de jour</td></tr><tr><td>[etablissement.si_personnel_nuit]</td><td>effectif du personnel de nuit</td></tr><tr><td>[etablissement.references_cadastrales]</td><td>réf. cadastrales</td></tr><tr><td>[etablissement.dossier_coordination_periodique]</td><td>dossier_coordination_periodique</td></tr><tr><td>[etablissement.geom_point]</td><td>centroïde</td></tr><tr><td>[etablissement.geom_emprise]</td><td>emprise</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>', 'objet', 'visite;dossier_instruction;dossier_coordination;etablissement', 'get_programmation_merge_fields');

-- Rattachement de la requête visite aux lettres-types convocation et annulation
UPDATE om_lettretype
    SET om_sql = (SELECT om_requete FROM om_requete WHERE code = 'visite')
    WHERE id = 'programmation_annulation_exploitant'
        OR id = 'programmation_convocation_exploitant';