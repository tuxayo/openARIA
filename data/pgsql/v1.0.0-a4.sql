--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a4
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Ajout du code de l'établissement composé de du prefixe suivi de l'id
ALTER TABLE etablissement ADD COLUMN code varchar(25);

INSERT INTO om_parametre VALUES (nextval('om_parametre_seq'), 'etablissement_code_prefixe', 'T', 1);