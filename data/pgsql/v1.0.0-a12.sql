--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a12
--
-- XXX Ce fichier doit être renommé en v1.0.0-a12.sql au moment de la release
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
-- START / Ajout des champs necessaires auw statistiques
--

-- Ajout d'une date de clôture du DC
ALTER TABLE dossier_coordination ADD COLUMN date_cloture date;
COMMENT ON COLUMN dossier_coordination.date_cloture IS 'Date de clôture du dossier de coordination';

-- Ajout d'une date de clôture du DI
ALTER TABLE dossier_instruction ADD COLUMN date_cloture date;
COMMENT ON COLUMN dossier_instruction.date_cloture IS 'Date de clôture du dossier d''instruction';

-- Ajourt d'une date d'ouverture du DI
ALTER TABLE dossier_instruction ADD COLUMN date_ouverture date;
COMMENT ON COLUMN dossier_instruction.date_ouverture IS 'Date d''ouverture du dossier d''instruction';

--
-- END / Ajout des champs necessaires auw statistiques
--

--
-- START / Mise à jour framework - permissions
--

-- XXX rajouter les commentaaires
CREATE TABLE om_permission (
    om_permission integer NOT NULL,
    libelle character varying(100) NOT NULL,
    type character varying(100) NOT NULL
);
CREATE SEQUENCE om_permission_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER SEQUENCE om_permission_seq OWNED BY om_permission.om_permission;


ALTER TABLE ONLY om_permission
    ADD CONSTRAINT om_permission_pkey PRIMARY KEY (om_permission);

--
-- END / Mise à jour framework - permissions
--


--
-- BEGIN / Correction aide à la saisie des éditions de l'analyse
--

UPDATE om_requete SET
merge_fields = '<table><thead><tr>
    <th colspan="2">Analyse</th></tr></thead><tbody><tr><td>[anls_id]</td><td>Identifiant unique</td></tr><tr><td>[anls_type]</td><td>type d''analyse</td></tr><tr><td>[anls_obj]</td><td>objet</td></tr><tr><td>[anls_desc]</td><td>descriptif de l''établissement</td></tr><tr><td>[anls_cr]</td><td>compte-rendu</td></tr><tr><td>[anls_obs]</td><td>observation</td></tr><tr><td>[anls_reg_app]</td><td>réglementation applicable</td></tr><tr><td>[anls_doc_pdnt]</td><td>document présenté lors des visites</td></tr><tr><td>[anls_avis_prop]</td><td>Avis de l''analyse</td></tr><tr><td>[anls_avis_comp]</td><td>complément d''avis</td></tr><tr><td>[anls_presc]</td><td>tableau des prescriptions</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
    <th colspan="2">Données techniques accessibilité</th></tr></thead><tbody><tr><td>[dt_desc_acc]</td><td>description</td></tr><tr><td>[dt_h_ment]</td><td>accessible mental</td></tr><tr><td>[dt_h_audtf]</td><td>accessible auditif</td></tr><tr><td>[dt_h_phsq]</td><td>accessible physique</td></tr><tr><td>[dt_h_visu]</td><td>accessible visuel</td></tr><tr><td>[dt_elvtr]</td><td>élévateur</td></tr><tr><td>[dt_ascsr]</td><td>Ascenseur</td></tr><tr><td>[dt_douche]</td><td>Douche aménagée</td></tr><tr><td>[dt_bcl_mgnt]</td><td>Boucle magnétique</td></tr><tr><td>[dt_sanitaire]</td><td>Sanitaire aménagé</td></tr><tr><td>[dt_plc_stmnt_amng]</td><td>nb places stationnements aménagées</td></tr><tr><td>[dt_chmbr_amng]</td><td>nb places stationnements aménagées</td></tr><tr><td>[dt_plc_ass_pub]</td><td>nb places assises plublic</td></tr><tr><td>[dt_scda]</td><td>Dérogation SCDA</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
    <th colspan="2">Données techniques sécurité incendie</th></tr></thead><tbody><tr><td>[dt_desc_si]</td><td>description</td></tr><tr><td>[dt_eff_pub]</td><td>effectif public</td></tr><tr><td>[dt_eff_pers]</td><td>si_effectif_personnell</td></tr><tr><td>[dt_ssi]</td><td>type SSI</td></tr><tr><td>[dt_alarme]</td><td>type d''alarme</td></tr><tr><td>[dt_i16]</td><td>conformité I16</td></tr><tr><td>[dt_alim]</td><td>alimentation de remplacement</td></tr><tr><td>[dt_svc_sec]</td><td>Service sécurité</td></tr><tr><td>[dt_pers_jour]</td><td>effectif du personnel de jour</td></tr><tr><td>[dt_pers_nuit]</td><td>effectif du personnel de nuit</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
    <th colspan="2">Dossier d''instruction</th></tr></thead><tbody><tr><td>[di_lib]</td><td>libellé</td></tr><tr><td>[di_tech]</td><td>Technicien en charge</td></tr><tr><td>[di_ac]</td><td>autorité compétente</td></tr><tr><td>[di_qualif]</td><td>à qualifier</td></tr><tr><td>[di_incomp]</td><td>incomplétude</td></tr><tr><td>[di_clos]</td><td>clôturé</td></tr><tr><td>[di_prio]</td><td>prioritaire</td></tr><tr><td>[di_piece]</td><td>pièce attendue</td></tr><tr><td>[di_desc]</td><td>description</td></tr><tr><td>[di_notes]</td><td>notes</td></tr><tr><td>[di_statut]</td><td>statut</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
    <th colspan="2">Dossier de coordination</th></tr></thead><tbody><tr><td>[dc_lib]</td><td>libellé</td></tr><tr><td>[dc_type]</td><td>type de dossier de coordination</td></tr><tr><td>[dc_date_dmnd]</td><td>date de la demande</td></tr><tr><td>[dc_date_butr]</td><td>date butoir</td></tr><tr><td>[dc_da_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dc_di_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dc_qualif]</td><td>à qualifier</td></tr><tr><td>[dc_etab_type]</td><td>type</td></tr><tr><td>[dc_etab_type_sec]</td><td>type(s) secondaire(s)</td></tr><tr><td>[dc_etab_cat]</td><td>catégorie</td></tr><tr><td>[dc_etab_erp]</td><td>erp</td></tr><tr><td>[dc_clos]</td><td>clôturé</td></tr><tr><td>[dc_ctr_urb]</td><td>contraintes URBA</td></tr><tr><td>[dc_etab_loc_som]</td><td>locaux sommeil</td></tr><tr><td>[dc_parent]</td><td>dossier de coordination parent</td></tr><tr><td>[dc_desc]</td><td>description</td></tr><tr><td>[dc_ap]</td><td>autorité de police en cours</td></tr><tr><td>[dc_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
    <th colspan="2">Établissement</th></tr></thead><tbody><tr><td>[etab_code]</td><td>Numero T</td></tr><tr><td>[etab_lib]</td><td>libellé</td></tr><tr><td>[etab_siret]</td><td>SIRET</td></tr><tr><td>[etab_tel]</td><td>téléphone</td></tr><tr><td>[etab_fax]</td><td>fax</td></tr><tr><td>[etab_annee_const]</td><td>année de construction</td></tr><tr><td>[etab_type]</td><td>type</td></tr><tr><td>[etab_cat]</td><td>catégorie</td></tr><tr><td>[etab_nat]</td><td>nature</td></tr><tr><td>[etab_stat_jur]</td><td>statut juridique</td></tr><tr><td>[etab_tut_adm]</td><td>tutelle administrative</td></tr><tr><td>[etab_etat]</td><td>état</td></tr><tr><td>[etab_adresse]</td><td>adresse</td></tr><tr><td>[etab_date_ar_ouv]</td><td>date d''arrêté d''ouverture</td></tr><tr><td>[etab_exp]</td><td>exploitant</td></tr><tr><td>[etab_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>'
WHERE code = 'anl';

--
-- END / Correction aide à la saisie des éditions de l'analyse
--