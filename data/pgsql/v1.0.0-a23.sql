----
-- BEGIN / ajout liaison pv <-> courrier de cardinalité 1/1
----
ALTER TABLE proces_verbal
    ADD courrier_genere integer NULL;
COMMENT ON COLUMN proces_verbal.courrier_genere IS 'Lien vers le PV généré et son document signé associé.';
ALTER TABLE ONLY proces_verbal
    ADD CONSTRAINT proces_verbal_courrier_genere_fkey FOREIGN KEY (courrier_genere) REFERENCES courrier(courrier);

ALTER TABLE courrier
    ADD proces_verbal integer NULL;
COMMENT ON COLUMN courrier.proces_verbal IS 'Lien vers le procès-verbal.';
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_proces_verbal_fkey FOREIGN KEY (proces_verbal) REFERENCES proces_verbal(proces_verbal);
----
-- END / ajout liaison pv <-> courrier de cardinalité 1/1
----


----
-- BEGIN / ajout liaison visite <-> courrier de cardinalité 1/1
----
ALTER TABLE courrier
    ADD visite integer NULL;
COMMENT ON COLUMN courrier.visite IS 'Lien vers la visite.';
ALTER TABLE ONLY courrier
    ADD CONSTRAINT visite_courrier_fkey FOREIGN KEY (visite) REFERENCES visite(visite);
----
-- END / ajout liaison visite <-> courrier de cardinalité 1/1
----


----
-- BEGIN / modification requêtes objet
----
-- Ajout méthode spécifique à la requête utilisée dans le contexte 'passage en réunion / DI'
UPDATE om_requete SET methode = 'get_all_merge_fields' WHERE om_requete = 5;
-- Ajout méthode spécifique à la requête utilisée dans le contexte 'procès-verbal / DI'
UPDATE om_requete SET methode = 'get_all_merge_fields_for_proces_verbal', classe='courrier' WHERE om_requete = 6;
-- Ajout méthode spécifique à la requête utilisée dans le contexte 'visite'
UPDATE om_requete SET methode = 'get_all_merge_fields_for_visite', classe='courrier' WHERE om_requete = 11;
----
-- END / modification requêtes objet


----
-- BEGIN / Ordonner les instances dans la réunion
----
ALTER TABLE reunion_instance ADD COLUMN ordre integer;
----
-- END / Ordonner les instances dans la réunion
----




--
--
ALTER TABLE prescription DROP COLUMN pr_tete_de_chapitre1;
ALTER TABLE prescription DROP COLUMN pr_tete_de_chapitre2;
ALTER TABLE prescription DROP COLUMN pr_libelle;
--
--


--
--
ALTER TABLE reglementation_applicable ADD COLUMN annee_debut_application integer;
ALTER TABLE reglementation_applicable ADD COLUMN annee_fin_application integer;
--
--

--
-- Prescription réglementaire obligatoire dans prescription d'analyse
ALTER TABLE prescription
    ALTER prescription_reglementaire SET NOT NULL;
--
--

-- Correction lettre-type PV SI
UPDATE om_lettretype
    SET corps_om_htmletatex = '<p>* Procès-verbal</p><p>Numéro : [proces_verbal.numero]<br />Date de rédaction : [proces_verbal.date_redaction]</p><p><br />* PV / Signataire</p><p>Civilité : [signataire_civilite]<br />Nom : [signataire_nom]<br />Prénom : [signataire_prenom]<br />Qualité : [signataire_qualite]<br />Signature : [signataire_signature]</p><p><br />* PV / Demande de passage en réunion</p><p>Date de passage souhaitée : [reunion_date]<br />Type de réunion : [reunion_type]<br />Catégorie du type de réunion : [reunion_categorie]<br />Proposition d''avis : [dpr.proposition_avis]<br />Complément de la proposition d''avis : [dpr.proposition_avis_comp]<br />Ordre de passage : [dpr.ordre]<br />Date souhaitée : [dpr.date_souhaitee]<br />Avis rendu : [dpr.avis_rendu]<br />Complément de l''avis rendu : [dpr.avis_rendu_comp]<br />Motivation de l''avis rendu : [dpr.avis_rendu_motiv]<br />Date : [reunion.date]<br />Heure : [reunion.heure]<br />Salle : [reunion.salle]<br />Adresse : [reunion.adresse]<br />Complément d''adresse : [reunion.adresse_comp]<br />Président : [reunion.president]</p><p><br />* Dosser d''instruction</p><p>Libellé : [dossier_instruction.libelle]<br />Autorité compétente : [dossier_instruction.autorite_competente]<br />À qualifier : [dossier_instruction..a_qualifier]<br />Incomplétude : [dossier_instruction.incompletude]<br />Clôturé : [dossier_instruction.dossier_cloture]<br />Prioritaire : [dossier_instruction.prioritaire]<br />Pièces attendues : [dossier_instruction.piece_attendue]<br />Description détaillée : [dossier_instruction.description]<br />Commentaires d''instruction : [dossier_instruction.notes]<br />Statut : [dossier_instruction.statut]</p><p>* DI / Technicien</p><p>Nom et prénom : [acteur.nom_prenom]<br />Service : [acteur.service]<br />Rôle : [acteur.role]<br />Acronyme : [acteur.acronyme]<br />Références : [acteur.reference]<br />Login : [acteur.login]<br />Email : [acteur.email]</p><p><br />* Dossier de coordination</p><p>Libellé : [dossier_coordination.libelle]<br />Type : [dossier_coordination.dossier_coordination_type]<br />Date de la demande : [dossier_coordination.date_demande]<br />Date butoir : [dossier_coordination.date_butoir]<br />Dossier d''autorisation ADS : [dossier_coordination.dossier_autorisation_ads]<br />Dossier d''instruction ADS : [dossier_coordination.dossier_instruction_ads]<br />À qualifier : [dossier_coordination.a_qualifier]<br />Clôturé : [dossier_coordination.dossier_cloture]<br />Contraintes urbaines : [dossier_coordination.contraintes_urba_om_html]<br />Dossier de coordination parent : [dossier_coordination.dossier_coordination_parent]<br />Description : [dossier_coordination.description]<br />Autorité de police en cours : [dossier_coordination.autorite_police_encours]<br />Références cadastrales : [dossier_coordination.references_cadastrales]</p><p> </p><p>* Établissement</p><p>Numéro T : [etablissement.code]<br />Libellé : [etablissement.libelle]<br />Type : [etablissement.etablissement_type]<br />Catégorie : [etablissement.etablissement_categorie]<br />Siret : [etablissement.siret]<br />Téléphone : [etablissement.telephone]<br />Fax : [etablissement.fax]<br />Année de construction : [etablissement.annee_de_construction]<br />Nature : [etablissement.etablissement_nature]<br />Statut juridique : [etablissement.etablissement_statut_juridique]<br />Tutelle administrative : [etablissement.etablissement_tutelle_adm]<br />État : [etablissement.etablissement_etat]<br />Adresse : [etablissement.adresse]<br />Date d''arrêté d''ouverture : [etablissement.date_arrete_ouverture]<br />Références cadastrales : [etablissement.references_cadastrales]</p><p><br />* Analyse</p><p>- Type d''analyse<br />[analyse.type]<br />- Objet<br />[analyse.objet]<br />- Descriptif de l''établissement<br />[analyse.descriptif_etablissement]<br />- Classification de l''établissement<br />Type d''établissement : [dossier_coordination.etablissement_type]<br />Type(s) secondaire(s) : [dossier_coordination.etablissement_type_secondaire]<br />Catégorie d''établissement : [dossier_coordination.etablissement_categorie]<br />Établissement référentiel : [dossier_coordination.erp]<br />Locaux à sommeil : [dossier_coordination.etablissement_locaux_sommeil]<br />- Données techniques<br />Effectif public : [analyse.dt_si_effectif_public]<br />Effectif personnel : [analyse.dt_si_effectif_personnel]<br />Type SSI : [analyse.dt_si_type_ssi]<br />Type alarme : [analyse.dt_si_type_alarme]<br />Conformité L16 : [analyse.dt_si_conformite_l16]<br />Alimentation de remplacement : [analyse.dt_si_alimentation_remplacement]<br />Service sécurité : [analyse.dt_si_service_securite]<br />Personnel de jour : [analyse.dt_si_personnel_jour]<br />Personnel de nuit : [analyse.dt_si_personnel_nuit]<br />- Réglementation applicable<br />[analyse.reglementation_applicable]<br />- Prescriptions<br />[analyse.prescriptions]<br />[analyse.prescriptions_defavorables]<br />- Documents présentés lors des visites<br />[analyse.documents_presentes_pendant]<br />- Documents fournis après les visites<br />[analyse.documents_presentes_apres]<br />- Essais réalisés<br />[analyse.essais_realises]<br />- Compte-rendu<br />[analyse.compte_rendu]<br />- Observation<br />[analyse.observations]<br />- Avis proposé<br />[analyse.proposition_avis]<br />[analyse.proposition_avis_complement]</p>'
    WHERE id = 'proces_verbal_si';


----
-- BEGIN / Modification du système de notification dans les réunions et les programmations
----
-- Visites
ALTER TABLE reunion_instance DROP visite;
-- Contacts insitutionnels
ALTER TABLE contact ADD COLUMN service integer NULL;
ALTER TABLE contact ADD COLUMN reception_programmation boolean NULL;
ALTER TABLE contact ADD COLUMN reception_commission boolean NULL;
COMMENT ON COLUMN contact.service IS 'Service du contact insitutionnel.';
COMMENT ON COLUMN contact.reception_programmation IS 'Réception de la programmation.';
COMMENT ON COLUMN contact.reception_commission IS 'Réception des éditions liées aux commissions.';
ALTER TABLE ONLY contact
    ADD CONSTRAINT contact_service_fkey FOREIGN KEY (service) REFERENCES service(service);
----
-- END / Modification du système de notification dans les réunions et les programmations
----

-- Le numéro des pv n'est plus obligatoire
ALTER TABLE proces_verbal ALTER COLUMN numero DROP NOT NULL;

----
-- BEGIN / Demande de passage automatique en réunion
----
ALTER TABLE reunion ADD COLUMN planifier_nouveau boolean DEFAULT false;
ALTER TABLE programmation ADD COLUMN planifier_nouveau boolean DEFAULT false;
----
-- END / Demande de passage automatique en réunion
----

