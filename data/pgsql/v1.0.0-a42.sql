--
-- START / TI#459 — Gestion de la commande d17249w01
--

-- Documents générés à éditer
-- Widget
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
VALUES (nextval('om_widget_seq'), 'Documents générés à éditer', 'documents_generes_a_editer', '', 'file');
-- Permissions
-- SECRETAIRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
-- SECRETAIRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
-- CADRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
-- CADRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
-- Dashboard
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_a_editer'));

-- Documents générés en attente de signature
-- Widget
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
VALUES (nextval('om_widget_seq'), 'Documents générés en attente de signature', 'documents_generes_attente_signature', '', 'file');
-- Permissions
-- SECRETAIRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
-- SECRETAIRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
-- CADRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
-- CADRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
-- Dashboard
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_signature'));

-- Documents générés en attente de retour ar
-- Widget
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
VALUES (nextval('om_widget_seq'), 'Documents générés en attente de retour AR', 'documents_generes_attente_retour_ar', '', 'file');
-- Permissions
-- SECRETAIRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
    );
-- SECRETAIRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
    );
-- CADRE ACC
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
    );
-- CADRE SI
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_finalise_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_om_fichier_signe_courrier_telecharger' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_finalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_finalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_definalise',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_definalise' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_previsualiser',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_previsualiser' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
    );
-- Dashboard
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, position, om_widget) VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'documents_generes_attente_retour_ar'));

--
-- END / TI#459 — Gestion de la commande d17249w01
--