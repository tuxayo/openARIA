--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a2
--
-- XXX Ce fichier doit être renommé en v1.0.0-a2.sql au moment de la release
--
-- @package openAria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--
ALTER TABLE reunion DROP COLUMN om_fichier_reunion_ordre_jour;
ALTER TABLE reunion DROP COLUMN om_final_reunion_ordre_jour;
ALTER TABLE reunion DROP COLUMN om_fichier_reunion_compte_rendu;
ALTER TABLE reunion DROP COLUMN om_final_reunion_compte_rendu;

--
ALTER TABLE reunion ADD COLUMN numerotation integer NOT NULL;
ALTER TABLE reunion ADD COLUMN om_fichier_reunion_odj character varying(64);
ALTER TABLE reunion ADD COLUMN om_final_reunion_odj boolean;
ALTER TABLE reunion ADD COLUMN om_fichier_reunion_cr_global character varying(64);
ALTER TABLE reunion ADD COLUMN om_final_reunion_cr_global boolean;
ALTER TABLE reunion ADD COLUMN om_fichier_reunion_cr_global_signe character varying(64);
ALTER TABLE reunion ADD COLUMN om_fichier_reunion_cr_par_dossier_signe character varying(64);

--
ALTER TABLE reunion ALTER COLUMN code SET NOT NULL;
ALTER TABLE reunion ALTER COLUMN libelle SET NOT NULL;
ALTER TABLE reunion_type ALTER COLUMN code SET NOT NULL;
ALTER TABLE reunion_type ALTER COLUMN libelle SET NOT NULL;

--
ALTER TABLE reunion_categorie ADD COLUMN ordre integer NOT NULL;

--Ajout des données de paramétrage de la table visite_motif_annulation
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'AE', 'Annulation exceptionnelle', 'Annulation exceptionnelle', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'SPGR', 'SPGR', 'Service de prévention et de gestion des risques', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'IM', 'Indisponibilité d''un membre', 'Indisponibilité d''un membre', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'SCDS', 'SCDS', 'Sous-division de la commission de sécurité', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'EI', 'Exploitant indisponible', 'Exploitant indisponible', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'ED', 'Exploitant défaillant', 'Exploitant défaillant', null, null);
INSERT INTO visite_motif_annulation VALUES (nextval('visite_motif_annulation_seq'), 'NPAI', 'NPAI', 'N''habite pas à l''adresse indiquée', null, null);

-- Ajout des droits pour l'annulation de visite
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'visite_annuler_plannification',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'visite_annuler_plannification' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'visite_annuler_plannification',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'visite_annuler_plannification' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'visite_annuler_plannification',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'visite_annuler_plannification' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN')
    );
SELECT nextval('om_droit_seq'),'visite_annuler_plannification',(SELECT om_profil FROM om_profil WHERE libelle = 'PROGRAMMATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'visite_annuler_plannification' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'PROGRAMMATEUR')
    );

--Ajout d'un champ NPAI dans la table etablissement
ALTER TABLE etablissement ADD npai boolean;
COMMENT ON COLUMN etablissement.npai IS 'Indique si l''adresse de l''établissement est incorrecte';

--Ajout du widget des établissement NPAI pour les secrétaires
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
SELECT nextval('om_widget_seq'), 'Établissements NPAI', 'etablissements_npai', '', 'file'
WHERE
    NOT EXISTS (
        SELECT libelle FROM om_widget WHERE libelle = 'Établissements NPAI'
    );
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
SELECT nextval('om_dashboard_seq'),(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE'), 'C1',   1,  (SELECT om_widget FROM om_widget WHERE libelle = 'Établissements NPAI')
WHERE
    NOT EXISTS (
        SELECT om_dashboard FROM om_dashboard WHERE om_widget = (SELECT om_widget FROM om_widget WHERE libelle = 'Établissements NPAI') AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
    );

--Droit pour le widget
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_tous_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_tous_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_tous_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_tous_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'etablissement_tous_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'etablissement_tous_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE')
    );

--Requête SQL vide
INSERT INTO om_requete VALUES (nextval('om_requete_seq'), 'aucune', 'Aucune REQUÊTE', NULL, 'SELECT '''' as aucune_valeur FROM &DB_PREFIXEom_collectivite', '[aucune_valeur]');
--Lettre type pour l'annulation d'une visite 
INSERT INTO om_lettretype VALUES (nextval('om_lettretype_seq'), 1, 'annulation_visite_contact', 'Lettre d''annulation de visite', true, 'P', 'A4', NULL, 10, 10, '<p>Lettre d''annulation de visite</p>', 109, 16, 0, 10, '0', '<p>Lettre d''annulation de visite</p>', (SELECT om_requete FROM om_requete WHERE code = 'aucune') , 0, 0, 0, 0, 'helvetica', '0-0-0');

--Ajout du widget des programmations à valider pour les cadres
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
SELECT nextval('om_widget_seq'), 'Programmations à valider', 'programmation_a_valider', '', 'file'
WHERE
    NOT EXISTS (
        SELECT libelle FROM om_widget WHERE libelle = 'Programmations à valider'
    );
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
SELECT nextval('om_dashboard_seq'),(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE'), 'C1',   1,  (SELECT om_widget FROM om_widget WHERE libelle = 'Programmations à valider')
WHERE
    NOT EXISTS (
        SELECT om_dashboard FROM om_dashboard WHERE om_widget = (SELECT om_widget FROM om_widget WHERE libelle = 'Programmations à valider') AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );

--Ajout de droits sur les programmations aux cadres
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_ajouter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_ajouter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_consulter',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_consulter' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_modifier',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_modifier' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_supprimer',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_supprimer' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_tab',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_tab' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );

-- Ajout des droits pour l'ajout d'une nouvelle version de programmation
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_nouvelle_version',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_nouvelle_version' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'programmation_nouvelle_version',(SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'programmation_nouvelle_version' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'ADMINISTRATEUR')
    );

-- Ajout de la lettre-type pour la lettre de convocation des partenaires
INSERT INTO om_lettretype VALUES (nextval('om_lettretype_seq'), 1, 'convocation_membre', 'Lettre de convocation', true, 'P', 'A4', NULL, 10, 10, '<p>Lettre de convocation</p>', 109, 16, 0, 10, '0', '<p>Lettre de convocation</p>', 1, 0, 0, 0, 0, 'helvetica', '0-0-0');
INSERT INTO om_lettretype VALUES (nextval('om_lettretype_seq'), 1, 'convocation_exploitant', 'Lettre de convocation', true, 'P', 'A4', NULL, 10, 10, '<p>Lettre de convocation</p>', 109, 16, 0, 10, '0', '<p>Lettre de convocation</p>', 1, 0, 0, 0, 0, 'helvetica', '0-0-0');

--
-- START / Autorité de Police
--

-- Création de la table des décisions de l'autorité de police
CREATE TABLE autorite_police_decision (
    autorite_police_decision integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer,
    suivi_delai boolean DEFAULT false,
    avis character varying(100),
    etablissement_etat integer,
    delai integer,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires
COMMENT ON TABLE autorite_police_decision IS 'Décisions possible concernant une autorité de police.';
COMMENT ON COLUMN autorite_police_decision.autorite_police_decision IS 'Identifiant unique';
COMMENT ON COLUMN autorite_police_decision.code IS 'Code de la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.libelle IS 'Libellé de la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.description IS 'Description de la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.service IS 'Service de la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.suivi_delai IS 'Suivi de délai pris en compte pour la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.avis IS 'Utilisé pour les statistiques sur les décisions d''autorité de police et pour vérifier si un lien avec un courrier est nécessaire ou non (favorable/défavorable).';
COMMENT ON COLUMN autorite_police_decision.etablissement_etat IS 'Utilisé pour mettre à jour l''état de l''établissement.';
COMMENT ON COLUMN autorite_police_decision.delai IS 'Délai par défaut de l''autorité de police (en jour).';
COMMENT ON COLUMN autorite_police_decision.om_validite_debut IS 'Date de début de validité de la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police_decision.om_validite_fin IS 'Date de fin de validité de la décision de l''autorité de police.';
-- Contraintes
ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_pkey PRIMARY KEY (autorite_police_decision);
ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_service_fkey FOREIGN KEY (service) REFERENCES service(service);
ALTER TABLE ONLY autorite_police_decision
    ADD CONSTRAINT autorite_police_decision_etablissement_etat_fkey FOREIGN KEY (etablissement_etat) REFERENCES etablissement_etat(etablissement_etat);
-- Séquence
CREATE SEQUENCE autorite_police_decision_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE autorite_police_decision_seq OWNED BY autorite_police_decision.autorite_police_decision;

-- Création de la table des motifs de l'autorité de police
CREATE TABLE autorite_police_motif (
    autorite_police_motif integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    service integer,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires
COMMENT ON TABLE autorite_police_motif IS 'Motifs possible concernant une autorité de police.';
COMMENT ON COLUMN autorite_police_motif.autorite_police_motif IS 'Identifiant unique';
COMMENT ON COLUMN autorite_police_motif.code IS 'Code du motif de l''autorité de police.';
COMMENT ON COLUMN autorite_police_motif.libelle IS 'Libellé du motif de l''autorité de police.';
COMMENT ON COLUMN autorite_police_motif.description IS 'Description du motif de l''autorité de police.';
COMMENT ON COLUMN autorite_police_motif.service IS 'Service du motif de l''autorité de police.';
COMMENT ON COLUMN autorite_police_motif.om_validite_debut IS 'Date de début de validité du motif de l''autorité de police.';
COMMENT ON COLUMN autorite_police_motif.om_validite_fin IS 'Date de fin de validité du motif de l''autorité de police.';
-- Contraintes
ALTER TABLE ONLY autorite_police_motif
    ADD CONSTRAINT autorite_police_motif_pkey PRIMARY KEY (autorite_police_motif);
ALTER TABLE ONLY autorite_police_motif
    ADD CONSTRAINT autorite_police_motif_service_fkey FOREIGN KEY (service) REFERENCES service(service);
-- Séquence
CREATE SEQUENCE autorite_police_motif_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE autorite_police_motif_seq OWNED BY autorite_police_motif.autorite_police_motif;

-- Création de la table des autorités de police
CREATE TABLE autorite_police (
    autorite_police integer NOT NULL,
    autorite_police_decision integer NOT NULL,
    date_decision date NOT NULL,
    delai integer NOT NULL,
    autorite_police_motif integer NOT NULL,
    cloture boolean DEFAULT false,
    date_notification date,
    date_butoir date,
    service integer NOT NULL,
    dossier_instruction_reunion integer,
    dossier_coordination integer,
    etablissement integer
);
-- Commentaires
COMMENT ON TABLE autorite_police IS 'Autorités de Police.';
COMMENT ON COLUMN autorite_police.autorite_police IS 'Identifiant unique';
COMMENT ON COLUMN autorite_police.autorite_police_decision IS 'Décision de l''autorité de police (liste à choix).';
COMMENT ON COLUMN autorite_police.date_decision IS 'Date de décision de l''autorité de police initialisée avec la date de la réunion qui porte la décision.';
COMMENT ON COLUMN autorite_police.delai IS 'Délai de l''autorité de police initialisé par défaut avec le délai de la décision.';
COMMENT ON COLUMN autorite_police.autorite_police_motif IS 'Motif de l''autorité de police (liste à choix).';
COMMENT ON COLUMN autorite_police.cloture IS 'L''autorité de police est clôturée.';
COMMENT ON COLUMN autorite_police.date_notification IS 'Mise à jour par la notification du ou des courriers à la décision de l''autorité de police.';
COMMENT ON COLUMN autorite_police.date_butoir IS 'Date butoir de l''autorité de police.';
COMMENT ON COLUMN autorite_police.service IS 'Service de l''autorité de police.';
COMMENT ON COLUMN autorite_police.dossier_instruction_reunion IS 'Référence à la demande de passage en réunion.';
COMMENT ON COLUMN autorite_police.dossier_coordination IS 'Dossier de coordination de l''autorité de police.';
COMMENT ON COLUMN autorite_police.etablissement IS 'Établissement du dossier de coordination dont l''autorité de police fait partie.';
-- Contraintes
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_pkey PRIMARY KEY (autorite_police);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_autorite_police_decision_fkey FOREIGN KEY (autorite_police_decision) REFERENCES autorite_police_decision(autorite_police_decision);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_autorite_police_motif_fkey FOREIGN KEY (autorite_police_motif) REFERENCES autorite_police_motif(autorite_police_motif);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_service_fkey FOREIGN KEY (service) REFERENCES service(service);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_instruction_reunion_fkey FOREIGN KEY (dossier_instruction_reunion) REFERENCES dossier_instruction_reunion(dossier_instruction_reunion);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);
ALTER TABLE ONLY autorite_police
    ADD CONSTRAINT autorite_police_etablissement_fkey FOREIGN KEY (etablissement) REFERENCES etablissement(etablissement);
-- Séquence
CREATE SEQUENCE autorite_police_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE autorite_police_seq OWNED BY autorite_police.autorite_police;

-- Création de la table de liaison des autorités de police et des courriers
CREATE TABLE lien_autorite_police_courrier (
    lien_autorite_police_courrier integer NOT NULL,
    autorite_police integer NOT NULL,
    courrier integer NOT NULL
);
-- Commentaires
COMMENT ON TABLE lien_autorite_police_courrier IS 'Liaison entre les autorités de police et les courriers.';
COMMENT ON COLUMN lien_autorite_police_courrier.lien_autorite_police_courrier IS 'Identifiant unique';
COMMENT ON COLUMN lien_autorite_police_courrier.autorite_police IS 'Autorité de police.';
COMMENT ON COLUMN lien_autorite_police_courrier.courrier IS 'Courrier lié.';
-- Contraintes
ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_pkey PRIMARY KEY (lien_autorite_police_courrier);
ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_autorite_police_fkey FOREIGN KEY (autorite_police) REFERENCES autorite_police(autorite_police);
ALTER TABLE ONLY lien_autorite_police_courrier
    ADD CONSTRAINT lien_autorite_police_courrier_courrier_fkey FOREIGN KEY (courrier) REFERENCES courrier(courrier);
-- Séquence
CREATE SEQUENCE lien_autorite_police_courrier_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_autorite_police_courrier_seq OWNED BY lien_autorite_police_courrier.lien_autorite_police_courrier;

--
-- END / Autorité de Police
--

-- Renomme le champ "autorite_de_police_en_cours" de la table "etablissement"
-- en "autorite_police_encours" identique à) la table "dossier_coordination"
ALTER TABLE etablissement RENAME COLUMN autorite_de_police_en_cours TO autorite_police_encours;

--
-- BEGIN / Création des tables métier analyse et données techniques,
-- ainsi que les tables de paramétrage et de liaison associées.
--

-- Paramétrage : type d'analyse
CREATE TABLE analyses_type (
    analyses_type integer NOT NULL,
    service integer NOT NULL,
    code character varying(20) NULL,
    libelle character varying(100) NULL,
    description text NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE analyses_type IS 'Type d''analyse';
COMMENT ON COLUMN analyses_type.analyses_type IS 'Identifiant unique';
COMMENT ON COLUMN analyses_type.service IS 'Service du type d''analyse';
COMMENT ON COLUMN analyses_type.code IS 'Code du type d''analyse';
COMMENT ON COLUMN analyses_type.libelle IS 'Libellé du type d''analyse';
COMMENT ON COLUMN analyses_type.description IS 'Description du type d''analyse';
COMMENT ON COLUMN analyses_type.om_validite_debut IS 'Date de début de validité du type d''analyse';
COMMENT ON COLUMN analyses_type.om_validite_fin IS 'Date de fin de validité du type d''analyse';
ALTER TABLE ONLY analyses_type
    ADD CONSTRAINT analyses_type_pkey PRIMARY KEY (analyses_type),
    ADD CONSTRAINT analyses_type_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE analyses_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY analyses_type.analyses_type;
-- Ajout des types d'analyse (données)
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '1', 'VR-ACC', 'Visite de réception accessibilité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '1', 'VR-SI', 'Visite de réception sécurité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '1', 'VR-VP-ACC', 'Visites de réception et périodique accessibilité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '1', 'VR-VP-SI', 'Visites de réception et périodique sécurité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '1', 'VP-ACC', 'Visite périodique accessibilité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '2', 'VP-SI', 'Visite périodique sécurité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '2', 'VC-ACC', 'Visite de contrôle accessibilité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '2', 'VC-SI', 'Visite de contrôle sécurité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '2', 'EP-ACC', 'Étude de plan accessibilité');
INSERT INTO analyses_type
    (analyses_type, service, code, libelle)
    VALUES (nextval('analyses_type_seq'), '2', 'EP-SI', 'Étude de plan sécurité');
-- Contrainte de clé étrangère aux deux types d'analyse du DC
ALTER TABLE ONLY dossier_coordination_type
    ADD CONSTRAINT dossier_coordination_type_analyses_type_si_fkey FOREIGN KEY (analyses_type_si) REFERENCES analyses_type(analyses_type),
    ADD CONSTRAINT dossier_coordination_type_analyses_type_acc_fkey FOREIGN KEY (analyses_type_acc) REFERENCES analyses_type(analyses_type);
-- Ajoute les types d'analyse aux types de DC (données)
UPDATE dossier_coordination_type
    SET analyses_type_si = 10, analyses_type_acc = 9
    WHERE code = 'PCADS';
UPDATE dossier_coordination_type
    SET analyses_type_si = 10, analyses_type_acc = 9
    WHERE code = 'AT';
UPDATE dossier_coordination_type
    SET analyses_type_si = 2, analyses_type_acc = 1
    WHERE code = 'DR';
UPDATE dossier_coordination_type
    SET analyses_type_si = 8, analyses_type_acc = 7
    WHERE code = 'VISITCS';
UPDATE dossier_coordination_type
    SET analyses_type_si = 7, analyses_type_acc = 8
    WHERE code = 'VISITCA';
UPDATE dossier_coordination_type
    SET analyses_type_si = 6, analyses_type_acc = 5
    WHERE code = 'VISITEP';
UPDATE dossier_coordination_type
    SET analyses_type_si = 9, analyses_type_acc = 10
    WHERE code = 'DPS';
UPDATE dossier_coordination_type
    SET analyses_type_si = 9, analyses_type_acc = 10
    WHERE code = 'DAV';
UPDATE dossier_coordination_type
    SET analyses_type_si = 10, analyses_type_acc = 9
    WHERE code = 'DM';
-- Contrainte not null au type d'analyse du DC
ALTER TABLE dossier_coordination_type
    ALTER COLUMN analyses_type_si SET NOT NULL,
    ALTER COLUMN analyses_type_acc SET NOT NULL;
-- Paramétrage : essais réalisés
CREATE TABLE essai_realise (
    essai_realise integer NOT NULL,
    service integer NOT NULL,
    code character varying(20) NULL,
    libelle character varying(100) NULL,
    description text NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE essai_realise IS 'Essai réalisé';
COMMENT ON COLUMN essai_realise.essai_realise IS 'Identifiant unique';
COMMENT ON COLUMN essai_realise.service IS 'Service de l''essai réalisé';
COMMENT ON COLUMN essai_realise.code IS 'Code de l''essai réalisé';
COMMENT ON COLUMN essai_realise.libelle IS 'Libellé de l''essai réalisé';
COMMENT ON COLUMN essai_realise.description IS 'Description de l''essai réalisé';
COMMENT ON COLUMN essai_realise.om_validite_debut IS 'Date de début de validité de l''essai réalisé';
COMMENT ON COLUMN essai_realise.om_validite_fin IS 'Date de fin de validité de l''essai réalisé';
ALTER TABLE ONLY essai_realise
    ADD CONSTRAINT essai_realise_pkey PRIMARY KEY (essai_realise),
    ADD CONSTRAINT essai_realise_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE essai_realise_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY essai_realise.essai_realise;

-- Paramétrage : prescriptions règlementaires
CREATE TABLE prescription_reglementaire (
    prescription_reglementaire integer NOT NULL,
    service integer NOT NULL,
    tete_de_chapitre1 character varying(250) NULL,
    tete_de_chapitre2 character varying(250) NULL,
    libelle character varying(100) NULL,
    description text NULL,
    defavorable boolean NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE prescription_reglementaire IS 'Prescription réglementaire';
COMMENT ON COLUMN prescription_reglementaire.prescription_reglementaire IS 'Identifiant unique';
COMMENT ON COLUMN prescription_reglementaire.service IS 'Service de la prescription réglementaire';
COMMENT ON COLUMN prescription_reglementaire.tete_de_chapitre1 IS 'Valeur de la tête de chapitre n°1';
COMMENT ON COLUMN prescription_reglementaire.tete_de_chapitre2 IS 'Valeur de la tête de chapitre n°2';
COMMENT ON COLUMN prescription_reglementaire.libelle IS 'Libellé de la prescription réglementaire';
COMMENT ON COLUMN prescription_reglementaire.description IS 'Description de la prescription réglementaire';
COMMENT ON COLUMN prescription_reglementaire.defavorable IS 'Donne lieu à un défavorable ?';
COMMENT ON COLUMN prescription_reglementaire.om_validite_debut IS 'Date de début de validité de la prescription réglementaire';
COMMENT ON COLUMN prescription_reglementaire.om_validite_fin IS 'Date de fin de validité de la prescription réglementaire';
ALTER TABLE ONLY prescription_reglementaire
    ADD CONSTRAINT prescription_reglementaire_pkey PRIMARY KEY (prescription_reglementaire),
    ADD CONSTRAINT prescription_reglementaire_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE prescription_reglementaire_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY prescription_reglementaire.prescription_reglementaire;

-- Paramétrage : prescriptions spécifiques
CREATE TABLE prescription_specifique (
    prescription_specifique integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100) NULL,
    description text NULL,
    prescription_reglementaire integer NOT NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE prescription_specifique IS 'Prescription spécifique';
COMMENT ON COLUMN prescription_specifique.prescription_specifique IS 'Identifiant unique';
COMMENT ON COLUMN prescription_specifique.service IS 'Service de la prescription spécifique';
COMMENT ON COLUMN prescription_specifique.libelle IS 'Libellé de la prescription spécifique';
COMMENT ON COLUMN prescription_specifique.description IS 'Description de la prescription spécifique';
COMMENT ON COLUMN prescription_specifique.prescription_reglementaire IS 'Prescription réglementaire à laquelle elle se rattache';
COMMENT ON COLUMN prescription_specifique.om_validite_debut IS 'Date de début de validité de la prescription spécifique';
COMMENT ON COLUMN prescription_specifique.om_validite_fin IS 'Date de fin de validité de la prescription spécifique';
ALTER TABLE ONLY prescription_specifique
    ADD CONSTRAINT prescription_specifique_pkey PRIMARY KEY (prescription_specifique),
    ADD CONSTRAINT prescription_specifique_service_fkey FOREIGN KEY (service) REFERENCES service(service),
    ADD CONSTRAINT prescription_specifique_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);
CREATE SEQUENCE prescription_specifique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY prescription_specifique.prescription_specifique;

-- Paramétrage : avis d'analyse
CREATE TABLE analyses_avis (
    analyses_avis integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100) NULL,
    description text NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE analyses_avis IS 'Avis de l''analyse';
COMMENT ON COLUMN analyses_avis.analyses_avis IS 'Identifiant unique';
COMMENT ON COLUMN analyses_avis.service IS 'Service de l''avis de l''analyse';
COMMENT ON COLUMN analyses_avis.libelle IS 'Libellé de l''avis de l''analyse';
COMMENT ON COLUMN analyses_avis.description IS 'Description de l''avis de l''analyse';
COMMENT ON COLUMN analyses_avis.om_validite_debut IS 'Date de début de validité de l''avis de l''analyse';
COMMENT ON COLUMN analyses_avis.om_validite_fin IS 'Date de fin de validité de l''avis de l''analyse';
ALTER TABLE ONLY analyses_avis
    ADD CONSTRAINT analyses_avis_pkey PRIMARY KEY (analyses_avis),
    ADD CONSTRAINT analyses_avis_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE analyses_avis_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY analyses_avis.analyses_avis;

-- Paramétrage : document présenté
CREATE TABLE document_presente (
    document_presente integer NOT NULL,
    service integer NOT NULL,
    pendant boolean NOT NULL,
    libelle character varying(100) NULL,
    description text NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE document_presente IS 'Documents présentés';
COMMENT ON COLUMN document_presente.document_presente IS 'Identifiant unique';
COMMENT ON COLUMN document_presente.service IS 'Service du document présenté';
COMMENT ON COLUMN document_presente.pendant IS 'Si vrai document présenté pendant la visite, sinon après';
COMMENT ON COLUMN document_presente.libelle IS 'Libellé du document présenté';
COMMENT ON COLUMN document_presente.description IS 'Description du document présenté';
COMMENT ON COLUMN document_presente.om_validite_debut IS 'Date de début de validité du document présenté';
COMMENT ON COLUMN document_presente.om_validite_fin IS 'Date de fin de validité du document présenté';
ALTER TABLE ONLY document_presente
    ADD CONSTRAINT document_presente_pkey PRIMARY KEY (document_presente),
    ADD CONSTRAINT document_presente_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE document_presente_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY document_presente.document_presente;

-- Paramétrage : réglementations applicables
CREATE TABLE reglementation_applicable (
    reglementation_applicable integer NOT NULL,
    service integer NOT NULL,
    libelle character varying(100) NULL,
    description text NULL,
    om_validite_debut date NULL,
    om_validite_fin date NULL
);
COMMENT ON TABLE reglementation_applicable IS 'Réglementations applicables';
COMMENT ON COLUMN reglementation_applicable.reglementation_applicable IS 'Identifiant unique';
COMMENT ON COLUMN reglementation_applicable.service IS 'Service de la réglementation applicable';
COMMENT ON COLUMN reglementation_applicable.libelle IS 'Libellé de la réglementation applicable';
COMMENT ON COLUMN reglementation_applicable.description IS 'Description de la réglementation applicable';
COMMENT ON COLUMN reglementation_applicable.om_validite_debut IS 'Date de début de validité de la réglementation applicable';
COMMENT ON COLUMN reglementation_applicable.om_validite_fin IS 'Date de fin de validité de la réglementation applicable';
ALTER TABLE ONLY reglementation_applicable
    ADD CONSTRAINT reglementation_applicable_pkey PRIMARY KEY (reglementation_applicable),
    ADD CONSTRAINT reglementation_applicable_service_fkey FOREIGN KEY (service) REFERENCES service(service);
CREATE SEQUENCE reglementation_applicable_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY reglementation_applicable.reglementation_applicable;

-- Table métier : analyses
CREATE TABLE analyses (
    analyses integer NOT NULL,
    service integer NOT NULL,
    analyses_etat character varying(100) NOT NULL,
    analyses_type integer NULL,
    objet character varying(250) NULL,
    descriptif_etablissement_om_html text NULL,
    etablissement_type integer NULL,
    etablissement_categorie integer NULL,
    reglementation_applicable text NULL,
    compte_rendu_om_html text NULL,
    prescription_reglementaire integer NULL,
    document_presente_pendant text NULL,
    document_presente_apres text NULL,
    observation_om_html text NULL,
    analyses_avis integer NULL,
    avis_complement text NULL
);
COMMENT ON TABLE analyses IS 'Analyses réglementaire des procès verbaux';
COMMENT ON COLUMN analyses.analyses IS 'Identifiant unique';
COMMENT ON COLUMN analyses.service IS 'Service de l''analyses';
COMMENT ON COLUMN analyses.analyses_etat IS 'Etat de l''analyses (en cours de rédaction, terminée, validée, actée)';
COMMENT ON COLUMN analyses.analyses_type IS 'Type d''analyses';
COMMENT ON COLUMN analyses.objet IS 'Objet de l''analyses';
COMMENT ON COLUMN analyses.descriptif_etablissement_om_html IS 'Descriptif de l''établissement';
COMMENT ON COLUMN analyses.etablissement_type IS 'Type de l''établissement';
COMMENT ON COLUMN analyses.etablissement_categorie IS 'Catégorie de l''établissement';
COMMENT ON COLUMN analyses.reglementation_applicable IS 'Réglementations applicables (champ libre avec bible)';
COMMENT ON COLUMN analyses.compte_rendu_om_html IS 'Compte rendu';
COMMENT ON COLUMN analyses.prescription_reglementaire IS 'Prescription réglementaire';
COMMENT ON COLUMN analyses.document_presente_pendant IS 'Liste des pièces fournies pendant la visite qui ont été prises en compte pour prendre la décision';
COMMENT ON COLUMN analyses.document_presente_apres IS 'Liste des pièces fournies après la visite qui ont été prises en compte pour prendre la décision';
COMMENT ON COLUMN analyses.observation_om_html IS 'Observations de l''analyses';
COMMENT ON COLUMN analyses.analyses_avis IS 'Proposition d''avis qui sera reprise lors de la réunion plénière de la commission';
COMMENT ON COLUMN analyses.avis_complement IS 'Complément à la proposition d''avis';
ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_pkey PRIMARY KEY (analyses),
    ADD CONSTRAINT analyses_service_fkey FOREIGN KEY (service) REFERENCES service(service),
    ADD CONSTRAINT analyses_analyses_avis_fkey FOREIGN KEY (analyses_avis) REFERENCES analyses_avis(analyses_avis),
    ADD CONSTRAINT analyses_analyses_type_fkey FOREIGN KEY (analyses_type) REFERENCES analyses_type(analyses_type),
    ADD CONSTRAINT analyses_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type),
    ADD CONSTRAINT analyses_etablissement_categorie_fkey FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie),
    ADD CONSTRAINT analyses_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);
CREATE SEQUENCE analyses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY analyses.analyses;

-- Table métier : données techniques

CREATE TABLE donnee_technique (
    donnee_technique integer NOT NULL,
    analyses integer NOT NULL,
    service integer NOT NULL,
    si_descriptif_om_html text NULL,
    si_locaux_sommeil boolean NULL,
    si_effectif_public integer NULL,
    si_effectif_personnel integer NULL,
    si_type_ssi character varying(100) NULL,
    si_type_alarme character varying(100) NULL,
    si_conformite_i16 boolean NULL,
    si_alimentation_remplacement boolean NULL,
    si_service_securite boolean NULL,
    si_personnel_jour integer NULL,
    si_personnel_nuit integer NULL,
    acc_descriptif_om_html text NULL,
    acc_handicap_mental boolean NULL,
    acc_handicap_auditif boolean NULL,
    acc_places_stationnement_amenagees integer NULL,
    acc_elevateur boolean NULL,
    acc_handicap_physique boolean NULL,
    acc_ascenceur boolean NULL,
    acc_handicap_visuel boolean NULL,
    acc_boucle_magnetique boolean NULL,
    acc_chambres_amenagees integer NULL,
    acc_douche boolean NULL,
    acc_derogation_scda integer NULL,
    acc_sanitaire boolean NULL,
    acc_places_assises_public integer NULL
);
COMMENT ON TABLE donnee_technique IS 'Données techniques de l''analyse';
COMMENT ON COLUMN donnee_technique.donnee_technique IS 'Identifiant unique';
COMMENT ON COLUMN donnee_technique.analyses IS 'Analyse à laquelle sont rattachées les données techniques';
COMMENT ON COLUMN donnee_technique.service IS 'Service des données techniques';
COMMENT ON COLUMN donnee_technique.si_descriptif_om_html IS 'Description de la sécurité de l''établissement';
COMMENT ON COLUMN donnee_technique.si_locaux_sommeil IS 'Présence de locaux à sommeil ?';
COMMENT ON COLUMN donnee_technique.si_effectif_public IS 'Effectif public';
COMMENT ON COLUMN donnee_technique.si_effectif_personnel IS 'Effectif personnel';
COMMENT ON COLUMN donnee_technique.si_type_ssi IS 'Type SSI (A/B/C/D/E)';
COMMENT ON COLUMN donnee_technique.si_type_alarme IS 'Type d''alarme (1/2a/2b/3/4)';
COMMENT ON COLUMN donnee_technique.si_conformite_i16 IS 'Conforme au référentiel I16 ?';
COMMENT ON COLUMN donnee_technique.si_alimentation_remplacement IS 'Présence d''une alimentation de remplacement ?';
COMMENT ON COLUMN donnee_technique.si_service_securite IS 'Présence d''un service sécurité ?';
COMMENT ON COLUMN donnee_technique.si_personnel_jour IS 'Effectif personnel de jour';
COMMENT ON COLUMN donnee_technique.si_personnel_nuit IS 'Effectif personnel de nuit';
COMMENT ON COLUMN donnee_technique.acc_descriptif_om_html IS 'Description de l''accessibilité de l''établissement';
COMMENT ON COLUMN donnee_technique.acc_handicap_mental IS 'Établissement accessible aux personnes atteintes d''un handicap mental ?';
COMMENT ON COLUMN donnee_technique.acc_handicap_auditif IS 'Établissement accessible aux personnes atteintes d''un handicap auditif ?';
COMMENT ON COLUMN donnee_technique.acc_places_stationnement_amenagees IS 'Nombre de places de stationnement aménagées';
COMMENT ON COLUMN donnee_technique.acc_elevateur IS 'Présence d''un élévateur ?';
COMMENT ON COLUMN donnee_technique.acc_handicap_physique IS 'Établissement accessible aux personnes atteintes d''un handicap physique ?';
COMMENT ON COLUMN donnee_technique.acc_ascenceur IS 'Présence d''un ascenseur ?';
COMMENT ON COLUMN donnee_technique.acc_handicap_visuel IS 'Établissement accessible aux personnes atteintes d''un handicap visuel ?';
COMMENT ON COLUMN donnee_technique.acc_boucle_magnetique IS 'Présence d''une boucle magnétique ?';
COMMENT ON COLUMN donnee_technique.acc_chambres_amenagees IS 'Nombre de chambres aménagées pour être accessibles';
COMMENT ON COLUMN donnee_technique.acc_douche IS 'Présence de douche(s) ?';
COMMENT ON COLUMN donnee_technique.acc_derogation_scda IS 'Dérogation SCDA';
COMMENT ON COLUMN donnee_technique.acc_sanitaire IS 'Présence de sanitaire(s) ?';
COMMENT ON COLUMN donnee_technique.acc_places_assises_public IS 'Nombre de places assises pour le public';
ALTER TABLE ONLY donnee_technique
    ADD CONSTRAINT donnee_technique_pkey PRIMARY KEY (donnee_technique),
    ADD CONSTRAINT donnee_technique_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses),
    ADD CONSTRAINT donnee_technique_service_fkey FOREIGN KEY (service) REFERENCES service(service),
    ADD CONSTRAINT donnee_technique_acc_derogation_scda_fkey FOREIGN KEY (acc_derogation_scda) REFERENCES derogation_scda(derogation_scda);
CREATE SEQUENCE donnee_technique_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY donnee_technique.donnee_technique;

-- Liaison : prescription règlementaire avec type d'établissement
CREATE TABLE lien_prescription_reglementaire_etablissement_type (
    lien_prescription_reglementaire_etablissement_type integer NOT NULL,
    prescription_reglementaire integer NOT NULL,
    etablissement_type integer NOT NULL
);
COMMENT ON TABLE lien_prescription_reglementaire_etablissement_type IS 'Liste des types d''établissement concernés par la prescription réglementaire';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type IS 'Identifiant unique';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.prescription_reglementaire IS 'Prescription réglementaire';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_type.etablissement_type IS 'Type de l''établissement';
ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_type
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_pkey PRIMARY KEY (lien_prescription_reglementaire_etablissement_type),
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire),
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_type_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);
CREATE SEQUENCE lien_prescription_reglementaire_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_prescription_reglementaire_etablissement_type.lien_prescription_reglementaire_etablissement_type;

-- Liaison : prescription règlementaire avec catégorie d'établissement
CREATE TABLE lien_prescription_reglementaire_etablissement_categorie (
    lien_prescription_reglementaire_etablissement_categorie integer NOT NULL,
    prescription_reglementaire integer NOT NULL,
    etablissement_categorie integer NOT NULL
);
COMMENT ON TABLE lien_prescription_reglementaire_etablissement_categorie IS 'Liste des catégories d''établissement concernés par la prescription réglementaire';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie IS 'Identifiant unique';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire IS 'Prescription réglementaire';
COMMENT ON COLUMN lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie IS 'Catégorie de l''établissement';
ALTER TABLE ONLY lien_prescription_reglementaire_etablissement_categorie
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_pkey PRIMARY KEY (lien_prescription_reglementaire_etablissement_categorie),
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire),
    ADD CONSTRAINT lien_prescription_reglementaire_etablissement_categorie_etablissement_categorie_fkey FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);
CREATE SEQUENCE lien_prescription_reglementaire_etablissement_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_prescription_reglementaire_etablissement_categorie.lien_prescription_reglementaire_etablissement_categorie;

-- Liaison : réglementation applicable avec type d'établissement
CREATE TABLE lien_reglementation_applicable_etablissement_type (
    lien_reglementation_applicable_etablissement_type integer NOT NULL,
    reglementation_applicable integer NOT NULL,
    etablissement_type integer NOT NULL
);
COMMENT ON TABLE lien_reglementation_applicable_etablissement_type IS 'Liste des types d''établissement concernés par la réglementation applicable';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type IS 'Identifiant unique';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.reglementation_applicable IS 'Réglementation applicable';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_type.etablissement_type IS 'Type de l''établissement';
ALTER TABLE ONLY lien_reglementation_applicable_etablissement_type
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_pkey PRIMARY KEY (lien_reglementation_applicable_etablissement_type),
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_reglementation_applicable_fkey FOREIGN KEY (reglementation_applicable) REFERENCES reglementation_applicable(reglementation_applicable),
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_type_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);
CREATE SEQUENCE lien_reglementation_applicable_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_reglementation_applicable_etablissement_type.lien_reglementation_applicable_etablissement_type;

-- Liaison : réglementation applicable avec catégorie d'établissement
CREATE TABLE lien_reglementation_applicable_etablissement_categorie (
    lien_reglementation_applicable_etablissement_categorie integer NOT NULL,
    reglementation_applicable integer NOT NULL,
    etablissement_categorie integer NOT NULL
);
COMMENT ON TABLE lien_reglementation_applicable_etablissement_categorie IS 'Liste des catégories d''établissement concernées par la réglementation applicable';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie IS 'Identifiant unique';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.reglementation_applicable IS 'Réglementation applicable';
COMMENT ON COLUMN lien_reglementation_applicable_etablissement_categorie.etablissement_categorie IS 'Catégorie de l''établissement';
ALTER TABLE ONLY lien_reglementation_applicable_etablissement_categorie
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_pkey PRIMARY KEY (lien_reglementation_applicable_etablissement_categorie),
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_reglementation_applicable_fkey FOREIGN KEY (reglementation_applicable) REFERENCES reglementation_applicable(reglementation_applicable),
    ADD CONSTRAINT lien_reglementation_applicable_etablissement_categorie_etablissement_categorie_fkey FOREIGN KEY (etablissement_categorie) REFERENCES etablissement_categorie(etablissement_categorie);
CREATE SEQUENCE lien_reglementation_applicable_etablissement_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_reglementation_applicable_etablissement_categorie.lien_reglementation_applicable_etablissement_categorie;

-- Liaison : analyses avec types secondaires d'établissement
CREATE TABLE lien_analyses_etablissement_type (
    lien_analyses_etablissement_type integer NOT NULL,
    analyses integer NOT NULL,
    etablissement_type integer NOT NULL
);
COMMENT ON TABLE lien_analyses_etablissement_type IS 'Liste des types secondaires d''établissement concernés par l''analyse''';
COMMENT ON COLUMN lien_analyses_etablissement_type.lien_analyses_etablissement_type IS 'Identifiant unique';
COMMENT ON COLUMN lien_analyses_etablissement_type.analyses IS 'Analyse';
COMMENT ON COLUMN lien_analyses_etablissement_type.etablissement_type IS 'Type de l''établissement';
ALTER TABLE ONLY lien_analyses_etablissement_type
    ADD CONSTRAINT lien_analyses_etablissement_type_pkey PRIMARY KEY (lien_analyses_etablissement_type),
    ADD CONSTRAINT lien_analyses_etablissement_type_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses),
    ADD CONSTRAINT lien_analyses_etablissement_type_etablissement_type_fkey FOREIGN KEY (etablissement_type) REFERENCES etablissement_type(etablissement_type);
CREATE SEQUENCE lien_analyses_etablissement_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_analyses_etablissement_type.lien_analyses_etablissement_type;

-- Liaison : essais réalisés avec analyses
CREATE TABLE lien_essai_realise_analyses (
    lien_essai_realise_analyses integer NOT NULL,
    essai_realise integer NOT NULL,
    analyses integer NOT NULL,
    concluant boolean,
    complement text
);
COMMENT ON TABLE lien_essai_realise_analyses IS 'Liste des essais réalisés par analyses''';
COMMENT ON COLUMN lien_essai_realise_analyses.lien_essai_realise_analyses IS 'Identifiant unique';
COMMENT ON COLUMN lien_essai_realise_analyses.essai_realise IS 'Essai réalisé';
COMMENT ON COLUMN lien_essai_realise_analyses.analyses IS 'Analyse';
COMMENT ON COLUMN lien_essai_realise_analyses.concluant IS 'Est-ce que l''essai réalisé a été concluant ?';
COMMENT ON COLUMN lien_essai_realise_analyses.complement IS 'Complément';
ALTER TABLE ONLY lien_essai_realise_analyses
    ADD CONSTRAINT lien_essai_realise_analyses_pkey PRIMARY KEY (lien_essai_realise_analyses),
    ADD CONSTRAINT lien_essai_realise_analyses_essai_realise_fkey FOREIGN KEY (essai_realise) REFERENCES essai_realise(essai_realise),
    ADD CONSTRAINT lien_essai_realise_analyses_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses);
CREATE SEQUENCE lien_essai_realise_analyses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_essai_realise_analyses.lien_essai_realise_analyses;

-- Liaison : prescriptions spécifiques avec analyses
CREATE TABLE lien_prescription_specifique_analyses (
    lien_prescription_specifique_analyses integer NOT NULL,
    prescription_specifique integer NOT NULL,
    analyses integer NOT NULL,
    ordre integer NULL
);
COMMENT ON TABLE lien_prescription_specifique_analyses IS 'Liste des prescriptions spécifiques par analyse''';
COMMENT ON COLUMN lien_prescription_specifique_analyses.lien_prescription_specifique_analyses IS 'Identifiant unique';
COMMENT ON COLUMN lien_prescription_specifique_analyses.prescription_specifique IS 'Prescription spécifique';
COMMENT ON COLUMN lien_prescription_specifique_analyses.analyses IS 'Analyse';
COMMENT ON COLUMN lien_prescription_specifique_analyses.ordre IS 'Ordre de la prescription dans la vue de l''analyse';
ALTER TABLE ONLY lien_prescription_specifique_analyses
    ADD CONSTRAINT lien_prescription_specifique_analyses_pkey PRIMARY KEY (lien_prescription_specifique_analyses),
    ADD CONSTRAINT lien_prescription_specifique_analyses_prescription_specifique_fkey FOREIGN KEY (prescription_specifique) REFERENCES prescription_specifique(prescription_specifique),
    ADD CONSTRAINT lien_prescription_specifique_analyses_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses);
CREATE SEQUENCE lien_prescription_specifique_analyses_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_prescription_specifique_analyses.lien_prescription_specifique_analyses;

-- Liaison : types de dossier de coordination avec types d'analyse
CREATE TABLE lien_dossier_coordination_type_analyses_type (
    lien_dossier_coordination_type_analyses_type integer NOT NULL,
    dossier_coordination_type integer NOT NULL,
    analyses_type integer NOT NULL
);
COMMENT ON TABLE lien_dossier_coordination_type_analyses_type IS 'Liste des types d''analyse par type de dossier de coordination';
COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type IS 'Identifiant unique';
COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.dossier_coordination_type IS 'Type de dossier de coordination';
COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.analyses_type IS 'Type d''analyse';
ALTER TABLE ONLY lien_dossier_coordination_type_analyses_type
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_pkey PRIMARY KEY (lien_dossier_coordination_type_analyses_type),
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_dossier_coordination_type_fkey FOREIGN KEY (dossier_coordination_type) REFERENCES dossier_coordination_type(dossier_coordination_type),
    ADD CONSTRAINT lien_dossier_coordination_type_analyses_type_analyses_type_fkey FOREIGN KEY (analyses_type) REFERENCES analyses_type(analyses_type);
CREATE SEQUENCE lien_dossier_coordination_type_analyses_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY lien_dossier_coordination_type_analyses_type.lien_dossier_coordination_type_analyses_type;

-- Ajout de la clé étrangère analyse dans la table dossier instruction
ALTER TABLE dossier_instruction
    ADD analyses integer NULL;
COMMENT ON COLUMN dossier_instruction.analyses IS 'Analyse';
ALTER TABLE ONLY dossier_instruction
    ADD CONSTRAINT dossier_instruction_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses);

--Ajout de données techniques de sécurite dans la table établissement
--pour garder l'homogénité avec les données techniques de l'analyse

ALTER TABLE etablissement
ADD si_type_ssi character varying(100) NULL,
ADD si_conformite_i16 boolean NULL,
ADD si_alimentation_remplacement boolean NULL,
ADD si_service_securite boolean NULL,
ADD si_personnel_jour integer NULL,
ADD si_personnel_nuit integer NULL;

COMMENT ON COLUMN etablissement.si_type_ssi IS 'Type SSI (A/B/C/D/E)';
COMMENT ON COLUMN etablissement.si_conformite_i16 IS 'Conforme au référentiel I16 ?';
COMMENT ON COLUMN etablissement.si_alimentation_remplacement IS 'Présence d''une alimentation de remplacement ?';
COMMENT ON COLUMN etablissement.si_service_securite IS 'Présence d''un service sécurité ?';
COMMENT ON COLUMN etablissement.si_personnel_jour IS 'Effectif personnel de jour';
COMMENT ON COLUMN etablissement.si_personnel_nuit IS 'Effectif personnel de nuit';

--
-- END / Création des tables métier analyse et données techniques,
-- ainsi que les tables de paramétrage et de liaison associées.

--Ajout du widget des analyses à valider pour les cadres
INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
SELECT nextval('om_widget_seq'), 'Analyses à valider', 'analyse_a_valider', '', 'file'
WHERE
    NOT EXISTS (
        SELECT libelle FROM om_widget WHERE libelle = 'Analyses à valider'
    );
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
SELECT nextval('om_dashboard_seq'),(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE'), 'C2',   1,  (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à valider')
WHERE
    NOT EXISTS (
        SELECT om_dashboard FROM om_dashboard WHERE om_widget = (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à valider') AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );

-- Ajout des droits au profil cadre pour la consultation d'analyse
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE')
    );