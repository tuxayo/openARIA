--
\set profil '\'CADRE SI\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
--
\set profil '\'CADRE ACC\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
--
\set profil '\'TECHNICIEN SI\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
--
\set profil '\'TECHNICIEN ACC\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
--
\set profil '\'SECRETAIRE SI\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
\set profil '\'SECRETAIRE ACC\''
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'proces_verbal_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'proces_verbal_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_a_editer_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_a_editer_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_signature_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_signature_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
INSERT INTO om_droit (om_droit, libelle, om_profil)
SELECT nextval('om_droit_seq'),'courrier_attente_retour_ar_send_pv_to_referentiel_ads',(SELECT om_profil FROM om_profil WHERE libelle = :profil)
WHERE
    NOT EXISTS (
        SELECT om_droit FROM om_droit WHERE libelle = 'courrier_attente_retour_ar_send_pv_to_referentiel_ads' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = :profil)
    );
