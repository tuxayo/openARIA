--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a28
--
-- XXX Ce fichier doit être renommé en v1.0.0-a28.sql au moment de la release
--
-- @package openAria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Modification du type du champ objet de la table analyses
ALTER TABLE analyses ALTER COLUMN objet TYPE text;

-- Ajout du bloc proposition de décision autorité de police dans l'analyse
ALTER TABLE analyses
    ADD dec1 character varying(250) NULL,
    ADD delai1 character varying(250) NULL,
    ADD dec2 character varying(250) NULL,
    ADD delai2 character varying(250) NULL;
COMMENT ON COLUMN analyses.dec1 IS 'Proposition de décision d''autorité de police 1';
COMMENT ON COLUMN analyses.delai1 IS 'Délais 1';
COMMENT ON COLUMN analyses.dec2 IS 'Proposition de décision d''autorité de police 2';
COMMENT ON COLUMN analyses.delai2 IS 'Délais 2';