-------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a39
--
-- @package openaria
-- @version SVN : $Id$
-------------------------------------------------------------------------------

DELETE FROM om_parametre WHERE libelle = 'option_message_ws';

-------------------------------------------------------------------------------
-- BEGIN - Modele de données 'messages'
-------------------------------------------------------------------------------
--
CREATE TABLE dossier_coordination_message (
    dossier_coordination_message integer NOT NULL,
    categorie character varying(20) NOT NULL,
    dossier_coordination integer,
    type character varying(60),
    emetteur character varying(40),
    date_emission timestamp without time zone NOT NULL,
    contenu text,
    contenu_json text,
    si_cadre_lu boolean,
    si_technicien_lu boolean,
    si_mode_lecture character varying(10),
    acc_cadre_lu boolean,
    acc_technicien_lu boolean,
    acc_mode_lecture character varying(10)
);
--
CREATE SEQUENCE dossier_coordination_message_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
--
ALTER SEQUENCE dossier_coordination_message_seq OWNED BY dossier_coordination_message.dossier_coordination_message;
--
ALTER TABLE ONLY dossier_coordination_message
    ADD CONSTRAINT dossier_coordination_message_pkey PRIMARY KEY (dossier_coordination_message);
ALTER TABLE ONLY dossier_coordination_message
    ADD CONSTRAINT dossier_coordination_message_dossier_coordination_fkey FOREIGN KEY (dossier_coordination) REFERENCES dossier_coordination(dossier_coordination);
-------------------------------------------------------------------------------
-- END - Modele de données 'messages'
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- BEGIN - Le widget 'Mes messages'
-------------------------------------------------------------------------------
--
INSERT INTO om_widget (om_widget, libelle, lien, texte, type) 
VALUES (nextval('om_widget_seq'), 'Mes messages', 'message_mes_non_lu', '', 'file');
--
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) 
VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) 
VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) 
VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) 
VALUES (nextval('om_dashboard_seq'), (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC'), 'C1', 1, (SELECT om_widget FROM om_widget WHERE lien = 'message_mes_non_lu'));
-------------------------------------------------------------------------------
-- END - Le widget 'Mes messages'
-------------------------------------------------------------------------------

--
--
\set profil '\'CADRE SI\''
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
(nextval('om_droit_seq'), 'dossier_coordination_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_cloturer_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_cloturer_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_qualifier_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_qualifier_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_si_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_si_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_si_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_si_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_mes_non_lu_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil))
;
--
\set profil '\'CADRE ACC\''
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
(nextval('om_droit_seq'), 'dossier_coordination_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_cloturer_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_cloturer_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_qualifier_marquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_a_qualifier_demarquer_a_enjeu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_acc_cadre_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_acc_cadre_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_tous_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_mes_non_lu_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil))
;
--
\set profil '\'TECHNICIEN SI\''
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_si_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_si_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_mes_non_lu_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil))
;
--
\set profil '\'TECHNICIEN ACC\''
INSERT INTO om_droit (om_droit, libelle, om_profil) VALUES
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_dc_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_consulter', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_contexte_di_marquer_acc_technicien_comme_non_lu', (SELECT om_profil FROM om_profil WHERE libelle = :profil)),
(nextval('om_droit_seq'), 'dossier_coordination_message_mes_non_lu_tab', (SELECT om_profil FROM om_profil WHERE libelle = :profil))
;
--
--
--

-------------------------------------------------------------------------------
-- BEGIN - Le marqueur indiquant la connexion au référentiel ADS
-------------------------------------------------------------------------------
ALTER TABLE dossier_coordination ADD interface_referentiel_ads boolean DEFAULT false;
-------------------------------------------------------------------------------
-- END - Le marqueur indiquant la connexion au référentiel ADS
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- BEGIN - Marquer un dossier de coordination à enjeu
-------------------------------------------------------------------------------
ALTER TABLE dossier_coordination ADD enjeu_erp boolean;
-------------------------------------------------------------------------------
-- END - Marquer un dossier de coordination à enjeu
-------------------------------------------------------------------------------

--
--
--
ALTER TABLE reunion_avis ADD COLUMN categorie character varying(50);
--
--
--

--
--
--
INSERT INTO contact_type (contact_type, libelle, code, description, om_validite_debut, om_validite_fin) VALUES (nextval('contact_type_seq'), '(*) Autres Demandeurs', 'AUTR', 'Ce sont les autres pétitionnaires du dossier de coordination', NULL, NULL);
--
--
--


-------------------------------------------------------------------------------
-- BEGIN - Marqueur indiquant le dépôt de pièces
-------------------------------------------------------------------------------
ALTER TABLE dossier_coordination ADD depot_de_piece boolean;
-------------------------------------------------------------------------------
-- END - Marqueur indiquant le dépôt de pièces
-------------------------------------------------------------------------------

