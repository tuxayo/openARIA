--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a3
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

--Ajout des données de paramétrage des essais réalisés
INSERT INTO essai_realise (essai_realise, service, code, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC-ETR',
        'Entrée accessible aux fauteils roulants',
        'Présence d''une pente douce à l''entrée'
    );
INSERT INTO essai_realise (essai_realise, service, code, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC-ETG',
        'Étage accessible aux fauteuils roulants',
        'Présence d''un ascenseur d''une surface minimum de 2 mètres carré'
    );
INSERT INTO essai_realise (essai_realise, service, code, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'SI'),
        'SI-DF',
        'Détection de fumée',
        'Alarme déclenchée par injection de 5 mètres cube de fumée à plus de trois mètres d''un détecteur'
    );
INSERT INTO essai_realise (essai_realise, service, code, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'SI'),
        'SI-EVC',
        'Évacution des locaux',
        'Test d''alarme incendie : évacuation des personnes en moins de cinq minutes'
    );

--Ajout des données de paramétrage des avis d'analyses
INSERT INTO analyses_avis (analyses_avis, service, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'ACC'),
        'Sous réserve',
        'L''ensemble est correct mais des points restent à améliorer.'
    );
INSERT INTO analyses_avis (analyses_avis, service, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'ACC'),
        'Déconseillé',
        'La plupart des exigences ne sont pas respectées.'
    );
INSERT INTO analyses_avis (analyses_avis, service, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'SI'),
        'Favorable',
        'Bon pour ouverture'
    );
INSERT INTO analyses_avis (analyses_avis, service, libelle, description)
    VALUES (nextval('essai_realise_seq'),
        (SELECT service from service where code = 'SI'),
        'Défavorable',
        'Manque de sécurité évident.'
    );

-- Ajout de données techniques pour l'analyse du jeu de données
INSERT INTO donnee_technique (donnee_technique, analyses, service)
    VALUES (nextval('donnee_technique_seq'),
        (SELECT analyses from dossier_instruction where libelle = 'DR-VISIT-000007-SI'),
        (SELECT service from service where code = 'SI')
    );

-- -- Ajout du widget analyse à acter
-- INSERT INTO om_widget (om_widget, libelle, lien, texte, type)
-- SELECT nextval('om_widget_seq'), 'Analyses à acter', 'analyse_a_acter', '', 'file'
-- WHERE
--     NOT EXISTS (
--         SELECT libelle FROM om_widget WHERE libelle = 'Analyses à acter'
--     );
-- INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
-- SELECT nextval('om_dashboard_seq'),(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI'), 'C2',   1,  (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à acter')
-- WHERE
--     NOT EXISTS (
--         SELECT om_dashboard FROM om_dashboard WHERE om_widget = (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à acter') AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
--     );
-- INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget)
-- SELECT nextval('om_dashboard_seq'),(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC'), 'C2',   1,  (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à acter')
-- WHERE
--     NOT EXISTS (
--         SELECT om_dashboard FROM om_dashboard WHERE om_widget = (SELECT om_widget FROM om_widget WHERE libelle = 'Analyses à acter') AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
--     );

-- -- Ajout des droits au profil cadre pour la consultation d'analyse
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
--     );

-- Transformation en texte riche des documents présentés de l'analyse
ALTER TABLE analyses
    RENAME document_presente_pendant TO document_presente_pendant_om_html;
ALTER TABLE analyses
    RENAME document_presente_apres TO document_presente_apres_om_html;
ALTER TABLE document_presente
    RENAME description TO description_om_html;

-- Changement du nom du champ afin de coller à l'interface
ALTER TABLE analyses
    RENAME reglementation_applicable TO reglementation_applicable_om_html;

-- Suppression du booléen inutile "pendant" dans la table "document_presente"
ALTER TABLE document_presente
    DROP pendant;

--Ajout des données de paramétrage des documents présentés
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'SI'),
        'Conformité I16',
        'Établissement certifié I16.'
    );
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'SI'),
        'Conformité alarme incendie',
        'Évacuation dans les temps réglementaires.'
    );
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'SI'),
        'Conformité poste de sécurité',
        'Présence d''un poste de sécurité conforme aux normes en vigueur.'
    );
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'ACC'),
        'Conformité handicap physique',
        'Établissement accessible aux personnes présentant un handicap physique.'
    );
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'ACC'),
        'Conformité handicap auditif',
        'Établissement accessible aux personnes présentant un handicap auditif.'
    );
INSERT INTO document_presente (document_presente, service, libelle, description_om_html)
    VALUES (nextval('document_presente_seq'),
        (SELECT service from service where code = 'ACC'),
        'Conformité handicap visuel',
        'Établissement accessible aux personnes présentant un handicap visuel.'
    );

-- --Ajout de droits manquants
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
--     );
--
-- START / Suivi des pièces
--

-- Modification de la table piece_type
ALTER TABLE ONLY piece_type
    ADD COLUMN description character varying(250),
    ADD COLUMN om_validite_debut date,
    ADD COLUMN om_validite_fin date NULL;
-- Commentaires de la table piece_type
COMMENT ON COLUMN piece_type.description IS 'Description détaillée du type de pièce.';
COMMENT ON COLUMN piece_type.om_validite_debut IS 'Date de début de validité.';
COMMENT ON COLUMN piece_type.om_validite_debut IS 'Date de fin de validité.';

-- Création de la table piece_statut
CREATE TABLE piece_statut (
    piece_statut integer NOT NULL,
    libelle character varying(100),
    code character varying(20),
    description text,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires de la table piece_statut
COMMENT ON TABLE piece_statut IS 'Statut possible d''une pièce (document entrant).';
COMMENT ON COLUMN piece_statut.piece_statut IS 'Identifiant unique.';
COMMENT ON COLUMN piece_statut.libelle IS 'Libellé du statut de pièce.';
COMMENT ON COLUMN piece_statut.code IS 'Code du statut de pièce.';
COMMENT ON COLUMN piece_statut.description IS 'Description du statut de pièce.';
COMMENT ON COLUMN piece_statut.om_validite_debut IS 'Date de début de validité du statut de pièce.';
COMMENT ON COLUMN piece_statut.om_validite_fin IS 'Date de fin de validité du statut de pièce.';
-- Contraintes de la table piece_statut
ALTER TABLE ONLY piece_statut
    ADD CONSTRAINT piece_statut_pkey PRIMARY KEY (piece_statut);
-- Séquence de la table piece_statut
CREATE SEQUENCE piece_statut_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE piece_statut_seq OWNED BY piece_statut.piece_statut;
-- Données de la table piece_statut
INSERT INTO piece_statut (piece_statut, libelle, code, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('piece_statut_seq'), 'En cours', 'ENCOURS', NULL, NULL, NULL);
INSERT INTO piece_statut (piece_statut, libelle, code, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('piece_statut_seq'), 'Qualifié', 'QUALIF', NULL, NULL, NULL);
INSERT INTO piece_statut (piece_statut, libelle, code, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('piece_statut_seq'), 'Validé', 'VALID', NULL, NULL, NULL);

-- Modification de la table piece
ALTER TABLE ONLY piece
    ADD COLUMN date_reception date,
    ADD COLUMN date_emission date,
    ADD COLUMN piece_statut integer,
    ADD COLUMN date_butoir date,
    ADD COLUMN suivi boolean,
    ADD COLUMN commentaire_suivi text,
    ADD COLUMN lu boolean,
    ADD COLUMN choix_lien character varying(40);
-- Commentaires de la table piece
COMMENT ON COLUMN piece.dossier_coordination IS 'Lien vers le dossier de coordination.';
COMMENT ON COLUMN piece.uid IS 'Identifiant unique du fichier.';
COMMENT ON COLUMN piece.dossier_instruction IS 'Lien vers le dossier d''instruction.';
COMMENT ON COLUMN piece.date_reception IS 'Date de réception de la pièce.';
COMMENT ON COLUMN piece.date_emission IS 'Date d''émission de la pièce.';
COMMENT ON COLUMN piece.piece_statut IS 'Statut de la pièce.';
COMMENT ON COLUMN piece.date_butoir IS 'Date butoir de la pièce.';
COMMENT ON COLUMN piece.suivi IS 'Indique que la pièce est suivi.';
COMMENT ON COLUMN piece.commentaire_suivi IS 'Commentaire sur le suivi de la pièce.';
COMMENT ON COLUMN piece.lu IS 'Indique que la pièce a été lu.';
COMMENT ON COLUMN piece.choix_lien IS 'Choix de l''élément de liaison (établissement, dc ou di).';
-- Contraintes de la table piece
ALTER TABLE ONLY piece
    ADD CONSTRAINT piece_piece_statut_fkey FOREIGN KEY (piece_statut) REFERENCES piece_statut(piece_statut);

-- Commentaires de la table courrier_type
COMMENT ON COLUMN courrier_type.courrier_type IS 'Identifiant unique.';
COMMENT ON COLUMN courrier_type.code IS 'Code du type de courrier.';
COMMENT ON COLUMN courrier_type.libelle IS 'Libellé du type de courrier.';
COMMENT ON COLUMN courrier_type.description IS 'Description du type de courrier.';
COMMENT ON COLUMN courrier_type.om_validite_debut IS 'Date de début de validité du type de courrier.';
COMMENT ON COLUMN courrier_type.om_validite_fin IS 'Date de fin de validité du type de courrier.';
-- Données de la table courrier_type
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('courrier_type_seq'), 'NOTIFAP', 'Notification d''Autorité de Police', NULL, NULL, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('courrier_type_seq'), 'COUS', 'Courrier simple', NULL, NULL, NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('courrier_type_seq'), 'ENVPV', 'Envoi d''un Procés-Verbal', NULL, NULL, NULL);

-- Création de la table modele_edition
CREATE TABLE modele_edition (
    modele_edition integer NOT NULL,
    libelle character varying(100),
    description text,
    om_lettretype integer,
    om_etat integer,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaire de la table modele_edition
COMMENT ON TABLE modele_edition IS 'Modèle d''édition pour les courriers générés.';
COMMENT ON COLUMN modele_edition.modele_edition IS 'Identifiant unique.';
COMMENT ON COLUMN modele_edition.libelle IS 'Libellé du modèle d''édition.';
COMMENT ON COLUMN modele_edition.description IS 'Description du modèle d''édition.';
COMMENT ON COLUMN modele_edition.om_lettretype IS 'Lien vers une lettre-type.';
COMMENT ON COLUMN modele_edition.om_etat IS 'Lien vers un état.';
COMMENT ON COLUMN modele_edition.om_validite_debut IS 'Date de début de validité du modèle d''édition.';
COMMENT ON COLUMN modele_edition.om_validite_fin IS 'Date de fin de validité du modèle d''édition.';
-- Contraintes de la table modele_edition
ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_pkey PRIMARY KEY (modele_edition);
ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_om_lettretype_fkey FOREIGN KEY (om_lettretype) REFERENCES om_lettretype(om_lettretype);
ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_om_etat_fkey FOREIGN KEY (om_etat) REFERENCES om_etat(om_etat);
-- Séquence de la table modele_edition
CREATE SEQUENCE modele_edition_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE modele_edition_seq OWNED BY modele_edition.modele_edition;

-- Contraintes de la table courrier_param
ALTER TABLE ONLY courrier_param DROP CONSTRAINT courrier_param_courrier_type_fkey;
-- Modification de la table courrier_param
ALTER TABLE ONLY courrier_param
    DROP COLUMN om_lettretype,
    DROP COLUMN delai;
-- Commentaires de la table courrier_param
COMMENT ON COLUMN courrier_param.courrier_param IS 'Identifiant unique.';
COMMENT ON COLUMN courrier_param.libelle IS 'Libellé du paramétrage des courriers.';
COMMENT ON COLUMN courrier_param.description IS 'Description du paramétrage des courriers.';
COMMENT ON COLUMN courrier_param.courrier_type IS 'Type de courrier du paramétrage des courriers.';
COMMENT ON COLUMN courrier_param.service IS 'Service du paramétrage des courriers.';
COMMENT ON COLUMN courrier_param.om_validite_debut IS 'Date de début de validité du paramétrage des courriers.';
COMMENT ON COLUMN courrier_param.om_validite_fin IS 'Date de fin de validité du paramétrage des courriers.';

-- Suppression de table courrier_avis et courrier_avis_type
ALTER TABLE ONLY courrier DROP CONSTRAINT courrier_courrier_avis_fkey;
ALTER TABLE ONLY courrier_avis DROP CONSTRAINT courrier_avis_courrier_avis_type_fkey;
DROP TABLE courrier_avis_type, courrier_avis;

-- Création de la table signataire_qualite
CREATE TABLE signataire_qualite (
    signataire_qualite integer NOT NULL,
    code character varying(20),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires de la table signataire_qualite
COMMENT ON TABLE signataire_qualite IS 'Qualité des signataires.';
COMMENT ON COLUMN signataire_qualite.signataire_qualite IS 'Identifiant unique.';
COMMENT ON COLUMN signataire_qualite.code IS 'Code de la qualité de signataire.';
COMMENT ON COLUMN signataire_qualite.libelle IS 'Libellé de la qualité de signataire.';
COMMENT ON COLUMN signataire_qualite.description IS 'Description de la qualité de signataire.';
COMMENT ON COLUMN signataire_qualite.om_validite_debut IS 'Date de début de validité de la qualité de signataire.';
COMMENT ON COLUMN signataire_qualite.om_validite_fin IS 'Date de fin de validité de la qualité de signataire.';
-- Contraintes de la table signataire_qualite
ALTER TABLE ONLY signataire_qualite
    ADD CONSTRAINT signataire_qualite_pkey PRIMARY KEY (signataire_qualite);
-- Séquence de la table signataire_qualite
CREATE SEQUENCE signataire_qualite_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE signataire_qualite_seq OWNED BY signataire_qualite.signataire_qualite;
-- Données d'initialisation de la table signataire_qualite
INSERT INTO signataire_qualite (signataire_qualite, code, libelle, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('signataire_qualite_seq'), 'MAIRE', 'Maire', NULL, NULL, NULL);
INSERT INTO signataire_qualite (signataire_qualite, code, libelle, description, om_validite_debut, om_validite_fin)
    VALUES (nextval('signataire_qualite_seq'), 'PREFET', 'Préfet', NULL, NULL, NULL);

-- Création de la table signataire
CREATE TABLE signataire (
    signataire integer NOT NULL,
    nom character varying(100),
    prenom character varying(100),
    civilite integer,
    signataire_qualite integer,
    signature text,
    defaut boolean DEFAULT false,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires de la table signataire
COMMENT ON TABLE signataire IS 'Liste des signataires de courriers sortants (documents générés).';
COMMENT ON COLUMN signataire.signataire IS 'Identifiant unique.';
COMMENT ON COLUMN signataire.nom IS 'Nom du signataire.';
COMMENT ON COLUMN signataire.prenom IS 'Prénom du signataire.';
COMMENT ON COLUMN signataire.civilite IS 'Civilité du signataire.';
COMMENT ON COLUMN signataire.signataire_qualite IS 'Qualité du signataire.';
COMMENT ON COLUMN signataire.signature IS 'Signature du signataire.';
COMMENT ON COLUMN signataire.defaut IS 'Signataire par défaut.';
COMMENT ON COLUMN signataire.om_validite_debut IS 'Date de début de validité du signataire.';
COMMENT ON COLUMN signataire.om_validite_fin IS 'Date de fin de validité du signataire.';
-- Contraintes de la table signataire
ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_pkey PRIMARY KEY (signataire);
ALTER TABLE ONLY signataire
    ADD CONSTRAINT signataire_signataire_qualite_fkey FOREIGN KEY (signataire_qualite) REFERENCES signataire_qualite(signataire_qualite);
-- Séquence de la table signataire
CREATE SEQUENCE signataire_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE signataire_seq OWNED BY signataire.signataire;

-- Modification de la table courrier
ALTER TABLE ONLY courrier RENAME COLUMN date_creation TO om_date_creation;
ALTER TABLE ONLY courrier
    DROP COLUMN courrier_avis,
    DROP COLUMN cloture,
    DROP COLUMN date_butoir,
    ADD COLUMN code_barres character varying(12),
    ADD COLUMN date_envoi_mail_om_fichier_finalise_courrier date,
    ADD COLUMN date_envoi_mail_om_fichier_signe_courrier date,
    ADD COLUMN mailing boolean DEFAULT false,
    ADD COLUMN courrier_parent integer,
    ADD COLUMN courrier_joint integer,
    ADD COLUMN modele_edition integer,
    ADD COLUMN signataire integer;
-- Commentaires de la table courrier
COMMENT ON TABLE courrier IS 'Liste des courriers sortants (documents générés)';
COMMENT ON COLUMN courrier.courrier IS 'Identifiant unique.';
COMMENT ON COLUMN courrier.courrier_param IS 'Paramétrage du courrier.';
COMMENT ON COLUMN courrier.etablissement IS 'Lien vers l''établissement.';
COMMENT ON COLUMN courrier.dossier_coordination IS 'Lien vers le dossier de coordination.';
COMMENT ON COLUMN courrier.dossier_instruction IS 'Lien vers le dossier d''instruction.';
COMMENT ON COLUMN courrier.complement1_om_html IS 'Texte de complément 1 du courrier.';
COMMENT ON COLUMN courrier.complement2_om_html IS 'Texte de complément 2 du courrier.';
COMMENT ON COLUMN courrier.finalise IS 'Indique que le courrier est finalisé.';
COMMENT ON COLUMN courrier.om_fichier_finalise_courrier IS 'Document généré à la finalisation du courrier.';
COMMENT ON COLUMN courrier.om_fichier_signe_courrier IS 'Document généré signé du courrier.';
COMMENT ON COLUMN courrier.om_date_creation IS 'Date de création du courrier.';
COMMENT ON COLUMN courrier.date_finalisation IS 'Date de finalisation du courrier.';
COMMENT ON COLUMN courrier.date_envoi_signature IS 'Date d''envoi en signature du courrier.';
COMMENT ON COLUMN courrier.date_retour_signature IS 'Date de retour de signature du courrier.';
COMMENT ON COLUMN courrier.date_envoi_controle_legalite IS 'Date d''envoi en contrôle légalité du courrier.';
COMMENT ON COLUMN courrier.date_retour_controle_legalite IS 'Date de retour du contrôle légalité du courrier.';
COMMENT ON COLUMN courrier.date_envoi_rar IS 'Date d''envoi en retour AR aux destinataires du courrier.';
COMMENT ON COLUMN courrier.date_retour_rar IS 'Date de notification des destinataires du courrier.';
COMMENT ON COLUMN courrier.code_barres IS 'Numéro du code barres correspondant au courrier.';
COMMENT ON COLUMN courrier.date_envoi_mail_om_fichier_finalise_courrier IS 'Date d''envoi par mail du fichier finalisé du courrier.';
COMMENT ON COLUMN courrier.date_envoi_mail_om_fichier_signe_courrier IS 'Date d''envoi par mail du fichier signé du courrier.';
COMMENT ON COLUMN courrier.mailing IS 'Indique que c''est un mailing.';
COMMENT ON COLUMN courrier.courrier_parent IS 'Courrier mailing auquel est rattaché le courrier.';
COMMENT ON COLUMN courrier.courrier_joint IS 'Courrier joint du courrier.';
COMMENT ON COLUMN courrier.modele_edition IS 'Lien vers le modèle d''édition.';
COMMENT ON COLUMN courrier.signataire IS 'Lien vers le signataire du courrier.';
-- Contraintes de la table courrier
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_parent_fkey FOREIGN KEY (courrier_parent) REFERENCES courrier(courrier);
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_joint_fkey FOREIGN KEY (courrier_joint) REFERENCES courrier(courrier);
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_modele_edition_fkey FOREIGN KEY (modele_edition) REFERENCES modele_edition(modele_edition);
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_signataire_fkey FOREIGN KEY (signataire) REFERENCES signataire(signataire);

-- Création de la table lien_courrier_contact
CREATE TABLE lien_courrier_contact (
    lien_courrier_contact integer NOT NULL,
    courrier integer,
    contact integer
);
-- Commentaires de la table lien_courrier_contact
COMMENT ON TABLE lien_courrier_contact IS 'Lien entre les tables courrier et contact.';
COMMENT ON COLUMN lien_courrier_contact.lien_courrier_contact IS 'Identifiant unique.';
COMMENT ON COLUMN lien_courrier_contact.courrier IS 'Lien vers le courrier.';
COMMENT ON COLUMN lien_courrier_contact.contact IS 'Lien vers le contact.';
-- Contraintes de la table lien_courrier_contact
ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_pkey PRIMARY KEY (lien_courrier_contact);
ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_courrier_fkey FOREIGN KEY (courrier) REFERENCES courrier(courrier);
ALTER TABLE ONLY lien_courrier_contact
    ADD CONSTRAINT lien_courrier_contact_contact_fkey FOREIGN KEY (contact) REFERENCES contact(contact);
-- Séquence de la table lien_courrier_contact
CREATE SEQUENCE lien_courrier_contact_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_courrier_contact_seq OWNED BY lien_courrier_contact.lien_courrier_contact;

-- Création de la table courrier_texte_type
CREATE TABLE courrier_texte_type (
    courrier_texte_type integer NOT NULL,
    libelle character varying(60),
    courrier_type integer,
    contenu text,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires de la table courrier_texte_type
COMMENT ON TABLE courrier_texte_type IS 'Liste des textes types utilisable sur les courriers sortants (documents générés).';
COMMENT ON COLUMN courrier_texte_type.courrier_texte_type IS 'Identifiant unique.';
COMMENT ON COLUMN courrier_texte_type.libelle IS 'Libellé du texte type.';
COMMENT ON COLUMN courrier_texte_type.courrier_type IS 'Type du courrier sur lequel le texte type est applicable.';
COMMENT ON COLUMN courrier_texte_type.contenu IS 'Contenu du texte type.';
COMMENT ON COLUMN courrier_texte_type.om_validite_debut IS 'Date de début de validité du texte type.';
COMMENT ON COLUMN courrier_texte_type.om_validite_fin IS 'Date de fin de validité du texte type.';
-- Contraintes de la table courrier_texte_type
ALTER TABLE ONLY courrier_texte_type
    ADD CONSTRAINT courrier_texte_type_pkey PRIMARY KEY (courrier_texte_type);
ALTER TABLE ONLY courrier_texte_type
    ADD CONSTRAINT courrier_texte_type_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);
-- Séquence de la table courrier_texte_type
CREATE SEQUENCE courrier_texte_type_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE courrier_texte_type_seq OWNED BY courrier_texte_type.courrier_texte_type;

--
-- END / Suivi des pièces
--

-- -- Ajout des droits sur une analyse aux profils cadre, secrétaire et technicien
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'CADRE ACC')
--     );
-- --
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
--     );
-- --
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'dossier_instruction_analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'dossier_instruction_analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'SECRETAIRE ACC')
--     );
-- --
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN SI')
--     );
-- INSERT INTO om_droit (om_droit, libelle, om_profil)
-- SELECT nextval('om_droit_seq'),'analyses',(SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
-- WHERE
--     NOT EXISTS (
--         SELECT om_droit FROM om_droit WHERE libelle = 'analyses' AND om_profil = (SELECT om_profil FROM om_profil WHERE libelle = 'TECHNICIEN ACC')
--     );

-- création de la table prescription
CREATE TABLE prescription (
    prescription integer NOT NULL,
    analyses integer NOT NULL,
    ordre integer NOT NULL,
    prescription_reglementaire integer NULL,
    pr_tete_de_chapitre1 character varying(250) NULL,
    pr_tete_de_chapitre2 character varying(250) NULL,
    pr_libelle character varying(100) NULL,
    pr_description_om_html text NULL,
    pr_defavorable boolean NULL,
    ps_description_om_html text NOT NULL
);
COMMENT ON TABLE prescription IS 'Prescriptions des analyses';
COMMENT ON COLUMN prescription.prescription IS 'Identifiant unique';
COMMENT ON COLUMN prescription.analyses IS 'Analyse de la prescription';
COMMENT ON COLUMN prescription.prescription_reglementaire IS 'Éventuelle prescription réglementaire.';
COMMENT ON COLUMN prescription.pr_tete_de_chapitre1 IS 'Valeur de la tête de chapitre n°1';
COMMENT ON COLUMN prescription.pr_tete_de_chapitre2 IS 'Valeur de la tête de chapitre n°2';
COMMENT ON COLUMN prescription.pr_libelle IS 'Libellé de la prescription réglementaire';
COMMENT ON COLUMN prescription.pr_description_om_html IS 'Description de la prescription réglementaire';
COMMENT ON COLUMN prescription.pr_defavorable IS 'Donne lieu à un défavorable ?';
COMMENT ON COLUMN prescription.ps_description_om_html IS 'Description de la prescription spécifique';
COMMENT ON COLUMN prescription.ordre IS 'Ordre de la prescription dans la vue de l''analyse';
ALTER TABLE ONLY prescription
    ADD CONSTRAINT prescription_pkey PRIMARY KEY (prescription),
    ADD CONSTRAINT prescription_analyses_fkey FOREIGN KEY (analyses) REFERENCES analyses(analyses),
    ADD CONSTRAINT prescription_prescription_reglementaire_fkey FOREIGN KEY (prescription_reglementaire) REFERENCES prescription_reglementaire(prescription_reglementaire);
CREATE SEQUENCE prescription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
    OWNED BY prescription.prescription;

-- suppression de la table de liaison obsolète
DROP TABLE "lien_prescription_specifique_analyses";

-- basculement en champ texte riche des descriptions des prescriptions
ALTER TABLE prescription_reglementaire RENAME description TO description_om_html;
ALTER TABLE prescription_specifique RENAME description TO description_om_html;

--Ajout des données de paramétrage des prescriptions réglementaires
INSERT INTO prescription_reglementaire (prescription_reglementaire, service, tete_de_chapitre1, tete_de_chapitre2, libelle, description_om_html, defavorable)
    VALUES (nextval('prescription_reglementaire_seq'),
        (SELECT service from service where code = 'ACC'),
        'T1',
        'T2',
        'ACC - PR1',
        '<p>Description PR1 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        'f'
    );
INSERT INTO prescription_reglementaire (prescription_reglementaire, service, tete_de_chapitre1, tete_de_chapitre2, libelle, description_om_html, defavorable)
    VALUES (nextval('prescription_reglementaire_seq'),
        (SELECT service from service where code = 'ACC'),
        'T1',
        'T2',
        'ACC - PR2',
        '<p>Description PR2 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        't'
    );
INSERT INTO prescription_reglementaire (prescription_reglementaire, service, tete_de_chapitre1, tete_de_chapitre2, libelle, description_om_html, defavorable)
    VALUES (nextval('prescription_reglementaire_seq'),
        (SELECT service from service where code = 'SI'),
        'T1',
        'T2',
        'SI - PR3',
        '<p>Description PR3 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        'f'
    );
INSERT INTO prescription_reglementaire (prescription_reglementaire, service, tete_de_chapitre1, tete_de_chapitre2, libelle, description_om_html, defavorable)
    VALUES (nextval('prescription_reglementaire_seq'),
        (SELECT service from service where code = 'SI'),
        'T1',
        'T2',
        'SI - PR4',
        '<p>Description PR4 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        't'
    );

--Ajout des données de paramétrage des prescriptions spécifiques
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC - PS1 / PR1',
        '<p>Description PS1 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC - PS2 / PR1',
        '<p>Description PS2 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR1')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC - PS3 / PR2',
        '<p>Description PS3 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'ACC'),
        'ACC - PS4 / PR2',
        '<p>Description PS4 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'ACC - PR2')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'SI'),
        'SI - PS5 / PR3',
        '<p>Description PS5 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'SI'),
        'SI - PS6 / PR3',
        '<p>Description PS6 : <span style=''font-weight: bold;''>favorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'SI'),
        'SI - PS7 / PR4',
        '<p>Description PS7 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4')
    );
INSERT INTO prescription_specifique (prescription_specifique, service, libelle, description_om_html, prescription_reglementaire)
    VALUES (nextval('prescription_specifique_seq'),
        (SELECT service from service where code = 'SI'),
        'SI - PS8 / PR4',
        '<p>Description PS8 : <span style=''font-weight: bold;''>défavorable</span>.</p>',
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4')
    );

--Ajout de prescriptions spécifiques à une analyse du jeu de données
INSERT INTO prescription (prescription, analyses, ordre, prescription_reglementaire, pr_tete_de_chapitre1, pr_tete_de_chapitre2, pr_libelle, pr_description_om_html, pr_defavorable, ps_description_om_html)
    VALUES (nextval('prescription_seq'),
        1,
        1,
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR3'),
        'T1',
        'T2',
        'SI - PR3',
        '<p>PR3 : favorable.</p>',
        'f',
        '<p>PR : <span style=''font-weight: bold;''>avec</span> (PR3).</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        SI - PS5 / PR3
        </li></ul>
        <p>Description PS5 : <span style=''font-weight: bold;''>favorable</span>.</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        SI - PS6 / PR3
        </li></ul>
        <p>Description PS6 : <span style=''font-weight: bold;''>favorable</span>.</p>'
    );
INSERT INTO prescription (prescription, analyses, ordre, prescription_reglementaire, pr_tete_de_chapitre1, pr_tete_de_chapitre2, pr_libelle, pr_description_om_html, pr_defavorable, ps_description_om_html)
    VALUES (nextval('prescription_seq'),
        1,
        2,
        null,
        null,
        null,
        null,
        null,
        null,
        '<p>PR : <span style=''font-weight: bold;''>sans</span>.</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        PS : <span style=''font-weight: bold;''>favorable</span>.
        </li></ul>'
    );
INSERT INTO prescription (prescription, analyses, ordre, prescription_reglementaire, pr_tete_de_chapitre1, pr_tete_de_chapitre2, pr_libelle, pr_description_om_html, pr_defavorable, ps_description_om_html)
    VALUES (nextval('prescription_seq'),
        1,
        3,
        (SELECT prescription_reglementaire FROM prescription_reglementaire WHERE libelle = 'SI - PR4'),
        'T1',
        'T2',
        'SI - PR4',
        '<p>PR4 : défavorable.</p>',
        't',
        '<p>PR : <span style=''font-weight: bold;''>avec</span> (PR4).</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        SI - PS7 / PR4
        </li></ul>
        <p>Description PS7 : <span style=''font-weight: bold;''>défavorable</span>.</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        SI - PS8 / PR4
        </li></ul>
        <p>Description PS8 : <span style=''font-weight: bold;''>défavorable</span>.</p>'
    );
INSERT INTO prescription (prescription, analyses, ordre, prescription_reglementaire, pr_tete_de_chapitre1, pr_tete_de_chapitre2, pr_libelle, pr_description_om_html, pr_defavorable, ps_description_om_html)
    VALUES (nextval('prescription_seq'),
        1,
        4,
        null,
        null,
        null,
        null,
        null,
        null,
        '<p>PR : <span style=''font-weight: bold;''>sans</span>.</p>
        <ul><li style=''list-style-type: square; margin-left: 10px;''>
        PS : <span style=''font-weight: bold;''>défavorable</span>.
        </li></ul>'
    );

-- Ajout du champ courrier_convocation_exploitants à la table visite
ALTER TABLE visite ADD COLUMN courrier_convocation_exploitants integer;
ALTER TABLE ONLY visite
    ADD CONSTRAINT courrier_visite_fkey FOREIGN KEY (courrier_convocation_exploitants) REFERENCES courrier(courrier);

-- Lien unique réglementation applicable / type d'établissement
ALTER TABLE lien_reglementation_applicable_etablissement_type
ADD CONSTRAINT unique_ra_et UNIQUE (reglementation_applicable, etablissement_type);

-- Lien unique réglementation applicable / catégorie d'établissement
ALTER TABLE lien_reglementation_applicable_etablissement_categorie
ADD CONSTRAINT unique_ra_ec UNIQUE (reglementation_applicable, etablissement_categorie);

-- Mise en concordance des noms dans champs dans la table données techniques et 
-- dans la table établissement
ALTER TABLE etablissement RENAME COLUMN descriptif_secu_om_html TO si_descriptif_om_html;
ALTER TABLE etablissement RENAME COLUMN locaux_sommeil TO si_locaux_sommeil;
ALTER TABLE etablissement RENAME COLUMN effectif_public TO si_effectif_public;
ALTER TABLE etablissement RENAME COLUMN effectif_personnel TO si_effectif_personnel;
ALTER TABLE etablissement RENAME COLUMN type_alarme TO si_type_alarme;
ALTER TABLE etablissement RENAME COLUMN acc_accessible_mental TO acc_handicap_mental;
ALTER TABLE etablissement RENAME COLUMN acc_accessible_auditif TO acc_handicap_auditif;
ALTER TABLE etablissement RENAME COLUMN acc_nb_stationnements_amenages TO acc_places_stationnement_amenagees;
ALTER TABLE etablissement RENAME COLUMN acc_accessible_physique TO acc_handicap_physique;
ALTER TABLE etablissement RENAME COLUMN acc_accessible_visuel TO acc_handicap_visuel;
ALTER TABLE etablissement RENAME COLUMN acc_nb_chambres_amenagees TO acc_chambres_amenagees;
ALTER TABLE etablissement RENAME COLUMN acc_nb_public_assis TO acc_places_assises_public;
ALTER TABLE etablissement RENAME COLUMN acc_description_om_html TO acc_descriptif_om_html;
ALTER TABLE donnee_technique RENAME COLUMN acc_ascenceur TO acc_ascenseur;

--
-- Ajout de contraintes uniques sur le champ libelle des table dossier_coordination
-- et dossier_instruction
--
ALTER TABLE dossier_coordination
ADD CONSTRAINT dossier_coordination_libelle_unique UNIQUE (libelle);

ALTER TABLE dossier_instruction
ADD CONSTRAINT dossier_instruction_libelle_unique UNIQUE (libelle);

--
-- Mise à jour des données de la table dossier_coordination_type
--
UPDATE dossier_coordination_type SET (code, libelle)=('VR', 'Visite de réception') 
WHERE dossier_coordination_type = 4;

UPDATE dossier_coordination_type SET (code, libelle)=('PC', 'Permis de construire')
WHERE dossier_coordination_type = 1;

UPDATE dossier_coordination_type SET (code, libelle)= ('AT', 'Autorisation de Travaux')
WHERE dossier_coordination_type = 3;

UPDATE dossier_coordination_type SET (code, libelle)=('VCS', 'Visite de contrôle SI')
WHERE dossier_coordination_type = 5;

UPDATE dossier_coordination_type SET (code, libelle)=('VCA', 'Visite de contrôle ACC')
WHERE dossier_coordination_type = 6;

UPDATE dossier_coordination_type SET (code, libelle)=('VPS', 'Visite périodique SI')
WHERE dossier_coordination_type = 7;

--
-- Mise à jour des données de test
--
UPDATE dossier_coordination SET libelle='PC-PLAN-000003' WHERE dossier_coordination=3; 
UPDATE dossier_coordination SET libelle='PC-PLAN-000001' WHERE dossier_coordination=1; 
UPDATE dossier_coordination SET libelle='DPS-PLAN-000002' WHERE dossier_coordination=2; 
UPDATE dossier_coordination SET libelle='VPS-VISIT-000004' WHERE dossier_coordination=4; 
UPDATE dossier_coordination SET libelle='VCA-VISIT-000005' WHERE dossier_coordination=5; 
UPDATE dossier_coordination SET libelle='VCS-VISIT-000006' WHERE dossier_coordination=6; 
UPDATE dossier_coordination SET libelle='VR-VISIT-000007' WHERE dossier_coordination=7; 
UPDATE dossier_coordination SET libelle='AT-PLAN-000008' WHERE dossier_coordination=8; 

UPDATE dossier_instruction SET libelle='VCS-VISIT-000006-ACC' WHERE dossier_instruction=6; 
UPDATE dossier_instruction SET libelle='VPS-VISIT-000004-ACC' WHERE dossier_instruction=7; 
UPDATE dossier_instruction SET libelle='DPS-PLAN-000002-ACC' WHERE dossier_instruction=9; 
UPDATE dossier_instruction SET libelle='PC-PLAN-000001-ACC' WHERE dossier_instruction=10; 
UPDATE dossier_instruction SET libelle='VR-VISIT-000007-SI' WHERE dossier_instruction=4; 
UPDATE dossier_instruction SET libelle='AT-PLAN-000008-SI' WHERE dossier_instruction=8; 
UPDATE dossier_instruction SET libelle='VCA-VISIT-000005-SI' WHERE dossier_instruction=5; 
UPDATE dossier_instruction SET libelle='PC-PLAN-000003-SI' WHERE dossier_instruction=3; 

--
-- START / contact
--

-- Suppression du champ adresse_arrondissement
ALTER TABLE ONLY contact DROP CONSTRAINT contact_adresse_arrondissement_fkey;
ALTER TABLE ONLY contact DROP COLUMN adresse_arrondissement;

-- Change le type du champ adresse_voie
ALTER TABLE ONLY contact DROP CONSTRAINT contact_adresse_voie_fkey;
ALTER TABLE ONLY contact ALTER COLUMN adresse_voie TYPE character varying(255);

--
-- END / contact
--