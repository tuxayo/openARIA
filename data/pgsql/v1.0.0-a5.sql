--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a5
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
ALTER TABLE etablissement ALTER COLUMN code SET NOT NULL;
--
alter table etablissement drop column adresse_complement2;
alter table contact drop column adresse_complement2;
alter table etablissement drop column pays;
alter table etablissement rename column consignes_om_html to si_consignes_om_html;
--
alter table etablissement rename column commission_competente_secu_visite to si_autorite_competente_visite;
alter table etablissement rename column commission_competente_secu_plan to si_autorite_competente_plan;
--
alter table etablissement DROP CONSTRAINT etablissement_commission_competente_secu_plan;
alter table etablissement DROP CONSTRAINT etablissement_commission_competente_secu_visite;
--
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_autorite_competente_plan_fkey FOREIGN KEY (si_autorite_competente_plan) REFERENCES autorite_competente(autorite_competente);
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_autorite_competente_visite_fkey FOREIGN KEY (si_autorite_competente_visite) REFERENCES autorite_competente(autorite_competente);
drop table commission_competente_secu;
drop table commission_competente_acc;
--
alter table etablissement rename column dernier_plan_avis_secu to si_dernier_plan_avis;
alter table etablissement alter column si_dernier_plan_avis type integer USING NULL;
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_dernier_plan_avis_fkey 
    FOREIGN KEY (si_dernier_plan_avis) REFERENCES reunion_avis(reunion_avis);
--
alter table etablissement rename column acc_date_derniere_cca to acc_dernier_plan_date;
alter table etablissement rename column acc_avis_derniere_cca to acc_dernier_plan_avis;
alter table etablissement alter column acc_dernier_plan_avis type integer USING NULL;
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_acc_dernier_plan_avis_fkey 
    FOREIGN KEY (acc_dernier_plan_avis) REFERENCES reunion_avis(reunion_avis);
--
alter table etablissement DROP CONSTRAINT etablissement_derniere_visite_secu_avis_avis_visite_fkey;
alter table etablissement rename column derniere_visite_secu_avis to si_derniere_visite_avis;
alter table etablissement alter column si_derniere_visite_avis type integer USING NULL;
ALTER TABLE ONLY etablissement  
    ADD CONSTRAINT etablissement_si_derniere_visite_avis_fkey 
    FOREIGN KEY (si_derniere_visite_avis) REFERENCES reunion_avis(reunion_avis);
alter table etablissement rename column derniere_visite_secu_date to si_derniere_visite_date;
alter table etablissement rename column derniere_visite_secu_technicien to si_derniere_visite_technicien;
--
alter table etablissement DROP CONSTRAINT etablissement_derniere_visite_acc_avis_avis_visite_fkey;
alter table etablissement rename column derniere_visite_acc_avis to acc_derniere_visite_avis;
alter table etablissement alter column acc_derniere_visite_avis type integer USING NULL;
ALTER TABLE ONLY etablissement  
    ADD CONSTRAINT etablissement_acc_derniere_visite_avis_fkey
    FOREIGN KEY (acc_derniere_visite_avis) REFERENCES reunion_avis(reunion_avis);
alter table etablissement rename column derniere_visite_acc_date to acc_derniere_visite_date;
alter table etablissement rename column derniere_visite_acc_technicien to acc_derniere_visite_technicien;
--
drop table visite_avis;
--
alter table etablissement rename column prochaine_visite_secu_date to si_prochaine_visite_date;
alter table etablissement rename column prochaine_visite_secu_type to si_prochaine_visite_type;
--
alter table etablissement rename column derniere_visite_periodique_secu_date to si_derniere_visite_periodique_date;
alter table etablissement rename column visite_duree to si_visite_duree;
alter table etablissement rename column date_prev_visite_periodique to si_prochaine_visite_periodique_date_previsionnelle;
alter table etablissement rename periodicite_visites to si_periodicite_visites;
--
alter table dossier_instruction drop column date_prev_visite_periodique;
--
ALTER TABLE ONLY etablissement  
    ADD CONSTRAINT etablissement_acc_derniere_visite_technicien_fkey
    FOREIGN KEY (acc_derniere_visite_technicien) REFERENCES acteur(acteur);
ALTER TABLE ONLY etablissement  
    ADD CONSTRAINT etablissement_si_derniere_visite_technicien_fkey
    FOREIGN KEY (si_derniere_visite_technicien) REFERENCES acteur(acteur);
--
alter table etablissement DROP CONSTRAINT etablissement_prochaine_visite_secu_type_visite_type_fkey;
drop table visite_type;
ALTER TABLE ONLY etablissement
    ADD CONSTRAINT etablissement_si_prochaine_visite_type_fkey
    FOREIGN KEY (si_prochaine_visite_type) REFERENCES analyses_type(analyses_type);
--

-- Modification du commentaire des champs types d'analyse dans la table type de DC
COMMENT ON COLUMN dossier_coordination_type.analyses_type_si
    IS 'Type d''analyse sécurité par défaut';
COMMENT ON COLUMN dossier_coordination_type.analyses_type_acc
    IS 'Type d''analyse accessibilité par défaut';

-- Correction de l'ID des services des types d'analyse
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'ACC')
    WHERE code = 'VR-ACC';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'ACC')
    WHERE code = 'VR-VP-ACC';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'ACC')
    WHERE code = 'VP-ACC';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'ACC')
    WHERE code = 'VC-ACC';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'ACC')
    WHERE code = 'EP-ACC';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI')
    WHERE code = 'VR-SI';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI')
    WHERE code = 'VR-VP-SI';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI')
    WHERE code = 'VP-SI';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI')
    WHERE code = 'VC-SI';
UPDATE analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI')
    WHERE code = 'EP-SI';

-- Ajout du filtre par service dans la liaison type de DC / type d'analyse
ALTER TABLE lien_dossier_coordination_type_analyses_type
ADD service integer NULL;
COMMENT ON COLUMN lien_dossier_coordination_type_analyses_type.service IS 'Service';
UPDATE lien_dossier_coordination_type_analyses_type
    SET service = (SELECT service FROM service WHERE code = 'SI');
ALTER TABLE lien_dossier_coordination_type_analyses_type
    ALTER service SET NOT NULL;

-- Ajout de la colonne code dans la table courrier_param
ALTER TABLE courrier_param ADD COLUMN code character varying(5);
ALTER TABLE courrier_param ADD CONSTRAINT courrier_param_code_key UNIQUE (code);

-- Ajout de la colonne code dans la table modele_edition
ALTER TABLE modele_edition ADD COLUMN code character varying(15);
ALTER TABLE modele_edition ADD CONSTRAINT modele_edition_code_key UNIQUE (code);

-- Modification du champ contenu de la table courrier_texte_type
ALTER TABLE ONLY courrier_texte_type RENAME COLUMN contenu TO contenu_om_html;
