--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a16
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

-- Renommage des champs description des prescriptions réglementaire et spécifiques
-- en raison de leur type texte riche qui peut occasionner un conflit avec tinyMCE.
ALTER TABLE prescription_reglementaire
    RENAME description_om_html TO description_pr_om_html;
ALTER TABLE prescription_specifique
    RENAME description_om_html TO description_ps_om_html;

-- Dates de validités sur la table acteur
ALTER TABLE acteur
ADD COLUMN om_validite_debut date;
ALTER TABLE acteur
ADD COLUMN om_validite_fin date;
COMMENT ON COLUMN piece_statut.om_validite_debut IS 'Date de début de validité.';
COMMENT ON COLUMN piece_statut.om_validite_fin IS 'Date de fin de validité.';

-- Ajout d'une séquence manquante
-- CREATE SEQUENCE contact_civilite_seq
--     START WITH 1
--     INCREMENT BY 1
--     NO MINVALUE
--     NO MAXVALUE
--     CACHE 1;
-- ALTER SEQUENCE contact_civilite_seq OWNED BY contact_civilite.contact_civilite;

-- Ajout du profil par défaut 'PROFIL NON CONFIGURÉ'
-- DELETE FROM om_dashboard WHERE om_profil=1 ;
-- INSERT INTO om_profil (om_profil, libelle, hierarchie) VALUES (8, 'ADMINISTRATEUR', 0);
-- UPDATE om_utilisateur SET om_profil=8 WHERE om_profil=1;
-- UPDATE om_profil SET libelle = 'PROFIL NON CONFIGURÉ' WHERE om_profil=1;
-- INSERT INTO om_widget (om_widget, libelle, lien, texte, type) VALUES (20, 'Profil non configuré', '', 'Votre profil ne peut accéder à aucune fonction du logiciel. Veuillez contacter l''administrateur pour que vos permissions soient configurées.', 'web');
-- INSERT INTO om_dashboard (om_dashboard, om_profil, bloc, "position", om_widget) VALUES (nextval('om_dashboard_seq'), 1, 'C1', 1, 20);

--

----
-- BEGIN / Homogénéité du type des champs complément d'avis
----
ALTER TABLE analyses
    ALTER avis_complement TYPE character varying(250);
ALTER TABLE dossier_instruction_reunion
    ALTER proposition_avis_complement TYPE character varying(250),
    ALTER avis_complement TYPE character varying(250);
----
-- END / Homogénéité du type des champs complément d'avis
----


----
-- BEGIN / Éditions des réunions
----
-- STEP 1/4 - Sous-états
INSERT INTO om_sousetat (om_sousetat, om_collectivite, id, libelle, actif, titre, titrehauteur, titrefont, titreattribut, titretaille, titrebordure, titrealign, titrefond, titrefondcouleur, titretextecouleur, intervalle_debut, intervalle_fin, entete_flag, entete_fond, entete_orientation, entete_hauteur, entetecolone_bordure, entetecolone_align, entete_fondcouleur, entete_textecouleur, tableau_largeur, tableau_bordure, tableau_fontaille, bordure_couleur, se_fond1, se_fond2, cellule_fond, cellule_hauteur, cellule_largeur, cellule_bordure_un, cellule_bordure, cellule_align, cellule_fond_total, cellule_fontaille_total, cellule_hauteur_total, cellule_fondcouleur_total, cellule_bordure_total, cellule_align_total, cellule_fond_moyenne, cellule_fontaille_moyenne, cellule_hauteur_moyenne, cellule_fondcouleur_moyenne, cellule_bordure_moyenne, cellule_align_moyenne, cellule_fond_nbr, cellule_fontaille_nbr, cellule_hauteur_nbr, cellule_fondcouleur_nbr, cellule_bordure_nbr, cellule_align_nbr, cellule_numerique, cellule_total, cellule_moyenne, cellule_compteur, om_sql)
VALUES (nextval('om_sousetat_seq'), 1,  'odj_cca',  'Ordre du jour CCA',    't',    ' ',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_visites', 'Ordre du jour CCS - Visites',  't',    'Visites',  10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''visites''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj',  'Ordre du jour',    't',    ' ',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_cca',  'Compte-rendu général CCA', 't',    ' ', 10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg',  'Compte-rendu général', 't',    ' ', 10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_plans',   'Ordre du jour CCS - Plans',    't',    'Plans',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''plans''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_enjeux',  'Ordre du jour CCS - Enjeux',   't',    'Enjeux',   10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''enjeux''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_apdif',   'Ordre du jour CCS - Avis différés',    't',    'Avis différés',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_dif''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_apmed',   'Ordre du jour CCS - Mises en demeure', 't',    'Mises en demeure', 10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_med''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'), 1,  'odj_ccs_plen_apscds',  'Ordre du jour CCS - Sous Commission Départementale',   't',    'Sous Commission Départementale',   10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|C|C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|60|60',   'LTBR|LTBR|LTBR|LTBR|LTBR', 'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR|LTBR', 'C|L|L|L|L',    '999|999|999|999|999',  '0|0|0|0|0',    '0|0|0|0|0',    '0|0|0|0|0',    'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Proposition
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis2.libelle,
        -- Ligne 2
        dossier_instruction_reunion.proposition_avis_complement
    ) as proposition,
    ---- Avis (colonne vide)
    '' '' as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_scds''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_apmed',   'Compte-rendu général CCS - Mises en demeure',  't',    'Mises en demeure', 10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_med''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_enjeux',  'Compte-rendu général CCS - Enjeux',    't',    'Enjeux',   10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''enjeux''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_apdif',   'Compte-rendu général CCS - Avis différés', 't',    'Avis différés',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_dif''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_plans',   'Compte-rendu général CCS - Plans', 't',    'Plans',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''plans''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_visites', 'Compte-rendu général CCS - Visites',   't',    'Visites',  10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''visites''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'crg_ccs_plen_apscds',  'Compte-rendu général CCS - Sous Commission Départementale',    't',    'Sous Commission Départementale',   10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '15|70|70|120', 'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    ---- Numéro
    dossier_instruction_reunion.ordre as n°,
    ---- Établissement
    CONCAT_WS(''
'',
        -- Ligne 1
        CONCAT(etablissement.code, '' - '', etablissement.libelle, '' [ '', etablissement_categorie.libelle, '' / '', etablissement_type.libelle ,'' ]''),
        -- Ligne 2
        CONCAT_WS('' '', etablissement.adresse_numero, etablissement.adresse_numero2, voie.libelle),
        -- Ligne 3
        CONCAT_WS('' '', etablissement.adresse_cp, etablissement.adresse_ville)
    ) as établissement,
    ---- Dossier
    CONCAT_WS(''
'',
        -- Ligne 1
        dossier_coordination_type.libelle,
        -- Ligne 2
        CONCAT(dossier_instruction.libelle, '' / '', dossier_coordination.dossier_instruction_ads),
        -- Ligne 3
        CONCAT(''Demande du  : '', to_char(dossier_coordination.date_demande, ''DD/MM/YYYY'')),
        -- Ligne 4
        CONCAT(''=> '', acteur.acronyme)
    ) as dossier,
    ---- Avis (colonne vide)
    CONCAT_WS(''
'',
        -- Ligne 1
        reunion_avis0.libelle,
        -- Ligne 2
        dossier_instruction_reunion.avis_complement
    ) as avis
FROM
    &DB_PREFIXEdossier_instruction_reunion
        LEFT JOIN &DB_PREFIXEdossier_instruction
            ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction
        LEFT JOIN &DB_PREFIXEdossier_coordination
            ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
        LEFT JOIN &DB_PREFIXEdossier_coordination_type
            ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
        LEFT JOIN &DB_PREFIXEetablissement
            ON dossier_coordination.etablissement = etablissement.etablissement
        LEFT JOIN &DB_PREFIXEvoie
            ON etablissement.adresse_voie = voie.voie
        LEFT JOIN &DB_PREFIXEetablissement_type
            ON etablissement.etablissement_type = etablissement_type.etablissement_type
        LEFT JOIN &DB_PREFIXEetablissement_categorie
            ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
        LEFT JOIN &DB_PREFIXEacteur
            ON dossier_instruction.technicien = acteur.acteur
        LEFT JOIN &DB_PREFIXEreunion_categorie
            ON dossier_instruction_reunion.reunion_type_categorie=reunion_categorie.reunion_categorie
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis0 
            ON dossier_instruction_reunion.avis=reunion_avis0.reunion_avis 
        LEFT JOIN &DB_PREFIXEreunion_avis as reunion_avis2 
            ON dossier_instruction_reunion.proposition_avis=reunion_avis2.reunion_avis
WHERE
    dossier_instruction_reunion.reunion = &idx
    AND lower(reunion_categorie.code) = ''ap_scds''
ORDER BY
    reunion_categorie.ordre, dossier_instruction_reunion.ordre'),
(nextval('om_sousetat_seq'),    1,  'fp',   'Feuille de présence',  't',    ' ',  10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR',   'C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '80|70|40', 'LTBR|LTBR|LTBR',   'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '999|999|999',  '0|0|0',    '0|0|0',    '0|0|0',    'SELECT
    ---- Organisme
    instance.libelle as organisme,
    ---- Fonction et nom
    CONCAT_WS(''
'',
        -- Ligne 1 : fonction
        membre.description,
        -- Ligne 2 : nom
        membre.membre
    ) as nom,
    ---- Signature
    ''

'' as signature
FROM
    &DB_PREFIXEreunion
LEFT JOIN &DB_PREFIXElien_reunion_r_instance_r_i_membre as lien
    ON lien.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEreunion_instance as instance
    ON lien.reunion_instance = instance.reunion_instance
LEFT JOIN &DB_PREFIXEreunion_instance_membre as membre
    ON lien.reunion_instance_membre = membre.reunion_instance_membre
WHERE
    reunion.reunion = &idx
ORDER BY
    instance.libelle, membre.membre'),
(nextval('om_sousetat_seq'),    1,  'cra',  'Compte-rendu d''avis', 't',    ' ',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0',    15, 'LTBR|LTBR|LTBR',   'C|C|C',    '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '80|70|40', 'LTBR|LTBR|LTBR',   'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR',   'L|L|L',    '999|999|999',  '0|0|0',    '0|0|0',    '0|0|0',    'SELECT
    ---- Membres
    CONCAT_WS(''
'',
        -- Ligne 1 : Instance
        CONCAT_WS('' '', ''Représentant'', instance.libelle),
        -- Ligne 2 : Membre
        membre.membre
    ) as membres,
    ---- Avis et motivation 
    ''



'' as avis,
    ---- Visas
    ''



'' as visas
FROM
    &DB_PREFIXElien_reunion_r_instance_r_i_membre as lien
LEFT JOIN &DB_PREFIXEreunion
    ON lien.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEdossier_instruction_reunion as dossier
    ON dossier.reunion = reunion.reunion
LEFT JOIN &DB_PREFIXEreunion_type
    ON reunion.reunion_type = reunion_type.reunion_type
LEFT JOIN &DB_PREFIXEreunion_instance as instance
    ON lien.reunion_instance = instance.reunion_instance
LEFT JOIN &DB_PREFIXEreunion_instance_membre as membre
    ON lien.reunion_instance_membre = membre.reunion_instance_membre
WHERE
    dossier.dossier_instruction_reunion = &idx
    AND lien.reunion_instance != reunion_type.president
ORDER BY
    instance.libelle');
;
-- STEP 2/4 - Lettres-types
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte)
VALUES (nextval('om_lettretype_seq'),    1,  'reunion_ordre_du_jour_cca',    'Réunion - Ordre du jour CCA',  't',    'L',    'A4',   NULL,   10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',  10, 16, 0,  10, '0',    '<p><span id=''odj_cca'' class=''mce_sousetat''>Ordre du jour CCA</span></p>
<p> </p>',  4,  10, 10, 10, 10, 'helvetica',    '0-0-0'),
(nextval('om_lettretype_seq'),    1,  'reunion_ordre_du_jour_ccs',    'Réunion - Ordre du jour CCS',  't',    'L',    'A4',   NULL,   10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',  10, 16, 0,  10, '0',    '<p><span id=''odj_ccs_plen_plans'' class=''mce_sousetat''>Ordre du jour CCS - Plans</span></p>
<p> </p>
<p><span id=''odj_ccs_plen_visites'' class=''mce_sousetat''>Ordre du jour CCS - Visites</span></p>
<p> </p>
<p><span id=''odj_ccs_plen_enjeux'' class=''mce_sousetat''>Ordre du jour CCS - Enjeux</span></p>
<p> </p>
<p><span id=''odj_ccs_plen_apdif'' class=''mce_sousetat''>Ordre du jour CCS - Avis différés</span></p>
<p> </p>
<p><span id=''odj_ccs_plen_apmed'' class=''mce_sousetat''>Ordre du jour CCS - Mises en demeure</span></p>
<p> </p>
<p><span id=''odj_ccs_plen_apscds'' class=''mce_sousetat''>Ordre du jour CCS - Sous Commission Départementale</span></p>
<p> </p>',  4,  10, 10, 10, 10, 'helvetica',    '0-0-0'),
(nextval('om_lettretype_seq'),    1,  'reunion_compte_rendu_general_cca', 'Réunion - Compte-rendu général CCA',   't',    'L',    'A4',   NULL,   10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',  10, 16, 0,  10, '0',    '<p><span id=''crg_cca'' class=''mce_sousetat''>Compte-rendu général CCA</span></p>
<p> </p>',  4,  10, 10, 10, 10, 'helvetica',    '0-0-0'),
(nextval('om_lettretype_seq'),    1,  'reunion_compte_rendu_general_ccs', 'Réunion - Compte-rendu général CCS',   't',    'L',    'A4',   NULL,   10, 10, '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',  10, 16, 0,  10, '0',    '<p><span id=''crg_ccs_plen_plans'' class=''mce_sousetat''>Compte-rendu général CCS - Plans</span></p>
<p> </p>
<p><span id=''crg_ccs_plen_visites'' class=''mce_sousetat''>Compte-rendu général CCS - Visites</span></p>
<p> </p>
<p><span id=''crg_ccs_plen_enjeux'' class=''mce_sousetat''>Compte-rendu général CCS - Enjeux</span></p>
<p> </p>
<p><span id=''crg_ccs_plen_apdif'' class=''mce_sousetat''>Compte-rendu général CCS - Avis différés</span></p>
<p> </p>
<p><span id=''crg_ccs_plen_apmed'' class=''mce_sousetat''>Compte-rendu général CCS - Mises en demeure</span></p>
<p> </p>
<p><span id=''crg_ccs_plen_apscds'' class=''mce_sousetat''>Compte-rendu général CCS - Sous Commission Départementale</span></p>
<p> </p>',  4,  10, 10, 10, 10, 'helvetica',    '0-0-0');
UPDATE om_lettretype SET
om_collectivite = '1',
id = 'reunion_ordre_du_jour',
libelle = 'Réunion - Ordre du jour',
actif = 't',
orientation = 'L',
format = 'A4',
logo = NULL,
logoleft = '10',
logotop = '10',
titre_om_htmletat = '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>ORDRE DU JOUR DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',
titreleft = '10',
titretop = '16',
titrelargeur = '0',
titrehauteur = '10',
titrebordure = '0',
corps_om_htmletatex = '<p><span id=''odj'' class=''mce_sousetat''>Ordre du jour</span></p>
<p> </p>',
om_sql = '4',
margeleft = '10',
margetop = '10',
margeright = '10',
margebottom = '10',
se_font = 'helvetica',
se_couleurtexte = '0-0-0'
WHERE id = 'reunion_ordre_du_jour';
UPDATE om_lettretype SET
om_collectivite = '1',
id = 'reunion_compte_rendu_general',
libelle = 'Réunion - Compte-rendu général',
actif = 't',
orientation = 'L',
format = 'A4',
logo = NULL,
logoleft = '10',
logotop = '10',
titre_om_htmletat = '<table>
<tbody>
<tr>
<td>
<p>[reunion.lieu_salle]</p>
<p>[reunion.lieu_adresse_ligne1]</p>
<p>[reunion.lieu_adresse_ligne2]</p>
</td>
<td>
<p style=''text-align: right;''><span class=''mce_maj''>[reunion.libelle]</span></p>
<p style=''text-align: right;''> </p>
<p style=''text-align: right;''><span style=''font-weight: bold;''>COMPTE-RENDU GÉNÉRAL DE LA RÉUNION DU [reunion.date_reunion] À [reunion.heure_reunion]</span></p>
</td>
</tr>
</tbody>
</table>',
titreleft = '10',
titretop = '16',
titrelargeur = '0',
titrehauteur = '10',
titrebordure = '0',
corps_om_htmletatex = '<p><span id=''crg'' class=''mce_sousetat''>Compte-rendu général</span></p>
<p> </p>',
om_sql = '4',
margeleft = '10',
margetop = '10',
margeright = '10',
margebottom = '10',
se_font = 'helvetica',
se_couleurtexte = '0-0-0'
WHERE id = 'reunion_compte_rendu';
UPDATE om_lettretype SET
om_collectivite = '1',
id = 'reunion_feuille_de_presence',
libelle = 'Réunion - Feuille de présence',
actif = 't',
orientation = 'P',
format = 'A4',
logo = NULL,
logoleft = '10',
logotop = '10',
titre_om_htmletat = '<p style=''text-align: center;''><span style=''text-decoration: underline;''><em><span style=''font-weight: bold;''>FEUILLE DE PRÉSENCE</span></em></span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''>[reunion.libelle]</p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''>[reunion.date_reunion]</p>',
titreleft = '10',
titretop = '16',
titrelargeur = '0',
titrehauteur = '10',
titrebordure = '0',
corps_om_htmletatex = '<p><span id=''fp'' class=''mce_sousetat''>Feuille de présence</span></p>
<p> </p>',
om_sql = '4',
margeleft = '10',
margetop = '10',
margeright = '10',
margebottom = '10',
se_font = 'helvetica',
se_couleurtexte = '0-0-0'
WHERE id = 'reunion_feuille_de_presence';
UPDATE om_lettretype SET
om_collectivite = '1',
id = 'reunion_compte_rendu_avis',
libelle = 'Réunion - Compte rendu d''avis',
actif = 't',
orientation = 'P',
format = 'A4',
logo = NULL,
logoleft = '10',
logotop = '10',
titre_om_htmletat = '<p style=''text-align: right;''><span class=''mce_maj'' style=''font-weight: bold;''>[dossier_instruction_reunion.reunion_type]</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>COMPTE RENDU DE LA REUNION</span></p>',
titreleft = '10',
titretop = '16',
titrelargeur = '0',
titrehauteur = '10',
titrebordure = '0',
corps_om_htmletatex = '<p>Dossier : [dossier_instruction_reunion.dossier_instruction]</p>
<p>Établissement :</p>
<p>Raison sociale :</p>
<p> </p>
<p><span id=''cra'' class=''mce_sousetat''>Compte-rendu d''avis</span></p>
<p> </p>
<p> </p>
<table border=''1''>
<tbody>
<tr>
<td>
<p><span class=''mce_maj'' style=''font-weight: bold;''>AVIS DE [dossier_instruction_reunion.reunion_type]</span></p>
<p> </p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-weight: bold;''>[dossier_instruction_reunion.avis]</span></p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Ne change pas l''avis de visite</span></p>
</td>
</tr>
</tbody>
</table>
<p> </p>
<p style=''text-align: right;''>Le Président</p>',
om_sql = '5',
margeleft = '10',
margetop = '10',
margeright = '10',
margebottom = '10',
se_font = 'helvetica',
se_couleurtexte = '0-0-0'
WHERE id = 'reunion_compte_rendu_avis';
-- STEP 3/4 - Modèles d'édition
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) VALUES
(nextval('modele_edition_seq'),    'Réunion - Ordre du jour CCA',  '', (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_ordre_du_jour_cca'), NULL,   NULL,   NULL,   NULL,   8),
(nextval('modele_edition_seq'),    'Réunion - Ordre du jour CCS',  '', (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_ordre_du_jour_ccs'), NULL,   NULL,   NULL,   NULL,   8),
(nextval('modele_edition_seq'),    'Réunion - Compte-rendu général CCA',   '', (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_compte_rendu_general_cca'), NULL,   NULL,   NULL,   NULL,   7),
(nextval('modele_edition_seq'),    'Réunion - Compte-rendu général CCS',   '', (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_compte_rendu_general_ccs'), NULL,   NULL,   NULL,   NULL,   7);
UPDATE modele_edition SET
libelle = 'Réunion - Ordre du jour',
om_lettretype = (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_ordre_du_jour')
WHERE libelle = 'Réunion - Ordre du jour';
UPDATE modele_edition SET
libelle = 'Réunion - Compte-rendu général',
om_lettretype = (SELECT om_lettretype FROM om_lettretype WHERE id = 'reunion_compte_rendu_general')
WHERE libelle = 'Réunion - Compte rendu';
-- STEP 4/4 - Réunions-types
UPDATE reunion_type SET
modele_ordre_du_jour = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Ordre du jour CCA'),
modele_compte_rendu_global = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Compte-rendu général CCA')
WHERE code = 'CCA';
UPDATE reunion_type SET
modele_ordre_du_jour = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Ordre du jour CCS'),
modele_compte_rendu_global = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Compte-rendu général CCS')
WHERE code = 'CCS';
UPDATE reunion_type SET
modele_ordre_du_jour = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Ordre du jour'),
modele_compte_rendu_global = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Compte-rendu général')
WHERE code = 'SCDA';
UPDATE reunion_type SET
modele_ordre_du_jour = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Ordre du jour'),
modele_compte_rendu_global = (SELECT modele_edition FROM modele_edition WHERE libelle = 'Réunion - Compte-rendu général')
WHERE code = 'GTEP';
----
-- END / Éditions des réunions
----

--
-- START / Modèle d'édition
--

-- Champs obligatoire modele_edition
UPDATE modele_edition SET libelle = 'libellé' WHERE libelle IS NULL OR libelle = '';
UPDATE modele_edition SET courrier_type = 1 WHERE courrier_type IS NULL;
ALTER TABLE modele_edition
    ALTER COLUMN libelle SET NOT NULL,
    ALTER COLUMN courrier_type SET NOT NULL;

-- Champs obligatoire courrier_type
UPDATE courrier_type SET courrier_type_categorie = 1 WHERE courrier_type_categorie IS NULL;
ALTER TABLE courrier_type
    ALTER COLUMN code SET NOT NULL,
    ALTER COLUMN libelle SET NOT NULL,
    ALTER COLUMN courrier_type_categorie SET NOT NULL;

-- Champs obligatoire courrier_type_categorie
ALTER TABLE courrier_type_categorie
    ALTER COLUMN code SET NOT NULL,
    ALTER COLUMN libelle SET NOT NULL;

-- Ajout du champ objet
ALTER TABLE courrier_type_categorie
    ADD COLUMN objet character varying(255) NOT NULL DEFAULT 'object';
-- Supprime la valeur par défaut
ALTER TABLE courrier_type_categorie
    ALTER COLUMN objet DROP DEFAULT;

--
-- END / Modèle d'édition
--