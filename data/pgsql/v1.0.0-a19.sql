-- Mise en cohérence des champs des unités d'accessibilité
-- avec les champs des établissements et des analyses
ALTER TABLE etablissement_unite RENAME consignes_om_html TO acc_consignes_om_html;
ALTER TABLE etablissement_unite RENAME description_om_html TO acc_descriptif_om_html;
ALTER TABLE etablissement_unite RENAME accessible_physique TO acc_handicap_physique;
ALTER TABLE etablissement_unite RENAME accessible_auditif TO acc_handicap_auditif;
ALTER TABLE etablissement_unite RENAME accessible_visuel TO acc_handicap_visuel;
ALTER TABLE etablissement_unite RENAME accessible_mental TO acc_handicap_mental;
ALTER TABLE etablissement_unite RENAME nb_stationnements_amenages TO acc_places_stationnement_amenagees;
ALTER TABLE etablissement_unite RENAME ascenseur TO acc_ascenseur;
ALTER TABLE etablissement_unite RENAME elevateur TO acc_elevateur;
ALTER TABLE etablissement_unite RENAME boucle_magnetique TO acc_boucle_magnetique;
ALTER TABLE etablissement_unite RENAME sanitaire TO acc_sanitaire;
ALTER TABLE etablissement_unite RENAME nb_public_assis TO acc_places_assises_public;
ALTER TABLE etablissement_unite RENAME nb_chambres_amenagees TO acc_chambres_amenagees;
ALTER TABLE etablissement_unite RENAME douche TO acc_douche;
ALTER TABLE etablissement_unite RENAME derogation_scda TO acc_derogation_scda;

----
-- BEGIN / Remplacement du sous-état "essais réalisés" par un champ de fusion
-- dans les lettres-types du contexte analyse
----
-- modification du corps des lettres-types utilisant le sous-état
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'analyse_rapport_si';
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'analyse_compte_rendu_si';
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Effectif public : [dt_eff_pub]</p>
<p>Effectif personnel : [dt_eff_pers]</p>
<p>Type SSI : [dt_ssi]</p>
<p>Type alarme : [dt_alarme]</p>
<p>Conformité L16 : [dt_l16]</p>
<p>Alimentation de remplacement : [dt_alim]</p>
<p>Service sécurité : [dt_svc_sec]</p>
<p>Personnel de jour : [dt_pers_jour]</p>
<p>Personnel de nuit : [dt_pers_nuit]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions</span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites</span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_si';
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'analyse_rapport_acc';
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'analyse_compte_rendu_acc';
UPDATE om_lettretype SET
corps_om_htmletatex = '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p> </p>
<p><span style=''font-weight: bold;''>* Procès-verbal</span></p>
<p>Numéro : [pv_numero]</p>
<p>Date de rédaction : [pv_date_redaction]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Signataire du PV</span></p>
<p>Civilité : [signataire_civilite]</p>
<p>Nom : [signataire_nom]</p>
<p>Prénom : [signataire_prenom]</p>
<p>Qualité : [signataire_qualite]</p>
<p>Signature : [signataire_signature]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Demande de passage en réunion rattachée au PV</span></p>
<p>Date de passage souhaitée : [reunion_date]</p>
<p>Type de réunion : [reunion_type]</p>
<p>Catégorie du type de réunion : [reunion_categorie]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p>Libellé : [di_lib]</p>
<p>Technicien en charge : [di_tech]</p>
<p>Autorité compétente : [di_ac]</p>
<p>À qualifier : [di_qualif]</p>
<p>Incomplétude : [di_incomp]</p>
<p>Clôturé : [di_clos]</p>
<p>Prioritaire : [di_prio]</p>
<p>Pièces attendues : [di_piece]</p>
<p>Description détaillée : [di_desc]</p>
<p>Commentaires d''instruction : [di_notes]</p>
<p>Statut : [di_statut]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Dossier de coordination</span></p>
<p>Libellé : [dc_lib]</p>
<p>Type : [dc_type]</p>
<p>Date de la demande : [dc_date_dmnd]</p>
<p>Date butoir : [dc_date_butr]</p>
<p>Dossier d''autorisation ADS : [dc_da_ads]</p>
<p>Dossier d''instruction ADS : [dc_di_ads]</p>
<p>À qualifier : [dc_qualif]</p>
<p>Clôturé : [dc_clos]</p>
<p>Contraintes urbaines : [dc_ctr_urb]</p>
<p>Dossier de coordination parent : [dc_parent]</p>
<p>Description : [dc_desc]</p>
<p>Autorité de police en cours : [dc_ap]</p>
<p>Références cadastrales : [dc_ref_cad]</p>
<p> </p>
<p><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p>Numéro T : [etab_code]</p>
<p>Libellé : [etab_lib]</p>
<p>Type : [etab_type]</p>
<p>Catégorie : [etab_cat]</p>
<p>Siret : [etab_siret]</p>
<p>Téléphone : [etab_tel]</p>
<p>Fax : [etab_fax]</p>
<p>Année de construction : [etab_annee_const]</p>
<p>Nature : [etab_nat]</p>
<p>Statut juridique : [etab_stat_jur]</p>
<p>Tutelle administrative : [etab_tut_adm]</p>
<p>État : [etab_etat]</p>
<p>Adresse : [etab_adresse]</p>
<p>Date d''arrêté d''ouverture : [etab_date_ar_ouv]</p>
<p>Exploitant : [etab_exp]</p>
<p>Références cadastrales : [etab_ref_cad]</p>
<p> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse<br /></span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[anls_type]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[anls_obj]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[anls_desc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Classification de l''établissement</span></p>
<p>Type d''établissement : [dc_etab_type]</p>
<p>Type(s) secondaire(s) : [dc_etab_type_sec]</p>
<p>Catégorie d''établissement : [dc_etab_cat]</p>
<p>Établissement référentiel : [dc_etab_erp]</p>
<p>Locaux à sommeil : [dc_etab_loc_som]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Handicap mental : [dt_h_ment]</p>
<p>Handicap auditif : [dt_h_audtf]</p>
<p>Handicap physique : [dt_h_phsq]</p>
<p>Handicap visuel : [dt_h_visu]</p>
<p>Élévateur : [dt_elvtr]</p>
<p>Ascenseur : [dt_ascsr]</p>
<p>Douche : [dt_douche]</p>
<p>Boucle magnétique : [dt_bcl_mgnt]</p>
<p>Sanitaires : [dt_sanitaire]</p>
<p>Nb de places de stationnement aménagées : [dt_plc_stmnt_amng]</p>
<p>Nb de chambres aménagées : [dt_chmbr_amng]</p>
<p>Nb de places assises public : [dt_plc_ass_pub]</p>
<p>Dérogation SCDA : [dt_scda]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[anls_reg_app]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>[anls_presc]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[anls_doc_pdnt]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[anls_doc_apres]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Essais réalisés</span></p>
<p>[anls_essais]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[anls_cr]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[anls_obs]</p>
<p> </p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[anls_avis_prop]</p>
<p>[anls_avis_comp]</p>'
WHERE id = 'proces_verbal_acc';
-- modification de l'aide à la saisie des requêtes objet de ces lettres-types
UPDATE om_requete SET
merge_fields = '<table><thead><tr>
                        <th colspan="2">Enregistrement de type proces_verbal</th></tr></thead><tbody><tr><td>[pv_numero]</td><td>numéro</td></tr><tr><td>[pv_date_redaction]</td><td>date de rédaction</td></tr><tr><td>[pv_dossier_instruction_reunion]</td><td>Demande de passage en réunion</td></tr><tr><td>[signataire_nom]</td><td>nom</td></tr><tr><td>[signataire_civilite]</td><td>civilité</td></tr><tr><td>[signataire_prenom]</td><td>prénom</td></tr><tr><td>[signataire_qualite]</td><td>qualité du signataire</td></tr><tr><td>[signataire_signature]</td><td>signature</td></tr><tr><td>[reunion_date]</td><td>date souhaitée</td></tr><tr><td>[reunion_type]</td><td>type de réunion</td></tr><tr><td>[reunion_categorie]</td><td>catégorie du type de réunion</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type analyse</th></tr></thead><tbody><tr><td>[anls_id]</td><td>Identifiant unique</td></tr><tr><td>[anls_type]</td><td>type d''analyse</td></tr><tr><td>[anls_obj]</td><td>objet</td></tr><tr><td>[anls_desc]</td><td>descriptif de l''établissement</td></tr><tr><td>[anls_cr]</td><td>compte-rendu</td></tr><tr><td>[anls_obs]</td><td>observation</td></tr><tr><td>[anls_reg_app]</td><td>réglementation applicable</td></tr><tr><td>[anls_doc_pdnt]</td><td>document présenté lors des visites</td></tr><tr><td>[anls_doc_apres]</td><td>document présenté après les visites</td></tr><tr><td>[anls_avis_prop]</td><td>Avis de l''analyse</td></tr><tr><td>[anls_avis_comp]</td><td>complément d''avis</td></tr><tr><td>[anls_presc]</td><td>tableau des prescriptions</td></tr><tr><td>[anls_essais]</td><td>tableau des essais realises</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type donnees techniques accessibilite</th></tr></thead><tbody><tr><td>[dt_h_ment]</td><td>accessible mental</td></tr><tr><td>[dt_h_audtf]</td><td>accessible auditif</td></tr><tr><td>[dt_h_phsq]</td><td>accessible physique</td></tr><tr><td>[dt_h_visu]</td><td>accessible visuel</td></tr><tr><td>[dt_elvtr]</td><td>élévateur</td></tr><tr><td>[dt_ascsr]</td><td>Ascenseur</td></tr><tr><td>[dt_douche]</td><td>douche aménagée</td></tr><tr><td>[dt_bcl_mgnt]</td><td>Boucle magnétique</td></tr><tr><td>[dt_sanitaire]</td><td>sanitaire aménagé</td></tr><tr><td>[dt_plc_stmnt_amng]</td><td>nb places stationnement aménagées</td></tr><tr><td>[dt_chmbr_amng]</td><td>nb places stationnement aménagées</td></tr><tr><td>[dt_plc_ass_pub]</td><td>nb places assises public</td></tr><tr><td>[dt_scda]</td><td>Dérogation SCDA</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type donnees techniques securite incendie</th></tr></thead><tbody><tr><td>[dt_eff_pub]</td><td>effectif public</td></tr><tr><td>[dt_eff_pers]</td><td>effectif personnel</td></tr><tr><td>[dt_ssi]</td><td>type SSI</td></tr><tr><td>[dt_alarme]</td><td>type d''alarme</td></tr><tr><td>[dt_l16]</td><td>conformité L16</td></tr><tr><td>[dt_alim]</td><td>alimentation de remplacement</td></tr><tr><td>[dt_svc_sec]</td><td>Service sécurité</td></tr><tr><td>[dt_pers_jour]</td><td>effectif du personnel de jour</td></tr><tr><td>[dt_pers_nuit]</td><td>effectif du personnel de nuit</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier_instruction</th></tr></thead><tbody><tr><td>[di_lib]</td><td>libellé</td></tr><tr><td>[di_tech]</td><td>Technicien en charge</td></tr><tr><td>[di_ac]</td><td>autorité compétente</td></tr><tr><td>[di_qualif]</td><td>à qualifier</td></tr><tr><td>[di_incomp]</td><td>incomplétude</td></tr><tr><td>[di_clos]</td><td>clôturé</td></tr><tr><td>[di_prio]</td><td>prioritaire</td></tr><tr><td>[di_piece]</td><td>pièce attendue</td></tr><tr><td>[di_desc]</td><td>description</td></tr><tr><td>[di_notes]</td><td>notes</td></tr><tr><td>[di_statut]</td><td>statut</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier_coordination</th></tr></thead><tbody><tr><td>[dc_lib]</td><td>libellé</td></tr><tr><td>[dc_type]</td><td>type de dossier de coordination</td></tr><tr><td>[dc_date_dmnd]</td><td>date de la demande</td></tr><tr><td>[dc_date_butr]</td><td>date butoir</td></tr><tr><td>[dc_da_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dc_di_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dc_qualif]</td><td>à qualifier</td></tr><tr><td>[dc_etab_type]</td><td>type</td></tr><tr><td>[dc_etab_type_sec]</td><td>type(s) secondaire(s)</td></tr><tr><td>[dc_etab_cat]</td><td>catégorie</td></tr><tr><td>[dc_etab_erp]</td><td>erp</td></tr><tr><td>[dc_clos]</td><td>clôturé</td></tr><tr><td>[dc_ctr_urb]</td><td>contraintes URBA</td></tr><tr><td>[dc_etab_loc_som]</td><td>locaux sommeil</td></tr><tr><td>[dc_parent]</td><td>dossier de coordination parent</td></tr><tr><td>[dc_desc]</td><td>description</td></tr><tr><td>[dc_ap]</td><td>autorité de police en cours</td></tr><tr><td>[dc_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type etablissement</th></tr></thead><tbody><tr><td>[etab_code]</td><td>Numéro T</td></tr><tr><td>[etab_lib]</td><td>libellé</td></tr><tr><td>[etab_siret]</td><td>SIRET</td></tr><tr><td>[etab_tel]</td><td>téléphone</td></tr><tr><td>[etab_fax]</td><td>fax</td></tr><tr><td>[etab_annee_const]</td><td>année de construction</td></tr><tr><td>[etab_type]</td><td>type</td></tr><tr><td>[etab_cat]</td><td>catégorie</td></tr><tr><td>[etab_nat]</td><td>nature</td></tr><tr><td>[etab_stat_jur]</td><td>statut juridique</td></tr><tr><td>[etab_tut_adm]</td><td>tutelle administrative</td></tr><tr><td>[etab_etat]</td><td>état</td></tr><tr><td>[etab_adresse]</td><td>adresse</td></tr><tr><td>[etab_date_ar_ouv]</td><td>date d''arrêté d''ouverture</td></tr><tr><td>[etab_exp]</td><td>exploitant</td></tr><tr><td>[etab_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>'
WHERE code = 'proces_verbal';
UPDATE om_requete SET
merge_fields = '<table><thead><tr>
                        <th colspan="2">Enregistrement de type analyse</th></tr></thead><tbody><tr><td>[anls_id]</td><td>Identifiant unique</td></tr><tr><td>[anls_type]</td><td>type d''analyse</td></tr><tr><td>[anls_obj]</td><td>objet</td></tr><tr><td>[anls_desc]</td><td>descriptif de l''établissement</td></tr><tr><td>[anls_cr]</td><td>compte-rendu</td></tr><tr><td>[anls_obs]</td><td>observation</td></tr><tr><td>[anls_reg_app]</td><td>réglementation applicable</td></tr><tr><td>[anls_doc_pdnt]</td><td>document présenté lors des visites</td></tr><tr><td>[anls_doc_apres]</td><td>document présenté après les visites</td></tr><tr><td>[anls_avis_prop]</td><td>Avis de l''analyse</td></tr><tr><td>[anls_avis_comp]</td><td>complément d''avis</td></tr><tr><td>[anls_presc]</td><td>tableau des prescriptions</td></tr><tr><td>[anls_essais]</td><td>tableau des essais realises</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type donnees techniques accessibilite</th></tr></thead><tbody><tr><td>[dt_h_ment]</td><td>accessible mental</td></tr><tr><td>[dt_h_audtf]</td><td>accessible auditif</td></tr><tr><td>[dt_h_phsq]</td><td>accessible physique</td></tr><tr><td>[dt_h_visu]</td><td>accessible visuel</td></tr><tr><td>[dt_elvtr]</td><td>élévateur</td></tr><tr><td>[dt_ascsr]</td><td>Ascenseur</td></tr><tr><td>[dt_douche]</td><td>douche aménagée</td></tr><tr><td>[dt_bcl_mgnt]</td><td>Boucle magnétique</td></tr><tr><td>[dt_sanitaire]</td><td>sanitaire aménagé</td></tr><tr><td>[dt_plc_stmnt_amng]</td><td>nb places stationnement aménagées</td></tr><tr><td>[dt_chmbr_amng]</td><td>nb places stationnement aménagées</td></tr><tr><td>[dt_plc_ass_pub]</td><td>nb places assises public</td></tr><tr><td>[dt_scda]</td><td>Dérogation SCDA</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type donnees techniques securite incendie</th></tr></thead><tbody><tr><td>[dt_eff_pub]</td><td>effectif public</td></tr><tr><td>[dt_eff_pers]</td><td>effectif personnel</td></tr><tr><td>[dt_ssi]</td><td>type SSI</td></tr><tr><td>[dt_alarme]</td><td>type d''alarme</td></tr><tr><td>[dt_l16]</td><td>conformité L16</td></tr><tr><td>[dt_alim]</td><td>alimentation de remplacement</td></tr><tr><td>[dt_svc_sec]</td><td>Service sécurité</td></tr><tr><td>[dt_pers_jour]</td><td>effectif du personnel de jour</td></tr><tr><td>[dt_pers_nuit]</td><td>effectif du personnel de nuit</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier_instruction</th></tr></thead><tbody><tr><td>[di_lib]</td><td>libellé</td></tr><tr><td>[di_tech]</td><td>Technicien en charge</td></tr><tr><td>[di_ac]</td><td>autorité compétente</td></tr><tr><td>[di_qualif]</td><td>à qualifier</td></tr><tr><td>[di_incomp]</td><td>incomplétude</td></tr><tr><td>[di_clos]</td><td>clôturé</td></tr><tr><td>[di_prio]</td><td>prioritaire</td></tr><tr><td>[di_piece]</td><td>pièce attendue</td></tr><tr><td>[di_desc]</td><td>description</td></tr><tr><td>[di_notes]</td><td>notes</td></tr><tr><td>[di_statut]</td><td>statut</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type dossier_coordination</th></tr></thead><tbody><tr><td>[dc_lib]</td><td>libellé</td></tr><tr><td>[dc_type]</td><td>type de dossier de coordination</td></tr><tr><td>[dc_date_dmnd]</td><td>date de la demande</td></tr><tr><td>[dc_date_butr]</td><td>date butoir</td></tr><tr><td>[dc_da_ads]</td><td>dossier d''autorisation ADS</td></tr><tr><td>[dc_di_ads]</td><td>dossier d''instruction ADS</td></tr><tr><td>[dc_qualif]</td><td>à qualifier</td></tr><tr><td>[dc_etab_type]</td><td>type</td></tr><tr><td>[dc_etab_type_sec]</td><td>type(s) secondaire(s)</td></tr><tr><td>[dc_etab_cat]</td><td>catégorie</td></tr><tr><td>[dc_etab_erp]</td><td>erp</td></tr><tr><td>[dc_clos]</td><td>clôturé</td></tr><tr><td>[dc_ctr_urb]</td><td>contraintes URBA</td></tr><tr><td>[dc_etab_loc_som]</td><td>locaux sommeil</td></tr><tr><td>[dc_parent]</td><td>dossier de coordination parent</td></tr><tr><td>[dc_desc]</td><td>description</td></tr><tr><td>[dc_ap]</td><td>autorité de police en cours</td></tr><tr><td>[dc_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr><tr>
                        <th colspan="2">Enregistrement de type etablissement</th></tr></thead><tbody><tr><td>[etab_code]</td><td>Numéro T</td></tr><tr><td>[etab_lib]</td><td>libellé</td></tr><tr><td>[etab_siret]</td><td>SIRET</td></tr><tr><td>[etab_tel]</td><td>téléphone</td></tr><tr><td>[etab_fax]</td><td>fax</td></tr><tr><td>[etab_annee_const]</td><td>année de construction</td></tr><tr><td>[etab_type]</td><td>type</td></tr><tr><td>[etab_cat]</td><td>catégorie</td></tr><tr><td>[etab_nat]</td><td>nature</td></tr><tr><td>[etab_stat_jur]</td><td>statut juridique</td></tr><tr><td>[etab_tut_adm]</td><td>tutelle administrative</td></tr><tr><td>[etab_etat]</td><td>état</td></tr><tr><td>[etab_adresse]</td><td>adresse</td></tr><tr><td>[etab_date_ar_ouv]</td><td>date d''arrêté d''ouverture</td></tr><tr><td>[etab_exp]</td><td>exploitant</td></tr><tr><td>[etab_ref_cad]</td><td>réf. cadastrales</td></tr><tr style="height: 10px !important;"><td colspan="2"></td></tr></tbody></table>'
WHERE code = 'analyses';
-- suppression du sous-état
DELETE FROM om_sousetat WHERE id = 'anl_essai_realise';
----
-- END / Remplacement du sous-état "essais réalisés" par un champ de fusion
-- dans les lettres-types du contexte analyse
----

----
-- BEGIN / Correction du paramétrage des sous-états du contexte réunion
----
-- Compte-rendu général
UPDATE om_sousetat
    SET entete_orientation = '0|0|0|0'
    WHERE id LIKE 'reu_crg%';
-- Ordre du jour
UPDATE om_sousetat
    SET entete_orientation = '0|0|0|0|0'
    WHERE id LIKE 'reu_odj%';
----
-- END / Correction du paramétrage des sous-états du contexte réunion
----

----
-- BEGIN / Correction du libellé des types de courrier de l'analyse
----
-- Compte-rendu
UPDATE courrier_type
    SET libelle = '(*) Analyse - Compte-rendu SI'
    WHERE code = 'ANL-SI-CRD';
UPDATE courrier_type
    SET libelle = '(*) Analyse - Compte-rendu ACC'
    WHERE code = 'ANL-ACC-CRD';
-- Rapport
UPDATE courrier_type
    SET libelle = '(*) Analyse - Rapport SI'
    WHERE code = 'ANL-SI-RPT';
UPDATE courrier_type
    SET libelle = '(*) Analyse - Rapport ACC'
    WHERE code = 'ANL-ACC-RPT';
-- Procès-verbal
UPDATE courrier_type
    SET libelle = '(*) Analyse - Procès-verbal SI'
    WHERE code = 'ANL-SI-PV';
UPDATE courrier_type
    SET libelle = '(*) Analyse - Procès-verbal ACC'
    WHERE code = 'ANL-ACC-PV';
----
-- END / Correction du libellé des types de courrier de l'analyse
----