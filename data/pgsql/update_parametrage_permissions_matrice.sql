--
SET search_path = openaria, public, pg_catalog;

--
\i init_parametrage_permissions_matrice.sql

-- Mise à jour des séquences
\set schema '\'openaria\''
\i update_sequences.sql
