--
-- START - Mise à jour des éditions - Novembre 2015
--
-- Entête
ALTER TABLE om_lettretype ADD COLUMN header_om_htmletat text;
ALTER TABLE om_lettretype ADD COLUMN header_offset integer NOT NULL DEFAULT 0;
ALTER TABLE om_etat ADD COLUMN header_om_htmletat text;
ALTER TABLE om_etat ADD COLUMN header_offset integer NOT NULL DEFAULT 12;
-- Pied de page
ALTER TABLE om_lettretype ADD COLUMN footer_om_htmletat text;
ALTER TABLE om_lettretype ADD COLUMN footer_offset integer NOT NULL DEFAULT 0;
ALTER TABLE om_etat ADD COLUMN footer_om_htmletat text;
ALTER TABLE om_etat ADD COLUMN footer_offset integer NOT NULL DEFAULT 12;
-- Rétro-compatibilité aujourd'hui toutes les éditions doivent avoir un pied de page
UPDATE om_lettretype SET footer_offset = 12,
footer_om_htmletat='<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>';
UPDATE om_etat SET footer_offset = 12,
footer_om_htmletat='<p style="text-align:center;font-size:8pt;"><em>Page &numpage/&nbpages</em></p>';
--
-- END - Mise à jour des éditions - Novembre 2015
--
