--------------------------------------------------------------------------------
-- Instructions pour l'utilisation du SIG via postgis
--
-- Ce fichier permet de modifier le modèle de données créé précédemment pour y
-- ajouter des champs de type geom pour la gestion du SIG.
--
-- @package openmairie_exemple
-- @version SVN : $Id$
--------------------------------------------------------------------------------


--
ALTER TABLE etablissement DROP COLUMN geom_point;
ALTER TABLE etablissement DROP COLUMN geom_emprise;
ALTER TABLE dossier_coordination DROP COLUMN geom_point;
ALTER TABLE dossier_coordination DROP COLUMN geom_emprise;

--
SELECT AddGeometryColumn ( 'etablissement', 'geom_point', 2154 , 'POINT', 2 );
SELECT AddGeometryColumn ( 'etablissement', 'geom_emprise', 2154 , 'MULTIPOLYGON', 2 );
SELECT AddGeometryColumn ( 'dossier_coordination', 'geom_point', -1, 'GEOMETRY', 2);
SELECT AddGeometryColumn ( 'dossier_coordination', 'geom_emprise', -1, 'GEOMETRY', 2);