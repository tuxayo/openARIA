--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a6
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------
--
ALTER TABLE visite ADD column courrier_annulation integer;
COMMENT ON COLUMN dossier_coordination_type.analyses_type_si
    IS 'Type d''analyse sécurité par défaut';
ALTER TABLE visite ADD column programmation_version_modification integer;
COMMENT ON COLUMN dossier_coordination_type.analyses_type_si
    IS 'Type d''analyse sécurité par défaut';
ALTER TABLE ONLY visite ADD CONSTRAINT courrier_annulation_visite_fkey 
    FOREIGN KEY (courrier_annulation) REFERENCES courrier(courrier);

-- Ajout des droits sur les programmation de visites pour les profils cadres
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_consulter', 7);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_envoyer_convoc_exploit', 7);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_envoyer_part', 7);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_tab', 7);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_consulter', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_valider', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_finaliser', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_envoyer_part', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_modifier', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_envoyer_convoc_exploit', 3);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_reouvrir', 3);

INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_supprimer', 2);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_modifier', 2);
INSERT INTO om_droit VALUES (nextval('om_droit_seq'), 'programmation_reouvrir', 2);

DELETE FROM om_droit WHERE libelle = 'visite_visite_consulter';

-- BEGIN
-- Suppression de la table de paramétrage analyse_avis remplacée par reunion_avis
ALTER TABLE analyses RENAME analyses_avis TO reunion_avis;
ALTER TABLE analyses
    DROP CONSTRAINT analyses_analyses_avis_fkey,
    ADD CONSTRAINT analyses_reunion_avis_fkey 
    FOREIGN KEY (reunion_avis) REFERENCES reunion_avis(reunion_avis);
DROP TABLE analyses_avis;
-- END
-- Suppression de la table de paramétrage analyse_avis remplacée par reunion_avis

-- BEGIN rapport d'analyse SI
-- sous-état essais réalisés
INSERT INTO "om_sousetat" ("om_sousetat", "om_collectivite", "id", "libelle", "actif", "titre", "titrehauteur", "titrefont", "titreattribut", "titretaille", "titrebordure", "titrealign", "titrefond", "titrefondcouleur", "titretextecouleur", "intervalle_debut", "intervalle_fin", "entete_flag", "entete_fond", "entete_orientation", "entete_hauteur", "entetecolone_bordure", "entetecolone_align", "entete_fondcouleur", "entete_textecouleur", "tableau_largeur", "tableau_bordure", "tableau_fontaille", "bordure_couleur", "se_fond1", "se_fond2", "cellule_fond", "cellule_hauteur", "cellule_largeur", "cellule_bordure_un", "cellule_bordure", "cellule_align", "cellule_fond_total", "cellule_fontaille_total", "cellule_hauteur_total", "cellule_fondcouleur_total", "cellule_bordure_total", "cellule_align_total", "cellule_fond_moyenne", "cellule_fontaille_moyenne", "cellule_hauteur_moyenne", "cellule_fondcouleur_moyenne", "cellule_bordure_moyenne", "cellule_align_moyenne", "cellule_fond_nbr", "cellule_fontaille_nbr", "cellule_hauteur_nbr", "cellule_fondcouleur_nbr", "cellule_bordure_nbr", "cellule_align_nbr", "cellule_numerique", "cellule_total", "cellule_moyenne", "cellule_compteur", "om_sql") VALUES
(1, 1,  'essai_realise',    'Essais réalisés',  't',    '* Essais réalisés',    10, 'helvetica',    'B',    12, '0',    'L',    '0',    '255-255-255',  '0-0-0',    10, 15, '1',    '1',    '0|0|0|0',  15, 'LTBR|LTBR|LTBR|LTBR',  'C|C|C|C',  '188-188-188',  '0-0-0',    195,    '1',    10, '0-0-0',    '255-255-255',  '239-239-239',  '1',    10, '30|50|60|60',  'LTBR|LTBR|LTBR|LTBR',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '1',    10, 15, '196-213-213',  'LTBR|LTBR|LTBR|LTBR',  'C|L|L|L',  '999|999|999|999',  '0|0|0|0',  '0|0|0|0',  '0|0|0|0',  'SELECT
    case lien_essai_realise_analyses.concluant
        when ''t''
        then ''Oui''
        else ''Non''
    end as concluant,
    essai_realise.libelle,
    essai_realise.description,
    lien_essai_realise_analyses.complement
FROM
    &DB_PREFIXElien_essai_realise_analyses
LEFT JOIN &DB_PREFIXEessai_realise
    ON essai_realise.essai_realise = lien_essai_realise_analyses.essai_realise
WHERE
    lien_essai_realise_analyses.analyses = &idx
ORDER BY
    lien_essai_realise_analyses.concluant DESC, essai_realise.libelle ASC');

-- requete
INSERT INTO "om_requete" ("om_requete", "code", "libelle", "description", "requete", "merge_fields") VALUES
(nextval('om_requete_seq'), 'si_rapport_analyse', 'Rapport d''analyse SI', NULL, 'SELECT
    -- table dossier_instruction
    dossier_instruction.libelle as dossier_instruction,
    -- table etablissement
    etablissement.libelle as etablissement,
    -- table analyse
    analyses.analyses,
    analyses.service as service,
    analyses.objet as objet,
    analyses.descriptif_etablissement_om_html as descriptif_etablissement,
    analyses.reglementation_applicable_om_html as reglementation_applicable,
    analyses.document_presente_pendant_om_html as document_presente_pendant,
    analyses.document_presente_apres_om_html as document_presente_apres,
    analyses.compte_rendu_om_html as compte_rendu,
    analyses.observation_om_html as observation,
    analyses.avis_complement as avis_complement,
    analyses.si_descriptif_om_html as description_securite,
    analyses.si_effectif_public as effectif_public,
    analyses.si_effectif_personnel as effectif_personnel,
    analyses.si_type_ssi as type_ssi,
    analyses.si_type_alarme as type_alarme,
    case analyses.si_conformite_i16
        when ''t''
        then ''Oui''
        else ''Non''
    end as conformite_i16,
    case analyses.si_alimentation_remplacement
        when ''t''
        then ''Oui''
        else ''Non''
    end as alimentation_remplacement,
    case analyses.si_service_securite
        when ''t''
        then ''Oui''
        else ''Non''
    end as service_securite,
    analyses.si_personnel_jour as personnel_jour,
    analyses.si_personnel_nuit as personnel_nuit,
    -- tables de paramétrage
    concat(etablissement_categorie.libelle, '' - '',
        etablissement_categorie.description) as etablissement_categorie,
    concat(type1.libelle, '' - '',
        type1.description) as etablissement_type,
    analyses_type.libelle as type_analyse,
    reunion_avis.libelle as avis_libelle,
    reunion_avis.description as avis_description,
    -- tables de liaison
    array_to_string(
        array_agg(
            concat(type2.libelle, '' - '', type2.description)
       ),
    ''<br/>'') as etablissement_type_secondaire
FROM
    &DB_PREFIXEanalyses
LEFT JOIN &DB_PREFIXEdossier_instruction
    ON dossier_instruction.analyses = analyses.analyses
LEFT JOIN &DB_PREFIXEdossier_coordination
    ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
LEFT JOIN &DB_PREFIXEetablissement
    ON etablissement.etablissement = dossier_coordination.etablissement
LEFT JOIN &DB_PREFIXEetablissement_type type1
    ON type1.etablissement_type = analyses.etablissement_type
LEFT JOIN &DB_PREFIXEanalyses_type
    ON analyses_type.analyses_type=analyses.analyses_type
LEFT JOIN &DB_PREFIXEreunion_avis
    ON reunion_avis.reunion_avis=analyses.reunion_avis
LEFT JOIN &DB_PREFIXEetablissement_categorie
    ON etablissement_categorie.etablissement_categorie=analyses.etablissement_categorie
LEFT JOIN &DB_PREFIXElien_analyses_etablissement_type lien_type
    ON lien_type.analyses = analyses.analyses
LEFT JOIN &DB_PREFIXEetablissement_type type2
    ON type2.etablissement_type = lien_type.etablissement_type
WHERE
    analyses.analyses = &idx
GROUP BY
    analyses.analyses, analyses_type.libelle, reunion_avis.libelle,
    reunion_avis.description,  etablissement_categorie.libelle,
    etablissement_categorie.description, type1.libelle, type1.description,
    dossier_instruction.libelle, analyses.si_descriptif_om_html,
    etablissement.libelle,
    analyses.si_effectif_public, analyses.si_effectif_personnel,
    analyses.si_type_ssi, analyses.si_type_alarme,
    analyses.si_conformite_i16, analyses.si_alimentation_remplacement,
    analyses.si_service_securite, analyses.si_personnel_jour,
    analyses.si_personnel_nuit', '    -- Libellé de l''établissement
[etablissement]
    -- Libellé du dossier d''instruction
[dossier_instruction]
    -- Type de l''analyse
[type_analyse]
    -- Objet
[objet]
    -- Descriptif de l''établissement
[descriptif_etablissement]
    -- Catégorie de l''établissement
[etablissement_categorie]
    -- Type de l''établissement
[etablissement_type]
    -- Types secondaires de l''établissement
[etablissement_type_secondaire]
    -- Réglementation applicable
[reglementation_applicable]
    -- Documents présentés pendant
[document_presente_pendant]
    -- Documents présentés Après
[document_presente_apres]
    -- Compte-rendu
[compte_rendu]
    -- Observation
[observation]
    -- Avis proposé
[avis_libelle]
[avis_description]
[avis_complement]
    -- Données techniques
[description_securite]
[effectif_public]
[effectif_personnel]
[type_ssi]
[type_alarme]
[conformite_i16]
[alimentation_remplacement]
[service_securite]
[personnel_jour]
[personnel_nuit]');
-- lettre type
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'si_rapport_analyse',   'Rapport d''analyse SI',    't',    'P',    'A4',   NULL,   10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Rapport d''analyse - sécurité incendie<br /></span></p>',  48, 10, 0,  10, '0',    '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span></p>
<p>Description sécurité : [description_securite]</p>
<p>Effectif public : [effectif_public]</p>
<p>Effectif personnel : [effectif_personnel]</p>
<p>Type SSI : [type_ssi]</p>
<p>Type alarme : [type_alarme]</p>
<p>Conformité I16 : [conformite_i16]</p>
<p>Alimentation de remplacement : [alimentation_remplacement]</p>
<p>Service sécurité : [service_securite]</p>
<p>Personnel de jour : [personnel_jour]</p>
<p>Personnel de nuit : [personnel_nuit]</p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  2,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- END rapport d'analyse SI

-- BEGIN rapport d'analyse ACC
-- requete
INSERT INTO "om_requete" ("om_requete", "code", "libelle", "description", "requete", "merge_fields") VALUES
(nextval('om_requete_seq'), 'acc_rapport_analyse', 'Rapport d''analyse ACC', NULL, 'SELECT
    -- table dossier_instruction
    dossier_instruction.libelle as dossier_instruction,
    -- table etablissement
    etablissement.libelle as etablissement,
    -- table analyse
    analyses.analyses,
    analyses.service as service,
    analyses.objet as objet,
    analyses.descriptif_etablissement_om_html as descriptif_etablissement,
    analyses.reglementation_applicable_om_html as reglementation_applicable,
    analyses.document_presente_pendant_om_html as document_presente_pendant,
    analyses.document_presente_apres_om_html as document_presente_apres,
    analyses.compte_rendu_om_html as compte_rendu,
    analyses.observation_om_html as observation,
    analyses.avis_complement as avis_complement,
    analyses.acc_descriptif_om_html as description_accessibilite,
    analyses.acc_places_stationnement_amenagees as places_stationnement,
    analyses.acc_places_assises_public as places_assises_public,
    analyses.acc_chambres_amenagees as chambres_amenagees,
    derogation_scda.libelle as derogation_scda,
    case analyses.acc_handicap_mental
        when ''t''
        then ''Oui''
        else ''Non''
    end as handicap_mental,
    case analyses.acc_handicap_auditif
        when ''t''
        then ''Oui''
        else ''Non''
    end as handicap_auditif,
    case analyses.acc_elevateur
        when ''t''
        then ''Oui''
        else ''Non''
    end as elevateur,
    case analyses.acc_handicap_physique
        when ''t''
        then ''Oui''
        else ''Non''
    end as handicap_physique,
    case analyses.acc_ascenseur
        when ''t''
        then ''Oui''
        else ''Non''
    end as ascenseur,
    case analyses.acc_handicap_visuel
        when ''t''
        then ''Oui''
        else ''Non''
    end as handicap_visuel,
    case analyses.acc_boucle_magnetique
        when ''t''
        then ''Oui''
        else ''Non''
    end as boucle_magnetique,
    case analyses.acc_douche
        when ''t''
        then ''Oui''
        else ''Non''
    end as douche,
    case analyses.acc_sanitaire
        when ''t''
        then ''Oui''
        else ''Non''
    end as sanitaire,
    -- tables de paramétrage
    concat(etablissement_categorie.libelle, '' - '',
        etablissement_categorie.description) as etablissement_categorie,
    concat(type1.libelle, '' - '',
        type1.description) as etablissement_type,
    analyses_type.libelle as type_analyse,
    reunion_avis.libelle as avis_libelle,
    reunion_avis.description as avis_description,
    -- tables de liaison
    array_to_string(
        array_agg(
            concat(type2.libelle, '' - '', type2.description)
       ),
    ''<br/>'') as etablissement_type_secondaire
FROM
    &DB_PREFIXEanalyses
LEFT JOIN &DB_PREFIXEderogation_scda
    ON derogation_scda.derogation_scda = analyses.acc_derogation_scda
LEFT JOIN &DB_PREFIXEdossier_instruction
    ON dossier_instruction.analyses = analyses.analyses
LEFT JOIN &DB_PREFIXEdossier_coordination
    ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
LEFT JOIN &DB_PREFIXEetablissement
    ON etablissement.etablissement = dossier_coordination.etablissement
LEFT JOIN &DB_PREFIXEetablissement_type type1
    ON type1.etablissement_type = analyses.etablissement_type
LEFT JOIN &DB_PREFIXEanalyses_type
    ON analyses_type.analyses_type=analyses.analyses_type
LEFT JOIN &DB_PREFIXEreunion_avis
    ON reunion_avis.reunion_avis=analyses.reunion_avis
LEFT JOIN &DB_PREFIXEetablissement_categorie
    ON etablissement_categorie.etablissement_categorie=analyses.etablissement_categorie
LEFT JOIN &DB_PREFIXElien_analyses_etablissement_type lien_type
    ON lien_type.analyses = analyses.analyses
LEFT JOIN &DB_PREFIXEetablissement_type type2
    ON type2.etablissement_type = lien_type.etablissement_type
WHERE
    analyses.analyses = &idx
GROUP BY
    analyses.analyses, analyses_type.libelle, reunion_avis.libelle,
    reunion_avis.description,  etablissement_categorie.libelle,
    etablissement_categorie.description, type1.libelle, type1.description,
    dossier_instruction.libelle, analyses.acc_descriptif_om_html,
    analyses.acc_places_stationnement_amenagees, etablissement.libelle,
    analyses.acc_places_assises_public, analyses.acc_chambres_amenagees,
    analyses.acc_handicap_mental, analyses.acc_handicap_auditif,
    analyses.acc_elevateur, analyses.acc_handicap_physique,
    analyses.acc_ascenseur, analyses.acc_handicap_visuel,
    analyses.acc_boucle_magnetique, analyses.acc_douche,
    analyses.acc_sanitaire, derogation_scda.libelle', '    -- Libellé de l''établissement
[etablissement]
    -- Libellé du dossier d''instruction
[dossier_instruction]
    -- Type de l''analyse
[type_analyse]
    -- Objet
[objet]
    -- Descriptif de l''établissement
[descriptif_etablissement]
    -- Catégorie de l''établissement
[etablissement_categorie]
    -- Type de l''établissement
[etablissement_type]
    -- Types secondaires de l''établissement
[etablissement_type_secondaire]
    -- Réglementation applicable
[reglementation_applicable]
    -- Documents présentés pendant
[document_presente_pendant]
    -- Documents présentés Après
[document_presente_apres]
    -- Compte-rendu
[compte_rendu]
    -- Observation
[observation]
    -- Avis proposé
[avis_libelle]
[avis_description]
[avis_complement]
    -- Données techniques
[places_stationnement]
[places_assises_public]
[chambres_amenagees]
[handicap_mental]
[handicap_auditif]
[elevateur]
[handicap_physique]
[ascenseur]
[handicap_visuel]
[boucle_magnetique]
[douche]
[sanitaire]
[derogation_scda]');
-- lettre type
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'acc_rapport_analyse',  'Rapport d''analyse ACC',   't',    'P',    'A4',   NULL,   10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Rapport d''analyse - accessibilité<br /></span></p>',    57, 10, 0,  10, '0',    '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span><br />Dérogation SCDA : [derogation_scda]<br />Accessible visuel : [handicap_visuel]</p>
<p>Accessible auditif : [handicap_auditif]</p>
<p>Accessible mental : [handicap_mental]<br />Accessible physique : [handicap_physique]</p>
<p>Présence de douche(s) : [douche]<br />Présence de sanitaire(s) : [sanitaire]</p>
<p>Présence d''un élévateur : [elevateur]<br />Présence d''un ascenseur : [ascenseur]<br />Présence d''une boucle magnétique : [boucle_magnetique]</p>
<p>Nombre de chambres aménagées : [chambres_amenagees]<br />Nombre de places assises pour le public : [places_assises_public]<br />Nombre de places de stationnement aménagées : [places_stationnement]<br /><br /></p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  3,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- END rapport d'analyse ACC

-- BEGIN lettre type compte-rendu d'analyse
-- SI
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'si_compte_rendu_analyse',  'Compte-rendu d''analyse SI',   't',    'P',    'A4',   NULL,   10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Compte-rendu d''analyse - sécurité incendie<br /></span></p>',    42, 10, 0,  10, '0',    '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span><br />Dérogation SCDA : [derogation_scda]<br />Accessible visuel : [handicap_visuel]</p>
<p>Accessible auditif : [handicap_auditif]</p>
<p>Accessible mental : [handicap_mental]<br />Accessible physique : [handicap_physique]</p>
<p>Présence de douche(s) : [douche]<br />Présence de sanitaire(s) : [sanitaire]</p>
<p>Présence d''un élévateur : [elevateur]<br />Présence d''un ascenseur : [ascenseur]<br />Présence d''une boucle magnétique : [boucle_magnetique]</p>
<p>Nombre de chambres aménagées : [chambres_amenagees]<br />Nombre de places assises pour le public : [places_assises_public]<br />Nombre de places de stationnement aménagées : [places_stationnement]<br /><br /></p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  3,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- ACC
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'acc_compte_rendu_analyse',  'Compte-rendu d''analyse ACC',   't',    'P',    'A4',   NULL,   10, 10, '<p><span class=''mce_maj'' style=''font-size: 14pt;''>Compte-rendu d''analyse - accessibilité<br /></span></p>',    50, 10, 0,  10, '0',    '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span><br />Dérogation SCDA : [derogation_scda]<br />Accessible visuel : [handicap_visuel]</p>
<p>Accessible auditif : [handicap_auditif]</p>
<p>Accessible mental : [handicap_mental]<br />Accessible physique : [handicap_physique]</p>
<p>Présence de douche(s) : [douche]<br />Présence de sanitaire(s) : [sanitaire]</p>
<p>Présence d''un élévateur : [elevateur]<br />Présence d''un ascenseur : [ascenseur]<br />Présence d''une boucle magnétique : [boucle_magnetique]</p>
<p>Nombre de chambres aménagées : [chambres_amenagees]<br />Nombre de places assises pour le public : [places_assises_public]<br />Nombre de places de stationnement aménagées : [places_stationnement]<br /><br /></p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  3,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- END lettre type compte-rendu d'analyse

-- BEGIN lettre type procès verbal
-- SI
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'si_proces_verbal', 'PV SI',    't',    'P',    'A4',   NULL,   10, 10, '<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''><span style=''text-decoration: line-through;''>Procès verbal</span> - <span style=''text-decoration: line-through;''>sécurité incendie</span><br /></span></p>
<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt; color: #ff0000;''><span style=''text-decoration: underline;''>/!\</span> <span style=''font-family: courier new,courier; font-weight: bold;''>DOCUMENT DE TRAVAIL</span> <span style=''text-decoration: underline;''>/!\</span></span></p>',  7,  10, 0,  10, '0',    '<p style=''text-align: center;''> </p>
<p style=''text-align: center;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span><br />Dérogation SCDA : [derogation_scda]<br />Accessible visuel : [handicap_visuel]</p>
<p>Accessible auditif : [handicap_auditif]</p>
<p>Accessible mental : [handicap_mental]<br />Accessible physique : [handicap_physique]</p>
<p>Présence de douche(s) : [douche]<br />Présence de sanitaire(s) : [sanitaire]</p>
<p>Présence d''un élévateur : [elevateur]<br />Présence d''un ascenseur : [ascenseur]<br />Présence d''une boucle magnétique : [boucle_magnetique]</p>
<p>Nombre de chambres aménagées : [chambres_amenagees]<br />Nombre de places assises pour le public : [places_assises_public]<br />Nombre de places de stationnement aménagées : [places_stationnement]<br /><br /></p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  3,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- ACC
INSERT INTO "om_lettretype" ("om_lettretype", "om_collectivite", "id", "libelle", "actif", "orientation", "format", "logo", "logoleft", "logotop", "titre_om_htmletat", "titreleft", "titretop", "titrelargeur", "titrehauteur", "titrebordure", "corps_om_htmletatex", "om_sql", "margeleft", "margetop", "margeright", "margebottom", "se_font", "se_couleurtexte") VALUES
(nextval('om_lettretype_seq'), 1,  'acc_proces_verbal',    'PV ACC',   't',    'P',    'A4',   NULL,   10, 10, '<p style=''text-align: center;''><span class=''mce_maj'' style=''font-size: 14pt;''><span style=''text-decoration: line-through;''>Procès verbal</span> - <span style=''text-decoration: line-through;''>accessibilité</span></span></p>
<p style=''text-align: center;''><span style=''font-size: 14pt; color: #ff0000;''><span style=''text-decoration: underline;''>/!\</span> <span style=''font-family: courier new,courier; font-weight: bold;''>DOCUMENT DE TRAVAIL</span> <span style=''text-decoration: underline;''>/!\</span></span></p>',    7,  10, 0,  10, '0',    '<p style=''text-align: center;''><span style=''font-weight: bold;''>Références</span></p>
<p style=''text-align: left;''> </p>
<p style=''text-align: left;''><span style=''font-weight: bold;''>* Dosser d''instruction</span></p>
<p style=''text-align: left;''>Libellé : [dossier_instruction]</p>
<p style=''text-align: left;''><br /><span style=''font-weight: bold;''>* Établissement</span></p>
<p style=''text-align: left;''>Libellé : [etablissement]</p>
<p style=''text-align: left;''>Type : [etablissement_type]</p>
<p style=''text-align: left;''>Catégorie : [etablissement_categorie]</p>
<p style=''text-align: left;''> </p>
<p style=''text-align: center;''><span style=''font-weight: bold;''>Analyse</span></p>
<p><span style=''font-weight: bold;''>* Type d''analyse</span><br />[type_analyse]</p>
<p><span style=''font-weight: bold;''>* Objet</span></p>
<p>[objet]</p>
<p><span style=''font-weight: bold;''>* Descriptif de l''établissement</span></p>
<p>[descriptif_etablissement]</p>
<p><span style=''font-weight: bold;''>* Types secondaires de l''établissement</span></p>
<p>[etablissement_type_secondaire]</p>
<p><span style=''font-weight: bold;''>* Données techniques</span><br />Dérogation SCDA : [derogation_scda]<br />Accessible visuel : [handicap_visuel]</p>
<p>Accessible auditif : [handicap_auditif]</p>
<p>Accessible mental : [handicap_mental]<br />Accessible physique : [handicap_physique]</p>
<p>Présence de douche(s) : [douche]<br />Présence de sanitaire(s) : [sanitaire]</p>
<p>Présence d''un élévateur : [elevateur]<br />Présence d''un ascenseur : [ascenseur]<br />Présence d''une boucle magnétique : [boucle_magnetique]</p>
<p>Nombre de chambres aménagées : [chambres_amenagees]<br />Nombre de places assises pour le public : [places_assises_public]<br />Nombre de places de stationnement aménagées : [places_stationnement]<br /><br /></p>
<p><span style=''font-weight: bold;''>* Réglementation applicable</span></p>
<p>[reglementation_applicable]</p>
<p><span style=''font-weight: bold;''>* Prescriptions<br /></span></p>
<p>&prescriptions</p>
<p><span style=''font-weight: bold;''>* Documents présentés lors des visites<br /></span></p>
<p>[document_presente_pendant]</p>
<p><span style=''font-weight: bold;''>* Documents fournis après les visites</span></p>
<p>[document_presente_apres]</p>
<p><span id=''essai_realise'' class=''mce_sousetat''>Essais réalisés</span></p>
<p><span style=''font-weight: bold;''>* Compte-rendu</span></p>
<p>[compte_rendu]</p>
<p><span style=''font-weight: bold;''>* Observation</span></p>
<p>[observation]</p>
<p><span style=''font-weight: bold;''>* Avis proposé</span></p>
<p>[avis_libelle]</p>
<p>[avis_description]</p>
<p>[avis_complement]</p>',  3,  5,  10, 5,  15, 'helvetica',    '0-0-0');
-- END lettre type procès verbal

-- Ajout des contraintes nécessaires sur la table dossier_instruction_reunion (demande de passage en reunion)
ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_reunion_type_categorie_fkey
    FOREIGN KEY (reunion_type_categorie) REFERENCES reunion_categorie(reunion_categorie);
ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_avis_fkey
    FOREIGN KEY (avis) REFERENCES reunion_avis(reunion_avis);
ALTER TABLE ONLY dossier_instruction_reunion
    ADD CONSTRAINT dossier_instruction_reunion_proposition_avis_fkey
    FOREIGN KEY (proposition_avis) REFERENCES reunion_avis(reunion_avis);

-- Ajout des laisons avec la table modele_edition
ALTER TABLE ONLY reunion_type ALTER COLUMN modele_ordre_du_jour TYPE integer USING NULL;
ALTER TABLE ONLY reunion_type ALTER COLUMN modele_compte_rendu_global TYPE integer USING NULL;
ALTER TABLE ONLY reunion_type ALTER COLUMN modele_compte_rendu_specifique TYPE integer USING NULL;
ALTER TABLE ONLY reunion_type ALTER COLUMN modele_feuille_presence TYPE integer USING NULL;
ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_ordre_du_jour_fkey
    FOREIGN KEY (modele_ordre_du_jour) REFERENCES modele_edition(modele_edition);
ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_compte_rendu_global_fkey
    FOREIGN KEY (modele_compte_rendu_global) REFERENCES modele_edition(modele_edition);
ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_compte_rendu_specifique_fkey
    FOREIGN KEY (modele_compte_rendu_specifique) REFERENCES modele_edition(modele_edition);
ALTER TABLE ONLY reunion_type
    ADD CONSTRAINT reunion_type_modele_feuille_presence_fkey
    FOREIGN KEY (modele_feuille_presence) REFERENCES modele_edition(modele_edition);

-- Gestion des membres présents à la réunion
CREATE TABLE reunion_instance_membre (
    reunion_instance_membre integer NOT NULL,
    membre character varying(100),
    description text,
    reunion_instance integer NOT NULL,
    om_validite_debut date,
    om_validite_fin date
);
CREATE SEQUENCE reunion_instance_membre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE reunion_instance_membre_seq OWNED BY reunion_instance_membre.reunion_instance_membre;
ALTER TABLE ONLY reunion_instance_membre
    ADD CONSTRAINT reunion_instance_membre_pkey PRIMARY KEY (reunion_instance_membre);
ALTER TABLE ONLY reunion_instance_membre
    ADD CONSTRAINT reunion_instance_membre_reunion_instance_fkey FOREIGN KEY (reunion_instance) REFERENCES reunion_instance(reunion_instance);

-- Gestion des membres présents à la réunion
CREATE TABLE lien_reunion_r_instance_r_i_membre (
    lien_reunion_r_instance_r_i_membre integer NOT NULL,
    reunion integer NOT NULL,
    reunion_instance integer NOT NULL,
    reunion_instance_membre integer,
    observation text
);
CREATE SEQUENCE lien_reunion_r_instance_r_i_membre_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE lien_reunion_r_instance_r_i_membre_seq OWNED BY lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre;
ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre_pkey PRIMARY KEY (lien_reunion_r_instance_r_i_membre);
ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion__fkey FOREIGN KEY (reunion) REFERENCES reunion(reunion);
ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion_instance__fkey FOREIGN KEY (reunion_instance) REFERENCES reunion_instance(reunion_instance);
ALTER TABLE ONLY lien_reunion_r_instance_r_i_membre
    ADD CONSTRAINT lien_reunion_r_instance_r_i_membre__reunion_instance_membre__fkey FOREIGN KEY (reunion_instance_membre) REFERENCES reunion_instance_membre(reunion_instance_membre);

-- Ajout du DI et des données techniques dans l'analyse
-- et donc suppression de l'analyse dans le DI et de la table données techniques
ALTER TABLE analyses
    ADD si_descriptif_om_html text NULL,
    ADD si_effectif_public integer NULL,
    ADD si_effectif_personnel integer NULL,
    ADD si_type_ssi character varying(100) NULL,
    ADD si_type_alarme character varying(100) NULL,
    ADD si_conformite_i16 boolean NULL,
    ADD si_alimentation_remplacement boolean NULL,
    ADD si_service_securite boolean NULL,
    ADD si_personnel_jour integer NULL,
    ADD si_personnel_nuit integer NULL,
    ADD acc_descriptif_om_html text NULL,
    ADD acc_handicap_mental boolean NULL,
    ADD acc_handicap_auditif boolean NULL,
    ADD acc_places_stationnement_amenagees integer NULL,
    ADD acc_elevateur boolean NULL,
    ADD acc_handicap_physique boolean NULL,
    ADD acc_ascenseur boolean NULL,
    ADD acc_handicap_visuel boolean NULL,
    ADD acc_boucle_magnetique boolean NULL,
    ADD acc_chambres_amenagees integer NULL,
    ADD acc_douche boolean NULL,
    ADD acc_derogation_scda integer NULL,
    ADD acc_sanitaire boolean NULL,
    ADD acc_places_assises_public integer NULL,
    ADD dossier_instruction integer NOT NULL;
COMMENT ON COLUMN analyses.si_descriptif_om_html IS 'Description de la sécurité de l''établissement';
COMMENT ON COLUMN analyses.si_effectif_public IS 'Effectif public';
COMMENT ON COLUMN analyses.si_effectif_personnel IS 'Effectif personnel';
COMMENT ON COLUMN analyses.si_type_ssi IS 'Type SSI (A/B/C/D/E)';
COMMENT ON COLUMN analyses.si_type_alarme IS 'Type d''alarme (1/2a/2b/3/4)';
COMMENT ON COLUMN analyses.si_conformite_i16 IS 'Conforme au référentiel I16 ?';
COMMENT ON COLUMN analyses.si_alimentation_remplacement IS 'Présence d''une alimentation de remplacement ?';
COMMENT ON COLUMN analyses.si_service_securite IS 'Présence d''un service sécurité ?';
COMMENT ON COLUMN analyses.si_personnel_jour IS 'Effectif personnel de jour';
COMMENT ON COLUMN analyses.si_personnel_nuit IS 'Effectif personnel de nuit';
COMMENT ON COLUMN analyses.acc_descriptif_om_html IS 'Description de l''accessibilité de l''établissement';
COMMENT ON COLUMN analyses.acc_handicap_mental IS 'Établissement accessible aux personnes atteintes d''un handicap mental ?';
COMMENT ON COLUMN analyses.acc_handicap_auditif IS 'Établissement accessible aux personnes atteintes d''un handicap auditif ?';
COMMENT ON COLUMN analyses.acc_places_stationnement_amenagees IS 'Nombre de places de stationnement aménagées';
COMMENT ON COLUMN analyses.acc_elevateur IS 'Présence d''un élévateur ?';
COMMENT ON COLUMN analyses.acc_handicap_physique IS 'Établissement accessible aux personnes atteintes d''un handicap physique ?';
COMMENT ON COLUMN analyses.acc_ascenseur IS 'Présence d''un ascenseur ?';
COMMENT ON COLUMN analyses.acc_handicap_visuel IS 'Établissement accessible aux personnes atteintes d''un handicap visuel ?';
COMMENT ON COLUMN analyses.acc_boucle_magnetique IS 'Présence d''une boucle magnétique ?';
COMMENT ON COLUMN analyses.acc_chambres_amenagees IS 'Nombre de chambres aménagées pour être accessibles';
COMMENT ON COLUMN analyses.acc_douche IS 'Présence de douche(s) ?';
COMMENT ON COLUMN analyses.acc_derogation_scda IS 'Dérogation SCDA';
COMMENT ON COLUMN analyses.acc_sanitaire IS 'Présence de sanitaire(s) ?';
COMMENT ON COLUMN analyses.acc_places_assises_public IS 'Nombre de places assises pour le public';
COMMENT ON COLUMN analyses.dossier_instruction IS 'Dossier d''instruction auquel l''analyse est rattachée';
ALTER TABLE ONLY analyses
    ADD CONSTRAINT analyses_dossier_instruction_fkey FOREIGN KEY (dossier_instruction) REFERENCES dossier_instruction(dossier_instruction),
    ADD CONSTRAINT analyses_acc_derogation_scda_fkey FOREIGN KEY (acc_derogation_scda) REFERENCES derogation_scda(derogation_scda),
    ADD CONSTRAINT analyses_dossier_instruction UNIQUE (dossier_instruction);
DROP TABLE donnee_technique;

--
-- START / Gestion des courriers sortants
--

-- Création de la table courrier_type_categorie
CREATE TABLE courrier_type_categorie (
    courrier_type_categorie integer NOT NULL,
    code character varying(7),
    libelle character varying(100),
    description text,
    om_validite_debut date,
    om_validite_fin date
);
-- Commentaires
COMMENT ON TABLE courrier_type_categorie IS 'Liste des catégories des types de courrier, cette table ne sera pas paramétrable.';
COMMENT ON COLUMN courrier_type_categorie.courrier_type_categorie IS 'Identifiant unique';
COMMENT ON COLUMN courrier_type_categorie.code IS 'Code de la catégorie du type de courrier.';
COMMENT ON COLUMN courrier_type_categorie.libelle IS 'Libellé de la catégorie du type de courrier.';
COMMENT ON COLUMN courrier_type_categorie.description IS 'Description de la catégorie du type de courrier.';
COMMENT ON COLUMN courrier_type_categorie.om_validite_debut IS 'Date de début de validité de la catégorie du type de courrier.';
COMMENT ON COLUMN courrier_type_categorie.om_validite_fin IS 'Date de fin de validité de la catégorie du type de courrier.';
-- Contraintes
ALTER TABLE ONLY courrier_type_categorie
    ADD CONSTRAINT courrier_type_categorie_pkey PRIMARY KEY (courrier_type_categorie);
-- Séquences
CREATE SEQUENCE courrier_type_categorie_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER SEQUENCE courrier_type_categorie_seq OWNED BY courrier_type_categorie.courrier_type_categorie;

-- Modification de la table courrier_type
ALTER TABLE courrier_type ADD COLUMN courrier_type_categorie integer;
ALTER TABLE courrier_type ADD COLUMN service integer;
-- Commentaires
COMMENT ON COLUMN courrier_type.courrier_type_categorie IS 'Catégorie du type de courrier.';
COMMENT ON COLUMN courrier_type.service IS 'Service du type de courrier.';
-- Contraintes
ALTER TABLE ONLY courrier_type
    ADD CONSTRAINT courrier_type_courrier_type_categorie_fkey FOREIGN KEY (courrier_type_categorie) REFERENCES courrier_type_categorie(courrier_type_categorie);
ALTER TABLE ONLY courrier_type
    ADD CONSTRAINT courrier_type_service_fkey FOREIGN KEY (service) REFERENCES service(service);

-- Modification de la table modele_edition
ALTER TABLE modele_edition ADD COLUMN courrier_type integer;
-- Commentaire
COMMENT ON COLUMN modele_edition.courrier_type IS 'Type de courrier pouvant avoir le modèle d''édition.';
-- Contraintes
ALTER TABLE ONLY modele_edition
    ADD CONSTRAINT modele_edition_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);

-- Modification de la table courrier
ALTER TABLE courrier DROP CONSTRAINT courrier_courrier_param_fkey;
ALTER TABLE courrier DROP COLUMN courrier_param;
ALTER TABLE courrier ADD COLUMN courrier_type integer;
-- Commentaires
COMMENT ON COLUMN courrier.courrier_type IS 'Type du courrier, permet de filtrer les modèles d''éditions à sélectionner .';
-- Contraintes
ALTER TABLE ONLY courrier
    ADD CONSTRAINT courrier_courrier_type_fkey FOREIGN KEY (courrier_type) REFERENCES courrier_type(courrier_type);

-- Suppression de la table courrier_param
DROP TABLE courrier_param;

--
-- END / Gestion des courriers sortants
--

----
-- BEGIN  / OM REQUETE
--
-- Ajout de champs pour la gestion objet,
-- dont un not null d'où gestion de l'existant.
-- Puis ajout nouvelle requête objet.
----

-- Modification de la structure
ALTER TABLE om_requete
    ADD type character varying(200) NULL,
    ADD classe character varying(200) NULL,
    ADD methode character varying(200) NULL;
COMMENT ON COLUMN om_requete.type IS 'Requête SQL ou objet ?';
COMMENT ON COLUMN om_requete.classe IS 'Nom de(s) la classe(s) contenant la méthode';
COMMENT ON COLUMN om_requete.methode IS 'Méthode (de la première classe si plusieurs définies) fournissant les champs de fusion. Si non spécifiée appel à une méthode générique';
-- Modification des données pour respect de la nouvelle contrainte
UPDATE om_requete SET type = 'sql';
-- Ajout de la nouvelle contrainte
ALTER TABLE om_requete
    ALTER type SET NOT NULL;

----
-- END  / OM REQUETE
----

--
INSERT INTO om_requete 
(om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) 
VALUES 
(nextval('om_requete_seq'), 'reunion', 'Tous les champs de fusion spécifiques aux réunions', NULL, '', '', 'objet', 'reunion', '');
--
INSERT INTO om_requete 
(om_requete, code, libelle, description, requete, merge_fields, type, classe, methode) 
VALUES 
(nextval('om_requete_seq'), 'dossier_instruction_reunion', 'Tous les champs de fusion spécifiques aux demandes de passage en réunion', NULL, '', '', 'objet', 'dossier_instruction_reunion', '');
--
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte) 
VALUES (nextval('om_lettretype_seq'), 1, 'reunion_ordre_du_jour', 'Réunion - Ordre du jour', true, 'P', 'A4', NULL, 10, 10, '<p>ORDRE DU JOUR</p>', 109, 16, 0, 10, '0', '<p>...</p>', 
    (SELECT om_requete FROM om_requete WHERE om_requete.code='reunion'), 
    10, 10, 10, 10, 'helvetica', '#000000');
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte) 
VALUES (nextval('om_lettretype_seq'), 1, 'reunion_compte_rendu', 'Réunion - Compte rendu', true, 'P', 'A4', NULL, 10, 10, '<p>COMPTE RENDU</p>', 109, 16, 0, 10, '0', '<p>...</p>', 
    (SELECT om_requete FROM om_requete WHERE om_requete.code='reunion'), 
    10, 10, 10, 10, 'helvetica', '#000000');
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte) 
VALUES (nextval('om_lettretype_seq'), 1, 'reunion_feuille_de_presence', 'Réunion - Feuille de présence', true, 'P', 'A4', NULL, 10, 10, '<p>FEUILLE DE PRÉSENCE</p>', 109, 16, 0, 10, '0', '<p>...</p>', 
    (SELECT om_requete FROM om_requete WHERE om_requete.code='reunion'), 
    10, 10, 10, 10, 'helvetica', '#000000');
INSERT INTO om_lettretype (om_lettretype, om_collectivite, id, libelle, actif, orientation, format, logo, logoleft, logotop, titre_om_htmletat, titreleft, titretop, titrelargeur, titrehauteur, titrebordure, corps_om_htmletatex, om_sql, margeleft, margetop, margeright, margebottom, se_font, se_couleurtexte) 
VALUES (nextval('om_lettretype_seq'), 1, 'reunion_compte_rendu_avis', 'Réunion - Compte rendu d''avis', true, 'P', 'A4', NULL, 10, 10, '<p>COMPTE RENDU D''AVIS</p>', 109, 16, 0, 10, '0', '<p>...</p>', 
    (SELECT om_requete FROM om_requete WHERE om_requete.code='dossier_instruction_reunion'), 
    10, 10, 10, 10, 'helvetica', '#000000');
--
INSERT INTO courrier_type_categorie (courrier_type_categorie, code, libelle, description, om_validite_debut, om_validite_fin) 
VALUES (nextval('courrier_type_categorie_seq'), 'REU', 'Documents générés liés aux Réunions', '', NULL, NULL);
--
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) 
VALUES (nextval('courrier_type_seq'), 'REU_CRG', 'Réunion - Compte rendu', '', NULL, NULL, 
    (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE courrier_type_categorie.code='REU'), 
    NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) 
VALUES (nextval('courrier_type_seq'), 'REU_ODJ', 'Réunion - Ordre du jour', '', NULL, NULL, 
    (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE courrier_type_categorie.code='REU'), 
    NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) 
VALUES (nextval('courrier_type_seq'), 'REU_FDP', 'Réunion - Feuille de présence', '', NULL, NULL, 
    (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE courrier_type_categorie.code='REU'), 
    NULL);
INSERT INTO courrier_type (courrier_type, code, libelle, description, om_validite_debut, om_validite_fin, courrier_type_categorie, service) 
VALUES (nextval('courrier_type_seq'), 'REU_CRA', 'Réunion - Compte rendu d''avis', '', NULL, NULL, 
    (SELECT courrier_type_categorie FROM courrier_type_categorie WHERE courrier_type_categorie.code='REU'), 
    NULL);
--
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) 
VALUES (nextval('modele_edition_seq'), 'Réunion - Compte rendu', '', 
    (SELECT om_lettretype FROM om_lettretype WHERE om_lettretype.id='reunion_compte_rendu'), 
    NULL, NULL, NULL, NULL, 
    (SELECT courrier_type FROM courrier_type WHERE courrier_type.code='REU_CRG'));
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) 
VALUES (nextval('modele_edition_seq'), 'Réunion - Ordre du jour', '', 
    (SELECT om_lettretype FROM om_lettretype WHERE om_lettretype.id='reunion_ordre_du_jour'),
    NULL, NULL, NULL, NULL, 
    (SELECT courrier_type FROM courrier_type WHERE courrier_type.code='REU_ODJ'));
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) 
VALUES (nextval('modele_edition_seq'), 'Réunion - Compte rendu d''avis', '', 
    (SELECT om_lettretype FROM om_lettretype WHERE om_lettretype.id='reunion_compte_rendu_avis'),
    NULL, NULL, NULL, NULL, 
    (SELECT courrier_type FROM courrier_type WHERE courrier_type.code='REU_CRA'));
INSERT INTO modele_edition (modele_edition, libelle, description, om_lettretype, om_etat, om_validite_debut, om_validite_fin, code, courrier_type) 
VALUES (nextval('modele_edition_seq'), 'Réunion - Feuille de présence', '', 
    (SELECT om_lettretype FROM om_lettretype WHERE om_lettretype.id='reunion_feuille_de_presence'),
    NULL, NULL, NULL, NULL, 
    (SELECT courrier_type FROM courrier_type WHERE courrier_type.code='REU_FDP'));



INSERT INTO etablissement_type (etablissement_type, libelle, description, om_validite_debut, om_validite_fin) VALUES (nextval('etablissement_type_seq'), 'IGH', 'Immeuble de grande hauteur', NULL, NULL);


INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (1, 'FAV', 'FAVORABLE', 'Description', 2, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (2, 'DEF', 'DEFAVORABLE', 'Description', 2, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (3, 'DIF', 'DIFFERE', 'Description', 2, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (4, 'SAV', 'SANS AVIS', 'Description', 2, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (5, 'ARE', 'A REVOIR', 'Description', 2, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (6, 'FAV', 'FAVORABLE', 'Description', 1, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (7, 'DEF', 'DEFAVORABLE', 'Description', 1, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (8, 'AJO', 'AJOURNE', 'Description', 1, NULL, NULL);
INSERT INTO reunion_avis (reunion_avis, code, libelle, description, service, om_validite_debut, om_validite_fin) VALUES (9, 'SAV', 'SANS AVIS', 'Description', 1, NULL, NULL);

-- Ajoute un type pour les contacts
INSERT INTO contact_type (contact_type, libelle, code, description, om_validite_debut, om_validite_fin)
VALUES (nextval('contact_type_seq'), 'Institutionnel', 'INST', NULL, NULL, NULL);
