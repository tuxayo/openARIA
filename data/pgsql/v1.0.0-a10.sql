--------------------------------------------------------------------------------
-- Script de mise à jour vers la version v1.0.0-a10
--
-- XXX Ce fichier doit être renommé en v1.0.0-a10.sql au moment de la release
--
-- @package openaria
-- @version SVN : $Id$
--------------------------------------------------------------------------------

----
-- BEGIN / Mise en cohérence de l'IHM des checkboxs avec la base de données
----
-- Gestion des valeurs existantes
UPDATE etablissement 
    SET    npai = false
    WHERE  npai IS NULL;
UPDATE etablissement 
    SET    autorite_police_encours = false
    WHERE  autorite_police_encours IS NULL;
UPDATE etablissement 
    SET    si_locaux_sommeil = false
    WHERE  si_locaux_sommeil IS NULL;
-- Champs obligatoires et faux par défaut
ALTER TABLE etablissement
    ALTER npai SET DEFAULT 'f',
    ALTER npai SET NOT NULL,
    ALTER autorite_police_encours SET DEFAULT 'f',
    ALTER autorite_police_encours SET NOT NULL,
    ALTER si_locaux_sommeil SET DEFAULT 'f',
    ALTER si_locaux_sommeil SET NOT NULL;
----
-- END / Mise en cohérence de l'IHM des checkboxs avec la base de données
----