-------------------------------------------------------------------------------
-------------------------------------------------------------------------------
--
-- Création de la matrice des permissions
--
-------------------------------------------------------------------------------
-------------------------------------------------------------------------------

-- On vide la table om_droit
DELETE FROM om_droit;

--
-- Profil 'ADMINISTRATEUR'
--
--
-- Aucune permission pour l'administrateur
--

\i init_parametrage_permissions_profil_cadre_si.sql
\i init_parametrage_permissions_profil_cadre_acc.sql
\i init_parametrage_permissions_profil_technicien_si.sql
\i init_parametrage_permissions_profil_technicien_acc.sql
\i init_parametrage_permissions_profil_secretaire_si.sql
\i init_parametrage_permissions_profil_secretaire_acc.sql

