<?php
/**
 * Ce fichier permet de déclarer la classe MessagesManager, qui effectue les
 * traitements pour la ressource 'messages'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Inclusion de la classe de base MetierManager
require_once("./metier/metiermanager.php");

// Inclusion de la classe métier dossier_message
include_once('../obj/dossier_coordination_message.class.php');

/**
 * Cette classe hérite de la classe MetierManager. Elle permet d'effectuer des
 * traitements pour la ressource 'messages'.
 */
class MessagesManager extends MetierManager {

    /**
     *
     */
    var $config_messages_in = null;

    /**
     * Cette méthode permet de récupérer le paramétrage des différents
     * messages entrants autorisés.
     *
     * @return mixed
     */
    function get_config_messages_in($mode = null, $type = null) {

        //
        if ($this->config_messages_in === null) {
            //
            require_once "../obj/interface_referentiel_ads.class.php";
            $interface_referentiel_ads = new interface_referentiel_ads();
            $config = $interface_referentiel_ads->get_config_messages_in();
            //
            $this->config_messages_in = $config;
        }

        //
        if ($mode === null) {
            return $this->config_messages_in;
        }

        //
        if ($mode === "list_types") {
            $list_types = array();
            foreach ($this->config_messages_in as $key => $value) {
                $list_types[] = $value["type"];
            }
            return $list_types;
        }

        //
        if ($mode === "content_fields") {
            foreach ($this->config_messages_in as $key => $value) {
                if ($value["type"] === $type && isset($value["content_fields"])) {
                    return $value["content_fields"];
                }
            }
        }

        //
        if ($mode === "treatment") {
            foreach ($this->config_messages_in as $key => $value) {
                if ($value["type"] === $type && isset($value["treatment"])) {
                    return $value["treatment"];
                }
            }
        }

        //
        return null;
    }

    /**
     * Cette méthode permet de gérer le fonctionnement de base de toutes les
     * méthodes métiers de la classe MessagesManager
     * 
     * @param array $data Les données reçues en format d'un tableau associative.
     */
    public function run($data) {

        /**
         * Vérification du type de message
         */
        // Vérification de la validité du type de message
        // Si le type dans le message ne correspond pas à un type disponible
        // alors on ajoute un message d'informations et on retourne un
        // résultat d'erreur
        if (in_array($data["type"], $this->get_config_messages_in("list_types")) !== true) {
            $this->setMessage("Le type de message n'est pas correct.");
            return $this->BAD_DATA;
        }

        /**
         * Vérification du contenu du message
         */
        //
        $contenu = 'dossier_instruction : '.$data["dossier_instruction"].'
';
        $content_fields = $this->get_config_messages_in("content_fields", $data["type"]);
        //
        if ($content_fields != null) {
            //
            if (is_array($data["contenu"]) !== true) {
                $this->setMessage("Le contenu du message n'est pas correct.");
                return $this->BAD_DATA;
            }
            //
            foreach ($data["contenu"] as $key => $value) {
                if (in_array($key, array_keys($content_fields)) !== true) {
                    $this->setMessage("Le contenu du message n'est pas correct.");
                    return $this->BAD_DATA;
                }
            }

            // Vérification de la validité du contenu en fonction du paramètre
            // $contenu_val_verif et formatage du contenu
            $valid_contenu = true;
            // On boucle sur chaque champs à vérifier
            foreach ($content_fields as $contenu_str => $possible_vals) {
                // On récupère la valeur
                $value = "";
                if (isset($data['contenu'][$contenu_str])) {
                    $value = $data['contenu'][$contenu_str];
                }
                // Si la valeur est vide alors on sort de la boucle
                if (empty($value)) {
                    $valid_contenu = false;
                    break;
                }
                // Si la valeur n'est pas dans les valeurs possible et que la valeur
                // possible n'est pas nulle alors on sort de la boucle
                if ($possible_vals != null
                    && !in_array(strtolower($value), $possible_vals)) {
                    $valid_contenu = false;
                    break;
                }
                // Formatage du contenu
                $contenu .= $contenu_str.' : '.$value.'
'; // il faut que cette ligne soit comme ça pour que le \n soit ajouté à la fin
        }

            // Si un des éléments du contenu n'est pas valide alors on ajoute un
            // message d'informations et on retourne un résultat d'erreur
            if ($valid_contenu === false) {
                $this->setMessage("Le contenu du message n'est pas correct.");
                return $this->BAD_DATA;
            }
        }

        /**
         * Date
         */
        // Vérification de la validité de la date
        // Si le format de la date transmise dans la requête n'est pas correct
        // alors on ajoute un message d'informations et on retourne un résultat
        // d'erreur
        // Important : $date_db est passé par référence et est modifié dans la
        // méthode timestampValide()
        $date_db = null;
        if (!$this->timestampValide($data['date'], $date_db, true)) {
            $this->setMessage("La date n'est pas correcte.");
            return $this->BAD_DATA;
        }

        //
        $dcs = array(null);

        /**
         * ATTACHMULTI
         */
        $treatment = $this->get_config_messages_in("treatment", $data["type"]);
        if ($treatment["mode"] == "attachmulti") {
            //
            $inst_util_dcm = new dossier_coordination_message(0);
            $di_ads = $inst_util_dcm->get_dossier_instruction_ads_from_contenu($contenu);
            if ($di_ads === null) {
                $this->setMessage("Impossible de récupérer le dossier d'instruction ADS depuis le contenu.");
                return $this->BAD_DATA;
            }
            //
            $dossier_coordination_type_code = null;
            if (isset($params["treatment"]["dossier_coordination_type_code"])) {
                $dossier_coordination_type_code = $params["treatment"]["dossier_coordination_type_code"];
            }
            //
            include_once('../obj/dossier_coordination.class.php');
            $inst_dc = new dossier_coordination(0);
            $ret = $inst_dc->get_dossier_coordination_for_dossier_ads(
                $di_ads,
                $dossier_coordination_type_code
            );
            if ($ret === 0) {
                $this->setMessage("Aucun dossier.");
                return $this->BAD_DATA;
            } elseif (is_array($ret) === false) {
                $dcs = array($ret, );
            } else {
                $dcs = $ret;
            }
        }

        //
        $this->db->autoCommit(false);
        foreach ($dcs as $value) {
            /**
             *
             */
            $dcm = array();
            // Affectation de l'identifiant du message (clé automatique)
            $dcm['dossier_coordination_message'] = 0;
            //
            $dcm['categorie'] = 'entrant';
            $dcm["type"] = $data["type"];
            $dcm['emetteur'] = $data["emetteur"];
            //
            $dcm["si_cadre_lu"] = false;
            $dcm["si_technicien_lu"] = false;
            $dcm["si_mode_lecture"] = 'mode0';
            $dcm["acc_cadre_lu"] = false;
            $dcm["acc_technicien_lu"] = false;
            $dcm["acc_mode_lecture"] = 'mode0';
            // champ date_emission de la table dans le format correct
            $dcm['date_emission'] = $date_db;
            // Affectation du contenu reformaté dans la variable $dcm['contenu']
            $dcm['contenu'] = $contenu;
            $dcm['contenu_json'] = json_encode($data);
            $dcm['dossier_coordination'] = $value;

            /**
             *
             */
            // On instancie la message qui va être créé par la requête
            $inst_dcm = new dossier_coordination_message("]");
            // Si aucune erreur n'a été rencontrée alors on appelle la méthode
            // ajouter pour insérer le message et on retourne le résultat de
            // la méthode ajouter
            // essai d'ajout des donnees dans la base de donnees
            $ret = $inst_dcm->ajouter($dcm, $this->db, $this->DEBUG);
            if ($ret === false) {
                //
                $inst_dcm->undoValidation();
                //
                if (isset($inst_dcm->msg)
                    && !empty($inst_dcm->msg)) {
                    //
                    $this->setMessage($this->filtreBalisesHtml($inst_dcm->msg));
                    return $this->KO;
                }
                $this->setMessage("Echec du message '".$dcm["type"]."'");
                return $this->KO;
            }
            $inst_dcm->__destruct();
        }

        //
        $this->db->commit();
        //
        $this->setMessage("Insertion du message '".$dcm["type"]."' OK.");
        return $this->OK;
    }

}

?>
