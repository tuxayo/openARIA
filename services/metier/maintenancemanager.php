<?php
/**
 * Ce fichier permet de déclarer la classe MaintenanceManager, qui effectue les
 * traitements pour la ressource 'maintenance'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

// Inclusion de la classe de base MetierManager
require_once("./metier/metiermanager.php");

/**
 * Cette classe hérite de la classe MetierManager. Elle permet d'effectuer des
 * traitements pour la ressource 'maintenance' qui ont vocation à être
 * déclenché de manière automatique ou par une application externe. Par
 * exemple, un cron permet chaque soir d'exécuter la synchronisation des
 * utilisateurs de l'application avec l'annuaire LDAP.
 */
class MaintenanceManager extends MetierManager {

    /**
     * @var mixed Ce tableau permet d'associer un module a une méthode,
     * le module (key) est celui reçu dans la requête REST, la méthode (value)
     * est la méthode de cette classe qui va exécuter le traitement
     */
    var $fptrs = array(
        'user' => 'synchronizeUsers',
        'voies' => 'synchronisationVoies',
        'import' => 'importDigitalizedDocuments',
        'purge' => 'purgeDigitalizedDocuments',
        'contraintes' => 'synchronisationContraintes',
        'geolocalisation' => 'geocoderTous',
        'messagesasync' => 'messagesasync',
        'ar_consultation' => 'triggerConsultationAR',
    );

    /**
     * Cette méthode permet de gérer l'appel aux différentes méthodes
     * correspondantes au module appelé.
     *
     * @param string $module Le module à exécuter reçu dans la requête
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */
    public function performMaintenance($module, $data) {

        // Si le module n'existe pas dans la liste des modules disponibles
        // alors on ajoute un message d'informations et on retourne un
        // un résultat d'erreur
        if (!in_array($module, array_keys($this->fptrs))) {
            $this->setMessage("Le module demandé n'existe pas");
            return $this->BAD_DATA;
        }

        // Si le module existe dans la liste des modules disponibles
        // alors on appelle la méthode en question et on retourne son résultat
        return call_user_func(array($this, $this->fptrs[$module]), $data);

    }

    /**
     * Cette méthode permet d'effectuer la synchronisation des utilisateurs
     * de l'application avec l'annuaire LDAP paramétré dans l'application.
     * 
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     *
     * @todo XXX Faire une getsion des erreurs dans om_application pour
     * permettre d'avoir un retour sur le nombre d'utilisateurs synchronisés
     */    
    public function synchronizeUsers($data) {

        // Initialisation de la synchronisation des utilisateurs LDAP
        $results = $this->f->initSynchronization();

        // Si l'initialisation ne se déroule pas correctement alors on retourne
        // un résultat d'erreur
        if (is_null($results) || $results == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }

        // Application du traitement de synchronisation
        $ret = $this->f->synchronizeUsers($results);

        // Si l'a synchronisation ne se déroule pas correctement alors on
        // retourne un résultat d'erreur
        if ($ret == false) {
            $this->setMessage("Erreur interne");
            return $this->KO;
        }

        // Si l'a synchronisation ne se déroule correctement alors on
        // retourne un résultat OK
        $this->setMessage("Synchronisation terminée.");
        return $this->OK;

    }

    /**
     * Cette méthode permet d'effectuer la synchronisation des voies du référentiel
     * vers la table des voies d'openaria.
     * 
     * @param mixed $data Les données reçues dans la requête
     * 
     * @return string Le résultat du traitement
     */ 
    public function synchronisationVoies($data) {

        require_once "../obj/voie_arrondissement_import.class.php";
        $import = new voie_arrondissement_import($this->f);

        // On désactive toutes les voies provenant du référentiel
        $nb_voies_archive = $import->disable_voies();
        if($nb_voies_archive === false) {
            $this->setMessage("Erreur lors de l'archivage.");
            return $this->KO;
        }
        // RAZ des liens entre voies et arrondissements
        if($import->delete_voie_arrondissement() === false) {
            $this->setMessage("Erreur lors de l'archivage.");
            return $this->KO;
        }

        $nb_voies_ajout = array();
        $nb_voies_maj = array();

        $fusion=array();
        $id_voies=array();
        // Récupération du fichier
        if (($handle = fopen($data["file_name"], "r")) !== false) {
            while (($row = fgetcsv($handle, 0, "|")) !== false) {
                if(!is_numeric($row[0])) {
                    continue;
                }
                $valImport=array();
                $valImport["numero_voie"] = $row[0];
                $valImport["code_rivoli"] = $row[1];
                $valImport["lib_voie"] = $row[2];
                $valImport["numero_arrondissement"] = $row[3];

                $voie = $import->is_voie_manuelle_exist($valImport["code_rivoli"], $valImport["lib_voie"]);

                if($import->is_voie_exist($valImport["numero_voie"])) {
                    // modification
                    if($import->update_voie($valImport) === false) {
                        $this->setMessage(
                            "Erreur lors de la mise a jour des voies existante"
                        );

                        return $this->KO;
                    }
                    $nb_voies_maj[$valImport["numero_voie"]] = 0;
                } elseif($voie !== false) {
                    // modification
                    if($import->update_voie_by_lib($voie, $valImport) === false) {
                        $this->setMessage(
                            "Erreur lors de la fusion des voies existante"
                        );

                        return $this->KO;
                    }
                    $fusion[$valImport["numero_voie"]] = 0;

                } else {
                    // insertion
                    if($import->insert_voie($valImport) === false) {
                        $this->setMessage("Erreur lors de l'ajout d'une nouvelle voie");
                        return $this->KO;
                    }
                    $nb_voies_ajout[$valImport["numero_voie"]] = 0;
                }
                // enregistrement des id à ne pas supprimer
                $id_voies[] = $valImport["numero_voie"];
            }
        } else {
            $this->setMessage(
                 "Problème ouverture fichier : ".$data["file_name"]
            );
            return $this->KO;
        }
        $nb_voies_maj = array_diff($nb_voies_maj, $nb_voies_ajout);
        // Si l'a synchronisation ne se déroule correctement alors on
        // retourne un résultat OK
        $this->setMessage(
            "Synchronisation terminée : ".
            count($nb_voies_ajout)." ajout(s) - ".
            count($nb_voies_maj)." mise(s) à jour - ".
            count($fusion)." fusionnée(s) - ".
            ($nb_voies_archive-count($nb_voies_maj))." archivée(s)"



            );
        return $this->OK;

    }
    
    /**
     * Cette méthode permet de faire l'importation des documents scannés
     * @param  string $data données envoyé en paramètre
     */
    public function importDigitalizedDocuments($data) {
        //Instance classe DigitalizedDocument
        require_once("../obj/digitalizedDocument.class.php");
        $digitalizedDocument = new DigitalizedDocument($this->f);
        //On vérifie que le service a été passé en paramètre
        $service =  isset($data["service"])?$data["service"]:"";
        if ($service===""&&$service!=="ACC"&&$service!=="SI") {
            //Message erreur
            $this->setMessage(_("Le nom du service n'a pas été fourni"));
            return $this->KO;
        }
        //Chemin des dossiers source et destination
        $path = $data;
        //Si deux données sont présentes dans $data
        if (!empty($path)) {
            //Dossier source
            $pathSrc = $this->getPathScan($path, 'Todo', $service);
            // On teste qu'aucune erreur ne s'est produite
            if (!$pathSrc){
                return $this->KO;
            }
            $pathDes = $this->getPathScan($path, 'Done', $service);
            // On teste qu'aucune erreur ne s'est produite
            if (!$pathSrc){
                return $this->KO;
            } 
        } else {

            //Si le répertoire de numérisation n'est pas configuré
            if (!isset($this->f->config["digitalization_folder"])) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                return $this->KO;
            }

            $pathSrc = $this->f->config['digitalization_folder'].'Todo/';
            $pathDes = $this->f->config['digitalization_folder'].'Done/';
        }
        //
        $repo = array_diff(scandir($pathSrc), array(".", "..") );
        $importDone = false;
        $importErrorFilename = array();

        //Si le dossier est ouvert
        if (is_array($repo)&&count($repo)>0) {
            //Importation des documents
            $import = $digitalizedDocument->run_import($pathSrc.'/', $pathDes.'/', $service);
            //Si  une importation a été faite
            if ($import === true) {
                $importDone = true;
            }
            //Si certain des fichiers ne se sont pas importés
            if (count($digitalizedDocument->filenameError) > 0){
                //
                $importErrorFilename = array_merge($importErrorFilename, $digitalizedDocument->filenameError);
                unset($digitalizedDocument->filenameError);
                $digitalizedDocument->filenameError = array();
            }
        }
        
        //Si des fichiers ne se sont pas importés à cause d'une erreur
        if (count($importErrorFilename)>0){
            $importErrorFilename = sprintf(_("Liste des fichiers en erreur : %s"),implode(',',$importErrorFilename));
        }

        //Message de retour
        ($importDone) ?
            //Il y avait des documents à traiter 
            $this->setMessage(_("Tous les documents ont ete traites").
                ((!is_array($importErrorFilename) && $importErrorFilename!= '')?
					"\n".$importErrorFilename:
					"")) :
            //Il n'y avait aucun document à traiter
            $this->setMessage(_("Aucun document a traiter")) ;
        return $this->OK;
    }
    
    /**
     * Cette méthode permet de faire la purge des documents numérisés
     * 
     * @param   string $data données envoyés en paramètre
     * 
     * @return  string
     */
    public function purgeDigitalizedDocuments($data) {
        //Instance classe DigitalizedDocument
        require_once("../obj/digitalizedDocument.class.php");
        $digitalizedDocument = new DigitalizedDocument($this->f);
        //Dossier à purger
        $path = isset($data["dossier"])?$data["dossier"]:"";
        //Si aucun dossier n'est fourni, on utilise le dossier de configuration
        if($path==""){
            //Si le répertoire de numérisation n'est pas configuré
            if (!isset($this->f->config["digitalization_folder"])) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                return $this->KO;
            }
            //On vérifie que le service a été passé en paramètre
            $service =  isset($data["service"])?$data["service"]:"";
            if ($service===""&&$service!=="ACC"&&$service!=="SI") {
                //Message erreur
                $this->setMessage(_("Le nom du service n'a pas été fourni"));
                return $this->KO;
            }
            //
            $path = $this->f->config['digitalization_folder'].$service.'/Done/';
            if (!is_dir($path)) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
                return $this->KO;
            }
        }
        //Si le dossier n'existe pas, on retourne une erreur
        elseif (!is_dir($path)) {
            //Message erreur
            $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
            return $this->KO;
        }
        //Nombre de jour à soustraite à la date du jour
        $nbDay = isset($data["nombre_de_jour"])?$data["nombre_de_jour"]:"";
        //Vérifie que $nbDay soit un entier
        if (!is_int($nbDay)) {
            $nbDay = null;
        }
        //
        $repo = array_diff(scandir($path), array(".", ".."));
        //Si le dossier contient des documents
        if (is_array($repo)&&count($repo)>0) {
            // On purge les documents
            $purge = $digitalizedDocument->run_purge($path.'/', $nbDay);
            //Si la purge n'a pas était faite
            if (!$purge) {
                //Message erreur
                $this->setMessage("Aucun document a traiter");
                return $this->OK;
            }
            //Retourne OK
            $this->setMessage(_("Tous les documents ont ete traites"));
            return $this->OK;
        }
        //Retourne erreur
        $this->setMessage(_("Aucun document a traiter"));
        return $this->OK;
    }
    
    /**
     * Retourne le chamin du dossier ou false si une erreur s'est produite
     * 
     * @param array $data Le tableau associatif de données données au webservice
     * @param string $folderName Nom du dossier
     * 
     * @return mixed
     */
    function getPathScan($data, $folderName, $service){
        //On test si des données sont contenues dans $data
        $path = isset($data[$folderName])?$data[$folderName]:"";
        //Aucun chemin n'est fourni pour le dossier source, on utilise la
        //configuration
        if($path===""){
            //Si le répertoire de numérisation n'est pas configuré
            if (!isset($this->f->config["digitalization_folder"])) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation n'est pas configure"));
                return false;
            }
            //
            $path = $this->f->config['digitalization_folder'].$service.'/'.$folderName.'/';
            if (!is_dir($path)) {
                //Message erreur
                $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
                return false;
            }
        }
        //Si le dossier n'existe pas, on retourne une erreur
        elseif (!is_dir($path)) {
            //Message erreur
            $this->setMessage(_("Le repertoire de numerisation fourni n'existe pas"));
            return false;
        }
        
        return $path;
    }

    /**
     * Synchronisation des contraintes
     *
     * Cette méthode permet de récupérer les contraintes du référentiel SIG
     * pour mettre à jour les contraintes paramétrées d'openARIA :
     *
     * - ajout de nouvelles
     * - modification des existantes
     * - archivage des obsolètes
     */
    public function synchronisationContraintes() {

        // Si l'option du SIG est désactivée
        if ($this->f->is_option_sig_enabled() === false) {
            //
            $this->setMessage(_("L'option SIG n'est pas activée."));
            return $this->OK;
        }
        //
        require_once('../obj/contrainte_synchronisation.class.php');
        $sync = new contrainte_synchronisation(']');
        $sync->begin_treatment_with_transaction(__METHOD__);
        try {
            $sync->synchroniser();
        } catch (treatment_exception $e) {
            $this->setMessage($e->getMessage());
            $sync->end_treatment_with_transaction(__METHOD__, false);
            return $this->KO;
        }
        //
        $this->setMessage($sync->msg);
        $sync->end_treatment_with_transaction(__METHOD__);
        return $this->OK;
    }

    /**
     * Géolocalisation
     *
     * Cette méthode permet de géolocaliser tous les dossiers de coordination
     * et établissements qui peuvent l'être automatiquement.
     */
    public function geocoderTous() {

        // Si l'option du SIG est désactivée
        if ($this->f->is_option_sig_enabled() === false) {
            //
            $this->setMessage(_("L'option SIG n'est pas activée."));
            return $this->OK;
        }
        //
        require_once ('../obj/dossier_coordination_geocoder_tous.class.php');
        $geoloc = new dossier_coordination_geocoder_tous(']');
        $geoloc->begin_treatment(__METHOD__);
        // Le traitement est toujours valide
        $geoloc->geocoder_tous();
        $this->setMessage($geoloc->msg);
        $geoloc->end_treatment(__METHOD__, true);
        return $this->OK;
    }

    /**
     * messagesasync
     *
     * Cette méthode permet d'exécuter le traitement des messages asynchrones.
     * 
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */    
    public function messagesasync($data) {
        //
        require_once "../obj/dossier_coordination_message.class.php";
        $inst_dcm = new dossier_coordination_message(']');
        //
        $ret = $inst_dcm->treat_all_messages_async();
        $this->setMessage($inst_dcm->msg);
        if ($ret !== true) {
            return $this->KO;
        }
        return $this->OK;
    }

    /**
     * triggerConsultationAR
     *
     * Cette méthode permet d'exécuter le traitement des messages de consultations.
     * 
     * @param mixed $data Les données reçues dans la requête
     * @return string Le résultat du traitement
     */    
    public function triggerConsultationAR($data) {
        //
        require_once "../obj/dossier_coordination_message.class.php";
        $inst_dcm = new dossier_coordination_message(0);
        //
        $ret = $inst_dcm->treat_all_consultation();
        $this->setMessage($inst_dcm->msg);
        if ($ret !== true) {
            return $this->KO;
        }
        return $this->OK;
    }

}

?>
