<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = _("dossiers")." -> "._("DC")." -> "._("tous les dossiers");

// FROM 
$table = DB_PREFIXE."dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination as dossier_coordination_2
        ON dossier_coordination.dossier_coordination_parent=dossier_coordination_2.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type 
        ON dossier_coordination.dossier_coordination_type=dossier_coordination_type.dossier_coordination_type 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie 
        ON dossier_coordination.etablissement_categorie=etablissement_categorie.etablissement_categorie 
    LEFT JOIN ".DB_PREFIXE."etablissement_type 
        ON dossier_coordination.etablissement_type=etablissement_type.etablissement_type ";

// SELECT 
$champAffiche = array(
    'dossier_coordination.dossier_coordination as "'._("dossier_coordination").'"',
    'dossier_coordination.libelle as "'._("DC").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
    "case dossier_coordination.erp when 't' then 'Oui' else 'Non' end as \""._("erp")."\"",
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    "case dossier_coordination.a_qualifier when 't' then 'Oui' else 'Non' end as \""._("a_qualifier")."\"",
    "case dossier_coordination.dossier_cloture when 't' then 'Oui' else 'Non' end as \""._("dossier_cloture")."\"",
    );

// Champs de recherche en mode simple
$champRecherche= array(
    'dossier_coordination.libelle as "'._("DC").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
);

//
$tab_title = _("DC");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_contrainte_dossier_coordination',
    'contact_contexte_dossier_coordination',
    'dossier_coordination',
    'piece',
    'courrier',
    'autorite_police',
    //'dossier_coordination_parcelle',
    //'dossier_instruction',
    //'lien_dossier_coordination_contact',
    'dossier_coordination_message_contexte_dc',
);
$sousformulaire_parameters = array(
    "lien_contrainte_dossier_coordination" => array(
        "title" => _("Contraintes"),
        "href" => "../scr/form.php?obj=dossier_coordination&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".((isset($_GET['obj']))?$_GET['obj']:"")."&",
    ),
    "contact_contexte_dossier_coordination" => array(
        "title" => _("Contacts"),
    ),
    "autorite_police" => array(
        "title" => _("AP"),
    ),
    "dossier_coordination" => array(
        "title" => _("DC Fils"),
    ),
    "piece" => array(
        "title" => _("Documents entrants"),
        "href" => "../scr/form.php?obj=".(isset($obj) ? $obj : "dossier_coordination")."&action=21&idx=".((isset($idx))?$idx:"")."&",
    ),
    "courrier" => array(
        "title" => _("Documents generes"),
    ),
    "dossier_coordination_message_contexte_dc" => array(
        "title" => _("Messages"),
    ),
);

// Cache le bouton ajouter
// Une entrée de menu dédiée permet d'ajouter un DC
$tab_actions['corner']['ajouter'] = null;

// Affichage du bouton de redirection vers le SIG externe si configuré
// On n'affiche pas le lien de redirection dans les sous-formulaires
if ($f->getParameter('option_sig') == 'sig_externe'
    && $retourformulaire === '') {
    try {
        $inst_geoaria = $f->get_inst_geoaria();
        if ($inst_geoaria->is_localization_dc_enabled() === true) {
            $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => 'form.php?obj=dossier_coordination&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'._('Localiser').'">'._('Localiser').'</span>',
                'rights' => array('list' => array('dossier_coordination', 'dossier_coordination_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false,
            );
        }
    } catch (geoaria_exception $e) {
        //
    }
}

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Options pour les select de faux booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(_("choisir"), _("Oui"), _("Non"), ),
);

// Champs pour la recherche avancée
$champs['libelle'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('DC')
);
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('etablissement')
);
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => _('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
$champs['dossier_coordination_type'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'dossier_coordination_type',
    'type' => 'select',
    'libelle' => _('dossier_coordination_type')
);
$champs['date_demande'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'date_demande',
    'libelle' => _('date_demande'),
    'lib1'=> _("debut"),
    'lib2' => _("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['date_butoir'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'date_butoir',
    'libelle' => _('date_butoir'),
    'lib1'=> _("debut"),
    'lib2' => _("fin"),
    'type' => 'date',
    'taille' => 8,
    'where' => 'intervaldate'
);
$champs['erp'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'erp',
    'type' => 'select',
    'libelle' => _('erp'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['a_qualifier'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'a_qualifier',
    'type' => 'select',
    'libelle' => _('a_qualifier'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_cloture'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'dossier_cloture',
    'type' => 'select',
    'libelle' => _('dossier_cloture'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_coordination_parent'] = array(
    'table' => 'dossier_coordination_2',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('dossier_coordination_parent')
);
$champs['etablissement_type'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_type',
    'type' => 'select',
    'libelle' => _('etablissement_type')
);
$champs['etablissement_categorie'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_categorie',
    'type' => 'select',
    'libelle' => _('etablissement_categorie')
);
$champs['etablissement_locaux_sommeil'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'etablissement_locaux_sommeil',
    'type' => 'select',
    'libelle' => _('locaux_a_sommeil'),
    "subtype" => "manualselect",
    "args" => $args,
);
$champs['dossier_ads'] = array(
    'table' => 'dossier_coordination',
    'colonne' => array('dossier_instruction_ads', 'dossier_autorisation_ads', ),
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('dossier ADS')
);
$champs['geolocalise'] = array(
    'table' => 'dossier_coordination',
    'colonne' => 'geolocalise',
    'type' => 'select',
    'libelle' => _('localise'),
    "subtype" => "manualselect",
    "args" => $args,
);

$tmp_advsearch_options = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_coordination'
);

if ($f->getParameter('option_sig') == 'sig_externe') {
    try {
        $inst_geoaria = $f->get_inst_geoaria();
        if ($inst_geoaria->is_localization_dc_enabled() === true) {
            $tmp_advsearch_options['export'] = array(
                'geoaria' => array(
                    'right' => 'dossier_coordination_geoaria',
                    'url' => '../scr/form.php?obj=dossier_coordination&action=31&idx=0',
                ),
            );
        }
    } catch (geoaria_exception $e) {
        //
    }
}

$options[] = $tmp_advsearch_options;

?>