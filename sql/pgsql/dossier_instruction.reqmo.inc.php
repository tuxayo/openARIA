<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=_('reqmo-libelle-dossier_instruction');
$reqmo['reqmo_libelle']=_('reqmo-libelle-dossier_instruction');
$ent=_('dossier_instruction');
$reqmo['sql']="select dossier_instruction, libelle, dossier_coordination, technicien, service, a_qualifier, incompletude, piece_attendue, description, notes, autorite_competente, dossier_cloture, prioritaire, statut, analyses, date_cloture, date_ouverture from ".DB_PREFIXE."dossier_instruction  order by [tri]";
$reqmo['tri']=array('dossier_instruction','libelle','dossier_coordination','technicien','service','a_qualifier','incompletude','piece_attendue','description','notes','autorite_competente','dossier_cloture','prioritaire','statut','analyses','date_cloture','date_ouverture');
?>