<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination_message.inc.php";

// Fil d'Ariane
$ent = _("dossiers")." -> "._("Messages")." -> "._("Tous les messages");

//
$champAffiche = array(
    'dossier_coordination_message.dossier_coordination_message as "'._("identifiant").'"',
    'to_char(dossier_coordination_message.date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as "'._("date").'"',
    'dossier_coordination_message.type as "'._("type").'"',
    'dossier_coordination_message.categorie as "'._("categorie").'"',
    'dossier_coordination_message.emetteur as "'._("emetteur").'"',
    'dossier_coordination.libelle as "'._("dc").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    $case_si_cadre_lu." as \""._("cadre si")."\"",
    $case_si_technicien_lu." as \""._("tech si")."\"",
    $case_acc_cadre_lu." as \""._("cadre acc")."\"",
    $case_acc_technicien_lu." as \""._("tech acc")."\"",
);

// Options
if (!isset($options)) {
    $options = array();
}
// Recherche avancée
$options[] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => array(
        'type' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'type',
            'type' => 'text',
            'taille' => 100,
            'libelle' => _('type'),
        ),
        'emetteur' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'emetteur',
            'type' => 'text',
            'taille' => 40,
            'libelle' => _('émetteur'),
        ),
        'categorie' => array(
            'table' => 'dossier_coordination_message',
            'colonne' => 'categorie',
            'type' => 'select',
            'libelle' => _('catégorie'),
            'subtype' => "manualselect",
            'args' => array(
                0 => array("", "interne", "entrant", "sortant", ),
                1 => array(_("Toutes"), "interne", "entrant", "sortant", ),
            ),
        ),
    ),
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_coordination_message'
);

?>
