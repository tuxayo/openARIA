<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_instruction.inc.php";

// Titre de la page
$ent = _("dossiers")." -> "._("DI")." -> "._("tous les dossiers");

// Complément pour le FROM
$table = DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."dossier_coordination_type
        ON dossier_coordination.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    LEFT JOIN ".DB_PREFIXE."dossier_type
        ON dossier_coordination_type.dossier_type = dossier_type.dossier_type
    LEFT JOIN ".DB_PREFIXE."autorite_competente 
        ON dossier_instruction.autorite_competente=autorite_competente.autorite_competente 
    LEFT JOIN ".DB_PREFIXE."service 
        ON dossier_instruction.service=service.service 
    LEFT JOIN ".DB_PREFIXE."acteur 
        ON dossier_instruction.technicien=acteur.acteur
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON dossier_coordination.etablissement=etablissement.etablissement
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
 ";

//
$tab_title = _("DI");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'analyses',
    'proces_verbal',
    'piece',
    'courrier',
    'dossier_instruction_reunion_contexte_di',
    'visite',
    'dossier_coordination_message_contexte_di',
);

//
$sousformulaire_parameters = array(
    "dossier_instruction_reunion_contexte_di" => array(
        "title" => _("reunions"),
    ),
    "analyses" => array(
        "title" => _("analyse"),
        "href" => "../scr/form.php?obj=dossier_instruction&action=7&idx=".((isset($idx))?$idx:"")."&",
        //"href" => "../scr/sousform.php?obj=analyses&action=4&objsf=".$obj.((isset($retourformulaire))?"&retourformulaire=".$retourformulaire:"")."&idxformulaire=".$idx."&retour=form&idx=]&",
    ),
    "proces_verbal" => array(
        "title" => _("PV"),
        "href" => "../scr/form.php?obj=dossier_instruction&action=9&idx=".((isset($idx))?$idx:"")."&",
    ),
    "piece" => array(
        "title" => _("Documents entrants"),
        "href" => "../scr/form.php?obj=".(isset($obj) ? $obj : "dossier_instruction")."&action=21&idx=".((isset($idx))?$idx:"")."&",
    ),
    "courrier" => array(
        "title" => _("Documents generes"),
    ),
    "visite" => array(
        "title" => _("Visites"),
    ),
    "dossier_coordination_message_contexte_di" => array(
        "title" => _("Messages"),
    ),
);

// SELECT 
$champAffiche = array(
    'dossier_instruction.dossier_instruction as "'._("id").'"',
    'dossier_instruction.libelle as "'._("DI").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'autorite_competente.code as "'._("autorite_competente").'"',
    'acteur.nom_prenom as "'._("technicien").'"',
    'CASE WHEN dossier_instruction.a_qualifier IS TRUE
        THEN \''._('Oui').'\'
        ELSE \''._('Non').'\'
    END as "'._("a_qualifier").'"',
    'CASE WHEN dossier_instruction.incompletude IS TRUE
        THEN \''._('Oui').'\'
        ELSE \''._('Non').'\'
    END as "'._("incompletude").'"',
    'CASE WHEN dossier_instruction.dossier_cloture IS TRUE
        THEN \''._('Oui').'\'
        ELSE \''._('Non').'\'
    END as "'._("dossier_cloture").'"',
    );

//
$champRecherche= array(
    'dossier_instruction.libelle as "'._("DI").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
);

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Options pour les select des booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(_("choisir"), _("Oui"), _("Non"), ),
);

//
$champs['dossier'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('DI')
);
//
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'libelle' => _('etablissement')
);

$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => _('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
$champs['autorite_competente'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'autorite_competente',
    'type' => 'select',
    'libelle' => _('autorite_competente')
);
// Si l'utilisateur n'a pas de service alors on lui autorise
// à filtrer par service
if (is_null($_SESSION["service"])) {
    //
    $champs['service'] = array(
        'table' => 'dossier_instruction',
        'colonne' => 'service',
        'type' => 'select',
        'libelle' => _('service')
    );
}
$champs['technicien'] = array(
    'table' => 'acteur',
    'colonne' => 'acteur',
    'type' => 'select',
    'subtype' => 'sqlselect',
    // QUERY liste des acteurs qui peuvent instruire un dossier
    'sql' => '
    SELECT 
        acteur,
        nom_prenom
    FROM
        '.DB_PREFIXE.'acteur
    WHERE
        (acteur.role=\'technicien\'
         OR acteur.role=\'cadre\')
        '.(!is_null($_SESSION["service"]) ? 'AND acteur.service=\''.$_SESSION["service"].'\'' : ''),
    'libelle' => _('technicien')
);
$champs['a_qualifier'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'a_qualifier',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('a_qualifier'),
    "args" => $args,
);
$champs['incompletude'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'incompletude',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('incompletude'),
    "args" => $args,
);
$champs['dossier_cloture'] = array(
    'table' => 'dossier_instruction',
    'colonne' => 'dossier_cloture',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('dossier_cloture'),
    "args" => $args,
);
$champs['dossier_ads'] = array(
    'table' => 'dossier_coordination',
    'colonne' => array('dossier_instruction_ads', 'dossier_autorisation_ads', ),
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('dossier ADS'),
);
$options["advsearch"] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'dossier_instruction'
);

// Il n'est pas possible d'ajouter un DI manuellement
$tab_actions['corner']['ajouter'] = null;

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe'
    && ($obj == 'dossier_instruction'
        || $obj == 'dossier_instruction_mes_plans'
        || $obj == 'dossier_instruction_tous_plans'
        || $obj == 'dossier_instruction_mes_visites'
        || $obj == 'dossier_instruction_tous_visites'
        || $obj == 'dossier_instruction_a_affecter'
        || $obj == 'dossier_instruction_a_qualifier')
    ) {
    $inst_geoaria = $f->get_inst_geoaria();
    if ($inst_geoaria->is_localization_dc_enabled() === true) {
        $tab_actions['left']["localiser-sig-externe"] = array(
            'lien' => 'form.php?obj=dossier_instruction&amp;action=30&amp;idx=',
            'id' => '',
            'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'._('Localiser').'">'._('Localiser').'</span>',
            'rights' => array('list' => array('dossier_coordination', 'dossier_coordination_consulter'), 'operator' => 'OR'),
            'ordre' => 20,
            'target' => "blank",
            'ajax' => false,
        );
    }
}

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
