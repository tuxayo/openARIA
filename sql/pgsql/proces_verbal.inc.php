<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/proces_verbal.inc.php";

// Supprime le bouton ajouter si on accède depuis le menu
if (isset($retourformulaire)
    && $retourformulaire == 'dossier_instruction'){
    $ent = "-> "._("proces_verbal");
}

// FROM
$table = DB_PREFIXE."proces_verbal
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON proces_verbal.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion 
        ON proces_verbal.dossier_instruction_reunion=dossier_instruction_reunion.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."reunion_type 
        ON dossier_instruction_reunion.reunion_type=reunion_type.reunion_type
    LEFT JOIN ".DB_PREFIXE."modele_edition 
        ON proces_verbal.modele_edition=modele_edition.modele_edition 
    LEFT JOIN ".DB_PREFIXE."signataire 
        ON proces_verbal.signataire=signataire.signataire ";

// SELECT
$champAffiche = array(
    'proces_verbal.proces_verbal as "'._("proces_verbal").'"',
    'proces_verbal.numero as "'._("numero").'"',
    'to_char(proces_verbal.date_redaction ,\'DD/MM/YYYY\') as "'._("date_redaction").'"',
    'concat(to_char(dossier_instruction_reunion.date_souhaitee,\'DD/MM/YYYY\'), \' - \', reunion_type.libelle) as "'._("demande de passage en reunion").'"',
    'concat(signataire.nom, \' \', signataire.prenom) as "'._("signataire").'"',
    "case proces_verbal.genere when 't' then '"._("genere")."' else '"._("ajoute")."' end as \""._("provenance")."\"",
);
//
$champNonAffiche = array(
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
);

$tri="ORDER BY proces_verbal.date_redaction DESC NULLS LAST";

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;

?>
