<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = _("etablissements")." -> "._("unites d'accessibilite");

//
$tab_title = _("UA");

// FROM
$table = DB_PREFIXE."etablissement_unite
    LEFT JOIN ".DB_PREFIXE."derogation_scda 
        ON etablissement_unite.acc_derogation_scda=derogation_scda.derogation_scda 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction 
        ON etablissement_unite.dossier_instruction=dossier_instruction.dossier_instruction 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON etablissement_unite.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."etablissement_unite as eu2
        ON etablissement_unite.etablissement_unite_lie=eu2.etablissement_unite
    LEFT JOIN ".DB_PREFIXE."arrondissement 
        ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
    LEFT JOIN ".DB_PREFIXE."voie 
        ON etablissement.adresse_voie=voie.voie 
 ";

//
$champAffiche_ua = array(
    'etablissement_unite.etablissement_unite as "'._("etablissement_unite").'"',
    'etablissement_unite.libelle as "'._("libelle").'"',
);

//
$champAffiche_infos_etab = array(
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
);

//
$champAffiche_cas1 = array(
    "CASE etablissement_unite.acc_handicap_auditif when 't' then 'Oui' when 'f' then 'Non' else '' end as \""._("acc. auditif")."\"",
    "case etablissement_unite.acc_handicap_mental when 't' then 'Oui' when 'f' then 'Non' else '' end as \""._("acc. mental")."\"",
    "case etablissement_unite.acc_handicap_physique when 't' then 'Oui' when 'f' then 'Non' else '' end as \""._("acc. physique")."\"",
    "case etablissement_unite.acc_handicap_visuel when 't' then 'Oui' when 'f' then 'Non' else '' end as \""._("acc. visuel")."\"",
);

//
$champAffiche_cas2 = array(
    "CONCAT(
        'auditif&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_auditif when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'mental&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_mental when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'physique&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_physique when 't' then 'Oui' when 'f' then 'Non' else ''  end, '<br/>',
        'visuel&nbsp;:&nbsp;', case etablissement_unite.acc_handicap_visuel when 't' then 'Oui' when 'f' then 'Non' else '' end
        )
    as \""._("accessible")."\"",
);

//
$champAffiche_etat = array(
    "CASE etablissement_unite.etat 
        when 'valide' then '"._("validé")."' 
        when 'enprojet' then '"._("en projet")."'
        else '-'
    end as \""._("etat")."\"",
);

//
$champAffiche_archive = array(
    "'Non' as \""._("archive")."\"",
);

//
$champAffiche_derogation = array(
    "'' as \""._("dérogation")."\"",
);

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_infos_etab,
    $champAffiche_cas2, 
    $champAffiche_etat
);

//
$champRecherche= array(
    'etablissement_unite.libelle as "'._("libelle").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
);

//
$selection = " WHERE etablissement_unite.archive IS FALSE ";

$retourformulaire = (isset($_GET['retourformulaire']) ? $_GET['retourformulaire'] : "");

// Supprime le bouton ajouter si on accède depuis le menu
if (!isset($retourformulaire)
    || $retourformulaire == null
    || $retourformulaire == ''){

    $tab_actions['corner']['ajouter'] = null;
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe'
    AND $retourformulaire != 'etablissement_tous'
    AND $retourformulaire != 'etablissement') {
    $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => 'form.php?obj=etablissement_unite&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'._('Localiser').'">'._('Localiser').'</span>',
                'rights' => array('list' => array('etablissement_unite', 'etablissement_unite_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false);
}

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
);


/**
 * OPTIONS
 */
if (!isset($options)) {
    $options = array();
}

/**
 * Recherche avancée
 */
// Options pour les select des booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(_("choisir"), _("Oui"), _("Non") ),
);
//
$args_etat = array(
    0 => array("", "enprojet", "valide", ),
    1 => array(_("choisir"), _("en projet"), _("validé"), ),
);
//
$champs = array();
//
$champs['libelle'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 30,
    'libelle' => _('libelle')
);
//
$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => array('libelle','code'),
    'type' => 'text',
    'taille' => 100,
    'libelle' => _('etablissement')
);
//
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('numero')
);
//
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('voie')
);
//
$champs['adresse_arrondissement'] = array(
    'table' => 'arrondissement',
    'colonne' => 'arrondissement',
    'libelle' => _('arrondissement'),
    'type' => 'select',
    'subtype' => 'sqlselect',
    'sql' => 'SELECT arrondissement, libelle FROM '.DB_PREFIXE.'arrondissement ORDER BY code::int' ,
);
//
$champs['etat'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'etat',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('etat'),
    "args" => $args_etat,
);
//
$champs['acc_handicap_auditif'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_auditif',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('acc_handicap_auditif'),
    "args" => $args,
);
//
$champs['acc_handicap_mental'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_mental',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('acc_handicap_mental'),
    "args" => $args,
);
//
$champs['acc_handicap_physique'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_physique',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('acc_handicap_physique'),
    "args" => $args,
);
//
$champs['acc_handicap_visuel'] = array(
    'table' => 'etablissement_unite',
    'colonne' => 'acc_handicap_visuel',
    'type' => 'select',
    "subtype" => "manualselect",
    'libelle' => _('acc_handicap_visuel'),
    "args" => $args,
);
//
$options["advsearch"] = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'etablissement_unite'
);

?>