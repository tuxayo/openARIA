<?php
/**
 * Script de paramétrage du listing des 'passages en réunion'.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/dossier_instruction_reunion.inc.php";

//
$ent = "-> "._("passage en reunion");

$table .= "
LEFT JOIN ".DB_PREFIXE."dossier_coordination
    ON dossier_instruction.dossier_coordination=dossier_coordination.dossier_coordination 
LEFT JOIN ".DB_PREFIXE."etablissement
    ON dossier_coordination.etablissement=etablissement.etablissement ";


$displayed_fields_begin = array(

);
$displayed_field_etablissement = 'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"';
$displayed_field_di = 'dossier_instruction.libelle as "'._("di").'"';

// SELECT 
$champAffiche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'._("dossier_instruction_reunion").'"',
    'to_char(dossier_instruction_reunion.date_souhaitee ,\'DD/MM/YYYY\') as "'._("date_souhaitee").'"',
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion_categorie.libelle as "'._("categorie").'"',
    $displayed_field_etablissement,
    $displayed_field_di,
    'reunion.code as "'._("reunion").'"',
    'to_char(reunion.date_reunion ,\'DD/MM/YYYY\') as "'._("date").'"',
    'dossier_instruction_reunion.ordre as "'._("ordre").'"',
    'reunion_avis0.libelle as "'._("avis").'"',
    );
//
$champRecherche = array(
    'dossier_instruction_reunion.dossier_instruction_reunion as "'._("dossier_instruction_reunion").'"',
    $displayed_field_di,
    'reunion_type.libelle as "'._("reunion_type").'"',
    'reunion_categorie.libelle as "'._("categorie").'"',
    'reunion.code as "'._("reunion").'"',
    'dossier_instruction_reunion.ordre as "'._("ordre").'"',
    'reunion_avis0.libelle as "'._("avis").'"',
    );

// Dans le contexte du DI, les colonnes 'etablissement' et 'di' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array($displayed_field_etablissement, $displayed_field_di, )
    );
}

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {

    // Instanciation du DI ou de sa surcharge
    require_once '../obj/'.$retourformulaire.'.class.php';
    $di = new $retourformulaire($idxformulaire);

    // Si service DI différent de celui de l'utilisateur
    if ($di->is_from_good_service() === false) {
        // Désactivation du bouton ajout du soustab
        $tab_actions['corner']['ajouter'] = null;
    }
}

?>
