<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/piece.inc.php');

// SELECT 
$champAffiche = array(
    'piece.piece as "'._("piece").'"',
    'piece.nom as "'._("nom").'"',
    'piece_type.libelle as "'._("piece_type").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'._("om_date_creation").'"',
    'to_char(piece.date_reception ,\'DD/MM/YYYY\') as "'._("date_reception").'"',
    'to_char(piece.date_emission ,\'DD/MM/YYYY\') as "'._("date_emission").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'piece_statut.libelle as "'._("piece_statut").'"',
    );

//
$tab_title = _("document entrant");

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {

    // Instanciation du DI ou de sa surcharge
    require_once '../obj/'.$retourformulaire.'.class.php';
    $di = new $retourformulaire($idxformulaire);

    // Si service DI différent de celui de l'utilisateur
    if ($di->is_from_good_service() === false) {
        // Désactivation du bouton ajout du soustab
        $tab_actions['corner']['ajouter'] = null;
    }
}

?>
