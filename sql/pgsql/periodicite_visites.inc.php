<?php
/**
 * Surcharge 'periodicite_visites'.
 *
 * L'objectif de la surcharge est :
 * - la modification des champs recherchables.
 *
 * @package openaria
 * @version SVN : $Id$ 
 */

// Inclusion du fichier GEN.
include '../gen/sql/pgsql/periodicite_visites.inc.php';

// On supprime la clé primaire des champs recherchables pour éviter la 
// confusion des utilisateurs.
$champRecherche = array(
    'periodicite_visites.periodicite as "'._("periodicite").'"',
    'etablissement_type.libelle as "'._("etablissement_type").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
);

// Actions en coin : update_all
$tab_actions['corner']['controlpanel'] = array(
    'lien' => '../scr/form.php?obj='.$obj.'&amp;action=99&idx=0',
    'id' => '&amp;advs_id='.$advs_id.'&amp;tricol='.$tricol.'&amp;valide='.$valide.'&amp;retour=tab',
    'lib' => sprintf(
        '<span class="om-icon om-icon-16 om-icon-fix loop-16" title="%s">%s</span>',
        _("Mettre à jour la périodicité des visites pour tous les établissements et dossiers de coordination concernés"),
        _("Visite périodique")
    ),
    'rights' => array('list' => array($obj, $obj.'_ajouter'), 'operator' => 'OR'),
    'ordre' => 20,
);

?>
