<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_coordination.form.inc.php";

//
$champs = array(
    "dossier_coordination.dossier_coordination",
    "dossier_coordination.etablissement",
    "dossier_coordination.libelle",
    "dossier_coordination_type",
    "date_demande",
    "date_butoir",
    "enjeu_erp",
    "enjeu_ads",
    "dossier_autorisation_ads",
    "dossier_instruction_ads",
    "a_qualifier",
    "dossier_coordination.etablissement_type",
    "etablissement_categorie",
    "array_to_string(
        array_agg(
            lien_dossier_coordination_etablissement_type.etablissement_type
            ORDER BY etablissement_type.libelle),
    ';') as etablissement_type_secondaire",
    "dossier_cloture",
    "contraintes_urba_om_html",
    "erp",
    "etablissement_locaux_sommeil",
    "references_cadastrales",
    "dossier_coordination_parent",
    "dossier_coordination.description",
    "dossier_instruction_secu",
    "dossier_instruction_acc",
    "autorite_police_encours",
    "geom_point",
    "geom_emprise",
    "geolocalise",
    "date_cloture",
    "interface_referentiel_ads",
    "depot_de_piece",
);
// En ajout et modification
if((isset($_GET["action"])&&($_GET["action"]==0||$_GET["action"]==1))||
    (isset($_GET["validation"]))){
    $champs = array(
        // Fieldset "Informations générales"
        "dossier_coordination.dossier_coordination",
        "dossier_coordination.libelle",
        "dossier_coordination_type",
        "dossier_coordination.description",
        "date_demande",
        "date_butoir",
        "date_cloture",
        "enjeu_erp",
        "enjeu_ads",
        "autorite_police_encours",
        "dossier_cloture",

        // Fieldset "Qualification"
        "a_qualifier",
        "dossier_instruction_secu",
        "'' as dossier_instruction_secu_lien",
        "dossier_instruction_acc",
        "'' as dossier_instruction_acc_lien",
        "etablissement",
        "dossier_coordination_parent",
        "'' as exploitant",
        "erp",
        "etablissement_locaux_sommeil",
        "dossier_coordination.etablissement_type",
        "array_to_string(
            array_agg(
                lien_dossier_coordination_etablissement_type.etablissement_type
                ORDER BY etablissement_type.libelle),
        ';') as etablissement_type_secondaire",
        "etablissement_categorie",

        // Fieldset "Urbanisme"
        "interface_referentiel_ads",
        "dossier_autorisation_ads",
        "dossier_instruction_ads",
        "references_cadastrales",
        "geom_point",
        "geom_emprise",
        "geolocalise",

        // XXX Champ non renseigné dans les specs techniques
        "contraintes_urba_om_html",
        "depot_de_piece",
    );
}
// En consultation et suppression
if(isset($_GET["action"])&&($_GET["action"]==2||$_GET["action"]==3)){
    $champs = array(
        // Fieldset "Informations générales"
        "dossier_coordination.dossier_coordination",
        "dossier_coordination.libelle",
        "dossier_coordination_type",
        "dossier_coordination.description",
        "date_demande",
        "date_butoir",
        "date_cloture",
        "enjeu_erp",
        "enjeu_ads",
        "autorite_police_encours",
        "dossier_coordination.etablissement_type",
        "array_to_string(
            array_agg(
                lien_dossier_coordination_etablissement_type.etablissement_type
                ORDER BY etablissement_type.libelle),
        ';') as etablissement_type_secondaire",
        "etablissement_categorie",
        "erp",
        "etablissement_locaux_sommeil",
        "dossier_coordination_parent",

        // Fieldset "Statut"
        "dossier_cloture",
        "a_qualifier",

        // Fieldset "Dossiers d'instruction"
        "dossier_instruction_secu",
        "'' as dossier_instruction_secu_lien",
        "dossier_instruction_acc",
        "'' as dossier_instruction_acc_lien",

        // Fieldset "Établissement"
        "etablissement",
        "'' as exploitant",

        // Fieldset "Urbanisme"
        "interface_referentiel_ads",
        "dossier_autorisation_ads",
        "dossier_instruction_ads",
        "references_cadastrales",
        "geom_point",
        "geom_emprise",
        "geolocalise",
        // XXX Champ non renseigné dans les specs techniques
        "contraintes_urba_om_html",
        "depot_de_piece",
    );
}

$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_etablissement_type
    ON lien_dossier_coordination_etablissement_type.dossier_coordination = dossier_coordination.dossier_coordination
LEFT JOIN ".DB_PREFIXE."etablissement_type
    ON etablissement_type.etablissement_type = lien_dossier_coordination_etablissement_type.etablissement_type
";

$selection = " GROUP BY dossier_coordination.dossier_coordination ";

// Requête du select du champ dossier_coordination_parent
$sql_dossier_coordination_parent_by_etablissement = "SELECT dossier_coordination.dossier_coordination, dossier_coordination.libelle FROM ".DB_PREFIXE."dossier_coordination WHERE dossier_coordination.etablissement = <idx_etab> AND dossier_coordination.dossier_coordination != <idx_dc>";

$sql_etablissement="SELECT etablissement.etablissement, CONCAT(etablissement.code,' - ',etablissement.libelle) FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, CONCAT(etablissement.code,' - ',etablissement.libelle) FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";

// Liaison NaN - dc / types secondaires d'établissement
$sql_etablissement_type_secondaire = "
    SELECT etablissement_type.etablissement_type,
    concat('[',etablissement_type.libelle, '] ', etablissement_type.description)
    FROM ".DB_PREFIXE."etablissement_type
    ORDER BY etablissement_type.libelle";
$sql_etablissement_type_secondaire_by_id = "
    SELECT lien_dossier_coordination_etablissement_type.etablissement_type,
    concat('[',etablissement_type.libelle, '] ', etablissement_type.description)
    FROM ".DB_PREFIXE."lien_dossier_coordination_etablissement_type
    LEFT JOIN ".DB_PREFIXE."etablissement_type
    ON etablissement_type.etablissement_type = lien_dossier_coordination_etablissement_type.etablissement_type
    WHERE lien_dossier_coordination_etablissement_type.dossier_coordination = <idx>
    ORDER BY etablissement_type.libelle";

// Catégorie d'établissement
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, concat('[',etablissement_categorie.libelle, '] ', etablissement_categorie.description) FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_categorie.libelle ASC";
$sql_etablissement_categorie_by_id = "SELECT etablissement_categorie.etablissement_categorie, concat('[',etablissement_categorie.libelle, '] ', etablissement_categorie.description) FROM ".DB_PREFIXE."etablissement_categorie WHERE etablissement_categorie = <idx>";
// Type d'établissement
$sql_etablissement_type="SELECT etablissement_type.etablissement_type, concat('[',etablissement_type.libelle, '] ', etablissement_type.description) FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement_type.libelle ASC";
$sql_etablissement_type_by_id = "SELECT etablissement_type.etablissement_type, concat('[',etablissement_type.libelle, '] ', etablissement_type.description) FROM ".DB_PREFIXE."etablissement_type WHERE etablissement_type = <idx>";

?>
