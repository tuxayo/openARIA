<?php
/**
 * @package openaria
 * @version SVN : $Id$ 
 */

//
include "../gen/sql/pgsql/contact.form.inc.php";

// Champs select
$sql_contact_type_whitout_expl_and_inst = "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE LOWER(code) != LOWER('expl') AND LOWER(code) != LOWER('inst') AND ((contact_type.om_validite_debut IS NULL AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE)) OR (contact_type.om_validite_debut <= CURRENT_DATE AND (contact_type.om_validite_fin IS NULL OR contact_type.om_validite_fin > CURRENT_DATE))) ORDER BY contact_type.libelle ASC";
$sql_contact_type_whitout_expl_and_inst_by_id_ = "SELECT contact_type.contact_type, contact_type.libelle FROM ".DB_PREFIXE."contact_type WHERE LOWER(code) != LOWER('expl') AND contact_type = <idx>";

/*
 * Champs du formulaire
 */
$champs = array(
    "contact",
    "etablissement",
    "contact_type",
    "service",
    "qualite",
    // personne morale
    "denomination",
    "raison_sociale",
    "siret",
    "categorie_juridique",
    // particulier
    "civilite",
    "nom",
    "prenom",
    "titre",
    // adresse
    "adresse_numero",
    "adresse_numero2",
    "adresse_voie",
    "adresse_complement",
    "lieu_dit",
    "boite_postale",
    "adresse_cp",
    "adresse_ville",
    "cedex",
    "pays",
    // coordonnées
    "telephone",
    "mobile",
    "fax",
    "courriel",
    "reception_convocation",
    "reception_programmation",
    "reception_commission",
    //
    "om_validite_debut",
    "om_validite_fin",
);

?>
