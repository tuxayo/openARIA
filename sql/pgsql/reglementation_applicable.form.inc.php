<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reglementation_applicable.form.inc.php";
//
$champs = array(
    "reglementation_applicable.reglementation_applicable",
    "reglementation_applicable.service",
    "reglementation_applicable.libelle",
    "reglementation_applicable.description",
    "array_to_string(
        array_agg(
            DISTINCT lien_reglementation_applicable_etablissement_categorie.etablissement_categorie
        ),
    ';') as etablissement_categorie",
    //
    "array_to_string(
        array_agg(
            DISTINCT lien_reglementation_applicable_etablissement_type.etablissement_type
        ),
    ';') as etablissement_type",
    "reglementation_applicable.annee_debut_application",
    "reglementation_applicable.annee_fin_application",
    "reglementation_applicable.om_validite_debut",
    "reglementation_applicable.om_validite_fin",
);
//
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_reglementation_applicable_etablissement_categorie
    ON lien_reglementation_applicable_etablissement_categorie.reglementation_applicable = reglementation_applicable.reglementation_applicable
LEFT JOIN ".DB_PREFIXE."lien_reglementation_applicable_etablissement_type
    ON lien_reglementation_applicable_etablissement_type.reglementation_applicable = reglementation_applicable.reglementation_applicable
";
//
$selection = " GROUP BY reglementation_applicable.reglementation_applicable ";
// Liaison N à N - réglementation applicable / catégorie d'établissement
$sql_etablissement_categorie = "
SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib
FROM ".DB_PREFIXE."etablissement_categorie
ORDER BY etablissement_categorie.libelle";
$sql_etablissement_categorie_by_id = "
SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib
FROM ".DB_PREFIXE."etablissement_categorie
WHERE etablissement_categorie.etablissement_categorie IN (<idx>)
ORDER BY etablissement_categorie.libelle";
// Liaison N à N - réglementation applicable / type d'établissement
$sql_etablissement_type = "
SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib
FROM ".DB_PREFIXE."etablissement_type
ORDER BY etablissement_type.libelle";
$sql_etablissement_type_by_id = "
SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib
FROM ".DB_PREFIXE."etablissement_type
WHERE etablissement_type.etablissement_type IN (<idx>)
ORDER BY etablissement_type.libelle";

?>
