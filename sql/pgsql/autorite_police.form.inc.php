<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/autorite_police.form.inc.php";

//
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_autorite_police_courrier
    ON lien_autorite_police_courrier.autorite_police = autorite_police.autorite_police
LEFT JOIN ".DB_PREFIXE."courrier
    ON lien_autorite_police_courrier.courrier = courrier.courrier
";

//
$champs=array(
    "autorite_police.autorite_police",
    "autorite_police.etablissement",
    "autorite_police.dossier_coordination",
    "autorite_police_decision",
    "date_decision",
    "delai",
    "autorite_police_motif",
    "autorite_police.cloture",
    "date_notification",
    "autorite_police.date_butoir",
    "service",
    "array_to_string(
        array_agg(
            lien_autorite_police_courrier.courrier
            ORDER BY courrier.courrier
        ),
    ';') as courriers_lies",
    "dossier_instruction_reunion",
    "dossier_instruction_reunion_prochain",
);

//
$selection = " GROUP BY autorite_police.autorite_police ";

// Liaison NaN - autorite_police/courrier
// SELECT
$sql_courriers_lies_select = "SELECT courrier.courrier, CONCAT(%s) as lib ";
// FROM
$sql_courriers_lies_from = " FROM ".DB_PREFIXE."courrier %s ";
//
$sql_courriers_lies_where = " 
WHERE courrier.mailing IS FALSE 
    AND courrier.finalise IS TRUE
    AND (LOWER(courrier_type.code) = LOWER('DEC') OR LOWER(courrier_type.code) = LOWER('NOTIFAP'))
    AND dossier_coordination.dossier_coordination = <idx_dc>
ORDER BY courrier.courrier";
//
$sql_courriers_lies_by_id_where = " 
WHERE courrier.courrier IN (<idx>)
ORDER BY courrier.courrier";

//
$sql_dossier_instruction_reunion="SELECT dossier_instruction_reunion.dossier_instruction_reunion, CONCAT('"._("Demande de passage n°")."', dossier_instruction_reunion.dossier_instruction_reunion, ' "._("du")." ', to_char(dossier_instruction_reunion.date_souhaitee ,'DD/MM/YYYY')) as lib FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY lib, dossier_instruction_reunion.dossier_instruction_reunion ASC";
$sql_dossier_instruction_reunion_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, CONCAT('"._("Demande de passage n°")."', dossier_instruction_reunion.dossier_instruction_reunion, ' "._("du")." ', to_char(dossier_instruction_reunion.date_souhaitee ,'DD/MM/YYYY')) as lib FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";

//
$sql_dossier_instruction_reunion_prochain="SELECT dossier_instruction_reunion.dossier_instruction_reunion, CONCAT('"._("Demande de passage n°")."', dossier_instruction_reunion.dossier_instruction_reunion, ' "._("du")." ', to_char(dossier_instruction_reunion.date_souhaitee ,'DD/MM/YYYY')) as lib FROM ".DB_PREFIXE."dossier_instruction_reunion ORDER BY lib, dossier_instruction_reunion.dossier_instruction_reunion ASC";
$sql_dossier_instruction_reunion_prochain_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, CONCAT('"._("Demande de passage n°")."', dossier_instruction_reunion.dossier_instruction_reunion, ' "._("du")." ', to_char(dossier_instruction_reunion.date_souhaitee ,'DD/MM/YYYY')) as lib FROM ".DB_PREFIXE."dossier_instruction_reunion WHERE dossier_instruction_reunion = <idx>";

?>
