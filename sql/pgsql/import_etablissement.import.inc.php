<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$import= "Insertion dans la table etablissement voir rec/import_utilisateur.inc";
$table= DB_PREFIXE."etablissement";
$id=''; // numerotation automatique
$verrou=1;// =0 pas de mise a jour de la base / =1 mise a jour
$fic_rejet=1; // =0 pas de fichier pour relance / =1 fichier relance traitement
$ligne1=1;// = 1 : 1ere ligne contient nom des champs / o sinon

/**
 *
 */
// Foreign Key : reunion_avis
// Gestion particulière car liée au service
// Requête d'existence
$reunion_avis_fkey_exist_query = "
SELECT * 
FROM 
    ".DB_PREFIXE."reunion_avis 
        LEFT JOIN ".DB_PREFIXE."service 
            ON reunion_avis.service=service.service
WHERE
    service.code='<SERVICE>'
    AND reunion_avis.reunion_avis = '";
// Requête de recherche depuis un alias
$reunion_avis_fkey_alias_query = "
SELECT 
    reunion_avis.reunion_avis 
FROM ".DB_PREFIXE."reunion_avis 
    LEFT JOIN ".DB_PREFIXE."service 
        ON reunion_avis.service=service.service
WHERE
    service.code='<SERVICE>'
    AND reunion_avis.code = '<SEARCH>'";
// Liste des champs de recherche d'alias
$reunion_avis_fkey_alias_fields_list = array("code", );
// Foreign Key : acteur
// Gestion particulière car liée au service
// Requête d'existence
$acteur_fkey_exist_query = "
SELECT * 
FROM 
    ".DB_PREFIXE."acteur 
        LEFT JOIN ".DB_PREFIXE."service 
            ON acteur.service=service.service
WHERE
    service.code='<SERVICE>'
    AND acteur.acteur = '";
// Requête de recherche depuis un alias
$acteur_fkey_alias_query = "
SELECT 
    acteur.acteur 
FROM ".DB_PREFIXE."acteur 
    LEFT JOIN ".DB_PREFIXE."service 
        ON acteur.service=service.service
WHERE
    service.code='<SERVICE>'
    AND acteur.acronyme = '<SEARCH>'";
// Liste des champs de recherche d'alias
$acteur_fkey_alias_fields_list = array("acronyme", );
// Foreign Key : autorite_competente
// Gestion particulière car liée au service
// Requête d'existence
$autorite_competente_fkey_exist_query = "
SELECT * 
FROM 
    ".DB_PREFIXE."autorite_competente 
        LEFT JOIN ".DB_PREFIXE."service 
            ON autorite_competente.service=service.service
WHERE
    service.code='<SERVICE>'
    AND autorite_competente.autorite_competente = '";
// Requête de recherche depuis un alias
$autorite_competente_fkey_alias_query = "
SELECT 
    autorite_competente.autorite_competente
FROM ".DB_PREFIXE."autorite_competente 
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_competente.service=service.service
WHERE
    service.code='<SERVICE>'
    AND autorite_competente.code = '<SEARCH>'";
// Liste des champs de recherche d'alias
$autorite_competente_fkey_alias_fields_list = array("code", );
// Foreign Key : analyses_type
// Gestion particulière car liée au service
// Requête d'existence
$analyses_type_fkey_exist_query = "
SELECT * 
FROM 
    ".DB_PREFIXE."analyses_type 
        LEFT JOIN ".DB_PREFIXE."service 
            ON analyses_type.service=service.service
WHERE
    service.code='<SERVICE>'
    AND analyses_type.analyses_type = '";
// Requête de recherche depuis un alias
$analyses_type_fkey_alias_query = "
SELECT 
    analyses_type.analyses_type 
FROM ".DB_PREFIXE."analyses_type 
    LEFT JOIN ".DB_PREFIXE."service 
        ON analyses_type.service=service.service
WHERE
    service.code='<SERVICE>'
    AND analyses_type.code = '<SEARCH>'";
// Liste des champs de recherche d'alias
$analyses_type_fkey_alias_fields_list = array("code", );

/**
 *
 */
$fields = array(
    "etablissement" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
    ),
    "code" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "25",
    ),
    "libelle" => array(
        "notnull" => "1",
        "type" => "string",
        "len" => "100",
    ),
    "adresse_numero" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "adresse_numero2" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "10",
    ),
    "adresse_voie" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "voie",
            "foreign_column_name" => "voie",
            "sql_exist" => "select * from ".DB_PREFIXE."voie where voie = '",
            "foreign_key_alias" => array(
                "query" => "select voie from ".DB_PREFIXE."voie where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "adresse_complement" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "lieu_dit" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "39",
    ),
    "boite_postale" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "adresse_cp" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "6",
    ),
    "adresse_ville" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "40",
    ),
    "adresse_arrondissement" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "arrondissement",
            "foreign_column_name" => "arrondissement",
            "sql_exist" => "select * from ".DB_PREFIXE."arrondissement where arrondissement = '",
        ),
    ),
    "cedex" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "5",
    ),
    "npai" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
        "default" => "false",
    ),
    "telephone" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
    ),
    "fax" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
    ),
    "etablissement_nature" => array(
        "notnull" => "1",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_nature",
            "foreign_column_name" => "etablissement_nature",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_nature where etablissement_nature = '",
            "foreign_key_alias" => array(
                "query" => "select etablissement_nature from ".DB_PREFIXE."etablissement_nature where code = '<SEARCH>'",
                "fields_list" => array("code", ),
            ),
        ),
    ),
    "siret" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "20",
    ),
    "annee_de_construction" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "4",
    ),
    "etablissement_statut_juridique" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_statut_juridique",
            "foreign_column_name" => "etablissement_statut_juridique",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_statut_juridique where etablissement_statut_juridique = '",
        ),
    ),
    "etablissement_tutelle_adm" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_tutelle_adm",
            "foreign_column_name" => "etablissement_tutelle_adm",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_tutelle_adm where etablissement_tutelle_adm = '",
        ),
    ),
    "ref_patrimoine" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "etablissement_type" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_type",
            "foreign_column_name" => "etablissement_type",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_type where etablissement_type = '",
            "foreign_key_alias" => array(
                "query" => "select etablissement_type from ".DB_PREFIXE."etablissement_type where libelle LIKE '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "etablissement_categorie" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_categorie",
            "foreign_column_name" => "etablissement_categorie",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_categorie where etablissement_categorie = '",
            "foreign_key_alias" => array(
                "query" => "select etablissement_categorie from ".DB_PREFIXE."etablissement_categorie where libelle = '<SEARCH>'",
                "fields_list" => array("libelle", ),
            ),
        ),
    ),
    "etablissement_etat" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "etablissement_etat",
            "foreign_column_name" => "etablissement_etat",
            "sql_exist" => "select * from ".DB_PREFIXE."etablissement_etat where etablissement_etat = '",
            "foreign_key_alias" => array(
                "query" => "select etablissement_etat from ".DB_PREFIXE."etablissement_etat where statut_administratif = '<SEARCH>'",
                "fields_list" => array("statut_administratif", ),
            ),
        ),
    ),
    "date_arrete_ouverture" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "autorite_police_encours" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
        "default" => "false",
    ),
    "om_validite_debut" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "om_validite_fin" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "si_effectif_public" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "si_effectif_personnel" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "si_locaux_sommeil" => array(
        "notnull" => "1",
        "type" => "bool",
        "len" => "1",
        "default" => "false",
    ),
    "si_periodicite_visites" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "periodicite_visites",
            "foreign_column_name" => "periodicite_visites",
            "sql_exist" => "select * from ".DB_PREFIXE."periodicite_visites where periodicite_visites = '",
        ),
    ),
    "si_prochaine_visite_periodique_date_previsionnelle" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "si_visite_duree" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "visite_duree",
            "foreign_column_name" => "visite_duree",
            "sql_exist" => "select * from ".DB_PREFIXE."visite_duree where visite_duree = '",
            "foreign_key_alias" => array(
                "query" => "select visite_duree from ".DB_PREFIXE."visite_duree where libelle = '<SEARCH>' or durre = '<SEARCH>'",
                "fields_list" => array("libelle", "duree", ),
            ),
        ),
    ),
    "si_derniere_visite_periodique_date" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "si_derniere_visite_date" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "si_derniere_visite_avis" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "reunion_avis",
            "foreign_column_name" => "reunion_avis",
            "sql_exist" => str_replace("<SERVICE>", "SI", $reunion_avis_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $reunion_avis_fkey_alias_query),
                "fields_list" => $reunion_avis_fkey_alias_fields_list,
            ),
        ),
    ),
    "si_derniere_visite_technicien" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "acteur",
            "foreign_column_name" => "acteur",
            "sql_exist" => str_replace("<SERVICE>", "SI", $acteur_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $acteur_fkey_alias_query),
                "fields_list" => $acteur_fkey_alias_fields_list,
            ),
        ),
    ),
    "si_prochaine_visite_date" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "si_prochaine_visite_type" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "analyses_type",
            "foreign_column_name" => "analyses_type",
            "sql_exist" => str_replace("<SERVICE>", "SI", $analyses_type_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $analyses_type_fkey_alias_query),
                "fields_list" => $analyses_type_fkey_alias_fields_list,
            ),
        ),
    ),
    "acc_derniere_visite_date" => array(
        "notnull" => "",
        "type" => "date",
        "len" => "12",
    ),
    "acc_derniere_visite_avis" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "reunion_avis",
            "foreign_column_name" => "reunion_avis",
            "sql_exist" => str_replace("<SERVICE>", "ACC", $reunion_avis_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "ACC", $reunion_avis_fkey_alias_query),
                "fields_list" => $reunion_avis_fkey_alias_fields_list,
            ),
        ),
    ),
    "acc_derniere_visite_technicien" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "acteur",
            "foreign_column_name" => "acteur",
            "sql_exist" => str_replace("<SERVICE>", "ACC", $acteur_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "ACC", $acteur_fkey_alias_query),
                "fields_list" => $acteur_fkey_alias_fields_list,
            ),
        ),
    ),
    "acc_consignes_om_html" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "acc_descriptif_om_html" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "si_consignes_om_html" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "si_descriptif_om_html" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "si_autorite_competente_visite" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "autorite_competente",
            "foreign_column_name" => "autorite_competente",
            "sql_exist" => str_replace("<SERVICE>", "SI", $autorite_competente_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $autorite_competente_fkey_alias_query),
                "fields_list" => $autorite_competente_fkey_alias_fields_list,
            ),
        ),
    ),
    "si_autorite_competente_plan" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "autorite_competente",
            "foreign_column_name" => "autorite_competente",
            "sql_exist" => str_replace("<SERVICE>", "SI", $autorite_competente_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $autorite_competente_fkey_alias_query),
                "fields_list" => $autorite_competente_fkey_alias_fields_list,
            ),
        ),
    ),
    "si_dernier_plan_avis" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
        "fkey" => array(
            "foreign_table_name" => "reunion_avis",
            "foreign_column_name" => "reunion_avis",
            "sql_exist" => str_replace("<SERVICE>", "SI", $reunion_avis_fkey_exist_query),
            "foreign_key_alias" => array(
                "query" => str_replace("<SERVICE>", "SI", $reunion_avis_fkey_alias_query),
                "fields_list" => $reunion_avis_fkey_alias_fields_list,
            ),
        ),
    ),
    "si_type_alarme" => array(
        "notnull" => "",
        "type" => "blob",
        "len" => "-5",
    ),
    "si_type_ssi" => array(
        "notnull" => "",
        "type" => "string",
        "len" => "100",
    ),
    "si_conformite_l16" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "si_alimentation_remplacement" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "si_service_securite" => array(
        "notnull" => "",
        "type" => "bool",
        "len" => "1",
    ),
    "si_personnel_jour" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    "si_personnel_nuit" => array(
        "notnull" => "",
        "type" => "int",
        "len" => "11",
    ),
    // "references_cadastrales" => array(
    //     "notnull" => "",
    //     "type" => "blob",
    //     "len" => "-5",
    // ),
	//	"dossier_coordination_periodique" => array(
	//	    "notnull" => "",
	//	    "type" => "int",
	//	    "len" => "11",
	//	    "fkey" => array(
	//	        "foreign_table_name" => "dossier_coordination",
	//	        "foreign_column_name" => "dossier_coordination",
	//	        "sql_exist" => "select * from ".DB_PREFIXE."dossier_coordination where dossier_coordination = '",
	//	    ),
	//	),
    // "geom_point" => array(
    //     "notnull" => "",
    //     "type" => "geom",
    //     "len" => "551424",
    // ),
    // "geom_emprise" => array(
    //     "notnull" => "",
    //     "type" => "geom",
    //     "len" => "551444",
    // ),
);
?>