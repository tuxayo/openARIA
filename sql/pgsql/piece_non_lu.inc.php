<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont non
 * lu, c'est-à-dire dont le champ lu est à false.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/dossier_instruction.inc.php');

// Fil d'ariane
$ent = _("dossiers")." -> "._("documents entrants")." -> "._("mes non lus");

// Titre de l'onglet
$tab_title = _("Document entrant");

// SELECT 
$champAffiche = array(
    'dossier_instruction.dossier_instruction as "'._("id").'"',
    'dossier_instruction.libelle as "'._("DI").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
    'piece.nom as "'._("nom").'"',
    'to_char(piece.om_date_creation ,\'DD/MM/YYYY\') as "'._("om_date_creation").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    );
//
$champRecherche = array();

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;

// FROM
$table .= " 
    LEFT JOIN ".DB_PREFIXE."piece
        ON piece.dossier_instruction = dossier_instruction.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur ";

// WHERE
$selection = " WHERE piece.lu = false 
        AND om_utilisateur.login = '".$_SESSION["login"]."'";

// ORDER BY
$tri = " ORDER BY piece.date_butoir ";

//
$obj = "dossier_instruction_mes_visites";
// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => 'form.php?obj='.$obj.'&amp;action=3'.'&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;recherche='.$recherche1.'&amp;tricol='.$tricol.'&amp;selectioncol='.$selectioncol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];
$obj = "piece_non_lu";

// Initialisation des options de l'écran de listing
$options = array();
// Désactivation de la recherche simple
$option = array(
  "type" => "search",
  "display" => false,
);
$options[] = $option;

?>
