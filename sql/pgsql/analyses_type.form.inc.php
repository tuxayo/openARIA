<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/analyses_type.form.inc.php";

// filtre les modèles d'édition
$sql_modele_edition = "SELECT modele_edition.modele_edition, modele_edition.libelle 
FROM ".DB_PREFIXE."modele_edition 
LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))
AND LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
ORDER BY modele_edition.libelle ASC";

?>
