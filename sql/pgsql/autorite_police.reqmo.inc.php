<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
$reqmo['libelle']=_('reqmo-libelle-autorite_police');
$reqmo['reqmo_libelle']=_('reqmo-libelle-autorite_police');
$ent=_('autorite_police');
// Requête
$reqmo['sql']="SELECT
    autorite_police.autorite_police as autorite_police,
    autorite_police_decision.libelle as autorite_police_decision,
    autorite_police.date_decision as date_decision,
    autorite_police.delai as delai,
    autorite_police_motif.libelle as autorite_police_motif,
    autorite_police.cloture as cloture,
    autorite_police.date_notification as date_notification,
    autorite_police.date_butoir as date_butoir,
    service.libelle as service,
    autorite_police.dossier_instruction_reunion as dossier_instruction_reunion,
    dossier_coordination.libelle as dossier_coordination,
    concat(etablissement.code,' - ',etablissement.libelle) as etablissement,
    autorite_police.dossier_instruction_reunion_prochain as dossier_instruction_reunion_prochain
FROM ".DB_PREFIXE."autorite_police
LEFT OUTER JOIN ".DB_PREFIXE."autorite_police_decision ON
autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision
LEFT OUTER JOIN ".DB_PREFIXE."autorite_police_motif ON
autorite_police.autorite_police_motif=autorite_police_motif.autorite_police_motif
LEFT OUTER JOIN ".DB_PREFIXE."service ON
autorite_police.service=service.service
LEFT OUTER JOIN ".DB_PREFIXE."dossier_coordination ON
autorite_police.dossier_coordination=dossier_coordination.dossier_coordination
LEFT OUTER JOIN ".DB_PREFIXE."etablissement ON
autorite_police.etablissement=etablissement.etablissement
ORDER BY [tri]";
// Possibilités de tri
$reqmo['tri'] = array(
    'autorite_police',
    'autorite_police_decision',
    'date_decision',
    'delai',
    'autorite_police_motif',
    'cloture',
    'date_notification',
    'date_butoir',
    'service',
    'dossier_instruction_reunion',
    'dossier_coordination',
    'etablissement',
    'dossier_instruction_reunion_prochain'
);
?>