<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/visite.inc.php');

$champAffiche = array(
    'visite.visite as "'._("visite").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'visite_etat.libelle as "'._("visite_etat").'"',
    'visite.convocation_exploitants as "'._("convocation_exploitants").'"',
    'visite.programmation_version_creation as "'._("programmation_version_creation").'"',
    'to_char(visite.date_creation ,\'DD/MM/YYYY\') as "'._("date_creation").'"',
);

$tab_actions['corner']['ajouter'] = NULL;

?>
