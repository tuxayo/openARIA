<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/etablissement.form.inc.php";

// Récupèration des paramètres
(isset($_GET['obj']) ? $obj = $_GET['obj'] : $obj = "");

// Requête SQL pour filtrer l'arrondissement en fonction de la voie
$sql_adresse_arrondissement_by_adresse_voie = "SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement LEFT JOIN ".DB_PREFIXE."voie_arrondissement ON voie_arrondissement.arrondissement = arrondissement.arrondissement WHERE voie_arrondissement.voie = <idx_voie>";

// S'il s'agit d'un établissement
if ($obj == "etablissement_tous"
        || $obj == "etablissement_referentiel_erp") {

    $champs = array(
        //
        // Détails sécurité et accessibilité
        //    
        // COL1
        // Fieldset détail accessibilité
        "acc_consignes_om_html",
        "acc_descriptif_om_html",
        "'' as tot_ua_valid",
        "'' as acc_handicap_auditif",
        "'' as acc_handicap_mental",
        "'' as acc_handicap_physique",
        "'' as acc_handicap_visuel",
        "'' as acc_derogation_scda",
        // COL2
        // Fieldset détail sécurité
        "si_consignes_om_html",
        "si_descriptif_om_html",
        "si_autorite_competente_visite",
        "si_autorite_competente_plan",
        "si_dernier_plan_avis",
        "si_type_alarme",
        "si_type_ssi",
        "si_conformite_l16",
        "si_alimentation_remplacement",
        "si_service_securite",
        "si_personnel_jour",
        "si_personnel_nuit",
        //
        // Fiche établissement
        //
        // COL 1
        // Fieldset établissement
        "etablissement",
        "code",
        "libelle",
        "adresse_numero",
        "adresse_numero2",
        "adresse_voie",
        "adresse_complement",
        "lieu_dit",
        "boite_postale",
        "adresse_cp",
        "adresse_ville",
        "adresse_arrondissement",
        "cedex",
        "npai",
        "telephone",
        "fax",
        "etablissement_nature",
        "siret",
        "annee_de_construction",
        "etablissement_statut_juridique",
        "etablissement_tutelle_adm",
        // Fieldset exploitant
        "'Aucun' as exp",
        "'' as exp_civilite",
        "'' as exp_nom",
        "'' as exp_prenom",
        "'' as exp_blank",
        "'' as meme_adresse",
        //
        "'' as exp_adresse_numero",
        "'' as exp_adresse_numero2",
        "'' as exp_adresse_voie",
        //
        "'' as exp_adresse_complement",
        //
        "'' as exp_lieu_dit",
        "'' as exp_boite_postale",
        //
        "'' as exp_adresse_cp",
        "'' as exp_adresse_ville",
        "'' as exp_cedex",
        //
        "'' as exp_pays",
        // Fieldset localisation
        "references_cadastrales",
            "ref_patrimoine",
	    "geom_point",
	    "geom_emprise",
            "geolocalise",
	    // COL 2
	    // Fieldset statut
	    "etablissement_type",
	    "etablissement_categorie",
	    "etablissement_etat",
	    "date_arrete_ouverture",
	    "autorite_police_encours",
	    "om_validite_debut",
	    "om_validite_fin",
	    // Fieldset détail technique
	    "si_effectif_public",
	    "si_effectif_personnel",
	    "si_locaux_sommeil",
        "si_visite_duree",
	    // Fieldset suivi des visites
	    // Visites sécurité
	    "si_periodicite_visites",
	    "si_prochaine_visite_periodique_date_previsionnelle",
	    "si_derniere_visite_periodique_date",
        "dossier_coordination_periodique",
	    "si_derniere_visite_date",
	    "si_derniere_visite_avis",
	    "si_derniere_visite_technicien",
	    "si_prochaine_visite_date",
	    "si_prochaine_visite_type",
	    // Visites accessibilité
	    "acc_derniere_visite_date",
	    "acc_derniere_visite_avis",
	    "acc_derniere_visite_technicien",
	);

    // Ajout des tableaux des données pour les select
    // On transforme les checkbox en select pour la gestion du null
    $booleens = array();
    $booleens[0][0] = '';
    $booleens[1][0] = _("NC");
    $booleens[0][1] = 't';
    $booleens[1][1] = _("Oui");
    $booleens[0][2] = 'f';
    $booleens[1][2] = _("Non");
    // On modifie le select nature pour supprimer la voie publique
    $sql_etablissement_nature=
    "SELECT etablissement_nature.etablissement_nature, 
        etablissement_nature.nature 
    FROM ".DB_PREFIXE."etablissement_nature 
    WHERE 
        (
            (
                (etablissement_nature.om_validite_debut IS NULL 
                    AND (etablissement_nature.om_validite_fin IS NULL 
                        OR etablissement_nature.om_validite_fin > CURRENT_DATE)
                ) OR ( etablissement_nature.om_validite_debut <= CURRENT_DATE 
                      AND (etablissement_nature.om_validite_fin IS NULL 
                            OR etablissement_nature.om_validite_fin > CURRENT_DATE)
                      )
            ) AND etablissement_nature.nature != 'Aménagement voie publique'
        ) 
    ORDER BY etablissement_nature.nature ASC";
}

//
$sql_autorite_competente = "
SELECT
    autorite_competente.autorite_competente, autorite_competente.libelle
FROM 
    ".DB_PREFIXE."autorite_competente 
        LEFT JOIN ".DB_PREFIXE."service
            ON autorite_competente.service=service.service
WHERE
    service.code='<service>'
ORDER BY 
    autorite_competente.libelle ASC
";
//
$sql_si_autorite_competente = str_replace("<service>", "SI", $sql_autorite_competente);
$sql_si_autorite_competente_plan = $sql_si_autorite_competente;
$sql_si_autorite_competente_visite = $sql_si_autorite_competente;

//
$sql_reunion_avis = "
SELECT 
    reunion_avis.reunion_avis, reunion_avis.libelle 
FROM 
    ".DB_PREFIXE."reunion_avis
        LEFT JOIN ".DB_PREFIXE."service
            ON reunion_avis.service=service.service
WHERE 
    ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) 
    AND service.code='<service>'
ORDER BY 
    reunion_avis.libelle ASC
";
//
$sql_si_reunion_avis = str_replace("<service>", "SI", $sql_reunion_avis);
$sql_si_derniere_visite_avis = $sql_si_reunion_avis;
$sql_si_dernier_plan_avis = $sql_si_reunion_avis;
//
$sql_acc_reunion_avis = str_replace("<service>", "ACC", $sql_reunion_avis);
$sql_acc_dernier_plan_avis = $sql_acc_reunion_avis;
$sql_acc_derniere_visite_avis = $sql_acc_reunion_avis;

//
$sql_adresse_arrondissement="SELECT arrondissement.arrondissement, arrondissement.libelle FROM ".DB_PREFIXE."arrondissement WHERE ((arrondissement.om_validite_debut IS NULL AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE)) OR (arrondissement.om_validite_debut <= CURRENT_DATE AND (arrondissement.om_validite_fin IS NULL OR arrondissement.om_validite_fin > CURRENT_DATE))) 
ORDER BY arrondissement.code::int ASC";

?>
