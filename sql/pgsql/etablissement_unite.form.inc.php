<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/etablissement_unite.form.inc.php";

// Ajout des tableaux des données pour les select
$booleens = array(
    array("null", "t", "f"), 
    array(_("Choisir..."), _("Oui"), _("Non")),
);

$champs = array(
    "etablissement_unite",
    //
    "libelle",
    "etat",
    "archive",
    "etablissement",
    "dossier_instruction",
    "etablissement_unite_lie",
    //
    "acc_handicap_auditif",
    "acc_handicap_mental",
    "acc_handicap_physique",
    "acc_handicap_visuel",
    "acc_ascenseur",
    "acc_elevateur",
    "acc_boucle_magnetique",
    "acc_sanitaire",
    "acc_douche",
    "acc_places_assises_public",
    "acc_chambres_amenagees",
    "acc_places_stationnement_amenagees",
    //
    "acc_derogation_scda",
    //
    "acc_descriptif_ua_om_html",
    "acc_notes_om_html",
    //
    "adap_date_validation",
    "adap_duree_validite",
    "adap_annee_debut_travaux",
    "adap_annee_fin_travaux",
    //

);

?>
