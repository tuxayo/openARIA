<?php
/**
 * Ce script permet...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/dossier_instruction_tous_visites.inc.php');

// Fil d'ariane
$ent = _("dossiers")." -> "._("DI")." -> "._("mes visites");

// Modification du WHERE
$selection .= " AND om_utilisateur.login = '".$_SESSION["login"]."' ";

// On enlève la colonne technicien inutile dans son contexte
$champAffiche = array_diff(
    $champAffiche,
    array('acteur.nom_prenom as "'._("technicien").'"', )
);

//
$table .= "LEFT JOIN ".DB_PREFIXE."om_utilisateur
        ON acteur.om_utilisateur = om_utilisateur.om_utilisateur";

// Pas de recherche avancée
//$options = array();
unset($options["advsearch"]["advanced"]["technicien"]);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
