<?php
/**
 * Surcharge de la classe contact pour afficher les contacts sans lien.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/contact.inc.php";

// Fil d'Ariane
$ent = _("suivi")." -> "._("documents generes")." -> "._("contact_institutionnel");

// SELECT
$champAffiche = array(
    'contact.contact as "'._("contact").'"',
    //
    "CASE  
        WHEN contact.qualite = 'particulier'
        THEN
            TRIM(CONCAT_WS(' ', contact_civilite.libelle, contact.nom, contact.prenom))
        ELSE
            CASE
                WHEN contact.nom IS NOT NULL OR contact.prenom IS NOT NULL
                THEN 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination, 'représenté(e) par', contact_civilite.libelle, contact.nom, contact.prenom))
                ELSE 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination))
            END
    END as \""._("contact")."\"",
    //
    'concat(contact.adresse_numero, 
    \' \', contact.adresse_numero2, \' \', contact.adresse_voie,
    \' \', contact.adresse_cp, \' \', contact.adresse_ville)
    as "'._("adresse").'"',
    //
    "CASE
        WHEN contact.qualite = 'particulier'
        THEN
            '"._("particulier")."'
        ELSE
            '"._("personne morale")."'
    END as \""._("qualite")."\"",
    //
    'contact.courriel as "'._("courriel").'"',
);

// Filtre du listing
$selection = " WHERE LOWER(contact_type.code) = LOWER('inst') ";

// Pas d'onglet
$sousformulaire = array();

?>
