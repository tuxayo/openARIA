<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_coordination_type.form.inc.php";

//
$champs = array(
    "dossier_coordination_type.dossier_coordination_type",
    "dossier_coordination_type.code",
    "dossier_coordination_type.libelle",
    "dossier_coordination_type.description",
    "dossier_coordination_type.om_validite_debut",
    "dossier_coordination_type.om_validite_fin",
    "dossier_type",
    "dossier_instruction_secu",
    "dossier_instruction_acc",
    "a_qualifier",
    "analyses_type_si",
    "analyses_type_acc",
    "array_to_string(
        array_agg(
            DISTINCT lien_SI.analyses_type
        ),
    ';') as analyses_type_disponible_si",
    //
    "array_to_string(
        array_agg(
            DISTINCT lien_ACC.analyses_type
        ),
    ';') as analyses_type_disponible_acc",
);

$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_type_analyses_type lien_SI
    ON lien_SI.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    AND lien_SI.service = (SELECT service FROM ".DB_PREFIXE."service WHERE code = 'SI')
LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_type_analyses_type lien_ACC
    ON lien_ACC.dossier_coordination_type = dossier_coordination_type.dossier_coordination_type
    AND lien_ACC.service = (SELECT service FROM ".DB_PREFIXE."service WHERE code = 'ACC')
";

$selection = " GROUP BY dossier_coordination_type.dossier_coordination_type ";

// Liaison N à N - type de DC / type d'analyse
$sql_analyses_type_disponible = "
SELECT analyses_type.analyses_type, analyses_type.libelle as lib
FROM ".DB_PREFIXE."analyses_type
WHERE analyses_type.service = <idx_service>
ORDER BY analyses_type.libelle";
$sql_analyses_type_disponible_by_id = "
SELECT analyses_type.analyses_type, analyses_type.libelle as lib
FROM ".DB_PREFIXE."analyses_type
WHERE analyses_type.analyses_type IN (<idx>)
AND analyses_type.service = <idx_service>
ORDER BY analyses_type.libelle";
//champs select
$sql_analyses_type_acc="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='ACC') AND
    ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_analyses_type_acc_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='ACC') AND
    analyses_type = <idx>";
$sql_analyses_type_si="SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='SI') AND
    ((analyses_type.om_validite_debut IS NULL AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE)) OR (analyses_type.om_validite_debut <= CURRENT_DATE AND (analyses_type.om_validite_fin IS NULL OR analyses_type.om_validite_fin > CURRENT_DATE))) ORDER BY analyses_type.libelle ASC";
$sql_analyses_type_si_by_id = "SELECT analyses_type.analyses_type, analyses_type.libelle FROM ".DB_PREFIXE."analyses_type WHERE service = (SELECT service FROM ".DB_PREFIXE."service WHERE code ='SI') AND
    analyses_type = <idx>";

?>
