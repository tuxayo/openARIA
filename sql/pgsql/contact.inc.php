<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/contact.inc.php";

//
$champAffiche = array(
    //
    'contact.contact as "'._("contact").'"',
    //
    'contact_type.libelle as "'._("contact_type").'"',
    //
    "CASE  
        WHEN contact.qualite = 'particulier'
        THEN
            TRIM(CONCAT_WS(' ', contact_civilite.libelle, contact.nom, contact.prenom))
        ELSE
            CASE
                WHEN contact.nom IS NOT NULL OR contact.prenom IS NOT NULL
                THEN 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination, 'représenté(e) par', contact_civilite.libelle, contact.nom, contact.prenom))
                ELSE 
                    TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination))
            END
    END as \""._("contact")."\"",
    //
    'concat(contact.adresse_numero, 
    \' \', contact.adresse_numero2, \' \', contact.adresse_voie,
    \' \', contact.adresse_cp, \' \', contact.adresse_ville)
    as "'._("adresse").'"',
    //
    "CASE
        WHEN contact.qualite = 'particulier'
        THEN
            '"._("particulier")."'
        ELSE
            '"._("personne morale")."'
    END as \""._("qualite")."\"",
    //
    'contact.courriel as "'._("courriel").'"',
);

?>
