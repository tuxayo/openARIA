<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_instruction.form.inc.php";

//
$tableSelect=DB_PREFIXE."dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_instruction.dossier_coordination = dossier_coordination.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."courrier
        ON dossier_instruction.dossier_instruction = courrier.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."etablissement
        ON dossier_coordination.etablissement = etablissement.etablissement
    LEFT JOIN ".DB_PREFIXE."etablissement_type
        ON etablissement.etablissement_type = etablissement_type.etablissement_type
    LEFT JOIN ".DB_PREFIXE."etablissement_categorie
        ON etablissement.etablissement_categorie = etablissement_categorie.etablissement_categorie
    LEFT JOIN ".DB_PREFIXE."contact
        ON etablissement.etablissement = contact.etablissement
    LEFT JOIN ".DB_PREFIXE."contact_civilite
        ON contact.civilite = contact_civilite.contact_civilite
    LEFT JOIN ".DB_PREFIXE."contact_type 
        ON contact.contact_type = contact_type.contact_type AND contact_type.code = 'EXPL'";

$champs=array(
    //Bloc infos générales
    "dossier_instruction.dossier_instruction", 
    "dossier_instruction.libelle as \"libelle\"",
    "dossier_instruction.description as \"description\"",
    "autorite_competente as \"autorite_competente\"",
    "technicien",
    "date_ouverture",
    "dossier_instruction.date_cloture",
    "dossier_instruction.a_qualifier",
    "dossier_instruction.dossier_cloture",
    "dossier_instruction.prioritaire",
    "dossier_instruction.statut",
    //
    "incompletude",
    "piece_attendue",
    //
    "notes",
    //Bloc DC
    "dossier_instruction.dossier_coordination",
    "dossier_coordination.enjeu_erp as \"dossier_coordination_enjeu_erp\"",
    "dossier_coordination.enjeu_ads as \"dossier_coordination_enjeu_ads\"",
    //Col1
    "'' as \"dossier_coordination_date_demande\"",
    "'' as \"dossier_coordination_date_butoir\"",
    "'' as \"dossier_coordination_date_cloture\"",
    "'' as \"courrier_dernier_arrete\"",
    //Col2
    "'' as \"dossier_coordination_dossier_cloture\"",
    "'' as \"dossier_coordination_autorite_police_encours\"",
    //Col3
    "'' as \"dossier_coordination_etablissement_type\"",
    "'' as \"dossier_coordination_etablissement_categorie\"",
    "'' as \"dossier_coordination_etablissement_locaux_sommeil\"",
    
    //Bloc établissement
    "'' as etablissement_coordonnees",
    "'' as exploitant",
    // "'' as etablissement_type",
    // "'' as etablissement_categorie",
    // "'' as etablissement_locaux_sommeil",
    
    //Bloc urbanisme
    "dossier_coordination.interface_referentiel_ads as \"dossier_coordination_interface_referentiel_ads\"",
    "dossier_coordination.dossier_autorisation_ads as \"dossier_coordination_dossier_autorisation_ads\"",
    "dossier_coordination.dossier_instruction_ads as \"dossier_coordination_dossier_instruction_ads\"",
    
    "dossier_instruction.service",
);

// SELECT
$sql_autorite_competente_by_service="SELECT autorite_competente.autorite_competente, autorite_competente.libelle FROM ".DB_PREFIXE."autorite_competente LEFT JOIN ".DB_PREFIXE."service ON autorite_competente.service = service.service WHERE service.service = <idx> ORDER BY autorite_competente.libelle ASC";
//
$statut = array(
    array("", "annule", "a_programmer", "a_poursuivre", "programme",),
    array(_("Choisir")." "._("statut"), _("annule"), _("a_programmer"), _("a_poursuivre"), _("programme"),),
);

$sql_etablissement="SELECT etablissement.etablissement, CONCAT(etablissement.code,' - ',etablissement.libelle) FROM ".DB_PREFIXE."etablissement WHERE ((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))) ORDER BY etablissement.libelle ASC";
$sql_etablissement_by_id = "SELECT etablissement.etablissement, CONCAT(etablissement.code,' - ',etablissement.libelle) FROM ".DB_PREFIXE."etablissement WHERE etablissement = <idx>";

?>
