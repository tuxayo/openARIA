<?php
/**
 * Ce scrip permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction_tous_visites.inc.php";

// Titre de la page
$ent = _("dossiers")." -> "._("visites")." -> "._("mes visites a realiser");

// Titre de l'onglet
$tab_title = _("Visite");

// SELECT 
$champAffiche = array(
    // Id nécessaire pour le lien consulter vers le DI
    'dossier_instruction.dossier_instruction as "'._("id").'"',
    //
    'dossier_instruction.libelle as "'._("DI").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    //
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
    'autorite_competente.code as "'._("autorite_competente").'"',
    "to_char(visite.date_visite,'TMDay') as \""._("jour")."\"",
    "to_char(visite.date_visite,'DD/MM/YYYY') as \""._("date")."\"",
    'CONCAT(\''._(" de ").'\', visite.heure_debut, \''._(" à ").'\', visite.heure_fin) as "'._("horaire").'"',
    'CASE 
        WHEN visite.programmation_version_annulation IS NOT NULL AND 
            visite.programmation_version_annulation > 1 
            THEN \''._("annulée").'\'
        WHEN visite.programmation_version_modification IS NOT NULL AND 
            visite.programmation_version_modification=programmation.version AND 
            programmation_etat.code != \'VAL\' 
            THEN \''._("en cours de modification").'\'
        ELSE
            \''._("planifiée").'\'
    END as "'._("etat").'"',
    );

// FROM 
$table = DB_PREFIXE."visite
LEFT JOIN ".DB_PREFIXE."dossier_instruction
    ON dossier_instruction.dossier_instruction = visite.dossier_instruction
LEFT JOIN ".DB_PREFIXE."autorite_competente 
    ON dossier_instruction.autorite_competente=autorite_competente.autorite_competente 
LEFT JOIN ".DB_PREFIXE."dossier_coordination
    ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
LEFT JOIN ".DB_PREFIXE."etablissement
    ON etablissement.etablissement = dossier_coordination.etablissement
LEFT JOIN ".DB_PREFIXE."arrondissement 
    ON etablissement.adresse_arrondissement=arrondissement.arrondissement 
LEFT JOIN ".DB_PREFIXE."voie 
    ON etablissement.adresse_voie=voie.voie 
LEFT JOIN ".DB_PREFIXE."acteur
    ON acteur.acteur = visite.acteur 
LEFT JOIN ".DB_PREFIXE."om_utilisateur
    ON acteur.om_utilisateur = om_utilisateur.om_utilisateur 
LEFT JOIN ".DB_PREFIXE."programmation
    ON visite.programmation = programmation.programmation
LEFT JOIN ".DB_PREFIXE."programmation_etat
    ON programmation.programmation_etat = programmation_etat.programmation_etat
";

// ORDER BY
$tri = "ORDER BY visite.date_visite ASC, visite.heure_debut ASC NULLS LAST";

// Modification du WHERE
$selection = " WHERE
om_utilisateur.login = '".$_SESSION["login"]."' 
AND visite.date_visite >= NOW() ";

// On enlève la colonne technicien inutile dans son contexte
$champAffiche = array_diff(
    $champAffiche,
    array('acteur.nom_prenom as "'._("technicien").'"', )
);
//
$champRecherche = array();

//
$obj = "dossier_instruction_tous_visites";
// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;
// Actions a gauche : consulter
$tab_actions['left']['consulter'] =
    array('lien' => 'form.php?obj='.$obj.'&amp;action=3'.'&amp;idx=',
          'id' => '&amp;premier='.$premier.'&amp;advs_id='.$advs_id.'&amp;recherche='.$recherche1.'&amp;tricol='.$tricol.'&amp;selectioncol='.$selectioncol.'&amp;valide='.$valide.'&amp;retour=tab',
          'lib' => '<span class="om-icon om-icon-16 om-icon-fix consult-16" title="'._('Consulter').'">'._('Consulter').'</span>',
          'rights' => array('list' => array($obj, $obj.'_consulter'), 'operator' => 'OR'),
          'ordre' => 10,);
// Action du contenu : consulter
$tab_actions['content'] = $tab_actions['left']['consulter'];
$obj = "visite_mes_visites_a_realiser";

// Initialisation des options de l'écran de listing
$options = array();
// Désactivation de la recherche simple
$option = array(
  "type" => "search",
  "display" => false,
);
$options[] = $option;

?>
