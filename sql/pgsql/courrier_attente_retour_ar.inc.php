<?php
/**
 * @package openads
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/courrier.inc.php";

// Fil d'ariane
$ent = _("suivi")." -> "._("documents generes")." -> "._("attente retour AR");
if (!empty($retourformulaire)) {
    $ent = _("documents générés en attente de retour AR");
}

// Nom de l'onglet
$tab_title = _("document généré en attente de retour AR");

// CONDITION
$selection = "
WHERE courrier.finalise IS TRUE
    AND (courrier.date_retour_signature IS NOT NULL
        OR courrier.date_envoi_rar IS NOT NULL)
    AND courrier.date_retour_rar IS NULL
";
// Filtre par service
if (!is_null($_SESSION['service'])) {
    $selection .= "
        AND (dossier_instruction.service = ".$_SESSION['service']."  
            OR dossier_instruction.service is NULL)
    ";
}

// TRI
$tri = "
ORDER BY courrier.date_finalisation ASC NULLS LAST
";

?>
