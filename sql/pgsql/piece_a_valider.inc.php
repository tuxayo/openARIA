<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont à
 * valider, c'est-à-dire les pièces dont le statut est "qualifié".
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/piece.inc.php');

// Fil d'ariane
$ent = _("suivi")." -> "._("documents entrants")." -> "._("a valider");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".$idz."&nbsp;";
}

// SELECT 
$champAffiche = array(
    'piece.piece as "'._("piece").'"',
    'piece.nom as "'._("nom").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    );

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;

//
$selection = " WHERE piece_statut.code = 'QUALIF' ";

//
$trie = " ORDER BY piece.date_creation ";

?>
