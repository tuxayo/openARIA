<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/autorite_police.inc.php";

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = NULL;

//
$tab_title = _("AP");

// FROM
$table = DB_PREFIXE."autorite_police
    LEFT JOIN ".DB_PREFIXE."autorite_police_decision 
        ON autorite_police.autorite_police_decision=autorite_police_decision.autorite_police_decision 
    LEFT JOIN ".DB_PREFIXE."autorite_police_motif 
        ON autorite_police.autorite_police_motif=autorite_police_motif.autorite_police_motif 
    LEFT JOIN ".DB_PREFIXE."dossier_coordination 
        ON autorite_police.dossier_coordination=dossier_coordination.dossier_coordination 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion 
        ON autorite_police.dossier_instruction_reunion=dossier_instruction_reunion.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."dossier_instruction_reunion as dossier_instruction_reunion_prochain
        ON autorite_police.dossier_instruction_reunion_prochain=dossier_instruction_reunion_prochain.dossier_instruction_reunion 
    LEFT JOIN ".DB_PREFIXE."etablissement 
        ON autorite_police.etablissement=etablissement.etablissement 
    LEFT JOIN ".DB_PREFIXE."service 
        ON autorite_police.service=service.service
    LEFT JOIN ".DB_PREFIXE."reunion_type
        ON dossier_instruction_reunion.reunion_type = reunion_type.reunion_type
    LEFT JOIN ".DB_PREFIXE."dossier_instruction
        ON dossier_instruction_reunion.dossier_instruction = dossier_instruction.dossier_instruction ";


//
$displayed_field_etablissement = 'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"';
$displayed_field_dc = 'dossier_coordination.libelle as "'._("dc").'"';

// SELECT 
$champAffiche = array(
    'autorite_police.autorite_police as "'._("autorite_police").'"',
    $displayed_field_etablissement,
    'autorite_police_decision.libelle as "'._("autorite_police_decision").'"',
    'to_char(autorite_police.date_decision ,\'DD/MM/YYYY\') as "'._("date_decision").'"',
    'autorite_police.delai as "'._("delai").'"',
    'autorite_police_motif.libelle as "'._("autorite_police_motif").'"',
    "case autorite_police.cloture when 't' then 'Oui' else 'Non' end as \""._("cloture")."\"",
    'to_char(autorite_police.date_notification ,\'DD/MM/YYYY\') as "'._("date_notification").'"',
    'to_char(autorite_police.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'service.libelle as "'._("service").'"',
    'dossier_instruction_reunion.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    $displayed_field_dc,
    $displayed_field_etablissement,
    'dossier_instruction_reunion_prochain.dossier_instruction as "'._("dossier_instruction_reunion_prochain").'"',
);

//
$champRecherche = array(
    'autorite_police.autorite_police as "'._("autorite_police").'"',
    $displayed_field_etablissement,
    'autorite_police_decision.libelle as "'._("autorite_police_decision").'"',
    'autorite_police.delai as "'._("delai").'"',
    'autorite_police_motif.libelle as "'._("autorite_police_motif").'"',
    'service.libelle as "'._("service").'"',
    'dossier_instruction_reunion.dossier_instruction as "'._("dossier_instruction_reunion").'"',
    $displayed_field_dc,
    'dossier_instruction_reunion_prochain.dossier_instruction as "'._("dossier_instruction_reunion_prochain").'"',
);

// Dans le contexte du DC, les colonnes 'etablissement' et 'dc' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($displayed_field_etablissement, $displayed_field_dc, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($displayed_field_etablissement, $displayed_field_dc, )
    );
}
// Dans le contexte de l'établissement, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    $champAffiche = array_diff(
        $champAffiche,
        array($displayed_field_etablissement, )
    );
    $champRecherche = array_diff(
        $champRecherche,
        array($displayed_field_etablissement, )
    );
}

?>
