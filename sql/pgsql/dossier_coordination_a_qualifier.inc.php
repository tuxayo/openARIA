<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à qualifier.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = _("dossiers")." -> "._("DC")." -> "._("dossiers a qualifier");

//
$redlimit = 15;
if (isset($f) && $f->getParameter("dc_a_qualifier_redlimit")) {
    $redlimit = intval($f->getParameter("dc_a_qualifier_redlimit"));
}
//
$case_ddp = "case 
when dossier_coordination.date_demande < now() - interval '".intval($redlimit)." days'
    then 'rouge'
when dossier_coordination.depot_de_piece is TRUE
    then 'vert'
end";

// SELECT 
$champAffiche = array(
    'dossier_coordination.dossier_coordination as "'._("dossier_coordination").'"',
    'dossier_coordination.libelle as "'._("DC").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    "case dossier_coordination.erp when 't' then 'Oui' else 'Non' end as \""._("erp")."\"",
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    "case dossier_coordination.dossier_cloture when 't' then 'Oui' else 'Non' end as \""._("dossier_cloture")."\"",
    $case_ddp." as ddp",
);

// Filtre du listing
$selection = " WHERE dossier_coordination.a_qualifier = TRUE";

// Pas de recherche avancée
$options = array();
// On affiche le champ lu en gras
$options[] = array(
    "type" => "condition",
    "field" => $case_ddp,
    "case" => array(
        "0" => array(
            "values" => array("rouge", ),
            "style" => "ligne-rouge",
        ),
        "1" => array(
            "values" => array("vert", ),
            "style" => "ligne-vert",
        ),
    ),
);


?>
