<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

include "../gen/sql/pgsql/dossier_coordination_message.form.inc.php";

//
$champs = array(
    "dossier_coordination_message",
    "categorie",
    "dossier_coordination",
    "type",
    "emetteur",
    "date_emission",
    "contenu",
    "contenu_json",
    "si_mode_lecture",
    "si_cadre_lu",
    "si_technicien_lu",
    "acc_mode_lecture",
    "acc_cadre_lu",
    "acc_technicien_lu",
);

?>
