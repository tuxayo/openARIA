<?php
/**
 * Surcharge de la classe dossier_coordination pour afficher directement
 * seulement les dossiers de coordination à clore.
 * 
 * @package openaria
 * @version SVN : $Id: dossier_coordination_a_cloturer.inc.php 1151 2015-03-12 18:29:52Z fmichon $
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = _("dossiers")." -> "._("DC")." -> "._("dossiers a cloturer");

// SELECT 
$champAffiche = array(
    'dossier_coordination.dossier_coordination as "'._("dossier_coordination").'"',
    'dossier_coordination.libelle as "'._("DC").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    "case dossier_coordination.erp when 't' then 'Oui' else 'Non' end as \""._("erp")."\"",
    'to_char(dossier_coordination.date_demande ,\'DD/MM/YYYY\') as "'._("date_demande").'"',
    'to_char(dossier_coordination.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    "case dossier_coordination.a_qualifier when 't' then 'Oui' else 'Non' end as \""._("a_qualifier")."\"",
    );

// Filtre du listing
$selection = " WHERE dossier_coordination.dossier_cloture = FALSE
                 AND dossier_coordination.dossier_coordination NOT IN (
                   SELECT dossier_coordination
                   FROM ".DB_PREFIXE."dossier_instruction
                   WHERE  dossier_instruction.dossier_cloture IS FALSE
                   GROUP BY dossier_coordination HAVING count(*)>0
                 )";

// Pas de recherche avancée
$options = array();

?>
