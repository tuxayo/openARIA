<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui sont
 * suivi et dont la date butoir est dépassée.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/piece.inc.php');

// Fil d'ariane
$ent = _("suivi")." -> "._("documents entrants")." -> "._("suivi");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".$idz."&nbsp;";
}

// SELECT 
$champAffiche = array(
    'piece.piece as "'._("piece").'"',
    'piece.nom as "'._("nom").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    'dossier_coordination.libelle as "'._("dossier_coordination").'"',
    'dossier_instruction.libelle as "'._("dossier_instruction").'"',
    'to_char(piece.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'piece_statut.libelle as "'._("piece_statut").'"',
    );

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;

//
$selection = " WHERE piece.suivi = true
    AND (piece.date_butoir <= NOW() OR piece.date_butoir is NULL)";

//
$trie = " ORDER BY piece.date_butoir ";

?>
