<?php
//$Id$ 
//gen openMairie le 10/08/2016 17:34

include "../gen/sql/pgsql/contrainte.form.inc.php";

$champs=array(
    "contrainte",
    // Contrainte
    "libelle",
    "nature",
    "lie_a_un_referentiel",
    "id_referentiel",
    "ordre_d_affichage",
    // Catégorie
    "groupe",
    "sousgroupe",
    // Détail
    "texte",
    "texte_surcharge",
    "om_validite_debut",
    "om_validite_fin"
);

// Ajout des tableaux des données pour les select
$nature = array(
    array("PLU", "POS", "CC", "RNU"), 
    array(_("PLU"), _("POS"), _("CC"), _("RNU")),
);

?>
