<?php
//$Id$ 
//gen openMairie le 10/08/2016 17:34

include "../gen/sql/pgsql/contrainte.inc.php";

// SELECT
$champAffiche = array(
    'contrainte.contrainte as "'._("contrainte").'"',
    'contrainte.libelle as "'._("libelle").'"',
    'contrainte.nature as "'._("nature").'"',
    'contrainte.groupe as "'._("groupe").'"',
    'contrainte.sousgroupe as "'._("sousgroupe").'"',
    "case contrainte.lie_a_un_referentiel when 't' then 'Oui' else 'Non' end as \""._("lie_a_un_referentiel")."\"",
    'to_char(contrainte.om_validite_debut ,\'DD/MM/YYYY\') as "'._("om_validite_debut").'"',
    'to_char(contrainte.om_validite_fin ,\'DD/MM/YYYY\') as "'._("om_validite_fin").'"',
);

// Supprime les sous-formulaires
$sousformulaire = array();


?>
