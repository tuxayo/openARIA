<?php
/**
 * Surcharge de dossier_coordination pour afficher directement
 * le formulaire spécifique de géolocalisation depuis le menu Administration & Paramétrage.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = _("administration_parametrage")." -> "._("options avancées")." -> "._("géolocalisation");

$sousformulaire = array();
$form_title = _("géolocalisation");

?>
