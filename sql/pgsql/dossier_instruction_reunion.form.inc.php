<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_instruction_reunion.form.inc.php";

// Filtre par service
$sql_reunion_type_by_service="
SELECT 
    reunion_type.reunion_type, reunion_type.libelle 
FROM ".DB_PREFIXE."reunion_type 
WHERE reunion_type.service = <idx_service>
    AND (((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) 
    OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))))
ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_service_by_id = "
SELECT 
    reunion_type.reunion_type, reunion_type.libelle 
FROM ".DB_PREFIXE."reunion_type 
WHERE reunion_type.service = <idx_service>
    AND reunion_type = <idx>";

// Filtre par reunion_type
$sql_reunion_type_categorie="
SELECT reunion_categorie.reunion_categorie, reunion_categorie.libelle 
FROM 
    ".DB_PREFIXE."reunion_categorie
        LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_categorie
            ON reunion_categorie.reunion_categorie=reunion_type_reunion_categorie.reunion_categorie
WHERE 
    ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE))) 
    AND reunion_type_reunion_categorie.reunion_type=<reunion_type>
ORDER BY reunion_categorie.libelle ASC";

// Filtre par reunion_type
$sql_reunion_avis = "
SELECT reunion_avis.reunion_avis, reunion_avis.libelle 
FROM 
	".DB_PREFIXE."reunion_avis
		LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_avis
            ON reunion_avis.reunion_avis=reunion_type_reunion_avis.reunion_avis
WHERE 
	((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE))) 
	AND reunion_type_reunion_avis.reunion_type=<reunion_type>
ORDER BY reunion_avis.libelle ASC";
$sql_avis = $sql_reunion_avis;
$sql_proposition_avis = $sql_reunion_avis;

?>
