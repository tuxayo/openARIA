<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/visite.form.inc.php');

$champs=array(
    "visite",
    "programmation",
    "date_creation",
    "'' as etablissement",
    "visite_etat",
    "convocation_exploitants",
    "dossier_instruction",
    "acteur",
    "date_visite",
    "heure_debut",
    "heure_fin",
    "programmation_version_creation",
    "programmation_version_modification",
    "programmation_version_annulation",
    "date_annulation",
    "visite_motif_annulation",
    "observation",
    "a_poursuivre",
    "courrier_convocation_exploitants",
    "courrier_annulation",
);

$sql_etablissement_by_id_visite = "
    SELECT
        etablissement.etablissement as id,
        CONCAT(etablissement.code, ' - ', etablissement.libelle) as libelle,
        lower(etablissement_nature.code) as code
    FROM ".DB_PREFIXE."visite
    LEFT JOIN ".DB_PREFIXE."dossier_instruction
        ON dossier_instruction.dossier_instruction = visite.dossier_instruction
    LEFT JOIN ".DB_PREFIXE."dossier_coordination
        ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
    LEFT JOIN ".DB_PREFIXE."etablissement
        ON etablissement.etablissement = dossier_coordination.etablissement
    LEFT JOIN ".DB_PREFIXE."etablissement_nature
        ON etablissement_nature.etablissement_nature = etablissement.etablissement_nature
    WHERE visite.visite = <idx>
";

?>
