<?php
/**
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/etablissement_tous.inc.php');

// Fil d'ariane
$ent = _("etablissements")." -> "._("referentiel ERP");

// Affiche uniquement les établissements ERP et masque les établissements archivés le cas échéant

if (isset($_GET['valide']) and $_GET['valide'] == 'false'){
	$selection = "WHERE etablissement_nature.nature = 'ERP Référentiel'";
}
else{
	$selection=" WHERE
	(etablissement_nature.nature = 'ERP Référentiel')
	AND	
	(((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))))";
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe') {
    $tmp_advsearch_options['export'] = array(
        'geoaria' => array(
            'right' => 'etablissement_referentiel_erp_geoaria',
            'url' => '../scr/form.php?obj=etablissement_referentiel_erp&action=31&idx=0',
        ),
    );
}

$options[] = $tmp_advsearch_options;

?>
