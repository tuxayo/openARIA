<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/lien_reunion_r_instance_r_i_membre.form.inc.php";

// Filtre par service
$sql_membre = "
SELECT 
    reunion_instance_membre.reunion_instance_membre, 
    reunion_instance_membre.membre 
FROM ".DB_PREFIXE."reunion_instance_membre
WHERE
    reunion_instance_membre.reunion_instance = <idx_instance>
    AND ((reunion_instance_membre.om_validite_debut IS NULL 
          AND (reunion_instance_membre.om_validite_fin IS NULL 
               OR reunion_instance_membre.om_validite_fin > CURRENT_DATE)) 
         OR (reunion_instance_membre.om_validite_debut <= CURRENT_DATE 
             AND (reunion_instance_membre.om_validite_fin IS NULL 
                  OR reunion_instance_membre.om_validite_fin > CURRENT_DATE))) 
ORDER BY reunion_instance_membre.membre ASC
";
$sql_membre_by_id = "
SELECT 
    reunion_instance_membre.reunion_instance_membre, 
    reunion_instance_membre.membre 
FROM ".DB_PREFIXE."reunion_instance_membre
WHERE 
    reunion_instance_membre.reunion_instance = <idx_instance>
    AND reunion_instance_membre = <idx>
";

?>
