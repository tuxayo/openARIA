<?php
/**
 * Listing des UA archivées dans le contexte d'un établissement.
 *
 * @package openaria
 * @version SVN : $Id: etablissement_unite.inc.php 1172 2015-03-17 13:45:35Z nmeucci $
 */

//
include "../sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = _("etablissements")." -> "._("unites d'accessibilite");

//
$tab_title = _("UA");

//
$champAffiche = array_merge(
    $champAffiche_ua,
    $champAffiche_cas1, 
    $champAffiche_etat,
    $champAffiche_derogation
);


// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])
    && isset($idx)) {
    $selection = " 
WHERE 
    (etablissement_unite.etablissement = '".$idx."') 
    AND etablissement_unite.archive IS TRUE 
";
} else {
    $selection = " WHERE NULL ";
}

// Aucune recherche dans ce contexte
$champRecherche= array(
);

// Aucune action d'ajout d'une UA n'est possible depuis ce listing 
$tab_actions['corner']['ajouter'] = null;

?>
