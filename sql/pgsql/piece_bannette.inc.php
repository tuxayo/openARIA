<?php
/**
 * Surcharge de la classe piece pour afficher seulement les pièces qui ne sont
 * pas liés à un etab/DC/DI.
 *
 * @package openaria
 * @version SVN : $Id$
 */

include('../sql/pgsql/piece.inc.php');

// Fil d'ariane
$ent = _("suivi")." -> "._("documents entrants")." -> "._("bannette");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= "&nbsp;".$idz."&nbsp;";
}

//
$selection = " WHERE piece.etablissement IS NULL
    AND piece.dossier_coordination IS NULL
    AND piece.dossier_instruction IS NULL ";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
