<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/proces_verbal.form.inc.php";

$champs = array(
    "proces_verbal",
    "numero",
    "dossier_instruction",
    "dossier_instruction_reunion",
    "date_redaction",
    "genere",
    "signataire",
    "modele_edition",
    // Le champ 'om_fichier_finalise' n'existe pas en base de données.
    // Il a été ajouté simplement pour afficher dans la fiche de 
    // visualisation du procès verbal un lien vers le fichier finalisé
    // qui est en réalité stocké dans la table courrier (document généré).
    "'' as om_fichier_finalise",
    "om_fichier_signe",
    "courrier_genere",
);

// champs select
$sql_demande = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, concat(to_char(dossier_instruction_reunion.date_souhaitee,'DD/MM/YYYY'), ' - ', reunion_type.libelle)
    FROM ".DB_PREFIXE."dossier_instruction_reunion
    LEFT JOIN ".DB_PREFIXE."reunion_type
    ON reunion_type.reunion_type = dossier_instruction_reunion.reunion_type
    WHERE dossier_instruction_reunion.dossier_instruction = <idx_di>
    ORDER BY dossier_instruction_reunion.date_souhaitee DESC, reunion_type.libelle ASC";
$sql_demande_by_id = "SELECT dossier_instruction_reunion.dossier_instruction_reunion, concat(to_char(dossier_instruction_reunion.date_souhaitee,'DD/MM/YYYY'), ' - ', reunion_type.libelle)
    FROM ".DB_PREFIXE."dossier_instruction_reunion
    LEFT JOIN ".DB_PREFIXE."reunion_type
    ON reunion_type.reunion_type = dossier_instruction_reunion.reunion_type
    WHERE dossier_instruction_reunion = <idx>";
// Change le libellé du select des signataires
$sql_signataire="SELECT signataire.signataire, CONCAT_WS(' ', contact_civilite.libelle, signataire_qualite.libelle, signataire.nom, signataire.prenom) FROM ".DB_PREFIXE."signataire LEFT JOIN ".DB_PREFIXE."contact_civilite ON signataire.civilite = contact_civilite.contact_civilite LEFT JOIN ".DB_PREFIXE."signataire_qualite ON signataire.signataire_qualite = signataire_qualite.signataire_qualite WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE))) ORDER BY signataire.nom ASC";
$sql_signataire_by_id = "SELECT signataire.signataire, CONCAT_WS(' ', contact_civilite.libelle, signataire_qualite.libelle, signataire.nom, signataire.prenom) FROM ".DB_PREFIXE."signataire LEFT JOIN ".DB_PREFIXE."contact_civilite ON signataire.civilite = contact_civilite.contact_civilite LEFT JOIN ".DB_PREFIXE."signataire_qualite ON signataire.signataire_qualite = signataire_qualite.signataire_qualite WHERE signataire = <idx>";

?>
