<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reunion.form.inc.php";

// Filtre par service
$sql_reunion_type_by_service="
SELECT 
    reunion_type.reunion_type, reunion_type.libelle 
FROM ".DB_PREFIXE."reunion_type 
WHERE reunion_type.service = <idx_service>
    AND (((reunion_type.om_validite_debut IS NULL AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE)) 
    OR (reunion_type.om_validite_debut <= CURRENT_DATE AND (reunion_type.om_validite_fin IS NULL OR reunion_type.om_validite_fin > CURRENT_DATE))))
ORDER BY reunion_type.libelle ASC";
$sql_reunion_type_by_service_by_id = "
SELECT 
    reunion_type.reunion_type, reunion_type.libelle 
FROM ".DB_PREFIXE."reunion_type 
WHERE reunion_type.service = <idx_service>
    AND reunion_type = <idx>";

?>
