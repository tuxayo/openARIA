<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/dossier_coordination_message.inc.php";

//
$tab_title = _("message");

//
$table .= " LEFT JOIN ".DB_PREFIXE."etablissement
        ON dossier_coordination.etablissement = etablissement.etablissement ";

// Cache le bouton ajouter
// Il n'est pas prévu d'ajouter des messages via l'interface utilisateur
$tab_actions['corner']['ajouter'] = null;

//
$case_lu_non_lu = "
WHEN 't' THEN '<span class=''om-icon om-icon-16 om-icon-fix read-16'' title=''"._('Lu')."''>Lu</span>' 
ELSE '<span class=''om-icon om-icon-16 om-icon-fix unread-16'' title=''"._('Non Lu')."''>Non Lu</span>'
";
$empty_marqueur = "-";
$case_si_cadre_lu = "
CASE 
    dossier_coordination_message.si_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN CASE dossier_coordination_message.si_cadre_lu ".$case_lu_non_lu." END
    WHEN 'mode2' THEN '".$empty_marqueur."'
    WHEN 'mode3' THEN CASE dossier_coordination_message.si_cadre_lu ".$case_lu_non_lu." END
END
";
$case_si_technicien_lu = "
CASE 
    dossier_coordination_message.si_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN '".$empty_marqueur."'
    WHEN 'mode2' THEN CASE dossier_coordination_message.si_technicien_lu  ".$case_lu_non_lu." END
    WHEN 'mode3' THEN CASE dossier_coordination_message.si_technicien_lu  ".$case_lu_non_lu." END
END
";
$case_acc_cadre_lu = "
CASE 
    dossier_coordination_message.acc_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN CASE dossier_coordination_message.acc_cadre_lu ".$case_lu_non_lu." END
    WHEN 'mode2' THEN '".$empty_marqueur."'
    WHEN 'mode3' THEN CASE dossier_coordination_message.acc_cadre_lu ".$case_lu_non_lu." END
END
";
$case_acc_technicien_lu = "
CASE 
    dossier_coordination_message.acc_mode_lecture 
    WHEN 'mode0' THEN '".$empty_marqueur."'
    WHEN 'mode1' THEN '".$empty_marqueur."'
    WHEN 'mode2' THEN CASE dossier_coordination_message.acc_technicien_lu  ".$case_lu_non_lu." END
    WHEN 'mode3' THEN CASE dossier_coordination_message.acc_technicien_lu  ".$case_lu_non_lu." END
END
";

//
$champRecherche = array(
    'dossier_coordination_message.type as "'._("type").'"',
    'dossier_coordination_message.categorie as "'._("categorie").'"',
    'dossier_coordination_message.emetteur as "'._("emetteur").'"',
);

// Tri par date
$tri = "order by date_emission desc";

// Options
if (!isset($options)) {
    $options = array();
}
// On affiche le champ lu en gras
$options[] = array(
    "type" => "condition",
    "field" => "dossier_coordination_message.categorie",
    "case" => array(
        "0" => array(
            "values" => array("interne", ),
            "style" => "message-categorie-interne",
        ),
        "1" => array(
            "values" => array("entrant", ),
            "style" => "message-categorie-entrant",
        ),
        "2" => array(
            "values" => array("sortant", ),
            "style" => "message-categorie-sortant",
        ),
    ),
);

?>
