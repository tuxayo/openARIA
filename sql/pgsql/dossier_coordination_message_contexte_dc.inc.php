<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination_message.inc.php";

/**
 * Récupération de la configuration de la requête à partir du widget.
 */
//
require_once "../obj/pilotage.class.php";
$p = new pilotage();
$conf = $p->get_config_message_mes_non_lu();

// Fil d'Ariane
$ent = _("message");

// SELECT
$champAffiche = array(
    'dossier_coordination_message.dossier_coordination_message as "'._("identifiant").'"',
    'to_char(dossier_coordination_message.date_emission ,\'DD/MM/YYYY HH24:MI:SS\') as "'._("date").'"',
    'dossier_coordination_message.type as "'._("type").'"',
    'dossier_coordination_message.categorie as "'._("categorie").'"',
    'dossier_coordination_message.emetteur as "'._("emetteur").'"',
);
$table = $conf["query_ct_from"];

if (!isset($f)) {
    return;
}

$case_specific_lu = "''";
if ($f->is_user_with_role_and_service("cadre", "si") === true) {
    $champAffiche[] = $case_si_cadre_lu." as \""._("cadre si")."\" ";
    $champAffiche[] = $case_si_technicien_lu." as \""._("tech si")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("technicien", "si") === true) {
    $champAffiche[] = $case_si_cadre_lu." as \""._("cadre si")."\" ";
    $champAffiche[] = $case_si_technicien_lu." as \""._("tech si")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("cadre", "acc") === true) {
    $champAffiche[] = $case_acc_cadre_lu." as \""._("cadre acc")."\" ";
    $champAffiche[] = $case_acc_technicien_lu." as \""._("tech acc")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
} elseif ($f->is_user_with_role_and_service("technicien", "acc") === true) {
    $champAffiche[] = $case_acc_cadre_lu." as \""._("cadre acc")."\" ";
    $champAffiche[] = $case_acc_technicien_lu." as \""._("tech acc")."\" ";
    $case_specific_lu = "CASE WHEN ".$conf["query_ct_where"]." THEN 'Non' END";
}
$champAffiche[] = $case_specific_lu." as \""._("lu")."\" ";
$options[] = array(
    "type" => "condition",
    "field" => $case_specific_lu,
    "case" => array(
        "0" => array(
            "values" => array("Non", ),
            "style" => "non_lu",
        ),
    ),
);

?>
