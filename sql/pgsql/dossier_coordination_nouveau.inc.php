<?php
/**
 * Surcharge de dossier_coordination pour afficher directement
 * le formulaire en mode ajouter depuis le menu.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_coordination.inc.php";

// Fil d'Ariane
$ent = _("dossiers")." -> "._("DC")." -> "._("nouveau dossier");

// Cache les onglets
$sousformulaire = array();

?>
