<?php
/**
 * Surcharge de la classe autorite_police pour afficher
 * seulement les autorités de police non notifiées ou executées.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/autorite_police.inc.php');

// Fil d'Ariane
$ent = _("dossiers")." -> "._("autorite de police")." -> "._("AP non notifiees ou executees");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "->&nbsp;".$idx."&nbsp;";
}

// Filtre du listing
$selection = " WHERE
(autorite_police.date_butoir < NOW()
AND (autorite_police.dossier_instruction_reunion_prochain IS NULL
    OR dossier_instruction_reunion_prochain.reunion IS NULL))
OR ((LOWER(autorite_police_decision.avis) = LOWER('favorable')
    OR LOWER(autorite_police_decision.avis) = LOWER('defavorable'))
AND autorite_police.autorite_police NOT IN 
    (SELECT autorite_police 
    FROM ".DB_PREFIXE."lien_autorite_police_courrier 
    WHERE courrier IS NOT NULL)
)
";

// Colonne établissement
$displayed_field_etablissement =
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"';
// SELECT 
$champAffiche = array(
    'autorite_police.autorite_police as "'._("autorite_police").'"',
    $displayed_field_etablissement,
    'autorite_police_decision.libelle as "'._("autorite_police_decision").'"',
    'to_char(autorite_police.date_decision ,\'DD/MM/YYYY\') as "'._("date_decision").'"',
    'autorite_police.delai as "'._("delai").'"',
    'autorite_police_motif.libelle as "'._("autorite_police_motif").'"',
    "case autorite_police.cloture when 't' then 'Oui' else 'Non' end as \""._("cloture")."\"",
    'to_char(autorite_police.date_notification ,\'DD/MM/YYYY\') as "'._("date_notification").'"',
    'to_char(autorite_police.date_butoir ,\'DD/MM/YYYY\') as "'._("date_butoir").'"',
    'service.libelle as "'._("service").'"',
    'concat(dossier_instruction.libelle, \' - \', reunion_type.libelle, \' '._("du").' \', to_char(dossier_instruction_reunion.date_souhaitee ,\'DD/MM/YYYY\')) as "'._("demande passage en reunion").'"',
);
//

//
$sousformulaire = array();

?>
