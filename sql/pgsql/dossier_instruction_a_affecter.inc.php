<?php
/**
 * Ce scrip permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction.inc.php";

// Titre de la page
$ent = _("dossiers")." -> "._("DI")." -> "._("dossiers a affecter");
if (isset($idx) && $idx == '0') {
	$ent .= " -> "._("affectation par lot");
}

// Filtre du listing
$selection = " WHERE dossier_instruction.technicien is NULL ";

// On enlève la colonne technicien inutile dans son contexte
$champAffiche = array_diff(
    $champAffiche,
    array('acteur.nom_prenom as "'._("technicien").'"', )
);

// Pas de recherche avancée
$options = array();

// Lien vers l'interface d'affectation des dossiers par lot
$tab_actions['corner']['affecter-par-lot'] = array(
    "lib" => '<span class="om-icon om-icon-16 om-icon-fix affectation-16" title="'._('Affecter par lot').'">'._('Affecter par lot').'</span>',
    "lien" => "../scr/form.php?obj=".$obj."&amp;action=8&amp;idx=0",
    "id" => "",
    "rights" => array('list' => array($obj, $obj.'_affecter_par_lot'), 'operator' => 'OR'),
);

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
