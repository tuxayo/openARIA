<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/courrier.inc.php";

// Fil d'ariane
$ent = _("suivi")." -> "._("documents generes")." -> "._("gestion");
if (!empty($retourformulaire)) {
    $ent = _("documents generes");
}
//
$tab_title = _("document généré");

//
$table .= " 
LEFT JOIN ".DB_PREFIXE."lien_courrier_contact
    ON lien_courrier_contact.courrier = courrier.courrier
    AND courrier.mailing IS FALSE
LEFT JOIN ".DB_PREFIXE."contact
    ON lien_courrier_contact.contact = contact.contact 
LEFT JOIN ".DB_PREFIXE."acteur
    ON acteur.acteur = dossier_instruction.technicien
 ";

// TRI
$tri = "
ORDER BY courrier.date_finalisation DESC NULLS LAST
";

/**
 * SELECT
 */
//
$displayed_fields_begin = array(
    'courrier.courrier as "'._("courrier").'"',
    'courrier.code_barres as "'._("code_barres").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
);
$displayed_fields_end = array(
    "concat(contact.nom, ' ', contact.prenom, ' ', contact.raison_sociale, ' ',contact.denomination) as \""._("destinataire")."\"",
    "case courrier.finalise when 't' then 'Oui' else 'Non' end as \""._("finalise")."\"",
    'to_char(courrier.om_date_creation ,\'DD/MM/YYYY\') as "'._("om_date_creation").'"',
    'to_char(courrier.date_finalisation ,\'DD/MM/YYYY\') as "'._("date_finalisation").'"',
    "case courrier.mailing when 't' then 'Oui' else 'Non' end as \""._("mailing")."\"",
);

//
$displayed_field_etablissement = 'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"';
$displayed_field_dc = 'dossier_coordination.libelle as "'._("dc").'"';
$displayed_field_di = 'dossier_instruction.libelle as "'._("di").'"';
$displayed_field_di_or_dc = "CASE WHEN dossier_instruction.libelle <> '' 
                    THEN  dossier_instruction.libelle ELSE dossier_coordination.libelle END as \""._("di ou dc")."\"";
$displayed_field_technicien = 'acteur.acronyme as "'._("technicien").'"';

// SELECT
$champAffiche = array_merge(
    $displayed_fields_begin,
    array(
        $displayed_field_etablissement,
        $displayed_field_di_or_dc,
        $displayed_field_technicien,
    ),
    $displayed_fields_end
);

// Dans le contexte de l'établissement, la colonne 'etablissement' est inutile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["etablissement"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array($displayed_field_etablissement, )
    );
}
// Dans le contexte du DC, les colonnes 'etablissement' et 'di_dc' sont inutiles
// mais les colonne 'di' et 'technicien' est utile
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_coordination"])) {
    //
    $champAffiche = array_merge(
        $displayed_fields_begin,
        array(
            $displayed_field_di,
            $displayed_field_technicien,
        ),
        $displayed_fields_end
    );
}
// Dans le contexte du DI, les colonnes 'etablissement' , 'di_dc' et
// 'technicien' sont inutiles
if (isset($retourformulaire)
    && in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {
    //
    $champAffiche = array_diff(
        $champAffiche,
        array(
            $displayed_field_etablissement,
            $displayed_field_di_or_dc,
            $displayed_field_technicien,
        )
    );
}

// Champs de la recherche simple
$champRecherche = array(
    'courrier.code_barres as "'._("code_barres").'"',
    'modele_edition.libelle as "'._("modele_edition").'"',
    'CONCAT(etablissement.code,\' - \',etablissement.libelle) as "'._("etablissement").'"',
    "CASE WHEN dossier_instruction.libelle <> '' 
        THEN  dossier_instruction.libelle
        ELSE dossier_coordination.libelle
    END as \""._("di ou dc")."\"",
    'signataire.nom as "'._("signataire").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    'proces_verbal.numero as "'._("proces_verbal").'"',
    'courrier.arrete_numero as "'._("arrete_numero").'"',
    'acteur.acronyme as "'._("technicien").'"',
);

/**
 *
 */
// Si ce n'est pas un sous-formulaire
if (empty($retourformulaire)
    || in_array($retourformulaire, $foreign_keys_extended["courrier"])) {
    // Cache le bouton ajouter
    $tab_actions['corner']['ajouter'] = null;
}

// Cache les onglets
$sousformulaire = array();

// Si contexte DI
if (in_array($retourformulaire, $foreign_keys_extended["dossier_instruction"])) {

    // Instanciation du DI ou de sa surcharge
    require_once '../obj/'.$retourformulaire.'.class.php';
    $di = new $retourformulaire($idxformulaire);

    // Si service DI différent de celui de l'utilisateur
    if ($di->is_from_good_service() === false) {
        // Désactivation du bouton ajout du soustab
        $tab_actions['corner']['ajouter'] = null;
    }
}

?>
