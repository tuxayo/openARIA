<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/analyses.form.inc.php";

//
$champs = array(
    "analyses.analyses",
    "analyses.service",
    "analyses_etat",
    "analyses_type",
    "objet",
    "descriptif_etablissement_om_html",
    "reglementation_applicable_om_html",
    "compte_rendu_om_html",
    "document_presente_pendant_om_html",
    "document_presente_apres_om_html",
    "observation_om_html",
    "reunion_avis",
    "avis_complement",
    "array_to_string(
        array_agg(
            lien_dossier_coordination_etablissement_type.etablissement_type
            ORDER BY etablissement_type.libelle),
    ';') as etablissement_type_secondaire",
    "si_effectif_public",
    "si_effectif_personnel",
    "si_type_ssi",
    "si_type_alarme",
    "si_conformite_l16",
    "si_alimentation_remplacement",
    "si_service_securite",
    "si_personnel_jour",
    "si_personnel_nuit",
    "acc_handicap_mental",
    "acc_handicap_auditif",
    "acc_places_stationnement_amenagees",
    "acc_elevateur",
    "acc_handicap_physique",
    "acc_ascenseur",
    "acc_handicap_visuel",
    "acc_boucle_magnetique",
    "acc_chambres_amenagees",
    "acc_douche",
    "acc_derogation_scda",
    "acc_sanitaire",
    "acc_places_assises_public",
    "analyses.dossier_instruction",
    "analyses.modele_edition_rapport",
    "analyses.modele_edition_compte_rendu",
    "analyses.modele_edition_proces_verbal",
    "modifiee_sans_gen",
    "dernier_pv",
    "dec1",
    "delai1",
    "dec2",
    "delai2",
);

$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."dossier_instruction
    ON dossier_instruction.dossier_instruction = analyses.dossier_instruction
LEFT JOIN ".DB_PREFIXE."dossier_coordination
    ON dossier_coordination.dossier_coordination = dossier_instruction.dossier_coordination
LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_etablissement_type
    ON lien_dossier_coordination_etablissement_type.dossier_coordination = dossier_coordination.dossier_coordination
LEFT JOIN ".DB_PREFIXE."etablissement_type
    ON etablissement_type.etablissement_type = lien_dossier_coordination_etablissement_type.etablissement_type
";

$selection = " GROUP BY analyses.analyses ";

// filtre les modèles d'édition par type de courrier et valide ou déjà sélectionné
$sql_modele_edition = "SELECT modele_edition.modele_edition, modele_edition.libelle 
FROM ".DB_PREFIXE."modele_edition 
LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
WHERE LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
    AND (
        ((modele_edition.om_validite_debut IS NULL
                AND (modele_edition.om_validite_fin IS NULL
                OR modele_edition.om_validite_fin > CURRENT_DATE))
                OR (modele_edition.om_validite_debut <= CURRENT_DATE
                AND (modele_edition.om_validite_fin IS NULL
                OR modele_edition.om_validite_fin > CURRENT_DATE)))
        <where_modele>)
ORDER BY modele_edition.libelle ASC";

?>
