<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/programmation.form.inc.php');

$champs = array(
	"programmation",
	"semaine_annee",
    "annee",
    "numero_semaine",
    "service",
    "version",
    "date_modification",
    "programmation_etat",
    "convocation_exploitants",
    "convocation_membres",
    "date_envoi_ce",    
    "date_envoi_cm",
    "planifier_nouveau",
);

?>
