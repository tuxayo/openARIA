<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/etablissement.inc.php";

//
$tab_title = _("etablissement");

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
    'lien_contrainte_etablissement',
    'etablissement_unite',
    'contact',
    'dossier_coordination',
    'piece',
    'courrier',
    'autorite_police',
    //'etablissement_parcelle',
    //'etablissement_type_secondaire',
);
$sousformulaire_parameters = array(
    "lien_contrainte_etablissement" => array(
        "title" => _("Contraintes"),
        "href" => "../scr/form.php?obj=etablissement&action=4&idx=".((isset($idx))?$idx:"")."&retourformulaire=".(isset($obj) ? $obj : "etablissement")."&",
    ),
    "autorite_police" => array(
        "title" => _("AP"),
    ),
    "dossier_coordination" => array(
        "title" => _("DC"),
    ),
    "contact" => array(
        "title" => _("Contacts"),
    ),
    "piece" => array(
        "title" => _("Documents entrants"),
    ),
    "courrier" => array(
        "title" => _("Documents generes"),
    ),
    "etablissement_unite" => array(
        "title" => _("UA"),
        "href" => "../scr/form.php?obj=".(isset($obj) ? $obj : "etablissement")."&action=20&idx=".((isset($idx))?$idx:"")."&",
    ),
);

?>
