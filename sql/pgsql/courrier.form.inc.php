<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/courrier.form.inc.php";

//
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_courrier_contact
    ON lien_courrier_contact.courrier = courrier.courrier
LEFT JOIN ".DB_PREFIXE."contact
    ON lien_courrier_contact.contact = contact.contact
LEFT JOIN ".DB_PREFIXE."lien_autorite_police_courrier
    ON lien_autorite_police_courrier.courrier = courrier.courrier
LEFT JOIN ".DB_PREFIXE."autorite_police
    ON lien_autorite_police_courrier.autorite_police = autorite_police.autorite_police
";

//
$champs = array(
    // Filedset courrier
    "courrier.courrier",
    "code_barres",
    "courrier_type",
    "modele_edition",
    "courrier_parent",
    "courrier_joint",
    "complement1_om_html",
    "'' as courrier_texte_type_complement1",
    "complement2_om_html",
    "'' as courrier_texte_type_complement2",

    // Fieldset document
    "om_fichier_finalise_courrier",
    "date_envoi_mail_om_fichier_finalise_courrier",
    "om_fichier_signe_courrier",
    "date_envoi_mail_om_fichier_signe_courrier",
    "signataire",

    // Fieldset suivi
    "om_date_creation",
    "date_finalisation",
    "date_envoi_signature",
    "date_retour_signature",
    "date_envoi_controle_legalite",
    "date_retour_controle_legalite",
    "date_envoi_rar",
    "date_retour_rar",

    // Fieldset statut
    "finalise",
    "mailing",

    // Fieldset Lien
    "courrier.etablissement",
    "courrier.dossier_coordination",
    "dossier_instruction",
    "proces_verbal",
    "visite",
    "arrete_numero",
    "array_to_string(
        array_agg(
            lien_courrier_contact.contact
            ORDER BY contact.contact
        ),
    ';') as contacts_lies",
    "array_to_string(
        array_agg(
            lien_autorite_police_courrier.autorite_police
            ORDER BY autorite_police.autorite_police
        ),
    ';') as autorites_police_liees"
);

// TRI
$tri = " ORDER BY courrier.date_finalisation DESC NULLS LAST ";

//
$selection = " GROUP BY courrier.courrier ";

// Liaison NaN - courrier/contact
$sql_contacts_lies = "
SELECT 
    contact.contact, 
    CONCAT(
        '(',
        contact_type.libelle,
        ') ',
        CASE  
            WHEN contact.qualite = 'particulier'
            THEN
                TRIM(CONCAT_WS(' ', contact_civilite.libelle, contact.nom, contact.prenom))
            ELSE
                CASE
                    WHEN contact.nom IS NOT NULL OR contact.prenom IS NOT NULL
                    THEN 
                        TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination, 'représenté(e) par', contact_civilite.libelle, contact.nom, contact.prenom))
                    ELSE 
                        TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination))
                END
        END
    ) as lib
FROM ".DB_PREFIXE."contact
    LEFT JOIN ".DB_PREFIXE."contact_type 
        ON contact.contact_type = contact_type.contact_type
    LEFT JOIN ".DB_PREFIXE."contact_civilite 
        ON contact.civilite = contact_civilite.contact_civilite
    LEFT JOIN ".DB_PREFIXE."lien_dossier_coordination_contact
         ON contact.contact = lien_dossier_coordination_contact.contact
WHERE 
    contact.etablissement = <idx_etab>
    OR (LOWER(contact_type.code) = LOWER('inst'))
    OR (lien_dossier_coordination_contact.dossier_coordination = <idx_dc>)
ORDER BY contact_type.contact_type, lib";
//
$sql_contacts_lies_by_id = "
SELECT 
    contact.contact,
    CONCAT(
        '(',
        contact_type.libelle,
        ') ',
        CASE  
            WHEN contact.qualite = 'particulier'
            THEN
                TRIM(CONCAT_WS(' ', contact_civilite.libelle, contact.nom, contact.prenom))
            ELSE
                CASE
                    WHEN contact.nom IS NOT NULL OR contact.prenom IS NOT NULL
                    THEN 
                        TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination, 'représenté(e) par', contact_civilite.libelle, contact.nom, contact.prenom))
                    ELSE 
                        TRIM(CONCAT_WS(' ', contact.raison_sociale, contact.denomination))
                END
        END
    ) as lib
FROM ".DB_PREFIXE."contact
    LEFT JOIN ".DB_PREFIXE."contact_type 
        ON contact.contact_type = contact_type.contact_type
    LEFT JOIN ".DB_PREFIXE."contact_civilite 
        ON contact.civilite = contact_civilite.contact_civilite
WHERE contact.contact IN (<idx>)
ORDER BY contact_type.contact_type, lib";

// Liaison NaN - courrier/autorite_police
$sql_autorites_police_liees = "
SELECT autorite_police.autorite_police, concat('AP n°', autorite_police.autorite_police, ' du ', to_char(autorite_police.date_decision ,'DD/MM/YYYY')) as lib
FROM ".DB_PREFIXE."autorite_police
WHERE autorite_police.dossier_coordination = <idx_dc>
ORDER BY autorite_police.autorite_police, lib";
//
$sql_autorites_police_liees_by_id = "
SELECT autorite_police.autorite_police, concat('AP n°', autorite_police.autorite_police, ' du ', to_char(autorite_police.date_decision ,'DD/MM/YYYY')) as lib
FROM ".DB_PREFIXE."autorite_police
WHERE autorite_police.autorite_police IN (<idx>)
ORDER BY autorite_police.autorite_police, lib";

// Liste des champs pour générer le libellé d'un courrier
$list_fields_libelle = array(
    "courrier.courrier",
    "to_char(courrier.om_date_creation ,'DD/MM/YYYY')",
    "CASE 
        WHEN dossier_instruction.libelle IS NOT NULL THEN dossier_instruction.libelle 
        WHEN dossier_coordination.libelle IS NOT NULL THEN dossier_coordination.libelle 
        WHEN etablissement.libelle IS NOT NULL THEN etablissement.libelle 
    END",
    "courrier_type.code",
    "modele_edition.code",
    "CONCAT (contact_type.libelle, ' ', contact_civilite.libelle, ' ', contact.nom, ' ', contact.prenom)"
);
// Liste des jointures pour le libellé
$list_joint_libelle = array(
    "lien_courrier_contact ON lien_courrier_contact.courrier = courrier.courrier",
    "contact ON lien_courrier_contact.contact = contact.contact",
    "etablissement ON courrier.etablissement = etablissement.etablissement",
    "dossier_coordination ON courrier.dossier_coordination = dossier_coordination.dossier_coordination",
    "dossier_instruction ON courrier.dossier_instruction = dossier_instruction.dossier_instruction",
    "modele_edition ON courrier.modele_edition = modele_edition.modele_edition",
    "courrier_type ON courrier.courrier_type = courrier_type.courrier_type",
    "contact_type ON contact.contact_type = contact_type.contact_type",
    "contact_civilite ON contact.civilite = contact_civilite.contact_civilite"
);

// SELECT de la requête de récupération des courriers
$sql_courrier_select = " SELECT CONCAT(".implode(",' - ', ", $list_fields_libelle).") ";
// FROM de la requête de récupération des courriers
$sql_courrier_from = " FROM ".DB_PREFIXE."courrier LEFT JOIN ".DB_PREFIXE.implode(" LEFT JOIN ".DB_PREFIXE, $list_joint_libelle);

// WHERE de la requête de récupération des courriers joints par le DI
$sql_courrier_joint_by_dossier_instruction_where = " mailing IS FALSE AND dossier_instruction.dossier_instruction = <idx_di> AND courrier.courrier NOT IN (<idx>) ";

// WHERE de la requête de récupération des courriers joints par le DC
$sql_courrier_joint_by_dossier_coordination_where = " mailing IS FALSE AND dossier_coordination.dossier_coordination = <idx_dc> AND courrier.courrier NOT IN (<idx>) ";

// WHERE de la requête de récupération des courriers joints par l'établissement
$sql_courrier_joint_by_etablissement_where = " mailing IS FALSE AND etablissement.etablissement = <idx_etab> AND courrier.courrier NOT IN (<idx>) ";

// Filtre des types de courrier par la catégorie
$sql_courrier_type_select = "SELECT courrier_type.courrier_type, courrier_type.libelle ";
$sql_courrier_type_from = " FROM ".DB_PREFIXE."courrier_type LEFT JOIN ".DB_PREFIXE."courrier_type_categorie ON courrier_type.courrier_type_categorie = courrier_type_categorie.courrier_type_categorie ";
$sql_courrier_type_where = " WHERE ((courrier_type.om_validite_debut IS NULL AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE)) OR (courrier_type.om_validite_debut <= CURRENT_DATE AND (courrier_type.om_validite_fin IS NULL OR courrier_type.om_validite_fin > CURRENT_DATE))) AND ((courrier_type_categorie.om_validite_debut IS NULL AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE)) OR (courrier_type_categorie.om_validite_debut <= CURRENT_DATE AND (courrier_type_categorie.om_validite_fin IS NULL OR courrier_type_categorie.om_validite_fin > CURRENT_DATE))) AND courrier_type_categorie.objet like '%<obj>%' ";
$sql_courrier_type_order = " ORDER BY courrier_type.libelle ASC ";
$sql_courrier_type_by_id = "SELECT courrier_type.courrier_type, courrier_type.libelle FROM ".DB_PREFIXE."courrier_type WHERE courrier_type.courrier_type = '<idx>' ";

// Filtre des modèles d'éditions par le type du courrier
$sql_modele_edition_by_courrier_type = "SELECT modele_edition.modele_edition, modele_edition.libelle FROM ".DB_PREFIXE."modele_edition WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE))) AND modele_edition.courrier_type = <idx_courrier_type> ";

// Change le libellé du select des signataires
$sql_signataire="SELECT signataire.signataire, CONCAT_WS(' ', contact_civilite.libelle, signataire_qualite.libelle, signataire.nom, signataire.prenom) FROM ".DB_PREFIXE."signataire LEFT JOIN ".DB_PREFIXE."contact_civilite ON signataire.civilite = contact_civilite.contact_civilite LEFT JOIN ".DB_PREFIXE."signataire_qualite ON signataire.signataire_qualite = signataire_qualite.signataire_qualite WHERE ((signataire.om_validite_debut IS NULL AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE)) OR (signataire.om_validite_debut <= CURRENT_DATE AND (signataire.om_validite_fin IS NULL OR signataire.om_validite_fin > CURRENT_DATE))) ORDER BY signataire.nom ASC";
$sql_signataire_by_id = "SELECT signataire.signataire, CONCAT_WS(' ', contact_civilite.libelle, signataire_qualite.libelle, signataire.nom, signataire.prenom) FROM ".DB_PREFIXE."signataire LEFT JOIN ".DB_PREFIXE."contact_civilite ON signataire.civilite = contact_civilite.contact_civilite LEFT JOIN ".DB_PREFIXE."signataire_qualite ON signataire.signataire_qualite = signataire_qualite.signataire_qualite WHERE signataire = <idx>";
?>
