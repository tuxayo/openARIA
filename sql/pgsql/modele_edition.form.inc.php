<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/modele_edition.form.inc.php";

//
$champs=array(
    "modele_edition",
    "code",
    "libelle",
    "courrier_type",
    "description",
    "om_lettretype",
    "om_etat",
    "om_validite_debut",
    "om_validite_fin"
);

?>
