<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/programmation.inc.php');

/*
 * /!\ Fil d'ariane surchargé dans l'objet
 */
// Fil d'ariane
$ent = _("suivi")." -> "._("programmations")." -> "._("gestion");
if (isset($idx) && $idx != ']' && trim($idx) != '') {
    $ent .= "-> ".$idx." ";
}
if (isset($idz) && trim($idz) != '') {
    $ent .= " ".$idz." ";
}

//
$case_convocation_exploitants = " CASE programmation.convocation_exploitants
    WHEN 'non_effectuees' THEN '"._('non_effectuees')."'
    WHEN 'a_envoyer' THEN '". _('a_envoyer')."'
    WHEN 'envoyees' THEN '"._('envoyees')."'
    WHEN 'a_completer' THEN '"._('a_completer')."'
END ";

//
$case_convocation_membres = " CASE programmation.convocation_membres
    WHEN 'non_envoyees' THEN '"._('non_envoyees')."'
    WHEN 'a_envoyer' THEN '". _('a_envoyer')."'
    WHEN 'envoyees' THEN '"._('envoyees')."'
    WHEN 'a_renvoyer' THEN '"._('a_renvoyer')."'
END ";

//
$champAffiche = array(
    'programmation.programmation as "'._("programmation").'"',
    'programmation.semaine_annee as "'._("semaine_annee").'"',
    'programmation.version as "'._("version").'"',
    'programmation_etat.libelle as "'._("programmation_etat").'"',
    'to_char(programmation.date_modification ,\'DD/MM/YYYY\') as "'._("date_modification").'"',
    $case_convocation_exploitants.' as "'._("convocation_exploitants").'"',
    'to_char(programmation.date_envoi_ce ,\'DD/MM/YYYY\') as "'._("date_envoi_ce").'"',
    $case_convocation_membres.' as "'._("convocation_membres").'"',
    'to_char(programmation.date_envoi_cm ,\'DD/MM/YYYY\') as "'._("date_envoi_cm").'"', 
);

$tri = "ORDER BY programmation.annee DESC, programmation.numero_semaine DESC";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
