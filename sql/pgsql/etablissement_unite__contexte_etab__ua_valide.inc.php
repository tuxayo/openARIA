<?php
/**
 * Listing des UA validées dans le contexte d'un établissement.
 *
 * @package openaria
 * @version SVN : $Id: etablissement_unite.inc.php 1172 2015-03-17 13:45:35Z nmeucci $
 */

//
include "../sql/pgsql/etablissement_unite.inc.php";

// Fil d'ariane
$ent = _("etablissements")." -> "._("unites d'accessibilite");

//
$tab_title = _("UA");

//
$champAffiche = array_merge(
    $champAffiche_ua, 
    $champAffiche_cas1, 
    $champAffiche_derogation
);

// Filtre listing sous formulaire - etablissement
if (in_array($retourformulaire, $foreign_keys_extended["etablissement"])
    && isset($idx)) {
    $selection = " 
WHERE 
    (etablissement_unite.etablissement = '".$idx."') 
    AND etablissement_unite.etat='valide' 
    AND etablissement_unite.archive IS FALSE 
";
} else {
    $selection = " WHERE NULL ";
}

// Aucune recherche dans ce contexte
$champRecherche= array(
);

?>
