<?php
/**
 * Ce scrip permet de configurer le listing 'Mes visites à réaliser'.
 *
 * L'objectif de ce listing est de présenter à l'utilisateur toutes les
 * visites du jour et à venir pour lesquelles il est le technicien.
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/dossier_instruction.inc.php";

// Titre de la page
$ent = _("dossiers")." -> "._("DI")." -> "._("dossiers a qualifier");

// Pas de recherche avancée
$options = array();

// Filtre du listing
$selection = " WHERE dossier_instruction.a_qualifier = TRUE";

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
