<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/lien_reunion_r_instance_r_i_membre.inc.php";

// En sous-formulaire de réunion
if (!empty($retourformulaire)
    && $retourformulaire == 'reunion') {
    // Cache le bouton ajouter
    $tab_actions['corner']['ajouter'] = null;
    // On supprime la colonne réunion inutile dans ce contexte
    $champAffiche = array(
    'lien_reunion_r_instance_r_i_membre.lien_reunion_r_instance_r_i_membre as "'._("lien_reunion_r_instance_r_i_membre").'"',
    'reunion_instance.libelle as "'._("instance").'"',
    'reunion_instance_membre.membre as "'._("membre").'"',
    'lien_reunion_r_instance_r_i_membre.observation as "'._("observation").'"',
    );
    //
    $tri = " ORDER BY reunion_instance.ordre ASC ";
}

?>
