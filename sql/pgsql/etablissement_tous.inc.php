<?php
/**
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/etablissement.inc.php');

// Fil d'ariane
$ent = _("etablissements")." -> "._("etablissement_tous");

// Masquage de toutes les voies publiques et des établissements archivés le cas échéant

if (isset($_GET['valide']) and $_GET['valide'] == 'false'){

	$selection = "WHERE etablissement_nature.nature != 'Aménagement voie publique'";
}
else{
    
	$selection=" WHERE
	(etablissement_nature.nature != 'Aménagement voie publique')
	AND	
	(((etablissement.om_validite_debut IS NULL AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)) OR (etablissement.om_validite_debut <= CURRENT_DATE AND (etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))))";
}

$case_validite = 'CASE WHEN ((etablissement.om_validite_debut IS NULL AND ';
$case_validite .= '(etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE))';
$case_validite .= ' OR ';
$case_validite .= '(etablissement.om_validite_debut <= CURRENT_DATE AND ';
$case_validite .= '(etablissement.om_validite_fin IS NULL OR etablissement.om_validite_fin > CURRENT_DATE)))';
$case_validite .= ' IS true THEN \''._("Non").'\' ELSE \''._("Oui").'\' END';
$champAffiche = array(
    'etablissement.etablissement as "'._("etablissement").'"',
    'etablissement.code as "'._("code").'"',
    'etablissement.libelle as "'._("libelle").'"',
    'concat(etablissement.adresse_numero, 
        \' \', etablissement.adresse_numero2, \' \', voie.libelle,
        \' \', etablissement.adresse_cp, \' (\', arrondissement.libelle,\')\')
        as "'._("adresse").'"',
    'etablissement_type.libelle as "'._("etablissement_type").'"',
    'etablissement_categorie.libelle as "'._("etablissement_categorie").'"',
    'to_char(etablissement.si_prochaine_visite_date,\'DD/MM/YYYY\') as "'._("si_prochaine_visite_date").'"',
    'to_char(si_prochaine_visite_periodique_date_previsionnelle,\'DD/MM/YYYY\') as "'._("si_prochaine_visite_periodique_date_previsionnelle").'"',
    // .lastcol !important
    $case_validite.' as "'._("archive").'"',
);
//
$champRecherche = array(
    'etablissement.code as "'._("code").'"',
    'etablissement.libelle as "'._("libelle").'"',
);

//
$sousformulaire_parameters["piece"] = array(
    "title" => _("documents entrants"),
);

// Recherche avancée
if (!isset($options)) {
    $options = array();
}

// Affichage du bouton de redirection vers le SIG externe si configuré
if ($f->getParameter('option_sig') == 'sig_externe' AND ($obj == 'etablissement_tous' OR $obj == 'etablissement_referentiel_erp')) {
    $tab_actions['left']["localiser-sig-externe"] = array(
                'lien' => 'form.php?obj=etablissement&amp;action=30&amp;idx=',
                'id' => '',
                'lib' => '<span class="om-icon om-icon-16 om-icon-fix sig-16" title="'._('Localiser').'">'._('Localiser').'</span>',
                'rights' => array('list' => array('etablissement_tous', 'etablissement_tous_consulter'), 'operator' => 'OR'),
                'ordre' => 20,
                'target' => "blank",
                'ajax' => false);
}

// Options pour les select de faux booléens
$args = array(
    0 => array("", "t", "f", ),
    1 => array(_("choisir"), _("Oui"), _("Non"), ),
);

$champs['etablissement'] = array(
    'table' => 'etablissement',
    'colonne' => 'code',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('code')
);
$champs['libelle'] = array(
    'table' => 'etablissement',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('libelle')
);
$champs['adresse_numero'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_numero',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('numero')
);
$champs['adresse_voie'] = array(
    'table' => 'voie',
    'colonne' => 'libelle',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('voie')
);
$champs['adresse_arrondissement'] = array(
    'table' => 'etablissement',
    'colonne' => 'adresse_arrondissement',
    'type' => 'select',
    'libelle' => _('arrondissement')
);
$champs['etablissement_type'] = array(
    'table' => 'etablissement_type',
    'colonne' => 'etablissement_type',
    'type' => 'select',
    'libelle' => _('etablissement_type')
);
$champs['etablissement_categorie'] = array(
    'table' => 'etablissement_categorie',
    'colonne' => 'etablissement_categorie',
    'type' => 'select',
    'libelle' => _('etablissement_categorie')
);
$champs['etablissement_etat'] = array(
    'table' => 'etablissement_etat',
    'colonne' => 'etablissement_etat',
    'type' => 'select',
    'libelle' => _('etablissement_etat')
);
$champs['ref_patrimoine'] = array(
    'table' => 'etablissement',
    'colonne' => 'ref_patrimoine',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('ref_patrimoine')
);
$champs['siret'] = array(
    'table' => 'etablissement',
    'colonne' => 'siret',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('siret')
);
$champs['nom'] = array(
    'table' => 'contact',
    'where' => 'injoin',
    'tablejoin' => 'INNER JOIN (
                SELECT DISTINCT etablissement 
                FROM '.DB_PREFIXE.'contact 
                WHERE lower(contact.nom) like %s ) 
            AS A1 
          ON A1.etablissement = etablissement.etablissement' ,
    'colonne' => 'nom',
    'type' => 'text',
    'taille' => 40,
    'libelle' => _('contact')
);
$champs['geolocalise'] = array(
    'table' => 'etablissement',
    'colonne' => 'geolocalise',
    'type' => 'select',
    'libelle' => _('localise'),
    "subtype" => "manualselect",
    "args" => $args,
);

$tmp_advsearch_options = array(
    'type' => 'search',
    'display' => true,
    'advanced'  => $champs,
    'default_form'  => 'advanced',
    'absolute_object' => 'etablissement',
);

// Affichage du bouton de redirection vers le SIG externe si configuré
// Le bouton est affiché seulement pour l'objet etablissement_tous
if ($f->getParameter('option_sig') == 'sig_externe' AND $obj == 'etablissement_tous') {
    $tmp_advsearch_options['export'] = array(
        'geoaria' => array(
            'right' => 'etablissement_tous_geoaria',
            'url' => '../scr/form.php?obj=etablissement_tous&action=31&idx=0',
        ),
    );
}

$options[] = $tmp_advsearch_options;

/**
 * Options
 */

// Code couleur ouvert/fermé
$options[] = array(
    "type" => "condition",
    "field" => "etablissement_statut.statut_administratif",
    "case" => array(
        "0" => array(
            "values" => array("Ouvert", ),
            "style" => "etablissement_ouvert",
        ),
        "1" => array(
            "values" => array("Fermé", ),
            "style" => "etablissement_ferme",
        ),
    ),
);
// Candidature interne
$options[] = array(
    "type" => "condition",
    "field" => $case_validite,
    "case" => array(
            array(
                "values" => array("Oui", ),
                "style" => "etablissement_archive",
                ),
            ),
    );
//
$options[] = array(
    "type" => "pagination_select",
    "display" => ""
);

?>
