<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../core/sql/pgsql/om_profil.form.inc.php";

$champs=array(
    "om_profil.om_profil",
    "om_profil.libelle",
    "om_profil.hierarchie",
    //
    "array_to_string(
        array_agg(
            distinct(om_droit.libelle)),
    ';') as permissions",
);

//
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."om_droit
    ON om_droit.om_profil = om_profil.om_profil
";

//
$selection = " GROUP BY om_profil.om_profil ";

?>
