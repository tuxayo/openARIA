<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/reunion_type.form.inc.php";

//
$champs = array(
    //
    "reunion_type.service",
    //
    "reunion_type.reunion_type",
    "reunion_type.code",
    "reunion_type.libelle",
    "reunion_type.lieu_adresse_ligne1",
    "reunion_type.lieu_adresse_ligne2",
    "reunion_type.lieu_salle",
    "reunion_type.heure",
    "reunion_type.listes_de_diffusion",
    "reunion_type.modele_courriel_convoquer",
    "reunion_type.modele_courriel_cloturer",
    "reunion_type.commission",
    "reunion_type.president",
    "reunion_type.modele_ordre_du_jour",
    "reunion_type.modele_compte_rendu_global",
    "reunion_type.modele_compte_rendu_specifique",
    "reunion_type.modele_feuille_presence",
    //
    "array_to_string(
        array_agg(
            distinct(reunion_type_reunion_categorie.reunion_categorie)),
    ';') as categories_autorisees",
    //
    "array_to_string(
        array_agg(
            distinct(reunion_type_reunion_avis.reunion_avis)),
    ';') as avis_autorises",
    //
    "array_to_string(
        array_agg(
            distinct(reunion_type_reunion_instance.reunion_instance)),
    ';') as instances_autorisees",
    //
    "reunion_type.om_validite_debut",
    "reunion_type.om_validite_fin",
);
 
//
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_avis
    ON reunion_type_reunion_avis.reunion_type = reunion_type.reunion_type
LEFT JOIN ".DB_PREFIXE."reunion_avis
    ON reunion_type_reunion_avis.reunion_avis = reunion_avis.reunion_avis
LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_instance
    ON reunion_type_reunion_instance.reunion_type = reunion_type.reunion_type
LEFT JOIN ".DB_PREFIXE."reunion_instance
    ON reunion_type_reunion_instance.reunion_instance = reunion_instance.reunion_instance
LEFT JOIN ".DB_PREFIXE."reunion_type_reunion_categorie
    ON reunion_type_reunion_categorie.reunion_type = reunion_type.reunion_type
LEFT JOIN ".DB_PREFIXE."reunion_categorie
    ON reunion_type_reunion_categorie.reunion_categorie = reunion_categorie.reunion_categorie
";

//
$selection = " GROUP BY reunion_type.libelle, reunion_type.reunion_type ";

// Filtre par service
$sql_president = "
SELECT 
    reunion_instance.reunion_instance, 
    reunion_instance.libelle 
FROM ".DB_PREFIXE."reunion_instance 
WHERE
    reunion_instance.service = <idx_service>
    AND ((reunion_instance.om_validite_debut IS NULL 
          AND (reunion_instance.om_validite_fin IS NULL 
               OR reunion_instance.om_validite_fin > CURRENT_DATE)) 
         OR (reunion_instance.om_validite_debut <= CURRENT_DATE 
             AND (reunion_instance.om_validite_fin IS NULL 
                  OR reunion_instance.om_validite_fin > CURRENT_DATE))) 
ORDER BY reunion_instance.libelle ASC
";
$sql_president_by_id = "
SELECT 
    reunion_instance.reunion_instance, 
    reunion_instance.libelle 
FROM ".DB_PREFIXE."reunion_instance 
WHERE 
    reunion_instance = <idx>
";

// Liaison NaN - reunion_type/reunion_categorie
$sql_categories_autorisees = "
SELECT
    reunion_categorie.reunion_categorie,
    reunion_categorie.libelle as lib
FROM ".DB_PREFIXE."reunion_categorie
WHERE reunion_categorie.service = <idx_service>
AND ((reunion_categorie.om_validite_debut IS NULL AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)) OR (reunion_categorie.om_validite_debut <= CURRENT_DATE AND (reunion_categorie.om_validite_fin IS NULL OR reunion_categorie.om_validite_fin > CURRENT_DATE)))
ORDER BY lib";
$sql_categories_autorisees_by_id = "
SELECT
    reunion_categorie.reunion_categorie,
    reunion_categorie.libelle as lib
FROM ".DB_PREFIXE."reunion_categorie
WHERE reunion_categorie.service = <idx_service>
    AND reunion_categorie.reunion_categorie IN (<idx>)
ORDER BY reunion_categorie.ordre, lib";

// Liaison NaN - reunion_type/reunion_avis
$sql_avis_autorises = "
SELECT
    reunion_avis.reunion_avis,
    reunion_avis.libelle as lib
FROM ".DB_PREFIXE."reunion_avis
WHERE reunion_avis.service = <idx_service>
AND ((reunion_avis.om_validite_debut IS NULL AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)) OR (reunion_avis.om_validite_debut <= CURRENT_DATE AND (reunion_avis.om_validite_fin IS NULL OR reunion_avis.om_validite_fin > CURRENT_DATE)))
ORDER BY lib";
$sql_avis_autorises_by_id = "
SELECT
    reunion_avis.reunion_avis,
    reunion_avis.libelle as lib
FROM ".DB_PREFIXE."reunion_avis
WHERE reunion_avis.service = <idx_service>
    AND reunion_avis.reunion_avis IN (<idx>)
ORDER BY lib";

// Liaison NaN - reunion_type/reunion_instance
$sql_instances_autorisees = "
SELECT
    reunion_instance.reunion_instance,
    reunion_instance.libelle as lib
FROM ".DB_PREFIXE."reunion_instance
WHERE reunion_instance.service = <idx_service>
AND ((reunion_instance.om_validite_debut IS NULL AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)) OR (reunion_instance.om_validite_debut <= CURRENT_DATE AND (reunion_instance.om_validite_fin IS NULL OR reunion_instance.om_validite_fin > CURRENT_DATE)))
ORDER BY lib";
$sql_instances_autorisees_by_id = "
SELECT
    reunion_instance.reunion_instance,
    reunion_instance.libelle as lib
FROM ".DB_PREFIXE."reunion_instance
WHERE reunion_instance.service = <idx_service>
    AND reunion_instance.reunion_instance IN (<idx>)
ORDER BY lib";

// filtre les modèles d'édition
$sql_modele_edition = "SELECT modele_edition.modele_edition, modele_edition.libelle 
FROM ".DB_PREFIXE."modele_edition 
LEFT JOIN ".DB_PREFIXE."courrier_type ON courrier_type.courrier_type = modele_edition.courrier_type
WHERE ((modele_edition.om_validite_debut IS NULL AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)) OR (modele_edition.om_validite_debut <= CURRENT_DATE AND (modele_edition.om_validite_fin IS NULL OR modele_edition.om_validite_fin > CURRENT_DATE)))
AND LOWER(courrier_type.code) = LOWER('<courrier_type_code>')
ORDER BY modele_edition.libelle ASC";

?>
