<?php
/**
 * Ce script permet...
 *
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../sql/pgsql/dossier_instruction.inc.php');

// Fil d'ariane
$ent = _("dossiers")." -> "._("DI")." -> "._("tous les plans");

// Modification du WHERE
$selection = " WHERE dossier_type.code = 'PLAN' ";

//
$key = array_search('visite', $sousformulaire);
if ($key !== false && isset($sousformulaire[$key])) {
    unset($sousformulaire[$key]);
}

// Filtre sur le service de l'utilisateur
include "../sql/pgsql/filter_service.inc.php";

?>
