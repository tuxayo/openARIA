<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/prescription_reglementaire.form.inc.php";

// Liaison avec les catégories d'établissement
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_prescription_reglementaire_etablissement_categorie
    ON lien_prescription_reglementaire_etablissement_categorie.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
LEFT JOIN ".DB_PREFIXE."etablissement_categorie
    ON lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie = etablissement_categorie.etablissement_categorie
";

// Liaison avec les types d'établissement
$tableSelect .= "
LEFT JOIN ".DB_PREFIXE."lien_prescription_reglementaire_etablissement_type
    ON lien_prescription_reglementaire_etablissement_type.prescription_reglementaire = prescription_reglementaire.prescription_reglementaire
LEFT JOIN ".DB_PREFIXE."etablissement_type
    ON lien_prescription_reglementaire_etablissement_type.etablissement_type = etablissement_type.etablissement_type
";

$champs=array(
    "prescription_reglementaire.prescription_reglementaire",
    "prescription_reglementaire.service",
    "prescription_reglementaire.tete_de_chapitre1",
    "prescription_reglementaire.tete_de_chapitre2",
    "prescription_reglementaire.libelle",
    "prescription_reglementaire.description_pr_om_html",
    "prescription_reglementaire.defavorable",
    "array_to_string(
        array_agg(
            distinct(lien_prescription_reglementaire_etablissement_categorie.etablissement_categorie)),
    ';') as etablissement_categorie",
    "array_to_string(
        array_agg(
            distinct(lien_prescription_reglementaire_etablissement_type.etablissement_type)),
    ';') as etablissement_type",
    "prescription_reglementaire.om_validite_debut",
    "prescription_reglementaire.om_validite_fin",
);

$selection = " GROUP BY prescription_reglementaire.prescription_reglementaire ";

// Liaison NaN - prescriptions réglementaires/ types d'établissement
$sql_etablissement_type="SELECT etablissement_type.etablissement_type, etablissement_type.libelle as lib FROM ".DB_PREFIXE."etablissement_type WHERE ((etablissement_type.om_validite_debut IS NULL AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE)) OR (etablissement_type.om_validite_debut <= CURRENT_DATE AND (etablissement_type.om_validite_fin IS NULL OR etablissement_type.om_validite_fin > CURRENT_DATE))) ORDER BY lib ASC";
$sql_etablissement_type_by_id = "
SELECT
    etablissement_type.etablissement_type,
    etablissement_type.libelle as lib
FROM ".DB_PREFIXE."etablissement_type
WHERE etablissement_type IN (<idx>)
ORDER BY lib ASC";

// Liaison NaN - prescriptions réglementaires/ catégories d'établissement
$sql_etablissement_categorie="SELECT etablissement_categorie.etablissement_categorie, etablissement_categorie.libelle as lib FROM ".DB_PREFIXE."etablissement_categorie WHERE ((etablissement_categorie.om_validite_debut IS NULL AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE)) OR (etablissement_categorie.om_validite_debut <= CURRENT_DATE AND (etablissement_categorie.om_validite_fin IS NULL OR etablissement_categorie.om_validite_fin > CURRENT_DATE))) ORDER BY lib ASC";
$sql_etablissement_categorie_by_id = "
SELECT
    etablissement_categorie.etablissement_categorie,
    etablissement_categorie.libelle as lib
FROM ".DB_PREFIXE."etablissement_categorie
WHERE etablissement_categorie IN (<idx>)
ORDER BY lib ASC";

?>
