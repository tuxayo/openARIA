<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/visite.inc.php');

// SELECT 
$champAffiche = array(
    'visite.visite as "'._("visite").'"',
    'to_char(visite.date_visite ,\'DD/MM/YYYY\') as "'._("date_visite").'"',
    'heure_debut as "'._("heure_visite").'"',
    'visite_etat.libelle as "'._("visite_etat").'"',
    'acteur.nom_prenom as "'._("acteur").'"',
    'visite.convocation_exploitants as "'._("convocation_exploitants").'"',
    'to_char(visite.date_annulation ,\'DD/MM/YYYY\') as "'._("date_annulation").'"',
    'programmation_version_creation as "'._("version à la création").'"',
    'programmation_version_modification as "'._("version à la modification").'"',
    'programmation_version_annulation as "'._("version à l'annulation").'"',
    );

// Cache le bouton ajouter
// Une entrée de menu dédiée permet d'ajouter un DC
$tab_actions['corner']['ajouter'] = null;

?>