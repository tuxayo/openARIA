<?php
/**
 * Surcharge de contrainte pour afficher directement
 * le formulaire spécifique de synchronisation depuis le menu Administration & Paramétrage.
 * 
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../sql/pgsql/contrainte.inc.php";

// Fil d'Ariane
$ent = _("administration_parametrage")." -> "._("contraintes")." -> "._("synchronisation");

?>
