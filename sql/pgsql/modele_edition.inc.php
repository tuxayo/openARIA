<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include "../gen/sql/pgsql/modele_edition.inc.php";

// SELECT 
$champAffiche = array(
    'modele_edition.modele_edition as "'._("modele_edition").'"',
    'modele_edition.code as "'._("code").'"',
    'modele_edition.libelle as "'._("libelle").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    'om_lettretype.libelle as "'._("om_lettretype").'"',
    );
// On affiche les champs de date de validité uniquement lorsque le paramètre
// d'affichage des éléments expirés est activé
if ($om_validite == true && isset($_GET['valide']) && $_GET['valide'] === 'false') {
    $champAffiche = array_merge($champAffiche, $displayed_fields_validite);
}
//
$champNonAffiche = array(
    'om_etat.libelle as "'._("om_etat").'"',
    'modele_edition.description as "'._("description").'"',
    );
//
$champRecherche = array(
    'modele_edition.modele_edition as "'._("modele_edition").'"',
    'modele_edition.code as "'._("code").'"',
    'modele_edition.libelle as "'._("libelle").'"',
    'courrier_type.libelle as "'._("courrier_type").'"',
    'om_lettretype.libelle as "'._("om_lettretype").'"',
    );

/**
 * Gestion SOUSFORMULAIRE => $sousformulaire
 */
$sousformulaire = array(
);

?>
