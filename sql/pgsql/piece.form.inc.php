<?php
/**
 * @package openaria
 * @version SVN : $Id$
 */

//
include('../gen/sql/pgsql/piece.form.inc.php');

$champs=array(
    // Fieldset 1
    "piece",
    "nom",
    "piece_type",
    "uid",
    "om_date_creation",
    "date_reception",
    "date_emission",
    "piece_statut",
    "lu",
    // Fieldset 2
    "service",
    "choix_lien",
    "etablissement",
    "dossier_coordination",
    "dossier_instruction",
    // Fieldset 3
    "suivi",
    "date_butoir",
    "commentaire_suivi"
);

// Champs select
$choix_lien = array(
    array("", "etablissement", "dossier_coordination", "dossier_instruction",),
    array(_("Choisir")." "._("lien"), _("etablissement"), _("dossier_coordination"), _("dossier_instruction"), ),
);

?>
