<?php
/**
 * Script de paramétrage du listing des 'passages en réunion'.
 *
 * Dans le contexte d'une réunion. 
 *
 * @package openaria
 * @version SVN : $Id$
 */

include "../sql/pgsql/dossier_instruction_reunion.inc.php";

//
$ent = "-> "._("passage en reunion");

// Cache le bouton ajouter
$tab_actions['corner']['ajouter'] = null;

?>
